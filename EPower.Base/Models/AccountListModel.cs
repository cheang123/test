﻿using HB01.Domain.Enums;
using HB01.Domain.ListModels;
using SoftTech.Helper;

namespace EPower.Base.Models
{
    public class AccountListModel : BaseListModel
    {
        public int RenderId { get; set; }
        public int ParentId { get; set; }
        public int ParentAccountId { get; set; }
        public int AccountTypeId { get; set; }
        public string AccountTypeCode { get; set; }
        public AccountListModel()
        {

        }
        //public AccountListModel(TBL_ACCOUNT_CHART account) : base(account)
        //{

        //}
        private string _accountNature;
        public string AccountNature
        {
            get
            {
                return ResourceHelper.Translate(_accountNature);
            }
            set
            {
                _accountNature = value;
            }
        }
        public AccountNatures Nature { get; set; }
        public string AccountName { get; set; }
        public ClassifiyAccountType ClassifiyAccountType { get; set; }
        public string AccountType { get; set; }
        public string AccountCode { get; set; }
        public string DisplayAccountName { get; set; }
        public string Currency { get; set; }
        public decimal AccountBalance { get; set; }
    }
}
