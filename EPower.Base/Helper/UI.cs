﻿using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.BandedGrid;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Base.Helper
{
    public static class FormatDataHelper
	{
		public static string DefaultNumber { get; set; } = "#,##0.00;-#,##0.00;-";
		public static string NumberFormat = "#,##0.00;-#,##0.00;0.00";
		public static string PriceFormat = "#,##0.00##;-#,##0.00##;0.00";
		public static string PriceDisplayFormat = "#,##0.00##;-#,##0.00##;-";
		public static string ShortDate => "dd-MM-yyyy";
		//public static string ShortDateReport => Current.Company.CompanySetting.LanguageKey == "km-KH" ? "dd MMMM yyyy" : "dd MMM yyyy";
		public static string ShortDateReport => "dd MMMM yyyy";
		//public static string ShortMonthReport => Current.Company.CompanySetting.LanguageKey == "km-KH" ? "MMMM yyyy" : "MMM yyyy";
		public static string ShortMonthReport => "MMMM yyyy";
		public static string LongDate => "dd-MM-yyyy hh:mm:ss tt";
		public static string CurrencyFormat { get; set; } = "";

		public static string ToStringPrice(this decimal val)
		{
			if (!string.IsNullOrEmpty(CurrencyFormat)) return val.ToString(CurrencyFormat);
			return val.ToString(PriceDisplayFormat);
		}
		public static string ToCurrency(this decimal val)
		{
			return val.ToString(NumberFormat);
		}
		public static string ToStringNumberFormat(this decimal val)
		{
			if (!string.IsNullOrEmpty(CurrencyFormat))
			{
				return val.ToString(CurrencyFormat);
			}
			return val.ToString(NumberFormat);
		}
		public static string ToShortDate(this DateTime d)
		{
			return d.ToString(ShortDate);
		}
		//public static string ToShortDateReport(this DateTime d)
		//{
		//	return d.ToString(ShortDateReport);
		//}
		//public static string ToLongDate(this DateTime d)
		//{
		//	return d.ToString(LongDate);
		//}


		public static void SetDefaultFontPointer()
		{
			WindowsFormsSettings.DefaultFont = new System.Drawing.Font("Khmer Kep", 9);
		}
		public static void SetDefaultFontEPower()
		{
			WindowsFormsSettings.DefaultFont = new System.Drawing.Font("Khmer OS System", 9);
		}

		public static void SetDefaultProperty(this DataGridView dgv)
		{
			dgv.AllowUserToResizeColumns = true;
			dgv.RowTemplate.Height = 27;
			dgv.ColumnHeadersHeight = 30;
			dgv.Cursor = Cursors.Hand;
			dgv.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(247, 247, 247);
			dgv.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(0, 0, 0);
			dgv.CellBorderStyle = DataGridViewCellBorderStyle.Single;
			//dgv.GridColor = Color.FromArgb(242, 242, 242);
			dgv.GridColor = Color.WhiteSmoke;
			dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;
		}

		public static void CustomOddColor(this DataGridView dgv)
		{
			foreach (DataGridViewRow row in dgv.Rows)
			{
				if (row.Index % 2 == 0) continue;
				row.DefaultCellStyle.BackColor = Color.WhiteSmoke;
				row.DefaultCellStyle.SelectionBackColor = Color.FromArgb(132, 187, 246);
			}
		}

		public static void DisableSortMode(this DataGridView dgv)
		{
			foreach (DataGridViewColumn column in dgv.Columns)
			{
				column.SortMode = DataGridViewColumnSortMode.NotSortable;
			}
		}

		public static object GetCellValueFromColumnHeader(this DataGridViewCellCollection CellCollection, string HeaderText)
		{
			return CellCollection.Cast<DataGridViewCell>().First(c => c.OwningColumn.HeaderText == HeaderText)?.Value;
		}

		public static void SetDefaultReportProperty(this DataGridView dgv)
		{
			dgv.AllowUserToResizeColumns = true;
			dgv.CellBorderStyle = DataGridViewCellBorderStyle.Single;
			dgv.GridColor = Color.FromArgb(247, 247, 247);
			dgv.Cursor = Cursors.Hand;
		}
		public static void FormatRowNo(this DataGridViewColumn col)
		{
			col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
			col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
		}

		public static void FormatCenter(this DataGridViewColumn col)
		{
			col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
			col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
		}

		public static void FormatShortDate(this DataGridViewColumn col)
		{
			col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
			col.DefaultCellStyle.Format = ShortDate;
			col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
		}
		public static void FormatLongDate(this DataGridViewColumn col)
		{
			col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
			col.DefaultCellStyle.Format = LongDate;
			col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
		}
		public static void FormatViewNumeric(this DataGridViewColumn col)
		{
			col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
			col.DefaultCellStyle.Format = (string.IsNullOrEmpty(CurrencyFormat)) ? DefaultNumber : CurrencyFormat;
			col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			col.ValueType = typeof(decimal);
		}


		public static void RegisterDecimalSeparater(this DataGridViewColumn col)
		{
			col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
			col.DefaultCellStyle.Format = (string.IsNullOrEmpty(CurrencyFormat)) ? DefaultNumber : CurrencyFormat;
			col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			col.ValueType = typeof(decimal);

		}

		public static void FormatNumeric(this DataGridViewColumn col)
		{
			col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
			col.DefaultCellStyle.Format = NumberFormat;
			col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			col.ValueType = typeof(decimal);
		}
		public static void FormatPriceNumeric(this DataGridViewColumn col)
		{
			col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
			col.DefaultCellStyle.Format = (string.IsNullOrEmpty(CurrencyFormat)) ? DefaultNumber : CurrencyFormat;
			col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			col.ValueType = typeof(decimal);
		}
		public static void FormatPriceNumericDisplay(this DataGridViewColumn col)
		{
			col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
			col.DefaultCellStyle.Format = PriceDisplayFormat;
			col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			col.ValueType = typeof(decimal);
		}
		public static void HederStyle(this DataGridView dgv)
		{
			dgv.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 120, 189);
			dgv.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
			dgv.ColumnHeadersHeight = 33;
			dgv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
		}
	}

	public class ExDataGridView : DataGridView
	{
		public ExDataGridView()
		{
			this.ApplyDefaultStyle();

			this.ReadOnly = true;
			this.EnableHeadersVisualStyles = false;

			this.KeyDown += new KeyEventHandler(ExDataGridView_KeyDown);
			this.CellDoubleClick += new DataGridViewCellEventHandler(ExDataGridView_CellDoubleClick);
			this.Sorted += new EventHandler(ExDataGridView_Sorted);
			this.DataError += ExDataGridView_DataError;
			this.DataSourceChanged += new EventHandler(ExDataGridView_DataSourceChanged);
		}

		private void ExDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
		{
			// intentionally blank.
		}

		protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
		{
			// overides ProcessDataGridViewKey &  ProcessDialogKey work more smoothly than ProcessCmdKey
			// But work with ExDropDownColumn work more functional with Process CmdKey.
			if ((keyData == (Keys.Enter)))
			{
				SendKeys.Send("{up}");
				SendKeys.Send("{tab}");
				return false;
			}
			else
			{
				return base.ProcessCmdKey(ref msg, keyData);
			}
		}

		void ExDataGridView_DataSourceChanged(object sender, EventArgs e)
		{
			this.MarkDefaultColor();
		}

		void ExDataGridView_Sorted(object sender, EventArgs e)
		{
			this.MarkDefaultColor();
		}

		public void MarkDefaultColor()
		{
			var numberColumns = this.Columns.Cast<DataGridViewColumn>().Where(x => x.ValueType == typeof(decimal)).Select(x => x.Name);
			foreach (DataGridViewRow row in this.Rows)
			{
				foreach (var column in numberColumns)
				{
					var cell = row.Cells[column];
					if (cell.Value != null &&
						DataHelper.ParseToDecimal(cell.Value.ToString()) < 0)
					{
						cell.Style.ForeColor = Color.Red;
						cell.Style.SelectionForeColor = Color.Red;
					}
				}
			}
		}

		void ExDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.ColumnIndex != -1 && e.RowIndex != -1 && this.SelectedRows.Count > 0)
			{
				//IPage parent = this.Parent as IPage;
				//if (parent != null)
				//{
				//	parent.View();
				//}
			}
		}

		void ExDataGridView_KeyDown(object sender, KeyEventArgs e)
		{
			//IPage parent = this.Parent as IPage;
			//if (parent != null)
			//{

			//	if (e.KeyCode == Keys.A)
			//	{
			//		parent.AddNew();
			//	}
			//	else if (e.KeyCode == Keys.Delete)
			//	{
			//		parent.Remove();
			//	}
			//	else if (e.KeyCode == Keys.Space)
			//	{
			//		parent.Edit();
			//	}
			//	else if (e.KeyCode == Keys.Enter)
			//	{
			//		e.Handled = true;
			//		parent.View();
			//	}
			//}
		}

	}

	public class DataGridViewExComboBoxColumn : DataGridViewColumn
	{
		public DataGridViewExComboBoxColumn() : base(new DataGridViewExComboBoxCell())
		{
		}

		public override DataGridViewCell CellTemplate
		{
			get
			{
				return base.CellTemplate;
			}
			set
			{
				DataGridViewExComboBoxCell dataGridViewComboBoxCell = value as DataGridViewExComboBoxCell;
				// Ensure that the cell used for the template is a TimeCell.
				if (value != null && !value.GetType().IsAssignableFrom(typeof(DataGridViewExComboBoxCell)))
				{
					throw new InvalidCastException("Must be a ExComboBox");
				}
				base.CellTemplate = value;
			}
		}

		public Func<IDropDownPanel> DropDownPanel { get; set; }

	}

	public class DataGridViewExComboBoxCell : DataGridViewTextBoxCell
	{
		public DataGridViewExComboBoxCell() : base()
		{
			this.Style.Format = "#";
		}

		//public new object Value
		//{
		//	get
		//	{
		//		return base.Value;
		//	}
		//	set
		//	{
		//		// this.SelectedValue = value; 
		//		if (this.DropDownPanel.Parent != null)
		//		{
		//			this.DropDownPanel.Parent.Value = value;
		//		}
		//		base.Value = value;
		//	}
		//}

		//public new object FormattedValue
		//{
		//	get
		//	{
		//		return this.DropDownPanel.GetDisplayText(base.Value);
		//	}
		//}

		//private IDropDownPanel dropDownPanel = null;
		//public IDropDownPanel DropDownPanel
		//{
		//	get
		//	{
		//		if (this.OwningColumn is DataGridViewExComboBoxColumn)
		//		{
		//			var col = (DataGridViewExComboBoxColumn)this.OwningColumn;
		//			if (dropDownPanel == null && col.DropDownPanel != null)
		//			{
		//				dropDownPanel = col.DropDownPanel();
		//			}
		//			return dropDownPanel;
		//		}
		//		return null;
		//	}
		//}



		//public override void InitializeEditingControl(int rowIndex, object
		//	initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
		//{
		//	// Set the value of the editing control to the current cell value.

		//	base.InitializeEditingControl(rowIndex, initialFormattedValue,
		//		dataGridViewCellStyle);
		//	var editor = DataGridView.EditingControl as DataGridViewExComboBoxCellEditingControl;
		//	if (editor.DropDownPanel == null)
		//	{
		//		editor.DropDownPanel = DropDownPanel;
		//	}
		//	editor.Value = this.Value;
		//}

		//protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context)
		//{
		//	var fv = this.DropDownPanel?.GetDisplayText(value);
		//	return fv;
		//}

		//public override Type EditType
		//{
		//	get
		//	{
		//		return typeof(DataGridViewExComboBoxCellEditingControl);
		//	}
		//}

		//public override Type ValueType
		//{
		//	get
		//	{
		//		return typeof(object);
		//	}
		//}


		//public override Type FormattedValueType
		//{
		//	get
		//	{
		//		return typeof(object);
		//	}
		//}

		//public override object DefaultNewRowValue
		//{
		//	get
		//	{
		//		return null;
		//	}
		//}
	}

	//public class DataGridViewExComboBoxCellEditingControl : ExComboBox, IDataGridViewEditingControl
	//{
	//	DataGridView dataGridView;
	//	private bool valueChanged = false;
	//	int rowIndex;

	//	//public DataGridViewExComboBoxCellEditingControl()
	//	//{
	//	//}

	//	//// Implements the IDataGridViewEditingControl.EditingControlFormattedValue 
	//	//// property.
	//	//public object EditingControlFormattedValue
	//	//{
	//	//	get
	//	//	{
	//	//		var val = this.Value;
	//	//		return val;
	//	//	}
	//	//	set
	//	//	{
	//	//		if (value is String)
	//	//		{
	//	//			var v = 0;
	//	//			int.TryParse(value.ToString(), out v);
	//	//			this.Value = v;
	//	//		}
	//	//	}
	//	//}
	//	// Implements the 
	//	// IDataGridViewEditingControl.GetEditingControlFormattedValue method.
	//	//public object GetEditingControlFormattedValue(
	//	//	DataGridViewDataErrorContexts context)
	//	//{
	//	//	return EditingControlFormattedValue;
	//	//}

	//	//// Implements the 
	//	//// IDataGridViewEditingControl.ApplyCellStyleToEditingControl method.
	//	//public void ApplyCellStyleToEditingControl(
	//	//	DataGridViewCellStyle dataGridViewCellStyle)
	//	//{
	//	//	this.Font = dataGridViewCellStyle.Font;
	//	//}

	//	//// Implements the IDataGridViewEditingControl.EditingControlRowIndex 
	//	//// property.
	//	//public int EditingControlRowIndex
	//	//{
	//	//	get
	//	//	{
	//	//		return rowIndex;
	//	//	}
	//	//	set
	//	//	{
	//	//		rowIndex = value;
	//	//	}
	//	//}

	//	//// Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
	//	//// method.
	//	//public bool EditingControlWantsInputKey(
	//	//	Keys key, bool dataGridViewWantsInputKey)
	//	//{
	//	//	// Let the DateTimePicker handle the keys listed.
	//	//	switch (key & Keys.KeyCode)
	//	//	{
	//	//		case Keys.Left:
	//	//		case Keys.Up:
	//	//		case Keys.Down:
	//	//		case Keys.Right:
	//	//		case Keys.Home:
	//	//		case Keys.End:
	//	//		case Keys.PageDown:
	//	//		case Keys.PageUp:
	//	//			return true;
	//	//		default:
	//	//			return !dataGridViewWantsInputKey;
	//	//	}
	//	//}

	//	//// Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
	//	//// method.
	//	//public void PrepareEditingControlForEdit(bool selectAll)
	//	//{
	//	//	// No preparation needs to be done.
	//	//}

	//	//// Implements the IDataGridViewEditingControl
	//	//// .RepositionEditingControlOnValueChange property.
	//	//public bool RepositionEditingControlOnValueChange
	//	//{
	//	//	get
	//	//	{
	//	//		return false;
	//	//	}
	//	//}

	//	// Implements the IDataGridViewEditingControl
	//	// .EditingControlDataGridView property.
	//	public DataGridView EditingControlDataGridView
	//	{
	//		get
	//		{
	//			return dataGridView;
	//		}
	//		set
	//		{
	//			dataGridView = value;
	//		}
	//	}

	//	// Implements the IDataGridViewEditingControl
	//	// .EditingControlValueChanged property.
	//	public bool EditingControlValueChanged
	//	{
	//		get
	//		{
	//			return valueChanged;
	//		}
	//		set
	//		{
	//			valueChanged = value;
	//		}
	//	}

	//	// Implements the IDataGridViewEditingControl
	//	// .EditingPanelCursor property.
	//	public Cursor EditingPanelCursor
	//	{
	//		get
	//		{
	//			return base.Cursor;
	//		}
	//	}

	//	protected override void OnValueChanged(EventArgs eventargs)
	//	{
	//		valueChanged = true;
	//		this.EditingControlDataGridView.CurrentCell.Value = this.Value;
	//		this.EditingControlDataGridView.NotifyCurrentCellDirty(true);
	//		base.OnValueChanged(eventargs);
	//	}
	//}

	public class HDataGridView : ExDataGridView
	{
		public HDataGridView()
		{
			this.ApplyDefaultStyle();
			this.ReadOnly = true;
			this.EnableHeadersVisualStyles = false;

			this.Sorted += new EventHandler(ExDataGridView_Sorted);
			this.DataSourceChanged += new EventHandler(ExDataGridView_DataSourceChanged);
			//this.CellDoubleClick += HDataGridView_CellDoubleClick;
			//this.KeyUp += HDataGridView_KeyUp;
		}

		//private void HDataGridView_KeyUp(object sender, KeyEventArgs e)
		//{
		//	var page = this.Parent as HPage;
		//	if (page == null)
		//	{
		//		// drilldown to next parent
		//		page = this.Parent.Parent as HPage;
		//		if (page == null)
		//		{
		//			return;
		//		}
		//	}
		//	if (e.KeyCode == Keys.Delete)
		//	{
		//		page.Remove();
		//	}
		//	if (e.KeyCode == Keys.Space)
		//	{
		//		page.Edit();
		//	}
		//}

		//private void HDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		//{
		//	if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
		//	{
		//		var page = this.Parent as HPage;
		//		if (page == null)
		//		{
		//			// drill down to next parent
		//			page = this.Parent.Parent as HPage;
		//			if (page == null)
		//			{
		//				return;
		//			}
		//		}
		//		page.View();
		//	}
		//}

		void ExDataGridView_DataSourceChanged(object sender, EventArgs e)
		{
			this.MarkDefaultColor();
		}

		void ExDataGridView_Sorted(object sender, EventArgs e)
		{
			this.MarkDefaultColor();
		}
	}

	public static class HUIHelper
    {
        //public static string ToShortDate(this DateTime d)
        //{
        //    return d.ToString("dd-MM-yyyy");
        //}
        //public static string ToLongDate(this DateTime d)
        //{
        //    return d.ToString("dd-MM-yyyy hh:mm:ss tt");
        //}
        public static void FormatSortDate(this DateTimePicker dtp)
        {
            dtp.CustomFormat = "dd-MM-yyyy";
            dtp.Format = DateTimePickerFormat.Custom;
        }

        public static void FormatCountryCurrency(this BandedGridColumn col)
		{
			//if (string.IsNullOrEmpty(CountryCurrencyFormat))
			//{
			//	CountryCurrencyFormat = CurrencyLogic.Instance.Find(Current.Company.Setting().CountryCurrencyId).Format;
			//}
			col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			col.DisplayFormat.FormatString = "#,##0";

			col.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			col.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
		}
	}

	public static class DataGridViewExtension
	{
		public static void ApplyDefaultStyle(this DataGridView THIS)
		{
			THIS.RowHeadersVisible =
				 THIS.AllowUserToAddRows =
				 THIS.AllowUserToDeleteRows =
				 THIS.AllowUserToResizeRows = false;
			THIS.MultiSelect = false;
			THIS.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			THIS.CellBorderStyle = DataGridViewCellBorderStyle.None;
			THIS.AlternatingRowsDefaultCellStyle = new DataGridViewCellStyle() { BackColor = Color.WhiteSmoke };
			THIS.DefaultCellStyle.SelectionBackColor = Color.FromArgb(173, 205, 239);
			THIS.DefaultCellStyle.SelectionForeColor = Color.Black;
			THIS.RowTemplate.Height = 28;
			THIS.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			THIS.ColumnHeadersHeight = 28;
			THIS.BackgroundColor = Color.White;
			THIS.BorderStyle = BorderStyle.None;
		}
		public static void ApplyDropdownFocus(this DataGridView dgv, DataGridViewExComboBoxColumn dropDownColumn, DataGridViewColumn focusColumn)
		{
			dgv.CellValueChanged += (object sender, DataGridViewCellEventArgs e) =>
			{
				// To focus next cell after select ExDropDown
				if (e.RowIndex >= 0 && dgv.Columns[e.ColumnIndex].Name == dropDownColumn.Name)
				{
					if (dgv[e.ColumnIndex, e.RowIndex].Value != null)
					{
						//dgv.Parent.Focus();
						dgv.Focus();
						dgv.CurrentCell = dgv[focusColumn.Index, e.RowIndex];
						dgv.CurrentCell.Selected = true;
					}
				}
			};
		} 
	}
	public class HButton : Button
	{
		public HButton()
		{
			Cursor = Cursors.Hand;
			FlatStyle = FlatStyle.Flat;
			FlatAppearance.BorderSize = 0;
			ForeColor = Color.White;
			BackColor = Color.FromArgb(70, 92, 115);
		}
	}

	public class HLabel : Label
	{
		public bool Required { get; set; }

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			if (Required)
			{


				//float dim = 5;
				////rgba(135, 90, 123, 1)
				//var drawBrush = new SolidBrush(Color.FromArgb(135,90,123));
				//var p = new PointF[]
				//{
				//    new PointF(Width-dim,0),
				//    new PointF(Width,0),
				//    new PointF(Width,dim)
				//};

				//e.Graphics.FillPolygon(drawBrush, p);


				// Create string to draw.
				var drawString = "*";

				// Create font and brush.
				var drawFont = new Font("Times New Roman", 9.75F);
				var drawBrush = new SolidBrush(Color.Red);

				float x = Width - 8;
				float y = -3;

				// Set format of string.
				var drawFormat = new StringFormat
				{
					FormatFlags = StringFormatFlags.DisplayFormatControl
				};
				e.Graphics.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);

			}
		}

		public override Size GetPreferredSize(Size proposedSize)
		{
			var size = base.GetPreferredSize(proposedSize);
			size.Width = size.Width + 3;
			return size;
		}

	}

	public partial class HLine : UserControl
	{
		public HLine()
		{
			
		}
	}
}
