﻿using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Logic;
using SoftTech;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EPower.Base.Helper
{
    public class FtpHelper
    {
        public static string FtpAddress = "";
        public static string UserName = "";
        public static string Password = "";
        public static string NewFile = "";
        public static bool LicenseIsExists = true;
        public static bool YearIsExists = true;
        public static bool MonthIsExists = true;
        public static bool FileIsExists = true;
        public static bool PenningIsExists = true;
        public static WebRequest request = null;
        public static WebResponse response = null;
        public static bool IsSuccess = false;
        
        public static string ReportPowerPurchseLicneseFee { get; set; } = "ReportPowerPurchseLicneseFee";
        public static string ReportQuarter { get; set; } = "ReportQuarter";
        public static string ReportAnnual { get; set; } = "ReportAnnual";
        public static string ReportSubsidy { get; set; } = "ReportSubsidy";
        public static string ReportCustomerUsageStatistic { get; set; } = "ReportCustomerUsageStatistic";

        public static void UploadToFtp(SendFtp ftp)
        {
            try
            {
                //if (!Directory.Exists(ftp.TMP))
                //{
                //    CreateDirectory(Path.Combine(ftp.TMP));
                //}

                //var uTILITY = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == (int)Utility.FTP_REPORT_CONNECTION_STRING).UTILITY_VALUE;
                //string DeValue = string.Format(Base64Decode(uTILITY)).Replace(" ", "");
                //string _FtpAddress = "FtpAddress=";
                //string _UserName = "UserName=";
                //string _Password = "Password=";
                //foreach (var item in DeValue.Split(';'))
                //{
                //    if (String.IsNullOrEmpty(item))
                //    {
                //        break;
                //    }

                //    if (item.Contains(_FtpAddress))
                //    {
                //        FtpAddress = item.Replace(_FtpAddress, "");
                //        continue;
                //    }

                //    if (item.Contains(_UserName))
                //    {
                //        UserName = item.Replace(_UserName, "");
                //        continue;
                //    }

                //    if (item.Contains(_Password))
                //    {
                //        Password = item.Replace(_Password, "");
                //    }
                //}

                //string PenningDirectory = FtpAddress + "/" + "Penning";
                //string YearDirectory = PenningDirectory + "/" + ftp.Year;
                //string MonthDirectory = YearDirectory + "/" + ftp.Month;
                //string LicenseDirectory = MonthDirectory + "/" + ftp.License;

                //Penning(PenningDirectory);
                //if (!PenningIsExists)
                //{
                //    CreateDirectory(PenningDirectory);
                //}

                //LicenseYears(YearDirectory);
                //if (!YearIsExists)
                //{
                //    CreateDirectory(YearDirectory);
                //}

                //LicenseMonth(MonthDirectory);
                //if (!MonthIsExists)
                //{
                //    CreateDirectory(MonthDirectory);
                //}

                //LicenseNumber(LicenseDirectory);
                //if (!LicenseIsExists)
                //{
                //    CreateDirectory(LicenseDirectory);
                //}

                //string fileName = ftp.ReportName +"-"+ ftp.License + "-" + ftp.Year + "-" + ftp.Month + ".zip";
                //FilesIsExists(LicenseDirectory + "/" + fileName);
                
                //if (!FileIsExists)
                //{
                //    UploadZipFile(LicenseDirectory, ftp.Path);
                //}
                //else
                //{
                //    DeleteZipFile(LicenseDirectory, fileName);
                //    UploadZipFile(LicenseDirectory, ftp.Path);
                //}
                //File.Delete(ftp.Path);
                //IsSuccess = true;
            }
            catch (Exception ex)
            {
                IsSuccess = false;
            }
        }
        public static bool Penning(string directoryPath)
        {
            PenningIsExists = true;
            try
            {
                ListDirectory(directoryPath);
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    FtpWebResponse response = (FtpWebResponse)ex.Response;
                    if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    {
                        return PenningIsExists = false;
                        // Directory not found.  
                    }
                }
            }
            return PenningIsExists;
        }
        public static bool FilesIsExists(string directoryPath)
        {
            FileIsExists = true;
            try
            {
                ListDirectory(directoryPath);
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    FtpWebResponse response = (FtpWebResponse)ex.Response;
                    if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    {
                        return FileIsExists = false;
                        // Directory not found.  
                    }
                }
            }
            return FileIsExists;
        }
        public static bool LicenseNumber(string directoryPath)
        {
            LicenseIsExists = true;
            try
            {
                ListDirectory(directoryPath);
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    FtpWebResponse response = (FtpWebResponse)ex.Response;
                    if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    {
                        return LicenseIsExists = false;
                        // Directory not found.  
                    }
                }
            }
            return LicenseIsExists;
        }
        public static bool LicenseYears(string directoryPath)
        {
            YearIsExists = true;
            try
            {
                ListDirectory(directoryPath);
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    FtpWebResponse response = (FtpWebResponse)ex.Response;
                    if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    {
                        return YearIsExists = false;
                        // Directory not found.  
                    }
                }
            }
            return YearIsExists;
        }
        public static bool LicenseMonth(string directoryPath)
        {
            MonthIsExists = true;
            try
            {
                ListDirectory(directoryPath);
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    FtpWebResponse response = (FtpWebResponse)ex.Response;
                    if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    {
                        return MonthIsExists = false;
                        // Directory not found.  
                    }
                }
            }
            return MonthIsExists;
        }
        public static void ListDirectory(string directoryPath)
        {
            request = (FtpWebRequest)WebRequest.Create(directoryPath);
            request.Credentials = new NetworkCredential(UserName, Password);
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.GetResponse();
        }
        public static void CreateDirectory(string directoryPath)
        {
            request = WebRequest.Create(directoryPath);
            request.Credentials = new NetworkCredential(UserName, Password);
            request.Method = WebRequestMethods.Ftp.MakeDirectory;
            response = request.GetResponse();
        }
        public static void UploadZipFile(string directoryPath, string FilePath)
        {
            request = (FtpWebRequest)WebRequest.Create(directoryPath + "/" + Path.GetFileName(FilePath));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(UserName, Password);

            Stream ftpStream = request.GetRequestStream();
            FileStream file = File.OpenRead(FilePath);

            int length = 100000;
            byte[] bytes = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(bytes, 0, length);
                ftpStream.Write(bytes, 0, bytesRead);
            } while (bytesRead != 0);
            file.Close();
            ftpStream.Close();
        }
        public static void DeleteZipFile(string directoryPath, string FilePath)
        {
            request = (FtpWebRequest)WebRequest.Create(directoryPath + "/" + Path.GetFileName(FilePath));
            request.Method = WebRequestMethods.Ftp.DeleteFile;
            request.Credentials = new NetworkCredential(UserName, Password);
            response = (FtpWebResponse)request.GetResponse();
        }
    }
    public class SendFtp
    {
        public string Year { get; set; }
        public string Month { get; set; }
        public string Path { get; set; }
        public string License { get; set; }
        public string TMP { get; set; }
        public string ReportName { get; set; }
        public int ReportId { get; set; }
    }

    public enum ReportID
    {
        ReportPowerPurchseLicneseFee = 1,
        ReportQuarter = 2,
        ReportAnnual = 3,
        ReportSubsidy = 4,
        ReportCustomerUsageStatistic = 5
    }
}