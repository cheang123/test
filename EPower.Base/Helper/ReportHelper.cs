﻿using CrystalDecisions.CrystalReports.Engine;
using SoftTech.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EPower.Base.Logic
{
    public class ReportHelper
    {
        protected static Lazy<ReportHelper> _instance = new Lazy<ReportHelper>(() =>
        {
            try
            {
                return new ReportHelper();
            }
            catch (Exception exp)
            {
                if (exp.InnerException != null)
                {
                    throw exp.InnerException;
                }
                throw exp;
            }

        }, System.Threading.LazyThreadSafetyMode.PublicationOnly);
        public ReportHelper()
        {
            RegisterPath(@"Reports");
        }
        public static ReportHelper Instance => _instance.Value;
        public enum Priority
        {
            Hight,
            Low
        }
        public List<string> ReportPath { get; private set; } = new List<string>();
        public void RegisterPath(string path, Priority priority = Priority.Low)
        {
            if (ReportPath.Any(x => x == path))
            {
                return;
            }

            if (priority == Priority.Hight)
            {
                ReportPath.Insert(0, path);
            }
            else
            {
                ReportPath.Add(path);
            }
        }
        public ReportDocument Load(string name, Dictionary<string, IEnumerable> data, Dictionary<string, Dictionary<string, IEnumerable>> subData = null, Dictionary<string, object> parameters = null)
        {
            ReportDocument report = new ReportDocument();
            name = name.EndsWith(".rpt") ? name : name + ".rpt";
            var path = CrystalReportHelper.GetDefaultReportPath();
            var file = Path.Combine(path, name);
            var customizePath = Path.Combine(path, @"Customized");
            file = Path.Combine(customizePath, name);
            if (!File.Exists(file))
            {
                file = Path.Combine(path, name);  
            }
            if (!File.Exists(file))
            {
                return null;
            };

            report.Load(file);
            if (data != null )
            {
                foreach (var table in data)
                {
                    report.Database.Tables[table.Key].SetDataSource(table.Value);
                }
            }
            if (subData != null)
            {
                
                foreach (var sub in subData)
                {
                    foreach (var table in sub.Value)
                    {
                        report.Subreports[sub.Key].Database.Tables[table.Key].SetDataSource(table.Value);
                    }
                }
            }
            /*
             * Default Parameters.
             */
            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    report.SetParameterValue(item.Key, item.Value);
                }
            }
            report.SummaryInfo.ReportTitle = name;
            return report;
        }
        public void SelectFile(string path)
        {
            System.Diagnostics.Process.Start("explorer.exe", $"/select, \"{path}\"");
        }

        public T TryCast<T>(object source)
        {
            try
            {
                return (T)source;
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}
