﻿using EPower.Base.Models;
using HB01.Domain.Enums;
using HB01.Domain.Models.Transactions;
using HB01.Helpers;
using HB01.Logics;
using SoftTech;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace EPower.Base.Logic
{
    internal class PushPrepaymentLogic : Integration<Deposit>
    {
        public static PushPrepaymentLogic Instance { get; } = new PushPrepaymentLogic();
        public override List<Deposit> AvailableRecords(DateTime startDate, DateTime endDate)
        {
            var invs = from i in DBDataContext.Db.TBL_INVOICEs
                       join d in DBDataContext.Db.TBL_INVOICE_DETAILs on i.INVOICE_ID equals d.INVOICE_ID
                       group d by d.INVOICE_ID into g
                       select new
                       {
                           INVOICE_ID = g.Key,
                           EXCHANGE_RATE = g.Max(x => x.EXCHANGE_RATE),
                           INVOICE_ITEM_ID = g.Max(x => x.INVOICE_ITEM_ID)
                       };

            var excludeInv = new List<int> { (int)InvoiceItem.Reverse, (int)InvoiceItem.Adjustment, (int)InvoiceItem.Power };
            var pAcc = AccountingLogic.Instance.AccountConfigId(AccountConfig.PREPAYMENT_ACCOUNT).FirstOrDefault();
            //var cAcc = AccountingLogic.Instance.AccountConfigId(AccountConfig.PREPAYMENT_ACCOUNT_CASH).FirstOrDefault();
            if (pAcc == 0)
            {
                return null;
            }

            var postedItems = from j in DBDataContext.Db.TBL_JOURNAL_ENTRies
                              join ja in DBDataContext.Db.TBL_JOURNAL_ENTRY_ACCOUNTs on j.JOURNAL_ENTRY_ID equals ja.JOURNAL_ENTRY_ID
                              where j.IS_ACTIVE && j.REFERENCE_TYPE_ID == (int)ReferenceType.Prepayment
                              select new
                              {
                                  j.JOURNAL_ENTRY_ID,
                                  ja.REFERENCE_ID
                              };

            //var payBill = from cp in DBDataContext.Db.TBL_CUS_PREPAYMENTs
            //              join p in DBDataContext.Db.TBL_PAYMENTs on cp.

            var receive = from p in DBDataContext.Db.TBL_CUS_PREPAYMENTs
                          join c in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals c.CURRENCY_ID
                          join ca in DBDataContext.Db.TBL_ACCOUNT_CHARTs on p.PAYMENT_ACCOUNT_ID equals ca.ACCOUNT_ID
                          join j in postedItems on p.CUS_PREPAYMENT_ID equals j.REFERENCE_ID into l
                          from je in l.DefaultIfEmpty()
                          where p.CREATE_ON >= startDate && p.CREATE_ON <= endDate
                          && p.USER_CASH_DRAWER_ID != 0
                          && ca.EXT_ACCOUNT_ID != 0
                          && je.JOURNAL_ENTRY_ID == null
                          select new Deposit
                          {
                              DepositId = p.CUS_PREPAYMENT_ID,
                              DepositAcc = pAcc,
                              CashAcc = ca.EXT_ACCOUNT_ID,
                              Amount = p.AMOUNT,
                              CurrencyId = c.EXTERNAL_CURRENCY_ID,
                              DepositDate = p.CREATE_ON,
                              PaymentExchangeRate = p.EXCHANGE_RATE,
                              InvoiceExchangeRate = p.EXCHANGE_RATE
                          };

            var bills = from cp in DBDataContext.Db.TBL_CUS_PREPAYMENTs
                        join c in DBDataContext.Db.TLKP_CURRENCies on cp.CURRENCY_ID equals c.CURRENCY_ID
                        join pd in DBDataContext.Db.TBL_PAYMENT_DETAILs on cp.PAYMENT_ID equals pd.PAYMENT_ID
                        join i in DBDataContext.Db.TBL_INVOICEs on pd.INVOICE_ID equals i.INVOICE_ID
                        join t in invs on i.INVOICE_ID equals t.INVOICE_ID
                        join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on i.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                        join j in postedItems on cp.CUS_PREPAYMENT_ID equals j.REFERENCE_ID into l
                        from je in l.DefaultIfEmpty()
                        where cp.CREATE_ON >= startDate && cp.CREATE_ON <= endDate
                            && i.IS_SERVICE_BILL == false
                            && cp.USER_CASH_DRAWER_ID == 0
                            && c.EXTERNAL_CURRENCY_ID != 0
                            && je.JOURNAL_ENTRY_ID == null
                        select new Deposit
                        {
                            DepositId = cp.CUS_PREPAYMENT_ID,
                            DepositAcc = pAcc,
                            CashAcc = i.CURRENCY_ID == (int)EPower.Currency.KHR ? ct.AR_KHR : i.CURRENCY_ID == (int)EPower.Currency.USD
                                              ? ct.AR_USD : i.CURRENCY_ID == (int)EPower.Currency.THB ? ct.AR_THB : ct.AR_VND,
                            Amount = cp.AMOUNT < 0 ? pd.PAY_AMOUNT * -1 : pd.PAY_AMOUNT, // seperate for each invoice
                            CurrencyId = c.EXTERNAL_CURRENCY_ID,
                            DepositDate = cp.CREATE_ON,
                            PaymentExchangeRate = cp.EXCHANGE_RATE,
                            InvoiceExchangeRate = t.EXCHANGE_RATE
                        };

            var services = from cp in DBDataContext.Db.TBL_CUS_PREPAYMENTs
                           join c in DBDataContext.Db.TLKP_CURRENCies on cp.CURRENCY_ID equals c.CURRENCY_ID
                           join pd in DBDataContext.Db.TBL_PAYMENT_DETAILs on cp.PAYMENT_ID equals pd.PAYMENT_ID
                           join i in DBDataContext.Db.TBL_INVOICEs on pd.INVOICE_ID equals i.INVOICE_ID
                           join id in invs on i.INVOICE_ID equals id.INVOICE_ID
                           //join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on i.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                           join it in DBDataContext.Db.TBL_INVOICE_ITEMs on id.INVOICE_ITEM_ID equals it.INVOICE_ITEM_ID
                           join j in postedItems on cp.CUS_PREPAYMENT_ID equals j.REFERENCE_ID into l
                           from je in l.DefaultIfEmpty()
                           where cp.CREATE_ON >= startDate && cp.CREATE_ON <= endDate
                               && i.IS_SERVICE_BILL == true
                               && cp.USER_CASH_DRAWER_ID == 0
                               && c.EXTERNAL_CURRENCY_ID != 0
                               && !excludeInv.Contains(id.INVOICE_ITEM_ID)
                               && je.JOURNAL_ENTRY_ID == null
                           select new Deposit
                           {
                               DepositId = cp.CUS_PREPAYMENT_ID,
                               DepositAcc = pAcc,
                               CashAcc = i.CURRENCY_ID == (int)EPower.Currency.KHR ? it.AR_KHR : i.CURRENCY_ID == (int)EPower.Currency.USD
                                                 ? it.AR_USD : i.CURRENCY_ID == (int)EPower.Currency.THB ? it.AR_THB : it.AR_VND,
                               Amount = cp.AMOUNT < 0 ? pd.PAY_AMOUNT * -1 : pd.PAY_AMOUNT, // seperate for each invoice
                               CurrencyId = c.EXTERNAL_CURRENCY_ID,
                               DepositDate = cp.CREATE_ON,
                               PaymentExchangeRate = cp.EXCHANGE_RATE,
                               InvoiceExchangeRate = id.EXCHANGE_RATE
                           };

            var result = bills.Concat(services);

            var paidOnInv = from s in result
                            join a in DBDataContext.Db.TBL_ACCOUNT_CHARTs on s.CashAcc equals a.ACCOUNT_ID
                            where a.EXT_ACCOUNT_ID != 0
                            select new Deposit
                            {
                                DepositId = s.DepositId,
                                DepositAcc = s.DepositAcc,
                                CashAcc = a.EXT_ACCOUNT_ID,
                                Amount = s.Amount,
                                CurrencyId = s.CurrencyId,
                                DepositDate = s.DepositDate,
                                PaymentExchangeRate = s.PaymentExchangeRate,
                                InvoiceExchangeRate = s.InvoiceExchangeRate
                            };
            var results = receive.ToList();
            results.AddRange(paidOnInv.ToList());
            return results;
        }

        public override List<TBL_JOURNAL_ENTRY_ACCOUNT> EntryItems(List<Deposit> list, int EntryId)
        {
            var items = (from l in list
                         group l by new { l.DepositId, l.CurrencyId, l.CashAcc, l.DepositAcc } into g
                         select new
                         {
                             CASH = new TBL_JOURNAL_ENTRY_ACCOUNT
                             {
                                 JOURNAL_ENTRY_ID = EntryId,
                                 ACCOUNT_ID = g.Key.CashAcc,
                                 AMOUNT = g.Sum(x => x.Amount),
                                 REFERENCE_ID = g.Key.DepositId
                             },
                             DEPOSIT = new TBL_JOURNAL_ENTRY_ACCOUNT
                             {
                                 JOURNAL_ENTRY_ID = EntryId,
                                 ACCOUNT_ID = g.Key.DepositAcc,
                                 AMOUNT = g.Sum(x => x.Amount),
                                 REFERENCE_ID = g.Key.DepositId
                             }
                         }).ToList();
            return items.Select(x => x.DEPOSIT).Concat(items.Select(x => x.CASH)).ToList();
        }

        public override List<JournalEntryLine> EntryLines(List<Deposit> list)
        {
            var GainAccId = Current.Company.CompanySetting.GainExchangeRateAccountId;
            var LossAccId = Current.Company.CompanySetting.LossExchangeRateAccountId;
            var GainAcc = AccountLogic.Instance.Find(GainAccId);
            var LossAcc = AccountLogic.Instance.Find(LossAccId);


            var signGain = GainAcc.AccountType.Nature == AccountNatures.Expense ? -1 : 1;
            var signLoss = LossAcc.AccountType.Nature == AccountNatures.Expense ? -1 : 1;

            var line = (from l in list
                        group l by new { l.CurrencyId, l.CashAcc, l.DepositAcc, l.InvoiceExchangeRate, l.PaymentExchangeRate } into g
                        select new
                        {
                            DEPOSIT = new JournalEntryLine
                            {
                                AccountId = g.Key.DepositAcc,
                                Title = $"",
                                NetAmount = g.Sum(x => x.Amount) * g.Key.PaymentExchangeRate,
                                CurrencyId = g.Key.CurrencyId,
                                Active = true,
                                CurrencyAmount = g.Sum(x => x.Amount),
                                ExchangeRate = g.Key.PaymentExchangeRate,
                                ExchangeRateId = -1
                            },
                            CASH = new JournalEntryLine
                            {
                                AccountId = g.Key.CashAcc,
                                Title = $"",
                                NetAmount = g.Sum(x => x.Amount) * g.Key.InvoiceExchangeRate,
                                CurrencyId = g.Key.CurrencyId,
                                Active = true,
                                CurrencyAmount = g.Sum(x => x.Amount),
                                ExchangeRate = g.Key.InvoiceExchangeRate,
                                ExchangeRateId = -1
                            },
                            GainLoss = new JournalEntryLine
                            {
                                AccountId = 0,
                                Title = $"GainLossExchange",
                                NetAmount = (g.Sum(x => x.Amount) * g.Key.InvoiceExchangeRate) - (g.Sum(x => x.Amount) * g.Key.PaymentExchangeRate),
                                CurrencyId = g.Key.CurrencyId,
                                Active = true,
                                CurrencyAmount = 0,
                                ExchangeRate = g.Key.InvoiceExchangeRate,
                                ExchangeRateId = -1
                            }
                        }).ToList();

            var gainLoss = line.Select(x => x.GainLoss).ToList();
            //gainLoss.ForEach(x => { x.AccountId = x.NetAmount < 0 ? LossAccId : GainAccId; x.NetAmount = x.NetAmount < 0 ? signLoss * x.NetAmount : signGain * x.NetAmount; });
            foreach (var item in gainLoss)
            {
                if (item.NetAmount < 0)
                {
                    item.AccountId = LossAccId;
                    item.NetAmount = signLoss * item.NetAmount;
                }
                else
                {
                    item.AccountId = GainAccId;
                    item.NetAmount = signGain * item.NetAmount;
                }
            }

            var result = new List<JournalEntryLine>();
            result.AddRange(line.Select(x => x.DEPOSIT));
            result.AddRange(line.Select(x => x.CASH));
            result.AddRange(gainLoss);
            return result;
        }

        public override void PushJournal(DateTime startDate, DateTime endDate)
        {
            int k = 1;
            var currDate = DBDataContext.Db.GetSystemDate();
            var tran = (from t in AvailableRecords(startDate, endDate)
                        group t by new { DepositDate = t.DepositDate.Date, t.CurrencyId } into g
                        orderby g.Key.DepositDate
                        select new Transaction
                        {
                            Date = g.Key.DepositDate,
                            CurrencyId = g.Key.CurrencyId,
                            Transactions = g.ToList()
                        }).ToList();

            var journalId = AccountingLogic.Instance.Journal(AccountingConfig.JOURNAL_PREPAYMENT).Id;

            foreach (var t in tran)
            {
                var tranOption = new TransactionOptions()
                {
                    IsolationLevel = IsolationLevel.ReadUncommitted,
                    Timeout = TimeSpan.MaxValue
                };

                TBL_JOURNAL_ENTRY afterUpdateJournal = new TBL_JOURNAL_ENTRY();
                TBL_JOURNAL_ENTRY beforeUpdateJournal = new TBL_JOURNAL_ENTRY();

                try
                {
                    using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, tranOption))
                    {
                        if (t.Transactions.Count > 0)
                        {
                            Runner.Instance.Text = $"ចុះបញ្ជីប្រាក់តម្កល់អតិថិជន {k++}/{tran.Count}";

                            //Sumit E-Power
                            var eJournal = new TBL_JOURNAL_ENTRY
                            {
                                REF_NO = "",
                                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                                CREATE_ON = currDate,
                                ENTRY_DATE = t.Date,
                                IS_ACTIVE = true,
                                CURRENCY_ID = t.CurrencyId,
                                REFERENCE_TYPE_ID = (int)ReferenceType.Prepayment,
                                HB01_JOURNAL_ID = 0,
                                NOTE = ""
                            };
                            DBDataContext.Db.TBL_JOURNAL_ENTRies.InsertOnSubmit(eJournal);
                            DBDataContext.Db.SubmitChanges();
                            var line = EntryItems(t.Transactions, eJournal.JOURNAL_ENTRY_ID);
                            DBDataContext.Db.BulkCopy(line._ToDataTable(), $"HB01.{nameof(TBL_JOURNAL_ENTRY_ACCOUNT)}");
                            DBDataContext.Db.SubmitChanges();
                            eJournal._CopyTo(afterUpdateJournal);
                            eJournal._CopyTo(beforeUpdateJournal);

                            //Submit Pointer
                            var journal = new JournalEntry()
                            {
                                CompanyId = Current.CompanyId,
                                Active = true,
                                CurrencyId = t.CurrencyId,
                                EntryDate = t.Date,
                                RefNo = "From E-Power",
                                JournalId = journalId,
                                SourceRefId = "CusPrepaymentIds." + string.Join(",", t.Transactions.Select(x => x.DepositId).ToList()),
                                Posted = true,
                                Source = PostingSources.External,
                                Lines = EntryLines(t.Transactions),
                                Amount = t.Transactions.Sum(x => x.Amount)
                            };
                            var j = JournalEntryLogic.Instance.PostExternal(journal);
                            afterUpdateJournal.HB01_JOURNAL_ID = j.Id;
                            afterUpdateJournal.REF_NO = j.EntryNo;
                        }
                        transaction.Complete();
                    }
                    DBDataContext.Db.Update(beforeUpdateJournal, afterUpdateJournal);
                    DBDataContext.Db.SubmitChanges();
                }
                catch (Exception e)
                {
                    //MsgBox.ShowInformation(e.Message);
                }
            }
        }
    }
}
