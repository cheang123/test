﻿using CrystalDecisions.Shared;
using CrystalDecisions.Windows.Forms;

namespace EPower.Base.Logic
{
    public static class ViewCrystal
    {
        public static void DefaultView(this CrystalReportViewer viewer)
        {
            int exportFormatFlags = (int)(ViewerExportFormats.PdfFormat | ViewerExportFormats.ExcelFormat | ViewerExportFormats.ExcelRecordFormat | ViewerExportFormats.XmlFormat | ViewerExportFormats.WordFormat | ViewerExportFormats.XLSXFormat | ViewerExportFormats.EditableRtfFormat | ViewerExportFormats.CsvFormat);
            viewer.AllowedExportFormats = exportFormatFlags;
            viewer.ShowRefreshButton = false;
        }
    }
}
