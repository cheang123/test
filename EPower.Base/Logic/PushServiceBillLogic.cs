﻿using EPower.Base.Models;
using HB01.Domain.Enums;
using HB01.Domain.Models.Transactions;
using HB01.Logics;
using SoftTech;
using SoftTech.Component;
using SoftTech.Security.Logic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EPower.Base.Logic
{
    internal class PushServiceBillLogic : Integration<InvoiceService>
    {
        public static PushServiceBillLogic Instance { get; } = new PushServiceBillLogic();
        public override List<InvoiceService> AvailableRecords(DateTime startDate, DateTime endDate)
        {
            var excludeInv = new List<int> { (int)InvoiceItem.Power, (int)InvoiceItem.Adjustment, (int)InvoiceItem.Reverse };


            var serviceBill = from i in DBDataContext.Db.TBL_INVOICEs
                              join id in DBDataContext.Db.TBL_INVOICE_DETAILs on i.INVOICE_ID equals id.INVOICE_ID
                              join c in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals c.CURRENCY_ID
                              join it in DBDataContext.Db.TBL_INVOICE_ITEMs on id.INVOICE_ITEM_ID equals it.INVOICE_ITEM_ID
                              join ar in DBDataContext.Db.TBL_ACCOUNT_CHARTs on it.AR_ACCOUNT equals ar.ACCOUNT_ID
                              join ic in DBDataContext.Db.TBL_ACCOUNT_CHARTs on it.INCOME_ACCOUNT_ID equals ic.ACCOUNT_ID
                              where id.TRAN_DATE >= startDate && id.TRAN_DATE <= endDate
                              && !excludeInv.Contains(id.INVOICE_ITEM_ID)
                              && i.IS_SERVICE_BILL
                              && ar.EXT_ACCOUNT_ID != 0
                              && ic.EXT_ACCOUNT_ID != 0
                              && c.EXTERNAL_CURRENCY_ID != 0
                              && DBDataContext.Db.TBL_JOURNAL_ENTRies
                                   .Join(DBDataContext.Db.TBL_JOURNAL_ENTRY_ACCOUNTs,
                                   e => e.JOURNAL_ENTRY_ID, a => a.JOURNAL_ENTRY_ID,
                                   (e, a) => new { e.REFERENCE_TYPE_ID, a.REFERENCE_ID, e.IS_ACTIVE })
                               .Any(x => x.IS_ACTIVE
                                   && (x.REFERENCE_TYPE_ID == (int)ReferenceType.InvoiceService)
                                   && x.REFERENCE_ID == id.INVOICE_DETAIL_ID) == false
                              select new InvoiceService
                              {
                                  INVOICE_DETAIL_ID = id.INVOICE_DETAIL_ID,
                                  INVOICE_ITEM_ID = it.INVOICE_ITEM_ID,
                                  INVOICE_ITEM_NAME = it.INVOICE_ITEM_NAME,
                                  AR_ACCOUNT_ID = ar.EXT_ACCOUNT_ID,
                                  INCOME_ACCOUNT_ID = ic.EXT_ACCOUNT_ID,
                                  TOTAL_AMOUNT = id.AMOUNT,
                                  CURRENCY_ID = c.EXTERNAL_CURRENCY_ID,
                                  EntryDate = id.TRAN_DATE
                              };
            return serviceBill.ToList();
        }

        public override List<TBL_JOURNAL_ENTRY_ACCOUNT> EntryItems(List<InvoiceService> list, int EntryId)
        {
            var items = from l in list
                        select new
                        {
                            INCOME = new TBL_JOURNAL_ENTRY_ACCOUNT
                            {
                                ACCOUNT_ID = l.INCOME_ACCOUNT_ID,
                                AMOUNT = l.TOTAL_AMOUNT,
                                REFERENCE_ID = l.INVOICE_DETAIL_ID,
                                JOURNAL_ENTRY_ID = EntryId
                            },
                            AR = new TBL_JOURNAL_ENTRY_ACCOUNT
                            {
                                ACCOUNT_ID = l.AR_ACCOUNT_ID,
                                AMOUNT = l.TOTAL_AMOUNT,
                                REFERENCE_ID = l.INVOICE_DETAIL_ID,
                                JOURNAL_ENTRY_ID = EntryId
                            }
                        };
            return items.Select(x => x.INCOME).Union(items.Select(x => x.AR)).ToList();
        }

        public override List<JournalEntryLine> EntryLines(List<InvoiceService> list)
        {
            var lines = (from l in list
                         group new { l }
                         by new
                         {
                             l.INVOICE_ITEM_ID,
                             l.INVOICE_ITEM_NAME,
                             l.AR_ACCOUNT_ID,
                             l.INCOME_ACCOUNT_ID,
                             l.CURRENCY_ID
                         } into g
                         select new
                         {
                             INCOME = new JournalEntryLine
                             {
                                 AccountId = g.Key.INCOME_ACCOUNT_ID,
                                 Title = $"Sold - Income - {g.Key.INVOICE_ITEM_NAME}",
                                 NetAmount = g.Sum(x => x.l.TOTAL_AMOUNT),
                                 CurrencyId = g.Key.CURRENCY_ID,
                                 Active = true,
                                 RefId = $"Income.{g.Key.INVOICE_ITEM_ID}"
                             },
                             AR = new JournalEntryLine
                             {
                                 AccountId = g.Key.AR_ACCOUNT_ID,
                                 Title = $"Sold - AR - {g.Key.INVOICE_ITEM_NAME}",
                                 NetAmount = g.Sum(x => x.l.TOTAL_AMOUNT),
                                 CurrencyId = g.Key.CURRENCY_ID,
                                 Active = true,
                                 RefId = $"AR.{g.Key.INVOICE_ITEM_ID}"
                             }
                         }).ToList();
            var incomes = lines.Select(x => x.INCOME).ToList();
            var AR = lines.Select(x => x.AR).ToList();

            var jeLine = new List<JournalEntryLine>() { };
            jeLine.AddRange(incomes);
            jeLine.AddRange(AR);
            return jeLine;
            //return lines.Select(x => x.INCOME).Union(lines.Select(x => x.AR)).ToList();
        }

        public override void PushJournal(DateTime startDate, DateTime endDate)
        {
            var k = 1;
            var currDate = DBDataContext.Db.GetSystemDate();
            var tran = (from t in AvailableRecords(startDate, endDate)
                        group t by new { EntryDate = t.EntryDate.Date, t.CURRENCY_ID } into g
                        orderby g.Key.EntryDate
                        select new Transaction
                        {
                            Date = g.Key.EntryDate,
                            CurrencyId = g.Key.CURRENCY_ID,
                            Transactions = g.ToList()
                        }).ToList();

            foreach (var t in tran)
            {
                if (t.Transactions.Count > 0)
                {
                    Runner.Instance.Text = $"ចុះបញ្ចីវិក្កយបត្រសេវាកម្ម {k++}/{t.Transactions.Count}";

                    //Submit Pointer
                    var journal = new JournalEntry()
                    {
                        CompanyId = Current.CompanyId,
                        Active = true,
                        CurrencyId = t.CurrencyId,
                        EntryDate = t.Date,
                        RefNo = "From E-Power",
                        JournalId = AccountingLogic.Instance.Journal(AccountingConfig.JOURNAL_SALE).Id,
                        SourceRefId = "InvoiceDetailIds." + string.Join(",", t.Transactions.Select(x => x.INVOICE_DETAIL_ID).ToList()),
                        Posted = true,
                        Source = PostingSources.External,
                        Lines = EntryLines(t.Transactions),
                        Amount = t.Transactions.Sum(x => x.TOTAL_AMOUNT)
                    };
                    var j = JournalEntryLogic.Instance.PostExternal(journal);

                    //Sumit E-Power
                    var eJournal = new TBL_JOURNAL_ENTRY
                    {
                        REF_NO = j.RefNo,
                        CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                        CREATE_ON = currDate,
                        ENTRY_DATE = t.Date,
                        IS_ACTIVE = true,
                        CURRENCY_ID = t.CurrencyId,
                        REFERENCE_TYPE_ID = (int)ReferenceType.InvoiceService,
                        HB01_JOURNAL_ID = j.Id,
                        NOTE = ""
                    };
                    DBDataContext.Db.TBL_JOURNAL_ENTRies.InsertOnSubmit(eJournal);
                    DBDataContext.Db.SubmitChanges();
                    var line = EntryItems(t.Transactions, eJournal.JOURNAL_ENTRY_ID);
                    DBDataContext.Db.BulkCopy(line._ToDataTable(), $"HB01.{nameof(TBL_JOURNAL_ENTRY_ACCOUNT)}");
                    DBDataContext.Db.SubmitChanges();
                }
            }
        }
    }
}
