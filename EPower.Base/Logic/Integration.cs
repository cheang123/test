﻿using HB01.Domain.Models.Transactions;
using SoftTech;
using System;
using System.Collections.Generic;

namespace EPower.Base.Logic
{
    internal abstract class Integration<T>
    {
        public abstract List<T> AvailableRecords(DateTime startDate, DateTime endDate);
        //public abstract List<TBL_JOURNAL_ENTRY_ACCOUNT> EntryItems(List<T> list);
        public abstract List<JournalEntryLine> EntryLines(List<T> list);
        public abstract List<TBL_JOURNAL_ENTRY_ACCOUNT> EntryItems(List<T> list, int EntryId);
        public abstract void PushJournal(DateTime startDate, DateTime endDate);

        public class Transaction
        {
            public DateTime Date { get; set; }
            public int CurrencyId { get; set; }
            public bool IsServiceBill { get; set; } = false;
            public List<T> Transactions { get; set; }
            public List<T> Tax { get; set; }
        }
    }
}
