﻿using HB01.Domain.Models;
using HB01.Logics;
using SoftTech;
using System;

namespace EPower.Base.Logic
{
    public class PostingLogic
    {
        public static PostingLogic Instance { get; } = new PostingLogic();
        public bool isPosting = false;
        public DateTime start = DateTime.Now;
        public DateTime end = DateTime.Now;
        public void PostToPointer(DateTime d1, DateTime d2)
        {
            if (!PointerLogic.isConnectedPointer)
            {
                return;
            }

            if (isPosting || !validateDates(d1, d2))
            {
                return;
            }
            isPosting = true;
            //try
            //{
                DBDataContext.Db.CommandTimeout = (int)TimeSpan.FromMinutes(15).TotalMilliseconds;
                PushBillLogic.Instance.PushJournal(start, end);
                PushPaymentLogic.Instance.PushJournal(start, end);
                PushDepositLogic.Instance.PushJournal(start, end);
                PushPrepaymentLogic.Instance.PushJournal(start, end);
                PushAdjustmentLogic.Instance.PushJournal(start, end);
                isPosting = false;
            //}
            //catch(Exception ex)
            //{
            //    isPosting = false;
            //}
        }

        public void PostToPointerBg(DateTime d1, DateTime d2)
        {
            //try
            //{
            //    BackgroundWorker worker = new BackgroundWorker();
            //    worker.DoWork += delegate (object s1, DoWorkEventArgs e1)
            //    {
            //        PostToPointer(d1,d2);
            //    };
            //    worker.RunWorkerCompleted += delegate (object s1, RunWorkerCompletedEventArgs e1)
            //    {
            //        if (e1.Error != null || e1.Result == null)
            //        {
            //            return;
            //        }
            //        if (e1.Result.ToString().Length > 10)
            //        {
            //            MsgBox.ShowError(e1.Result.ToString(), Resources.WARRING);
            //            return;
            //        }
            //    };
            //    worker.RunWorkerAsync();
            //}
            //catch (Exception ex)
            //{
            //    isPosting = false;
            //}
        }

        public bool validateDates(DateTime d1, DateTime d2)
        {
            var LastOBDate = OpeningBalanceLogic.Instance.getLastOpeningBalanceDate(0).Date;
            if (LastOBDate <= Constant.MinDate)
            {
                return false;
            }
            start = d1.Date;
            end = d2.Date.AddDays(1).AddSeconds(-1);
            if (end <= LastOBDate)
            {
                return false;
            }
            if (start <= LastOBDate)
            {
                start = LastOBDate;
            }
            return true;
        }
    }
}
