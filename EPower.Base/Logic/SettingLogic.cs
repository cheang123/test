﻿using SoftTech;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EPower.Base.Logic
{
    public class SettingLogic
    {
        #region ConnectionTypes
        internal static List<CustomerConnectionTypes> connectionTypes = null;
        public static List<CustomerConnectionTypes> ConnectionTypes
        {
            get
            {
                if (connectionTypes == null)
                {
                    connectionTypes = (from cc in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                                       join cg in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on cc.NONLICENSE_CUSTOMER_GROUP_ID equals cg.CUSTOMER_GROUP_ID
                                       where cg.IS_ACTIVE && cc.IS_ACTIVE
                                       orderby cg.CUSTOMER_GROUP_ID, cc.CUSTOMER_CONNECTION_TYPE_ID
                                       select new CustomerConnectionTypes
                                       {
                                           CUSTOMER_CONNECTION_TYPE_ID = cc.CUSTOMER_CONNECTION_TYPE_ID,
                                           CUSTOMER_CONNECTION_TYPE_NAME = cg.CUSTOMER_GROUP_ID != 4 && cg.CUSTOMER_GROUP_ID != 5 ? cg.CUSTOMER_GROUP_NAME + " (" + cc.CUSTOMER_CONNECTION_TYPE_NAME + ")" : cc.CUSTOMER_CONNECTION_TYPE_NAME,
                                           INCOME_ACCOUNT = cc.IC_ACCOUNT,
                                           AR_KHR = cc.AR_KHR,
                                           AR_USD = cc.AR_USD,
                                           AR_THB = cc.AR_THB,
                                           AR_VND = cc.AR_VND
                                       }).ToList();
                }
                return connectionTypes;
            }
            set
            {
                connectionTypes = value;
            }
        }
        public static void CommitConnectionType()
        {
            //TLKP_CUSTOMER_CONNECTION_TYPE objNew = new TLKP_CUSTOMER_CONNECTION_TYPE();
            foreach (var con in ConnectionTypes)
            {
                var objOld = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.FirstOrDefault(x => x.CUSTOMER_CONNECTION_TYPE_ID == con.CUSTOMER_CONNECTION_TYPE_ID);
                if (objOld != null)
                {
                    //objOld._CopyTo(objNew);
                    objOld.IC_ACCOUNT = con.INCOME_ACCOUNT;
                    objOld.AR_KHR = con.AR_KHR;
                    objOld.AR_USD = con.AR_USD;
                    objOld.AR_THB = con.AR_THB;
                    objOld.AR_VND = con.AR_VND;
                    DBDataContext.Db.SubmitChanges();
                    //DBDataContext.Db.Update(objOld, objNew);
                }
            }
        }
        #endregion ConnectionTypes
        #region Currencies
        internal static List<Currency> currencies = null;
        public static List<Currency> Currencies
        {
            get
            {
                if (currencies == null)
                {
                    currencies = DBDataContext.Db.TLKP_CURRENCies.Select(c => new Currency
                    {
                        CURRENCY_ID = c.CURRENCY_ID,
                        CURRENCY_CODE = c.CURRENCY_CODE,
                        CURRENCY_NAME = c.CURRENCY_NAME,
                        CURRENCY_SIGN = c.CURRENCY_SING,
                        EXTERNAL_CURRENCY_ID = c.EXTERNAL_CURRENCY_ID,
                        IS_DEFAULT_CURRENCY = c.IS_DEFAULT_CURRENCY,
                        FORMAT = c.FORMAT
                    }).ToList();
                }
                return currencies;
            }
            set
            {
                currencies = value;
            }
        }
        public static void CommitCurrencies()
        {
            TLKP_CURRENCY objNew = new TLKP_CURRENCY();
            foreach (var currency in Currencies)
            {
                var objOld = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == currency.CURRENCY_ID);
                if (objOld != null)
                {
                    objOld._CopyTo(objNew);
                    objNew.EXTERNAL_CURRENCY_ID = currency.EXTERNAL_CURRENCY_ID;
                    DBDataContext.Db.Update(objOld, objNew);
                }
            }
            DBDataContext.Db.SubmitChanges();
        }
        #endregion
        #region InvoiceItems
        internal static List<InvoiceItems> invoiceItems = null;
        public static List<InvoiceItems> InvoiceItems
        {
            get
            {
                if (invoiceItems == null)
                {
                    invoiceItems = DBDataContext.Db.TBL_INVOICE_ITEMs.Where(i => i.IS_ACTIVE && i.INVOICE_ITEM_TYPE_ID != 0)
                                  .Select(i => new InvoiceItems
                                  {
                                      INVOICE_ITEM_ID = i.INVOICE_ITEM_ID,
                                      INVOICE_ITEM_NAME = i.INVOICE_ITEM_NAME,
                                      INVOICE_ITEM_TYPE_ID = i.INVOICE_ITEM_TYPE_ID,
                                      PRICE = i.PRICE,
                                      CURRENCY_ID = i.CURRENCY_ID,
                                      INCOME_ACCOUNT_ID = i.INCOME_ACCOUNT_ID,
                                      AR_KHR = i.AR_KHR,
                                      AR_USD = i.AR_USD,
                                      AR_THB = i.AR_THB,
                                      AR_VND = i.AR_VND
                                  }).ToList();
                }
                return invoiceItems;
            }
            set
            {
                invoiceItems = value;
            }
        }
        public static void CommitInvoiceItems()
        {
            TBL_INVOICE_ITEM objNew = new TBL_INVOICE_ITEM();
            foreach (var item in InvoiceItems)
            {
                var objOld = DBDataContext.Db.TBL_INVOICE_ITEMs.FirstOrDefault(x => x.INVOICE_ITEM_ID == item.INVOICE_ITEM_ID);
                if (objOld != null)
                {
                    objOld._CopyTo(objNew);
                    objNew.INCOME_ACCOUNT_ID = item.INCOME_ACCOUNT_ID;
                    objNew.AR_KHR = item.AR_KHR;
                    objNew.AR_USD = item.AR_USD;
                    objNew.AR_THB = item.AR_THB;
                    objNew.AR_VND = item.AR_VND;
                    objNew.CURRENCY_ID = item.CURRENCY_ID;
                    objNew.PRICE = item.PRICE;
                    DBDataContext.Db.Update(objOld, objNew);
                }
            }
            DBDataContext.Db.SubmitChanges();
        }
        #endregion
        #region AccountConfig
        internal static List<AccountingConfigs> accountConfigs = null;
        public static List<AccountingConfigs> AccountConfigs
        {
            get
            {
                if (accountConfigs == null)
                {
                    accountConfigs = DBDataContext.Db.TBL_ACCOUNT_CONFIGs
                                    .Select(c => new AccountingConfigs
                                    {
                                        AccountConfigId = c.CONFIG_ID,
                                        AccountConfigValue = c.CONFIG_VALUE
                                    }).ToList();
                }
                return accountConfigs;
            }
            set
            {
                accountConfigs = value;
            }
        }

        public static AccountingConfigs GetAccountConfig(int configId)
        {
            return AccountConfigs.FirstOrDefault(x => x.AccountConfigId == configId);
        }
        public static void UpdateAccountConfig(AccountingConfigs oldObj, AccountingConfigs newObj)
        {
            accountConfigs.Remove(oldObj);
            accountConfigs.Add(newObj);
        }
        public static void CommitAccountConfig()
        {
            TBL_ACCOUNT_CONFIG objNew = new TBL_ACCOUNT_CONFIG();
            foreach (var item in AccountConfigs)
            {
                var objOld = DBDataContext.Db.TBL_ACCOUNT_CONFIGs.FirstOrDefault(x => x.CONFIG_ID == item.AccountConfigId);
                if (objOld != null)
                {
                    objOld._CopyTo(objNew);
                    objNew.CONFIG_VALUE = item.AccountConfigValue;
                    DBDataContext.Db.Update(objOld, objNew);
                }
                DBDataContext.Db.SubmitChanges();
            }
        }
        #endregion
        #region JournalConfig
        internal static List<AccountingConfigs> journalConfigs = null;
        public static List<AccountingConfigs> JournalConfigs
        {
            get
            {
                if (journalConfigs == null)
                {
                    journalConfigs = DBDataContext.Db.TBL_ACCOUNTING_CONFIGs
                                    .Select(c => new AccountingConfigs
                                    {
                                        AccountConfigId = c.CONFIG_ID,
                                        AccountConfigValue = c.CONFIG_VALUE
                                    }).ToList();
                }
                return journalConfigs;
            }
            set
            {
                journalConfigs = value;
            }
        }

        public static AccountingConfigs GetJournalConfig(int configId)
        {
            return JournalConfigs.FirstOrDefault(x => x.AccountConfigId == configId);
        }
        public static void UpdateJournalConfig(AccountingConfigs objOld, AccountingConfigs objNew)
        {
            journalConfigs.Remove(objOld);
            journalConfigs.Add(objNew);
        }
        public static void CommitJournalConfig()
        {
            TBL_ACCOUNTING_CONFIG objNew = new TBL_ACCOUNTING_CONFIG();
            foreach (var item in JournalConfigs)
            {
                var objOld = DBDataContext.Db.TBL_ACCOUNTING_CONFIGs.FirstOrDefault(x => x.CONFIG_ID == item.AccountConfigId);
                if (objNew != null)
                {
                    objOld._CopyTo(objNew);
                    objNew.CONFIG_VALUE = item.AccountConfigValue;
                    DBDataContext.Db.Update(objOld, objNew);
                }
            }
            DBDataContext.Db.SubmitChanges();
        }
        #endregion
        #region PaymentConfig
        internal static List<PaymentSettingConfigs> paymentConfig = null;
        public static List<PaymentSettingConfigs> PaymentConfig
        {
            get
            {
                if (paymentConfig == null)
                {
                    paymentConfig = DBDataContext.Db.TBL_PAYMENT_CONFIGs.Where(x =>x.IS_ACTIVE)
                                  .Select(pc => new PaymentSettingConfigs
                                  {
                                      CONFIG_ID = pc.CONFIG_ID,
                                      PAYMENT_TYPE = pc.PAYMENT_TYPE,
                                      ACCOUNT_ID_KHR = pc.ACCOUNT_ID_KHR,
                                      ACCOUNT_ID_USD = pc.ACCOUNT_ID_USD,
                                      ACCOUNT_ID_THB = pc.ACCOUNT_ID_THB,
                                      ACCOUNT_ID_VND = pc.ACCOUNT_ID_VND,
                                      CUT_OFF_DATE_PAYMENT = pc.CUT_OFF_DATE_PAYMENT
                                  }).ToList();
                }
                return paymentConfig;
            }
            set
            {
                paymentConfig = value;
            }
        }
        public static void CommitPaymentConfig()
        {
            TBL_PAYMENT_CONFIG objNew = new TBL_PAYMENT_CONFIG();
            foreach (var paymentConfig in PaymentConfig)
            {
                var objOld = DBDataContext.Db.TBL_PAYMENT_CONFIGs.FirstOrDefault(x => x.CONFIG_ID == paymentConfig.CONFIG_ID);
                if (objOld != null)
                {
                    objOld._CopyTo(objNew);
                    objNew.ACCOUNT_ID_KHR = paymentConfig.ACCOUNT_ID_KHR;
                    objNew.ACCOUNT_ID_USD = paymentConfig.ACCOUNT_ID_USD;
                    objNew.CUT_OFF_DATE_PAYMENT = paymentConfig.CUT_OFF_DATE_PAYMENT;
                    DBDataContext.Db.Update(objOld, objNew);
                }
            }
            DBDataContext.Db.SubmitChanges();
        }
        #endregion

        public static void RenewSetting()
        {
            connectionTypes = null;
            currencies = null;
            invoiceItems = null;
            accountConfigs = null;
            journalConfigs = null;
        }
    }

    #region Models
    public class CustomerConnectionTypes
    {
        public int CUSTOMER_CONNECTION_TYPE_ID { get; set; }
        public string CUSTOMER_CONNECTION_TYPE_NAME { get; set; }
        public int INCOME_ACCOUNT { get; set; }
        public int AR_KHR { get; set; }
        public int AR_USD { get; set; }
        public int AR_THB { get; set; }
        public int AR_VND { get; set; }
    }
    public class Currency
    {
        public int CURRENCY_ID { get; set; }
        public string CURRENCY_NAME { get; set; }
        public string CURRENCY_SIGN { get; set; }
        public string CURRENCY_CODE { get; set; }
        public string FORMAT { get; set; }
        public int EXTERNAL_CURRENCY_ID { get; set; }
        public bool IS_DEFAULT_CURRENCY { get; set; }
    }
    public class InvoiceItems
    {
        public int AR_KHR { get; set; }
        public int AR_USD { get; set; }
        public int AR_THB { get; set; }
        public int AR_VND { get; set; }
        public int INCOME_ACCOUNT_ID { get; set; }
        public decimal PRICE { get; set; }
        public string INVOICE_ITEM_NAME { get; set; }
        public int CURRENCY_ID { get; set; }
        public int INVOICE_ITEM_TYPE_ID { get; set; }
        public int INVOICE_ITEM_ID { get; set; }
    }
    public class AccountingConfigs
    {
        public int AccountConfigId { get; set; }
        public string AccountConfigValue { get; set; }

    }

    public class PaymentSettingConfigs
    {
        public int CONFIG_ID { get; set; }
        public string PAYMENT_TYPE { get; set; }
        public int ACCOUNT_ID_KHR { get; set; }
        public int ACCOUNT_ID_USD { get; set; }
        public int ACCOUNT_ID_THB { get; set; }
        public int ACCOUNT_ID_VND { get; set; }
        public bool IS_ACTIVE { get; set; }
        public DateTime? CUT_OFF_DATE_PAYMENT { get; set; }
    }
    #endregion
}
