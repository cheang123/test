﻿namespace EPower.Accounting.Interface.Setting
{
    partial class PageInvoiceItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageInvoiceItem));
            this.roundPanel1 = new Dynamic.Component.RoundPanel();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.dgvInvoiceItems = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colINVOICE_ITEM_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINVOICE_ITEM_TYPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemType = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colINVOICE_ITEM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIS_ONLY_IN_BILLING = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVALUE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCURRENCY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colIS_ACTIVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIS_EDITABLE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repArAccount = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colIS_RECURRING_SERVICE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRECURRING_MONTH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colINCOME_ACCOUNT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repIcAccount = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colAR_KHR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.colAR_USD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAR_THB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAR_VND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.roundPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repArAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIcAccount)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // roundPanel1
            // 
            this.roundPanel1._BackColor = System.Drawing.Color.White;
            this.roundPanel1._Radius = 10;
            this.roundPanel1.BackColor = System.Drawing.Color.Transparent;
            this.roundPanel1.Controls.Add(this.dgv);
            this.roundPanel1.Controls.Add(this.panel1);
            resources.ApplyResources(this.roundPanel1, "roundPanel1");
            this.roundPanel1.Name = "roundPanel1";
            // 
            // dgv
            // 
            this.dgv.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EmbeddedNavigator.Margin = ((System.Windows.Forms.Padding)(resources.GetObject("dgv.EmbeddedNavigator.Margin")));
            this.dgv.MainView = this.dgvInvoiceItems;
            this.dgv.Name = "dgv";
            this.dgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repArAccount,
            this.repItemType,
            this.repIcAccount,
            this.repCurrency});
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvInvoiceItems});
            // 
            // dgvInvoiceItems
            // 
            this.dgvInvoiceItems.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colINVOICE_ITEM_ID,
            this.colINVOICE_ITEM_TYPE,
            this.colINVOICE_ITEM,
            this.colIS_ONLY_IN_BILLING,
            this.colVALUE,
            this.colCURRENCY,
            this.colIS_ACTIVE,
            this.colIS_EDITABLE,
            this.colIS_RECURRING_SERVICE,
            this.colRECURRING_MONTH,
            this.colINCOME_ACCOUNT,
            this.colAR_KHR,
            this.colAR_USD,
            this.colAR_THB,
            this.colAR_VND});
            this.dgvInvoiceItems.DetailHeight = 228;
            this.dgvInvoiceItems.GridControl = this.dgv;
            this.dgvInvoiceItems.Name = "dgvInvoiceItems";
            this.dgvInvoiceItems.OptionsDetail.EnableMasterViewMode = false;
            this.dgvInvoiceItems.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent;
            this.dgvInvoiceItems.OptionsView.ShowGroupPanel = false;
            this.dgvInvoiceItems.OptionsView.ShowIndicator = false;
            this.dgvInvoiceItems.OptionsView.WaitAnimationOptions = DevExpress.XtraEditors.WaitAnimationOptions.Panel;
            // 
            // colINVOICE_ITEM_ID
            // 
            this.colINVOICE_ITEM_ID.FieldName = "INVOICE_ITEM_ID";
            this.colINVOICE_ITEM_ID.MinWidth = 15;
            this.colINVOICE_ITEM_ID.Name = "colINVOICE_ITEM_ID";
            resources.ApplyResources(this.colINVOICE_ITEM_ID, "colINVOICE_ITEM_ID");
            // 
            // colINVOICE_ITEM_TYPE
            // 
            this.colINVOICE_ITEM_TYPE.ColumnEdit = this.repItemType;
            this.colINVOICE_ITEM_TYPE.FieldName = "INVOICE_ITEM_TYPE_ID";
            this.colINVOICE_ITEM_TYPE.MinWidth = 15;
            this.colINVOICE_ITEM_TYPE.Name = "colINVOICE_ITEM_TYPE";
            this.colINVOICE_ITEM_TYPE.OptionsColumn.ReadOnly = true;
            resources.ApplyResources(this.colINVOICE_ITEM_TYPE, "colINVOICE_ITEM_TYPE");
            // 
            // repItemType
            // 
            resources.ApplyResources(this.repItemType, "repItemType");
            this.repItemType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repItemType.Buttons"))))});
            this.repItemType.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repItemType.Columns"), resources.GetString("repItemType.Columns1"), ((int)(resources.GetObject("repItemType.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repItemType.Columns3"))), resources.GetString("repItemType.Columns4"), ((bool)(resources.GetObject("repItemType.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repItemType.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repItemType.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repItemType.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repItemType.Columns9"), resources.GetString("repItemType.Columns10")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repItemType.Columns11"), resources.GetString("repItemType.Columns12"), ((int)(resources.GetObject("repItemType.Columns13"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repItemType.Columns14"))), resources.GetString("repItemType.Columns15"), ((bool)(resources.GetObject("repItemType.Columns16"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repItemType.Columns17"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repItemType.Columns18"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repItemType.Columns19"))))});
            this.repItemType.DisplayMember = "INVOICE_ITEM_TYPE_NAME";
            this.repItemType.Name = "repItemType";
            this.repItemType.ValueMember = "INVOICE_ITEM_TYPE_ID";
            // 
            // colINVOICE_ITEM
            // 
            this.colINVOICE_ITEM.FieldName = "INVOICE_ITEM_NAME";
            this.colINVOICE_ITEM.MinWidth = 15;
            this.colINVOICE_ITEM.Name = "colINVOICE_ITEM";
            this.colINVOICE_ITEM.OptionsColumn.ReadOnly = true;
            resources.ApplyResources(this.colINVOICE_ITEM, "colINVOICE_ITEM");
            // 
            // colIS_ONLY_IN_BILLING
            // 
            this.colIS_ONLY_IN_BILLING.FieldName = "IS_ONLY_IN_BILLING";
            this.colIS_ONLY_IN_BILLING.MinWidth = 15;
            this.colIS_ONLY_IN_BILLING.Name = "colIS_ONLY_IN_BILLING";
            resources.ApplyResources(this.colIS_ONLY_IN_BILLING, "colIS_ONLY_IN_BILLING");
            // 
            // colVALUE
            // 
            this.colVALUE.AppearanceCell.Options.UseTextOptions = true;
            this.colVALUE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colVALUE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colVALUE.AppearanceHeader.Options.UseTextOptions = true;
            this.colVALUE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colVALUE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colVALUE.DisplayFormat.FormatString = "#,##0.####";
            this.colVALUE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colVALUE.FieldName = "PRICE";
            this.colVALUE.MinWidth = 15;
            this.colVALUE.Name = "colVALUE";
            resources.ApplyResources(this.colVALUE, "colVALUE");
            // 
            // colCURRENCY
            // 
            resources.ApplyResources(this.colCURRENCY, "colCURRENCY");
            this.colCURRENCY.ColumnEdit = this.repCurrency;
            this.colCURRENCY.FieldName = "CURRENCY_ID";
            this.colCURRENCY.MinWidth = 15;
            this.colCURRENCY.Name = "colCURRENCY";
            // 
            // repCurrency
            // 
            resources.ApplyResources(this.repCurrency, "repCurrency");
            this.repCurrency.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repCurrency.Buttons"))))});
            this.repCurrency.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repCurrency.Columns"), resources.GetString("repCurrency.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repCurrency.Columns2"), resources.GetString("repCurrency.Columns3"))});
            this.repCurrency.DisplayMember = "CURRENCY_CODE";
            this.repCurrency.Name = "repCurrency";
            this.repCurrency.ValueMember = "CURRENCY_ID";
            // 
            // colIS_ACTIVE
            // 
            this.colIS_ACTIVE.FieldName = "IS_ACTIVE";
            this.colIS_ACTIVE.MinWidth = 15;
            this.colIS_ACTIVE.Name = "colIS_ACTIVE";
            resources.ApplyResources(this.colIS_ACTIVE, "colIS_ACTIVE");
            // 
            // colIS_EDITABLE
            // 
            this.colIS_EDITABLE.ColumnEdit = this.repArAccount;
            this.colIS_EDITABLE.FieldName = "IS_EDITABLE";
            this.colIS_EDITABLE.MinWidth = 15;
            this.colIS_EDITABLE.Name = "colIS_EDITABLE";
            resources.ApplyResources(this.colIS_EDITABLE, "colIS_EDITABLE");
            // 
            // repArAccount
            // 
            resources.ApplyResources(this.repArAccount, "repArAccount");
            this.repArAccount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repArAccount.Buttons"))))});
            this.repArAccount.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repArAccount.Columns"), resources.GetString("repArAccount.Columns1"), ((int)(resources.GetObject("repArAccount.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repArAccount.Columns3"))), resources.GetString("repArAccount.Columns4"), ((bool)(resources.GetObject("repArAccount.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repArAccount.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repArAccount.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repArAccount.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repArAccount.Columns9"), resources.GetString("repArAccount.Columns10"), ((int)(resources.GetObject("repArAccount.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repArAccount.Columns12"))), resources.GetString("repArAccount.Columns13"), ((bool)(resources.GetObject("repArAccount.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repArAccount.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repArAccount.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repArAccount.Columns17"))))});
            this.repArAccount.DisplayMember = "AccountName";
            this.repArAccount.Name = "repArAccount";
            this.repArAccount.PopupWidth = 375;
            this.repArAccount.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.repArAccount.ValueMember = "Id";
            // 
            // colIS_RECURRING_SERVICE
            // 
            this.colIS_RECURRING_SERVICE.ColumnEdit = this.repArAccount;
            this.colIS_RECURRING_SERVICE.FieldName = "IS_RECURRING_SERVICE";
            this.colIS_RECURRING_SERVICE.MinWidth = 15;
            this.colIS_RECURRING_SERVICE.Name = "colIS_RECURRING_SERVICE";
            resources.ApplyResources(this.colIS_RECURRING_SERVICE, "colIS_RECURRING_SERVICE");
            // 
            // colRECURRING_MONTH
            // 
            this.colRECURRING_MONTH.FieldName = "RECURRING_MONTH";
            this.colRECURRING_MONTH.MinWidth = 15;
            this.colRECURRING_MONTH.Name = "colRECURRING_MONTH";
            resources.ApplyResources(this.colRECURRING_MONTH, "colRECURRING_MONTH");
            // 
            // colINCOME_ACCOUNT
            // 
            resources.ApplyResources(this.colINCOME_ACCOUNT, "colINCOME_ACCOUNT");
            this.colINCOME_ACCOUNT.ColumnEdit = this.repIcAccount;
            this.colINCOME_ACCOUNT.FieldName = "INCOME_ACCOUNT_ID";
            this.colINCOME_ACCOUNT.MinWidth = 15;
            this.colINCOME_ACCOUNT.Name = "colINCOME_ACCOUNT";
            // 
            // repIcAccount
            // 
            resources.ApplyResources(this.repIcAccount, "repIcAccount");
            this.repIcAccount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repIcAccount.Buttons"))))});
            this.repIcAccount.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repIcAccount.Columns"), resources.GetString("repIcAccount.Columns1"), ((int)(resources.GetObject("repIcAccount.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repIcAccount.Columns3"))), resources.GetString("repIcAccount.Columns4"), ((bool)(resources.GetObject("repIcAccount.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repIcAccount.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repIcAccount.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repIcAccount.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repIcAccount.Columns9"), resources.GetString("repIcAccount.Columns10"), ((int)(resources.GetObject("repIcAccount.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repIcAccount.Columns12"))), resources.GetString("repIcAccount.Columns13"), ((bool)(resources.GetObject("repIcAccount.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repIcAccount.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repIcAccount.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repIcAccount.Columns17"))))});
            this.repIcAccount.DisplayMember = "AccountName";
            this.repIcAccount.Name = "repIcAccount";
            this.repIcAccount.PopupWidth = 375;
            this.repIcAccount.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.repIcAccount.ValueMember = "Id";
            // 
            // colAR_KHR
            // 
            resources.ApplyResources(this.colAR_KHR, "colAR_KHR");
            this.colAR_KHR.ColumnEdit = this.repArAccount;
            this.colAR_KHR.FieldName = "AR_KHR";
            this.colAR_KHR.MinWidth = 15;
            this.colAR_KHR.Name = "colAR_KHR";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.searchControl);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            // 
            // searchControl
            // 
            this.searchControl.Client = this.dgv;
            resources.ApplyResources(this.searchControl, "searchControl");
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.Client = this.dgv;
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            // 
            // colAR_USD
            // 
            resources.ApplyResources(this.colAR_USD, "colAR_USD");
            this.colAR_USD.ColumnEdit = this.repArAccount;
            this.colAR_USD.FieldName = "AR_USD";
            this.colAR_USD.Name = "colAR_USD";
            // 
            // colAR_THB
            // 
            resources.ApplyResources(this.colAR_THB, "colAR_THB");
            this.colAR_THB.ColumnEdit = this.repArAccount;
            this.colAR_THB.FieldName = "AR_THB";
            this.colAR_THB.Name = "colAR_THB";
            // 
            // colAR_VND
            // 
            resources.ApplyResources(this.colAR_VND, "colAR_VND");
            this.colAR_VND.ColumnEdit = this.repArAccount;
            this.colAR_VND.FieldName = "AR_VND";
            this.colAR_VND.Name = "colAR_VND";
            // 
            // PageInvoiceItem
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.roundPanel1);
            this.Name = "PageInvoiceItem";
            this.roundPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repArAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repIcAccount)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Dynamic.Component.RoundPanel roundPanel1;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvInvoiceItems;
        private DevExpress.XtraGrid.Columns.GridColumn colINVOICE_ITEM_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colINVOICE_ITEM_TYPE;
        private DevExpress.XtraGrid.Columns.GridColumn colCURRENCY;
        private DevExpress.XtraGrid.Columns.GridColumn colINVOICE_ITEM;
        private DevExpress.XtraGrid.Columns.GridColumn colIS_ONLY_IN_BILLING;
        private DevExpress.XtraGrid.Columns.GridColumn colVALUE;
        private DevExpress.XtraGrid.Columns.GridColumn colIS_ACTIVE;
        private DevExpress.XtraGrid.Columns.GridColumn colIS_EDITABLE;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repArAccount;
        private DevExpress.XtraGrid.Columns.GridColumn colIS_RECURRING_SERVICE;
        private DevExpress.XtraGrid.Columns.GridColumn colRECURRING_MONTH;
        private DevExpress.XtraGrid.Columns.GridColumn colINCOME_ACCOUNT;
        private DevExpress.XtraGrid.Columns.GridColumn colAR_KHR;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repItemType;
        private DevExpress.XtraEditors.SearchControl searchControl;
        private SoftTech.Component.ExButton btnREMOVE;
        private SoftTech.Component.ExButton btnADD;
        private SoftTech.Component.ExButton btnEDIT;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repIcAccount;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colAR_USD;
        private DevExpress.XtraGrid.Columns.GridColumn colAR_THB;
        private DevExpress.XtraGrid.Columns.GridColumn colAR_VND;
    }
}
