﻿using EPower.Base.Logic;
using HB01.Domain.Enums;
using HB01.Domain.ListModels;
using HB01.Domain.Models.Settings;
using HB01.Helpers.DevExpressCustomize;
using HB01.Logics;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Accounting.Interface
{
    public partial class PageGeneralSetting : Form
    {
        #region PrivateDate
        AccountingConfigs objOld = new AccountingConfigs();
        AccountingConfigs objNew = new AccountingConfigs();

        #endregion

        #region Constructor
        public PageGeneralSetting()
        {
            InitializeComponent();

            //Diable control if not connect Pointer
            if (PointerLogic.isConnectedPointer)
            {
                Bind();
                Read();
            }
            else
            {
                this.Enabled = false;
            }

            /**
			 * Event
			 */
            this.cboJOURNAL_SALE.EditValueChanged += cboJOURNAL_SALE_EditValueChanged;
            this.cboJOURNAL_CASH.EditValueChanged += cboJOURNAL_CASH_EditValueChanged;
            this.cboJOURNAL_ADJUSTMENT.EditValueChanged += cboJOURNAL_ADJUSTMENT_EditValueChanged;
            this.cboJOURNAL_DEPOSIT.EditValueChanged += cboJOURNAL_DEPOSIT_EditValueChanged;
            this.cboJOURNAL_PREPAYMENT.EditValueChanged += cboJOURNAL_PREPAYMENT_EditValueChanged;
            this.cboTaxVAT.EditValueChanged += cboTaxVAT_EditValueChanged;

            this.cboPrepayAccount.EditValueChanged += cboPrepayAccount_EditValueChanged;
            this.cboDepositAccount.EditValueChanged += cboDepositAccount_EditValueChanged;
            this.cboDepositReceive.EditValueChanged += cboDepositReceive_EditValueChanged;
        }

        #endregion

        #region Method
        private void Bind()
        {
            //Journal
            cboJOURNAL_SALE.SetDatasource(JournalLogic.Instance.All().ToList(), nameof(Journal.Name));
            cboJOURNAL_CASH.SetDatasource(JournalLogic.Instance.All().ToList(), nameof(Journal.Name));
            cboJOURNAL_ADJUSTMENT.SetDatasource(JournalLogic.Instance.All().ToList(), nameof(Journal.Name));
            cboJOURNAL_DEPOSIT.SetDatasource(JournalLogic.Instance.All().ToList(), nameof(Journal.Name));
            cboJOURNAL_PREPAYMENT.SetDatasource(JournalLogic.Instance.All().ToList(), nameof(Journal.Name));
            cboTaxVAT.SetDatasource(TaxLogic.Instance.All().ToList(), nameof(Name));

            //Account
            var accountSearchParam = new AccountSearchParam() { };
            var accounts = AccountLogic.Instance.List<AccountListModel>(accountSearchParam);
            var cashAcc = accounts.Where(x => x.AccountNature == HB01.Domain.Helpers.DomainResourceHelper.Translate(AccountNatures.Asset.ToString())).ToList();
            var depositAcc = accounts.Where(x => x.AccountNature == HB01.Domain.Helpers.DomainResourceHelper.Translate(AccountNatures.Liability.ToString())).ToList();

            this.cboDepositAccount.SetDatasourceList(depositAcc, nameof(AccountListModel.DisplayAccountName));
            this.cboPrepayAccount.SetDatasourceList(depositAcc, nameof(AccountListModel.DisplayAccountName));
            this.cboDepositReceive.SetDatasourceList(cashAcc, nameof(AccountListModel.DisplayAccountName));
        }

        private void Read()
        {
            //Journal
            cboJOURNAL_SALE.EditValue = AccountingLogic.Instance.Journal(AccountingConfig.JOURNAL_SALE)?.Id;
            cboJOURNAL_CASH.EditValue = AccountingLogic.Instance.Journal(AccountingConfig.JOURNAL_CASH)?.Id;
            cboJOURNAL_ADJUSTMENT.EditValue = AccountingLogic.Instance.Journal(AccountingConfig.JOURNAL_ADJUSTMENT)?.Id;
            cboJOURNAL_DEPOSIT.EditValue = AccountingLogic.Instance.Journal(AccountingConfig.JOURNAL_DEPOSIT)?.Id;
            cboJOURNAL_PREPAYMENT.EditValue = AccountingLogic.Instance.Journal(AccountingConfig.JOURNAL_PREPAYMENT)?.Id;
            cboTaxVAT.EditValue = AccountingLogic.Instance.Tax(AccountingConfig.TAX)?.Id;

            //Account
            this.cboPrepayAccount.EditValue = AccountingLogic.Instance.AccountConfigId(AccountConfig.PREPAYMENT_ACCOUNT).FirstOrDefault();
            this.cboDepositAccount.EditValue = AccountingLogic.Instance.AccountConfigId(AccountConfig.DEPOSIT_ACCOUNT).FirstOrDefault();
            this.cboDepositReceive.EditValue = AccountingLogic.Instance.AccountConfigId(AccountConfig.DEPOSIT_ACCOUNT_CASH).FirstOrDefault();
        }

        #endregion

        #region Events
        private void cboJOURNAL_SALE_EditValueChanged(object sender, EventArgs e)
        {
            objOld = SettingLogic.GetJournalConfig((int)AccountingConfig.JOURNAL_SALE);
            objNew = objOld;
            objNew.AccountConfigValue = this.cboJOURNAL_SALE.EditValue?.ToString() ?? "";
            SettingLogic.UpdateJournalConfig(objOld, objNew);
        }

        private void cboJOURNAL_CASH_EditValueChanged(object sender, EventArgs e)
        {
            objOld = SettingLogic.GetJournalConfig((int)AccountingConfig.JOURNAL_CASH);
            objNew = objOld;
            objNew.AccountConfigValue = this.cboJOURNAL_CASH.EditValue?.ToString() ?? "";
            SettingLogic.UpdateJournalConfig(objOld, objNew);
        }

        private void cboJOURNAL_ADJUSTMENT_EditValueChanged(object sender, EventArgs e)
        {
            objOld = SettingLogic.GetJournalConfig((int)AccountingConfig.JOURNAL_ADJUSTMENT);
            objNew = objOld;
            objNew.AccountConfigValue = this.cboJOURNAL_ADJUSTMENT.EditValue?.ToString() ?? "";
            SettingLogic.UpdateJournalConfig(objOld, objNew);
        }

        private void cboJOURNAL_DEPOSIT_EditValueChanged(object sender, EventArgs e)
        {
            objOld = SettingLogic.GetJournalConfig((int)AccountingConfig.JOURNAL_DEPOSIT);
            objNew = objOld;
            objNew.AccountConfigValue = this.cboJOURNAL_DEPOSIT.EditValue?.ToString() ?? "";
            SettingLogic.UpdateJournalConfig(objOld, objNew);
        }

        private void cboJOURNAL_PREPAYMENT_EditValueChanged(object sender, EventArgs e)
        {
            objOld = SettingLogic.GetJournalConfig((int)AccountingConfig.JOURNAL_PREPAYMENT);
            objNew = objOld;
            objNew.AccountConfigValue = this.cboJOURNAL_PREPAYMENT.EditValue?.ToString() ?? "";
            SettingLogic.UpdateJournalConfig(objOld, objNew);
        }

        private void cboTaxVAT_EditValueChanged(object sender, EventArgs e)
        {
            objOld = SettingLogic.GetJournalConfig((int)AccountingConfig.TAX);
            objNew = objOld;
            objNew.AccountConfigValue = this.cboTaxVAT.EditValue?.ToString() ?? "";
            SettingLogic.UpdateJournalConfig(objOld, objNew);
        }

        private void cboDepositReceive_EditValueChanged(object sender, EventArgs e)
        {
            objOld = SettingLogic.GetAccountConfig((int)AccountConfig.DEPOSIT_ACCOUNT_CASH);
            objNew = objOld;
            objNew.AccountConfigValue = this.cboDepositReceive.EditValue?.ToString() ?? "";
            SettingLogic.UpdateAccountConfig(objOld, objNew);
        }

        private void cboDepositAccount_EditValueChanged(object sender, EventArgs e)
        {
            objOld = SettingLogic.GetAccountConfig((int)AccountConfig.DEPOSIT_ACCOUNT);
            objNew = objOld;
            objNew.AccountConfigValue = this.cboDepositAccount.EditValue?.ToString() ?? "";
            SettingLogic.UpdateAccountConfig(objOld, objNew);
        }

        private void cboPrepayAccount_EditValueChanged(object sender, EventArgs e)
        {
            objOld = SettingLogic.GetAccountConfig((int)AccountConfig.PREPAYMENT_ACCOUNT);
            objNew = objOld;
            objNew.AccountConfigValue = this.cboPrepayAccount.EditValue?.ToString() ?? "";
            SettingLogic.UpdateAccountConfig(objOld, objNew);
        }

        #endregion
    }
}
