﻿namespace EPower.Accounting.Interface
{
	partial class PageGeneralSetting
    {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.roundPanel1 = new Dynamic.Component.RoundPanel();
            this.grpACCOUNT_SETTING = new System.Windows.Forms.GroupBox();
            this.cboDepositReceive = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.lblDeposit_Receive_Account = new System.Windows.Forms.Label();
            this.cboDepositAccount = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.lblDeposit_Account = new System.Windows.Forms.Label();
            this.cboPrepayAccount = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.lblPrepay_Account = new System.Windows.Forms.Label();
            this.grpJOURNAL = new System.Windows.Forms.GroupBox();
            this.cboJOURNAL_PREPAYMENT = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.lblJOURNAL_PREPAYMENT = new System.Windows.Forms.Label();
            this.cboTaxVAT = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.cboJOURNAL_ADJUSTMENT = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.cboJOURNAL_DEPOSIT = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.lblTax_VAT = new System.Windows.Forms.Label();
            this.cboJOURNAL_CASH = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.cboJOURNAL_SALE = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.lblJOURNAL_ADJUSTMENT = new System.Windows.Forms.Label();
            this.lblJOURNAL_DEPOSIT = new System.Windows.Forms.Label();
            this.lblJOURNAL_CASH = new System.Windows.Forms.Label();
            this.lblJOURNAL_SALE = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.roundPanel1.SuspendLayout();
            this.grpACCOUNT_SETTING.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboDepositReceive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDepositAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPrepayAccount.Properties)).BeginInit();
            this.grpJOURNAL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboJOURNAL_PREPAYMENT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTaxVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboJOURNAL_ADJUSTMENT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboJOURNAL_DEPOSIT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboJOURNAL_CASH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboJOURNAL_SALE.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // roundPanel1
            // 
            this.roundPanel1._BackColor = System.Drawing.Color.White;
            this.roundPanel1._Radius = 10;
            this.roundPanel1.BackColor = System.Drawing.Color.Transparent;
            this.roundPanel1.Controls.Add(this.grpACCOUNT_SETTING);
            this.roundPanel1.Controls.Add(this.grpJOURNAL);
            this.roundPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roundPanel1.Location = new System.Drawing.Point(2, 2);
            this.roundPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.roundPanel1.Name = "roundPanel1";
            this.roundPanel1.Size = new System.Drawing.Size(847, 333);
            this.roundPanel1.TabIndex = 34;
            // 
            // grpACCOUNT_SETTING
            // 
            this.grpACCOUNT_SETTING.Controls.Add(this.cboDepositReceive);
            this.grpACCOUNT_SETTING.Controls.Add(this.lblDeposit_Receive_Account);
            this.grpACCOUNT_SETTING.Controls.Add(this.cboDepositAccount);
            this.grpACCOUNT_SETTING.Controls.Add(this.lblDeposit_Account);
            this.grpACCOUNT_SETTING.Controls.Add(this.cboPrepayAccount);
            this.grpACCOUNT_SETTING.Controls.Add(this.lblPrepay_Account);
            this.grpACCOUNT_SETTING.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpACCOUNT_SETTING.Location = new System.Drawing.Point(0, 129);
            this.grpACCOUNT_SETTING.Name = "grpACCOUNT_SETTING";
            this.grpACCOUNT_SETTING.Padding = new System.Windows.Forms.Padding(5);
            this.grpACCOUNT_SETTING.Size = new System.Drawing.Size(847, 101);
            this.grpACCOUNT_SETTING.TabIndex = 286;
            this.grpACCOUNT_SETTING.TabStop = false;
            this.grpACCOUNT_SETTING.Text = "ប្រភពគណនី";
            // 
            // cboDepositReceive
            // 
            this.cboDepositReceive.Location = new System.Drawing.Point(145, 60);
            this.cboDepositReceive.Name = "cboDepositReceive";
            this.cboDepositReceive.Properties.Appearance.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cboDepositReceive.Properties.Appearance.Options.UseFont = true;
            this.cboDepositReceive.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDepositReceive.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AccountCode", "កូដ", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AccountName", "ឈ្មោះ", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AccountType", "ប្រភេទ", 120, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cboDepositReceive.Properties.NullText = "";
            this.cboDepositReceive.Properties.PopupWidth = 500;
            this.cboDepositReceive.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboDepositReceive.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboDepositReceive.Required = false;
            this.cboDepositReceive.Size = new System.Drawing.Size(250, 26);
            this.cboDepositReceive.TabIndex = 8;
            // 
            // lblDeposit_Receive_Account
            // 
            this.lblDeposit_Receive_Account.AutoSize = true;
            this.lblDeposit_Receive_Account.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblDeposit_Receive_Account.Location = new System.Drawing.Point(18, 63);
            this.lblDeposit_Receive_Account.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.lblDeposit_Receive_Account.Name = "lblDeposit_Receive_Account";
            this.lblDeposit_Receive_Account.Size = new System.Drawing.Size(77, 19);
            this.lblDeposit_Receive_Account.TabIndex = 286;
            this.lblDeposit_Receive_Account.Text = "ទទួលប្រាក់កក់";
            // 
            // cboDepositAccount
            // 
            this.cboDepositAccount.Location = new System.Drawing.Point(145, 28);
            this.cboDepositAccount.Name = "cboDepositAccount";
            this.cboDepositAccount.Properties.Appearance.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cboDepositAccount.Properties.Appearance.Options.UseFont = true;
            this.cboDepositAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboDepositAccount.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AccountCode", "កូដ", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AccountName", "ឈ្មោះ", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AccountType", "ប្រភេទ", 120, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cboDepositAccount.Properties.NullText = "";
            this.cboDepositAccount.Properties.PopupWidth = 500;
            this.cboDepositAccount.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboDepositAccount.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboDepositAccount.Required = false;
            this.cboDepositAccount.Size = new System.Drawing.Size(250, 26);
            this.cboDepositAccount.TabIndex = 7;
            // 
            // lblDeposit_Account
            // 
            this.lblDeposit_Account.AutoSize = true;
            this.lblDeposit_Account.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblDeposit_Account.Location = new System.Drawing.Point(18, 31);
            this.lblDeposit_Account.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.lblDeposit_Account.Name = "lblDeposit_Account";
            this.lblDeposit_Account.Size = new System.Drawing.Size(89, 19);
            this.lblDeposit_Account.TabIndex = 284;
            this.lblDeposit_Account.Text = "ប្រាក់កក់អតិថិជន";
            // 
            // cboPrepayAccount
            // 
            this.cboPrepayAccount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboPrepayAccount.Location = new System.Drawing.Point(575, 28);
            this.cboPrepayAccount.Name = "cboPrepayAccount";
            this.cboPrepayAccount.Properties.Appearance.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cboPrepayAccount.Properties.Appearance.Options.UseFont = true;
            this.cboPrepayAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboPrepayAccount.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AccountCode", "កូដ", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AccountName", "ឈ្មោះ", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("AccountType", "ប្រភេទ", 120, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cboPrepayAccount.Properties.NullText = "";
            this.cboPrepayAccount.Properties.PopupWidth = 500;
            this.cboPrepayAccount.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboPrepayAccount.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboPrepayAccount.Required = false;
            this.cboPrepayAccount.Size = new System.Drawing.Size(250, 26);
            this.cboPrepayAccount.TabIndex = 9;
            // 
            // lblPrepay_Account
            // 
            this.lblPrepay_Account.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPrepay_Account.AutoSize = true;
            this.lblPrepay_Account.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblPrepay_Account.Location = new System.Drawing.Point(431, 31);
            this.lblPrepay_Account.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.lblPrepay_Account.Name = "lblPrepay_Account";
            this.lblPrepay_Account.Size = new System.Drawing.Size(101, 19);
            this.lblPrepay_Account.TabIndex = 282;
            this.lblPrepay_Account.Text = "ប្រាក់តម្កល់អតិថិជន";
            // 
            // grpJOURNAL
            // 
            this.grpJOURNAL.Controls.Add(this.cboJOURNAL_PREPAYMENT);
            this.grpJOURNAL.Controls.Add(this.lblJOURNAL_PREPAYMENT);
            this.grpJOURNAL.Controls.Add(this.cboTaxVAT);
            this.grpJOURNAL.Controls.Add(this.cboJOURNAL_ADJUSTMENT);
            this.grpJOURNAL.Controls.Add(this.cboJOURNAL_DEPOSIT);
            this.grpJOURNAL.Controls.Add(this.lblTax_VAT);
            this.grpJOURNAL.Controls.Add(this.cboJOURNAL_CASH);
            this.grpJOURNAL.Controls.Add(this.cboJOURNAL_SALE);
            this.grpJOURNAL.Controls.Add(this.lblJOURNAL_ADJUSTMENT);
            this.grpJOURNAL.Controls.Add(this.lblJOURNAL_DEPOSIT);
            this.grpJOURNAL.Controls.Add(this.lblJOURNAL_CASH);
            this.grpJOURNAL.Controls.Add(this.lblJOURNAL_SALE);
            this.grpJOURNAL.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpJOURNAL.Location = new System.Drawing.Point(0, 0);
            this.grpJOURNAL.Name = "grpJOURNAL";
            this.grpJOURNAL.Padding = new System.Windows.Forms.Padding(5);
            this.grpJOURNAL.Size = new System.Drawing.Size(847, 129);
            this.grpJOURNAL.TabIndex = 75;
            this.grpJOURNAL.TabStop = false;
            this.grpJOURNAL.Text = "ទិនានុប្បវត្តិ";
            // 
            // cboJOURNAL_PREPAYMENT
            // 
            this.cboJOURNAL_PREPAYMENT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboJOURNAL_PREPAYMENT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboJOURNAL_PREPAYMENT.Location = new System.Drawing.Point(575, 54);
            this.cboJOURNAL_PREPAYMENT.Margin = new System.Windows.Forms.Padding(2);
            this.cboJOURNAL_PREPAYMENT.Name = "cboJOURNAL_PREPAYMENT";
            this.cboJOURNAL_PREPAYMENT.Properties.Appearance.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cboJOURNAL_PREPAYMENT.Properties.Appearance.Options.UseFont = true;
            this.cboJOURNAL_PREPAYMENT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboJOURNAL_PREPAYMENT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "ឈ្មោះ", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cboJOURNAL_PREPAYMENT.Properties.NullText = "";
            this.cboJOURNAL_PREPAYMENT.Properties.PopupWidth = 200;
            this.cboJOURNAL_PREPAYMENT.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboJOURNAL_PREPAYMENT.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboJOURNAL_PREPAYMENT.Required = false;
            this.cboJOURNAL_PREPAYMENT.Size = new System.Drawing.Size(250, 26);
            this.cboJOURNAL_PREPAYMENT.TabIndex = 5;
            // 
            // lblJOURNAL_PREPAYMENT
            // 
            this.lblJOURNAL_PREPAYMENT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblJOURNAL_PREPAYMENT.AutoSize = true;
            this.lblJOURNAL_PREPAYMENT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblJOURNAL_PREPAYMENT.Location = new System.Drawing.Point(431, 59);
            this.lblJOURNAL_PREPAYMENT.Margin = new System.Windows.Forms.Padding(20, 10, 2, 2);
            this.lblJOURNAL_PREPAYMENT.Name = "lblJOURNAL_PREPAYMENT";
            this.lblJOURNAL_PREPAYMENT.Size = new System.Drawing.Size(61, 19);
            this.lblJOURNAL_PREPAYMENT.TabIndex = 283;
            this.lblJOURNAL_PREPAYMENT.Text = "ប្រាក់តម្កល់";
            // 
            // cboTaxVAT
            // 
            this.cboTaxVAT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTaxVAT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboTaxVAT.Location = new System.Drawing.Point(575, 84);
            this.cboTaxVAT.Margin = new System.Windows.Forms.Padding(2);
            this.cboTaxVAT.Name = "cboTaxVAT";
            this.cboTaxVAT.Properties.Appearance.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cboTaxVAT.Properties.Appearance.Options.UseFont = true;
            this.cboTaxVAT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboTaxVAT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "ឈ្មោះ", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cboTaxVAT.Properties.NullText = "";
            this.cboTaxVAT.Properties.PopupWidth = 200;
            this.cboTaxVAT.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboTaxVAT.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboTaxVAT.Required = false;
            this.cboTaxVAT.Size = new System.Drawing.Size(250, 26);
            this.cboTaxVAT.TabIndex = 6;
            this.cboTaxVAT.Visible = false;
            // 
            // cboJOURNAL_ADJUSTMENT
            // 
            this.cboJOURNAL_ADJUSTMENT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboJOURNAL_ADJUSTMENT.Location = new System.Drawing.Point(145, 84);
            this.cboJOURNAL_ADJUSTMENT.Margin = new System.Windows.Forms.Padding(2);
            this.cboJOURNAL_ADJUSTMENT.Name = "cboJOURNAL_ADJUSTMENT";
            this.cboJOURNAL_ADJUSTMENT.Properties.Appearance.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cboJOURNAL_ADJUSTMENT.Properties.Appearance.Options.UseFont = true;
            this.cboJOURNAL_ADJUSTMENT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboJOURNAL_ADJUSTMENT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "ឈ្មោះ", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cboJOURNAL_ADJUSTMENT.Properties.NullText = "";
            this.cboJOURNAL_ADJUSTMENT.Properties.PopupWidth = 200;
            this.cboJOURNAL_ADJUSTMENT.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboJOURNAL_ADJUSTMENT.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboJOURNAL_ADJUSTMENT.Required = false;
            this.cboJOURNAL_ADJUSTMENT.Size = new System.Drawing.Size(250, 26);
            this.cboJOURNAL_ADJUSTMENT.TabIndex = 3;
            // 
            // cboJOURNAL_DEPOSIT
            // 
            this.cboJOURNAL_DEPOSIT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboJOURNAL_DEPOSIT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboJOURNAL_DEPOSIT.Location = new System.Drawing.Point(575, 24);
            this.cboJOURNAL_DEPOSIT.Margin = new System.Windows.Forms.Padding(2);
            this.cboJOURNAL_DEPOSIT.Name = "cboJOURNAL_DEPOSIT";
            this.cboJOURNAL_DEPOSIT.Properties.Appearance.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cboJOURNAL_DEPOSIT.Properties.Appearance.Options.UseFont = true;
            this.cboJOURNAL_DEPOSIT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboJOURNAL_DEPOSIT.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "ឈ្មោះ", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cboJOURNAL_DEPOSIT.Properties.NullText = "";
            this.cboJOURNAL_DEPOSIT.Properties.PopupWidth = 200;
            this.cboJOURNAL_DEPOSIT.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboJOURNAL_DEPOSIT.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboJOURNAL_DEPOSIT.Required = false;
            this.cboJOURNAL_DEPOSIT.Size = new System.Drawing.Size(250, 26);
            this.cboJOURNAL_DEPOSIT.TabIndex = 4;
            // 
            // lblTax_VAT
            // 
            this.lblTax_VAT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTax_VAT.AutoSize = true;
            this.lblTax_VAT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblTax_VAT.Location = new System.Drawing.Point(431, 89);
            this.lblTax_VAT.Margin = new System.Windows.Forms.Padding(20, 10, 2, 2);
            this.lblTax_VAT.Name = "lblTax_VAT";
            this.lblTax_VAT.Size = new System.Drawing.Size(106, 19);
            this.lblTax_VAT.TabIndex = 83;
            this.lblTax_VAT.Text = "ពន្ធអាករ (VAT 10%)";
            this.lblTax_VAT.Visible = false;
            // 
            // cboJOURNAL_CASH
            // 
            this.cboJOURNAL_CASH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboJOURNAL_CASH.Location = new System.Drawing.Point(145, 54);
            this.cboJOURNAL_CASH.Margin = new System.Windows.Forms.Padding(2);
            this.cboJOURNAL_CASH.Name = "cboJOURNAL_CASH";
            this.cboJOURNAL_CASH.Properties.Appearance.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cboJOURNAL_CASH.Properties.Appearance.Options.UseFont = true;
            this.cboJOURNAL_CASH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboJOURNAL_CASH.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "ឈ្មោះ", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cboJOURNAL_CASH.Properties.NullText = "";
            this.cboJOURNAL_CASH.Properties.PopupWidth = 200;
            this.cboJOURNAL_CASH.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboJOURNAL_CASH.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboJOURNAL_CASH.Required = false;
            this.cboJOURNAL_CASH.Size = new System.Drawing.Size(250, 26);
            this.cboJOURNAL_CASH.TabIndex = 2;
            // 
            // cboJOURNAL_SALE
            // 
            this.cboJOURNAL_SALE.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboJOURNAL_SALE.Location = new System.Drawing.Point(145, 24);
            this.cboJOURNAL_SALE.Margin = new System.Windows.Forms.Padding(2);
            this.cboJOURNAL_SALE.Name = "cboJOURNAL_SALE";
            this.cboJOURNAL_SALE.Properties.Appearance.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.cboJOURNAL_SALE.Properties.Appearance.Options.UseFont = true;
            this.cboJOURNAL_SALE.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboJOURNAL_SALE.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "ឈ្មោះ", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default)});
            this.cboJOURNAL_SALE.Properties.NullText = "";
            this.cboJOURNAL_SALE.Properties.PopupWidth = 200;
            this.cboJOURNAL_SALE.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboJOURNAL_SALE.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboJOURNAL_SALE.Required = false;
            this.cboJOURNAL_SALE.Size = new System.Drawing.Size(250, 26);
            this.cboJOURNAL_SALE.TabIndex = 1;
            // 
            // lblJOURNAL_ADJUSTMENT
            // 
            this.lblJOURNAL_ADJUSTMENT.AutoSize = true;
            this.lblJOURNAL_ADJUSTMENT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblJOURNAL_ADJUSTMENT.Location = new System.Drawing.Point(18, 89);
            this.lblJOURNAL_ADJUSTMENT.Margin = new System.Windows.Forms.Padding(20, 10, 2, 2);
            this.lblJOURNAL_ADJUSTMENT.Name = "lblJOURNAL_ADJUSTMENT";
            this.lblJOURNAL_ADJUSTMENT.Size = new System.Drawing.Size(45, 19);
            this.lblJOURNAL_ADJUSTMENT.TabIndex = 70;
            this.lblJOURNAL_ADJUSTMENT.Text = "កែតម្រូវ";
            // 
            // lblJOURNAL_DEPOSIT
            // 
            this.lblJOURNAL_DEPOSIT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblJOURNAL_DEPOSIT.AutoSize = true;
            this.lblJOURNAL_DEPOSIT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblJOURNAL_DEPOSIT.Location = new System.Drawing.Point(431, 29);
            this.lblJOURNAL_DEPOSIT.Margin = new System.Windows.Forms.Padding(20, 10, 2, 2);
            this.lblJOURNAL_DEPOSIT.Name = "lblJOURNAL_DEPOSIT";
            this.lblJOURNAL_DEPOSIT.Size = new System.Drawing.Size(49, 19);
            this.lblJOURNAL_DEPOSIT.TabIndex = 70;
            this.lblJOURNAL_DEPOSIT.Text = "ប្រាក់កក់";
            // 
            // lblJOURNAL_CASH
            // 
            this.lblJOURNAL_CASH.AutoSize = true;
            this.lblJOURNAL_CASH.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblJOURNAL_CASH.Location = new System.Drawing.Point(18, 59);
            this.lblJOURNAL_CASH.Margin = new System.Windows.Forms.Padding(20, 10, 2, 2);
            this.lblJOURNAL_CASH.Name = "lblJOURNAL_CASH";
            this.lblJOURNAL_CASH.Size = new System.Drawing.Size(58, 19);
            this.lblJOURNAL_CASH.TabIndex = 70;
            this.lblJOURNAL_CASH.Text = "សាច់ប្រាក់";
            // 
            // lblJOURNAL_SALE
            // 
            this.lblJOURNAL_SALE.AutoSize = true;
            this.lblJOURNAL_SALE.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblJOURNAL_SALE.Location = new System.Drawing.Point(18, 29);
            this.lblJOURNAL_SALE.Margin = new System.Windows.Forms.Padding(20, 10, 2, 2);
            this.lblJOURNAL_SALE.Name = "lblJOURNAL_SALE";
            this.lblJOURNAL_SALE.Size = new System.Drawing.Size(29, 19);
            this.lblJOURNAL_SALE.TabIndex = 70;
            this.lblJOURNAL_SALE.Text = "លក់";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "PRODUCTION_WATER_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "PRODUCTION_WATER_ID";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "METER_CODE";
            this.dataGridViewTextBoxColumn2.HeaderText = "លេខកូដនាឡិកា";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "START_USAGE";
            this.dataGridViewTextBoxColumn3.HeaderText = "អំណានចាស់";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "END_USAGE";
            dataGridViewCellStyle1.Format = "MM-yyyy";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn4.FillWeight = 150F;
            this.dataGridViewTextBoxColumn4.HeaderText = "អំណានថ្មី";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "MULTIPLIER";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn5.HeaderText = "មេគុណ";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "COMPENSATE_USAGE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn6.HeaderText = "បរិមាណរំលឹកបន្ថែម";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "TOTAL_USAGE";
            this.dataGridViewTextBoxColumn7.HeaderText = "បរិមាណសរុប";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn8.DataPropertyName = "PAID_AMOUNT";
            this.dataGridViewTextBoxColumn8.HeaderText = "ប្រាក់បានបង់";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // PageGeneralSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 337);
            this.Controls.Add(this.roundPanel1);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "PageGeneralSetting";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.roundPanel1.ResumeLayout(false);
            this.grpACCOUNT_SETTING.ResumeLayout(false);
            this.grpACCOUNT_SETTING.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboDepositReceive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboDepositAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboPrepayAccount.Properties)).EndInit();
            this.grpJOURNAL.ResumeLayout(false);
            this.grpJOURNAL.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboJOURNAL_PREPAYMENT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTaxVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboJOURNAL_ADJUSTMENT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboJOURNAL_DEPOSIT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboJOURNAL_CASH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboJOURNAL_SALE.Properties)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private Dynamic.Component.RoundPanel roundPanel1;
        private System.Windows.Forms.GroupBox grpJOURNAL;
        private System.Windows.Forms.Label lblJOURNAL_SALE;
        private System.Windows.Forms.Label lblJOURNAL_CASH;
        private System.Windows.Forms.Label lblJOURNAL_DEPOSIT;
        private System.Windows.Forms.Label lblJOURNAL_ADJUSTMENT;
        private System.Windows.Forms.Label lblTax_VAT;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboTaxVAT;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboJOURNAL_ADJUSTMENT;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboJOURNAL_CASH;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboJOURNAL_SALE;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboJOURNAL_DEPOSIT;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboJOURNAL_PREPAYMENT;
        private System.Windows.Forms.Label lblJOURNAL_PREPAYMENT;
        private System.Windows.Forms.GroupBox grpACCOUNT_SETTING;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboPrepayAccount;
        private System.Windows.Forms.Label lblPrepay_Account;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboDepositAccount;
        private System.Windows.Forms.Label lblDeposit_Account;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboDepositReceive;
        private System.Windows.Forms.Label lblDeposit_Receive_Account;
    }
}
