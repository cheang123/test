﻿namespace EPower.Accounting.Interface.Setting
{
    partial class PageCurrency
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageCurrency));
            this.roundPanel1 = new Dynamic.Component.RoundPanel();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.dgvCurrenciesList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCURRENCY_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCURRENCY_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCURRENCY_SIGN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCURRENCY_CODE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFORMAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXTERNAL_CURRENCY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repExternalCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.colEXT_IC_ACC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXT_AR_ACC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRICE_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.roundPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrenciesList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repExternalCurrency)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // roundPanel1
            // 
            this.roundPanel1._BackColor = System.Drawing.Color.White;
            this.roundPanel1._Radius = 10;
            this.roundPanel1.BackColor = System.Drawing.Color.Transparent;
            this.roundPanel1.Controls.Add(this.dgv);
            this.roundPanel1.Controls.Add(this.panel1);
            resources.ApplyResources(this.roundPanel1, "roundPanel1");
            this.roundPanel1.Name = "roundPanel1";
            // 
            // dgv
            // 
            this.dgv.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EmbeddedNavigator.Margin = ((System.Windows.Forms.Padding)(resources.GetObject("dgv.EmbeddedNavigator.Margin")));
            this.dgv.MainView = this.dgvCurrenciesList;
            this.dgv.Name = "dgv";
            this.dgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repExternalCurrency});
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvCurrenciesList});
            // 
            // dgvCurrenciesList
            // 
            this.dgvCurrenciesList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCURRENCY_ID,
            this.colCURRENCY_NAME,
            this.colCURRENCY_SIGN,
            this.colCURRENCY_CODE,
            this.colFORMAT,
            this.colEXTERNAL_CURRENCY});
            this.dgvCurrenciesList.DetailHeight = 228;
            this.dgvCurrenciesList.GridControl = this.dgv;
            this.dgvCurrenciesList.Name = "dgvCurrenciesList";
            this.dgvCurrenciesList.OptionsDetail.EnableMasterViewMode = false;
            this.dgvCurrenciesList.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent;
            this.dgvCurrenciesList.OptionsView.ShowGroupPanel = false;
            this.dgvCurrenciesList.OptionsView.ShowIndicator = false;
            this.dgvCurrenciesList.OptionsView.WaitAnimationOptions = DevExpress.XtraEditors.WaitAnimationOptions.Panel;
            // 
            // colCURRENCY_ID
            // 
            resources.ApplyResources(this.colCURRENCY_ID, "colCURRENCY_ID");
            this.colCURRENCY_ID.FieldName = "CURRENCY_ID";
            this.colCURRENCY_ID.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("colCURRENCY_ID.ImageOptions.Alignment")));
            this.colCURRENCY_ID.MinWidth = 15;
            this.colCURRENCY_ID.Name = "colCURRENCY_ID";
            this.colCURRENCY_ID.OptionsColumn.ReadOnly = true;
            // 
            // colCURRENCY_NAME
            // 
            resources.ApplyResources(this.colCURRENCY_NAME, "colCURRENCY_NAME");
            this.colCURRENCY_NAME.FieldName = "CURRENCY_NAME";
            this.colCURRENCY_NAME.MinWidth = 15;
            this.colCURRENCY_NAME.Name = "colCURRENCY_NAME";
            this.colCURRENCY_NAME.OptionsColumn.AllowEdit = false;
            this.colCURRENCY_NAME.OptionsColumn.ReadOnly = true;
            // 
            // colCURRENCY_SIGN
            // 
            resources.ApplyResources(this.colCURRENCY_SIGN, "colCURRENCY_SIGN");
            this.colCURRENCY_SIGN.FieldName = "CURRENCY_SING";
            this.colCURRENCY_SIGN.MinWidth = 15;
            this.colCURRENCY_SIGN.Name = "colCURRENCY_SIGN";
            this.colCURRENCY_SIGN.OptionsColumn.AllowEdit = false;
            this.colCURRENCY_SIGN.OptionsColumn.ReadOnly = true;
            // 
            // colCURRENCY_CODE
            // 
            resources.ApplyResources(this.colCURRENCY_CODE, "colCURRENCY_CODE");
            this.colCURRENCY_CODE.FieldName = "CURRENCY_CODE";
            this.colCURRENCY_CODE.ImageOptions.Alignment = ((System.Drawing.StringAlignment)(resources.GetObject("colCURRENCY_CODE.ImageOptions.Alignment")));
            this.colCURRENCY_CODE.MinWidth = 15;
            this.colCURRENCY_CODE.Name = "colCURRENCY_CODE";
            this.colCURRENCY_CODE.OptionsColumn.AllowEdit = false;
            this.colCURRENCY_CODE.OptionsColumn.ReadOnly = true;
            // 
            // colFORMAT
            // 
            resources.ApplyResources(this.colFORMAT, "colFORMAT");
            this.colFORMAT.FieldName = "FORMAT";
            this.colFORMAT.MinWidth = 15;
            this.colFORMAT.Name = "colFORMAT";
            // 
            // colEXTERNAL_CURRENCY
            // 
            resources.ApplyResources(this.colEXTERNAL_CURRENCY, "colEXTERNAL_CURRENCY");
            this.colEXTERNAL_CURRENCY.ColumnEdit = this.repExternalCurrency;
            this.colEXTERNAL_CURRENCY.FieldName = "EXTERNAL_CURRENCY_ID";
            this.colEXTERNAL_CURRENCY.MinWidth = 15;
            this.colEXTERNAL_CURRENCY.Name = "colEXTERNAL_CURRENCY";
            // 
            // repExternalCurrency
            // 
            resources.ApplyResources(this.repExternalCurrency, "repExternalCurrency");
            this.repExternalCurrency.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("repExternalCurrency.Buttons"))))});
            this.repExternalCurrency.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repExternalCurrency.Columns"), resources.GetString("repExternalCurrency.Columns1"), ((int)(resources.GetObject("repExternalCurrency.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repExternalCurrency.Columns3"))), resources.GetString("repExternalCurrency.Columns4"), ((bool)(resources.GetObject("repExternalCurrency.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repExternalCurrency.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repExternalCurrency.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repExternalCurrency.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("repExternalCurrency.Columns9"), resources.GetString("repExternalCurrency.Columns10"), ((int)(resources.GetObject("repExternalCurrency.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("repExternalCurrency.Columns12"))), resources.GetString("repExternalCurrency.Columns13"), ((bool)(resources.GetObject("repExternalCurrency.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("repExternalCurrency.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("repExternalCurrency.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("repExternalCurrency.Columns17"))))});
            this.repExternalCurrency.DisplayMember = "Code";
            this.repExternalCurrency.Name = "repExternalCurrency";
            this.repExternalCurrency.PopupWidth = 375;
            this.repExternalCurrency.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.repExternalCurrency.ValueMember = "Id";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.searchControl);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // searchControl
            // 
            this.searchControl.Client = this.dgv;
            resources.ApplyResources(this.searchControl, "searchControl");
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.Appearance.Font")));
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.Client = this.dgv;
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            // 
            // colEXT_IC_ACC
            // 
            resources.ApplyResources(this.colEXT_IC_ACC, "colEXT_IC_ACC");
            this.colEXT_IC_ACC.FieldName = "EXT_IC_ACC_ID";
            this.colEXT_IC_ACC.Name = "colEXT_IC_ACC";
            // 
            // colEXT_AR_ACC
            // 
            resources.ApplyResources(this.colEXT_AR_ACC, "colEXT_AR_ACC");
            this.colEXT_AR_ACC.FieldName = "EXT_AR_ACC_ID";
            this.colEXT_AR_ACC.Name = "colEXT_AR_ACC";
            // 
            // colPRICE_ID
            // 
            resources.ApplyResources(this.colPRICE_ID, "colPRICE_ID");
            this.colPRICE_ID.FieldName = "PRICE_ID";
            this.colPRICE_ID.Name = "colPRICE_ID";
            // 
            // PageCurrency
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.roundPanel1);
            this.Name = "PageCurrency";
            this.roundPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCurrenciesList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repExternalCurrency)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Dynamic.Component.RoundPanel roundPanel1;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvCurrenciesList;
        private DevExpress.XtraGrid.Columns.GridColumn colCURRENCY_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colCURRENCY_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn colCURRENCY_SIGN;
        private DevExpress.XtraGrid.Columns.GridColumn colCURRENCY_CODE;
        private DevExpress.XtraGrid.Columns.GridColumn colEXTERNAL_CURRENCY;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repExternalCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colEXT_IC_ACC;
        private DevExpress.XtraGrid.Columns.GridColumn colEXT_AR_ACC;
        private DevExpress.XtraGrid.Columns.GridColumn colPRICE_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colFORMAT;
        private DevExpress.XtraEditors.SearchControl searchControl;
    }
}
