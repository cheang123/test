﻿namespace EPower.Accounting.Interface.Setting
{
    partial class DialogSetting
	{
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogSetting));
            this.main = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSAVE = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblACCOUNTING_2 = new System.Windows.Forms.Label();
            this.btnSETTING = new SoftTech.Component.ExMenuItem();
            this.btnCURRENCY = new SoftTech.Component.ExMenuItem();
            this.btnCUSTOMER_TYPE = new SoftTech.Component.ExMenuItem();
            this.btnINVOICE_ITEM = new SoftTech.Component.ExMenuItem();
            this.btnPAYMENT_CONFIG = new SoftTech.Component.ExMenuItem();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnLogout = new System.Windows.Forms.PictureBox();
            this.btnCOMPANY = new System.Windows.Forms.PictureBox();
            this.btnCOMPANY_SETTING = new System.Windows.Forms.PictureBox();
            this.lblSETTING = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnLogout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCOMPANY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCOMPANY_SETTING)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.main);
            this.content.Controls.Add(this.panel5);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.panel2);
            this.content.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.content.Size = new System.Drawing.Size(1144, 620);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.panel5, 0);
            this.content.Controls.SetChildIndex(this.main, 0);
            // 
            // main
            // 
            this.main.BackColor = System.Drawing.Color.WhiteSmoke;
            this.main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main.Location = new System.Drawing.Point(205, 30);
            this.main.Name = "main";
            this.main.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.main.Size = new System.Drawing.Size(938, 557);
            this.main.TabIndex = 81;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSAVE);
            this.panel1.Controls.Add(this.btnCLOSE);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(205, 587);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(938, 32);
            this.panel1.TabIndex = 79;
            // 
            // btnSAVE
            // 
            this.btnSAVE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSAVE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnSAVE.Location = new System.Drawing.Point(782, 3);
            this.btnSAVE.Margin = new System.Windows.Forms.Padding(2);
            this.btnSAVE.Name = "btnSAVE";
            this.btnSAVE.Size = new System.Drawing.Size(71, 23);
            this.btnSAVE.TabIndex = 4;
            this.btnSAVE.Text = "រក្សាទុក";
            this.btnSAVE.UseVisualStyleBackColor = true;
            this.btnSAVE.Click += new System.EventHandler(this.btnSAVE_Click);
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(861, 4);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(2);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(71, 23);
            this.btnCLOSE.TabIndex = 3;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(1, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(204, 619);
            this.panel2.TabIndex = 57;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.flowLayoutPanel1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(204, 619);
            this.panel3.TabIndex = 6;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.LightGray;
            this.flowLayoutPanel1.Controls.Add(this.lblACCOUNTING_2);
            this.flowLayoutPanel1.Controls.Add(this.btnSETTING);
            this.flowLayoutPanel1.Controls.Add(this.btnCURRENCY);
            this.flowLayoutPanel1.Controls.Add(this.btnCUSTOMER_TYPE);
            this.flowLayoutPanel1.Controls.Add(this.btnINVOICE_ITEM);
            this.flowLayoutPanel1.Controls.Add(this.btnPAYMENT_CONFIG);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(204, 619);
            this.flowLayoutPanel1.TabIndex = 6;
            // 
            // lblACCOUNTING_2
            // 
            this.lblACCOUNTING_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblACCOUNTING_2.BackColor = System.Drawing.Color.Silver;
            this.lblACCOUNTING_2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblACCOUNTING_2.Location = new System.Drawing.Point(0, 0);
            this.lblACCOUNTING_2.Margin = new System.Windows.Forms.Padding(0);
            this.lblACCOUNTING_2.Name = "lblACCOUNTING_2";
            this.lblACCOUNTING_2.Size = new System.Drawing.Size(224, 24);
            this.lblACCOUNTING_2.TabIndex = 5;
            this.lblACCOUNTING_2.Text = "គណនេយ្យ";
            this.lblACCOUNTING_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblACCOUNTING_2.Visible = false;
            // 
            // btnSETTING
            // 
            this.btnSETTING.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSETTING.BackColor = System.Drawing.Color.Transparent;
            this.btnSETTING.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnSETTING.Image = ((System.Drawing.Image)(resources.GetObject("btnSETTING.Image")));
            this.btnSETTING.Location = new System.Drawing.Point(0, 24);
            this.btnSETTING.Margin = new System.Windows.Forms.Padding(0);
            this.btnSETTING.Name = "btnSETTING";
            this.btnSETTING.Selected = false;
            this.btnSETTING.Size = new System.Drawing.Size(203, 24);
            this.btnSETTING.TabIndex = 3;
            this.btnSETTING.Text = "ការកំណត់";
            this.btnSETTING.Click += new System.EventHandler(this.btnPRODUCTION_SETTING_Click);
            // 
            // btnCURRENCY
            // 
            this.btnCURRENCY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCURRENCY.BackColor = System.Drawing.Color.Transparent;
            this.btnCURRENCY.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCURRENCY.Image = ((System.Drawing.Image)(resources.GetObject("btnCURRENCY.Image")));
            this.btnCURRENCY.Location = new System.Drawing.Point(0, 48);
            this.btnCURRENCY.Margin = new System.Windows.Forms.Padding(0);
            this.btnCURRENCY.Name = "btnCURRENCY";
            this.btnCURRENCY.Selected = false;
            this.btnCURRENCY.Size = new System.Drawing.Size(203, 24);
            this.btnCURRENCY.TabIndex = 29;
            this.btnCURRENCY.Text = "រូបិយប័ណ្ណ";
            this.btnCURRENCY.Click += new System.EventHandler(this.btnCURRENCY_Click);
            // 
            // btnCUSTOMER_TYPE
            // 
            this.btnCUSTOMER_TYPE.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCUSTOMER_TYPE.BackColor = System.Drawing.Color.Transparent;
            this.btnCUSTOMER_TYPE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCUSTOMER_TYPE.Image = ((System.Drawing.Image)(resources.GetObject("btnCUSTOMER_TYPE.Image")));
            this.btnCUSTOMER_TYPE.Location = new System.Drawing.Point(0, 72);
            this.btnCUSTOMER_TYPE.Margin = new System.Windows.Forms.Padding(0);
            this.btnCUSTOMER_TYPE.Name = "btnCUSTOMER_TYPE";
            this.btnCUSTOMER_TYPE.Selected = false;
            this.btnCUSTOMER_TYPE.Size = new System.Drawing.Size(203, 24);
            this.btnCUSTOMER_TYPE.TabIndex = 4;
            this.btnCUSTOMER_TYPE.Text = "ប្រភេទប្រើប្រាស់";
            this.btnCUSTOMER_TYPE.Click += new System.EventHandler(this.btnCUSTOMER_TYPE_Click);
            // 
            // btnINVOICE_ITEM
            // 
            this.btnINVOICE_ITEM.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnINVOICE_ITEM.BackColor = System.Drawing.Color.Transparent;
            this.btnINVOICE_ITEM.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnINVOICE_ITEM.Image = ((System.Drawing.Image)(resources.GetObject("btnINVOICE_ITEM.Image")));
            this.btnINVOICE_ITEM.Location = new System.Drawing.Point(0, 96);
            this.btnINVOICE_ITEM.Margin = new System.Windows.Forms.Padding(0);
            this.btnINVOICE_ITEM.Name = "btnINVOICE_ITEM";
            this.btnINVOICE_ITEM.Selected = false;
            this.btnINVOICE_ITEM.Size = new System.Drawing.Size(203, 24);
            this.btnINVOICE_ITEM.TabIndex = 17;
            this.btnINVOICE_ITEM.Text = "សេវ៉ាកម្ម";
            this.btnINVOICE_ITEM.Click += new System.EventHandler(this.btnINVOICE_ITEM_Click);
            // 
            // btnPAYMENT_CONFIG
            // 
            this.btnPAYMENT_CONFIG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPAYMENT_CONFIG.BackColor = System.Drawing.Color.Transparent;
            this.btnPAYMENT_CONFIG.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnPAYMENT_CONFIG.Image = ((System.Drawing.Image)(resources.GetObject("btnPAYMENT_CONFIG.Image")));
            this.btnPAYMENT_CONFIG.Location = new System.Drawing.Point(0, 120);
            this.btnPAYMENT_CONFIG.Margin = new System.Windows.Forms.Padding(0);
            this.btnPAYMENT_CONFIG.Name = "btnPAYMENT_CONFIG";
            this.btnPAYMENT_CONFIG.Selected = false;
            this.btnPAYMENT_CONFIG.Size = new System.Drawing.Size(203, 24);
            this.btnPAYMENT_CONFIG.TabIndex = 18;
            this.btnPAYMENT_CONFIG.Text = "ការទទួលប្រាក់";
            this.btnPAYMENT_CONFIG.Visible = false;
            this.btnPAYMENT_CONFIG.Click += new System.EventHandler(this.btnPAYMENT_CONFIG_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnLogout);
            this.panel5.Controls.Add(this.btnCOMPANY);
            this.panel5.Controls.Add(this.btnCOMPANY_SETTING);
            this.panel5.Controls.Add(this.lblSETTING);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(205, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(938, 30);
            this.panel5.TabIndex = 80;
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogout.Image = global::EPower.Accounting.Properties.Resources.icons8_change_user_306;
            this.btnLogout.Location = new System.Drawing.Point(903, 2);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(30, 30);
            this.btnLogout.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btnLogout.TabIndex = 81;
            this.btnLogout.TabStop = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnCOMPANY
            // 
            this.btnCOMPANY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCOMPANY.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCOMPANY.Image = global::EPower.Accounting.Properties.Resources.icons8_company_254;
            this.btnCOMPANY.Location = new System.Drawing.Point(835, 2);
            this.btnCOMPANY.Name = "btnCOMPANY";
            this.btnCOMPANY.Size = new System.Drawing.Size(30, 30);
            this.btnCOMPANY.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btnCOMPANY.TabIndex = 79;
            this.btnCOMPANY.TabStop = false;
            this.btnCOMPANY.Click += new System.EventHandler(this.btnCOMPANY_Click);
            // 
            // btnCOMPANY_SETTING
            // 
            this.btnCOMPANY_SETTING.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCOMPANY_SETTING.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCOMPANY_SETTING.Image = global::EPower.Accounting.Properties.Resources.icons8_settings_256;
            this.btnCOMPANY_SETTING.Location = new System.Drawing.Point(869, 2);
            this.btnCOMPANY_SETTING.Name = "btnCOMPANY_SETTING";
            this.btnCOMPANY_SETTING.Size = new System.Drawing.Size(30, 30);
            this.btnCOMPANY_SETTING.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btnCOMPANY_SETTING.TabIndex = 80;
            this.btnCOMPANY_SETTING.TabStop = false;
            this.btnCOMPANY_SETTING.Click += new System.EventHandler(this.btnCOMPANY_SETTING_Click);
            // 
            // lblSETTING
            // 
            this.lblSETTING.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSETTING.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblSETTING.Location = new System.Drawing.Point(0, 0);
            this.lblSETTING.Name = "lblSETTING";
            this.lblSETTING.Size = new System.Drawing.Size(938, 30);
            this.lblSETTING.TabIndex = 78;
            this.lblSETTING.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DialogSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1144, 643);
            this.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Name = "DialogSetting";
            this.Text = "ការកំណត់ទូទៅ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DialogSetting_FormClosing);
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnLogout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCOMPANY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCOMPANY_SETTING)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel main;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label lblACCOUNTING_2;
        private SoftTech.Component.ExMenuItem btnSETTING;
        private SoftTech.Component.ExMenuItem btnCURRENCY;
        private SoftTech.Component.ExMenuItem btnCUSTOMER_TYPE;
        private SoftTech.Component.ExMenuItem btnINVOICE_ITEM;
        private SoftTech.Component.ExMenuItem btnPAYMENT_CONFIG;
        private System.Windows.Forms.Panel panel1;
        private SoftTech.Component.ExButton btnSAVE;
        private SoftTech.Component.ExButton btnCLOSE;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox btnLogout;
        private System.Windows.Forms.PictureBox btnCOMPANY;
        private System.Windows.Forms.PictureBox btnCOMPANY_SETTING;
        private System.Windows.Forms.Label lblSETTING;
    }
}