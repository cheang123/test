﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Accounting.Interface
{
    partial class NavAccounting
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NavAccounting));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblACCOUNTING_2 = new System.Windows.Forms.Label();
            this.btnACCOUNT = new SoftTech.Component.ExMenuItem();
            this.btnDAILY_TRANSACTION = new SoftTech.Component.ExMenuItem();
            this.btnTRANSACTION = new SoftTech.Component.ExMenuItem();
            this.btnJOURNAL_ENTRY = new SoftTech.Component.ExMenuItem();
            this.btnOPENING_BALANCE = new SoftTech.Component.ExMenuItem();
            this.btnGAIN_LOSS_EXCHANGE_RATE = new SoftTech.Component.ExMenuItem();
            this.btnTRANSFER = new SoftTech.Component.ExMenuItem();
            this.btnCASH_EXCHANGE = new SoftTech.Component.ExMenuItem();
            this.lblSALE = new System.Windows.Forms.Label();
            this.btnCUSTOMER = new SoftTech.Component.ExMenuItem();
            this.btnINVOICE_SALE = new SoftTech.Component.ExMenuItem();
            this.btnPAYMENT = new SoftTech.Component.ExMenuItem();
            this.btnCREDIT_NOTE = new SoftTech.Component.ExMenuItem();
            this.lblPURCHASE = new System.Windows.Forms.Label();
            this.btnVENDOR = new SoftTech.Component.ExMenuItem();
            this.btnPURCHASE_ORDER = new SoftTech.Component.ExMenuItem();
            this.btnINVOICE_PURCHASE = new SoftTech.Component.ExMenuItem();
            this.btnPURCHASE_PAYMENT = new SoftTech.Component.ExMenuItem();
            this.lblFIXED_ASSET = new System.Windows.Forms.Label();
            this.btnFIXED_ASSET = new SoftTech.Component.ExMenuItem();
            this.btnASSET_TYPE = new SoftTech.Component.ExMenuItem();
            this.lblINVENTORY = new System.Windows.Forms.Label();
            this.btnSTOCK_IN = new SoftTech.Component.ExMenuItem();
            this.btnSTOCK_OUT = new SoftTech.Component.ExMenuItem();
            this.btnPRODUCT_CATEGORY = new SoftTech.Component.ExMenuItem();
            this.btnPRODUCT = new SoftTech.Component.ExMenuItem();
            this.btnUNIT = new SoftTech.Component.ExMenuItem();
            this.lblREPORTS = new System.Windows.Forms.Label();
            this.btnREPORT_ACCOUNTING = new SoftTech.Component.ExMenuItem();
            this.lblSETTING = new System.Windows.Forms.Label();
            this.btnSETTING = new SoftTech.Component.ExMenuItem();
            this.btnOPEN_POINTER = new SoftTech.Component.ExMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.lblACCOUNTING = new SoftTech.Component.ExTabItem();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.main = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.lblACCOUNTING_1 = new SoftTech.Component.ExTabItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel12);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel16);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel2);
            this.panel12.Controls.Add(this.lblACCOUNTING);
            resources.ApplyResources(this.panel12, "panel12");
            this.panel12.Name = "panel12";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.panel13);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.lblACCOUNTING_2);
            this.panel1.Controls.Add(this.btnACCOUNT);
            this.panel1.Controls.Add(this.btnDAILY_TRANSACTION);
            this.panel1.Controls.Add(this.btnTRANSACTION);
            this.panel1.Controls.Add(this.btnJOURNAL_ENTRY);
            this.panel1.Controls.Add(this.btnOPENING_BALANCE);
            this.panel1.Controls.Add(this.btnGAIN_LOSS_EXCHANGE_RATE);
            this.panel1.Controls.Add(this.btnTRANSFER);
            this.panel1.Controls.Add(this.btnCASH_EXCHANGE);
            this.panel1.Controls.Add(this.lblSALE);
            this.panel1.Controls.Add(this.btnCUSTOMER);
            this.panel1.Controls.Add(this.btnINVOICE_SALE);
            this.panel1.Controls.Add(this.btnPAYMENT);
            this.panel1.Controls.Add(this.btnCREDIT_NOTE);
            this.panel1.Controls.Add(this.lblPURCHASE);
            this.panel1.Controls.Add(this.btnVENDOR);
            this.panel1.Controls.Add(this.btnPURCHASE_ORDER);
            this.panel1.Controls.Add(this.btnINVOICE_PURCHASE);
            this.panel1.Controls.Add(this.btnPURCHASE_PAYMENT);
            this.panel1.Controls.Add(this.lblFIXED_ASSET);
            this.panel1.Controls.Add(this.btnFIXED_ASSET);
            this.panel1.Controls.Add(this.btnASSET_TYPE);
            this.panel1.Controls.Add(this.lblINVENTORY);
            this.panel1.Controls.Add(this.btnSTOCK_IN);
            this.panel1.Controls.Add(this.btnSTOCK_OUT);
            this.panel1.Controls.Add(this.btnPRODUCT_CATEGORY);
            this.panel1.Controls.Add(this.btnPRODUCT);
            this.panel1.Controls.Add(this.btnUNIT);
            this.panel1.Controls.Add(this.lblREPORTS);
            this.panel1.Controls.Add(this.btnREPORT_ACCOUNTING);
            this.panel1.Controls.Add(this.lblSETTING);
            this.panel1.Controls.Add(this.btnSETTING);
            this.panel1.Controls.Add(this.btnOPEN_POINTER);
            this.panel1.Name = "panel1";
            // 
            // lblACCOUNTING_2
            // 
            resources.ApplyResources(this.lblACCOUNTING_2, "lblACCOUNTING_2");
            this.lblACCOUNTING_2.BackColor = System.Drawing.Color.Silver;
            this.lblACCOUNTING_2.Name = "lblACCOUNTING_2";
            // 
            // btnACCOUNT
            // 
            resources.ApplyResources(this.btnACCOUNT, "btnACCOUNT");
            this.btnACCOUNT.BackColor = System.Drawing.Color.Transparent;
            this.btnACCOUNT.Image = ((System.Drawing.Image)(resources.GetObject("btnACCOUNT.Image")));
            this.btnACCOUNT.Name = "btnACCOUNT";
            this.btnACCOUNT.Selected = false;
            this.btnACCOUNT.Click += new System.EventHandler(this.btnACCOUNT_Click);
            // 
            // btnDAILY_TRANSACTION
            // 
            resources.ApplyResources(this.btnDAILY_TRANSACTION, "btnDAILY_TRANSACTION");
            this.btnDAILY_TRANSACTION.BackColor = System.Drawing.Color.Transparent;
            this.btnDAILY_TRANSACTION.Image = ((System.Drawing.Image)(resources.GetObject("btnDAILY_TRANSACTION.Image")));
            this.btnDAILY_TRANSACTION.Name = "btnDAILY_TRANSACTION";
            this.btnDAILY_TRANSACTION.Selected = false;
            this.btnDAILY_TRANSACTION.Click += new System.EventHandler(this.btnDAILY_TRANSACTION_Click);
            // 
            // btnTRANSACTION
            // 
            resources.ApplyResources(this.btnTRANSACTION, "btnTRANSACTION");
            this.btnTRANSACTION.BackColor = System.Drawing.Color.Transparent;
            this.btnTRANSACTION.Image = ((System.Drawing.Image)(resources.GetObject("btnTRANSACTION.Image")));
            this.btnTRANSACTION.Name = "btnTRANSACTION";
            this.btnTRANSACTION.Selected = false;
            this.btnTRANSACTION.Click += new System.EventHandler(this.btnTRANSACTION_Click);
            // 
            // btnJOURNAL_ENTRY
            // 
            resources.ApplyResources(this.btnJOURNAL_ENTRY, "btnJOURNAL_ENTRY");
            this.btnJOURNAL_ENTRY.BackColor = System.Drawing.Color.Transparent;
            this.btnJOURNAL_ENTRY.Image = ((System.Drawing.Image)(resources.GetObject("btnJOURNAL_ENTRY.Image")));
            this.btnJOURNAL_ENTRY.Name = "btnJOURNAL_ENTRY";
            this.btnJOURNAL_ENTRY.Selected = false;
            this.btnJOURNAL_ENTRY.Click += new System.EventHandler(this.btnJOURNAL_ENTRY_Click);
            // 
            // btnOPENING_BALANCE
            // 
            resources.ApplyResources(this.btnOPENING_BALANCE, "btnOPENING_BALANCE");
            this.btnOPENING_BALANCE.BackColor = System.Drawing.Color.Transparent;
            this.btnOPENING_BALANCE.Image = ((System.Drawing.Image)(resources.GetObject("btnOPENING_BALANCE.Image")));
            this.btnOPENING_BALANCE.Name = "btnOPENING_BALANCE";
            this.btnOPENING_BALANCE.Selected = false;
            this.btnOPENING_BALANCE.Click += new System.EventHandler(this.btnOPENING_BALANCE_Click);
            // 
            // btnGAIN_LOSS_EXCHANGE_RATE
            // 
            resources.ApplyResources(this.btnGAIN_LOSS_EXCHANGE_RATE, "btnGAIN_LOSS_EXCHANGE_RATE");
            this.btnGAIN_LOSS_EXCHANGE_RATE.BackColor = System.Drawing.Color.Transparent;
            this.btnGAIN_LOSS_EXCHANGE_RATE.Image = ((System.Drawing.Image)(resources.GetObject("btnGAIN_LOSS_EXCHANGE_RATE.Image")));
            this.btnGAIN_LOSS_EXCHANGE_RATE.Name = "btnGAIN_LOSS_EXCHANGE_RATE";
            this.btnGAIN_LOSS_EXCHANGE_RATE.Selected = false;
            this.btnGAIN_LOSS_EXCHANGE_RATE.Click += new System.EventHandler(this.btnGAIN_LOSS_EXCHANGE_RATE_Click);
            // 
            // btnTRANSFER
            // 
            resources.ApplyResources(this.btnTRANSFER, "btnTRANSFER");
            this.btnTRANSFER.BackColor = System.Drawing.Color.Transparent;
            this.btnTRANSFER.Image = ((System.Drawing.Image)(resources.GetObject("btnTRANSFER.Image")));
            this.btnTRANSFER.Name = "btnTRANSFER";
            this.btnTRANSFER.Selected = false;
            this.btnTRANSFER.Click += new System.EventHandler(this.btnTRANSFER_Click);
            // 
            // btnCASH_EXCHANGE
            // 
            resources.ApplyResources(this.btnCASH_EXCHANGE, "btnCASH_EXCHANGE");
            this.btnCASH_EXCHANGE.BackColor = System.Drawing.Color.Transparent;
            this.btnCASH_EXCHANGE.Image = ((System.Drawing.Image)(resources.GetObject("btnCASH_EXCHANGE.Image")));
            this.btnCASH_EXCHANGE.Name = "btnCASH_EXCHANGE";
            this.btnCASH_EXCHANGE.Selected = false;
            this.btnCASH_EXCHANGE.Click += new System.EventHandler(this.btnCASH_EXCHANGE_Click);
            // 
            // lblSALE
            // 
            resources.ApplyResources(this.lblSALE, "lblSALE");
            this.lblSALE.BackColor = System.Drawing.Color.Silver;
            this.lblSALE.Name = "lblSALE";
            // 
            // btnCUSTOMER
            // 
            resources.ApplyResources(this.btnCUSTOMER, "btnCUSTOMER");
            this.btnCUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.btnCUSTOMER.Image = ((System.Drawing.Image)(resources.GetObject("btnCUSTOMER.Image")));
            this.btnCUSTOMER.Name = "btnCUSTOMER";
            this.btnCUSTOMER.Selected = false;
            this.btnCUSTOMER.Click += new System.EventHandler(this.btnCUSTOMER_Click);
            // 
            // btnINVOICE_SALE
            // 
            resources.ApplyResources(this.btnINVOICE_SALE, "btnINVOICE_SALE");
            this.btnINVOICE_SALE.BackColor = System.Drawing.Color.Transparent;
            this.btnINVOICE_SALE.Image = ((System.Drawing.Image)(resources.GetObject("btnINVOICE_SALE.Image")));
            this.btnINVOICE_SALE.Name = "btnINVOICE_SALE";
            this.btnINVOICE_SALE.Selected = false;
            this.btnINVOICE_SALE.Click += new System.EventHandler(this.btnINVOICE_SALE_Click);
            // 
            // btnPAYMENT
            // 
            resources.ApplyResources(this.btnPAYMENT, "btnPAYMENT");
            this.btnPAYMENT.BackColor = System.Drawing.Color.Transparent;
            this.btnPAYMENT.Image = ((System.Drawing.Image)(resources.GetObject("btnPAYMENT.Image")));
            this.btnPAYMENT.Name = "btnPAYMENT";
            this.btnPAYMENT.Selected = false;
            this.btnPAYMENT.Click += new System.EventHandler(this.btnPAYMENT_Click);
            // 
            // btnCREDIT_NOTE
            // 
            resources.ApplyResources(this.btnCREDIT_NOTE, "btnCREDIT_NOTE");
            this.btnCREDIT_NOTE.BackColor = System.Drawing.Color.Transparent;
            this.btnCREDIT_NOTE.Image = ((System.Drawing.Image)(resources.GetObject("btnCREDIT_NOTE.Image")));
            this.btnCREDIT_NOTE.Name = "btnCREDIT_NOTE";
            this.btnCREDIT_NOTE.Selected = false;
            this.btnCREDIT_NOTE.Click += new System.EventHandler(this.btnCREDIT_NOTE_Click);
            // 
            // lblPURCHASE
            // 
            resources.ApplyResources(this.lblPURCHASE, "lblPURCHASE");
            this.lblPURCHASE.BackColor = System.Drawing.Color.Silver;
            this.lblPURCHASE.Name = "lblPURCHASE";
            // 
            // btnVENDOR
            // 
            resources.ApplyResources(this.btnVENDOR, "btnVENDOR");
            this.btnVENDOR.BackColor = System.Drawing.Color.Transparent;
            this.btnVENDOR.Image = ((System.Drawing.Image)(resources.GetObject("btnVENDOR.Image")));
            this.btnVENDOR.Name = "btnVENDOR";
            this.btnVENDOR.Selected = false;
            this.btnVENDOR.Click += new System.EventHandler(this.btnVENDOR_Click);
            // 
            // btnPURCHASE_ORDER
            // 
            resources.ApplyResources(this.btnPURCHASE_ORDER, "btnPURCHASE_ORDER");
            this.btnPURCHASE_ORDER.BackColor = System.Drawing.Color.Transparent;
            this.btnPURCHASE_ORDER.Image = ((System.Drawing.Image)(resources.GetObject("btnPURCHASE_ORDER.Image")));
            this.btnPURCHASE_ORDER.Name = "btnPURCHASE_ORDER";
            this.btnPURCHASE_ORDER.Selected = false;
            this.btnPURCHASE_ORDER.Click += new System.EventHandler(this.btnPURCHASE_ORDER_Click);
            // 
            // btnINVOICE_PURCHASE
            // 
            resources.ApplyResources(this.btnINVOICE_PURCHASE, "btnINVOICE_PURCHASE");
            this.btnINVOICE_PURCHASE.BackColor = System.Drawing.Color.Transparent;
            this.btnINVOICE_PURCHASE.Image = ((System.Drawing.Image)(resources.GetObject("btnINVOICE_PURCHASE.Image")));
            this.btnINVOICE_PURCHASE.Name = "btnINVOICE_PURCHASE";
            this.btnINVOICE_PURCHASE.Selected = false;
            this.btnINVOICE_PURCHASE.Click += new System.EventHandler(this.btnINVOICE_PURCHASE_Click);
            // 
            // btnPURCHASE_PAYMENT
            // 
            resources.ApplyResources(this.btnPURCHASE_PAYMENT, "btnPURCHASE_PAYMENT");
            this.btnPURCHASE_PAYMENT.BackColor = System.Drawing.Color.Transparent;
            this.btnPURCHASE_PAYMENT.Image = ((System.Drawing.Image)(resources.GetObject("btnPURCHASE_PAYMENT.Image")));
            this.btnPURCHASE_PAYMENT.Name = "btnPURCHASE_PAYMENT";
            this.btnPURCHASE_PAYMENT.Selected = false;
            this.btnPURCHASE_PAYMENT.Click += new System.EventHandler(this.btnPURCHASE_PAYMENT_Click);
            // 
            // lblFIXED_ASSET
            // 
            resources.ApplyResources(this.lblFIXED_ASSET, "lblFIXED_ASSET");
            this.lblFIXED_ASSET.BackColor = System.Drawing.Color.Silver;
            this.lblFIXED_ASSET.Name = "lblFIXED_ASSET";
            // 
            // btnFIXED_ASSET
            // 
            resources.ApplyResources(this.btnFIXED_ASSET, "btnFIXED_ASSET");
            this.btnFIXED_ASSET.BackColor = System.Drawing.Color.Transparent;
            this.btnFIXED_ASSET.Image = ((System.Drawing.Image)(resources.GetObject("btnFIXED_ASSET.Image")));
            this.btnFIXED_ASSET.Name = "btnFIXED_ASSET";
            this.btnFIXED_ASSET.Selected = false;
            this.btnFIXED_ASSET.Click += new System.EventHandler(this.btnFIXED_ASSET_Click);
            // 
            // btnASSET_TYPE
            // 
            resources.ApplyResources(this.btnASSET_TYPE, "btnASSET_TYPE");
            this.btnASSET_TYPE.BackColor = System.Drawing.Color.Transparent;
            this.btnASSET_TYPE.Image = ((System.Drawing.Image)(resources.GetObject("btnASSET_TYPE.Image")));
            this.btnASSET_TYPE.Name = "btnASSET_TYPE";
            this.btnASSET_TYPE.Selected = false;
            this.btnASSET_TYPE.Click += new System.EventHandler(this.btnASSET_TYPE_Click);
            // 
            // lblINVENTORY
            // 
            resources.ApplyResources(this.lblINVENTORY, "lblINVENTORY");
            this.lblINVENTORY.BackColor = System.Drawing.Color.Silver;
            this.lblINVENTORY.Name = "lblINVENTORY";
            // 
            // btnSTOCK_IN
            // 
            resources.ApplyResources(this.btnSTOCK_IN, "btnSTOCK_IN");
            this.btnSTOCK_IN.BackColor = System.Drawing.Color.Transparent;
            this.btnSTOCK_IN.Image = ((System.Drawing.Image)(resources.GetObject("btnSTOCK_IN.Image")));
            this.btnSTOCK_IN.Name = "btnSTOCK_IN";
            this.btnSTOCK_IN.Selected = false;
            this.btnSTOCK_IN.Click += new System.EventHandler(this.btnSTOCK_IN_Click);
            // 
            // btnSTOCK_OUT
            // 
            resources.ApplyResources(this.btnSTOCK_OUT, "btnSTOCK_OUT");
            this.btnSTOCK_OUT.BackColor = System.Drawing.Color.Transparent;
            this.btnSTOCK_OUT.Image = ((System.Drawing.Image)(resources.GetObject("btnSTOCK_OUT.Image")));
            this.btnSTOCK_OUT.Name = "btnSTOCK_OUT";
            this.btnSTOCK_OUT.Selected = false;
            this.btnSTOCK_OUT.Click += new System.EventHandler(this.btnSTOCK_OUT_Click);
            // 
            // btnPRODUCT_CATEGORY
            // 
            resources.ApplyResources(this.btnPRODUCT_CATEGORY, "btnPRODUCT_CATEGORY");
            this.btnPRODUCT_CATEGORY.BackColor = System.Drawing.Color.Transparent;
            this.btnPRODUCT_CATEGORY.Image = ((System.Drawing.Image)(resources.GetObject("btnPRODUCT_CATEGORY.Image")));
            this.btnPRODUCT_CATEGORY.Name = "btnPRODUCT_CATEGORY";
            this.btnPRODUCT_CATEGORY.Selected = false;
            this.btnPRODUCT_CATEGORY.Click += new System.EventHandler(this.btnPRODUCT_CATEGORY_Click);
            // 
            // btnPRODUCT
            // 
            resources.ApplyResources(this.btnPRODUCT, "btnPRODUCT");
            this.btnPRODUCT.BackColor = System.Drawing.Color.Transparent;
            this.btnPRODUCT.Image = ((System.Drawing.Image)(resources.GetObject("btnPRODUCT.Image")));
            this.btnPRODUCT.Name = "btnPRODUCT";
            this.btnPRODUCT.Selected = false;
            this.btnPRODUCT.Click += new System.EventHandler(this.btnPRODUCT_Click);
            // 
            // btnUNIT
            // 
            resources.ApplyResources(this.btnUNIT, "btnUNIT");
            this.btnUNIT.BackColor = System.Drawing.Color.Transparent;
            this.btnUNIT.Image = ((System.Drawing.Image)(resources.GetObject("btnUNIT.Image")));
            this.btnUNIT.Name = "btnUNIT";
            this.btnUNIT.Selected = false;
            this.btnUNIT.Click += new System.EventHandler(this.btnUNIT_Click);
            // 
            // lblREPORTS
            // 
            resources.ApplyResources(this.lblREPORTS, "lblREPORTS");
            this.lblREPORTS.BackColor = System.Drawing.Color.Silver;
            this.lblREPORTS.Name = "lblREPORTS";
            // 
            // btnREPORT_ACCOUNTING
            // 
            resources.ApplyResources(this.btnREPORT_ACCOUNTING, "btnREPORT_ACCOUNTING");
            this.btnREPORT_ACCOUNTING.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_ACCOUNTING.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_ACCOUNTING.Image")));
            this.btnREPORT_ACCOUNTING.Name = "btnREPORT_ACCOUNTING";
            this.btnREPORT_ACCOUNTING.Selected = false;
            this.btnREPORT_ACCOUNTING.Click += new System.EventHandler(this.btnREPORT_ACCOUNTING_Click);
            // 
            // lblSETTING
            // 
            resources.ApplyResources(this.lblSETTING, "lblSETTING");
            this.lblSETTING.BackColor = System.Drawing.Color.Silver;
            this.lblSETTING.Name = "lblSETTING";
            // 
            // btnSETTING
            // 
            resources.ApplyResources(this.btnSETTING, "btnSETTING");
            this.btnSETTING.BackColor = System.Drawing.Color.Transparent;
            this.btnSETTING.Image = ((System.Drawing.Image)(resources.GetObject("btnSETTING.Image")));
            this.btnSETTING.Name = "btnSETTING";
            this.btnSETTING.Selected = false;
            this.btnSETTING.Click += new System.EventHandler(this.btnSETTING_Click);
            // 
            // btnOPEN_POINTER
            // 
            resources.ApplyResources(this.btnOPEN_POINTER, "btnOPEN_POINTER");
            this.btnOPEN_POINTER.BackColor = System.Drawing.Color.Transparent;
            this.btnOPEN_POINTER.Image = ((System.Drawing.Image)(resources.GetObject("btnOPEN_POINTER.Image")));
            this.btnOPEN_POINTER.Name = "btnOPEN_POINTER";
            this.btnOPEN_POINTER.Selected = false;
            this.btnOPEN_POINTER.Click += new System.EventHandler(this.btnOpenPointer_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel15, "panel15");
            this.panel15.Name = "panel15";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel13, "panel13");
            this.panel13.Name = "panel13";
            // 
            // lblACCOUNTING
            // 
            this.lblACCOUNTING.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblACCOUNTING, "lblACCOUNTING");
            this.lblACCOUNTING.Image = global::EPower.Accounting.Properties.Resources.Money;
            this.lblACCOUNTING.IsTitle = true;
            this.lblACCOUNTING.Name = "lblACCOUNTING";
            this.lblACCOUNTING.Selected = false;
            this.lblACCOUNTING.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.panel20);
            this.panel16.Controls.Add(this.lblACCOUNTING_1);
            resources.ApplyResources(this.panel16, "panel16");
            this.panel16.Name = "panel16";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.main);
            this.panel20.Controls.Add(this.panel17);
            this.panel20.Controls.Add(this.panel18);
            this.panel20.Controls.Add(this.panel19);
            resources.ApplyResources(this.panel20, "panel20");
            this.panel20.Name = "panel20";
            // 
            // main
            // 
            this.main.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.main, "main");
            this.main.Name = "main";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel17, "panel17");
            this.panel17.Name = "panel17";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel18, "panel18");
            this.panel18.Name = "panel18";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel19, "panel19");
            this.panel19.Name = "panel19";
            // 
            // lblACCOUNTING_1
            // 
            this.lblACCOUNTING_1.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblACCOUNTING_1, "lblACCOUNTING_1");
            this.lblACCOUNTING_1.Image = null;
            this.lblACCOUNTING_1.IsTitle = true;
            this.lblACCOUNTING_1.Name = "lblACCOUNTING_1";
            this.lblACCOUNTING_1.Selected = false;
            this.lblACCOUNTING_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NavAccounting
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "NavAccounting";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel16;
        private Panel panel20;
        private ExTabItem lblACCOUNTING_1;
        private Panel panel12;
        private ExTabItem lblACCOUNTING;
        private SplitContainer splitContainer1;
        private Panel main;
        private Panel panel17;
        private Panel panel18;
        private Panel panel19;
        private Panel panel2;
        private Panel panel15;
        private Panel panel13;
        private Panel panel3;
        private FlowLayoutPanel panel1;
        private Label lblPURCHASE;
        private ExMenuItem btnACCOUNT;
        private ExMenuItem btnVENDOR;
        private ExMenuItem btnTRANSACTION;
        private Label lblACCOUNTING_2;
        private Label lblFIXED_ASSET;
        private ExMenuItem btnFIXED_ASSET;
        private ExMenuItem btnASSET_TYPE;
        private ExMenuItem btnSTOCK_IN;
        private ExMenuItem btnINVOICE_PURCHASE;
        private ExMenuItem btnPURCHASE_PAYMENT;
        private ExMenuItem btnREPORT_ACCOUNTING;
        private ExMenuItem btnGAIN_LOSS_EXCHANGE_RATE;
        private ExMenuItem btnPURCHASE_ORDER;
        private ExMenuItem btnSTOCK_OUT;
        private ExMenuItem btnJOURNAL_ENTRY;
        private ExMenuItem btnOPENING_BALANCE;
        private ExMenuItem btnTRANSFER;
        private Label lblINVENTORY;
        private Label lblREPORTS;
        private Label lblSETTING;
        private ExMenuItem btnSETTING;
        private Label lblSALE;
        private ExMenuItem btnCUSTOMER;
        private ExMenuItem btnINVOICE_SALE;
        private ExMenuItem btnPAYMENT;
        private ExMenuItem btnCREDIT_NOTE;
        private ExMenuItem btnDAILY_TRANSACTION;
        private ExMenuItem btnPRODUCT_CATEGORY;
        private ExMenuItem btnPRODUCT;
        private ExMenuItem btnUNIT;
        private ExMenuItem btnOPEN_POINTER;
        private ExMenuItem btnCASH_EXCHANGE;
    }
}
