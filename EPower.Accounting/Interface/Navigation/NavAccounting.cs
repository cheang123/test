﻿using EPower.Accounting.Helper;
using EPower.Base.Logic;
using HB01.Domain.ListModels;
using HB01.Helpers;
using HB01.Interfaces;
using HB01.Interfaces.MainForm;
using HB01.Interfaces.Reports;
using HB01.Inventory.Interfaces;
using HB01.Logics;
using Interfaces.FixedAsset;
using SoftTech;
using SoftTech.Component;
using System;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Accounting.Interface
{
    public partial class NavAccounting : UserControl
    {
        public NavAccounting()
        {
            InitializeComponent();

            btnACCOUNT.Enabled = Login.IsAuthorized(Permission.ACCOUNT_POINTER);
            btnDAILY_TRANSACTION.Enabled = Login.IsAuthorized(Permission.TRANSACTION_DATE_POINTER);
            btnTRANSACTION.Enabled = Login.IsAuthorized(Permission.TRANSACTION_POINTER);
            btnJOURNAL_ENTRY.Enabled = Login.IsAuthorized(Permission.JOURNAL_POINTER);
            btnOPENING_BALANCE.Enabled = Login.IsAuthorized(Permission.OPENING_BALANCE_POINTER);
            btnGAIN_LOSS_EXCHANGE_RATE.Enabled = Login.IsAuthorized(Permission.EXCHANE_RATE_LOSSES_POINTER);
            btnTRANSFER.Enabled = Login.IsAuthorized(Permission.TRANSFER_BALANCE_POINTER);
            btnCASH_EXCHANGE.Enabled = Login.IsAuthorized(Permission.EXCHANG_RATE_POINTER);
            btnCUSTOMER.Enabled = Login.IsAuthorized(Permission.CUSTOMER_POINTER);
            btnINVOICE_SALE.Enabled = Login.IsAuthorized(Permission.SALE_BILL_POINTER);
            btnPAYMENT.Enabled = Login.IsAuthorized(Permission.SALE_PAYMENT_POINTER);
            btnCREDIT_NOTE.Enabled = Login.IsAuthorized(Permission.CREDIT_INVOCIE_POINTER);
            btnVENDOR.Enabled = Login.IsAuthorized(Permission.SUPPLIER_POINTER);
            btnPURCHASE_ORDER.Enabled = Login.IsAuthorized(Permission.PURCHASE_ORDER_POINTER);
            btnINVOICE_PURCHASE.Enabled = Login.IsAuthorized(Permission.PURCHASE_BILL_POINTER);
            btnPURCHASE_PAYMENT.Enabled = Login.IsAuthorized(Permission.PURCHASE_PAYMENT_POINTER);
            btnFIXED_ASSET.Enabled = Login.IsAuthorized(Permission.FIX_ASSET_POINTER);
            btnASSET_TYPE.Enabled = Login.IsAuthorized(Permission.FIX_ASSET_TYPE_POINTER);
            btnSTOCK_IN.Enabled = Login.IsAuthorized(Permission.RECEVIE_PRODUCT_POINTER);
            btnSTOCK_OUT.Enabled = Login.IsAuthorized(Permission.SHIPPING_PRODUCT_POINTER);
            btnPRODUCT_CATEGORY.Enabled = Login.IsAuthorized(Permission.PRODUCT_TYPE_POINTER);
            btnPRODUCT.Enabled = Login.IsAuthorized(Permission.PRODUCT_POINTER);
            btnUNIT.Enabled = Login.IsAuthorized(Permission.SCALE_POINTER);
            btnREPORT_ACCOUNTING.Enabled = Login.IsAuthorized(Permission.REPORT_ACCOUNTING_POINTER);
            btnSETTING.Enabled = Login.IsAuthorized(Permission.SETTING_POINTER);
            btnOPEN_POINTER.Enabled = Login.IsAuthorized(Permission.OPEN_SYSTEM_POINTER);

            syncAccount();
            this.Load += NavAccounting_Load;
        }

        private void NavAccounting_Load(object sender, EventArgs e)
        {
            if (TCPHelper.IsInternetConnected())
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += delegate (object s1, DoWorkEventArgs e1)
                {
                    HB01.Logics.ExchangeRateLogic.Instance.SyncExchangeRateAync(new SyncExchangeParam() { fromDate = DateTime.Now, toDate = DateTime.Now });
                };
                worker.RunWorkerCompleted += delegate (object s1, RunWorkerCompletedEventArgs e1)
                {
                    if (e1.Error != null)
                    {
                        MsgBox.LogError(e1.Error);
                    }
                };
                worker.RunWorkerAsync();
            }
        }

        private void btnACCOUNT_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(AccountPage), main, this.lblACCOUNTING_1, btnACCOUNT.Text);
        }

        private void btnTRANSACTION_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(TransactionPage), main, this.lblACCOUNTING_1, btnTRANSACTION.Text);
        }

        private void btnJOURNAL_ENTRY_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(JournalEntryPage), main, this.lblACCOUNTING_1, btnJOURNAL_ENTRY.Text);
        }

        private void btnOPENING_BALANCE_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(OpeningBalancePage), main, this.lblACCOUNTING_1, btnOPENING_BALANCE.Text);
        }

        private void btnGAIN_LOSS_EXCHANGE_RATE_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(UnrealizedGaintLosePage), main, this.lblACCOUNTING_1, btnGAIN_LOSS_EXCHANGE_RATE.Text);
        }

        private void btnTRANSFER_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(TransferPage), main, this.lblACCOUNTING_1, btnTRANSFER.Text);
        }

        private void btnCASH_EXCHANGE_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(CashExchangePage), main, this.lblACCOUNTING_1, btnCASH_EXCHANGE.Text);
        }

        private void btnCUSTOMER_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(CustomerPage), main, this.lblACCOUNTING_1, btnCUSTOMER.Text);
        }

        private void btnINVOICE_SALE_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(InvoicePage), main, this.lblACCOUNTING_1, btnINVOICE_SALE.Text);
        }

        private void btnPAYMENT_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(InvoicePaymentPage), main, this.lblACCOUNTING_1, btnPAYMENT.Text);
        }

        private void btnCREDIT_NOTE_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(CreditnotePage), main, this.lblACCOUNTING_1, btnCREDIT_NOTE.Text);
        }

        private void btnVENDOR_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(VendorPage), main, this.lblACCOUNTING_1, btnVENDOR.Text);
        }

        private void btnPURCHASE_ORDER_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(PurchaseOrderPage), main, this.lblACCOUNTING_1, btnPURCHASE_ORDER.Text);
        }

        private void btnINVOICE_PURCHASE_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(BillPage), main, this.lblACCOUNTING_1, btnINVOICE_PURCHASE.Text);
        }

        private void btnPURCHASE_PAYMENT_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(BillPaymentPage), main, this.lblACCOUNTING_1, btnPURCHASE_PAYMENT.Text);
        }

        private void btnFIXED_ASSET_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(FixedAssetPage), main, this.lblACCOUNTING_1, btnFIXED_ASSET.Text);
        }

        private void btnASSET_TYPE_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(FixedAssetTypePage), main, this.lblACCOUNTING_1, btnASSET_TYPE.Text);
        }

        private void btnSTOCK_IN_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(ReceivePage), main, this.lblACCOUNTING_1, btnSTOCK_IN.Text);
        }

        private void btnSTOCK_OUT_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(DeliveryPage), main, this.lblACCOUNTING_1, btnSTOCK_OUT.Text);
        }

        private void btnREPORT_ACCOUNTING_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(ReportContainViewPage), main, this.lblACCOUNTING_1, btnREPORT_ACCOUNTING.Text);
        }

        private void btnSETTING_Click(object sender, EventArgs e)
        {
            try
            {
                new Setting.DialogSetting().ShowDialog();
            }
            catch (Exception ex)
            {
                HMsgBox.ShowWarning(ex.Message);
            }
        }

        private void btnDAILY_TRANSACTION_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(JournalTransactionPage), main, this.lblACCOUNTING_1, btnDAILY_TRANSACTION.Text);
        }

        private void btnPRODUCT_CATEGORY_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(CategoryPage), main, this.lblACCOUNTING_1, btnPRODUCT_CATEGORY.Text);
        }

        private void btnUNIT_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(UoMPage), main, this.lblACCOUNTING_1, btnUNIT.Text);
        }

        private void btnPRODUCT_Click(object sender, EventArgs e)
        {
            UIHelper.ShowPage(typeof(ProductPage), main, this.lblACCOUNTING_1, btnPRODUCT.Text);
        }

        private void btnOpenPointer_Click(object sender, EventArgs e)
        {
            new FluentMainForm().Show();
            //if (string.IsNullOrEmpty(PointerLogic.PointerPath.Trim()))
            //{
            //    if (MsgBox.ShowQuestion(Resources.MSQ_POINTER_PATH_NOT_FOUND, Resources.OPEN_POINTER) == DialogResult.Yes)
            //    {
            //        FolderBrowserDialog diag = new FolderBrowserDialog();
            //        if (diag.ShowDialog() == DialogResult.OK)
            //        {
            //            PointerLogic.PointerPath = diag.SelectedPath;
            //            try
            //            {
            //                var path = Path.Combine(PointerLogic.PointerPath, "HB01.exe");
            //                System.Diagnostics.Process.Start(path);
            //            }
            //            catch (Exception exp)
            //            {
            //                MsgBox.ShowError(exp);
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    try
            //    {
            //        var path = Path.Combine(PointerLogic.PointerPath, "HB01.exe");
            //        System.Diagnostics.Process.Start(path);
            //    }
            //    catch (Exception exp)
            //    {
            //        MsgBox.ShowError(exp);
            //    }
            //}
        }

        private void syncAccount()
        {
            Runner.RunNewThread((Process)delegate ()
            {
                var PointerAcc = AccountLogic.Instance.List<AccountListModel>(new AccountSearchParam() { });
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.MaxValue))
                {
                    var EPowerAcc = DBDataContext.Db.TBL_ACCOUNT_CHARTs.Where(x => x.IS_ACTIVE && x.EXT_ACCOUNT_ID == 0).ToList();
                    foreach (var item in EPowerAcc)
                    {
                        item.EXT_ACCOUNT_ID = PointerAcc.FirstOrDefault(x => x.AccountCode == item.ACCOUNT_CODE)?.Id ?? 0;
                    }
                    DBDataContext.Db.SubmitChanges();

                    var oldPrepayAccount = SettingLogic.GetAccountConfig((int)AccountConfig.PREPAYMENT_ACCOUNT);
                    if (oldPrepayAccount.AccountConfigValue == "" || oldPrepayAccount.AccountConfigValue == "0")
                    {
                        var newPrepayAccount = oldPrepayAccount;
                        newPrepayAccount.AccountConfigValue = PointerAcc.FirstOrDefault(x => x.AccountCode == "38020")?.Id.ToString() ?? "";
                        SettingLogic.UpdateAccountConfig(oldPrepayAccount, newPrepayAccount);
                    }

                    var oldDepositAccount = SettingLogic.GetAccountConfig((int)AccountConfig.DEPOSIT_ACCOUNT);
                    if (oldDepositAccount.AccountConfigValue == "" || oldDepositAccount.AccountConfigValue == "0")
                    {
                        var newDepositAccount = oldDepositAccount;
                        newDepositAccount.AccountConfigValue = PointerAcc.FirstOrDefault(x => x.AccountCode == "31500")?.Id.ToString() ?? "";
                        SettingLogic.UpdateAccountConfig(oldDepositAccount, newDepositAccount);
                    }

                    var oldDepositReceive = SettingLogic.GetAccountConfig((int)AccountConfig.DEPOSIT_ACCOUNT_CASH);
                    if (oldDepositReceive.AccountConfigValue == "" || oldDepositReceive.AccountConfigValue == "0")
                    {
                        var newDepositReceive = oldDepositReceive;
                        newDepositReceive.AccountConfigValue = PointerAcc.FirstOrDefault(x => x.AccountCode == "16511")?.Id.ToString() ?? "";
                        SettingLogic.UpdateAccountConfig(oldDepositReceive, newDepositReceive);
                    }

                    SettingLogic.CommitAccountConfig();
                    tran.Complete();
                }
            });
        }

    }
}
