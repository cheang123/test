﻿
namespace EPower.Accounting.Interface.FixAsset
{
    partial class DialogGetFixAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BottomPanel = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOk = new SoftTech.Component.ExButton();
            this.TopPanel = new System.Windows.Forms.Panel();
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.gridPanel = new System.Windows.Forms.Panel();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.dgvFixAsset = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colASSET_NAME = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colASSET_CATEGORY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCREATE_ON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRICE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQTY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.BottomPanel.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            this.gridPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFixAsset)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.gridPanel);
            this.content.Controls.Add(this.TopPanel);
            this.content.Controls.Add(this.BottomPanel);
            this.content.Size = new System.Drawing.Size(800, 427);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.BottomPanel, 0);
            this.content.Controls.SetChildIndex(this.TopPanel, 0);
            this.content.Controls.SetChildIndex(this.gridPanel, 0);
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnClose);
            this.BottomPanel.Controls.Add(this.btnOk);
            this.BottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomPanel.Location = new System.Drawing.Point(1, 390);
            this.BottomPanel.Name = "BottomPanel";
            this.BottomPanel.Size = new System.Drawing.Size(798, 36);
            this.BottomPanel.TabIndex = 6;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnClose.Location = new System.Drawing.Point(720, 6);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 25;
            this.btnClose.Text = "បិទ";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOk.Location = new System.Drawing.Point(639, 6);
            this.btnOk.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 24;
            this.btnOk.Text = "យល់ព្រម";
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.searchControl);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopPanel.Location = new System.Drawing.Point(1, 0);
            this.TopPanel.Name = "TopPanel";
            this.TopPanel.Size = new System.Drawing.Size(798, 36);
            this.TopPanel.TabIndex = 7;
            // 
            // searchControl
            // 
            this.searchControl.EditValue = "";
            this.searchControl.Location = new System.Drawing.Point(1, 2);
            this.searchControl.Margin = new System.Windows.Forms.Padding(2);
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            this.searchControl.Size = new System.Drawing.Size(174, 32);
            this.searchControl.TabIndex = 18;
            // 
            // gridPanel
            // 
            this.gridPanel.Controls.Add(this.dgv);
            this.gridPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPanel.Location = new System.Drawing.Point(1, 36);
            this.gridPanel.Name = "gridPanel";
            this.gridPanel.Size = new System.Drawing.Size(798, 354);
            this.gridPanel.TabIndex = 8;
            // 
            // dgv
            // 
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 0);
            this.dgv.MainView = this.dgvFixAsset;
            this.dgv.Name = "dgv";
            this.dgv.Size = new System.Drawing.Size(798, 354);
            this.dgv.TabIndex = 0;
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvFixAsset});
            // 
            // dgvFixAsset
            // 
            this.dgvFixAsset.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colASSET_NAME,
            this.colASSET_CATEGORY,
            this.colCREATE_ON,
            this.colPRICE,
            this.colQTY,
            this.gridColumn1});
            this.dgvFixAsset.GridControl = this.dgv;
            this.dgvFixAsset.Name = "dgvFixAsset";
            this.dgvFixAsset.OptionsCustomization.AllowGroup = false;
            this.dgvFixAsset.OptionsMenu.EnableGroupPanelMenu = false;
            this.dgvFixAsset.OptionsView.ShowGroupPanel = false;
            // 
            // colASSET_NAME
            // 
            this.colASSET_NAME.Caption = "AssetName";
            this.colASSET_NAME.FieldName = "AssetName";
            this.colASSET_NAME.Name = "colASSET_NAME";
            this.colASSET_NAME.Visible = true;
            this.colASSET_NAME.VisibleIndex = 0;
            // 
            // colASSET_CATEGORY
            // 
            this.colASSET_CATEGORY.Caption = "AssetCategory";
            this.colASSET_CATEGORY.FieldName = "AssetCategory";
            this.colASSET_CATEGORY.Name = "colASSET_CATEGORY";
            this.colASSET_CATEGORY.Visible = true;
            this.colASSET_CATEGORY.VisibleIndex = 1;
            // 
            // colCREATE_ON
            // 
            this.colCREATE_ON.Caption = "CreateOn";
            this.colCREATE_ON.FieldName = "CreateOn";
            this.colCREATE_ON.Name = "colCREATE_ON";
            this.colCREATE_ON.Visible = true;
            this.colCREATE_ON.VisibleIndex = 2;
            // 
            // colPRICE
            // 
            this.colPRICE.Caption = "Price";
            this.colPRICE.FieldName = "Price";
            this.colPRICE.Name = "colPRICE";
            this.colPRICE.Visible = true;
            this.colPRICE.VisibleIndex = 3;
            // 
            // colQTY
            // 
            this.colQTY.Caption = "Qty";
            this.colQTY.FieldName = "Qty";
            this.colQTY.Name = "colQTY";
            this.colQTY.Visible = true;
            this.colQTY.VisibleIndex = 4;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "gridColumn1";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 5;
            // 
            // DialogGetFixAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "DialogGetFixAsset";
            this.Text = "DialogGetFixAsset";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.BottomPanel.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            this.gridPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFixAsset)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel BottomPanel;
        private System.Windows.Forms.Panel TopPanel;
        private SoftTech.Component.ExButton btnClose;
        private SoftTech.Component.ExButton btnOk;
        private System.Windows.Forms.Panel gridPanel;
        private DevExpress.XtraEditors.SearchControl searchControl;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvFixAsset;
        private DevExpress.XtraGrid.Columns.GridColumn colASSET_NAME;
        private DevExpress.XtraGrid.Columns.GridColumn colASSET_CATEGORY;
        private DevExpress.XtraGrid.Columns.GridColumn colCREATE_ON;
        private DevExpress.XtraGrid.Columns.GridColumn colPRICE;
        private DevExpress.XtraGrid.Columns.GridColumn colQTY;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
    }
}