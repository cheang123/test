﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Accounting.Interface
{
    partial class DialogAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogAccount));
            this.lblACCOUNT_NAME = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblPARENT_ACCOUNT = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblACCOUNT_CODE = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cboExtAccount = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.lblEXT_ACCOUNT = new System.Windows.Forms.Label();
            this.cboParentAccount = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOk = new SoftTech.Component.ExButton();
            this.cboAccountType = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.lblACCOUNT_TYPE = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboCurrency = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboExtAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboParentAccount.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboAccountType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCurrency.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.cboAccountType);
            this.content.Controls.Add(this.lblACCOUNT_TYPE);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.cboParentAccount);
            this.content.Controls.Add(this.cboExtAccount);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.lblEXT_ACCOUNT);
            this.content.Controls.Add(this.txtCode);
            this.content.Controls.Add(this.label8);
            this.content.Controls.Add(this.lblACCOUNT_CODE);
            this.content.Controls.Add(this.lblPARENT_ACCOUNT);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.txtName);
            this.content.Controls.Add(this.lblACCOUNT_NAME);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblACCOUNT_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtName, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.lblPARENT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblACCOUNT_CODE, 0);
            this.content.Controls.SetChildIndex(this.label8, 0);
            this.content.Controls.SetChildIndex(this.txtCode, 0);
            this.content.Controls.SetChildIndex(this.lblEXT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.cboExtAccount, 0);
            this.content.Controls.SetChildIndex(this.cboParentAccount, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.lblACCOUNT_TYPE, 0);
            this.content.Controls.SetChildIndex(this.cboAccountType, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            // 
            // lblACCOUNT_NAME
            // 
            resources.ApplyResources(this.lblACCOUNT_NAME, "lblACCOUNT_NAME");
            this.lblACCOUNT_NAME.Name = "lblACCOUNT_NAME";
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            this.txtName.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Name = "label7";
            // 
            // lblPARENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPARENT_ACCOUNT, "lblPARENT_ACCOUNT");
            this.lblPARENT_ACCOUNT.Name = "lblPARENT_ACCOUNT";
            // 
            // txtCode
            // 
            resources.ApplyResources(this.txtCode, "txtCode");
            this.txtCode.Name = "txtCode";
            this.txtCode.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblACCOUNT_CODE
            // 
            resources.ApplyResources(this.lblACCOUNT_CODE, "lblACCOUNT_CODE");
            this.lblACCOUNT_CODE.Name = "lblACCOUNT_CODE";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Name = "label8";
            // 
            // cboExtAccount
            // 
            resources.ApplyResources(this.cboExtAccount, "cboExtAccount");
            this.cboExtAccount.Name = "cboExtAccount";
            this.cboExtAccount.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboExtAccount.Properties.Appearance.Font")));
            this.cboExtAccount.Properties.Appearance.Options.UseFont = true;
            this.cboExtAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboExtAccount.Properties.Buttons"))))});
            this.cboExtAccount.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboExtAccount.Properties.Columns"), resources.GetString("cboExtAccount.Properties.Columns1"), ((int)(resources.GetObject("cboExtAccount.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboExtAccount.Properties.Columns3"))), resources.GetString("cboExtAccount.Properties.Columns4"), ((bool)(resources.GetObject("cboExtAccount.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboExtAccount.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboExtAccount.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboExtAccount.Properties.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboExtAccount.Properties.Columns9"), resources.GetString("cboExtAccount.Properties.Columns10"), ((int)(resources.GetObject("cboExtAccount.Properties.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboExtAccount.Properties.Columns12"))), resources.GetString("cboExtAccount.Properties.Columns13"), ((bool)(resources.GetObject("cboExtAccount.Properties.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboExtAccount.Properties.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboExtAccount.Properties.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboExtAccount.Properties.Columns17")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboExtAccount.Properties.Columns18"), resources.GetString("cboExtAccount.Properties.Columns19"), ((int)(resources.GetObject("cboExtAccount.Properties.Columns20"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboExtAccount.Properties.Columns21"))), resources.GetString("cboExtAccount.Properties.Columns22"), ((bool)(resources.GetObject("cboExtAccount.Properties.Columns23"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboExtAccount.Properties.Columns24"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboExtAccount.Properties.Columns25"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboExtAccount.Properties.Columns26"))))});
            this.cboExtAccount.Properties.NullText = resources.GetString("cboExtAccount.Properties.NullText");
            this.cboExtAccount.Properties.PopupWidth = 500;
            this.cboExtAccount.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboExtAccount.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboExtAccount.Required = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // lblEXT_ACCOUNT
            // 
            resources.ApplyResources(this.lblEXT_ACCOUNT, "lblEXT_ACCOUNT");
            this.lblEXT_ACCOUNT.Name = "lblEXT_ACCOUNT";
            // 
            // cboParentAccount
            // 
            resources.ApplyResources(this.cboParentAccount, "cboParentAccount");
            this.cboParentAccount.Name = "cboParentAccount";
            this.cboParentAccount.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboParentAccount.Properties.Appearance.Font")));
            this.cboParentAccount.Properties.Appearance.Options.UseFont = true;
            this.cboParentAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboParentAccount.Properties.Buttons"))))});
            this.cboParentAccount.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboParentAccount.Properties.Columns"), resources.GetString("cboParentAccount.Properties.Columns1"), ((int)(resources.GetObject("cboParentAccount.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboParentAccount.Properties.Columns3"))), resources.GetString("cboParentAccount.Properties.Columns4"), ((bool)(resources.GetObject("cboParentAccount.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboParentAccount.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboParentAccount.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboParentAccount.Properties.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboParentAccount.Properties.Columns9"), resources.GetString("cboParentAccount.Properties.Columns10"), ((int)(resources.GetObject("cboParentAccount.Properties.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboParentAccount.Properties.Columns12"))), resources.GetString("cboParentAccount.Properties.Columns13"), ((bool)(resources.GetObject("cboParentAccount.Properties.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboParentAccount.Properties.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboParentAccount.Properties.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboParentAccount.Properties.Columns17"))))});
            this.cboParentAccount.Properties.NullText = resources.GetString("cboParentAccount.Properties.NullText");
            this.cboParentAccount.Properties.PopupWidth = 500;
            this.cboParentAccount.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboParentAccount.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboParentAccount.Required = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCHANGE_LOG);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnOk);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cboAccountType
            // 
            resources.ApplyResources(this.cboAccountType, "cboAccountType");
            this.cboAccountType.Name = "cboAccountType";
            this.cboAccountType.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboAccountType.Properties.Appearance.Font")));
            this.cboAccountType.Properties.Appearance.Options.UseFont = true;
            this.cboAccountType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboAccountType.Properties.Buttons"))))});
            this.cboAccountType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboAccountType.Properties.Columns"), resources.GetString("cboAccountType.Properties.Columns1"), ((int)(resources.GetObject("cboAccountType.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboAccountType.Properties.Columns3"))), resources.GetString("cboAccountType.Properties.Columns4"), ((bool)(resources.GetObject("cboAccountType.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboAccountType.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboAccountType.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboAccountType.Properties.Columns8"))))});
            this.cboAccountType.Properties.NullText = resources.GetString("cboAccountType.Properties.NullText");
            this.cboAccountType.Properties.PopupWidth = 500;
            this.cboAccountType.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboAccountType.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboAccountType.Required = false;
            // 
            // lblACCOUNT_TYPE
            // 
            resources.ApplyResources(this.lblACCOUNT_TYPE, "lblACCOUNT_TYPE");
            this.lblACCOUNT_TYPE.Name = "lblACCOUNT_TYPE";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // cboCurrency
            // 
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboCurrency.Properties.Appearance.Font")));
            this.cboCurrency.Properties.Appearance.Options.UseFont = true;
            this.cboCurrency.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboCurrency.Properties.Buttons"))))});
            this.cboCurrency.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboCurrency.Properties.Columns"), resources.GetString("cboCurrency.Properties.Columns1"), ((int)(resources.GetObject("cboCurrency.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboCurrency.Properties.Columns3"))), resources.GetString("cboCurrency.Properties.Columns4"), ((bool)(resources.GetObject("cboCurrency.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboCurrency.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboCurrency.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboCurrency.Properties.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboCurrency.Properties.Columns9"), resources.GetString("cboCurrency.Properties.Columns10"), ((int)(resources.GetObject("cboCurrency.Properties.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboCurrency.Properties.Columns12"))), resources.GetString("cboCurrency.Properties.Columns13"), ((bool)(resources.GetObject("cboCurrency.Properties.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboCurrency.Properties.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboCurrency.Properties.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboCurrency.Properties.Columns17"))))});
            this.cboCurrency.Properties.NullText = resources.GetString("cboCurrency.Properties.NullText");
            this.cboCurrency.Properties.PopupWidth = 500;
            this.cboCurrency.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboCurrency.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboCurrency.Required = false;
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // DialogAccount
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogAccount";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboExtAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboParentAccount.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cboAccountType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboCurrency.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtName;
        private Label lblACCOUNT_NAME;
        private Label label7;
        private Label lblPARENT_ACCOUNT;
        private Label lblACCOUNT_CODE;
        private TextBox txtCode;
        private Label label8;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboExtAccount;
        private Label label1;
        private Label lblEXT_ACCOUNT;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboParentAccount;
        private Panel panel2;
        private ExButton btnCHANGE_LOG;
        private Panel panel1;
        private ExButton btnClose;
        private ExButton btnOk;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboAccountType;
        private Label lblACCOUNT_TYPE;
        private Label label3;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboCurrency;
        private Label lblCURRENCY;
        private Label label2;
    }
}