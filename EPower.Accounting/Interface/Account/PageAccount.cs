﻿using EPower.Base.Properties;
using HB01.Domain.Enums;
using HB01.Domain.ListModels;
using HB01.Logics;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Accounting.Interface
{
    public partial class PageAccount : Form
    {
        #region Data
        private List<AccountModel> _accounts = new List<AccountModel>();
        List<AccountListModel> PointerAcc = new List<AccountListModel>();
        #endregion
        #region Constructor
        public PageAccount()
        {
            InitializeComponent();
            searchControl.Properties.NullValuePrompt = Resources.SEARCH;
            bind();
            this.Load += PageAccount_Load;
        }

        #endregion

        #region Operation

        private void PageAccount_Load(object sender, EventArgs e)
        {
            tgv.CollapseToLevel(1);
        }

        private void btnNew_Click(object sender, EventArgs e)
        {

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var account = tgv.GetFocusedRow() as AccountModel;
            if (account == null || !account.IsEditable)
            {
                return;
            }
            var obj = DBDataContext.Db.TBL_ACCOUNT_CHARTs.FirstOrDefault(x => x.ACCOUNT_ID == account.AccountId);
            DialogAccount diag = new DialogAccount(GeneralProcess.Update, obj);
            if (diag.ShowDialog() == DialogResult.OK)
            {
                bind();
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {

            }
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            bool val = false;
            if (this.tgv.GetFocusedRow() != null)
            {
                val = true;
            }
            return val;
        }

        private void bind()
        {
            this.tgv.Columns[Ext_Account.FieldName].Visible = Base.Logic.PointerLogic.isConnectedPointer;
            if (Base.Logic.PointerLogic.isConnectedPointer && PointerAcc.Count() == 0)
            {
                PointerAcc = AccountLogic.Instance.List<AccountListModel>(new AccountSearchParam() { });
            }

            var q = (from a in DBDataContext.Db.TBL_ACCOUNT_CHARTs
                     where a.IS_ACTIVE
                     select new AccountModel
                     {
                         ParentId = a.PARENT_ID,
                         RenderId = a.ACCOUNT_ID,
                         ParentAccountId = a.PARENT_ID,
                         AccountId = a.ACCOUNT_ID,
                         Account = a.ACCOUNT_CODE + " " + a.ACCOUNT_NAME,
                         AccountTypeId = a.ACCOUNT_TYPE_ID,
                         ExtAccountId = a.EXT_ACCOUNT_ID
                     }).ToList();

            _accounts = (from a in q
                         join e in PointerAcc on a.ExtAccountId equals e.Id into l
                         from ext in l.DefaultIfEmpty()
                         orderby a.Account ascending
                         select new AccountModel
                         {
                             ParentId = a.ParentId,
                             RenderId = a.RenderId,
                             ParentAccountId = a.ParentAccountId,
                             AccountId = a.AccountId,
                             Account = a.Account,
                             AccountTypeId = a.AccountTypeId,
                             ExtAccountId = a.ExtAccountId,
                             ExtAccount = ext == null ? "" : ext.AccountCode + " " + ext.AccountName,
                             IsEditable = true
                         }).ToList();

            var accType = (from t in DBDataContext.Db.TBL_ACCOUNT_TYPEs
                           where t.IS_ACTIVE
                           select new AccountModel
                           {
                               Account = t.TYPE_NAME,
                               AccountId = t.INDEX,
                               AccountTypeId = t.TYPE_ID,
                               Nature = t.NATURE,
                               ParentId = 0,
                               RenderId = t.TYPE_ID * -1,
                               IsEditable = false
                           }).ToList();
            var accNature = Enum.GetValues(typeof(AccountNatures)).Cast<AccountNatures>().Where(x => x != AccountNatures.All).Select(x => new AccountModel
            {
                Account = ResourceHelper.Translate(x.ToString().ToUpper()),
                AccountId = (int)x,
                ParentId = 0,
                RenderId = (int)x - 100,
                IsEditable = false
            });
            foreach (var n in accNature)
            {
                var accTypeByNature = accType.Where(x => x.Nature == n.AccountId);
                foreach (var type in accTypeByNature)
                {
                    var accByType = _accounts.Where(x => x.AccountTypeId == type.AccountTypeId && x.ParentId == 0).ToList();
                    type.ParentId = n.RenderId;
                    foreach (var acc in accByType)
                    {
                        acc.ParentId = type.RenderId;
                    }
                    _accounts.Add(type);
                }
                _accounts.Add(n);
            }
            tgv.DataSource = _accounts;
            tgv.ExpandAll();
        }
        #endregion

        private void btnSyncAccount_Click(object sender, EventArgs e)
        {
            if (PointerAcc.Count == 0)
            {
                return;
            }

            if (MsgBox.ShowQuestion(Resources.MSQ_CONFIRM_RESYNC_ACCOUNT, Resources.WARNING) == DialogResult.Yes)
            {
                Runner.RunNewThread(delegate ()
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.RequiresNew, TimeSpan.MaxValue))
                    {
                        var EPowerAcc = DBDataContext.Db.TBL_ACCOUNT_CHARTs.Where(x => x.IS_ACTIVE).ToList();
                        foreach (var item in EPowerAcc)
                        {
                            item.EXT_ACCOUNT_ID = PointerAcc.FirstOrDefault(x => x.AccountCode == item.ACCOUNT_CODE)?.Id ?? 0;
                        }
                        DBDataContext.Db.SubmitChanges();
                        tran.Complete();
                    }
                    bind();
                });
            }
        }
    }
    class AccountModel
    {
        public int ParentId { get; set; }
        public int RenderId { get; set; }
        public int ParentAccountId { get; set; }
        public int AccountId { get; set; }
        public int AccountTypeId { get; set; }
        public int Nature { get; set; }
        public string Account { get; set; }
        public int ExtAccountId { get; set; }
        public string ExtAccount { get; set; }
        public bool IsEditable { get; set; }
    }
}
