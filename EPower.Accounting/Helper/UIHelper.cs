﻿using DevExpress.XtraEditors;
using Dynamic.Helpers;
using HB01.Helpers;
using HB01.Helpers.DevExpressCustomize;
using HB01.Interfaces.Reports;
using HB01.Logics;
using HB01.Properties;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Accounting.Helper
{
    internal static class UIHelper
    {
        private static string[] _userLoggedIn = Properties.Settings.Default.USER_LOGGED_IN?.Cast<string>().ToArray() ?? new string[0];
        public static Control currentPage = null;
        public static Dictionary<Type, Control> pages = new Dictionary<Type, Control>(); 
        public static void ShowPage(Type pageType, Control main, Control lblTitle, string title = "", string securityKey = "")
        {
            var isReloadedConfigure = true;
            var isShown = true;
            try
            {
                if (pages.ContainsKey(pageType) == false)
                {
                    var page = (HPage)Activator.CreateInstance(pageType);
                    page.Text = title;
                    lblTitle.Text = title;
                    page.TopLevel = false;
                    page.Dock = DockStyle.Fill;
                    page.Visible = true;
                    main.Controls.Add(page);
                    pages[pageType] = page;
                    isReloadedConfigure = false;
                    isShown = false;
                    /*
                     * Apply resource
                     */
                    //ResourceHelper.ApplyResource(this.pages[pageType]);
                }
                var currentPage = ((HPage)pages[pageType]);
                main.Controls.SetChildIndex(currentPage, 0);
                currentPage.Text = title;
                lblTitle.Text = title;
                if (isShown)
                {
                    currentPage.Reload();
                }
                currentPage.Shown += (object sender2, EventArgs e2) =>
                {
                    var timer = new System.Windows.Forms.Timer();
                    timer.Interval = 500;
                    timer.Start();
                    timer.Tick += (object sender1, EventArgs e1) =>
                    {
                        currentPage.IsLoading = false;
                        timer.Stop();
                        currentPage.Reload();
                    };
                };
                currentPage.SecurityKey = securityKey;
                if (!isReloadedConfigure)
                {
                    currentPage.RestoreUserConfigure();
                    isReloadedConfigure = true;
                }
                if (pageType == typeof(ReportContainViewPage))
                {
                    pages.Remove(pageType);
                }
                //Reload();
            }
            catch (Exception ex)
            {
                HMsgBox.ShowWarning(ex.InnerException.Message);
            }
        }
        //public static void ActivateResource(bool needStart = false)
        //{
        //    ResourceHelper.SecondaryResourceManager = Resources.ResourceManager;
        //    //var cult = new CultureInfo(Settings.Default.LANGUAGE);
        //    var language = "";
        //    if (needStart)
        //    {
        //        language = "km-KH";
        //    }
        //    else
        //    {
        //        language = Current.Company?.CompanySetting.LanguageKey ?? "km-KH";
        //    }
        //    var cult = new CultureInfo(language);
        //    SetDefaultCulture(cult);
        //    if (cult.Name == "km-KH")
        //    {
        //        DevExpress.XtraEditors.Controls.Localizer.Active = new KHEditorsLocalizer();
        //        DevExpress.XtraGrid.Localization.GridLocalizer.Active = new KHGridLocalizer();
        //    }
        //}
    }
}
