-- TARGET ERROR : 
-- "a logical consistency-based error I/O error : incorrect pageid..."

-- NOTICE:
-- before fix this make sure you backup database first

-- STEP TO FIX
-- ******************************************
--  1. CHECKDB without data loss -- finish if ok
--  2. CHECKDB with data loss
--  2.1 Copy table before fix
--  2.2 DBCC with data losss
--  2.3 Copy table after fix
--  2.4 Copmare table of before vs after
GO
-- 1. CHECK if database chan be fix without any loss
ALTER DATABASE Epower SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
DBCC CHECKDB(EPower);
DBCC CHECKDB(EPower);
ALTER DATABASE Epower SET MULTI_USER;


GO
-- 2. CHECKDB with data loss
-- this step may result in data loss in some case of error

-- 2.1 Copy total rows before fix with data loss
DECLARE @T_Name VARCHAR(250)
DECLARE @COUNT INT
DECLARE @SQL VARCHAR(2000)
CREATE TABLE #T_Info(ID INT IDENTITY(1,1),T_Name VARCHAR(200),D_Count INT)
DECLARE TINFO_CUR CURSOR FOR 
SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_TYPE='BASE TABLE'
OPEN TINFO_CUR
FETCH NEXT FROM TINFO_CUR INTO @T_Name
WHILE @@FETCH_STATUS =0
BEGIN
	SET @SQL='INSERT INTO #T_Info(T_Name,D_Count) SELECT '''+@T_Name+''',COUNT(*) FROM '+@T_Name+''
	EXECUTE (@SQL)  
	FETCH NEXT FROM TINFO_CUR INTO @T_Name
END
CLOSE TINFO_CUR
DEALLOCATE TINFO_CUR
SELECT * INTO T1 FROM #T_Info ORDER BY T_NAME
DROP TABLE #T_Info
GO

-- 2.2 CHECKDB with data loss
ALTER DATABASE Epower SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
DBCC CHECKDB(EPower,REPAIR_ALLOW_DATA_LOSS);
DBCC CHECKDB(EPower,REPAIR_ALLOW_DATA_LOSS);
DBCC CHECKDB(EPower);
ALTER DATABASE Epower SET MULTI_USER;
GO

-- 2.3 Copy total rows after fix
DECLARE @T_Name VARCHAR(250)
DECLARE @COUNT INT
DECLARE @SQL VARCHAR(2000)
CREATE TABLE #T_Info(ID INT IDENTITY(1,1),T_Name VARCHAR(200),D_Count INT)
DECLARE TINFO_CUR CURSOR FOR 
SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_TYPE='BASE TABLE'
OPEN TINFO_CUR
FETCH NEXT FROM TINFO_CUR INTO @T_Name
WHILE @@FETCH_STATUS =0
BEGIN
	SET @SQL='INSERT INTO #T_Info(T_Name,D_Count) SELECT '''+@T_Name+''',COUNT(*) FROM '+@T_Name+''
	EXECUTE (@SQL)
	FETCH NEXT FROM TINFO_CUR INTO @T_Name
END
CLOSE TINFO_CUR
DEALLOCATE TINFO_CUR
SELECT * INTO T2 FROM #T_Info ORDER BY T_NAME
DROP TABLE #T_Info 
GO

-- 2.4 Compare result if any loss
-- if found any loss then restore database back for find other solution
SELECT * 
FROM T1 
FULL OUTER JOIN T2 ON T1.T_Name=T2.T_Name
WHERE ISNULL(T1.D_Count,0) <> ISNULL(T2.D_Count,0)
AND T1.T_NAME <> 'T1'
