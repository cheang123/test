﻿IF NOT EXISTS(SELECT * FROM dbo.TBL_UTILITY WHERE UTILITY_ID = 108)
	INSERT INTO dbo.TBL_UTILITY
	(UTILITY_ID, UTILITY_NAME, DESCRIPTION, UTILITY_VALUE)
	VALUES (108, N'EAC_MAIL_FOR_REPORT_STATISTIC', N'Default mail to send report statistic', N'eac.tariff@gmail.com;eac.tariff1@gmail.com')
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TLKP_CUSTOMER_CONNECTION_TYPE' AND COLUMN_NAME = 'IS_ACTIVE')
	ALTER TABLE dbo.TLKP_CUSTOMER_CONNECTION_TYPE ADD [IS_ACTIVE] BIT
GO

UPDATE c
SET c.IS_ACTIVE = g.IS_ACTIVE
FROM dbo.TLKP_CUSTOMER_CONNECTION_TYPE c
INNER JOIN dbo.TLKP_CUSTOMER_GROUP g ON c.NONLICENSE_CUSTOMER_GROUP_ID = g.CUSTOMER_GROUP_ID
WHERE c.IS_ACTIVE IS NULL
GO

DROP TABLE IF EXISTS #tmpConnection

SELECT IS_ACTIVE = 1, ID =	35	, NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV' INTO #tmpConnection UNION ALL
SELECT IS_ACTIVE = 0, ID =	36	, NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់ (លុប)' UNION ALL
SELECT IS_ACTIVE = 1, ID =	37	, NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	47	, NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV' UNION ALL
SELECT IS_ACTIVE = 0, ID =	48	, NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់ (លុប)' UNION ALL
SELECT IS_ACTIVE = 1, ID =	49	, NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	59	, NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV' UNION ALL
SELECT IS_ACTIVE = 0, ID =	60	, NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់ (លុប)' UNION ALL
SELECT IS_ACTIVE = 1, ID =	61	, NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	71	, NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV ' UNION ALL
SELECT IS_ACTIVE = 0, ID =	72	, NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ  មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់ (លុប)' UNION ALL
SELECT IS_ACTIVE = 1, ID =	73	, NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	83	, NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV' UNION ALL
SELECT IS_ACTIVE = 0, ID =	84	, NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម  មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់ (លុប)' UNION ALL
SELECT IS_ACTIVE = 1, ID =	85	, NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	95	, NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV' UNION ALL
SELECT IS_ACTIVE = 0, ID =	96	, NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់ (លុប)' UNION ALL
SELECT IS_ACTIVE = 1, ID =	97	, NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	107	, NAME = N'ពាណិជ្ជកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV' UNION ALL
SELECT IS_ACTIVE = 0, ID =	108	, NAME = N'ពាណិជ្ជកម្ម  មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់  (លុប)' UNION ALL
SELECT IS_ACTIVE = 1, ID =	109	, NAME = N'ពាណិជ្ជកម្ម  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	119	, NAME = N'រដ្ឋបាលសាធារណៈ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV' UNION ALL
SELECT IS_ACTIVE = 0, ID =	120	, NAME = N'រដ្ឋបាលសាធារណៈ  មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់  (លុប)' UNION ALL
SELECT IS_ACTIVE = 1, ID =	121	, NAME = N'រដ្ឋបាលសាធារណៈ  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	131	, NAME = N'សេវាកម្មផ្សេងៗ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV' UNION ALL
SELECT IS_ACTIVE = 0, ID =	132	, NAME = N'សេវាកម្មផ្សេងៗ  មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់  (លុប)' UNION ALL
SELECT IS_ACTIVE = 1, ID =	133	, NAME = N'សេវាកម្មផ្សេងៗ  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	22	, NAME = N'ពាណិជ្ជកម្ម LV ទិញសាធារណៈ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	23	, NAME = N'រដ្ឋបាល LV ទិញសាធារណៈ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	134	, NAME = N'សេវាកម្មផ្សេងៗ ទិញLV សាធារណៈ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	25	, NAME = N'បូមទឹកកសិកម្មពេលយប់ ម៉ោង៩យប់ ដល់ម៉ោង៧ព្រឹក ទិញLV សាធារណៈ' UNION ALL
SELECT IS_ACTIVE = 1, ID =	141	, NAME = N'បូមទឹកកសិកម្មពេលយប់ ម៉ោង៩យប់ ដល់ម៉ោង៧ព្រឹក ទិញMV' UNION ALL
SELECT IS_ACTIVE = 1, ID =	142	, NAME = N'បូមទឹកកសិកម្មពេលយប់ ម៉ោង៩យប់ ដល់ម៉ោង៧ព្រឹក ទិញLV ត្រង់ស្វូអ្នកលក់' UNION ALL
SELECT IS_ACTIVE = 1, ID =	143	, NAME = N'បូមទឹកកសិកម្មពេលយប់ ម៉ោង៩យប់ ដល់ម៉ោង៧ព្រឹក ទិញLV ត្រង់ស្វូអ្នកទិញ'
GO

UPDATE c
SET c.IS_ACTIVE = t.IS_ACTIVE, c.CUSTOMER_CONNECTION_TYPE_NAME = t.NAME
FROM dbo.TLKP_CUSTOMER_CONNECTION_TYPE c
INNER JOIN #tmpConnection t ON c.CUSTOMER_CONNECTION_TYPE_ID = t.ID
WHERE c.IS_ACTIVE <> t.IS_ACTIVE OR c.CUSTOMER_CONNECTION_TYPE_NAME <> t.NAME
GO

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'TLKP_CUSTOMER_CONNECTION_TYPE' AND COLUMN_NAME = 'IS_ACTIVE' AND IS_NULLABLE = 'YES')
	ALTER TABLE dbo.TLKP_CUSTOMER_CONNECTION_TYPE ALTER COLUMN IS_ACTIVE BIT NOT NULL
GO

IF NOT EXISTS(SELECT TOP 1 * FROM dbo.TLKP_CUSTOMER_CONNECTION_TYPE WHERE CUSTOMER_CONNECTION_TYPE_ID = 145)
	INSERT INTO dbo.TLKP_CUSTOMER_CONNECTION_TYPE
	(CUSTOMER_CONNECTION_TYPE_ID, CUSTOMER_CONNECTION_TYPE_NAME, DESCRIPTION, CURRENCY_ID, TARIFF, NONLICENSE_CUSTOMER_GROUP_ID, PRICE_ID, IS_ACTIVE)
	SELECT 145, N'រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញMV', N'13.រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញMV', 1, 653, 9, -37, 1 UNION ALL
	SELECT 146, N'រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកលក់', N'14.រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកលក់', 1, 712, 9, -38, 1 UNION ALL
	SELECT 147, N'រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកទិញ', N'15.រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកទិញ', 1, 679, 9, -39, 1
GO

SET IDENTITY_INSERT dbo.TBL_PRICE ON
GO

IF NOT EXISTS (SELECT TOP 1 * FROM dbo.TBL_PRICE WHERE PRICE_ID = -37)
	INSERT INTO dbo.TBL_PRICE
	(PRICE_ID, PRICE_NAME, CREATE_ON, CREATE_BY, CURRENCY_ID, IS_ACTIVE, IS_STANDARD_RATING, CAPACITY_PRICE)
	SELECT -37, N'រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញMV', GETDATE(), N'ប្រព័ន្ធ', 1, 1, 0, 0 UNION ALL
	SELECT -38, N'រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកលក់', GETDATE(), N'ប្រព័ន្ធ', 1, 1, 0, 0 UNION ALL
	SELECT -39, N'រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកទិញ', GETDATE(), N'ប្រព័ន្ធ', 1, 1, 0, 0

    INSERT INTO dbo.TBL_PRICE_DETAIL
    (PRICE_ID, START_USAGE, END_USAGE, PRICE, IS_ALLOW_FIXED_AMOUNT, FIXED_AMOUNT)
    SELECT -37, 0, -1, 653, 0, 0 UNION ALL
    SELECT -38, 0, -1, 712, 0, 0 UNION ALL
    SELECT -39, 0, -1, 679, 0, 0
GO

SET IDENTITY_INSERT dbo.TBL_PRICE OFF
GO

IF OBJECT_ID('RUN_REF_03B') IS NOT NULL
	DROP PROC RUN_REF_03B
GO

CREATE PROC dbo.RUN_REF_03B
    @DATA_MONTH DATETIME = '2017-03-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
IF @DATA_MONTH
	BETWEEN '2022-01-01' AND '9999-12-31'
	 EXEC RUN_REF_03B_2022 @DATA_MONTH, @BILLING_CYCLE_ID, @AREA_ID
ELSE IF @DATA_MONTH
   BETWEEN '2021-01-01' AND '2021-12-01'
    EXEC RUN_REF_03B_2021 @DATA_MONTH, @BILLING_CYCLE_ID, @AREA_ID
ELSE IF @DATA_MONTH
        BETWEEN '2020-02-01' AND '2020-12-01'
    EXEC RUN_REF_03B_2020 @DATA_MONTH, @BILLING_CYCLE_ID, @AREA_ID
ELSE IF @DATA_MONTH
        BETWEEN '2019-01-01' AND '2020-01-01'
    EXEC RUN_REF_03B_2019 @DATA_MONTH, @BILLING_CYCLE_ID, @AREA_ID
ELSE IF @DATA_MONTH
        BETWEEN '2017-03-01' AND '2018-12-01'
    EXEC RUN_REF_03B_2017 @DATA_MONTH, @BILLING_CYCLE_ID, @AREA_ID
ELSE IF @DATA_MONTH
        BETWEEN '2016-03-01' AND '2017-02-01'
    EXEC RUN_REF_03B_2016 @DATA_MONTH, @BILLING_CYCLE_ID, @AREA_ID
-- END RUN_REF_03B
GO

IF OBJECT_ID('RUN_REF_03B_2022') IS NOT NULL
	DROP PROC RUN_REF_03B_2022
GO

CREATE PROC dbo.RUN_REF_03B_2022
    @DATA_MONTH DATETIME = '2022-01-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
-- CLEAR DATA
DELETE FROM TBL_REF_03B
WHERE DATA_MONTH = @DATA_MONTH

DECLARE @TYPE_SALE_CUSTOMER_USAGE_0_10 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_11_50 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_USAGE_51_200 NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SCHOOL NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_AGRI NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_SPECIAL NVARCHAR(200),
        @LICENSE_TYPE_ID INT,
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5),
        @TYPE_CUSTOMER_NORMAL NVARCHAR(200),
        @TYPE_CUSTOMER_FAVOR NVARCHAR(200),
        @TYPE_CUSTOMER_MV NVARCHAR(200),
        @TYPE_CUSTOMER_LV_LICENSE NVARCHAR(200),
        @TYPE_CUSTOMER_LV_CUSTOMER_BUY NVARCHAR(200),
        @TYPE_CUSTOMER_LV_GENERAL NVARCHAR(200)
SET @TYPE_CUSTOMER_NORMAL = N'  1' + SPACE(4) + N'អតិថិជនលំនៅដ្ឋាន'
SET @TYPE_CUSTOMER_FAVOR = N'  2' + SPACE(4) + N'អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស'
SET @TYPE_SALE_CUSTOMER_USAGE_0_10 = N'1.1' + N' លំនៅដ្ឋានប្រើ មិនលើស  10 kWh/ខែ'
SET @TYPE_SALE_CUSTOMER_USAGE_11_50 = N'1.2' + N' លំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ'
SET @TYPE_SALE_CUSTOMER_USAGE_51_200 = N'1.3' + N' លំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ'
SET @TYPE_SALE_CUSTOMER_SCHOOL = N'2.1' + N' សាលារៀន, មន្ទីរពេទ្យ, និងមណ្ឌលសុខភាព នៅតំបន់ជនបទ'
SET @TYPE_SALE_CUSTOMER_AGRI = N'2.2' + N' អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម(ម៉ោង៩យប់ដល់៧ព្រឹក)'
SET @TYPE_CUSTOMER_MV = N'  2.2.1' + N'  អ្នកប្រើប្រាស់ MV'
SET @TYPE_CUSTOMER_LV_LICENSE = N'  2.2.2' + N'  អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកលក់)'
SET @TYPE_CUSTOMER_LV_CUSTOMER_BUY = N'  2.2.3' + N'  អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកទិញ)'
SET @TYPE_CUSTOMER_LV_GENERAL = N'  2.2.4' + N'  អ្នកប្រើប្រាស់ LV (ភ្ជាប់ពីខ្សែសាធារណៈ)'

SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @BASED_TARIFF = BASED_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH

IF (@LICENSE_TYPE_ID <> 2)
BEGIN
    INSERT INTO TBL_REF_03B
    (
        DATA_MONTH,
        ROW_NO,
        TYPE_OF_SALE_ID,
        TYPE_OF_SALE_NAME,
        TOTAL_CUSTOMER,
        TOTAL_POWER_SOLD,
        PRICE,
        SUBSIDY_RATE,
        SUBSIDY_AMOUNT,
        CURRENCY_ID,
        CURRENCY_NAME,
        CURRENCY_SIGN,
        IS_ACTIVE,
        TYPE_ID,
        TYPE_NAME
    )
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.SUBSIDY_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.1370 * 4130, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.1370 * 4130, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.15048 * 4130, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.15048 * 4130, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.3',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.14248 * 4130, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.14248 * 4130, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.4',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(730, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(730, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF >= @SUBSIDY_TARIFF)
    GROUP BY t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN

    --WHEN BASE TARFF < SUBSIDY TARIFF
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.1',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.2',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'1.3',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(t.BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(t.BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.1',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.1370 * 4130, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.1370 * 4130, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.2',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.15048 * 4130, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.15048 * 4130, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.3',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(0.14248 * 4130, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(ROUND(0.14248 * 4130, 0), 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           ROW_NO = N'2.2.4',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_SOLD = SUM(i.TOTAL_USAGE),
           RATE = i.PRICE,
           SUBSIDY_RATE = ISNULL(@BASED_TARIFF, 0) - i.PRICE,
           SUBSIDY_AMOUNT = (ISNULL(@BASED_TARIFF, 0) - i.PRICE) * SUM(i.TOTAL_USAGE),
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           N'៛',
           IS_ACTIVE = 1,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    FROM TBL_REF_INVOICE i
        LEFT JOIN TBL_TARIFF t
            ON t.CURRENCY_ID = i.CURRENCY_ID
               AND t.DATA_MONTH = @DATA_MONTH
    WHERE i.DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
          AND (@BASED_TARIFF < @SUBSIDY_TARIFF)
    GROUP BY t.BASED_TARIFF,
             t.SUBSIDY_TARIFF,
             i.PRICE,
             i.CURRENCY_ID,
             i.CURRENCY_NAME,
             i.CURRENCY_SIGN

    -- BLANK RECORD
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.1',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_0_10,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.2',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_11_50,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '1.3'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'1.3',
               TYPE_OF_SALE_ID = 1,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_USAGE_51_200,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 1,
               TYPE_NAME = @TYPE_CUSTOMER_NORMAL
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.1',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_SCHOOL,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_AGRI,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.1'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.1',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.2'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.2',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_LICENSE,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.3'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.3',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_CUSTOMER_BUY,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR
    IF NOT EXISTS
    (
        SELECT *
        FROM TBL_REF_03B
        WHERE DATA_MONTH = @DATA_MONTH
              AND ROW_NO = '2.2.4'
    )
        INSERT INTO TBL_REF_03B
        (
            DATA_MONTH,
            ROW_NO,
            TYPE_OF_SALE_ID,
            TYPE_OF_SALE_NAME,
            TOTAL_CUSTOMER,
            TOTAL_POWER_SOLD,
            PRICE,
            SUBSIDY_RATE,
            SUBSIDY_AMOUNT,
            CURRENCY_ID,
            CURRENCY_NAME,
            CURRENCY_SIGN,
            IS_ACTIVE,
            TYPE_ID,
            TYPE_NAME
        )
        SELECT DATA_MONTH = @DATA_MONTH,
               ROW_NO = N'2.2.4',
               TYPE_OF_SALE_ID = 2,
               TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_GENERAL,
               TOTAL_CUSTOMER = 0,
               TOTAL_SOLD = 0,
               RATE = 0,
               SUBSIDY_RATE = 0,
               SUBSIDY_AMOUNT = 0,
               CURRENCY_ID = 1,
               CURRENCY_NAME = '',
               CURRENCY_SIGN = '',
               IS_ACTIVE = 1,
               TYPE_ID = 2,
               TYPE_NAME = @TYPE_CUSTOMER_FAVOR
END
-- END RUN_REF_03B_2022
GO

IF OBJECT_ID('RUN_REF_02') IS NOT NULL
	DROP PROC RUN_REF_02
GO

CREATE PROC dbo.RUN_REF_02
    @DATA_MONTH DATETIME = '2022-02-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
IF @DATA_MONTH
	BETWEEN '2022-01-01' AND '9999-12-31'
	EXEC RUN_REF_02_2022 @DATA_MONTH, @BILLING_CYCLE_ID, @AREA_ID
ELSE IF @DATA_MONTH
   BETWEEN '2021-01-01' AND '2021-12-01'
    EXEC RUN_REF_02_2021 @DATA_MONTH, @BILLING_CYCLE_ID, @AREA_ID
ELSE IF @DATA_MONTH
        BETWEEN '2020-02-01' AND '2020-12-01'
    EXEC RUN_REF_02_2020 @DATA_MONTH, @BILLING_CYCLE_ID, @AREA_ID
ELSE IF @DATA_MONTH
        BETWEEN '2019-01-01' AND '2020-01-01'
    EXEC RUN_REF_02_2019 @DATA_MONTH, @BILLING_CYCLE_ID, @AREA_ID
ELSE IF @DATA_MONTH
        BETWEEN '2017-03-01' AND '2018-12-01'
    EXEC RUN_REF_02_2017 @DATA_MONTH, @BILLING_CYCLE_ID, @AREA_ID
ELSE IF @DATA_MONTH
        BETWEEN '2016-03-01' AND '2017-02-01'
    EXEC RUN_REF_02_2016 @DATA_MONTH, @BILLING_CYCLE_ID, @AREA_ID
--END OF RUN_REF_02
GO

IF OBJECT_ID('RUN_REF_02_2022') IS NOT NULL
	DROP PROC RUN_REF_02_2022
GO

CREATE PROC dbo.RUN_REF_02_2022
    @DATA_MONTH DATETIME = '2022-01-01',
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS

-- CLEAR RESULT
DELETE FROM TBL_REF_02
WHERE DATA_MONTH = @DATA_MONTH
DECLARE @LICENSE_TYPE_ID INT,
        @TYPE_LICENSEE NVARCHAR(200),
        @TYPE_LICENSEE_MV NVARCHAR(200),
        @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO NVARCHAR(200),
        @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO NVARCHAR(200),

        --1.​ ឧស្សាហកម្មវិស័យរុករករ៉ែ
        @TYPE_MINING_INDUSTRY NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_SALE_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_SUN NVARCHAR(200),
        @TYPE_MINING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --2.​ ឧស្សាហកម្មវិស័យកម្មន្តសាល 
        @TYPE_MANUFACTURING_INDUSTRY NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_SALE_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_SUN NVARCHAR(200),
        @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --3.​ ឧស្សាហកម្មវិស័យវាយនភណ្ឌ
        @TYPE_TEXTILE_INDUSTRY NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_SALE_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_SUN NVARCHAR(200),
        @TYPE_TEXTILE_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --4.​ កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ
        @TYPE_AGRICULTURE NVARCHAR(200),
        @TYPE_AGRICULTURE_MV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_SUN NVARCHAR(200),
        @TYPE_AGRICULTURE_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --5.​ សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។)
        @TYPE_ACTIVITIES NVARCHAR(200),
        @TYPE_ACTIVITIES_MV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_SALE_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_SUN NVARCHAR(200),
        @TYPE_ACTIVITIES_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),


        --6.​ សកម្មភាពកែច្នៃផលិតផលកសិកម្ម
        @TYPE_AGRICULTURAL NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_SUN NVARCHAR(200),
        @TYPE_AGRICULTURAL_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --1.ពាណិជ្ជកម្ម 
        @TYPE_BUSINESS NVARCHAR(200),
        @TYPE_BUSINESS_MV NVARCHAR(200),
        @TYPE_BUSINESS_MV_SALE_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_BUSINESS_MV_SUN NVARCHAR(200),
        @TYPE_BUSINESS_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),

        --2.រដ្ឋបាលសាធារណៈ 
        @TYPE_PUBLIC_ADMINISTRATION NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_SUN NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_RIEL NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV_RIEL NVARCHAR(200),
        @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV_RIEL NVARCHAR(200),
		

        --3.សេវាកម្មផ្សេងៗ 
        @TYPE_OTHER_SERVICES NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_SALE_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_SALE_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_SALE_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_SUN NVARCHAR(200),
        @TYPE_OTHER_SERVICES_MV_SUN_CUSTOMER_BUY_LV NVARCHAR(200),
        @TYPE_CUSTOMER_SPECIAl_FAVOR NVARCHAR(200),
        @TYPE_INDUSTRY_NO_SUBSIDY NVARCHAR(200),
        @TYPE_COMMERCIAL_NO_SUBSIDY NVARCHAR(200),
        @TYPE_COMMERCIAL_NO_SUBSIDY_SALE_LV_USAGE_OVER_2000 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000 NVARCHAR(200),
        @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER NVARCHAR(200),
        @TYPE_SUBSIDY NVARCHAR(200),
        @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200 NVARCHAR(200),
        @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200 NVARCHAR(200),
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5),
        @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR NVARCHAR(200),
        @TYPE_SALE_CUSTOMER_MARKET_VENDOR NVARCHAR(200),
        @TYPE_CUSTOMER_AGRICULTURE NVARCHAR(200),
        @TYPE_CUSTOMER_MV_AGRICULTURE NVARCHAR(200),
        @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM NVARCHAR(200),
        @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM_9_TO_7 NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM_9_TO_7 NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7 NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_GENERAL NVARCHAR(200),
        @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_9_TO_7 NVARCHAR(200)

--CHECK LICENSE TYPE ID
SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @BASED_TARIFF = BASED_TARIFF,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH
SET @TYPE_LICENSEE = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ'
SET @TYPE_LICENSEE_MV = N'1. ថាមពលលក់ភ្ជាប់ពីខ្សែបណ្តាញតង់ស្យុងមធ្យម MV'
SET @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO = N'2. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកទិញ)'
SET @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO = N'3. ថាមពលលក់ភ្ជាប់ពីក្រោមត្រង់ស្វូចែកចាយតាមកុងទ័រ LV (ត្រង់ស្វូអ្នកលក់)'

SET @TYPE_INDUSTRY_NO_SUBSIDY = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (អ្នកប្រើប្រាស់ធុនធំ និងធុនមធ្យម ប្រភេទ ឧស្សាហកម្ម និងកសិកម្ម)'
SET @TYPE_MINING_INDUSTRY = N'1.​ ឧស្សាហកម្មវិស័យរុករករ៉ែ'
SET @TYPE_MINING_INDUSTRY_MV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV'
SET @TYPE_MINING_INDUSTRY_MV_SALE_LV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកលក់'
SET @TYPE_MINING_INDUSTRY_MV_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)'
SET @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)'
SET @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_MINING_INDUSTRY_MV_SUN = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
SET @TYPE_MINING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ក្រោមត្រង់ស្វូ'

SET @TYPE_MANUFACTURING_INDUSTRY = N'2.​ ឧស្សាហកម្មវិស័យកម្មន្តសាល'
SET @TYPE_MANUFACTURING_INDUSTRY_MV = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV'
SET @TYPE_MANUFACTURING_INDUSTRY_MV_SALE_LV = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកលក់'
SET @TYPE_MANUFACTURING_INDUSTRY_MV_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)'
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)'
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_MANUFACTURING_INDUSTRY_MV_SUN = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
SET @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ក្រោមត្រង់ស្វូ'

SET @TYPE_TEXTILE_INDUSTRY = N'3.​ ឧស្សាហកម្មវិស័យវាយនភណ្ឌ'
SET @TYPE_TEXTILE_INDUSTRY_MV = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV'
SET @TYPE_TEXTILE_INDUSTRY_MV_SALE_LV = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកលក់'
SET @TYPE_TEXTILE_INDUSTRY_MV_CUSTOMER_BUY_LV = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)'
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)'
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_TEXTILE_INDUSTRY_MV_SUN = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
SET @TYPE_TEXTILE_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV
    = N'	- ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ក្រោមត្រង់ស្វូ'

SET @TYPE_AGRICULTURE = N'4.​ កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ'
SET @TYPE_AGRICULTURE_MV = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV'
SET @TYPE_AGRICULTURE_MV_SALE_LV = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកលក់'
SET @TYPE_AGRICULTURE_MV_CUSTOMER_BUY_LV = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)'
SET @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)'
SET @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_AGRICULTURE_MV_SUN = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
SET @TYPE_AGRICULTURE_MV_SUN_CUSTOMER_BUY_LV
    = N'	- កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ក្រោមត្រង់ស្វូ'

SET @TYPE_ACTIVITIES = N'5.​ សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ។ល។)'
SET @TYPE_ACTIVITIES_MV = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV'
SET @TYPE_ACTIVITIES_MV_SALE_LV = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
SET @TYPE_ACTIVITIES_MV_CUSTOMER_BUY_LV = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)'
SET @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)'
SET @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_ACTIVITIES_MV_SUN = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
SET @TYPE_ACTIVITIES_MV_SUN_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ក្រោមត្រង់ស្វូ'

SET @TYPE_AGRICULTURAL = N'6.​ សកម្មភាពកែច្នៃផលិតផលកសិកម្ម'
SET @TYPE_AGRICULTURAL_MV = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV'
SET @TYPE_AGRICULTURAL_MV_SALE_LV = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
SET @TYPE_AGRICULTURAL_MV_CUSTOMER_BUY_LV = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)'
SET @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)'
SET @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_AGRICULTURAL_MV_SUN = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
SET @TYPE_AGRICULTURAL_MV_SUN_CUSTOMER_BUY_LV
    = N'	- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ក្រោមត្រង់ស្វូ'


SET @TYPE_COMMERCIAL_NO_SUBSIDY
    = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (អ្នកប្រើប្រាស់ធុនធំ និងធុនមធ្យម ប្រភេទពាណិជ្ជកម្ម រដ្ឋបាល និងផ្សេងៗ)'
SET @TYPE_BUSINESS = N'1.​ ពាណិជ្ជកម្ម'
SET @TYPE_BUSINESS_MV = N'	- ពាណិជ្ជកម្ម ទិញ MV'
SET @TYPE_BUSINESS_MV_SALE_LV = N'	- ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
SET @TYPE_BUSINESS_MV_CUSTOMER_BUY_LV = N'	- ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT = N'	- ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)'
SET @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING = N'	- ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)'
SET @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_BUSINESS_MV_SUN = N'	- ពាណិជ្ជកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
SET @TYPE_BUSINESS_MV_SUN_CUSTOMER_BUY_LV
    = N'	- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ក្រោមត្រង់ស្វូ'

SET @TYPE_PUBLIC_ADMINISTRATION = N'2.​ រដ្ឋបាលសាធារណៈ'
SET @TYPE_PUBLIC_ADMINISTRATION_MV = N'	- រដ្ឋបាលសាធារណៈ ទិញ MV'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកលក់'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT = N'	- រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING = N'	- រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_SUN = N'	- រដ្ឋបាលសាធារណៈ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_CUSTOMER_BUY_LV
    = N'	- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ក្រោមត្រង់ស្វូ'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_RIEL = N'	- រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញMV'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV_RIEL = N'	- រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកលក់'
SET @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV_RIEL = N'	- រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកទិញ'

SET @TYPE_OTHER_SERVICES = N'3.​ សេវាកម្មផ្សេងៗ'
SET @TYPE_OTHER_SERVICES_MV = N'	- សេវាកម្មផ្សេងៗ ទិញ MV'
SET @TYPE_OTHER_SERVICES_MV_SALE_LV = N'	- សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកលក់'
SET @TYPE_OTHER_SERVICES_MV_CUSTOMER_BUY_LV = N'	- សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT = N'	- សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)'
SET @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_SALE_LV
    = N'	- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV
    = N'	- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING = N'	- សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)'
SET @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_SALE_LV
    = N'	- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
SET @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV
    = N'	- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
SET @TYPE_OTHER_SERVICES_MV_SUN = N'	- សេវាកម្មផ្សេងៗ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
SET @TYPE_OTHER_SERVICES_MV_SUN_CUSTOMER_BUY_LV
    = N'	- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ក្រោមត្រង់ស្វូ'

SET @TYPE_COMMERCIAL_NO_SUBSIDY_SALE_LV_USAGE_OVER_2000
    = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែបណ្តាញសាធារណៈ)'
SET @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000
    = N'1. អតិថិជន ឧស្សាហកម្ម&កសិកម្ម, ពាណិជ្ជកម្ម រដ្ឋបាល&សេវាកម្ម ប្រើចាប់ពី 2001 kWh/ខែ ឡើង'

SET @TYPE_SUBSIDY = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)'
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL = N'1. ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន'
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10 = SPACE(9) + N'1.1.' + N' ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើ មិនលើសពី 10 kWh/ខែ'
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50 = SPACE(9) + N'1.2.' + N' ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ'
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200
    = SPACE(9) + N'1.3.' + N' ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ'
SET @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200 = SPACE(9) + N'1.4.' + N' ថាមពលលក់ឲ្យលំនៅដ្ឋានប្រើចាប់ពី 201 kWh/ខែ'
SET @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001
    = N'2. អតិថិជន ឧស្សាហកម្ម&កសិកម្ម, ពាណិជ្ជកម្ម រដ្ឋបាល&សេវាកម្ម ប្រើក្រោម 2001 kWh/ខែ '
SET @TYPE_CUSTOMER_SPECIAl_FAVOR = N'3. អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស'
SET @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER
    = SPACE(9) + N'3.1.' + N' អតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ'
SET @TYPE_CUSTOMER_AGRICULTURE
    = N' អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ភ្ជាប់ពីខ្សែបណ្តាញចែកចាយ MV និង LV '
SET @TYPE_CUSTOMER_MV_AGRICULTURE = N'1. អ្នកប្រើប្រាស់ MV'
SET @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM = N'	- ទិញ MV តាមអត្រាថ្លៃមធ្យម'
SET @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM_9_TO_7 = N'	- ទិញ MV តាមអត្រាថ្លៃអនុគ្រោះ (ម៉ោង៩យប់ដល់៧ព្រឹក)'
SET @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY = N'2. អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកលក់)'
SET @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM = N'	- ទិញ LV ត្រង់ស្វូរអ្នកលក់ តាមអត្រាថ្លៃមធ្យម'
SET @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM_9_TO_7
    = N'	- ទិញ LV ត្រង់ស្វូរអ្នកលក់ តាមអត្រាថ្លៃអនុគ្រោះ (ម៉ោង៩យប់ដល់៧ព្រឹក)'
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY = N'3. អ្នកប្រើប្រាស់ LV (ត្រង់ស្វូរអ្នកទិញ)'
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM = N'	- ទិញ LV ត្រង់ស្វូរអ្នកទិញ តាមអត្រាថ្លៃមធ្យម'
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7
    = N'	- ទិញ LV ត្រង់ស្វូរអ្នកទិញ តាមអត្រាថ្លៃអនុគ្រោះ (ម៉ោង៩យប់ដល់៧ព្រឹក)'
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER = N'4. អ្នកប្រើប្រាស់ LV (ភ្ជាប់ពីខ្សែសាធារណៈ)'
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_GENERAL = N'	- ទិញតាមអត្រាថ្លៃទូទៅ'
SET @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_9_TO_7 = N'	- ទិញតាមអត្រាថ្លៃអនុគ្រោះ (៩យប់ដល់៧ព្រឹក)'
SET @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ'
SET @TYPE_SALE_CUSTOMER_MARKET_VENDOR = N'1. ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ'

IF (@LICENSE_TYPE_ID = 1)
BEGIN
    -- Licensee Customer(MV)
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 6,
           TYPE_NAME = @TYPE_LICENSEE,
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = @TYPE_LICENSEE_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    INTO #RESULT
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 11
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- Licensee Customer(buyer's transformer)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 6,
           TYPE_NAME = @TYPE_LICENSEE,
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 12
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- Licensee Customer(license's transformer)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 6,
           TYPE_NAME = @TYPE_LICENSEE,
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 13
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ 2022
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 77,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 26
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 76,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 27
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 75,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 28
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 74,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 29
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 73,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 30
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 72,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 31
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 71,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 32
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 70,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 33
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 69,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 34
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 68,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 35
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 66,
           TYPE_OF_SALE_NAME = @TYPE_MINING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 37
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --END ឧស្សាហកម្មវិស័យរុករករ៉ែ 2022

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល 2022
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 64,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 38
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 63,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 39
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 62,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 40
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 61,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 41
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 60,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 42
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 59,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 43
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 58,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 44
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 57,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 45
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 56,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 46
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 55,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 47
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 53,
           TYPE_OF_SALE_NAME = @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 49
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END ឧស្សាហកម្មវិស័យកម្មន្តសាល 2022

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ 2022
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 51,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 50
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 50,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 51
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 49,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 52
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 48,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 53
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 47,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 54
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 46,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 55
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 45,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 56
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 44,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 57
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 43,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 58
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 42,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 59
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 40,
           TYPE_OF_SALE_NAME = @TYPE_TEXTILE_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 61
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END ឧស្សាហកម្មវិស័យវាយនភណ្ឌ 2022

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ 2022
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 38,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 62
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 37,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 63
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 36,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 64
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 35,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 65
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 34,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 66
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 33,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 67
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 32,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 68
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 31,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 69
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 30,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 70
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 29,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 71
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 27,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURE_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 73
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ 2022

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) 2022
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 25,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 74
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 24,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 75
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 23,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 76
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 22,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 77
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 21,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 78
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 20,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 79
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 19,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 80
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 18,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 81
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 17,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 82
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 16,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 83
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 14,
           TYPE_OF_SALE_NAME = @TYPE_ACTIVITIES_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 85
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END  សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។) 2022


    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម 2022
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 86
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 87
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 88
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 89
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 90
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 91
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 92
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 93
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 94
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 95
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 5,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_AGRICULTURAL_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 97
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END សកម្មភាពកែច្នៃផលិតផលកសិកម្ម 2022


    -- ពាណិជ្ជកម្ម 2022
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 38,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 98
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 37,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 99
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 36,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 100
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 35,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 101
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 34,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 102
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 33,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 103
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 32,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 104
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 31,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 105
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 30,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 106
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 29,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 107
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- ពាណិជ្ជកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 28,
           TYPE_OF_SALE_NAME = @TYPE_BUSINESS_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 109
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END ពាណិជ្ជកម្ម 2022


    -- រដ្ឋបាលសាធារណៈ 2022
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 26,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 110
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 25,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 111
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 24,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 112
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 23,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 113
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 22,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 114
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 21,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 115
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 20,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 116
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 19,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 117
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 18,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 118
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 18,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 119
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- រដ្ឋបាលសាធារណៈ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 16,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 121
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

	--រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញMV
	UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 15,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_RIEL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 145
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 14,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV_RIEL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 146
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 13,
           TYPE_OF_SALE_NAME = @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV_RIEL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 147
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    -- END រដ្ឋបាលសាធារណៈ 2022


    -- សេវាកម្មផ្សេងៗ 2022
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 122
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកលក់
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 123
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    --សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 124
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN


    -- សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 125
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN



    -- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 126
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៧ព្រឹកដល់៩យប់) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 127
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 128
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកលក់ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_SALE_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 129
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 130
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ MV 
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_SUN,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 131
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- សេវាកម្មផ្សេងៗ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញ LV ត្រង់ស្វូអ្នកទិញ
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 4,
           TYPE_NAME = @TYPE_INDUSTRY_NO_SUBSIDY,
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_OTHER_SERVICES_MV_SUN_CUSTOMER_BUY_LV,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 133
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- END សេវាកម្មផ្សេងៗ 2022

    -- for Normal Customer(TOTAL_USAGE>=2001) COMMERCIAL 2022
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 3,
           TYPE_NAME = @TYPE_COMMERCIAL_NO_SUBSIDY_SALE_LV_USAGE_OVER_2000,
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140, 144 )
          AND i.TOTAL_USAGE >= 2001
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Total Normal Customer
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -12,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = BASED_PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY BASED_PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE<=10)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -13,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE BETWEEN 11 AND 50)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -14,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND (i.TOTAL_USAGE
          BETWEEN 11 AND 50
              )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE BETWEEN 51 AND 200)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -15,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND (i.TOTAL_USAGE
          BETWEEN 51 AND 200
              )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE>=200)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -16,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer(TOTAL_USAGE<2001)
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -17,
           TYPE_OF_SALE_NAME = @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = BASED_PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140, 144 )
          AND i.TOTAL_USAGE < 2001
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY BASED_PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    -- for Normal Customer SPECIAL
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 2,
           TYPE_NAME = @TYPE_SUBSIDY,
           TYPE_OF_SALE_ID = -19,
           TYPE_OF_SALE_NAME = @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -101,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 0.1370
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -102,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM_9_TO_7,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -104,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 0.15048
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -105,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM_9_TO_7,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -107,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           i.CURRENCY_ID,
           i.CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 0.14248
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -108,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -110,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_GENERAL,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 730
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    UNION ALL
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_CUSTOMER_AGRICULTURE,
           TYPE_OF_SALE_ID = -111,
           TYPE_OF_SALE_NAME = @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           i.CURRENCY_SIGN,
           IS_ACTIVE = 1
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN

    CREATE TABLE #DEFAULT_RESULT
    (
        DATA_MONTH DATETIME,
        TYPE_ID INT,
        TYPE_NAME NVARCHAR(200),
        TYPE_OF_SALE_ID INT,
        TYPE_OF_SALE_NAME NVARCHAR(200),
        TOTAL_CUSTOMER INT,
        TOTAL_POWER_SOLD INT,
        RATE DECIMAL(18, 4),
        CURRENCY_ID INT,
        CURRENCY_NAME NVARCHAR(20),
        CURRENCY_SIGN NVARCHAR(5),
        IS_ACTIVE BIT
    )
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 6, @TYPE_LICENSEE, 5, @TYPE_LICENSEE_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 6, @TYPE_LICENSEE, 4, @TYPE_LICENSEE_MV_LV_CUSTOMER_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 6, @TYPE_LICENSEE, 3, @TYPE_LICENSEE_MV_LV_LICENSEE_TRANSFO, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 78, @TYPE_MINING_INDUSTRY, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 77, @TYPE_MINING_INDUSTRY_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 76, @TYPE_MINING_INDUSTRY_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 75, @TYPE_MINING_INDUSTRY_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 74, @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 73, @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 72, @TYPE_MINING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 71, @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 70, @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 69, @TYPE_MINING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 68, @TYPE_MINING_INDUSTRY_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 66, @TYPE_MINING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    --END ឧស្សាហកម្មវិស័យរុករករ៉ែ


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 65, @TYPE_MANUFACTURING_INDUSTRY, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 64, @TYPE_MANUFACTURING_INDUSTRY_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 63, @TYPE_MANUFACTURING_INDUSTRY_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 62, @TYPE_MANUFACTURING_INDUSTRY_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 61, @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 60, @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0,
     0  , 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 59,
     @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 58, @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 57, @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0,
     0  , 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 56,
     @TYPE_MANUFACTURING_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 55, @TYPE_MANUFACTURING_INDUSTRY_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 53, @TYPE_MANUFACTURING_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    --END ឧស្សាហកម្មវិស័យកម្មន្តសាល


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 52, @TYPE_TEXTILE_INDUSTRY, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 51, @TYPE_TEXTILE_INDUSTRY_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 50, @TYPE_TEXTILE_INDUSTRY_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 49, @TYPE_TEXTILE_INDUSTRY_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 48, @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 47, @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 46, @TYPE_TEXTILE_INDUSTRY_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 45, @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 44, @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 43, @TYPE_TEXTILE_INDUSTRY_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0,
     0  , 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 42, @TYPE_TEXTILE_INDUSTRY_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 40, @TYPE_TEXTILE_INDUSTRY_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    --END ឧស្សាហកម្មវិស័យវាយនភណ្ឌ


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 39, @TYPE_AGRICULTURE, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 38, @TYPE_AGRICULTURE_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 37, @TYPE_AGRICULTURE_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 36, @TYPE_AGRICULTURE_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  )
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 35, @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 34, @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 33, @TYPE_AGRICULTURE_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 32, @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 31, @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 30, @TYPE_AGRICULTURE_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 29, @TYPE_AGRICULTURE_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 27, @TYPE_AGRICULTURE_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    --END កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 26, @TYPE_ACTIVITIES, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 25, @TYPE_ACTIVITIES_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 24, @TYPE_ACTIVITIES_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 23, @TYPE_ACTIVITIES_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 22, @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 21, @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 20, @TYPE_ACTIVITIES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 19, @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 18, @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 17, @TYPE_ACTIVITIES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 16, @TYPE_ACTIVITIES_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 14, @TYPE_ACTIVITIES_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    --END សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។)



    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 13, @TYPE_AGRICULTURAL, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 12, @TYPE_AGRICULTURAL_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 11, @TYPE_AGRICULTURAL_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 10, @TYPE_AGRICULTURAL_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  )
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 9, @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 8, @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 7, @TYPE_AGRICULTURAL_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 6, @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 5, @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 4, @TYPE_AGRICULTURAL_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 3, @TYPE_AGRICULTURAL_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 5, @TYPE_INDUSTRY_NO_SUBSIDY, 1, @TYPE_AGRICULTURAL_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    --END សកម្មភាពកែច្នៃផលិតផលកសិកម្ម


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 39, @TYPE_BUSINESS, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 38, @TYPE_BUSINESS_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 37, @TYPE_BUSINESS_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 36, @TYPE_BUSINESS_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 35, @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 34, @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 33, @TYPE_BUSINESS_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 32, @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 31, @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2,
     N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 30, @TYPE_BUSINESS_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0,
     2  , N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 29, @TYPE_BUSINESS_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 28, @TYPE_BUSINESS_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា',
     N'$', 1)
    --END ពាណិជ្ជកម្ម


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 27, @TYPE_PUBLIC_ADMINISTRATION, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 26, @TYPE_PUBLIC_ADMINISTRATION_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 25, @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 24, @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 23, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 22, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0  , 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 21, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 20, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2,​N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 19, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0,​0  , 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 18, @TYPE_PUBLIC_ADMINISTRATION_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 17, @TYPE_PUBLIC_ADMINISTRATION_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$',​1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 16, @TYPE_PUBLIC_ADMINISTRATION_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2,​N'ដុល្លា', N'$', 1)
	INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 15, @TYPE_PUBLIC_ADMINISTRATION_MV_RIEL, 0, 0, 0, 2, N'រៀល', N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 14, @TYPE_PUBLIC_ADMINISTRATION_MV_SALE_LV_RIEL, 0, 0, 0, 2, N'រៀល', N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 13, @TYPE_PUBLIC_ADMINISTRATION_MV_CUSTOMER_BUY_LV_RIEL, 0, 0, 0, 2,​N'រៀល', N'៛', 1)
    --END រដ្ឋបាលសាធារណៈ


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 12, @TYPE_OTHER_SERVICES, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 11, @TYPE_OTHER_SERVICES_MV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 10, @TYPE_OTHER_SERVICES_MV_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 9, @TYPE_OTHER_SERVICES_MV_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 8, @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 7, @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 6, @TYPE_OTHER_SERVICES_MV_TOU_MORNING_TO_NIGHT_CUSTOMER_BUY_LV, 0, 0  , 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 5, @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 4, @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_SALE_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 3, @TYPE_OTHER_SERVICES_MV_TOU_NIGHT_TO_MORNING_CUSTOMER_BUY_LV, 0, 0  , 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 2, @TYPE_OTHER_SERVICES_MV_SUN, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 4, @TYPE_COMMERCIAL_NO_SUBSIDY, 1, @TYPE_OTHER_SERVICES_MV_SUN_CUSTOMER_BUY_LV, 0, 0, 0, 2, N'ដុល្លា', N'$', 1)
    --END សេវាកម្មផ្សេងៗ

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 3, @TYPE_COMMERCIAL_NO_SUBSIDY_SALE_LV_USAGE_OVER_2000, 1, @TYPE_COMMERCIAL_SALE_LV_USAGE_OVER_2000,
     0  , 0, 0, 1, N'រៀល', N'៛', 1)


    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -12, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_NORMAL, 0, 0, 0, 1, N'រៀល', N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -13, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_0_10, 0, 0, 0, 1, N'រៀល', N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -14, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_11_50, 0, 0, 0, 1, N'រៀល', N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -15, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_51_200, 0, 0, 0, 1, N'រៀល', N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -16, @TYPE_SUBSIDY_SALE_CUSTOMER_USAGE_OVER_200, 0, 0, 0, 1, N'រៀល', N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -17, @TYPE_SUBSIDY_INDUSTRY_COMMERCIAL_SALE_LV_USAGE_UNDER_2001, 0, 0, 0, 1,
     N'រៀល', N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -18, @TYPE_CUSTOMER_SPECIAl_FAVOR, 0, 0, 0, 1, N'រៀល', N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 2, @TYPE_SUBSIDY, -19, @TYPE_COMMERCIAL_SALE_SCHOOL_HOSPITAL_HEALTH_CENTER_CUSTOMER, 0, 0, 0, 1,
     N'រៀល', N'៛', 1)

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -100, @TYPE_CUSTOMER_MV_AGRICULTURE, 0, 0, 0, 1, N'រៀល', N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -101, @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM, 0, 0, 0, 2, N'ដុល្លា', N'$',
     1  )
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -102, @TYPE_CUSTOMER_MV_AGRICULTURE_MEDIUM_9_TO_7, 0, 0, 0, 1, N'រៀល',
     N'៛', 1)

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -103, @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY, 0, 0, 0, 1, N'រៀល',
     N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -104, @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM, 0, 0, 0, 1, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -105, @TYPE_CUSTOMER_LV_AGRICULTURE_LICENSE_BUY_MEDIUM_9_TO_7, 0, 0,
     0  , 1, N'រៀល', N'៛', 1)

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -106, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY, 0, 0, 0, 1, N'រៀល',
     N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -107, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM, 0, 0, 0, 1, N'ដុល្លា', N'$', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -108, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_BUY_MEDIUM_9_TO_7, 0, 0,
     0  , 1, N'រៀល', N'៛', 1)

    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -109, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER, 0, 0, 0, 1, N'រៀល',
     N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -110, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_GENERAL, 0, 0, 0, 1,
     N'រៀល', N'៛', 1)
    INSERT INTO #DEFAULT_RESULT
    VALUES
    (@DATA_MONTH, 1, @TYPE_CUSTOMER_AGRICULTURE, -111, @TYPE_CUSTOMER_LV_AGRICULTURE_CUSTOMER_9_TO_7, 0, 0, 0, 1,
     N'រៀល', N'៛', 1)

    -- RESULT
    INSERT INTO TBL_REF_02
    SELECT *
    FROM #RESULT
    UNION ALL
    SELECT *
    FROM #DEFAULT_RESULT t
    WHERE NOT EXISTS
    (
        SELECT *
        FROM #RESULT
        WHERE TYPE_ID = t.TYPE_ID
              AND TYPE_OF_SALE_ID = t.TYPE_OF_SALE_ID
    )
    ORDER BY TYPE_ID DESC,
             TYPE_OF_SALE_ID DESC
END
ELSE IF (@LICENSE_TYPE_ID = 2)
BEGIN
    -- Market vendor customer
    SELECT DATA_MONTH = @DATA_MONTH,
           TYPE_ID = 1,
           TYPE_NAME = @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR,
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = @TYPE_SALE_CUSTOMER_MARKET_VENDOR,
           TOTAL_CUSTOMER = COUNT(*),
           TOTAL_POWER_SOLD = SUM(TOTAL_USAGE),
           RATE = PRICE,
           CURRENCY_ID,
           CURRENCY_NAME,
           CURRENCY_SIGN,
           IS_ACTIVE = 1
    INTO #RESULT_MARKET_VENDOR
    FROM TBL_REF_INVOICE i
    WHERE DATA_MONTH = @DATA_MONTH
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 )
          AND
          (
              @BILLING_CYCLE_ID = 0
              OR i.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
          )
          AND
          (
              @AREA_ID = 0
              OR i.AREA_ID = @AREA_ID
          )
    GROUP BY CUSTOMER_GROUP_ID,
             CUSTOMER_GROUP_NAME,
             CUSTOMER_CONNECTION_TYPE_ID,
             CUSTOMER_CONNECTION_TYPE_NAME,
             PRICE,
             CURRENCY_ID,
             CURRENCY_NAME,
             CURRENCY_SIGN
    CREATE TABLE #DEFAULT_RESULT_MARKET_VENDOR
    (
        DATA_MONTH DATETIME,
        TYPE_ID INT,
        TYPE_NAME NVARCHAR(200),
        TYPE_OF_SALE_ID INT,
        TYPE_OF_SALE_NAME NVARCHAR(200),
        TOTAL_CUSTOMER INT,
        TOTAL_POWER_SOLD INT,
        RATE DECIMAL(18, 4),
        CURRENCY_ID INT,
        CURRENCY_NAME NVARCHAR(20),
        CURRENCY_SIGN NVARCHAR(5),
        IS_ACTIVE BIT
    )
    INSERT INTO #DEFAULT_RESULT_MARKET_VENDOR
    VALUES
    (@DATA_MONTH, 1, @TYPE_SUBSIDY_SPECIAL_FOR_MARKET_VENDOR, -1, @TYPE_SALE_CUSTOMER_MARKET_VENDOR, 0, 0, 0, 1,
     N'រៀល', N'៛', 1)

    -- RESULT
    INSERT INTO TBL_REF_02
    SELECT *
    FROM #RESULT_MARKET_VENDOR
    UNION ALL
    SELECT *
    FROM #DEFAULT_RESULT_MARKET_VENDOR t
    WHERE NOT EXISTS
    (
        SELECT *
        FROM #RESULT_MARKET_VENDOR
        WHERE TYPE_ID = t.TYPE_ID
              AND TYPE_OF_SALE_ID = t.TYPE_OF_SALE_ID
    )
    ORDER BY TYPE_ID DESC,
             TYPE_OF_SALE_ID DESC
END
--END OF RUN_REF_02_2022
GO

IF OBJECT_ID('RUN_REF_INVALID_CUSTOMER') IS NOT NULL
	DROP PROC RUN_REF_INVALID_CUSTOMER
GO

CREATE PROC dbo.RUN_REF_INVALID_CUSTOMER 
	@DATA_MONTH DATETIME = '2021-01-01'
AS
IF @DATA_MONTH
   BETWEEN '2022-01-01' AND '9999-12-31'
    EXEC RUN_REF_INVALID_CUSTOMER_2022 @DATA_MONTH
ELSE IF @DATA_MONTH
   BETWEEN '2021-01-01' AND '2021-12-01'
    EXEC RUN_REF_INVALID_CUSTOMER_2021 @DATA_MONTH
ELSE IF @DATA_MONTH
        BETWEEN '2020-02-01' AND '2020-12-01'
    EXEC RUN_REF_INVALID_CUSTOMER_2020 @DATA_MONTH
ELSE IF @DATA_MONTH
        BETWEEN '2019-01-01' AND '2020-01-31'
    EXEC RUN_REF_INVALID_CUSTOMER_2019 @DATA_MONTH
ELSE IF @DATA_MONTH
        BETWEEN '2018-03-01' AND '2018-12-31'
    EXEC RUN_REF_INVALID_CUSTOMER_2018 @DATA_MONTH
ELSE IF @DATA_MONTH
        BETWEEN '2017-03-01' AND '2018-02-01'
    EXEC RUN_REF_INVALID_CUSTOMER_2017 @DATA_MONTH
ELSE IF @DATA_MONTH
        BETWEEN '2016-03-01' AND '2017-02-01'
    EXEC RUN_REF_INVALID_CUSTOMER_2016 @DATA_MONTH
--END OF RUN_REF_INVALID_CUSTOMER
GO

IF OBJECT_ID('RUN_REF_INVALID_CUSTOMER_2022') IS NOT NULL
	DROP PROC RUN_REF_INVALID_CUSTOMER_2022
GO

CREATE PROC dbo.RUN_REF_INVALID_CUSTOMER_2022 
	@DATA_MONTH DATETIME = '2022-02-01'
AS
/*
@2017-09-06 By Morm Raksmey
1. Clear Data before INSERT DATA

@2018-03-23 By Morm Raksmey
1. Update REF for Tariff 2018

@2018-09-03 By Pov Lyhorng
1. Update find invalid customer by Connection type & Customer Gruop
	from TLKP_CUSTOMER_CONNECTION_TYPE & TLKP_CUSTOMER_GROUP

@2019-Jan-17 By Phoung Sovathvong
1. Update for REF 2019

@2019-Dec-02 By Hor Dara
1. Update for REF 2020

@20201-Jan-05 BY Vonn kimputhmunyvorn
1. Update for REF 2021
*/
DELETE FROM TBL_REF_INVALID_CUSTOMER
WHERE DATA_MONTH = @DATA_MONTH
DECLARE @M DECIMAL(18, 4)
DECLARE @LICENSE_TYPE_ID INT

SET @M = 99999999999.9999


IF OBJECT_ID('#TMP_VALIDATE') IS NOT NULL
    DROP TABLE #TMP_VALIDATE
CREATE TABLE #TMP_VALIDATE
(
    DATA_MONTH DATETIME,
    VALIDATE_NAME NVARCHAR(250),
    START_USAGE DECIMAL(18, 4),
    END_USAGE DECIMAL(18, 4),
    PRICE DECIMAL(18, 5),
    BASED_PRICE DECIMAL(18, 5),
    CURRENCY_ID INT,
    CUSTOMER_CONNECTION_TYPE_ID INT,
    CUSTOMER_GROUP_ID INT
)

--CHECK LICENSE TYPE ID
SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID
FROM dbo.TBL_TARIFF

INSERT INTO #TMP_VALIDATE
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
       START_USAGE = 0,
       END_USAGE = 10,
       PRICE = 380,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យលំនៅដ្ឋានប្រើពី 11 ដល់ 50 kWh/ខែ',
       START_USAGE = 11,
       END_USAGE = 50,
       PRICE = 480,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យលំនៅដ្ឋានប្រើពី  51 ដល់ 200 kWh/ខែ',
       START_USAGE = 51,
       END_USAGE = 200,
       PRICE = 610,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង  201 kWh/ខែ',
       START_USAGE = 201,
       END_USAGE = @M,
       PRICE = 730,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 730,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'សាលារៀន, មន្ទីរពេទ្យ, និងមណ្ឌលសុខភាព នៅតំបន់ជនបទ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 610,
       BASE_PRICE = 610,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ផ្សេងៗទៀតទាំងអស់ក្នុងវិស័យកសិកម្មប្រើ ពេលយប់ ម៉ោង៩យប់ ដល់ម៉ោង៧ព្រឹក',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 480,
       BASE_PRICE = 480,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN dbo.TBL_CUSTOMER tc
        ON tc.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 24, 141, 142, 143 )
      AND tc.PRICE_ID IN
          (
              SELECT 480 FROM dbo.TBL_PRICE_DETAIL WHERE PRICE = 480
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ផ្សេងៗទៀតទាំងអស់ក្នុងវិស័យកសិកម្ម(ទិញតាមអត្រាថ្លៃទូទៅ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 730,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN dbo.TBL_CUSTOMER tc
        ON tc.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
      AND tc.PRICE_ID IN
          (
              SELECT 730 FROM dbo.TBL_PRICE_DETAIL WHERE PRICE = 730
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ផ្សេងៗទៀតទាំងអស់ក្នុងវិស័យកសិកម្ម(ទិញ MV តាមអត្រាថ្លៃមធ្យម)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13700,
       BASE_PRICE = 0.13700,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN dbo.TBL_CUSTOMER tc
        ON tc.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
      AND tc.PRICE_ID IN
          (
              SELECT 0.13700 FROM dbo.TBL_PRICE_DETAIL WHERE PRICE = 0.13700
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ផ្សេងៗទៀតទាំងអស់ក្នុងវិស័យកសិកម្ម(ទិញ LV ត្រង់ស្វូរអ្នកលក់ តាមអត្រាថ្លៃមធ្យម)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.150480,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN dbo.TBL_CUSTOMER tc
        ON tc.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
      AND tc.PRICE_ID IN
          (
              SELECT 0.15048 FROM dbo.TBL_PRICE_DETAIL WHERE PRICE = 0.15048
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ផ្សេងៗទៀតទាំងអស់ក្នុងវិស័យកសិកម្ម(ទិញ LV ត្រង់ស្វូរអ្នកទិញ តាមអត្រាថ្លៃមធ្យម)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN dbo.TBL_CUSTOMER tc
        ON tc.CUSTOMER_CONNECTION_TYPE_ID = c.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
      AND tc.PRICE_ID IN
          (
              SELECT 0.14248 FROM dbo.TBL_PRICE_DETAIL WHERE PRICE = 0.14248
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យអ្នកកាន់អាជ្ញាប័ណ្ណ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1290,
       BASE_PRICE = 0.1290,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 11 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យអ្នកកាន់អាជ្ញាប័ណ្ណ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13416,
       BASE_PRICE = 0.13416,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 12 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យអ្នកកាន់អាជ្ញាប័ណ្ណ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14216,
       BASE_PRICE = 0.14216,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 13 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.137,
       BASE_PRICE = 0.137,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 26 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 27 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 28 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13,
       BASE_PRICE = 0.13,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 29 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1432,
       BASE_PRICE = 0.1432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 30 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13520,
       BASE_PRICE = 0.13520,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 31 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1100,
       BASE_PRICE = 0.1100,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 32 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12240,
       BASE_PRICE = 0.12240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 33 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.11440,
       BASE_PRICE = 0.11440,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 34 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 35 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យរុករករ៉ែ LV ក្រោមនៃត្រង់ស្វូ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1352,
       BASE_PRICE = 0.1352,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 37 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1370,
       BASE_PRICE = 0.1370,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 38 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 39 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 40 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 41 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14320,
       BASE_PRICE = 0.14320,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 42 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13520,
       BASE_PRICE = 0.13520,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 43 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1100,
       BASE_PRICE = 0.1100,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 44 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12240,
       BASE_PRICE = 0.12240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 45 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.11440,
       BASE_PRICE = 0.11440,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 46 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 47 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យកម្មន្តសាល LV ក្រោមនៃត្រង់ស្វូ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1352,
       BASE_PRICE = 0.1352,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 49 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.137,
       BASE_PRICE = 0.137,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 50 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 51 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 52 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13,
       BASE_PRICE = 0.13,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 53 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1432,
       BASE_PRICE = 0.1432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 54 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13520,
       BASE_PRICE = 0.13520,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 55 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1100,
       BASE_PRICE = 0.1100,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 56 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12240,
       BASE_PRICE = 0.12240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 57 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.11440,
       BASE_PRICE = 0.11440,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 58 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 59 )

UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យឧស្សាហកម្មវិស័យវាយនភណ្ឌ LV ក្រោមនៃត្រង់ស្វូ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1352,
       BASE_PRICE = 0.1352,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 61 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.137,
       BASE_PRICE = 0.137,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 62 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 63 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 64 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13,
       BASE_PRICE = 0.13,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 65 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1432,
       BASE_PRICE = 0.1432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 66 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13520,
       BASE_PRICE = 0.13520,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 67 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1100,
       BASE_PRICE = 0.1100,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 68 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12240,
       BASE_PRICE = 0.12240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 69 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.11440,
       BASE_PRICE = 0.11440,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 70 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 71 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យកសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ LV ក្រោមនៃត្រង់ស្វូ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1352,
       BASE_PRICE = 0.1352,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 73 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ចិញ្ចឹមសត្វ និងនេសាទ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.137,
       BASE_PRICE = 0.137,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 74 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 75 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 76 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13,
       BASE_PRICE = 0.13,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 77 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1432,
       BASE_PRICE = 0.1432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 78 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13520,
       BASE_PRICE = 0.13520,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 79 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1100,
       BASE_PRICE = 0.1100,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 80 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12240,
       BASE_PRICE = 0.12240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 81 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.11440,
       BASE_PRICE = 0.11440,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 82 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 83 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1352,
       BASE_PRICE = 0.1352,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 85 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម ចិញ្ចឹមសត្វ និងនេសាទ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.137,
       BASE_PRICE = 0.137,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 86 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = 0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 87 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = 0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 88 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13,
       BASE_PRICE = 0.13,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 89 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1432,
       BASE_PRICE = 0.1432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 90 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13520,
       BASE_PRICE = 0.13520,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 91 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1100,
       BASE_PRICE = 0.1100,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 92 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12240,
       BASE_PRICE = 0.12240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 93 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.11440,
       BASE_PRICE = 0.11440,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 94 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = 0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 95 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសកម្មភាពកែច្នៃផលិតផលកសិកម្ម LV ក្រោមនៃត្រង់ស្វូ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1352,
       BASE_PRICE = 0.1352,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 97 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.158,
       BASE_PRICE = 0.158,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 98 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.17232,
       BASE_PRICE = 0.17232,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 99 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16432,
       BASE_PRICE = 0.16432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 100 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15,
       BASE_PRICE = 0.15,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 101 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16400,
       BASE_PRICE = 0.16400,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 102 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15600,
       BASE_PRICE = 0.15600,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 103 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1240,
       BASE_PRICE = 0.1240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 104 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13696,
       BASE_PRICE = 0.13696,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 105 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12896,
       BASE_PRICE = 0.12896,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 106 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1500,
       BASE_PRICE = 0.1500,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 107 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យពាណិជ្ជកម្ម LV ក្រោមនៃត្រង់ស្វូ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15600,
       BASE_PRICE = 0.15600,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 109 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.158,
       BASE_PRICE = 0.158,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 110 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.17232,
       BASE_PRICE = 0.17232,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 111 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16432,
       BASE_PRICE = 0.16432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 112 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15,
       BASE_PRICE = 0.15,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 113 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16400,
       BASE_PRICE = 0.16400,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 114 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15600,
       BASE_PRICE = 0.15600,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 115 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1240,
       BASE_PRICE = 0.1240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 116 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13696,
       BASE_PRICE = 0.13696,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 117 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12896,
       BASE_PRICE = 0.12896,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 118 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1500,
       BASE_PRICE = 0.1500,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 119 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈ LV ក្រោមនៃត្រង់ស្វូ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15600,
       BASE_PRICE = 0.15600,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 121 )

UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 653,
       BASE_PRICE = 653,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 145 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យរដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 712,
       BASE_PRICE = 712,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 146 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យរដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 679,
       BASE_PRICE = 679,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 147 )

UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.158,
       BASE_PRICE = 0.158,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 122 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.17232,
       BASE_PRICE = 0.17232,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 123 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16432,
       BASE_PRICE = 0.16432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 124 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15,
       BASE_PRICE = 0.15,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 125 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16400,
       BASE_PRICE = 0.16400,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 126 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15600,
       BASE_PRICE = 0.15600,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 127 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1240,
       BASE_PRICE = 0.1240,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 128 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.13696,
       BASE_PRICE = 0.13696,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 129 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.12896,
       BASE_PRICE = 0.12896,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 130 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1500,
       BASE_PRICE = 0.1500,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 131 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យសេវាកម្មផ្សេងៗ LV ក្រោមនៃត្រង់ស្វូ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15600,
       BASE_PRICE = 0.15600,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 133 )

UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យអាជីវករតូបលក់ដូរក្នុងផ្សារ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 730,
       BASE_PRICE = 730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 )

---Old Csutomer Type
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 730,
       BASE_PRICE = -730,
       CURRENCY = 1,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1580,
       BASE_PRICE = -0.1580,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1500,
       BASE_PRICE = -0.1500,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN TBL_CUSTOMER cu
        ON c.CUSTOMER_CONNECTION_TYPE_ID = cu.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
      AND cu.PRICE_ID NOT IN
          (
              SELECT PRICE_ID
              FROM TBL_PRICE_DETAIL
              WHERE PRICE = 0.1500
                    OR PRICE = 0.1240
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.16432,
       BASE_PRICE = -0.16432,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.17232,
       BASE_PRICE = -0.17232,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1370,
       BASE_PRICE = -0.1370,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.1300,
       BASE_PRICE = -0.1300,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN TBL_CUSTOMER cu
        ON c.CUSTOMER_CONNECTION_TYPE_ID = cu.CUSTOMER_CONNECTION_TYPE_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
      AND cu.PRICE_ID NOT IN
          (
              SELECT PRICE_ID
              FROM TBL_PRICE_DETAIL
              WHERE PRICE = 0.1300
                    OR PRICE = 0.1100
          )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.14248,
       BASE_PRICE = -0.14248,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 0.15048,
       BASE_PRICE = -0.15048,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 )
UNION ALL
SELECT DATA_MONTH = @DATA_MONTH,
       VALIDATE_NAME = N'លក់ឱ្យអាជីវករតូបលក់ដូរក្នុងផ្សារ',
       START_USAGE = 0,
       END_USAGE = @M,
       PRICE = 730,
       BASE_PRICE = -730,
       CURRENCY = 2,
       c.CUSTOMER_CONNECTION_TYPE_ID,
       g.CUSTOMER_GROUP_ID
FROM TLKP_CUSTOMER_CONNECTION_TYPE c
    INNER JOIN TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = c.NONLICENSE_CUSTOMER_GROUP_ID
WHERE c.CUSTOMER_CONNECTION_TYPE_ID NOT IN ( 144 ) AND @LICENSE_TYPE_ID=2

DELETE FROM TBL_REF_INVALID_CUSTOMER
WHERE DATA_MONTH = @DATA_MONTH

INSERT INTO TBL_REF_INVALID_CUSTOMER
SELECT TYPE_ID = i.CUSTOMER_GROUP_ID,
       TYPE_NAME = v.VALIDATE_NAME,
       METER_CODE_FORMAT = REPLACE(LTRIM(REPLACE(ISNULL(i.METER_CODE, '-'), '0', ' ')), ' ', '0'),
       IS_WRONG_BASED_PRICE = CASE
                                  WHEN i.BASED_PRICE != v.BASED_PRICE THEN
                                      1
                                  ELSE
                                      0
                              END,
       i.*
FROM TBL_REF_INVOICE i
    LEFT JOIN #TMP_VALIDATE v
        ON v.CUSTOMER_CONNECTION_TYPE_ID = i.CUSTOMER_CONNECTION_TYPE_ID
           AND v.CUSTOMER_GROUP_ID = i.CUSTOMER_GROUP_ID
WHERE i.DATA_MONTH = @DATA_MONTH
      AND
      (
          i.PRICE != v.PRICE
          OR i.BASED_PRICE != v.BASED_PRICE
          OR i.CURRENCY_ID != v.CURRENCY_ID
      )
      AND i.IS_ACTIVE = 1
      AND i.TOTAL_USAGE
      BETWEEN v.START_USAGE AND v.END_USAGE
      OR i.TOTAL_USAGE < 0
--END OF RUN_REF_INVALID_CUSTOMER_2022
GO

IF OBJECT_ID('GET_SMALLEST_VALUE')IS NOT NULL
	DROP FUNCTION GET_SMALLEST_VALUE

GO

CREATE FUNCTION [dbo].[GET_SMALLEST_VALUE](@DIGIT INT) RETURNS DECIMAL(18,5)
AS 
BEGIN
	SET @DIGIT = @DIGIT*(-1)
	RETURN CAST(1.00*POWER(10.00,@DIGIT) AS DECIMAL(18,5))
END

GO
IF OBJECT_ID('REPORT_AGING_NEW')IS NOT NULL
	DROP PROC REPORT_AGING_NEW

GO

CREATE PROC [dbo].[REPORT_AGING_NEW]
    @DATE DATETIME = '2022-02-15',
    @MONTH DATETIME = '1900-01-01'
AS
--START REPORT_AGING_DETAIL
SET @MONTH = CONVERT(NVARCHAR(7), @MONTH, 126) + '-01'
-- DEFAULT DATETIME FOR NULL.
IF @MONTH = '1900-01-01'
    SET @MONTH = NULL

DECLARE @AsOFDate AS DATE
SET @AsOFDate = CAST(@DATE AS DATE)
SET @AsOFDate = DATEADD(DAY, 1, @AsOFDate)

DECLARE @AsOfDateTime DATETIME
SET @AsOfDateTime = CAST(@AsOFDate AS DATETIME)

SET @AsOfDateTime = DATEADD(MILLISECOND, -1, @AsOfDateTime)
--PRINT @AsOfDateTime
SET @DATE = @AsOfDateTime

--old adjustment by date 
SELECT adj.INVOICE_ID,
       TOAL_ADJUSTMENT = SUM(adj.ADJUST_AMOUNT)
INTO #OLD_INVOICE_ADJUSTMENT_BY_DATE
FROM dbo.TBL_INVOICE_ADJUSTMENT adj
WHERE adj.CREATE_ON <= @DATE
GROUP BY adj.INVOICE_ID

SELECT INVOICE_ID = i.INVOICE_ID,
       i.INVOICE_NO,
       AMOUNT = SUM(d.AMOUNT)
INTO #INVOICE_AMOUNT
FROM dbo.TBL_INVOICE_DETAIL d
    INNER JOIN dbo.TBL_INVOICE i
        ON i.INVOICE_ID = d.INVOICE_ID
WHERE d.INVOICE_ITEM_ID != -1
      AND i.INVOICE_STATUS NOT IN ( 3 )
      AND d.TRAN_DATE <= @DATE

GROUP BY i.INVOICE_ID,
         i.INVOICE_NO

SELECT INVOICE_ID = i.INVOICE_ID,
       i.INVOICE_NO,
       AMOUNT = d.AMOUNT + ISNULL(fadj.TOAL_ADJUSTMENT, 0)
INTO #invs
FROM #INVOICE_AMOUNT d
    INNER JOIN dbo.TBL_INVOICE i
        ON i.INVOICE_ID = d.INVOICE_ID
    LEFT JOIN #OLD_INVOICE_ADJUSTMENT_BY_DATE fadj
        ON fadj.INVOICE_ID = i.INVOICE_ID

--INVOCIE_ITEM
SELECT ROWNUMBER = ROW_NUMBER() OVER (PARTITION BY INV.INVOICE_ID ORDER BY INVD.INVOICE_DETAIL_ID),
       INV.INVOICE_ID,
       INVT.INVOICE_ITEM_NAME,
       invType.INVOICE_ITEM_TYPE_NAME
INTO #TMP_INVOICE_ITEM_DETAIL
FROM TBL_INVOICE INV
    INNER JOIN TBL_INVOICE_DETAIL INVD
        ON INV.INVOICE_ID = INVD.INVOICE_ID
    INNER JOIN TBL_INVOICE_ITEM INVT
        ON INVT.INVOICE_ITEM_ID = INVD.INVOICE_ITEM_ID
    LEFT JOIN dbo.TBL_INVOICE_ITEM_TYPE invType
        ON invType.INVOICE_ITEM_TYPE_ID = INVT.INVOICE_ITEM_TYPE_ID
WHERE INV.INVOICE_DATE < @DATE

--Payment 
SELECT INVOICE_ID = dd.INVOICE_ID,
       PAID_AMOUNT = SUM(dd.PAY_AMOUNT)
INTO #Payment
FROM TBL_PAYMENT dp
    INNER JOIN TBL_PAYMENT_DETAIL dd
        ON dd.PAYMENT_ID = dp.PAYMENT_ID
WHERE dp.PAY_DATE <= @DATE

GROUP BY dd.INVOICE_ID

SELECT cx.DIGITS_AFTER_DECIMAL,
       cx.CURRENCY_ID,
       cx.CURRENCY_NAME,
       cx.CURRENCY_SING,
       cx.CURRENCY_CODE,
       i.INVOICE_ID,
       c.CUSTOMER_CODE,
       CUSTOMER_NAME = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
       c.STATUS_ID,
       r.AREA_NAME,
       p.POLE_CODE,
       b.BOX_CODE,
       CUSTOMER_GROUP = g.CUSTOMER_GROUP_NAME,
       CUSTOMER_CONNECTION_TYPE = ct.CUSTOMER_CONNECTION_TYPE_NAME,
       i.INVOICE_NO,
       i.INVOICE_DATE,
       i.INVOICE_MONTH,
       i.INVOICE_TITLE,
       --SETTLE_AMOUNT= CASE WHEN ISNULL(f.FORWARD_AMOUNT,0) =0 THEN  i.SETTLE_AMOUNT ELSE i.TOTAL_AMOUNT END  - ISNULL(old_adj.TOAL_ADJUSTMENT,0) + ISNULL(old_adj_by_date.TOAL_ADJUSTMENT,0),
       SETTLE_AMOUNT = invoice_amount.AMOUNT,
       PAID_AMOUNT = ISNULL(pay.PAID_AMOUNT, 0),
       OPEN_DAYS = DATEDIFF(D, INVOICE_DATE, @DATE),
       DUE_AMOUNT = ISNULL(invoice_amount.AMOUNT, 0) - ISNULL(pay.PAID_AMOUNT, 0),
       STATUS = cs.CUS_STATUS_NAME,
       bc.CYCLE_NAME,
       PHONE = c.PHONE_1,
       INVOICE_ITEM_TYPE = ISNULL(id.INVOICE_ITEM_TYPE_NAME, N'ថ្លៃអគ្គិសនី'),
       INVOICE_ITEM = id.INVOICE_ITEM_NAME
INTO #AgeDebt
FROM #invs invoice_amount
    INNER JOIN TBL_INVOICE i
        ON i.INVOICE_ID = invoice_amount.INVOICE_ID
    LEFT JOIN #Payment pay
        ON i.INVOICE_ID = pay.INVOICE_ID
    INNER JOIN TBL_CUSTOMER c
        ON i.CUSTOMER_ID = c.CUSTOMER_ID
    INNER JOIN dbo.TLKP_CUSTOMER_CONNECTION_TYPE ct
        ON c.CUSTOMER_CONNECTION_TYPE_ID = ct.CUSTOMER_CONNECTION_TYPE_ID
    INNER JOIN dbo.TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = ct.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN TBL_AREA r
        ON r.AREA_ID = c.AREA_ID
    LEFT JOIN TBL_CUSTOMER_METER cm
        ON cm.CUSTOMER_ID = c.CUSTOMER_ID
           AND cm.IS_ACTIVE = 1
    LEFT JOIN TBL_BOX b
        ON b.BOX_ID = cm.BOX_ID
    LEFT JOIN TBL_POLE p
        ON p.POLE_ID = b.POLE_ID
    INNER JOIN TLKP_CURRENCY cx
        ON cx.CURRENCY_ID = i.CURRENCY_ID
    INNER JOIN dbo.TLKP_CUS_STATUS cs
        ON cs.CUS_STATUS_ID = c.STATUS_ID
    INNER JOIN dbo.TBL_BILLING_CYCLE bc
        ON bc.CYCLE_ID = c.BILLING_CYCLE_ID
    LEFT JOIN #TMP_INVOICE_ITEM_DETAIL id
        ON id.INVOICE_ID = i.INVOICE_ID
           AND id.ROWNUMBER = 1
           --  AND (invoice_amount.AMOUNT -  ISNULL(pay.PAID_AMOUNT, 0)) >0
           AND
           (
               @MONTH IS NULL
               OR INVOICE_MONTH = @MONTH
           )

SELECT cx.CURRENCY_ID,
       cx.CURRENCY_NAME,
       cx.CURRENCY_SING,
       cx.CURRENCY_CODE,
       INVOICE_ID = ISNULL(i.INVOICE_ID, 0),
       c.CUSTOMER_CODE,
       CUSTOMER_NAME = c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
       c.STATUS_ID,
       r.AREA_NAME,
       p.POLE_CODE,
       b.BOX_CODE,
       CUSTOMER_GROUP = g.CUSTOMER_GROUP_NAME,
       CUSTOMER_CONNECTION_TYPE = ct.CUSTOMER_CONNECTION_TYPE_NAME,
       INVOICE_NO = N'ប្រាក់សល់ពីធនាគារ',
       i.INVOICE_DATE,
       i.INVOICE_MONTH,
       INVOICE_TITLE = N'ប្រាក់សល់ពីធនាគារ',
       -- SETTLE AMOUTN AS OF @DATE
       SETTLE_AMOUNT = 0,
       -- SETTLE AMOUTN AS OF @DATE
       PAID_AMOUNT = bpd.PAY_AMOUNT - bpd.PAID_AMOUNT + ISNULL(pd.PAY_AMOUNT, 0),
       OPEN_DAYS = ISNULL(DATEDIFF(D, INVOICE_DATE, @DATE), 0),
       STATUS = cs.CUS_STATUS_NAME,
       bc.CYCLE_NAME,
       PHONE = c.PHONE_1,
       INVOICE_ITEM_TYPE = ISNULL(id.INVOICE_ITEM_TYPE_NAME, N'ថ្លៃអគ្គិសនី'),
       INVOICE_ITEM = id.INVOICE_ITEM_NAME
INTO #BankTransaction
FROM dbo.TBL_BANK_PAYMENT_DETAIL bpd
    LEFT JOIN dbo.TBL_PAYMENT pa
        ON pa.BANK_PAYMENT_DETAIL_ID = bpd.BANK_PAYMENT_DETAIL_ID
    LEFT JOIN dbo.TBL_PAYMENT_DETAIL pd
        ON pd.PAYMENT_ID = pa.PAYMENT_ID
    LEFT JOIN dbo.TBL_INVOICE i
        ON i.INVOICE_ID = pd.INVOICE_ID
    INNER JOIN TBL_CUSTOMER c
        ON bpd.CUSTOMER_ID = c.CUSTOMER_ID
    INNER JOIN dbo.TLKP_CUSTOMER_CONNECTION_TYPE ct
        ON c.CUSTOMER_CONNECTION_TYPE_ID = ct.CUSTOMER_CONNECTION_TYPE_ID
    INNER JOIN dbo.TLKP_CUSTOMER_GROUP g
        ON g.CUSTOMER_GROUP_ID = ct.NONLICENSE_CUSTOMER_GROUP_ID
    INNER JOIN TBL_AREA r
        ON r.AREA_ID = c.AREA_ID
    LEFT JOIN TBL_CUSTOMER_METER cm
        ON cm.CUSTOMER_ID = c.CUSTOMER_ID
           AND cm.IS_ACTIVE = 1
    LEFT JOIN TBL_BOX b
        ON b.BOX_ID = cm.BOX_ID
    LEFT JOIN TBL_POLE p
        ON p.POLE_ID = b.POLE_ID
    INNER JOIN TLKP_CURRENCY cx
        ON cx.CURRENCY_ID = bpd.CURRENCY_ID
    INNER JOIN dbo.TLKP_CUS_STATUS cs
        ON cs.CUS_STATUS_ID = c.STATUS_ID
    INNER JOIN dbo.TBL_BILLING_CYCLE bc
        ON bc.CYCLE_ID = c.BILLING_CYCLE_ID
    LEFT JOIN #TMP_INVOICE_ITEM_DETAIL id
        ON id.INVOICE_ID = i.INVOICE_ID
           AND id.ROWNUMBER = 1
WHERE (
          i.INVOICE_DATE IS NULL
          OR i.INVOICE_DATE > @DATE
      )
      AND bpd.PAY_DATE <= @DATE
      AND
      (
          i.INVOICE_STATUS IS NULL
          OR i.INVOICE_STATUS NOT IN ( 3 )
      )
      AND bpd.PAY_AMOUNT - bpd.PAID_AMOUNT + ISNULL(pd.PAY_AMOUNT, 0) > 0
      AND
      (
          @MONTH IS NULL
          OR INVOICE_MONTH = @MONTH
      )

--GainLoss forward amount 
SELECT d.CURRENCY_ID,
       d.CURRENCY_NAME,
       d.CURRENCY_SING,
       d.CURRENCY_CODE,
       INVOICE_ID = 0,
       CUSTOMER_CODE = '',
       CUSTOMER_NAME = '',
       STATUS_ID = 0,
       AREA_NAME = '',
       POLE_CODE = '',
       BOX_CODE = '',
       CUSTOMER_GROUP = N'សរុបទឹកប្រាក់លម្អៀងទៅខែបន្ទាប់',
       CUSTOMER_CONNECTION_TYPE = N'សរុបទឹកប្រាក់លម្អៀងទៅខែបន្ទាប់',
       INVOICE_NO = '',
       INVOICE_DATE = @DATE,
       INVOICE_MONTH = @DATE,
       INVOICE_TITLE = N'សរុបទឹកប្រាក់លម្អៀងទៅខែបន្ទាប់',
       SETTLE_AMOUNT = SUM(d.SETTLE_AMOUNT - d.PAID_AMOUNT),
       PAID_AMOUNT = 0,
       OPEN_DAYS = 0,
       STATUS = '',
       CYCLE_NAME = '',
       PHONE = '',
       INVOICE_ITEM_TYPE = '',
       INVOICE_ITEM = ''
INTO #FORWARD_AMOUNT
FROM #AgeDebt d
WHERE d.DUE_AMOUNT < dbo.GET_SMALLEST_VALUE(d.DIGITS_AFTER_DECIMAL)
GROUP BY d.CURRENCY_ID,
         d.CURRENCY_NAME,
         d.CURRENCY_SING,
         d.CURRENCY_CODE

SELECT *
INTO #RESULT
FROM
(
    SELECT d.CURRENCY_ID,
           d.CURRENCY_NAME,
           d.CURRENCY_SING,
           d.CURRENCY_CODE,
           d.INVOICE_ID,
           d.CUSTOMER_CODE,
           d.CUSTOMER_NAME,
           d.STATUS_ID,
           d.AREA_NAME,
           d.POLE_CODE,
           d.BOX_CODE,
           d.CUSTOMER_GROUP,
           d.CUSTOMER_CONNECTION_TYPE,
           d.INVOICE_NO,
           d.INVOICE_DATE,
           d.INVOICE_MONTH,
           d.INVOICE_TITLE,
           d.SETTLE_AMOUNT,
           d.PAID_AMOUNT,
           d.OPEN_DAYS,
           d.CYCLE_NAME,
           d.INVOICE_ITEM,
           d.INVOICE_ITEM_TYPE,
           d.STATUS
    FROM #AgeDebt d
    WHERE d.DUE_AMOUNT >= (dbo.GET_SMALLEST_VALUE(d.DIGITS_AFTER_DECIMAL))
    UNION ALL
    SELECT b.CURRENCY_ID,
           b.CURRENCY_NAME,
           b.CURRENCY_SING,
           b.CURRENCY_CODE,
           b.INVOICE_ID,
           b.CUSTOMER_CODE,
           b.CUSTOMER_NAME,
           b.STATUS_ID,
           b.AREA_NAME,
           b.POLE_CODE,
           b.BOX_CODE,
           b.CUSTOMER_GROUP,
           b.CUSTOMER_CONNECTION_TYPE,
           b.INVOICE_NO,
           b.INVOICE_DATE,
           b.INVOICE_MONTH,
           b.INVOICE_TITLE,
           b.SETTLE_AMOUNT,
           b.PAID_AMOUNT,
           b.OPEN_DAYS,
           b.CYCLE_NAME,
           b.INVOICE_ITEM,
           b.INVOICE_ITEM_TYPE,
           b.STATUS
    FROM #BankTransaction b
    UNION ALL
    SELECT f.CURRENCY_ID,
           f.CURRENCY_NAME,
           f.CURRENCY_SING,
           f.CURRENCY_CODE,
           f.INVOICE_ID,
           f.CUSTOMER_CODE,
           f.CUSTOMER_NAME,
           f.STATUS_ID,
           f.AREA_NAME,
           f.POLE_CODE,
           f.BOX_CODE,
           f.CUSTOMER_GROUP,
           f.CUSTOMER_CONNECTION_TYPE,
           f.INVOICE_NO,
           f.INVOICE_DATE,
           f.INVOICE_MONTH,
           f.INVOICE_TITLE,
           f.SETTLE_AMOUNT,
           f.PAID_AMOUNT,
           f.OPEN_DAYS,
           f.CYCLE_NAME,
           f.INVOICE_ITEM,
           f.INVOICE_ITEM_TYPE,
           f.STATUS
    FROM #FORWARD_AMOUNT f
) AS result
ORDER BY AREA_NAME,
         POLE_CODE,
         BOX_CODE
SELECT *
FROM #RESULT
---END OF REPORT_AGING_NEW
GO

IF OBJECT_ID('REPORT_POWER_SOLD_2022') IS NOT NULL
	DROP PROC REPORT_POWER_SOLD_2022
GO

CREATE PROC dbo.REPORT_POWER_SOLD_2022
    @YEAR_ID INT = 2022,
    @AREA_ID INT = 0,
    @BILLING_CYCLE_ID INT = 0
AS
BEGIN
DECLARE @MONTH DATETIME;
SET @MONTH = CAST(@YEAR_ID AS NVARCHAR(50)) + '-01-01';
-- collect all data
SELECT i.INVOICE_MONTH,
       c.CUSTOMER_TYPE_ID,
       i.CUSTOMER_CONNECTION_TYPE_ID,
       ct.CUSTOMER_TYPE_NAME,
       cct.CUSTOMER_CONNECTION_TYPE_NAME,
       USAGE = i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE, 0),
       c.CUSTOMER_ID,
       i.PRICE,
       i.POWER_CAPACITY
INTO #TMP
FROM TBL_INVOICE i
    INNER JOIN TBL_CUSTOMER c
        ON c.CUSTOMER_ID = i.CUSTOMER_ID
    INNER JOIN TBL_CUSTOMER_TYPE ct
        ON ct.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID
    INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE cct
        ON cct.CUSTOMER_CONNECTION_TYPE_ID = i.CUSTOMER_CONNECTION_TYPE_ID
    OUTER APPLY
(
    SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
    FROM TBL_INVOICE_ADJUSTMENT
    WHERE INVOICE_ID = i.INVOICE_ID
) adj
WHERE YEAR(INVOICE_MONTH) = @YEAR_ID
      AND i.IS_SERVICE_BILL = 0
      AND
      (
          @AREA_ID = 0
          OR c.AREA_ID = @AREA_ID
      )
      AND
      (
          @BILLING_CYCLE_ID = 0
          OR c.BILLING_CYCLE_ID = @BILLING_CYCLE_ID
      )
      AND c.IS_REACTIVE = 0
      AND i.INVOICE_STATUS NOT IN ( 3 )
UNION ALL
SELECT cr.CREDIT_MONTH,
       c.CUSTOMER_TYPE_ID,
       cr.CUSTOMER_CONNECTION_TYPE_ID,
       ct.CUSTOMER_TYPE_NAME,
       cct.CUSTOMER_CONNECTION_TYPE_NAME,
       USAGE = cr.USAGE,
       c.CUSTOMER_ID,
       cr.PRICE,
       c.POWER_CAPACITY
FROM TBL_PREPAID_CREDIT cr
    INNER JOIN TBL_CUSTOMER c
        ON cr.CUSTOMER_ID = c.CUSTOMER_ID
    INNER JOIN TBL_CUSTOMER_TYPE ct
        ON ct.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID
    INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE cct
        ON cct.CUSTOMER_CONNECTION_TYPE_ID = cr.CUSTOMER_CONNECTION_TYPE_ID
ORDER BY i.INVOICE_MONTH,
         ct.CUSTOMER_TYPE_NAME;

-- for customer nomarl
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 6
INTO #RESULT
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 5
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140 )
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID = 141
      AND PRICE = 0.13700
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = -4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID = 141
      AND PRICE = 480
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID = 142
      AND PRICE = 0.15048
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = -3
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID = 142
      AND PRICE = 480
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID = 143
      AND PRICE = 0.14248
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = -2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID = 143
      AND PRICE = 480
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 4
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID = 24
      AND PRICE = 730
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = -1
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID = 24
      AND PRICE = 480
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 3
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 11, 12, 13 )
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 2
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID
BETWEEN '26' AND '97'
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 1
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID
BETWEEN '98' AND '133'
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = -1
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 144 )
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 1
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 145 )
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 1
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 146 )
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME
UNION ALL
SELECT INVOICE_MONTH,
       USAGE_0_10 = SUM(   CASE
                               WHEN USAGE
                                    BETWEEN 0 AND 10 THEN
                                   USAGE
                               ELSE
                                   0
                           END
                       ),
       USAGE_11_50 = SUM(   CASE
                                WHEN USAGE
                                     BETWEEN 11 AND 50 THEN
                                    USAGE
                                ELSE
                                    0
                            END
                        ),
       USAGE_51_200 = SUM(   CASE
                                 WHEN USAGE
                                      BETWEEN 51 AND 200 THEN
                                     USAGE
                                 ELSE
                                     0
                             END
                         ),
       USAGE_201_2000 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 201 AND 2000 THEN
                                       USAGE
                                   ELSE
                                       0
                               END
                           ),
       USAGE_OVER_2001 = SUM(   CASE
                                    WHEN USAGE >= 2001 THEN
                                        USAGE
                                    ELSE
                                        0
                                END
                            ),
       TOTAL_USAGE = SUM(USAGE),
       CUSTOMER_0_10 = SUM(   CASE
                                  WHEN USAGE
                                       BETWEEN 0 AND 10 THEN
                                      1
                                  ELSE
                                      0
                              END
                          ),
       CUSTOMER_11_50 = SUM(   CASE
                                   WHEN USAGE
                                        BETWEEN 11 AND 50 THEN
                                       1
                                   ELSE
                                       0
                               END
                           ),
       CUSTOMER_51_200 = SUM(   CASE
                                    WHEN USAGE
                                         BETWEEN 51 AND 200 THEN
                                        1
                                    ELSE
                                        0
                                END
                            ),
       CUSTOMER_201_2000 = SUM(   CASE
                                      WHEN USAGE
                                           BETWEEN 201 AND 2000 THEN
                                          1
                                      ELSE
                                          0
                                  END
                              ),
       CUSTOMER_OVER_2001 = SUM(   CASE
                                       WHEN USAGE >= 2001 THEN
                                           1
                                       ELSE
                                           0
                                   END
                               ),
       TOTAL_CUSTOMER = COUNT(*),
       TOTAL_CAPACITY = SUM(POWER_CAPACITY),
       CUSTOMER_CONNECTION_TYPE_ID,
       GROUP_ID = 1
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN ( 147 )
GROUP BY INVOICE_MONTH,
         CUSTOMER_CONNECTION_TYPE_ID,
         CUSTOMER_CONNECTION_TYPE_NAME

-- BUILD 12 MONTHs TABLE
CREATE TABLE #M
(
    COL_ORDER INT,
    ID INT,
    NAME NVARCHAR(250),
    INV_MONTH DATETIME,
    GROUP_ID INT
);
DECLARE @M INT;
SET @M = 1;
WHILE @M <= 12
BEGIN
    INSERT INTO #M
    VALUES
    (1, 1, N'​​អតិថិជនលំនៅដ្ឋាន', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 6);

    INSERT INTO #M
    VALUES
    (2, 22, N'​​អាជីវកម្មធុនតូច LV សាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
    INSERT INTO #M
    VALUES
    (3, 23, N'រដ្ឋបាល LV សាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
    INSERT INTO #M
    VALUES
    (4, 134, N'សេវាកម្មផ្សេងៗ LV សាធារណៈ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
    INSERT INTO #M
    VALUES
    (5, 135, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV សាធារណៈ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
    INSERT INTO #M
    VALUES
    (6, 136, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV សាធារណៈ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
    INSERT INTO #M
    VALUES
    (7, 137, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV សាធារណៈ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
    INSERT INTO #M
    VALUES
    (8, 138, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV សាធារណៈ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
    INSERT INTO #M
    VALUES
    (9, 139, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV សាធារណៈ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);
    INSERT INTO #M
    VALUES
    (10, 140, N'កសកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV សាធារណៈ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 5);

    INSERT INTO #M
    VALUES
    (11, 9, N'​​សាលារៀន មន្ទីរពេទ្យ និងមណ្ឌលសុខភាពនៅជនបទ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 4);
    INSERT INTO #M
    VALUES
    (12, 141, N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 4);
    INSERT INTO #M
    VALUES
    (13, 141, N'​​អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម​ ទិញ MV ម៉ោង៩យប់ដល់៧ព្រឹក',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', -4);
    INSERT INTO #M
    VALUES
    (14, 142, N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្​ ទិញ LV ត្រង់ស្វូរអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 4);
    INSERT INTO #M
    VALUES
    (15, 142,
     N'​​អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ទិញ​ LV ត្រង់ស្វូរអ្នកលក់ ម៉ោង៩យប់ដល់៧ព្រឹក',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', -3);
    INSERT INTO #M
    VALUES
    (16, 143, N'អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូរអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 4);
    INSERT INTO #M
    VALUES
    (17, 143,
     N'​​អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ទិញ​ LV ត្រង់ស្វូរអ្នកទិញ ម៉ោង៩យប់ដល់៧ព្រឹក',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', -2);
    INSERT INTO #M
    VALUES
    (18, 24, N'​​អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ទិញ​​ LV ភ្ជាប់ពីខ្សែសាធារណៈ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 4);
    INSERT INTO #M
    VALUES
    (19, 24, N'​​អ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម ទីញ LV ៩យប់ដល់៧ព្រឹក',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', -1);

    INSERT INTO #M
    VALUES
    (20, 11, N'អ្នកកាន់អាជ្ញាបណ្ណ ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 3);
    INSERT INTO #M
    VALUES
    (21, 13, N'អ្នកកាន់អាជ្ញាបណ្ណ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 3);
    INSERT INTO #M
    VALUES
    (22, 12, N'អ្នកកាន់អាជ្ញាបណ្ណ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 3);

    INSERT INTO #M
    VALUES
    (23, 26, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (24, 27, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (25, 28, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (26, 29, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (27, 30, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (28, 31, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (29, 32, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (30, 33, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (31, 34, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (32, 35, N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (34, 37,
     N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (35, 38, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (36, 39, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (37, 40, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (38, 41, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (39, 42, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (40, 43, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (41, 44, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (42, 45, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (43, 46, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (44, 47, N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (46, 49,
     N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (47, 50, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (48, 51, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (49, 52, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (50, 53, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (51, 54, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (52, 55, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (53, 56, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (54, 57, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (55, 58, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (56, 59, N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (58, 61,
     N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (59, 62, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ MV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (60, 63, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (61, 64, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (62, 65, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (63, 66, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (64, 67, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (65, 68, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (66, 69, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (67, 70, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (68, 71, N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (70, 73,
     N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (71, 74, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1',
     2  );
    INSERT INTO #M
    VALUES
    (72, 75, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (73, 76, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (74, 77, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (75, 78, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (76, 79, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (77, 80, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (78, 81, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (79, 82, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (80, 83, N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (82, 85,
     N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (83, 86, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (84, 87, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (85, 88, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (86, 89, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (87, 90, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (88, 91, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (89, 92, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (90, 93, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (91, 94, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (92, 95, N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (94, 97,
     N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 2);
    INSERT INTO #M
    VALUES
    (95, 98, N'ពាណិជ្ជកម្ម ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (96, 99, N'ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1',
     1  );
    INSERT INTO #M
    VALUES
    (97, 100, N'ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1',
     1  );
    INSERT INTO #M
    VALUES
    (98, 101, N'ពាណិជ្ជកម្ម MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (99, 102, N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (100, 103, N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (101, 104, N'ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (102, 105, N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (103, 106, N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (104, 107, N'ពាណិជ្ជកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (106, 109, N'ពាណិជ្ជកម្ម  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (107, 110, N'រដ្ឋបាលសាធារណៈ ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (108, 111, N'រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (109, 112, N'រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (110, 113, N'រដ្ឋបាលសាធារណៈ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (111, 114, N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (112, 115, N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (113, 116, N'រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (114, 117, N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (115, 118, N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (116, 119, N'រដ្ឋបាលសាធារណៈ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (118, 121, N'រដ្ឋបាលសាធារណៈ  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);

    INSERT INTO #M
    VALUES
    (119, 145, N'រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញMV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (120, 146, N'រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (121, 147, N'រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);

    INSERT INTO #M
    VALUES
    (122, 122, N'សេវាកម្មផ្សេងៗ ទិញ MV', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (123, 123, N'សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (124, 124, N'សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (125, 125, N'សេវាកម្មផ្សេងៗ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (126, 126, N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (127, 127, N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (128, 128, N'សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (129, 129, N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (130, 130, N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (131, 131, N'សេវាកម្មផ្សេងៗ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (132, 133, N'សេវាកម្មផ្សេងៗ  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ',
     CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', 1);
    INSERT INTO #M
    VALUES
    (133, 144, N'អាជីវករតូបលក់ដូរក្នុងផ្សារ', CONVERT(NVARCHAR, @YEAR_ID) + '-' + CONVERT(NVARCHAR, @M) + '-1', -1);

    SET @M = @M + 1;
END;
SELECT INVOICE_MONTH = m.INV_MONTH,
       CUSTOMER_TYPE_NAME = m.NAME,
       USAGE_0_10 = ISNULL(USAGE_0_10, 0),
       USAGE_11_50 = ISNULL(USAGE_11_50, 0),
       USAGE_51_200 = ISNULL(USAGE_51_200, 0),
       USAGE_201_2000 = ISNULL(USAGE_201_2000, 0),
       USAGE_OVER_2001 = ISNULL(USAGE_OVER_2001, 0),
       TOTAL_USAGE = ISNULL(TOTAL_USAGE, 0),
       CUSTOMER_0_10 = ISNULL(CUSTOMER_0_10, 0),
       CUSTOMER_11_50 = ISNULL(CUSTOMER_11_50, 0),
       CUSTOMER_51_200 = ISNULL(CUSTOMER_51_200, 0),
       CUSTOMER_201_2000 = ISNULL(CUSTOMER_201_2000, 0),
       CUSTOMER_OVER_2001 = ISNULL(CUSTOMER_OVER_2001, 0),
       TOTAL_CUSTOMER = ISNULL(TOTAL_CUSTOMER, 0),
       TOTAL_CAPACITY = ISNULL(TOTAL_CAPACITY, 0),
       GROUP_TYPE_ID = 1,
       m.ID,
       m.GROUP_ID
INTO #RESULT2
FROM #M m
    LEFT JOIN #RESULT r
        ON r.INVOICE_MONTH = m.INV_MONTH
           AND r.CUSTOMER_CONNECTION_TYPE_ID = m.ID
           AND m.GROUP_ID = r.GROUP_ID
ORDER BY m.INV_MONTH,
         m.COL_ORDER;
INSERT INTO #RESULT2
SELECT INVOICE_MONTH = NULL,
       N'សរុបថាមពល',
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0;
INSERT INTO #RESULT2
SELECT INVOICE_MONTH = NULL,
       m.NAME,
       d.USAGE_0_10,
       d.USAGE_11_50,
       d.USAGE_51_200,
       d.USAGE_201_2000,
       d.USAGE_OVER_2001,
       d.TOTAL_USAGE,
       CUSTOMER_0_10 = 0,
       CUSTOMER_11_50 = 0,
       CUSTOMER_51_200 = 0,
       CUSTOMER_201_2000 = 0,
       CUSTOMER_OVER_2001 = 0,
       TOTAL_CUSTOMER = 0,
       TOTAL_CAPACITY = 0,
       GROUP_TYPE_ID = 2,
       ID,
       GROUP_ID
FROM #M m
    OUTER APPLY
(
    SELECT USAGE_0_10 = SUM(USAGE_0_10),
           USAGE_11_50 = SUM(USAGE_11_50),
           USAGE_51_200 = SUM(USAGE_51_200),
           USAGE_201_2000 = SUM(USAGE_201_2000),
           USAGE_OVER_2001 = SUM(USAGE_OVER_2001),
           TOTAL_USAGE = SUM(TOTAL_USAGE)
    FROM #RESULT2 r
    WHERE r.ID = m.ID
          AND m.GROUP_ID = r.GROUP_ID
) d
WHERE m.INV_MONTH = @MONTH;
SELECT *
FROM #RESULT2
ORDER BY CASE
             WHEN INVOICE_MONTH IS NULL THEN
                 '2100-01-01'
             ELSE
                 INVOICE_MONTH
         END,
         16

END
--- END OF REPORT_POWER_SOLD_2022
GO

IF OBJECT_ID('REPORT_POWER_SOLD_2017') IS NOT NULL
	DROP PROC REPORT_POWER_SOLD_2017
GO

CREATE PROC dbo.REPORT_POWER_SOLD_2017
	@YEAR_ID INT=2017,
	@AREA_ID INT=0,
	@BILLING_CYCLE_ID INT=0
AS
/*
@2017-01-20 By Morm Raksmey
- Add Prepaid Power

*/
BEGIN
DECLARE @MONTH DATETIME
SET @MONTH=CAST(@YEAR_ID AS NVARCHAR(50))+'-01-01'
-- collect all data
SELECT i.INVOICE_MONTH
		,c.CUSTOMER_TYPE_ID
		,c.CUSTOMER_CONNECTION_TYPE_ID
		,ct.CUSTOMER_TYPE_NAME
		,cct.CUSTOMER_CONNECTION_TYPE_NAME
		,USAGE=i.TOTAL_USAGE+ISNULL(adj.ADJUST_USAGE,0)
		,c.CUSTOMER_ID
INTO #TMP
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE ct ON ct.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE cct ON cct.CUSTOMER_CONNECTION_TYPE_ID=c.CUSTOMER_CONNECTION_TYPE_ID
OUTER APPLY(
		SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
		FROM TBL_INVOICE_ADJUSTMENT   
		WHERE INVOICE_ID =  i.INVOICE_ID
) adj 
WHERE YEAR(INVOICE_MONTH)=@YEAR_ID
	AND i.IS_SERVICE_BILL=0 
	AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
	AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID) 
	--AND c.CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
	AND c.IS_REACTIVE = 0 
	--AND c.CUSTOMER_TYPE_ID <> -1 -- USE IN PRODUCTION 
UNION ALL
SELECT cr.CREDIT_MONTH
		,c.CUSTOMER_TYPE_ID
		,c.CUSTOMER_CONNECTION_TYPE_ID
		,ct.CUSTOMER_TYPE_NAME
		,cct.CUSTOMER_CONNECTION_TYPE_NAME
		,USAGE=cr.USAGE
		,c.CUSTOMER_ID
FROM TBL_PREPAID_CREDIT cr
INNER JOIN TBL_CUSTOMER c ON cr.CUSTOMER_ID=c.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE ct ON ct.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CUSTOMER_CONNECTION_TYPE cct ON cct.CUSTOMER_CONNECTION_TYPE_ID=c.CUSTOMER_CONNECTION_TYPE_ID
ORDER BY i.INVOICE_MONTH,ct.CUSTOMER_TYPE_NAME

-- for customer LICENSEE MV,LV/MV transfo Licensee and LV/MV transfo Customer
SELECT INVOICE_MONTH
		,USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END)
		,USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END)
		,USAGE_51_1000=SUM(CASE WHEN USAGE BETWEEN 51 AND 1000 THEN USAGE ELSE 0 END)
		,USAGE_1001_2000=SUM(CASE WHEN USAGE BETWEEN 1001 AND 2000 THEN USAGE ELSE 0 END)
		,USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END)
		,TOTAL_USAGE=SUM(USAGE)
		,CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END)
		,CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END)
		,CUSTOMER_51_1000=SUM(CASE WHEN USAGE BETWEEN 51 AND 1000 THEN 1 ELSE 0 END)
		,CUSTOMER_1001_2000=SUM(CASE WHEN USAGE BETWEEN 1001 AND 2000 THEN 1 ELSE 0 END)
		,CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE >=2001 THEN 1 ELSE 0 END)
		,TOTAL_CUSTOMER=COUNT(*)
		,CUSTOMER_CONNECTION_TYPE_ID
		,GROUP_ID=1
INTO #RESULT
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN(2,3,4)
	 AND CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,CUSTOMER_CONNECTION_TYPE_ID,CUSTOMER_CONNECTION_TYPE_NAME

-- for customer MV,LV/MV transfo Licensee and LV/MV transfo Customer
UNION ALL
SELECT INVOICE_MONTH
		,USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END)
		,USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END)
		,USAGE_51_1000=SUM(CASE WHEN USAGE BETWEEN 51 AND 1000 THEN USAGE ELSE 0 END)
		,USAGE_1001_2000=SUM(CASE WHEN USAGE BETWEEN 1001 AND 2000 THEN USAGE ELSE 0 END)
		,USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END)
		,TOTAL_USAGE=SUM(USAGE)
		,CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END)
		,CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END)
		,CUSTOMER_51_1000=SUM(CASE WHEN USAGE BETWEEN 51 AND 1000 THEN 1 ELSE 0 END)
		,CUSTOMER_1001_2000=SUM(CASE WHEN USAGE BETWEEN 1001 AND 2000 THEN 1 ELSE 0 END)
		,CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE >=2001 THEN 1 ELSE 0 END)
		,TOTAL_CUSTOMER=COUNT(*)
		,CUSTOMER_CONNECTION_TYPE_ID
		,GROUP_ID=2 
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN(2,3,4)
	 AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,CUSTOMER_CONNECTION_TYPE_ID,CUSTOMER_CONNECTION_TYPE_NAME

-- for customer normal business as Telecome,Bank,..
UNION ALL
SELECT INVOICE_MONTH
		,USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END)
		,USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END)
		,USAGE_51_1000=SUM(CASE WHEN USAGE BETWEEN 51 AND 1000 THEN USAGE ELSE 0 END)
		,USAGE_1001_2000=SUM(CASE WHEN USAGE BETWEEN 1001 AND 2000 THEN USAGE ELSE 0 END)
		,USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END)
		,TOTAL_USAGE=SUM(USAGE)
		,CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END)
		,CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END)
		,CUSTOMER_51_1000=SUM(CASE WHEN USAGE BETWEEN 51 AND 1000 THEN 1 ELSE 0 END)
		,CUSTOMER_1001_2000=SUM(CASE WHEN USAGE BETWEEN 1001 AND 2000 THEN 1 ELSE 0 END)
		,CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE >=2001 THEN 1 ELSE 0 END)
		,TOTAL_CUSTOMER=COUNT(*)
		,CUSTOMER_CONNECTION_TYPE_ID=1
		,GROUP_ID=3
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN(1) AND CUSTOMER_TYPE_ID <>1
	 AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,CUSTOMER_CONNECTION_TYPE_ID,CUSTOMER_CONNECTION_TYPE_NAME 

-- for customer normal 
UNION ALL
SELECT INVOICE_MONTH
		,USAGE_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN USAGE ELSE 0 END)
		,USAGE_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN USAGE ELSE 0 END)
		,USAGE_51_1000=SUM(CASE WHEN USAGE BETWEEN 51 AND 1000 THEN USAGE ELSE 0 END)
		,USAGE_1001_2000=SUM(CASE WHEN USAGE BETWEEN 1001 AND 2000 THEN USAGE ELSE 0 END)
		,USAGE_OVER_2001=SUM(CASE WHEN USAGE>=2001 THEN USAGE ELSE 0 END)
		,TOTAL_USAGE=SUM(USAGE)
		,CUSTOMER_0_10=SUM(CASE WHEN USAGE BETWEEN 0 AND 10 THEN 1 ELSE 0 END)
		,CUSTOMER_11_50=SUM(CASE WHEN USAGE BETWEEN 11 AND 50 THEN 1 ELSE 0 END)
		,CUSTOMER_51_1000=SUM(CASE WHEN USAGE BETWEEN 51 AND 1000 THEN 1 ELSE 0 END)
		,CUSTOMER_1001_2000=SUM(CASE WHEN USAGE BETWEEN 1001 AND 2000 THEN 1 ELSE 0 END)
		,CUSTOMER_OVER_2001=SUM(CASE WHEN USAGE >=2001 THEN 1 ELSE 0 END)
		,TOTAL_CUSTOMER=COUNT(*)
		,CUSTOMER_CONNECTION_TYPE_ID=0
		,GROUP_ID=3
FROM #TMP
WHERE CUSTOMER_CONNECTION_TYPE_ID IN(1) AND CUSTOMER_TYPE_ID=1 
	 AND CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1)
GROUP BY INVOICE_MONTH,CUSTOMER_CONNECTION_TYPE_ID,CUSTOMER_CONNECTION_TYPE_NAME

-- BUILD 12 MONTHs TABLE
CREATE TABLE #M(ID INT,NAME NVARCHAR(50),INV_MONTH DATETIME,GROUP_ID INT)
DECLARE @M INT
SET @M=1
WHILE @M<=12
BEGIN
	INSERT INTO #M VALUES(4,N'សេវាករ  MV ',CONVERT(NVARCHAR,@YEAR_ID)+'-'+CONVERT(NVARCHAR,@M)+'-1',1) 
	INSERT INTO #M VALUES(3,N'សេវាករ  LV ត្រង់ស្វូអ្នកទិញ',CONVERT(NVARCHAR,@YEAR_ID)+'-'+CONVERT(NVARCHAR,@M)+'-1',1) 
	INSERT INTO #M VALUES(2,N'សេវាករ  LV ត្រង់ស្វូអ្នកលក់',CONVERT(NVARCHAR,@YEAR_ID)+'-'+CONVERT(NVARCHAR,@M)+'-1',1) 
	INSERT INTO #M VALUES(4,N'អាជីវកម្ម  MV',CONVERT(NVARCHAR,@YEAR_ID)+'-'+CONVERT(NVARCHAR,@M)+'-1',2) 
	INSERT INTO #M VALUES(3,N'អាជីវកម្ម LV ត្រង់ស្វូអ្នកទិញ',CONVERT(NVARCHAR,@YEAR_ID)+'-'+CONVERT(NVARCHAR,@M)+'-1',2) 
	INSERT INTO #M VALUES(2,N'អាជីវកម្ម LV ត្រង់ស្វូអ្នកលក់',CONVERT(NVARCHAR,@YEAR_ID)+'-'+CONVERT(NVARCHAR,@M)+'-1',2) 
	INSERT INTO #M VALUES(1,N'អាជីវកម្មភ្ជាប់ពីបណ្តាញសាធារណៈ',CONVERT(NVARCHAR,@YEAR_ID)+'-'+CONVERT(NVARCHAR,@M)+'-1',3) 
	INSERT INTO #M VALUES(0,N'អតិថិជនលំនៅដ្ឋាន',CONVERT(NVARCHAR,@YEAR_ID)+'-'+CONVERT(NVARCHAR,@M)+'-1',3) 
	SET @M=@M+1
END  

SELECT INVOICE_MONTH=m.INV_MONTH
		,CUSTOMER_TYPE_NAME=m.NAME 
		,USAGE_0_10=ISNULL(USAGE_0_10,0)
		,USAGE_11_50=ISNULL(USAGE_11_50,0)
		,USAGE_51_1000=ISNULL(USAGE_51_1000,0)
		,USAGE_1001_2000=ISNULL(USAGE_1001_2000,0)
		,USAGE_OVER_2001=ISNULL(USAGE_OVER_2001,0)
		,TOTAL_USAGE=ISNULL(TOTAL_USAGE,0)
		,CUSTOMER_0_10=ISNULL(CUSTOMER_0_10,0)
		,CUSTOMER_11_50=ISNULL(CUSTOMER_11_50,0)
		,CUSTOMER_51_1000=ISNULL(CUSTOMER_51_1000,0)
		,CUSTOMER_1001_2000=ISNULL(CUSTOMER_1001_2000,0)
		,CUSTOMER_OVER_2001=ISNULL(CUSTOMER_OVER_2001,0)
		,TOTAL_CUSTOMER=ISNULL(TOTAL_CUSTOMER,0)
		,GROUP_TYPE_ID=1 
		,m.ID
		,m.GROUP_ID
INTO #RESULT2
FROM #M m 
LEFT JOIN #RESULT r ON r.INVOICE_MONTH=m.INV_MONTH AND r.CUSTOMER_CONNECTION_TYPE_ID=m.ID AND m.GROUP_ID=r.GROUP_ID
ORDER BY m.INV_MONTH,m.GROUP_ID,m.ID DESC

INSERT INTO #RESULT2
SELECT  INVOICE_MONTH=NULL
		,N'សរុបថាមពល'
	    ,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0

INSERT INTO #RESULT2
SELECT  INVOICE_MONTH=NULL
		,m.NAME
	    ,d.USAGE_0_10
		,d.USAGE_11_50
		,d.USAGE_51_1000
		,d.USAGE_1001_2000
		,d.USAGE_OVER_2001
		,d.TOTAL_USAGE
		,CUSTOMER_0_10=0
		,CUSTOMER_11_50=0
		,CUSTOMER_51_1000=0
		,CUSTOMER_1001_2000=0
		,CUSTOMER_OVER_2001=0
		,TOTAL_CUSTOMER=0
		,GROUP_TYPE_ID=2
		,ID
		,GROUP_ID
FROM #M m
OUTER APPLY(
	SELECT USAGE_0_10=SUM(USAGE_0_10)
			,USAGE_11_50=SUM(USAGE_11_50)
			,USAGE_51_1000=SUM(USAGE_51_1000)
			,USAGE_1001_2000=SUM(USAGE_1001_2000)
			,USAGE_OVER_2001=SUM(USAGE_OVER_2001)
			,TOTAL_USAGE=SUM(TOTAL_USAGE)
	FROM #RESULT2 r
	WHERE r.ID=m.ID AND m.GROUP_ID=r.GROUP_ID
)d
WHERE m.INV_MONTH=@MONTH
  
SELECT * FROM #RESULT2
ORDER BY CASE WHEN INVOICE_MONTH IS NULL THEN '2100-01-01' ELSE INVOICE_MONTH END,15
END
--REPORT_POWER_SOLD_2017
GO

IF OBJECT_ID('REPORT_POWER_SOLD') IS NOT NULL
	DROP PROC REPORT_POWER_SOLD
GO

CREATE PROC REPORT_POWER_SOLD
	@YEAR_ID INT=2022,
	@AREA_ID INT=0,
	@BILLING_CYCLE_ID INT=0
AS
IF @YEAR_ID > 2021
	EXEC dbo.REPORT_POWER_SOLD_2022 @YEAR_ID, @AREA_ID, @BILLING_CYCLE_ID
ELSE IF @YEAR_ID > 2020
	EXEC dbo.REPORT_POWER_SOLD_2021 @YEAR_ID, @AREA_ID, @BILLING_CYCLE_ID
ELSE IF @YEAR_ID > 2017
	EXEC dbo.REPORT_POWER_SOLD_2018 @YEAR_ID, @AREA_ID, @BILLING_CYCLE_ID
ELSE 
	EXEC dbo.REPORT_POWER_SOLD_2017 @YEAR_ID, @AREA_ID, @BILLING_CYCLE_ID
GO

IF NOT EXISTS(SELECT * FROM TBL_HOLIDAY WHERE YEAR(HOLIDAY_DATE)=2022)
BEGIN
	INSERT INTO TBL_HOLIDAY 
	SELECT N'ទិវា​ចូល​ឆ្នាំ​សកល','2022-01-01','',1
	UNION ALL SELECT N'ទិវា​ជ័យជម្នះ​លើ​របប​ប្រល័យ​ពូជ​សាសន៍','2022-01-07','',1
	UNION ALL SELECT N'ទិវា​នារី​អន្តរជាតិ','2022-03-08','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ចូល​ឆ្នាំ​ថ្មី ប្រពៃណី​ជាតិ - មហាសង្ក្រាន្ត','2022-04-14','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ចូល​ឆ្នាំ​ថ្មី ប្រពៃណី​ជាតិ - វិរៈវ័នបត','2022-04-15','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ចូល​ឆ្នាំ​ថ្មី ប្រពៃណី​ជាតិ - ថ្ងៃឡើងស័ក','2022-04-16','',1
	UNION ALL SELECT N'ទិវា​ពលកម្ម​អន្តរជាតិ','2022-05-01','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​ចម្រើន​ព្រះ​ជន្ម ព្រះ​ករុណា ព្រះ​បាទ​សម្តេច ព្រះ​បរម​នាថ នរោត្តម សីហមុនី ព្រះមហាក្សត្រ នៃព្រះរាជាណាចក្រកម្ពុជា','2022-05-14','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​វិសាខ​បូជា','2022-05-15','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​ច្រត់​ព្រះ​នង្គ័ល','2020-05-19','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​ចម្រើន​ព្រះ​ជន្ម សម្តេច​ព្រះ​មហាក្សត្រី នរោត្តម មុនិនាថ សីហនុ ព្រះវររាជមាតាជាតិខ្មែរ','2022-06-18','',1
	UNION ALL SELECT N'ទិវា​ប្រកាស​រដ្ឋ​ធម្មនុញ្ញ','2022-09-24','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ភ្ជុំ​បិណ្ឌ','2022-09-24','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ភ្ជុំ​បិណ្ឌ','2022-09-25','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ភ្ជុំ​បិណ្ឌ','2022-09-26','',1
	UNION ALL SELECT N'ទិវា​ប្រារព្ធ​ពិធី​គោរព​ព្រះវិញ្ញាណក្ខន្ធ ព្រះករុណា​ ព្រះបាទ​សម្តេច​ព្រះ នរោត្តម សីហនុ ព្រះមហាវីរក្សត្រ ព្រះ​វររាជ​បិតា​ ឯករាជ្យបូរណភាព​ទឹកដី និង​ឯកភាព​ជាតិ​ខ្មែរ ព្រះបរមរតនកោដ្ឋ','2022-10-15','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​គ្រង​ព្រះ​បរម​រាជ​សម្បត្តិ​របស់ ព្រះ​ករុណា ព្រះ​បាទ​សម្តេចព្រះ​បរមនាថ នរោត្តម សីហមុនី ព្រះ​មហាក្សត្រ​ នៃ​ព្រះរាជាណាចក្រ​កម្ពុជា','2022-10-29','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​អុំ​ទូក បណ្ដែត​ប្រទីប និង​សំពះ​ព្រះ​ខែ អកអំបុក','2022-11-07','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​អុំ​ទូក បណ្ដែត​ប្រទីប និង​សំពះ​ព្រះ​ខែ អកអំបុក','2022-11-08','',1
	UNION ALL SELECT N'ព្រះ​រាជ​ពិធី​បុណ្យ​អុំ​ទូក បណ្ដែត​ប្រទីប និង​សំពះ​ព្រះ​ខែ អកអំបុក','2022-11-09','',1
	UNION ALL SELECT N'ពិធី​បុណ្យ​ឯករាជ្យ​ជាតិ','2022-11-09','',1
END
GO


IF OBJECT_ID('REPORT_QUARTER_SOLD_CONSUMER') IS NOT NULL
	DROP PROC REPORT_QUARTER_SOLD_CONSUMER
GO

CREATE PROC dbo.REPORT_QUARTER_SOLD_CONSUMER 
	@DATE DATETIME = '2022-02-01'
AS
DECLARE @D1 DATETIME,
        @D2 DATETIME
SET @DATE = DATEADD(D, 0, DATEDIFF(D, 0, @DATE))
SET @D1 = CONVERT(NVARCHAR(4), @DATE, 126) + '-01-01'
SET @D2 = DATEADD(S, -1, DATEADD(M, 3, @DATE))


-- BUILD 12 MONTHs TABLE
CREATE TABLE #MONTHS
(
    M DATETIME
)
DECLARE @M INT
SET @M = 1
WHILE @M <= 12
BEGIN
    INSERT INTO #MONTHS
    VALUES
    (CONVERT(NVARCHAR(4), @D1, 126) + '-' + CONVERT(NVARCHAR, @M) + '-1')
    SET @M = @M + 1
END

SELECT id.INVOICE_ID,
       id.PRICE,
       RowNumer = ROW_NUMBER() OVER (PARTITION BY id.INVOICE_ID ORDER BY id.INVOICE_DETAIL_ID DESC)
INTO #tmpPrice1
FROM dbo.TBL_INVOICE_DETAIL id
WHERE id.INVOICE_ITEM_ID IN ( -1, 1 )

IF (@DATE >= '2021-01-01')
BEGIN
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'លំនៅដ្ឋាន'
    INTO #TMP_TYPE1 UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'អាជីវកម្មធុនតូច LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'រដ្ឋបាល LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'អាជីវករតូបលក់ដូរក្នុងផ្សារ'
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 12,
           TYPE_NAME = N'សាលារៀន, មន្ទីរពេទ្យ, មណ្ឌលសុខភាព នៅតំបន់ជនបទ'
    UNION ALL
    SELECT TYPE_ID = 13,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 14,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 15,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 16,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 17,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 18,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 19,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 20,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 21,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 22,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
    UNION ALL
    SELECT TYPE_ID = 23,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់ (លុប)'
    UNION ALL
    SELECT TYPE_ID = 24,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យរុករករ៉ែ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ'
    UNION ALL
    SELECT TYPE_ID = 25,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 26,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 27,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 28,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 29,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 30,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 31,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 32,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 33,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 34,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
    UNION ALL
    SELECT TYPE_ID = 35,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់ (លុប)'
    UNION ALL
    SELECT TYPE_ID = 36,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យកម្មន្តសាល ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ'
    UNION ALL
    SELECT TYPE_ID = 37,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 38,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 39,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 40,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 41,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 42,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 43,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 44,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 45,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 46,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
    UNION ALL
    SELECT TYPE_ID = 47,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់ (លុប)'
    UNION ALL
    SELECT TYPE_ID = 48,
           TYPE_NAME = N'ឧស្សាហកម្មវិស័យវាយនភណ្ឌ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ'
    UNION ALL
    SELECT TYPE_ID = 49,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 50,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 51,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 52,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 53,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 54,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 55,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 56,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 57,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 58,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV '
    UNION ALL
    SELECT TYPE_ID = 59,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ  មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់ (លុប)'
    UNION ALL
    SELECT TYPE_ID = 60,
           TYPE_NAME = N'កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ ឬនេសាទ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ'
    UNION ALL
    SELECT TYPE_ID = 61,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 62,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 63,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 64,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 65,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្មទ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 66,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 67,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 68,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 69,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 70,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
    UNION ALL
    SELECT TYPE_ID = 71,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម  មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់ (លុប)'
    UNION ALL
    SELECT TYPE_ID = 72,
           TYPE_NAME = N'សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ'
    UNION ALL
    SELECT TYPE_ID = 73,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 74,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 75,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 76,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 77,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 78,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 79,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 80,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 81,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 82,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
    UNION ALL
    SELECT TYPE_ID = 83,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់ (លុប)'
    UNION ALL
    SELECT TYPE_ID = 84,
           TYPE_NAME = N'សកម្មភាពកែច្នៃផលិតផលកសិកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ'
    UNION ALL
    SELECT TYPE_ID = 85,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 86,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 87,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 88,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 89,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 90,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 91,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 92,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 93,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 94,
           TYPE_NAME = N'ពាណិជ្ជកម្ម ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
    UNION ALL
    SELECT TYPE_ID = 95,
           TYPE_NAME = N'ពាណិជ្ជកម្ម  មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់  (លុប)'
    UNION ALL
    SELECT TYPE_ID = 96,
           TYPE_NAME = N'ពាណិជ្ជកម្ម  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ'
    UNION ALL
    SELECT TYPE_ID = 97,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 98,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 99,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 101,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 102,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 103,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 104,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 105,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 106,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 107,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
    UNION ALL
    SELECT TYPE_ID = 108,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ  មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់  (លុប)'
    UNION ALL
    SELECT TYPE_ID = 109,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈ  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ'
    UNION ALL
    SELECT TYPE_ID = 110,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញMV'
    UNION ALL
    SELECT TYPE_ID = 111,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 112,
           TYPE_NAME = N'រដ្ឋបាលសាធារណៈដែលទូទាត់ដោយថវិការដ្ឋ ទិញLV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 113,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ MV'
    UNION ALL
    SELECT TYPE_ID = 114,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 115,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 116,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់)'
    UNION ALL
    SELECT TYPE_ID = 117,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 118,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៧ព្រឹក-៩យប់) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 119,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ MV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 120,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 121,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ទិញ LV តាមពេលវេលា(ម៉ោង៩យប់-៧ព្រឹក) ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 122,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកMV'
    UNION ALL
    SELECT TYPE_ID = 123,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ  មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ ទិញLV ត្រង់ស្វូអ្នកលក់  (លុប)'
    UNION ALL
    SELECT TYPE_ID = 124,
           TYPE_NAME = N'សេវាកម្មផ្សេងៗ  ធុនធំមានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យនាឡិកាស្ទង់តម្លើងនៅផ្នែកLV ក្រោមត្រង់ស្វូ'
    UNION ALL
    SELECT TYPE_ID = 125,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ MV (ម៉ោង៩យប់ដល់៧ព្រឹក)'
    UNION ALL
    SELECT TYPE_ID = 126,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV (ម៉ោង៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូរអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 127,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV (ម៉ោង៩យប់ដល់៧ព្រឹក) ត្រង់ស្វូរអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 128,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 129,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ MV តាមអត្រាថ្លៃមធ្យម'
    UNION ALL
    SELECT TYPE_ID = 130,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV ត្រង់ស្វូរអ្នកលក់ តាមអត្រាថ្លៃមធ្យម'
    UNION ALL
    SELECT TYPE_ID = 131,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV ត្រង់ស្វូរអ្នកទិញ តាមអត្រាថ្លៃមធ្យម'
    UNION ALL
    SELECT TYPE_ID = 132,
           TYPE_NAME = N'បូមទឹកកសិកម្មពេលយប់ ទិញ LV សាធារណៈ តាមអត្រាថ្លៃមធ្យម'
    -- BUILD DATA VALUE TABLE
    SELECT i.INVOICE_MONTH,
           i.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22 ) THEN
                                                 2
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 23 ) THEN
                                                 3
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 134 ) THEN
                                                 4
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 ) THEN
                                                 5
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 135 ) THEN
                                                 6
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 136 ) THEN
                                                 7
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 137 ) THEN
                                                 8
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 138 ) THEN
                                                 9
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 139 ) THEN
                                                 10
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 140 ) THEN
                                                 11
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 12
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 26 ) THEN
                                                 13
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 27 ) THEN
                                                 14
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 28 ) THEN
                                                 15
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 29 ) THEN
                                                 16
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 30 ) THEN
                                                 17
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 31 ) THEN
                                                 18
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 32 ) THEN
                                                 19
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 33 ) THEN
                                                 20
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 34 ) THEN
                                                 21
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 35 ) THEN
                                                 22
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 36 ) THEN
                                                 23
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 37 ) THEN
                                                 24
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 38 ) THEN
                                                 25
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 39 ) THEN
                                                 26
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 40 ) THEN
                                                 27
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 41 ) THEN
                                                 28
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 42 ) THEN
                                                 29
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 43 ) THEN
                                                 30
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 44 ) THEN
                                                 31
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 45 ) THEN
                                                 32
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 46 ) THEN
                                                 33
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 47 ) THEN
                                                 34
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 48 ) THEN
                                                 35
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 49 ) THEN
                                                 36
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 50 ) THEN
                                                 37
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 51 ) THEN
                                                 38
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 52 ) THEN
                                                 39
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 53 ) THEN
                                                 40
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 54 ) THEN
                                                 41
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 55 ) THEN
                                                 42
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 56 ) THEN
                                                 43
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 57 ) THEN
                                                 44
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 58 ) THEN
                                                 45
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 59 ) THEN
                                                 46
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 60 ) THEN
                                                 47
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 61 ) THEN
                                                 48
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 62 ) THEN
                                                 49
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 63 ) THEN
                                                 50
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 64 ) THEN
                                                 51
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 65 ) THEN
                                                 52
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 66 ) THEN
                                                 53
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 67 ) THEN
                                                 54
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 68 ) THEN
                                                 55
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 69 ) THEN
                                                 56
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 70 ) THEN
                                                 57
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 71 ) THEN
                                                 58
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 72 ) THEN
                                                 59
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 73 ) THEN
                                                 60
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 74 ) THEN
                                                 61
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 75 ) THEN
                                                 62
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 76 ) THEN
                                                 63
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 77 ) THEN
                                                 64
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 78 ) THEN
                                                 65
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 79 ) THEN
                                                 66
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 80 ) THEN
                                                 67
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 81 ) THEN
                                                 68
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 82 ) THEN
                                                 69
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 83 ) THEN
                                                 70
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 84 ) THEN
                                                 71
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 85 ) THEN
                                                 72
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 86 ) THEN
                                                 73
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 87 ) THEN
                                                 74
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 88 ) THEN
                                                 75
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 89 ) THEN
                                                 76
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 90 ) THEN
                                                 77
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 91 ) THEN
                                                 78
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 92 ) THEN
                                                 79
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 93 ) THEN
                                                 80
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 94 ) THEN
                                                 81
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 95 ) THEN
                                                 82
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 96 ) THEN
                                                 83
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 97 ) THEN
                                                 84
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 98 ) THEN
                                                 85
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 99 ) THEN
                                                 86
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 100 ) THEN
                                                 87
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 101 ) THEN
                                                 88
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 102 ) THEN
                                                 89
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 103 ) THEN
                                                 90
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 104 ) THEN
                                                 91
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 105 ) THEN
                                                 92
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 106 ) THEN
                                                 93
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 107 ) THEN
                                                 94
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 108 ) THEN
                                                 95
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 109 ) THEN
                                                 96
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 110 ) THEN
                                                 97
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 111 ) THEN
                                                 98
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 112 ) THEN
                                                 99
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 113 ) THEN
                                                 101
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 114 ) THEN
                                                 102
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 115 ) THEN
                                                 103
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 116 ) THEN
                                                 104
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 117 ) THEN
                                                 105
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 118 ) THEN
                                                 106
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 119 ) THEN
                                                 107
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 120 ) THEN
                                                 108
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 121 ) THEN
                                                 109
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 145 ) THEN
                                                 110
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 146 ) THEN
                                                 111
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 147 ) THEN
                                                 112
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 122 ) THEN
                                                 113
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 123 ) THEN
                                                 114
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 124 ) THEN
                                                 115
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 125 ) THEN
                                                 116
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 126 ) THEN
                                                 117
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 127 ) THEN
                                                 118
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 128 ) THEN
                                                 119
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 129 ) THEN
                                                 120
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 130 ) THEN
                                                 121
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 131 ) THEN
                                                 122
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 132 ) THEN
                                                 123
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 133 ) THEN
                                                 124
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND i.PRICE = 480 THEN
                                                 125
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND i.PRICE = 480 THEN
                                                 126
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND i.PRICE = 480 THEN
                                                 127
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND i.PRICE = 480 THEN
                                                 128
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND i.PRICE = 0.1370 THEN
                                                 129
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND i.PRICE = 0.15048 THEN
                                                 130
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND i.PRICE = 0.14248 THEN
                                                 131
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND i.PRICE = 730 THEN
                                                 132
                                             ELSE
                                                 i.CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           TOTAL_USAGE = i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE, 0),
           d.PRICE,
           i.TOTAL_AMOUNT
    INTO #TMP1
    FROM TBL_INVOICE i
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = i.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = i.CURRENCY_ID
        INNER JOIN #tmpPrice1 d
            ON d.INVOICE_ID = i.INVOICE_ID
               AND d.RowNumer = 1
        OUTER APPLY
    (
        SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
        FROM TBL_INVOICE_ADJUSTMENT
        WHERE INVOICE_ID = i.INVOICE_ID
    ) adj
    WHERE i.INVOICE_MONTH
          BETWEEN @D1 AND @D2
          --AND c.CUSTOMER_CONNECTION_TYPE_ID <> -1 -- USE IN PRODUCTION
          AND i.IS_SERVICE_BILL = 0
          AND c.CUSTOMER_ID NOT IN
              (
                  SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
              )
          AND c.IS_REACTIVE = 0
          AND i.INVOICE_STATUS NOT IN ( 3 )
    UNION ALL
    SELECT CONVERT(NVARCHAR(7), CREATE_ON, 126) + '-01',
           b.CURRENCY_ID,
           cc.CURRENCY_SING,
            CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 22 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 23 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 134 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 144 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 135 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 136 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 137 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 138 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 139 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 140 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 26 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 27 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 28 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 29 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 30 ) THEN
                                                 17
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 31 ) THEN
                                                 18
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 32 ) THEN
                                                 19
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 33 ) THEN
                                                 20
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 34 ) THEN
                                                 21
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 35 ) THEN
                                                 22
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 36 ) THEN
                                                 23
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 37 ) THEN
                                                 24
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 38 ) THEN
                                                 25
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 39 ) THEN
                                                 26
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 40 ) THEN
                                                 27
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 41 ) THEN
                                                 28
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 42 ) THEN
                                                 29
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 43 ) THEN
                                                 30
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 44 ) THEN
                                                 31
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 45 ) THEN
                                                 32
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 46 ) THEN
                                                 33
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 47 ) THEN
                                                 34
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 48 ) THEN
                                                 35
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 49 ) THEN
                                                 36
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 50 ) THEN
                                                 37
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 51 ) THEN
                                                 38
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 52 ) THEN
                                                 39
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 53 ) THEN
                                                 40
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 54 ) THEN
                                                 41
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 55 ) THEN
                                                 42
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 56 ) THEN
                                                 43
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 57 ) THEN
                                                 44
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 58 ) THEN
                                                 45
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 59 ) THEN
                                                 46
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 60 ) THEN
                                                 47
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 61 ) THEN
                                                 48
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 62 ) THEN
                                                 49
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 63 ) THEN
                                                 50
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 64 ) THEN
                                                 51
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 65 ) THEN
                                                 52
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 66 ) THEN
                                                 53
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 67 ) THEN
                                                 54
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 68 ) THEN
                                                 55
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 69 ) THEN
                                                 56
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 70 ) THEN
                                                 57
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 71 ) THEN
                                                 58
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 72 ) THEN
                                                 59
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 73 ) THEN
                                                 60
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 74 ) THEN
                                                 61
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 75 ) THEN
                                                 62
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 76 ) THEN
                                                 63
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 77 ) THEN
                                                 64
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 78 ) THEN
                                                 65
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 79 ) THEN
                                                 66
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 80 ) THEN
                                                 67
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 81 ) THEN
                                                 68
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 82 ) THEN
                                                 69
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 83 ) THEN
                                                 70
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 84 ) THEN
                                                 71
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 85 ) THEN
                                                 72
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 86 ) THEN
                                                 73
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 87 ) THEN
                                                 74
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 88 ) THEN
                                                 75
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 89 ) THEN
                                                 76
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 90 ) THEN
                                                 77
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 91 ) THEN
                                                 78
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 92 ) THEN
                                                 79
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 93 ) THEN
                                                 80
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 94 ) THEN
                                                 81
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 95 ) THEN
                                                 82
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 96 ) THEN
                                                 83
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 97 ) THEN
                                                 84
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 98 ) THEN
                                                 85
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 99 ) THEN
                                                 86
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 100 ) THEN
                                                 87
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 101 ) THEN
                                                 88
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 102 ) THEN
                                                 89
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 103 ) THEN
                                                 90
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 104 ) THEN
                                                 91
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 105 ) THEN
                                                 92
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 106 ) THEN
                                                 93
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 107 ) THEN
                                                 94
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 108 ) THEN
                                                 95
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 109 ) THEN
                                                 96
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 110 ) THEN
                                                 97
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 111 ) THEN
                                                 98
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 112 ) THEN
                                                 99
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 113 ) THEN
                                                 101
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 114 ) THEN
                                                 102
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 115 ) THEN
                                                 103
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 116 ) THEN
                                                 104
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 117 ) THEN
                                                 105
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 118 ) THEN
                                                 106
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 119 ) THEN
                                                 107
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 120 ) THEN
                                                 108
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 121 ) THEN
                                                 109
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 145 ) THEN
                                                 110
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 146 ) THEN
                                                 111
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 147 ) THEN
                                                 112
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 122 ) THEN
                                                 113
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 123 ) THEN
                                                 114
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 124 ) THEN
                                                 115
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 125 ) THEN
                                                 116
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 126 ) THEN
                                                 117
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 127 ) THEN
                                                 118
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 128 ) THEN
                                                 119
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 129 ) THEN
                                                 120
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 130 ) THEN
                                                 121
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 131 ) THEN
                                                 122
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 132 ) THEN
                                                 123
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 133 ) THEN
                                                 124
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 480 THEN
                                                 125
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 480 THEN
                                                 126
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 480 THEN
                                                 127
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 480 THEN
                                                 128
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 0.1370 THEN
                                                 129
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 0.15048 THEN
                                                 130
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 0.14248 THEN
                                                 131
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 730 THEN
                                                 132
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           b.BUY_QTY,
           b.PRICE,
           b.BUY_AMOUNT
    FROM TBL_CUSTOMER_BUY b
        INNER JOIN TBL_PREPAID_CUSTOMER p
            ON p.PREPAID_CUS_ID = b.PREPAID_CUS_ID
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = p.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = b.CURRENCY_ID
    WHERE b.CREATE_ON
          BETWEEN @D1 AND @D2
          AND c.CUSTOMER_ID NOT IN
              (
                  SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
              )
          AND c.IS_REACTIVE = 0
          AND CONVERT(NVARCHAR(7), CREATE_ON, 126) + '-01' <= '2016-02-01'
    UNION ALL
    SELECT cr.CREDIT_MONTH,
           cr.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 22 ) THEN
                                                 2
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 23 ) THEN
                                                 3
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 134 ) THEN
                                                 4
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 ) THEN
                                                 5
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 135 ) THEN
                                                 6
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 136 ) THEN
                                                 7
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 137 ) THEN
                                                 8
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 138 ) THEN
                                                 9
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 139 ) THEN
                                                 10
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 140 ) THEN
                                                 11
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 12
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 26 ) THEN
                                                 13
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 27 ) THEN
                                                 14
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 28 ) THEN
                                                 15
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 29 ) THEN
                                                 16
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 30 ) THEN
                                                 17
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 31 ) THEN
                                                 18
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 32 ) THEN
                                                 19
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 33 ) THEN
                                                 20
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 34 ) THEN
                                                 21
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 35 ) THEN
                                                 22
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 36 ) THEN
                                                 23
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 37 ) THEN
                                                 24
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 38 ) THEN
                                                 25
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 39 ) THEN
                                                 26
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 40 ) THEN
                                                 27
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 41 ) THEN
                                                 28
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 42 ) THEN
                                                 29
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 43 ) THEN
                                                 30
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 44 ) THEN
                                                 31
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 45 ) THEN
                                                 32
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 46 ) THEN
                                                 33
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 47 ) THEN
                                                 34
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 48 ) THEN
                                                 35
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 49 ) THEN
                                                 36
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 50 ) THEN
                                                 37
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 51 ) THEN
                                                 38
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 52 ) THEN
                                                 39
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 53 ) THEN
                                                 40
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 54 ) THEN
                                                 41
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 55 ) THEN
                                                 42
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 56 ) THEN
                                                 43
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 57 ) THEN
                                                 44
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 58 ) THEN
                                                 45
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 59 ) THEN
                                                 46
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 60 ) THEN
                                                 47
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 61 ) THEN
                                                 48
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 62 ) THEN
                                                 49
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 63 ) THEN
                                                 50
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 64 ) THEN
                                                 51
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 65 ) THEN
                                                 52
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 66 ) THEN
                                                 53
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 67 ) THEN
                                                 54
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 68 ) THEN
                                                 55
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 69 ) THEN
                                                 56
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 70 ) THEN
                                                 57
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 71 ) THEN
                                                 58
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 72 ) THEN
                                                 59
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 73 ) THEN
                                                 60
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 74 ) THEN
                                                 61
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 75 ) THEN
                                                 62
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 76 ) THEN
                                                 63
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 77 ) THEN
                                                 64
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 78 ) THEN
                                                 65
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 79 ) THEN
                                                 66
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 80 ) THEN
                                                 67
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 81 ) THEN
                                                 68
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 82 ) THEN
                                                 69
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 83 ) THEN
                                                 70
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 84 ) THEN
                                                 71
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 85 ) THEN
                                                 72
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 86 ) THEN
                                                 73
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 87 ) THEN
                                                 74
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 88 ) THEN
                                                 75
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 89 ) THEN
                                                 76
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 90 ) THEN
                                                 77
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 91 ) THEN
                                                 78
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 92 ) THEN
                                                 79
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 93 ) THEN
                                                 80
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 94 ) THEN
                                                 81
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 95 ) THEN
                                                 82
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 96 ) THEN
                                                 83
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 97 ) THEN
                                                 84
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 98 ) THEN
                                                 85
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 99 ) THEN
                                                 86
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 100 ) THEN
                                                 87
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 101 ) THEN
                                                 88
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 102 ) THEN
                                                 89
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 103 ) THEN
                                                 90
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 104 ) THEN
                                                 91
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 105 ) THEN
                                                 92
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 106 ) THEN
                                                 93
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 107 ) THEN
                                                 94
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 108 ) THEN
                                                 95
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 109 ) THEN
                                                 96
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 110 ) THEN
                                                 97
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 111 ) THEN
                                                 98
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 112 ) THEN
                                                 99
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 113 ) THEN
                                                 101
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 114 ) THEN
                                                 102
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 115 ) THEN
                                                 103
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 116 ) THEN
                                                 104
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 117 ) THEN
                                                 105
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 118 ) THEN
                                                 106
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 119 ) THEN
                                                 107
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 120 ) THEN
                                                 108
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 121 ) THEN
                                                 109
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 145 ) THEN
                                                 110
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 146 ) THEN
                                                 111
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 147 ) THEN
                                                 112
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 122 ) THEN
                                                 113
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 123 ) THEN
                                                 114
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 124 ) THEN
                                                 115
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 125 ) THEN
                                                 116
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 126 ) THEN
                                                 117
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 127 ) THEN
                                                 118
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 128 ) THEN
                                                 119
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 129 ) THEN
                                                 120
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 130 ) THEN
                                                 121
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 131 ) THEN
                                                 122
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 132 ) THEN
                                                 123
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 133 ) THEN
                                                 124
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 480 THEN
                                                 125
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 480 THEN
                                                 126
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 480 THEN
                                                 127
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 480 THEN
                                                 128
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 0.1370 THEN
                                                 129
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 0.15048 THEN
                                                 130
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 0.14248 THEN
                                                 131
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 730 THEN
                                                 132
                                             ELSE
                                                 cr.CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           cr.USAGE,
           PRICE = 800,
           TOTAL_AMOUNT = cr.USAGE * 800
    FROM TBL_PREPAID_CREDIT cr
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = cr.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE cr.CREDIT_MONTH
          BETWEEN @D1 AND @D2
          AND (cr.CREDIT_MONTH
          BETWEEN '2016-03-01' AND '2017-02-01'
              )
    UNION ALL
    SELECT cr.CREDIT_MONTH,
           cr.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 22 ) THEN
                                                 2
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 23 ) THEN
                                                 3
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 134 ) THEN
                                                 4
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 ) THEN
                                                 5
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 135 ) THEN
                                                 6
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 136 ) THEN
                                                 7
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 137 ) THEN
                                                 8
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 138 ) THEN
                                                 9
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 139 ) THEN
                                                 10
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 140 ) THEN
                                                 11
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 12
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 26 ) THEN
                                                 13
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 27 ) THEN
                                                 14
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 28 ) THEN
                                                 15
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 29 ) THEN
                                                 16
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 30 ) THEN
                                                 17
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 31 ) THEN
                                                 18
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 32 ) THEN
                                                 19
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 33 ) THEN
                                                 20
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 34 ) THEN
                                                 21
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 35 ) THEN
                                                 22
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 36 ) THEN
                                                 23
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 37 ) THEN
                                                 24
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 38 ) THEN
                                                 25
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 39 ) THEN
                                                 26
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 40 ) THEN
                                                 27
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 41 ) THEN
                                                 28
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 42 ) THEN
                                                 29
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 43 ) THEN
                                                 30
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 44 ) THEN
                                                 31
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 45 ) THEN
                                                 32
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 46 ) THEN
                                                 33
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 47 ) THEN
                                                 34
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 48 ) THEN
                                                 35
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 49 ) THEN
                                                 36
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 50 ) THEN
                                                 37
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 51 ) THEN
                                                 38
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 52 ) THEN
                                                 39
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 53 ) THEN
                                                 40
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 54 ) THEN
                                                 41
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 55 ) THEN
                                                 42
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 56 ) THEN
                                                 43
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 57 ) THEN
                                                 44
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 58 ) THEN
                                                 45
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 59 ) THEN
                                                 46
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 60 ) THEN
                                                 47
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 61 ) THEN
                                                 48
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 62 ) THEN
                                                 49
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 63 ) THEN
                                                 50
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 64 ) THEN
                                                 51
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 65 ) THEN
                                                 52
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 66 ) THEN
                                                 53
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 67 ) THEN
                                                 54
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 68 ) THEN
                                                 55
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 69 ) THEN
                                                 56
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 70 ) THEN
                                                 57
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 71 ) THEN
                                                 58
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 72 ) THEN
                                                 59
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 73 ) THEN
                                                 60
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 74 ) THEN
                                                 61
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 75 ) THEN
                                                 62
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 76 ) THEN
                                                 63
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 77 ) THEN
                                                 64
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 78 ) THEN
                                                 65
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 79 ) THEN
                                                 66
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 80 ) THEN
                                                 67
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 81 ) THEN
                                                 68
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 82 ) THEN
                                                 69
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 83 ) THEN
                                                 70
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 84 ) THEN
                                                 71
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 85 ) THEN
                                                 72
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 86 ) THEN
                                                 73
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 87 ) THEN
                                                 74
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 88 ) THEN
                                                 75
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 89 ) THEN
                                                 76
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 90 ) THEN
                                                 77
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 91 ) THEN
                                                 78
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 92 ) THEN
                                                 79
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 93 ) THEN
                                                 80
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 94 ) THEN
                                                 81
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 95 ) THEN
                                                 82
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 96 ) THEN
                                                 83
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 97 ) THEN
                                                 84
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 98 ) THEN
                                                 85
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 99 ) THEN
                                                 86
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 100 ) THEN
                                                 87
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 101 ) THEN
                                                 88
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 102 ) THEN
                                                 89
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 103 ) THEN
                                                 90
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 104 ) THEN
                                                 91
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 105 ) THEN
                                                 92
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 106 ) THEN
                                                 93
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 107 ) THEN
                                                 94
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 108 ) THEN
                                                 95
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 109 ) THEN
                                                 96
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 110 ) THEN
                                                 97
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 111 ) THEN
                                                 98
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 112 ) THEN
                                                 99
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 113 ) THEN
                                                 101
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 114 ) THEN
                                                 102
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 115 ) THEN
                                                 103
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 116 ) THEN
                                                 104
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 117 ) THEN
                                                 105
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 118 ) THEN
                                                 106
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 119 ) THEN
                                                 107
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 120 ) THEN
                                                 108
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 121 ) THEN
                                                 109
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 145 ) THEN
                                                 110
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 146 ) THEN
                                                 111
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 147 ) THEN
                                                 112
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 122 ) THEN
                                                 113
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 123 ) THEN
                                                 114
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 124 ) THEN
                                                 115
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 125 ) THEN
                                                 116
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 126 ) THEN
                                                 117
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 127 ) THEN
                                                 118
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 128 ) THEN
                                                 119
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 129 ) THEN
                                                 120
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 130 ) THEN
                                                 121
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 131 ) THEN
                                                 122
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 132 ) THEN
                                                 123
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 133 ) THEN
                                                 124
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 480 THEN
                                                 125
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 480 THEN
                                                 126
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 480 THEN
                                                 127
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 480 THEN
                                                 128
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
                                                  AND PRICE = 0.1370 THEN
                                                 129
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
                                                  AND PRICE = 0.15048 THEN
                                                 130
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
                                                  AND PRICE = 0.14248 THEN
                                                 131
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
                                                  AND PRICE = 730 THEN
                                                 132
                                             ELSE
                                                 cr.CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           cr.USAGE,
           PRICE = cr.BASED_PRICE,
           TOTAL_AMOUNT = cr.USAGE * BASED_PRICE
    FROM TBL_PREPAID_CREDIT cr
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = cr.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE cr.CREDIT_MONTH
    BETWEEN @D1 AND @D2

    -- BUILD CROSS TAB DEFINITION
    SELECT CUSTOMER_CONNECTION_TYPE_ID,
           COL = ROW_NUMBER() OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID) % 2,
           ROW = (1 + ROW_NUMBER() OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID)) / 2
    INTO #DEF1
    FROM
    (SELECT DISTINCT CUSTOMER_CONNECTION_TYPE_ID FROM #TMP1) x

    -- TO FILL 12MONTHs DATA 
    INSERT INTO #TMP1
    SELECT m.M,
           t.CURRENCY_ID,
           t.CURRENCY_SING,
           t.CUSTOMER_CONNECTION_TYPE_ID,
           TOTAL_USAGE = 0,
           PRICE = 0,
           TOTAL_AMOUNT = 0
    FROM #MONTHS m,
    (
        SELECT DISTINCT
               CUSTOMER_CONNECTION_TYPE_ID,
               CURRENCY_ID,
               CURRENCY_SING
        FROM #TMP1
    ) t

    -- IN CASE NO RECORD
    IF
    (
        SELECT COUNT(*) FROM #TMP1
    ) = 0
    BEGIN
        INSERT INTO #TMP1
        SELECT M,
               0,
               0,
               0,
               0,
               0,
               0
        FROM #MONTHS
        INSERT INTO #DEF1
        SELECT 0,
               1,
               1
    END
    -- RENDER RESULT
    SELECT ROW,
           INVOICE_MONTH,
           TYPE_ID1 = MAX(   CASE
                                 WHEN COL = 1 THEN
                                     t.CUSTOMER_CONNECTION_TYPE_ID
                                 ELSE
                                     0
                             END
                         ),
           [TYPE_NAME] = CONVERT(NTEXT,
                                 MAX(   CASE
                                            WHEN COL = 1 THEN
                                                TYPE_NAME
                                            ELSE
                                                ''
                                        END
                                    )
                                ),
           USAGE1 = NULLIF(SUM(   CASE
                                      WHEN COL = 1 THEN
                                          t.TOTAL_USAGE
                                      ELSE
                                          0
                                  END
                              ), 0),
           PRICE1 = NULLIF(MAX(   CASE
                                      WHEN COL = 1 THEN
                                          t.PRICE
                                      ELSE
                                          0
                                  END
                              ), 0),
           CURRENCY_ID1 = MAX(   CASE
                                     WHEN COL = 1 THEN
                                         t.CURRENCY_ID
                                     ELSE
                                         0
                                 END
                             ),
           CURR_SIGN1 = MAX(   CASE
                                   WHEN COL = 1 THEN
                                       t.CURRENCY_SING
                                   ELSE
                                       ''
                               END
                           ),
           AMOUNT1 = NULLIF(SUM(   CASE
                                       WHEN COL = 1 THEN
                                           t.TOTAL_AMOUNT
                                       ELSE
                                           0
                                   END
                               ), 0),
           TYPE_ID2 = MAX(   CASE
                                 WHEN COL = 0 THEN
                                     t.CUSTOMER_CONNECTION_TYPE_ID
                                 ELSE
                                     0
                             END
                         ),
           TYPE_NAME2 = MAX(   CASE
                                   WHEN COL = 0 THEN
                                       TYPE_NAME
                                   ELSE
                                       ''
                               END
                           ),
           USAGE2 = NULLIF(SUM(   CASE
                                      WHEN COL = 0 THEN
                                          t.TOTAL_USAGE
                                      ELSE
                                          0
                                  END
                              ), 0),
           PRICE2 = NULLIF(MAX(   CASE
                                      WHEN COL = 0 THEN
                                          t.PRICE
                                      ELSE
                                          0
                                  END
                              ), 0),
           CURRENCY_ID2 = MAX(   CASE
                                     WHEN COL = 0 THEN
                                         t.CURRENCY_ID
                                     ELSE
                                         0
                                 END
                             ),
           CURR_SIGN2 = MAX(   CASE
                                   WHEN COL = 0 THEN
                                       t.CURRENCY_SING
                                   ELSE
                                       ''
                               END
                           ),
           AMOUNT2 = NULLIF(SUM(   CASE
                                       WHEN COL = 0 THEN
                                           t.TOTAL_AMOUNT
                                       ELSE
                                           0
                                   END
                               ), 0)
    FROM #TMP1 t
        LEFT JOIN #DEF1 d
            ON d.CUSTOMER_CONNECTION_TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
        LEFT JOIN #TMP_TYPE1 c
            ON c.TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
    GROUP BY ROW,
             INVOICE_MONTH
    ORDER BY ROW,
             INVOICE_MONTH
--END REPORT_QUARTER_SOLD_CONSUMER
END

ELSE
BEGIN
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'លំនៅដ្ឋាន'
    INTO #TMP_TYPE UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ឧស្សាហកម្ម MV'
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ឧស្សាហកម្ម MV តាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ឧស្សាហកម្ម MV ពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់តាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកលក់ពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញតាមពេល'
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ឧស្សាហកម្ម LV ត្រង់ស្វូអ្នកទិញពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ឧស្សាហកម្ម LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 12,
           TYPE_NAME = N'ពាណិជ្ជកម្ម MV'
    UNION ALL
    SELECT TYPE_ID = 13,
           TYPE_NAME = N'ពាណិជ្ជកម្ម MV តាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 14,
           TYPE_NAME = N'ពាណិជ្ជកម្ម MV ពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT TYPE_ID = 15,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV​ ត្រង់ស្វូអ្នកលក់'
    UNION ALL
    SELECT TYPE_ID = 16,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកលក់​តាមពេល'
    UNION ALL
    SELECT TYPE_ID = 17,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកលក់ពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT TYPE_ID = 18,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកទិញ'
    UNION ALL
    SELECT TYPE_ID = 19,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV​ ត្រង់ស្វូអ្នកទិញតាមពេលវេលា'
    UNION ALL
    SELECT TYPE_ID = 20,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV ត្រង់ស្វូអ្នកទិញពន្លឺព្រះអាទិត្យ'
    UNION ALL
    SELECT TYPE_ID = 21,
           TYPE_NAME = N'ពាណិជ្ជកម្ម LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 22,
           TYPE_NAME = N'អង្គភាពរដ្ឋ LV សាធារណៈ'
    UNION ALL
    SELECT TYPE_ID = 23,
           TYPE_NAME = N'សាលារៀន, មន្ទីរពេទ្យ'

    -- BUILD DATA VALUE TABLE
    SELECT i.INVOICE_MONTH,
           i.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 21 ) THEN
                                                 4
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 5
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 6
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 7
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 8
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 9
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 10
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 11
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 12
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 13
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 14
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 15
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 16
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 17
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 18
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 19
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 20
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 21
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 22
                                             WHEN i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 23
                                             ELSE
                                                 i.CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           TOTAL_USAGE = i.TOTAL_USAGE + ISNULL(adj.ADJUST_USAGE, 0),
           d.PRICE,
           i.TOTAL_AMOUNT
    INTO #TMP
    FROM TBL_INVOICE i
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = i.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = i.CURRENCY_ID
        OUTER APPLY
    (
        SELECT TOP 1
               PRICE
        FROM TBL_INVOICE_DETAIL
        WHERE INVOICE_ID = i.INVOICE_ID
        ORDER BY INVOICE_DETAIL_ID DESC
    ) d
        OUTER APPLY
    (
        SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
        FROM TBL_INVOICE_ADJUSTMENT
        WHERE INVOICE_ID = i.INVOICE_ID
    ) adj
    WHERE i.INVOICE_MONTH
          BETWEEN @D1 AND @D2
          --AND c.CUSTOMER_CONNECTION_TYPE_ID <> -1 -- USE IN PRODUCTION
          AND i.IS_SERVICE_BILL = 0
          AND c.CUSTOMER_ID NOT IN
              (
                  SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
              )
          AND c.IS_REACTIVE = 0
          AND i.INVOICE_STATUS NOT IN ( 3 )
    UNION ALL
    SELECT CONVERT(NVARCHAR(7), CREATE_ON, 126) + '-01',
           b.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 21 ) THEN
                                                 4
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 5
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 6
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 7
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 8
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 9
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 10
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 11
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 12
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 13
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 14
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 15
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 16
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 17
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 18
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 19
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 20
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 21
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 22
                                             WHEN CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 23
                                             ELSE
                                                 CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           b.BUY_QTY,
           b.PRICE,
           b.BUY_AMOUNT
    FROM TBL_CUSTOMER_BUY b
        INNER JOIN TBL_PREPAID_CUSTOMER p
            ON p.PREPAID_CUS_ID = b.PREPAID_CUS_ID
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = p.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = b.CURRENCY_ID
    WHERE b.CREATE_ON
          BETWEEN @D1 AND @D2
          AND c.CUSTOMER_ID NOT IN
              (
                  SELECT CUSTOMER_ID FROM TBL_LICENSEE_CONNECTION WHERE IS_ACTIVE = 1
              )
          AND c.IS_REACTIVE = 0
          AND CONVERT(NVARCHAR(7), CREATE_ON, 126) + '-01' <= '2016-02-01'
    UNION ALL
    SELECT cr.CREDIT_MONTH,
           cr.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 21 ) THEN
                                                 4
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 5
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 6
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 7
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 8
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 9
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 10
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 11
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 12
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 13
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 14
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 15
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 16
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 17
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 18
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 19
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 20
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 21
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 22
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 23
                                             ELSE
                                                 cr.CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           cr.USAGE,
           PRICE = 800,
           TOTAL_AMOUNT = cr.USAGE * 800
    FROM TBL_PREPAID_CREDIT cr
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = cr.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE cr.CREDIT_MONTH
          BETWEEN @D1 AND @D2
          AND (cr.CREDIT_MONTH
          BETWEEN '2016-03-01' AND '2017-02-01'
              )
    UNION ALL
    SELECT cr.CREDIT_MONTH,
           cr.CURRENCY_ID,
           cc.CURRENCY_SING,
           CUSTOMER_CONNECTION_TYPE_ID = CASE
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID = 1 THEN
                                                 1
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 ) THEN
                                                 2
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 ) THEN
                                                 3
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 21 ) THEN
                                                 4
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 ) THEN
                                                 5
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 18 ) THEN
                                                 6
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 7
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 ) THEN
                                                 8
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 17 ) THEN
                                                 9
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 10
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 ) THEN
                                                 11
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 ) THEN
                                                 12
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 ) THEN
                                                 13
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 14
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 ) THEN
                                                 15
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 20 ) THEN
                                                 16
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 17
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 ) THEN
                                                 18
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 19 ) THEN
                                                 19
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 0 ) THEN
                                                 20
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 15 ) THEN
                                                 21
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 16 ) THEN
                                                 22
                                             WHEN cr.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 ) THEN
                                                 23
                                             ELSE
                                                 cr.CUSTOMER_CONNECTION_TYPE_ID
                                         END,
           cr.USAGE,
           PRICE = cr.BASED_PRICE,
           TOTAL_AMOUNT = cr.USAGE * BASED_PRICE
    FROM TBL_PREPAID_CREDIT cr
        INNER JOIN TBL_CUSTOMER c
            ON c.CUSTOMER_ID = cr.CUSTOMER_ID
        INNER JOIN TLKP_CURRENCY cc
            ON cc.CURRENCY_ID = cr.CURRENCY_ID
    WHERE cr.CREDIT_MONTH
    BETWEEN @D1 AND @D2

    -- BUILD CROSS TAB DEFINITION
    SELECT CUSTOMER_CONNECTION_TYPE_ID,
           COL = ROW_NUMBER() OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID) % 2,
           ROW = (1 + ROW_NUMBER() OVER (ORDER BY CUSTOMER_CONNECTION_TYPE_ID)) / 2
    INTO #DEF
    FROM
    (SELECT DISTINCT CUSTOMER_CONNECTION_TYPE_ID FROM #TMP) x

    -- TO FILL 12MONTHs DATA 
    INSERT INTO #TMP
    SELECT m.M,
           t.CURRENCY_ID,
           t.CURRENCY_SING,
           t.CUSTOMER_CONNECTION_TYPE_ID,
           TOTAL_USAGE = 0,
           PRICE = 0,
           TOTAL_AMOUNT = 0
    FROM #MONTHS m,
    (
        SELECT DISTINCT
               CUSTOMER_CONNECTION_TYPE_ID,
               CURRENCY_ID,
               CURRENCY_SING
        FROM #TMP
    ) t

    -- IN CASE NO RECORD
    IF
    (
        SELECT COUNT(*) FROM #TMP
    ) = 0
    BEGIN
        INSERT INTO #TMP
        SELECT M,
               0,
               0,
               0,
               0,
               0,
               0
        FROM #MONTHS
        INSERT INTO #DEF
        SELECT 0,
               1,
               1
    END
    -- RENDER RESULT
    SELECT ROW,
           INVOICE_MONTH,
           TYPE_ID1 = MAX(   CASE
                                 WHEN COL = 1 THEN
                                     t.CUSTOMER_CONNECTION_TYPE_ID
                                 ELSE
                                     0
                             END
                         ),
           [TYPE_NAME] = MAX(   CASE
                                    WHEN COL = 1 THEN
                                        TYPE_NAME
                                    ELSE
                                        ''
                                END
                            ),
           USAGE1 = NULLIF(SUM(   CASE
                                      WHEN COL = 1 THEN
                                          t.TOTAL_USAGE
                                      ELSE
                                          0
                                  END
                              ), 0),
           PRICE1 = NULLIF(MAX(   CASE
                                      WHEN COL = 1 THEN
                                          t.PRICE
                                      ELSE
                                          0
                                  END
                              ), 0),
           CURRENCY_ID1 = MAX(   CASE
                                     WHEN COL = 1 THEN
                                         t.CURRENCY_ID
                                     ELSE
                                         0
                                 END
                             ),
           CURR_SIGN1 = MAX(   CASE
                                   WHEN COL = 1 THEN
                                       t.CURRENCY_SING
                                   ELSE
                                       ''
                               END
                           ),
           AMOUNT1 = NULLIF(SUM(   CASE
                                       WHEN COL = 1 THEN
                                           t.TOTAL_AMOUNT
                                       ELSE
                                           0
                                   END
                               ), 0),
           TYPE_ID2 = MAX(   CASE
                                 WHEN COL = 0 THEN
                                     t.CUSTOMER_CONNECTION_TYPE_ID
                                 ELSE
                                     0
                             END
                         ),
           TYPE_NAME2 = MAX(   CASE
                                   WHEN COL = 0 THEN
                                       TYPE_NAME
                                   ELSE
                                       ''
                               END
                           ),
           USAGE2 = NULLIF(SUM(   CASE
                                      WHEN COL = 0 THEN
                                          t.TOTAL_USAGE
                                      ELSE
                                          0
                                  END
                              ), 0),
           PRICE2 = NULLIF(MAX(   CASE
                                      WHEN COL = 0 THEN
                                          t.PRICE
                                      ELSE
                                          0
                                  END
                              ), 0),
           CURRENCY_ID2 = MAX(   CASE
                                     WHEN COL = 0 THEN
                                         t.CURRENCY_ID
                                     ELSE
                                         0
                                 END
                             ),
           CURR_SIGN2 = MAX(   CASE
                                   WHEN COL = 0 THEN
                                       t.CURRENCY_SING
                                   ELSE
                                       ''
                               END
                           ),
           AMOUNT2 = NULLIF(SUM(   CASE
                                       WHEN COL = 0 THEN
                                           t.TOTAL_AMOUNT
                                       ELSE
                                           0
                                   END
                               ), 0)
    FROM #TMP t
        LEFT JOIN #DEF d
            ON d.CUSTOMER_CONNECTION_TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
        LEFT JOIN #TMP_TYPE c
            ON c.TYPE_ID = t.CUSTOMER_CONNECTION_TYPE_ID
    GROUP BY ROW,
             INVOICE_MONTH
    ORDER BY ROW,
             INVOICE_MONTH
--END REPORT_QUARTER_SOLD_CONSUMER
END
GO

IF OBJECT_ID('TBL_LOG_SEND_MAIL') IS NULL
	CREATE TABLE TBL_LOG_SEND_MAIL(
	LOG_ID  UNIQUEIDENTIFIER PRIMARY KEY,
	REPORT_NAME NVARCHAR(250) NOT NULL,
	REPORT_DATE DATETIME NOT NULL,
	SEND_DATE DATETIME NOT NULL,
	SEND_TO NVARCHAR(500) NOT NULL
	)
GO

IF OBJECT_ID('REPORT_REF_INVOICE') IS NOT NULL
	DROP PROC REPORT_REF_INVOICE
GO

CREATE PROC dbo.REPORT_REF_INVOICE
    @DATA_MONTH DATETIME = '2021-01-01',
    @CUSTOMER_GROUP_ID INT = 0,
    @CUSTOMER_CONNECTION_TYPE_ID INT = 0,
    @CURRENCY_ID INT = 0,
    @V1 DECIMAL(18, 4) = 0,
    @V2 DECIMAL(18, 4) = 1000000,
    @BILLING_CYCLE_ID INT = 0,
    @AREA_ID INT = 0
AS
/*
@ 2016-05-25 Morm Raksmey
	Add Column Billing Cycle Id and Billing Cycle Name
@ 2017-03-10 Sieng Sothera
	Trim meter code without '0'
@ 2017-04-19 Sieng Sotheara
	Update REF Invoice 2016 & 2017
@ 2018-09-19 Pov Lyhorng
	fix Organization customer is non subsidy type
@ 2019-01-16 Phuong Sovathvong
	Update REF Invoice 2019
@ 2019-12-12 HOR DARA
	Update REF Invoice 2020
@ 2020-07-28 HOR DARA
	Update REF Invoice 2020 - market vendor get subsidy even over 2000kwh (49 licensee)
@ 2021-01-08 Vonn kimputhmunyvorn
	Update REF Invoice 2021
*/
DECLARE @LICENSE_TYPE_ID INT,
        @BASED_TARIFF DECIMAL(18, 5),
        @SUBSIDY_TARIFF DECIMAL(18, 5);
SELECT @LICENSE_TYPE_ID = LICENSE_TYPE_ID,
       @SUBSIDY_TARIFF = SUBSIDY_TARIFF,
       @BASED_TARIFF = BASED_TARIFF
FROM dbo.TBL_TARIFF
WHERE DATA_MONTH = @DATA_MONTH;
CREATE TABLE #RESULT
(
    TYPE_ID INT,
    TYPE_NAME NVARCHAR(250),
    TYPE_OF_SALE_ID INT,
    TYPE_OF_SALE_NAME NVARCHAR(250),
    HISTORY_ID BIGINT,
    CUSTOMER_GROUP_ID INT,
    CUSTOMER_GROUP_NAME NVARCHAR(250),
    INVOICE_ID BIGINT,
    DATA_MONTH DATETIME,
    CUSTOMER_ID INT,
    CUSTOMER_CODE NVARCHAR(50),
    FIRST_NAME_KH NVARCHAR(250),
    LAST_NAME_KH NVARCHAR(250),
    AREA_ID INT,
    AREA_CODE NVARCHAR(50),
    BOX_ID INT,
    BOX_CODE NVARCHAR(50),
    CUSTOMER_CONNECTION_TYPE_ID INT,
    CUSTOMER_CONNECTION_TYPE_NAME NVARCHAR(250),
    AMPARE_ID INT,
    AMPARE_NAME NVARCHAR(50),
    PRICE_ID INT,
    METER_ID INT,
    METER_CODE NVARCHAR(50),
    MULTIPLIER INT,
    CURRENCY_SIGN NVARCHAR(50),
    CURRENCY_NAME NVARCHAR(50),
    CURRENCY_ID INT,
    PRICE DECIMAL(18, 5),
    BASED_PRICE DECIMAL(18, 5),
    START_USAGE DECIMAL(18, 4),
    END_USAGE DECIMAL(18, 4),
    TOTAL_USAGE DECIMAL(18, 4),
    TOTAL_AMOUNT DECIMAL(18, 5),
    IS_POST_PAID BIT,
    IS_AGRICULTURE BIT,
    SHIFT_ID INT,
    IS_ACTIVE BIT,
    BILLING_CYCLE_ID INT,
    BILLING_CYCLE_NAME NVARCHAR(250),
    ROW_DATE DATETIME,
    CUSTOMER_TYPE_ID INT,
    CUSTOMER_TYPE_NAME NVARCHAR(50),
    LICENSE_NUMBER NVARCHAR(50)
);
/*REF 2016*/
IF @DATA_MONTH
   BETWEEN '2016-03-01' AND '2017-02-01'
BEGIN
    INSERT INTO #RESULT
    /*លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ*/
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 4
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 3
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'លក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 2
    UNION ALL
    /*លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)*/
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 2
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 4
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 2
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 3
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'លក់ឲ្យអតិថិជន MV​(ឧស្សាហកម្ម ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 2
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 2
    UNION ALL
    /*លក់លើតង់ស្យុងទាប*/
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'លក់លើតង់ស្យុងទាប',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'អតិថិជនធម្មតា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 1
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 1;
END;
/*REF 2017*/
ELSE IF @DATA_MONTH
        BETWEEN '2017-03-01' AND '2018-12-01'
BEGIN
    INSERT INTO #RESULT
    /*ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ*/
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាប័ណ្ណ',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID = 3
          AND i.CUSTOMER_CONNECTION_TYPE_ID = 13
    UNION ALL
    /*ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)*/
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4, 7, 8, 10 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3, 6 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2, 5 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2, 5 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9, 14, 15, 16 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    /*ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)*/
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 1, 2, 5 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9, 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    /* ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន */
    SELECT TYPE_ID = 0,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 4 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 0,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 4 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 0,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 51 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_GROUP_ID IN ( 4 )
          AND i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 51;
END;
/*REF 2019*/
ELSE IF @DATA_MONTH
        BETWEEN '2019-01-01' AND '2020-01-01'
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 13
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវាលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 15, 16 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 51 AND 200
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201;
END;
/*REF 2020 - NORMAL LICENSE TYPE ID =1*/
ELSE IF @DATA_MONTH
        BETWEEN '2020-02-01' AND '2020-12-01'
        AND @LICENSE_TYPE_ID = 1
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 13
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 4 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 7 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 3 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 17 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 2 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 18 )
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 8 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 10 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 6 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 19 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 5 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 20 )
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 15, 16 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (ពាណិជ្ជកម្ម និងរដ្ឋបាល អាជីវកម្ម)',
           TYPE_OF_SALE_ID = -2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = -3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 51 AND 200
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = -7,
           TYPE_OF_SALE_NAE = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201;
END;
/*REF 2020 - LICENSE TYPE ID =2*/
ELSE IF @DATA_MONTH
        BETWEEN '2020-02-01' AND '2020-12-01'
        AND @LICENSE_TYPE_ID = 2
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 14, 15, 16 );
--AND i.TOTAL_USAGE<2001
END;

/*REF 2021 - NORMAL LICENSE TYPE ID =1*/
ELSE IF @DATA_MONTH
        BETWEEN '2021-01-01' AND '9999-12-01'
        AND @LICENSE_TYPE_ID = 1
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 12,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 11
    UNION ALL
    SELECT TYPE_ID = 12,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 12
    UNION ALL
    SELECT TYPE_ID = 12,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនអ្នកកាន់អាជ្ញាបណ្ណ',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 13

    --ឧស្សាហកម្មវិស័យរុករករ៉ែ
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 26
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 27
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 28
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 29
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 30
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 31
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 32
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 33
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 34
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 35
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 36
    UNION ALL
    SELECT TYPE_ID = 11,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យរុករករ៉ែ) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 37

    --ឧស្សាហកម្មវិស័យកម្មន្តសាល
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 38
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 39
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 40
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 41
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 42
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 43
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 44
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 45
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 46
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 47
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 48
    UNION ALL
    SELECT TYPE_ID = 10,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យកម្មន្តសាល) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 49

    --ឧស្សាហកម្មវិស័យវាយនភណ្ឌ
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 50
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 51
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 52
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 53
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 54
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 55
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 56
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 57
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 58
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 59
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 60
    UNION ALL
    SELECT TYPE_ID = 9,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្មវិស័យវាយនភណ្ឌ) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 61

    --កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 62
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 63
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 64
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 65
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 66
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 67
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 68
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 69
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 70
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 71
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 72
    UNION ALL
    SELECT TYPE_ID = 8,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (កសិកម្មដាំដុះ ចិញ្ចឹមសត្វ និងនេសាទ) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 73

    --សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម (បូមទឹក សម្ងួត បង្កក ការផលិតជីកសិកម្ម ….។ល។)
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 74
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 75
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 76
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 77
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 78
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 79
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 80
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 81
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 82
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 83
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 84
    UNION ALL
    SELECT TYPE_ID = 7,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពបម្រើឲ្យវិស័យកសិកម្ម) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 85

    --សកម្មភាពកែច្នៃផលិតផលកសិកម្ម
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 86
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 87
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 88
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 89
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 90
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 91
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 92
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 93
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 94
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 95
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 96
    UNION ALL
    SELECT TYPE_ID = 6,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សកម្មភាពកែច្នៃផលិតផលកសិកម្ម) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 97

    --ពាណិជ្ជកម្ម
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 98
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 99
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 100
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 101
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 102
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 103
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 104
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 105
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 106
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 107
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 108
    UNION ALL
    SELECT TYPE_ID = 5,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ពាណិជ្ជកម្ម) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 109

    --រដ្ឋបាលសាធារណៈ
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 110
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 111
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 112
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 113
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 114
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 115
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 116
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 117
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 118
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 119
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 120
    UNION ALL
    SELECT TYPE_ID = 4,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 121
	UNION ALL
    SELECT TYPE_ID = 4,
			TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
			TYPE_OF_SALE_ID = 15,
			TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV ដែលទូទាត់ដោយថវិការដ្ឋ',
			i.*
	FROM dbo.TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 145
	UNION ALL
    SELECT TYPE_ID = 4,
			TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
			TYPE_OF_SALE_ID = 14,
			TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) ដែលទូទាត់ដោយថវិការដ្ឋ',
			i.*
	FROM dbo.TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 146
	UNION ALL
    SELECT TYPE_ID = 4,
			TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (រដ្ឋបាលសាធារណៈ)',
			TYPE_OF_SALE_ID = 13,
			TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) ដែលទូទាត់ដោយថវិការដ្ឋ',
			i.*
	FROM dbo.TBL_REF_INVOICE i
	WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 147
    --សេវាកម្មផ្សេងៗ
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 12,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 122
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 11,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 123
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 10,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 124
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 125
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 126
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៧ព្រឹកដល់៩យប់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 127
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 128
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 129
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) តាមពេលវេលា(៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 130
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ MV មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 131
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 132
    UNION ALL
    SELECT TYPE_ID = 3,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (សេវាកម្មផ្សេងៗ) ',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ) មានដាក់តម្លើងប្រភពពន្លឺព្រះអាទិត្យ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID = 133
    UNION ALL
    SELECT TYPE_ID = 2,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែបណ្តាញសាធារណៈ)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140, 144 )
          AND i.TOTAL_USAGE >= 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 9,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជន សាលារៀន មន្ទីរពេទ្យ មណ្ឌលសុខភាព នៅតំបន់ជនបទ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 9 )
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 22, 23, 134, 135, 136, 137, 138, 139, 140,144 )
          AND i.TOTAL_USAGE < 2001
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE <= 10
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 11 AND 50
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 51 ដល់ 200 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE
          BETWEEN 51 AND 200
    UNION ALL
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAE = N'ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 201 kWh/ខែ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 1 )
          AND i.TOTAL_USAGE >= 201
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 8,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម(ទិញ MV)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 0.13700
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 7,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ MV ម៉ោង៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 141 )
          AND i.PRICE = 480
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 6,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកលក់)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 0.15048
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 5,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកលក់ ម៉ោង៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 142 )
          AND i.PRICE = 480
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 4,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកទិញ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 0.14248
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 3,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV ត្រង់ស្វូរអ្នកទិញ ម៉ោង៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 143 )
          AND i.PRICE = 480
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 2,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV សាធារណៈ)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 730
    UNION ALL
    SELECT TYPE_ID = -1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (អ្នកប្រើប្រាស់ប្រភេទអនុគ្រោះពិសេស)',
           TYPE_OF_SALE_ID = 1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអ្នកបូមទឹកធ្វើកសិកម្ម និងអ្នកប្រើប្រាស់ក្នុងវិស័យកសិកម្ម (ទិញ LV សាធារណៈ ៩យប់ដល់៧ព្រឹក)',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 24 )
          AND i.PRICE = 480
END;
ELSE IF @DATA_MONTH
        BETWEEN '2021-01-01' AND '9999-12-31'
        AND @LICENSE_TYPE_ID = 2
BEGIN
    INSERT INTO #RESULT
    SELECT TYPE_ID = 1,
           TYPE_NAME = N'ថាមពលលក់ឲ្យអតិថិជននៅក្នុងបរិវេណផ្សារ',
           TYPE_OF_SALE_ID = -1,
           TYPE_OF_SALE_NAME = N'ថាមពលលក់ឲ្យអតិថិជនក្នុងផ្សារ',
           i.*
    FROM TBL_REF_INVOICE i
    WHERE i.CUSTOMER_CONNECTION_TYPE_ID IN ( 144 );
END;

/*RESULT*/
SELECT r.*,
       METER_FORMAT = REPLACE(LTRIM(REPLACE(ISNULL(METER_CODE, '-'), '0', ' ')), ' ', '0')
FROM #RESULT r
WHERE DATA_MONTH = @DATA_MONTH
      AND IS_ACTIVE = 1
      AND
      (
          @CUSTOMER_GROUP_ID = 0
          OR CUSTOMER_GROUP_ID = @CUSTOMER_GROUP_ID
      )
      AND
      (
          @CURRENCY_ID = 0
          OR CURRENCY_ID = @CURRENCY_ID
      )
      AND
      (
          @CUSTOMER_CONNECTION_TYPE_ID = 0
          OR CUSTOMER_CONNECTION_TYPE_ID = @CUSTOMER_CONNECTION_TYPE_ID
      )
      AND (TOTAL_USAGE
      BETWEEN @V1 AND @V2
          )
      AND
      (
          @BILLING_CYCLE_ID = 0
          OR BILLING_CYCLE_ID = @BILLING_CYCLE_ID
      )
      AND
      (
          @AREA_ID = 0
          OR AREA_ID = @AREA_ID
      )
ORDER BY TYPE_ID DESC,
         TYPE_OF_SALE_ID DESC,
         AREA_CODE,
         BOX_CODE;
--END REPORT_REF_INVOICE
GO

IF OBJECT_ID('REPORT_RECURRING_SERVICE_INCOME_SUMMARY') IS NOT NULL
	DROP PROC REPORT_RECURRING_SERVICE_INCOME_SUMMARY
GO

CREATE PROC dbo.REPORT_RECURRING_SERVICE_INCOME_SUMMARY
	@AREA_ID INT = 0,
	@INVOICE_ITEM_ID INT =0,
	@MONTH DATETIME='2022-01-01'
AS 
SELECT  
		cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		a.AREA_ID,a.AREA_NAME, 
		d.INVOICE_ITEM_ID,d.INVOICE_ITEM_NAME,
		QTY = SUM(d.QTY),
		SETTLE_AMOUNT = SUM(i.SETTLE_AMOUNT),
		PAID_AMOUNT = SUM(i.PAID_AMOUNT)
FROM TBL_INVOICE i
CROSS APPLY(
	SELECT TOP 1 it.INVOICE_ITEM_ID,it.INVOICE_ITEM_NAME,it.IS_RECURRING_SERVICE,QTY = d.USAGE
	FROM TBL_INVOICE_DETAIL d 
	INNER JOIN TBL_INVOICE_ITEM it ON it.INVOICE_ITEM_ID = d.INVOICE_ITEM_ID
	WHERE d.INVOICE_ID = i.INVOICE_ID 
		AND it.IS_RECURRING_SERVICE=1
) d
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID  
INNER JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
INNER JOIN dbo.TLKP_CURRENCY cx ON cx.CURRENCY_ID = i.CURRENCY_ID
WHERE	i.SETTLE_AMOUNT>0
		AND	(@INVOICE_ITEM_ID = 0 OR d.INVOICE_ITEM_ID =@INVOICE_ITEM_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID) 
		AND (i.INVOICE_MONTH=@MONTH)
GROUP BY cx.CURRENCY_ID, cx.CURRENCY_NAME, cx.CURRENCY_SING,  a.AREA_ID,a.AREA_NAME, 
		d.INVOICE_ITEM_ID,d.INVOICE_ITEM_NAME;
--- END OF REPORT_RECURRING_SERVICE_INCOME_SUMMARY
GO

IF OBJECT_ID('REPORT_RECURRING_SERVICE_INCOME_MONTHLY') IS NOT NULL
	DROP PROC REPORT_RECURRING_SERVICE_INCOME_MONTHLY
GO

CREATE PROC dbo.REPORT_RECURRING_SERVICE_INCOME_MONTHLY
	@AREA_ID INT = 0,
	@INVOICE_ITEM_ID INT =0,
	@YEAR INT=2011
AS 
SELECT  
		cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		i.INVOICE_MONTH, 
		d.INVOICE_ITEM_ID,d.INVOICE_ITEM_NAME,
		QTY = SUM(d.QTY),
		SETTLE_AMOUNT=SUM(i.SETTLE_AMOUNT),
		PAID_AMOUNT=SUM(i.PAID_AMOUNT)
FROM TBL_INVOICE i
CROSS APPLY(
	SELECT TOP 1 it.INVOICE_ITEM_ID,it.INVOICE_ITEM_NAME,it.IS_RECURRING_SERVICE,QTY=d.USAGE
	FROM TBL_INVOICE_DETAIL d 
	INNER JOIN TBL_INVOICE_ITEM it ON it.INVOICE_ITEM_ID = d.INVOICE_ITEM_ID
	WHERE d.INVOICE_ID = i.INVOICE_ID 
		AND it.IS_RECURRING_SERVICE=1
) d
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID  
INNER JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
INNER JOIN dbo.TLKP_CURRENCY cx ON cx.CURRENCY_ID = i.CURRENCY_ID
WHERE	i.SETTLE_AMOUNT>0
		AND	(@INVOICE_ITEM_ID = 0 OR d.INVOICE_ITEM_ID =@INVOICE_ITEM_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID) 
		AND (YEAR(i.INVOICE_MONTH)=@YEAR)
GROUP BY cx.CURRENCY_ID, cx.CURRENCY_NAME, cx.CURRENCY_SING, i.INVOICE_MONTH, 
		d.INVOICE_ITEM_ID,d.INVOICE_ITEM_NAME
-- END OF REPORT_RECURRING_SERVICE_INCOME_MONTHLY
GO
