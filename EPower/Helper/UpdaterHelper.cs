﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;

namespace EPower.Helper
{
    public class UpdaterHelper
    {
        public static List<Version> GetVersionList(string version)
        {
            var url = Base.Logic.Method.Utilities[Utility.UPDATE_SERVICE_URL];
            var client = new RestClient($"{url}/api/Version/GetVersionList");
            var request = new RestRequest(Method.GET);
            var parameter = new Parameter() { Name = "version", Value = version, Type = ParameterType.QueryString };
            request.AddParameter(parameter);
            var response = client.Execute<List<Version>>(request);
            var result = JsonConvert.DeserializeObject<List<Version>>(response.Content);
            return result;
        }

        public static Version GetLastVersion()
        {
            var url = Base.Logic.Method.Utilities[Utility.UPDATE_SERVICE_URL];
            var client = new RestClient($"{url}/api/Version/GetLastVersion");
            var request = new RestRequest(Method.GET);  
            var response = client.Execute<Version>(request);
            var result = JsonConvert.DeserializeObject<Version>(response.Content);
            return result;
        } 
    }
    public class Version
    {
        public int Id { get; set; }
        public string VersionName { get; set; }
        public string SourceUrl { get; set; }
        public string FileName { get; set; }
        public DateTime VersionDate { get; set; }
    }
}