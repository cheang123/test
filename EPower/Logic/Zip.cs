﻿using System;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;

namespace EPower.Logic
{
    public static class Zip
    {
        public static void ZipFile(string FullName, string OutputFileName, string password)
        { 
            int TrimLength = (Directory.GetParent(FullName)).FullName.Length;
            // find number of chars to remove 	// from orginal file path
            TrimLength += 1; //remove '\'
            FileStream ostream;
            byte[] obuffer;
            string outPath = OutputFileName;
            ZipOutputStream oZipStream = new ZipOutputStream(File.Create(outPath)); // create zip stream
            if (password != null && password != String.Empty)
            {
                oZipStream.Password = password;
            }
            oZipStream.SetLevel(9); // maximum compression
            ZipEntry oZipEntry;

            oZipEntry = new ZipEntry(FullName.Remove(0, TrimLength));
            oZipStream.PutNextEntry(oZipEntry);

            if (!FullName.EndsWith(@"/")) // if a file ends with '/' its a directory
            {
                ostream = File.OpenRead(FullName);
                obuffer = new byte[ostream.Length];
                ostream.Read(obuffer, 0, obuffer.Length);
                oZipStream.Write(obuffer, 0, obuffer.Length);
            }

            oZipStream.Finish();
            oZipStream.Close();
        }

        //Unzip Folder
        public static void UnZipFiles(string zipPathAndFile, string outputFolder, string password, bool deleteZipFile)
        {
            ICSharpCode.SharpZipLib.Zip.ZipConstants.DefaultCodePage = 437;
            ZipInputStream s = new ZipInputStream(File.OpenRead(zipPathAndFile));
            if (password != null && password != String.Empty)
                s.Password = password;
            ZipEntry theEntry;
            string tmpEntry = string.Empty;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                string directoryName = outputFolder;
                string fileName = Path.GetFileName(theEntry.Name);
                // create directory 
                if (directoryName != "")
                {
                    Directory.CreateDirectory(directoryName);
                }
                if (fileName != String.Empty)
                {
                    if (theEntry.Name.IndexOf(".ini") < 0)
                    {
                        string fullPath = directoryName + "\\" + theEntry.Name;
                        fullPath = fullPath.Replace("\\ ", "\\");
                        string fullDirPath = Path.GetDirectoryName(fullPath);
                        if (!Directory.Exists(fullDirPath)) Directory.CreateDirectory(fullDirPath);
                        FileStream streamWriter = File.Create(fullPath);
                        int size = 2048;
                        byte[] data = new byte[2048];
                        while (true)
                        {
                            size = s.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }
                        streamWriter.Close();
                    }
                }
            }
            s.Close();
            if (deleteZipFile)
                File.Delete(zipPathAndFile);
        }

        public static void ZipDirectory(string sourceDirectory,string outputFileName)
        {
            FastZip f = new FastZip();
            f.CreateZip(outputFileName, sourceDirectory, true, ""); 
        }

        //public static void FastZipDemo()
        //{
        //    // zip directory
        //    ICSharpCode.SharpZipLib.Zip.FastZip f = new ICSharpCode.SharpZipLib.Zip.FastZip();
        //    f.CreateZip("D:/test.zip", "D:/Test/", true, "");

        //    // extract zip file 
        //    f.ExtractZip("D:/test.zip", "D:/test2/", "");
        //}
    }
}
