﻿using System;

namespace TRC.Helpers
{
    public class ReadNumber
    {
        private static string[] AffterCurrencyKH = { "", " សេន" };
        private static string[] AffterCurrencyENG = { "", " CENT" };
        private static string[] oneDigitENG = { "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        private static string[] oneDigitKH = { "មួយ", "ពីរ", "បី", "បួន", "ប្រាំ", "ប្រាំមួយ", "ប្រាំពីរ", "ប្រាំបី", "ប្រាំបួន" };
        private static string[] twoDigitENG = { "Ten", "Eleven", "Twelve", "Thirteen", "FourTeen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
        private static string[] twoDigitKH = { "ដប់", "ដប់មួយ", "ដប់ពីរ", "ដប់បី", "ដប់បួន", "ដប់ប្រាំ", "ដប់ប្រាំមួយ", "ដប់ប្រាំពីរ", "ដប់ប្រាំបី", "ដប់ប្រាំបួន", "ម្ភៃ", "សាមសិប", "សែសិប", "ហាសិប", "ហុកសិប", "ចិតសិប", "ប៉ែតសិប", "កៅសិប" };


        /// <summary>
        /// Read number digit to word.
        /// Return Khmer language.
        /// </summary>
        /// <param name="numb"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        public static string ReadKH(decimal numb,string currency="") 
        { 
            return Read(numb, true, currency);
        }

        /// <summary>
        /// Read number digit to word.
        /// Return English language.
        /// </summary>
        /// <param name="numb"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        public static string ReadENG(decimal numb,string currency="")
        { 
            return Read(numb, false,currency);
        }


        /// <summary>
        /// Read Number to String
        /// </summary>
        /// <param name="numb">Number To Read</param>
        /// <param name="kh">True to get Khmer Language</param>
        /// <returns>String of Read Number</returns>
        public static string Read(decimal numb, bool kh, string  currency )
        {

            // to fix erro went read number that have long decimal
            numb = decimal.Parse(numb.ToString("#.00"));

            string stringNum = string.Empty, number = string.Empty, decimalVal = "", splitRange = "", dotStr = "",
                read = string.Empty;

            stringNum = numb.ToString();

            number = stringNum;

            try
            {
                int dot = stringNum.IndexOf(".");
                if (dot > 0)
                {
                    number = stringNum.Substring(0, dot);
                    decimalVal = stringNum.Substring(dot + 1);
                    if (Convert.ToInt32(decimalVal) > 0)
                    {

                        if (kh)
                        {
                            dotStr = readDecimal(decimalVal, kh);
                            splitRange = "ចុច"; 
                            dotStr += " សេន";
                            splitRange = string.Empty; 
                        }
                        else
                        {
                            dotStr = readDecimal(decimalVal, kh);
                            splitRange = " Dot"; 
                            dotStr += " CENT";
                            splitRange = string.Empty; 
                        }

                    }
                }
                if (kh)
                { 
                    read = dotStr == string.Empty ?
                        string.Format("{0} {1}{2}", number2WordKH(number).Replace(" ",""),currency, "គត់")
                        : String.Format("{0} {1}{2} {3}", number2WordKH(number).Replace(" ",""), currency, splitRange, dotStr);
                }
                else
                {
                    read = dotStr == string.Empty ? string.Format("{0} {1} {2}", number2Word(number).Trim(), currency, "Only")
                        : String.Format("{0} {1}{2} {3}", number2Word(number).Trim(), currency, splitRange, dotStr);
                }
                return read;

            }
            catch
            {
                return "";
            }
        }



        /// <summary>
        /// Read Each One Digit of Number
        /// </summary>
        /// <param name="digit">Number Digit</param>
        /// <param name="kh">True to get Khmer Language</param>
        /// <returns>String of Read Digit</returns>
        static private String fOneDigt(String digit, bool kh)
        {
            int digt = Convert.ToInt32(digit);
            if (digt == 0)
            {
                return string.Empty;
            }
            if (kh)
                return oneDigitKH[digt - 1];
            else
                return oneDigitENG[digt - 1];
        }

        /// <summary>
        /// Read Fret to Digit Number
        /// </summary>
        /// <param name="digit">Fret Two Digit</param>
        /// <param name="kh">True to get Khmer Language</param>
        /// <returns>String of Read Fret Two Digit</returns>
        static private String fTwoDigit(String digit, bool kh)
        {
            int digt = Convert.ToInt32(digit);
            String name = "", nameKH = "";


            if ((digt >= 10) && (digt / 10.0) < 2)
            {
                name = twoDigitENG[digt % 10];
                nameKH = twoDigitKH[digt % 10];
            }
            else if ((digt >= 20) && (digt % 10) == 0)
            {
                name = twoDigitENG[8 + (digt / 10)];
                nameKH = twoDigitKH[8 + (digt / 10)];
            }
            else if (digt > 0)
            {
                name = fTwoDigit(digit.Substring(0, 1) + "0", false) + " " + fOneDigt(digit.Substring(1), false);
                nameKH = fTwoDigit(digit.Substring(0, 1) + "0", true) + " " + fOneDigt(digit.Substring(1), true);
            }

            if (kh)
                return nameKH;
            else
                return name;

        }

        /// <summary>
        /// Read Fret Number
        /// </summary>
        /// <param name="number">Number to Read</param>
        /// <returns>String of Read Fret Number English</returns>
        static private String number2Word(String number)
        {
            string word = "";
            try
            {
                bool zeroStart = false; //ពិនិត្យមើល សូន្យនៅខាងមុខ 0XX
                bool isChange = false;   //ពិនិត្យមើល ខ្ទង់ដែលបានបំលែងទៅជា word
                double dblAmt = (Convert.ToDouble(number));
                if (dblAmt >= 0)
                {
                    zeroStart = number.StartsWith("0");

                    int numDigits = number.Length;
                    int pos = 0; //រក្សាទុកទីតាំងនៃខ្ទង់នីមួយៗ (រាយ, ដប់ , រយ ...)
                    String place = "";//digit grouping name:hundres,thousand,etc...
                    switch (numDigits)
                    {
                        case 1:   //ខ្ទង់រាយ
                            word = fOneDigt(number, false);
                            isChange = true;
                            break;
                        case 2:  //ខ្ទង់ដប់
                            word = fTwoDigit(number, false);
                            isChange = true;
                            break;
                        case 3:  //ខ្ទង់រយ
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4:  //ខ្ទង់ពាន់
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7:  //ខ្ទង់លាន
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10:    //ខ្ទង់កោដ៏
                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        default:
                            isChange = true;
                            break;
                    }
                    if (!isChange)
                    {
                        if (zeroStart)
                        {
                            //number = dblAmt.ToString();
                            if (numDigits >= 5 && numDigits % 3 == 0)
                            {
                                number = dblAmt.ToString();
                                word = number2Word(number);
                            }
                            else
                            {
                                word = number2Word(number.Substring(pos));
                            }


                        }
                        else
                        {
                            //បើសិនជាមានខ្ទង់មិនទាន់បំលែងជាពាក្យ បន្តរដោយការហៅ
                            //function នេះឡើងវិញដោយខ្លួនវា (នេះជាការអនុវត្តន៏ Recursiv)
                            word = number2Word(number.Substring(0, pos)) + place + number2Word(number.Substring(pos));
                        }


                        //if ((numDigits <= 5) && (numDigits % 3 == 0))
                        //    if (zeroStart)
                        //    {
                        //        word = " Zero " + word.Trim();
                        //    }
                    }
                    if (word.Trim().Equals(place.Trim()))
                    {
                        word = "";
                    }
                    //MessageBox.Show(isChange.ToString());
                }
            }
            catch { ;}
            return word.Trim();
        }

        /// <summary>
        /// Read Fret Number
        /// </summary>
        /// <param name="number">Number to Read</param>
        /// <returns>String of Read Fret Number English</returns>
        static private String number2WordKH(String number)
        {
            string wordKH = "";
            try
            {
                bool zeroStart = false; //ពិនិត្យមើល សូន្យនៅខាងមុខ 0XX
                bool isChange = false;   //ពិនិត្យមើល ខ្ទង់ដែលបានបំលែងទៅជា word
                double dblAmt = (Convert.ToDouble(number));

                if (dblAmt >= 0)
                {
                    zeroStart = number.StartsWith("0");

                    int numDigits = number.Length;
                    int pos = 0; //រក្សាទុកទីតាំងនៃខ្ទង់នីមួយៗ (រាយ, ដប់ , រយ ...)
                    String placeKH = "";
                    switch (numDigits)
                    {
                        case 1:   //ខ្ទង់រាយ
                            wordKH = fOneDigt(number, true);
                            isChange = true;
                            break;
                        case 2:  //ខ្ទង់ដប់
                            wordKH = fTwoDigit(number, true);
                            isChange = true;
                            break;
                        case 3:  //ខ្ទង់រយ
                            pos = (numDigits % 3) + 1;
                            placeKH = " រយ ";
                            break;
                        case 4:  //ខ្ទង់ពាន់
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            placeKH = " ពាន់ ";
                            break;
                        //case 4:  //ខ្ទង់ពាន់
                        //    pos = (numDigits % 3);
                        //    placeKH = " ពាន់ ";
                        //    break;
                        //case 5:
                        //    pos = (numDigits % 3) - 1;
                        //    placeKH = " ម៉ឺន ";
                        //    break;
                        //case 6:
                        //    pos = (numDigits % 4) - 1;
                        //    placeKH = " សែន ";
                        //    break;
                        case 7:  //ខ្ទង់លាន
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            placeKH = " លាន ";
                            break;
                        case 10:    //ខ្ទង់កោដ៏
                            pos = (numDigits % 10) + 1;
                            placeKH = " កោដ៏ ";
                            break;
                        default:
                            isChange = true;
                            break;
                    }
                    if (!isChange)
                    {
                        if (zeroStart)
                        {
                            //number = dblAmt.ToString();
                            if (numDigits >= 5 && numDigits % 3 == 0)
                            {
                                number = dblAmt.ToString();
                                wordKH = number2WordKH(number);
                            }
                            else
                            {
                                wordKH = number2WordKH(number.Substring(pos));
                            } 
                        } 
                        else
                        {
                            //បើសិនជាមានខ្ទង់មិនទាន់បំលែងជាពាក្យ បន្តរដោយការហៅ
                            //function នេះឡើងវិញដោយខ្លួនវា (នេះជាការអនុវត្តន៏ Recursiv)
                            wordKH = number2WordKH(number.Substring(0, pos)) + placeKH + number2WordKH(number.Substring(pos));
                        }
                    }
                    if (wordKH.Trim().Equals(placeKH.Trim()))
                    {
                        wordKH = "";
                    }
                }
            }
            catch { ;}
            return wordKH.Trim();
        }

        /// <summary>
        /// Read Number After Decimal
        /// </summary>
        /// <param name="tailVal">String To Read</param>
        /// <param name="kh">True to get Khmer Language</param>
        /// <returns>String of Read Number</returns>
        static private String readDecimal(String tailVal, bool kh)
        {
            String tailToWord = "", digit = "", inWord = "";
            for (int i = 0; i < tailVal.Length; i++)
            {
                digit = tailVal[i].ToString();
                if (digit.Equals("0"))
                {
                    if (kh)
                        inWord = "សូន្យ";
                    else
                        inWord = "Zero";
                }
                else
                {
                    if (kh)

                        inWord = number2WordKH(tailVal.Substring(i));
                    else
                        inWord = number2Word(tailVal.Substring(i));
                    i = tailVal.Length;
                }
                tailToWord += " " + inWord;
            }
            return tailToWord;
        }
    }   
}