using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;

public class ExcelEngine
{
    /// <summary>
    /// To combine multiple workbooks into a file
    /// </summary>
    /// <remarks>
    /// The following file name convention will be used while combining the child files
    /// exportFileKey_[Description] where the description will be the tab name
    ///
    /// The ordering of the worksheets will be using the file creation time
    ///
    /// The above convention can be enhanced when necessary but need to make sure the backward compatibility for the existing codes
    ///
    /// Note:
    /// - the index starts from 1 in the excel automation array
    /// - be careful when making changes, especially moving things around in the method, e.g. prompts might come up unexpectedly
    /// - to avoid "zombie" excel instances in the task manager when referencing the COM object, please refer to the http://support.microsoft.com/default.aspx/kb/317109
    ///
    /// </remarks>
    /// <param name="exportFilePath">the destination file name choosen by the user</param>
    /// <param name="exportFileKey">the unique key file name choosen by the user, this is to avoid merging files with similar names</param>
    /// <param name="rawFilesDirectory">the folder where the files are being generated, this can be temp folder or any folder basically</param>
    /// <param name="deleteRawFiles">delete the raw files after completed?</param>
    ///
    /// <returns></returns>
    public static bool CombineWorkBooks(string exportFilePath, string exportFileKey, string rawFilesDirectory, bool deleteRawFiles)
    {
        Application xlApp = null;
        Workbooks newBooks = null;
        Workbook newBook = null;
        Sheets newBookWorksheets = null;
        Worksheet defaultWorksheet = null;
        IEnumerable<string> filesToMerge = null;
        bool areRowsTruncated = false;

        try
        {
            //Console.WriteLine("Method: CombineWorkBooks - Starting excel");
            xlApp = new Application();

            if (xlApp == null)
            {
                //Console.WriteLine("EXCEL could not be started. Check that your office installation and project references are correct.");
                return false;
            }

            //Console.WriteLine("Method: CombineWorkBooks - Disabling the display alerts to prevent any prompts during workbooks close");
            // not an elegant solution? however has to do this else will prompt for save on exit, even set the Saved property didn't help
            xlApp.DisplayAlerts = false;

            //Console.WriteLine("Method: CombineWorkBooks - Set Visible to false as a background process, else it will be displayed in the task bar");
            xlApp.Visible = false;

            //Console.WriteLine("Method: CombineWorkBooks - Create a new workbook, comes with an empty default worksheet");
            newBooks = xlApp.Workbooks;
            newBook = newBooks.Add(XlWBATemplate.xlWBATWorksheet);
            newBookWorksheets = newBook.Worksheets;

            // get the reference for the empty default worksheet
            if (newBookWorksheets.Count > 0)
            {
                defaultWorksheet = newBookWorksheets[1] as Worksheet;
            }

            //Console.WriteLine("Method: CombineWorkBooks - Get the files sorted by creation date");
            var dirInfo = new DirectoryInfo(rawFilesDirectory);
            filesToMerge = from f in dirInfo.GetFiles(exportFileKey + "*", SearchOption.TopDirectoryOnly)
                           orderby f.CreationTimeUtc
                           select f.FullName;

            foreach (var filePath in filesToMerge)
            {
                Workbook childBook = null;
                Sheets childSheets = null;
                try
                {
                    //Console.WriteLine("Method: CombineWorkBooks - Processing {0}", filePath);
                    childBook = newBooks.Open(filePath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing
                                                         , Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                    childSheets = childBook.Worksheets;
                    if (childSheets != null)
                    {
                        for (int iChildSheet = 1; iChildSheet <= childSheets.Count; iChildSheet++)
                        {
                            Worksheet sheetToCopy = null;
                            try
                            {
                                sheetToCopy = childSheets[iChildSheet] as Worksheet;
                                if (sheetToCopy != null)
                                {
                                    //Console.WriteLine("Method: CombineWorkBooks - Assigning the worksheet name");
                                    sheetToCopy.Name = Truncate(GetReportDescription(Path.GetFileNameWithoutExtension(filePath), sheetToCopy.Name), 31); // only 31 char max

                                    //Console.WriteLine("Method: CombineWorkBooks - Copy the worksheet before the default sheet");
                                    sheetToCopy.Copy(defaultWorksheet, Type.Missing);
                                }
                            }
                            finally
                            {
                                DisposeCOMObject(sheetToCopy);
                            }
                        }

                        //Console.WriteLine("Method: CombineWorkBooks - Close the childbook");
                        // for some reason, calling close below may cause an exception -> System.Runtime.InteropServices.COMException (0x80010108): The object invoked has disconnected from its clients.
                        childBook.Close(false, Type.Missing, Type.Missing);
                    }
                }
                finally
                {
                    DisposeCOMObject(childSheets);
                    DisposeCOMObject(childBook);
                }
            }

            //Console.WriteLine("Method: CombineWorkBooks - Delete the empty default worksheet");
            if (defaultWorksheet != null) defaultWorksheet.Delete();

            //Console.WriteLine("Method: CombineWorkBooks - Save the new book into the export file path: {0}", exportFilePath);
            newBook.SaveAs(exportFilePath, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing
                , XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

            newBooks.Close();
            xlApp.DisplayAlerts = true;

            return true;
        }
        catch (Exception)
        {
            //Console.WriteLine("Method: CombineWorkBooks - Exception: {0}", ex.ToString());
            return false;
        }
        finally
        {
            DisposeCOMObject(defaultWorksheet);
            DisposeCOMObject(newBookWorksheets);
            DisposeCOMObject(newBooks);
            DisposeCOMObject(newBook);

            //Console.WriteLine("Method: CombineWorkBooks - Closing the excel app");
            if (xlApp != null)
            {
                xlApp.Quit();
                DisposeCOMObject(xlApp);
            }

            if (deleteRawFiles)
            {
                //Console.WriteLine("Method: CombineWorkBooks - Deleting the temporary files");
                DeleteTemporaryFiles(filesToMerge);
            }
        }
    }

    private static void DisposeCOMObject(object o)
    {
        //Console.WriteLine("Method: DisposeCOMObject - Disposing");
        if (o == null)
        {
            return;
        }
        try
        {
            Marshal.ReleaseComObject(o);
        }
        catch (Exception ex)
        {
            throw new Exception("Method: DisposeCOMObject - Exception: "+ ex.ToString());
            //Console.WriteLine("Method: DisposeCOMObject - Exception: {0}", ex.ToString());
        }
    }

    private static void DeleteTemporaryFiles(IEnumerable<string> tempFilenames)
    {
        foreach (var tempFile in tempFilenames)
        {
            try
            {
                File.Delete(tempFile);
            }
            catch
                (Exception)
            {
                throw new Exception("Could not delete temporary file " + tempFilenames);
                //Console.WriteLine("Could not delete temporary file '{0}'", tempFilenames);
            }
        }
    }

    /// <summary>
    /// the first array item will be the key
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="defaultName"></param>
    /// <returns></returns>
    protected static string GetReportDescription(string fileName, string defaultName)
    {
        var splits = fileName.Split('_');
        return splits.Length > 1 ? string.Join("-", splits, 1, splits.Length - 1) : defaultName;
    }

    /// <summary>
    /// Get a substring of the first N characters.
    /// http://dotnetperls.com/truncate-string
    /// </summary>
    public static string Truncate(string source, int length)
    {
        if (source.Length > length)
        {
            source = source.Substring(0, length);
        }
        return source;
    }
}