﻿using B24.Model;
using B24.Resource;
using EPower.Base.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EPower.Logic
{
    public enum B24_REF_TYPE
    {
        ACCOUNT = 1,
        INVOICE = 2
    }

    class B24_Integration
    {
        TBL_B24_LOG objB24;
        List<TBL_B24_LOG_DETAIL> objB24Details = new List<TBL_B24_LOG_DETAIL>();
        List<Customer> dataCustomer = new List<Customer>();
        List<SaleOrderBill> dataBill = new List<SaleOrderBill>();
        private string KHR = "KHR";
        public string USD = "USD";

        public B24_Integration()
        {
            BaseResource.ApiUrl = Method.Utilities[Utility.B24_SUPPLIER_API_URL];
            BaseResource.ApiToken = Method.Utilities[Utility.B24_SUPPLIER_API_KEY];
        }

        public void Push(bool isResendAll)
        {
            pushCustomer(isResendAll);
            pushInvoice(isResendAll);
            logHistory();
        }

        private Customer customer(TBL_CUSTOMER c, bool isActive)
        {
            return new Customer()
            {
                org_code = c.CUSTOMER_CODE.RemoveZeroSpace(),
                name = (c.LAST_NAME_KH + " " + c.FIRST_NAME_KH).RemoveZeroSpace(),
                phone = c.PHONE_1.RemoveZeroSpace(),
                email = c.PHONE_2.RemoveZeroSpace(),
                sync_code = c.CUSTOMER_ID.ToString(),
                is_active = isActive
            };
        }

        private SaleOrderBill bill(TBL_INVOICE objInvoice)
        {
            TBL_CUSTOMER objC = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == objInvoice.CUSTOMER_ID);
            var inv = new SaleOrderBill()
            {
                title = objInvoice.INVOICE_TITLE + (objC.INVOICE_CUSTOMER_ID > 0 && objC.CUSTOMER_ID != objC.INVOICE_CUSTOMER_ID ? " (អតិថិជនរួម)" : ""),
                description = "",
                total_amount = objInvoice.SETTLE_AMOUNT,
                bill_date = objInvoice.INVOICE_DATE,
                due_date = objInvoice.DUE_DATE,
                currency_id = objInvoice.CURRENCY_ID == 1 ? KHR : USD,
                org_code = objInvoice.INVOICE_NO,
                sync_code = objInvoice.INVOICE_ID.ToString(),
                paid_amount = objInvoice.PAID_AMOUNT,
                customer_sync_code = objC.INVOICE_CUSTOMER_ID == 0 ? objC.CUSTOMER_ID.ToString() : objC.INVOICE_CUSTOMER_ID.ToString(),
                is_active = objInvoice.INVOICE_STATUS == (int)InvoiceStatus.Open ? true : false,
                ext = new SaleOrderBill.Ext()
                {
                    details = new List<SaleOrderBill.Detail>()
                    {

                    }
                }
            };
            var objDetails = DBDataContext.Db.TBL_INVOICE_DETAILs.Where(x => x.INVOICE_ID == objInvoice.INVOICE_ID && x.INVOICE_ITEM_ID != -1);
            foreach (var objDetail in objDetails)
            {
                inv.ext.details.Add(new SaleOrderBill.Detail()
                {
                    item_name = DBDataContext.Db.TBL_INVOICE_ITEMs.FirstOrDefault(x => x.INVOICE_ITEM_ID == objDetail.INVOICE_ITEM_ID).INVOICE_ITEM_NAME + " " + objInvoice.INVOICE_MONTH.ToString("MM-yyyy"),
                    quantity = 1,
                    price = objInvoice.SETTLE_AMOUNT,
                    amount = objInvoice.SETTLE_AMOUNT
                });
            }
            return inv;
        }

        private void insertLog()
        {
            objB24 = new TBL_B24_LOG();
            objB24.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
            objB24.CREATE_ON = DBDataContext.Db.GetSystemDate();
            objB24.IS_ACTIVE = true;
            DBDataContext.Db.TBL_B24_LOGs.InsertOnSubmit(objB24);
            DBDataContext.Db.SubmitChanges();
        }

        private void insertLogDetail(int refId, int refTypeId)
        {
            objB24Details.Add(new TBL_B24_LOG_DETAIL()
            {
                B24_LOG_ID = 0,
                IS_ACTIVE = true,
                REF_ID = refId,
                REF_TYPE_ID = refTypeId
            });
        }

        public void pushCustomer(bool isResendAll)
        {
            try
            {
                objB24 = DBDataContext.Db.TBL_B24_LOGs.OrderByDescending(x => x.B24_LOG_ID).FirstOrDefault();

                // Send First Time or Check Resend All
                if (objB24 == null || isResendAll)
                {
                    var cus = from c in DBDataContext.Db.TBL_CUSTOMERs
                              join b24 in DBDataContext.Db.TBL_B24_CUSTOMERs on c.CUSTOMER_ID equals b24.CUSTOMER_ID
                              where b24.IS_ACTIVE
                              select c;
                    foreach (var c in cus)
                    {
                        dataCustomer.Add(customer(c, true));
                        // Insert Track Log Detail
                        insertLogDetail(c.CUSTOMER_ID, (int)B24_REF_TYPE.ACCOUNT);
                    }
                }
                else
                {
                    var cus = from c in DBDataContext.Db.TBL_CUSTOMERs
                              join b24 in DBDataContext.Db.TBL_B24_CUSTOMERs on c.CUSTOMER_ID equals b24.CUSTOMER_ID
                              where b24.ROW_DATE > objB24.CREATE_ON
                              select b24;
                    foreach (var c in cus)
                    {
                        TBL_CUSTOMER objCus = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == c.CUSTOMER_ID);
                        dataCustomer.Add(customer(objCus, c.IS_ACTIVE));
                        // Insert Track Log Detail
                        insertLogDetail(c.CUSTOMER_ID, (int)B24_REF_TYPE.ACCOUNT);
                    }
                }

                if (dataCustomer.Count() > 0)
                {
                    CustomerResource.Instance.Push(dataCustomer);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void pushInvoice(bool isResendAll)
        {
            try
            {
                // Send First Time or Check Resend All
                if (objB24 == null || isResendAll)
                {
                    var invs = from c in DBDataContext.Db.TBL_CUSTOMERs
                               join b24 in DBDataContext.Db.TBL_B24_CUSTOMERs on c.CUSTOMER_ID equals b24.CUSTOMER_ID
                               join inv in DBDataContext.Db.TBL_INVOICEs on c.CUSTOMER_ID equals inv.CUSTOMER_ID
                               where (isResendAll || inv.SETTLE_AMOUNT > inv.PAID_AMOUNT) && b24.IS_ACTIVE
                               select inv;

                    foreach (var inv in invs)
                    {
                        dataBill.Add(bill(inv));
                        // Insert Track Log Detail
                        insertLogDetail(DataHelper.ParseToInt(inv.INVOICE_ID.ToString()), (int)B24_REF_TYPE.INVOICE);
                    }
                }
                else
                {
                    var invs = from b24 in DBDataContext.Db.TBL_B24_CUSTOMERs
                               join inv in DBDataContext.Db.TBL_INVOICEs on b24.CUSTOMER_ID equals inv.CUSTOMER_ID
                               where inv.ROW_DATE > objB24.CREATE_ON && b24.IS_ACTIVE
                               select inv;

                    foreach (var inv in invs)
                    {
                        dataBill.Add(bill(inv));
                        // Insert Track Log Detail
                        insertLogDetail(DataHelper.ParseToInt(inv.INVOICE_ID.ToString()), (int)B24_REF_TYPE.INVOICE);
                    }
                }
                if (dataBill.Count() > 0)
                {
                    SaleOrderBillResource.Instance.Push(dataBill);
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        [RequiredTransaction]
        public void logHistory()
        {
            try
            {
                DBDataContext.Db.RequiredTransaction();                 
                insertLog();
                foreach (var item in objB24Details)
                {
                    DBDataContext.Db.TBL_B24_LOG_DETAILs.InsertOnSubmit(new TBL_B24_LOG_DETAIL()
                    {
                        B24_LOG_ID = objB24.B24_LOG_ID,
                        IS_ACTIVE = item.IS_ACTIVE,
                        REF_ID = item.REF_ID,
                        REF_TYPE_ID = item.REF_TYPE_ID
                    });
                    DBDataContext.Db.SubmitChanges();
                }
                objB24.TOTAL_CUSTOMER = objB24Details.Count(x => x.REF_TYPE_ID == (int)B24_REF_TYPE.ACCOUNT);
                objB24.TOTAL_INVOICE = objB24Details.Count(x => x.REF_TYPE_ID == (int)B24_REF_TYPE.INVOICE);
                DBDataContext.Db.SubmitChanges(); 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateCustomerRowDate(int customerId, bool isActive)
        {
            TBL_B24_CUSTOMER objB24Customer = DBDataContext.Db.TBL_B24_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == customerId && x.IS_ACTIVE);
            if (objB24Customer != null)
            {
                objB24Customer.ROW_DATE = DBDataContext.Db.GetSystemDate();
                objB24Customer.IS_ACTIVE = isActive;
                DBDataContext.Db.SubmitChanges();
            }
        }

    }
}
