﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EPower.Logic
{ 
    
    public class RequiredTransactionAttribute : Attribute
    {
        public RequiredTransactionAttribute()
        {
            int k = 10;
            


            if (System.Transactions.Transaction.Current == null)
            {
                throw new Exception("Transaction scope is needed.\nMethod must run inside TransactionScope.");
            }
        } 
    }
}
