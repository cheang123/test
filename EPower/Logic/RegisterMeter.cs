﻿using EPower.Base.Logic;
using SoftTech;
using System.Linq;

namespace EPower.Logic
{
    public class RegisterMeter
    {
        public static void CheckUnregisterMeter(DBDataContext db)
        {
            var q = db.TBL_METERs.Select(x => new //TMP_METER_CHECKSUM()
            {
                METER_CODE = x.METER_CODE,
                CHECKSUM = Method.GetMETER_REGCODE(x.METER_CODE, x.IS_DIGITAL)
            });
            var dt = q._ToDataTable();
            db.CommandTimeout = 60 * 10; // 60 multiple with mn
            db.ExecuteCommand(@"
IF OBJECT_ID('TMP_METER_CHECKSUM') IS NULL
    CREATE TABLE TMP_METER_CHECKSUM(
        METER_CODE NVARCHAR(50) PRIMARY KEY,
        CHECKSUM NVARCHAR(100) NOT NULL,
    );");
            db.ExecuteCommand("TRUNCATE TABLE TMP_METER_CHECKSUM;");

            db.BulkCopy(dt, "TMP_METER_CHECKSUM");
            //db.BulkCopy(q);

            db.ExecuteCommand(@"
UPDATE TBL_METER 
SET REGISTER_CODE=''
FROM TBL_METER m
INNER JOIN TMP_METER_CHECKSUM c ON m.METER_CODE=c.METER_CODE
WHERE m.REGISTER_CODE!=c.CHECKSUM;");

            db.ExecuteCommand("TRUNCATE TABLE TMP_METER_CHECKSUM;");
            db.SubmitChanges();
        }
        public static void CheckUnregisterMeter()
        {
            CheckUnregisterMeter(new DBDataContext(Method.GetConnectionString(Properties.Settings.Default.CONNECTION)));
        }
    }
}
