﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlServerCe;
using System.IO;
using System.Windows.Forms;
using EPower.Properties;
using Microsoft.Win32;
using OpenNETCF.Desktop.Communication;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using Process = System.Diagnostics.Process;

namespace EPower
{
    class Devices
    {
        private static Dictionary<DeviceInfo, string> myDevice=new Dictionary<DeviceInfo,string>();

        public static Dictionary<DeviceInfo, string> MyDevice
        {
            get { return myDevice; }
            set { myDevice = value; }
        }

        public static bool ?Connect()
        {
            bool conn = true;
            myDevice.Clear();
            RegistryKey regkey = Registry.CurrentUser.OpenSubKey(Settings.Default.DeviceRegidit);
            if (regkey==null)
            {
                decimal osVersion = DataHelper.ParseToDecimal(Environment.OSVersion.Version.Major.ToString());

                if (osVersion>5)
                {
                    MsgBox.ShowInformation("សូមបញ្ចូលកម្មវិធីMicrosoft Windows Mobile Device Center 6.1, ដើម្បីធ្វើការភ្ជាប់ទៅកាន់ឩបករណ័!។");
                    if (Environment.OSVersion.Platform== PlatformID.Win32NT)
                    {
                        Process.Start(Settings.Default.ActiveSync61X86);
                    }
                    else
                    {
                        Process.Start(Settings.Default.ActiveSync61X64);
                    }
                }
                else
                {                    
                    MsgBox.ShowInformation("សូមបញ្ចូលកម្មវិធីActive Sync 4.5, ដើម្បីធ្វើការភ្ជាប់ទៅកាន់ឩបករណ័!។");
                    Process.Start(Settings.Default.ActiveSync45);
                }

                
                return null;
            }

            foreach (string item in Enum.GetNames(typeof(DeviceInfo)))
            {
                string val = "";
                if (regkey.GetValue(item) != null)
                {
                    val = regkey.GetValue(item).ToString();
                    myDevice.Add((DeviceInfo)Enum.Parse(typeof(DeviceInfo),item), val);
                }
                else
                {
                    conn = false;
                    break;
                }
            }
            regkey.Close();
            return conn;
        } 
    
        //Database transaction.
        public SqlCeConnection Connection = null; 
        public void InitializConnection()
        { 
            if (Connection.State== ConnectionState.Closed)
            {
                Connection.Open();
            } 
        }

        public Devices(string DeviceCode)
        {
            string ConnectionString = string.Concat("Data Source=", FileDatabase(DeviceCode), ";Password=1");
            Connection = new SqlCeConnection(ConnectionString);
            InitializConnection();
        }

        public Devices()
        {
            string ConnectionString = string.Concat("Data Source=",FileMobileDatabase(), ";Password=1");
            Connection = new SqlCeConnection(ConnectionString);
            InitializConnection();
        }   

        public static string FileDatabase(string DiviceCode)
        {
            if (!Directory.Exists(PathDatabase))
            {
                Directory.CreateDirectory(PathDatabase);
            }
            string FileName = DiviceCode + "-EMobile.sdf";
            string FullName = string.Concat(PathDatabase, "\\", FileName);
            return FullName;
        }

        public static string FileMobileDatabase()
        {
            if (!Directory.Exists(PathDatabase))
            {
                Directory.CreateDirectory(PathDatabase);
            }
            return string.Concat(PathDatabase,"\\","EMobile.sdf");
        }

        public static string PathDatabase = string.Concat(Application.StartupPath, "\\", "MobileDB");
         
        public DataTable ExecuteDataTable(string SelectStatement)
        { 
            SqlCeCommand cmd = new SqlCeCommand(SelectStatement, Connection);
            DataTable dt = new DataTable(); 
            if (Connection.State == ConnectionState.Closed)
            {
                Connection.Open();
            }
            dt.Load(cmd.ExecuteReader()); 
            return dt;
        }
        public DataTable ExecuteDataTable(string SelectStatement,string tableName)
        {
            SqlCeCommand cmd = new SqlCeCommand(SelectStatement, Connection);
            DataTable dt = new DataTable(tableName);
            if (Connection.State == ConnectionState.Closed)
            {
                Connection.Open();
            }
            dt.Load(cmd.ExecuteReader());
            return dt;
        }

        public int ExecuteCommand(string Sql)
        {
            SqlCeCommand cmd = new SqlCeCommand(Sql, Connection);
            return cmd.ExecuteNonQuery();   
        }
        public int ExecuteCommand(string Sql, params SqlCeParameter[] parameter)
        { 
            SqlCeCommand cmd = new SqlCeCommand(Sql, Connection);
            cmd.Parameters.AddRange(parameter);                
            return cmd.ExecuteNonQuery(); 
        }

        public void CloseConnection()
        {
            if (Connection.State == ConnectionState.Open)
            {
                Connection.Close();
            } 
        }


        public static void CopyDbToDevice()
        {
            RAPI rp = new RAPI();
            try
            {
                rp.Connect();
                string path = FileMobileDatabase();
                rp.CopyFileToDevice(path, Settings.Default.PDB, true);
            }
            catch (RAPIException ex)
            {
                if (ex.Win32Error == 3)
                {
                    throw new Exception(Resources.MS_DEVICE_DONT_HAVE_APPLICATION);
                }
                else if (ex.Win32Error == 32)
                {
                    throw new Exception(Resources.MS_CLOSE_APPLICATION_IN_DEVICE);
                }
                else
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rp.Disconnect();
            }
        }

        public static void CopyDbFromDevice()
        {
            RAPI rp = new RAPI();
            try
            {
                rp.Connect();
                string path = FileMobileDatabase();
                string pathBackup = Path.Combine(Settings.Default.PATH_BACKUP, "EPower MobileDb " + DBDataContext.Db.GetSystemDate().ToString("yyyyMMddHHmmss") + ".sdf");
                rp.CopyFileFromDevice(path, Settings.Default.PDB, true);
                File.Copy(path, pathBackup, true);
            }
            catch (RAPIException ex)
            {
                if (ex.Win32Error == 3)
                {
                    throw new Exception(Resources.MS_DEVICE_DONT_HAVE_APPLICATION);
                }
                else if (ex.Win32Error == 32)
                {
                    throw new Exception(Resources.MS_CLOSE_APPLICATION_IN_DEVICE);
                }
                else
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            finally
            {
                rp.Disconnect();
            }
        } 
    }
}
