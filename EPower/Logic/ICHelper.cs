﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using EPower.Properties;

namespace EPower.Logic
{
    internal class ICHelper
    {
        [DllImport("Sgdj_RDK.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern Int32 RDKIcard(Int32 icdev, string sendstr, StringBuilder recvstr, string callpwd);

        #region Default Data

        #endregion Default Data

        //sum up the value of each byte and 
        //divide the sum by the maximum value of the byte .the result is the 校验和
        //工号: operator No.最大功率因素：maximum power factor 
        //突变功率:mutant power 最大屯积量:maximum accumulating consumption and 
        //最大负荷功率 maximum loading power
        //tuyhong1171: so how it work differently to meter
        //tuyhong1171: and what is 工号 meaning to meter

        //数据帧  6 8 0   D   0   5   0   0   0   0   0   0   9   6   0   0    C8 16
        //ascii码    (30H+44H+30H+35H+30H+30H+30H+30H+30H+30H+39H+36H+30H+30H) mod 256 = C8
        //头两位68不加入校验和计算
        #region Data
        private static int _intCOMPORT = 1;

        public static int ComPortNumber
        {
            get { return _intCOMPORT; }
            //Close port
            set 
            {
                var port = value > 0 ? value - 1 : value; // port name -1
                _intCOMPORT = port; 
            }
        }
        private static int _intOpenKey = 0;         
        #endregion Data

        #region Constructor

        internal ICHelper(int intComport)
        {
            _intCOMPORT = intComport;
        }

        #endregion Constructor

        #region Private Method

        /// <summary>
        /// Sent all command to com device
        /// </summary>
        /// <param name="strSentCommand">Command sent to device</param>
        /// <param name="strReturnString">string returned from device</param>
        /// <returns>Return 0 Mean Success</returns>
        internal static int SentCommand(string strSentCommand, out string strReturnString)
        {
            if (_intOpenKey == 0)
            {
                Open();
            }
            int intReturn = 0;
            strReturnString = "";
            StringBuilder mw = new StringBuilder(1024);
            intReturn = RDKIcard(_intOpenKey, strSentCommand, mw, "SM05102103");
            strReturnString = mw.ToString();
            return intReturn;
        }

        /// <summary>
        /// Get Frame to sent 
        /// </summary>
        /// <param name="strCommand">Command String</param>
        /// <param name="strData">Data String</param>
        /// <returns></returns>
        internal static string GetSendFrame(string strCommand, string strData)
        {
            string strReturn = "";
            //get data length then convert to hexa decimal
            string strLengthHex = (strData.Length / 2).ToString("X");
            if (strLengthHex.Length == 1)
            {
                strLengthHex = "0" + strLengthHex;
            }
            else if (strLengthHex.Length > 2)
            {
                MessageBox.Show("Error data length");
            }
            string strCheckSum = GetCheckSum(strCommand, strLengthHex, strData);
            strReturn = "68" + strCommand + strLengthHex + strData + strCheckSum + "16";
            return strReturn;
        }

        /// <summary>
        /// Get Check Sum String
        /// </summary>
        /// <param name="strCommand">Command String</param>
        /// <param name="strLen">Data Length String</param>
        /// <param name="strData">Data String</param>
        /// <returns></returns>
        private static string GetCheckSum(string strCommand, string strLen, string strData)
        {
            string strReturn = "";
            string strFrame = strCommand + strLen + strData;
            char[] chs = strFrame.ToCharArray();
            string strMakeCheckSumFrame = "";

            for (int i = 0; i < chs.Length; i++)
            {
                int intResult = 0;
                //if the string is not integer
                //meaning that, it is A to F
                if (!int.TryParse(chs[i].ToString(), out intResult))
                {
                    switch (chs[i])
                    {
                        case 'A': strMakeCheckSumFrame += "11 "; break;
                        case 'B': strMakeCheckSumFrame += "12 "; break;
                        case 'C': strMakeCheckSumFrame += "13 "; break;
                        case 'D': strMakeCheckSumFrame += "14 "; break;
                        case 'E': strMakeCheckSumFrame += "15 "; break;
                        case 'F': strMakeCheckSumFrame += "16 "; break;
                    }
                }
                //if the string is integer
                //meaning that, it is 0 to 9
                else
                {
                    strMakeCheckSumFrame += chs[i].ToString() + " ";
                }
            }
            //trim the last space
            strMakeCheckSumFrame = strMakeCheckSumFrame.Trim();

            //convert and sum each byte to hexa
            string[] strs = strMakeCheckSumFrame.Split(' ');
            int intHex = 0;
            foreach (string str in strs)
            {
                intHex += 0x30 + Convert.ToInt32(str, 16);
            }

            //modolus by 256 = 100 in hexa
            intHex = intHex % 0x100;

            //convet hexa to string
            strReturn = String.Format("{0:X}", intHex);

            return strReturn;
        }

        #endregion Private Method

        #region Internal Method
        /// <summary>
        /// Open com port
        /// </summary>
        /// <returns></returns>
        internal static int Open()
        { 
            int intReturn = 0;            
            Byte[] str = new Byte[1024];
            StringBuilder mw = new StringBuilder(1024);
            string strSend, strReceive;


            ComPortNumber = Settings.Default.PREPAID_COM_PORT; 
            //is open
            if (_intOpenKey != 0)
            {
                
                strSend = GetSendFrame("0D", string.Concat("01", FormatComPort()));//1-Close 0-Open -2Beep                           
                intReturn = RDKIcard(1, strSend, mw, "SM05102103");
                if (intReturn!=0)
                {
                    return intReturn;
                }
            }
            strSend = GetSendFrame("0D", string.Concat( "00",FormatComPort(),"009600"));//1-Close 0-Open -2Beep           
            intReturn = RDKIcard(1, strSend, mw, "SM05102103");
            strReceive = mw.ToString();
            if (intReturn == 0)
            {
                _intOpenKey = int.Parse(strReceive.Substring(8, 6));
            }
            return intReturn;
        }

        internal static string FormatComPort()
        {
            return (_intCOMPORT).ToString("00");
        }

        /// <summary>
        /// Reset COM Port Number and Test this port.
        /// </summary>
        /// <param name="intCOMPORT">Port Number of PC</param>
        /// <returns>0 is open sucessful.</returns>
        internal static int TestPort( )
        {
            int intReturn;
            if (_intOpenKey==0)
            {
                intReturn = Open();
            }
            var r = "";
            intReturn =BeepSound(out r);
             
            return intReturn;      
        }

        internal static int BeepSound(out string strReturnString)
        {
            int intReturn = 1;
            strReturnString = "";
            if (_intOpenKey == 0)
            {
                MessageBox.Show("Please open port first!");
                return 1;
            }
            string strSend = GetSendFrame("0D", "0250000000"); //"680D050250000000C016";
            intReturn = SentCommand(strSend, out strReturnString);
            return intReturn;

        }

        internal static int ReadCard(out string strReturnString)
        {
            strReturnString = "";
            if (_intOpenKey == 0)
            {
                //MessageBox.Show("Please open port first!");
                //return 1;
                Open();
            }
            int intReturn = 1;
            string strSend = GetSendFrame("0B", ""); //"680B00D216";
            intReturn = SentCommand(strSend, out strReturnString);
            return intReturn;
        }

        internal static int ReadFactory(out string strReturnString)
        {
            strReturnString = "";
            if (_intOpenKey == 0)
            {
                //MessageBox.Show("Please open port first!");
                //return 1;
                Open();
            }
            int intReturn = 1;
            string strSend = GetSendFrame("1C", ""); //"681C00D216";
            intReturn = SentCommand(strSend, out strReturnString);
            return intReturn;
        }

        #endregion Internal Method
    }
}
 