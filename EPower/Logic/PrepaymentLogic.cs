﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SoftTech.Component;

namespace EPower.Logic
{
    class PrepaymentLogic
    {
        public static PrepaymentLogic Instance { get; } = new PrepaymentLogic();
        public TBL_CUS_PREPAYMENT getLastestPrepayCus(int CusId, int CurrencyId)
        {
            return DBDataContext.Db.TBL_CUS_PREPAYMENTs.OrderByDescending(x => x.CUS_PREPAYMENT_ID).FirstOrDefault(x => x.CUSTOMER_ID == CusId && x.CURRENCY_ID == CurrencyId);
        }

        public List<TBL_CUS_PREPAYMENT> getPrepayCustomer()
        {
            var ids = (from p in DBDataContext.Db.TBL_CUS_PREPAYMENTs
                       group p by new { p.CUSTOMER_ID, p.CURRENCY_ID } into g
                       select new
                       {
                           id = g.Max(x => x.CUS_PREPAYMENT_ID)
                       }).ToList();
            if (ids.Count <= 0)
            {
                return null;
            }
            var result = from i in ids
                         join p in DBDataContext.Db.TBL_CUS_PREPAYMENTs on i.id equals p.CUS_PREPAYMENT_ID
                         select p;

            return result.ToList();
        }

        public decimal GetCustomerPrepayment(int intCusId, int nCurrency)
        {
            // TODO: Change Modifier
            decimal decReturn = 0;
            TBL_CUS_PREPAYMENT objPrepayment = DBDataContext.Db.TBL_CUS_PREPAYMENTs.Where(cd => cd.CUSTOMER_ID == intCusId && cd.CURRENCY_ID == nCurrency).OrderByDescending(cd => cd.CUS_PREPAYMENT_ID).FirstOrDefault();
            if (objPrepayment != null)
            {
                decReturn = objPrepayment.BALANCE;
            }
            return decReturn;
        }

        public List<TBL_INVOICE> getInvoiceCanClearWithPrepay(int cycleId)
        {
            List<int> InvStatus = new List<int>()
            {
                (int)InvoiceStatus.Open,
                (int)InvoiceStatus.Pay
            };

            var prepayCus = getPrepayCustomer();

            if (prepayCus == null)
            {
                return null;
            }

            var invoice = (from c in DBDataContext.Db.TBL_CUSTOMERs
                           join i in DBDataContext.Db.TBL_INVOICEs on c.CUSTOMER_ID equals i.CUSTOMER_ID
                           where c.BILLING_CYCLE_ID == cycleId && (i.INVOICE_STATUS == (int)InvoiceStatus.Open || i.INVOICE_STATUS == (int)InvoiceStatus.Pay)
                           select i).ToList();

            var result = from c in prepayCus
                         join inv in invoice on new { c.CUSTOMER_ID, c.CURRENCY_ID } equals new { inv.CUSTOMER_ID, inv.CURRENCY_ID }
                         where c.BALANCE > 0 && InvStatus.Contains(inv.INVOICE_STATUS)
                         orderby inv.INVOICE_ID ascending
                         select inv;

            return result.ToList();
        }

        public TBL_CUS_PREPAYMENT AddCustomerPrepayment(DateTime payDate, int intCusId, decimal decAmount, decimal decBalance,
int intCurrencyId, string strPrepaymentNumber, string strNote, PrepaymentAction ppAction, int PaymentId = 0, int BandPaymentId = 0, bool fromReverseBill = false)
        {
            var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(payDate, intCurrencyId);
            TBL_CUS_PREPAYMENT objPrepaid = new TBL_CUS_PREPAYMENT();
            objPrepaid.AMOUNT = decAmount;
            if (objPrepaid.AMOUNT <= 0)
            {
                //if settle prepay to invoice outstanding =0;
                objPrepaid.OUT_STANDAING_BALANCE = 0;
            }
            else
            {
                objPrepaid.OUT_STANDAING_BALANCE = decAmount;
            }
            objPrepaid.BALANCE = decBalance;
            objPrepaid.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
            objPrepaid.CREATE_ON = payDate;
            objPrepaid.NOTE = strNote;
            objPrepaid.CURRENCY_ID = intCurrencyId;
            objPrepaid.CUSTOMER_ID = intCusId;
            objPrepaid.PREPAYMENT_NO = strPrepaymentNumber;
            objPrepaid.ACTION_ID = (int)ppAction;
            objPrepaid.PAYMENT_ID = PaymentId;
            objPrepaid.BANK_PAYMENT_DETAIL_ID = BandPaymentId;
            objPrepaid.USER_CASH_DRAWER_ID = 0;
            objPrepaid.REVERSE_FROM_PREPAYMENT_ID = 0;
            objPrepaid.EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE;
            objPrepaid.EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON;

            if (ppAction != PrepaymentAction.ApplyPayment)
            {
                if (!fromReverseBill)
                {
                    objPrepaid.USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID;
                }
            }
            return objPrepaid;
        }

        public TBL_CUS_PREPAYMENT RemoveCustomerPrepayment(DateTime payDate, int intCusId, decimal decAmount, decimal decBalance, int intCurrencyId, string strPrepaymentNumber, string strNote, PrepaymentAction ppAction, int reversePrepaymentId, int cashDrawer_Id, int PaymentId = 0, int BandPaymentId = 0)
        {
            var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(payDate, intCurrencyId);
            TBL_CUS_PREPAYMENT objPrepaid = new TBL_CUS_PREPAYMENT();
            objPrepaid.AMOUNT = decAmount;

            objPrepaid.OUT_STANDAING_BALANCE = 0;
            objPrepaid.BALANCE = decBalance;
            objPrepaid.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
            objPrepaid.CREATE_ON = payDate;
            objPrepaid.NOTE = strNote;
            objPrepaid.CURRENCY_ID = intCurrencyId;
            objPrepaid.CUSTOMER_ID = intCusId;
            objPrepaid.PREPAYMENT_NO = strPrepaymentNumber;
            objPrepaid.ACTION_ID = (int)ppAction;
            objPrepaid.PAYMENT_ID = PaymentId;
            objPrepaid.BANK_PAYMENT_DETAIL_ID = BandPaymentId;
            objPrepaid.REVERSE_FROM_PREPAYMENT_ID = reversePrepaymentId;
            objPrepaid.USER_CASH_DRAWER_ID = cashDrawer_Id;
            objPrepaid.EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE;
            objPrepaid.EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON;

            if (ppAction == PrepaymentAction.ApplyPayment)
            {
                objPrepaid.USER_CASH_DRAWER_ID = 0;// Login.CurrentCashDrawer.USER_CASH_DRAWER_ID;
            }
            else
            {
                if (objPrepaid.USER_CASH_DRAWER_ID != 0)
                {
                    objPrepaid.USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID;
                }
            }
            if (objPrepaid.USER_CASH_DRAWER_ID == 0)
            {
                //update out standaing 
                var appplyFromPrepaymentId = DBDataContext.Db.TBL_APPLY_PAYMENT_HISTORies.Where(x => x.IS_ACTIVE == true && x.PREPAYMENT_ID == reversePrepaymentId).ToList();
                appplyFromPrepaymentId = appplyFromPrepaymentId ?? new List<TBL_APPLY_PAYMENT_HISTORY>();
                if (appplyFromPrepaymentId.Any())
                {
                    foreach (var applyPrepayment in appplyFromPrepaymentId)
                    {
                        var source = DBDataContext.Db.TBL_CUS_PREPAYMENTs.FirstOrDefault(x => x.CUS_PREPAYMENT_ID == applyPrepayment.APPLY_FROM_PREPAYMENT_ID);
                        if (source == null)
                        {
                            continue;
                        }
                        applyPrepayment.IS_ACTIVE = false;
                        DBDataContext.Db.Update(applyPrepayment, applyPrepayment);

                        source.OUT_STANDAING_BALANCE += applyPrepayment.AMOUNT;
                        DBDataContext.Db.SubmitChanges();
                    }
                }
            }
            else
            {
                // if prepaymnet ,add  
                var appplyFromPrepaymentId = DBDataContext.Db.TBL_APPLY_PAYMENT_HISTORies.Where(x => x.IS_ACTIVE == true && x.APPLY_FROM_PREPAYMENT_ID == reversePrepaymentId).ToList();
                appplyFromPrepaymentId = appplyFromPrepaymentId ?? new List<TBL_APPLY_PAYMENT_HISTORY>();
                if (appplyFromPrepaymentId.Any())
                {
                    foreach (var applyPrepayment in appplyFromPrepaymentId)
                    {

                        var source = DBDataContext.Db.TBL_CUS_PREPAYMENTs.FirstOrDefault(x => x.CUS_PREPAYMENT_ID == applyPrepayment.PREPAYMENT_ID);
                        if (source == null)
                        {
                            continue;
                        }
                        applyPrepayment.IS_ACTIVE = false;
                        DBDataContext.Db.Update(applyPrepayment, applyPrepayment);
                        //reverse refund
                        if (source.ACTION_ID == (int)PrepaymentAction.RefundPrepayment)
                        {
                            var balanceRefund = getLastestPrepayCus(intCusId, intCurrencyId);
                            objPrepaid.BALANCE = balanceRefund.BALANCE - source.AMOUNT;
                            var reverseRefund = AddCustomerPrepayment(payDate, intCusId, -source.AMOUNT, objPrepaid.BALANCE, intCurrencyId, Method.GetNextSequence(Sequence.Receipt, true), "", PrepaymentAction.CancelPrepayment, 0, 0);
                            reverseRefund.REVERSE_FROM_PREPAYMENT_ID = source.CUS_PREPAYMENT_ID;
                            DBDataContext.Db.Insert(reverseRefund);
                            DBDataContext.Db.SubmitChanges();
                        }
                        else
                        {
                            TBL_PAYMENT objP = DBDataContext.Db.TBL_PAYMENTs.FirstOrDefault(x => x.PAYMENT_ID == source.PAYMENT_ID);
                            var objPayment = new TBL_PAYMENT() { PAYMENT_ID = source.PAYMENT_ID, PAY_DATE = payDate, BANK_PAYMENT_DETAIL_ID = objP.BANK_PAYMENT_DETAIL_ID };
                            Payment.CancelPayment(objPayment, DBDataContext.Db, "", true);
                        }
                        var CusPrepay = getLastestPrepayCus(intCusId, intCurrencyId);
                        objPrepaid.BALANCE = CusPrepay.BALANCE;
                    }
                    objPrepaid.BALANCE = objPrepaid.BALANCE + decAmount;
                    objPrepaid.CREATE_ON = payDate.AddSeconds(10);
                }
                //reverse outstand 
                var reverseFrom = DBDataContext.Db.TBL_CUS_PREPAYMENTs.FirstOrDefault(x => x.CUS_PREPAYMENT_ID == reversePrepaymentId);
                reverseFrom.OUT_STANDAING_BALANCE = 0m;
                DBDataContext.Db.SubmitChanges();
            }
            return objPrepaid;
        }
        public void ClearPaymentWithPrepay(int cycleId)
        {
            //var k = getInvoiceCanClearWithPrepay();
            ClearPaymentWithPrepay(getInvoiceCanClearWithPrepay(cycleId));
        }

        public void ClearPaymentWithPrepay(List<TBL_INVOICE> invoices)
        {
            if (invoices == null)
            {
                return;
            }

            DateTime now = DBDataContext.Db.GetSystemDate();
            var customers = invoices.GroupBy(x => new { x.CUSTOMER_ID, x.CURRENCY_ID }).Select(x => new { x.Key.CUSTOMER_ID, x.Key.CURRENCY_ID });
            foreach (var cusId in customers)
            {
                TBL_PAYMENT objPayment = new TBL_PAYMENT();
                TBL_CUS_PREPAYMENT objCusPrepayment = new TBL_CUS_PREPAYMENT();
                //var cusId = invoices.FirstOrDefault().CUSTOMER_ID;
                //var currencyId = invoices.FirstOrDefault(x => x.CUSTOMER_ID == cusId).CURRENCY_ID;
                var currencyId = cusId.CURRENCY_ID;

                //objPrepay
                var objPrepay = DBDataContext.Db.TBL_CUS_PREPAYMENTs.OrderByDescending(x => x.CUS_PREPAYMENT_ID).FirstOrDefault(x => x.CUSTOMER_ID == cusId.CUSTOMER_ID && x.CURRENCY_ID == currencyId);
                //AVAILABLE PREPAYMENT FOR SETTLE WITH INVOICE
                var payablePrepayment = DBDataContext.Db.TBL_CUS_PREPAYMENTs.Where(x => x.CUSTOMER_ID == cusId.CUSTOMER_ID && x.CURRENCY_ID == currencyId && x.OUT_STANDAING_BALANCE > 0).ToList();
                //prevent null object reference not set. 
                payablePrepayment = payablePrepayment ?? new List<TBL_CUS_PREPAYMENT>();

                //history prepayment 
                var histories = new List<TBL_APPLY_PAYMENT_HISTORY>();

                //FIRST IN FIRST SETTLE ...
                payablePrepayment = payablePrepayment.OrderBy(x => x.CUS_PREPAYMENT_ID).ToList();

                //Exchange rate
                var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(now, currencyId);
                // 1. ADD PAYMENT.
                objPayment = new TBL_PAYMENT()
                {
                    CREATE_BY = Login.CurrentLogin.LOGIN_NAME + "(" + Resources.PREPAYMENT + ")",
                    CREATE_ON = now,
                    CURRENCY_ID = currencyId,
                    PAY_DATE = now,
                    CUSTOMER_ID = cusId.CUSTOMER_ID,
                    IS_ACTIVE = true,
                    PAY_AMOUNT = 0,
                    DUE_AMOUNT = 0,
                    PAYMENT_NO = Method.GetNextSequence(Sequence.Receipt, true),
                    USER_CASH_DRAWER_ID = 0,//Logic.Login.CurrentCashDrawer.USER_CASH_DRAWER_ID,
                    PAYMENT_ACCOUNT_ID = 86,
                    EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                    EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                    PAYMENT_METHOD_ID = (int)PaymentMethod.Prepayment
                };
                DBDataContext.Db.Insert(objPayment);
                DBDataContext.Db.SubmitChanges();

                decimal totalDue = 0;
                decimal totalPay = 0;
                decimal prepayBalance = objPrepay.BALANCE;


                foreach (var inv in invoices.Where(x => x.CUSTOMER_ID == cusId.CUSTOMER_ID && x.CURRENCY_ID == currencyId))
                {
                    if (prepayBalance <= 0)
                    {
                        break;
                    }

                    var dueAmount = inv.SETTLE_AMOUNT - inv.PAID_AMOUNT;
                    totalDue += dueAmount;
                    TBL_PAYMENT_DETAIL obj = new TBL_PAYMENT_DETAIL()
                    {
                        INVOICE_ID = inv.INVOICE_ID,
                        DUE_AMOUNT = inv.SETTLE_AMOUNT - inv.PAID_AMOUNT,
                        PAYMENT_ID = objPayment.PAYMENT_ID,
                        PAY_AMOUNT = prepayBalance >= (inv.SETTLE_AMOUNT - inv.PAID_AMOUNT) ? (inv.SETTLE_AMOUNT - inv.PAID_AMOUNT) : prepayBalance,
                    };
                    DBDataContext.Db.Insert(obj);
                    totalPay += obj.PAY_AMOUNT;
                    prepayBalance -= obj.PAY_AMOUNT;

                    inv.PAID_AMOUNT += obj.PAY_AMOUNT;
                    inv.INVOICE_STATUS = inv.SETTLE_AMOUNT == inv.PAID_AMOUNT ? (int)InvoiceStatus.Close : (int)InvoiceStatus.Open;
                    DBDataContext.Db.SubmitChanges();

                    //UPDATE PREPAYMENT OUTSTANDING
                    var payAmount = obj.PAY_AMOUNT;
                    foreach (var prepayment in payablePrepayment)
                    {
                        if (payAmount == 0)
                        {
                            break;
                        }
                        if (prepayment.OUT_STANDAING_BALANCE == 0)
                        {
                            break;
                        }
                        var amountToSettle = payAmount;
                        if (payAmount > prepayment.OUT_STANDAING_BALANCE)
                        {
                            amountToSettle = prepayment.OUT_STANDAING_BALANCE ?? 0;
                        }
                        prepayment.OUT_STANDAING_BALANCE -= amountToSettle;
                        payAmount -= amountToSettle;

                        //OLD OBJECT AND NEW OBJECT ARE THE SAME = NO CHANGE LOG 
                        DBDataContext.Db.Update(prepayment, prepayment);
                        var history = new TBL_APPLY_PAYMENT_HISTORY()
                        {
                            AMOUNT = amountToSettle,
                            IS_ACTIVE = true,
                            INVOICE_ID = inv.INVOICE_ID,
                            APPLY_FROM_PREPAYMENT_ID = prepayment.CUS_PREPAYMENT_ID,
                            PREPAYMENT_ID = 0
                        };
                        histories.Add(history);
                    }
                }
                objPayment.PAY_AMOUNT = totalPay;
                objPayment.DUE_AMOUNT = totalDue;
                DBDataContext.Db.SubmitChanges();

                //Update PrePayment
                objCusPrepayment = AddCustomerPrepayment(now, cusId.CUSTOMER_ID, -totalPay, prepayBalance, currencyId, objPayment.PAYMENT_NO, "", PrepaymentAction.ApplyPayment, objPayment.PAYMENT_ID);
                DBDataContext.Db.TBL_CUS_PREPAYMENTs.InsertOnSubmit(objCusPrepayment);
                DBDataContext.Db.SubmitChanges();

                //LOG HISTORY 
                histories.ForEach(x =>
                {
                    x.PREPAYMENT_ID = objCusPrepayment.CUS_PREPAYMENT_ID;
                });

                DBDataContext.Db.TBL_APPLY_PAYMENT_HISTORies.InsertAllOnSubmit<TBL_APPLY_PAYMENT_HISTORY>(histories);
                DBDataContext.Db.SubmitChanges();
            }
        }

        public bool PaymentCancelRemovePrepay(TBL_PAYMENT payment, TBL_PAYMENT objCancel)
        {
            DateTime now = DBDataContext.Db.GetSystemDate();
            var objToRevese = DBDataContext.Db.TBL_CUS_PREPAYMENTs.FirstOrDefault(x => x.PAYMENT_ID == payment.PAYMENT_ID);
            var Payment = DBDataContext.Db.TBL_PAYMENTs.FirstOrDefault(x => x.PARENT_ID == payment.PAYMENT_ID);
            if (objToRevese == null)
            {
                //prepay =0;
                return true;
            }
            var CusPrepay = getLastestPrepayCus(Payment.CUSTOMER_ID, Payment.CURRENCY_ID);
            var objCusPrepayment = RemoveCustomerPrepayment(now, Payment.CUSTOMER_ID, -objToRevese.AMOUNT, CusPrepay.BALANCE - objToRevese.AMOUNT, objToRevese.CURRENCY_ID, Method.GetNextSequence(Sequence.Receipt, true), "", PrepaymentAction.CancelPrepayment, objToRevese.CUS_PREPAYMENT_ID, objToRevese.USER_CASH_DRAWER_ID, objCancel.PAYMENT_ID);

            if (objToRevese.USER_CASH_DRAWER_ID == 0)
            {
                objCusPrepayment.USER_CASH_DRAWER_ID = 0;
            }
            else
            {
                objCusPrepayment.USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID;
            }
            DBDataContext.Db.TBL_CUS_PREPAYMENTs.InsertOnSubmit(objCusPrepayment);
            DBDataContext.Db.SubmitChanges();
            return true;
        }

        public void MovePaymentToPrepay(List<TBL_PAYMENT> payments)
        {
            DateTime now = DBDataContext.Db.GetSystemDate();
            //customer meter
            var paymentDistinct = payments.Distinct().ToList();
            var payId = DBDataContext.Db.TBL_CUS_PREPAYMENTs.Select(x => x.PAYMENT_ID).Distinct();

            //var k = from p in payments
            //        where !payId.Contains(p.PAYMENT_ID)
            //        select p;

            var CusPrepay = getPrepayCustomer();
            foreach (var objPayment in paymentDistinct)
            {
                decimal balance = 0;
                if (CusPrepay != null)
                {
                    balance = CusPrepay.FirstOrDefault(x => x.CUSTOMER_ID == objPayment.CUSTOMER_ID && x.CURRENCY_ID == objPayment.CURRENCY_ID) == null ? 0 : CusPrepay.FirstOrDefault(x => x.CUSTOMER_ID == objPayment.CUSTOMER_ID && x.CURRENCY_ID == objPayment.CURRENCY_ID).BALANCE;
                }
                var action = PrepaymentAction.AddPrepayment;

                //when cash drawer = 0 it mean that payment use prepayment to settle => when add move to prepayment the balance shall not increase.
                if (objPayment.USER_CASH_DRAWER_ID == 0)
                {
                    action = PrepaymentAction.ApplyPayment;
                }
                var objCusPrepayment = AddCustomerPrepayment(now, objPayment.CUSTOMER_ID, objPayment.PAY_AMOUNT, balance + objPayment.PAY_AMOUNT, objPayment.CURRENCY_ID, Method.GetNextSequence(Sequence.Receipt, true), "", action, objPayment.PAYMENT_ID, 0, true);
                DBDataContext.Db.Insert(objCusPrepayment);
            }
            DBDataContext.Db.SubmitChanges();
        }
    }
}
