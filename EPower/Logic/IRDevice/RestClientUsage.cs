﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;
using SoftTech;
using SoftTech.Component;
using System.Reflection;
using Newtonsoft.Json;
using RestSharp.Authenticators;
using EPower.Properties;
using System.Linq;
using System.Net;
using SoftTech.Helper;
using System.Windows.Forms;
using System.Transactions;
using System.Data.SqlClient;

namespace EPower.Logic.IRDevice
{
    class RestClientUsage : IRDevice
    {
        string URL = "http://192.168.1.201:5555/api/v1.0/";
        RestClient _client = null;
        RestSupplier Supplier { get; set; }
        JsonSerializerSettings js = new JsonSerializerSettings();

        string format = "csv";
         

        enum UploadStatus
        {
            uploading = 3101,
            ready = 3102,
            processing = 3103
        }

        enum Operation
        {
            BillingUpload = 1,
            BillingDownload = 2,
            DeviceUpload = 3,
            DeviceDownload = 4

        }


        int _licenseId =0;

        List<string> data = new List<string>();

        

        StringBuilder sendDataUsages;

        public RestClientUsage()
        {
            this.HasSerial = false;
            _client = new RestClient(URL);
            Supplier = new RestSupplier();
            Supplier.code = "234";
            Supplier.secret_code = "234-234";
            js.DateFormatString = "yyyy-MMM-dd";
            
        }

        public override int GetUsage(List<TBL_BILLING_CYCLE> mCycle, List<TBL_AREA> mArea, ref string month, bool newDb = true)
        {
            
            var usages = base.GetMonthlyUsages(mCycle, mArea);
            data = new List<string>();
            sendDataUsages = new StringBuilder();
            string licenseCode = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault().AREA_CODE;
            string str = Runner.Instance.Text;
            int total = usages.Count(); 
            int deviceId = 0; 
            int num = 0;
             
            var processDate = DateTime.Now; 

            var isCollectUsage = false;
            var downloadDate = new DateTime(1900, 1, 1); 
            var uploadDate = DateTime.Now;
            var readDate = new DateTime(1900, 1, 1);
            var latitude = 0;
            var longitude = 0;


            var process = new RestProcessUsageModel();
            process.operation_code = Guid.NewGuid().ToString();
            process.operation = (int)Operation.BillingUpload;
            process.operation_params = Operation.BillingUpload.ToString(); 
            process.completed = false;
            process.note = ""; 
            process.data = "";
            getHostInformation(process);



            /*
             * Add header
             */
            string header = String.Join(",", typeof(RestUsageModel).GetProperties().Select(x => x.Name).ToArray());
            sendDataUsages.AppendLine(header);
             
            foreach (var row in usages)
            { 
                var csvRow = "";
                csvRow += string.Format("\"{0}\",", _licenseId);
                csvRow += string.Format("\"{0}\",",  process.operation_code.Replace("\"",""));
                csvRow += string.Format("\"{0}\",", deviceId);
                csvRow += string.Format("\"{0}\",", row.METER_CODE.Replace("\"", ""));
                csvRow += string.Format("\"{0}\",", row.CUSTOMER_CODE.Replace("\"", ""));
                csvRow += string.Format("\"{0}\",", row.FULL_NAME.Replace("\"", ""));
                csvRow += string.Format("\"{0}\",", row.AREA_CODE.Replace("\"", ""));
                csvRow += string.Format("\"{0}\",", row.POLE_CODE.Replace("\"", ""));
                csvRow += string.Format("\"{0}\",", row.BOX_CODE.Replace("\"", ""));
                csvRow += string.Format("\"{0}\",", row.START_USAGE);
                csvRow += string.Format("\"{0}\",", row.END_USAGE);
                csvRow += string.Format("\"{0}\",", row.MULTIPLIER);
                csvRow += string.Format("\"{0}\",", row.LAST_TOTAL_USAGE);
                csvRow += string.Format("\"{0}\",", row.USAGE_HISTORY.Replace(',',':').Replace("\"", ""));
                csvRow += string.Format("\"{0}\",", row.IS_READ_IR);
                csvRow += string.Format("\"{0}\",", row.IS_DIGITAL);
                csvRow += string.Format("\"{0}\",", row.IS_METER_RENEW_CYCLE);
                csvRow += string.Format("\"{0}\",", isCollectUsage);
                csvRow += string.Format("\"{0}\",", downloadDate.ToString("yyyy-MM-dd").Replace("\"", ""));
                csvRow += string.Format("\"{0}\",", readDate.ToString("yyyy-MM-dd").Replace("\"", ""));
                csvRow += string.Format("\"{0}\",", uploadDate.ToString("yyyy-MM-dd").Replace("\"", ""));
                csvRow += string.Format("\"{0}\",", latitude);
                csvRow += string.Format("\"{0}\"", longitude);

                sendDataUsages.AppendLine(csvRow);


                //Commit Transaction every insert 1000 record or end of record. 
                num++;
                if (num % 1000 == 0 || num % total == 0)
                {
                    process.data = sendDataUsages.ToString();
                    data.Add(JsonConvert.SerializeObject(process, js));
                    sendDataUsages = new StringBuilder();
                    sendDataUsages.AppendLine(header);

                    Runner.Instance.Text = String.Format(str + "\nCustomer {0} ({1:#.##}%)", num, (float)(num * 100 / total));
                    System.Windows.Forms.Application.DoEvents();
                }
            }



            List<string> m = new List<string>();
            foreach (var c in mCycle)
            {
                m.Add(Method.GetNextBillingMonth(c.CYCLE_ID).Month.ToString("00"));
            }
            month = string.Join(",", m.Distinct().ToArray());


            process.status = (int)UploadStatus.ready;
            process.data = "";

            data.Add(JsonConvert.SerializeObject(process, js));

            return total;
        }


        public override bool SendFile()
        {
            var total = data.Count;
            if (total==0)
            {
                throw new Exception("No Data");
            }

            //_client.Authenticator = new HttpBasicAuthenticator("admin", "admin");
            int i = 0;
            _client.Timeout = (int)TimeSpan.FromMinutes(30).TotalMilliseconds;
            foreach (var row in data)
            {
                var req = new RestRequest("processes/upload", RestSharp.Method.POST);
                req.AddHeader("Content-Type", "application/json");
                req.AddParameter("application/json", row, ParameterType.RequestBody);

                var res = _client.Execute(req);

                if (res.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    throw new Exception(res.StatusCode.ToString());
                }
                i++;
                Runner.Instance.Text = String.Format( "\nData Uploading... {0}/{1} ({2:#.##}%)", i,total, (float)(i * 100 / total));
                System.Windows.Forms.Application.DoEvents();
            }

            return true; 
        }


        public override bool GetFile(BackupType backup)
        { 
            if (backup  == BackupType.BeforeSFTD)
            {
                return true;
            }
            data = new List<string>(); 

            //_client.Authenticator = new HttpBasicAuthenticator("admin", "admin");
            

            var process = new RestProcessUsageModel();
            //process.operation_code = "f31e731b-60be-40a9-9958-dc2d7b429396";
            process.operation_code = Guid.NewGuid().ToString();
            process.operation = (int)Operation.BillingDownload;
            process.operation_params = Operation.BillingDownload.ToString() ; 
            process.completed = false; 
            process.note = ""; 
            process.data = "";
            getHostInformation(process);

            var task = download(process, (int)TimeSpan.FromMinutes(30).TotalMilliseconds); 

            if (task!=null)
            {
                /*
                 * exc 1 csv data
                 */
                data.Add(task.data);  
                while (task?.pending>0)
                {
                    task = download(process);
                    /*
                     * exc loop csv data
                     */
                    data.Add(task.data);
                }
                if (task.pending == 0)
                {
                    process.status = (int)UploadStatus.ready;
                    task = download(process);
                    return task != null;
                } 
            }
            return true;
        }

        private void getHostInformation(RestProcessUsageModel process)
        {
            process.format = format;
            process.created_host = Environment.MachineName;
            process.created_ip = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)?.ToString();
            process.created_version = Method.GetUtilityValue(Utility.VERSION);
            process.supplier_code = this.Supplier.code;
            process.supplier_secret_code = this.Supplier.secret_code;

        }

        public override bool SaveUsage(int intDeviceID, int intCollectorID)
        {
            bool blnReturn = false;
            //TBL_DEVICE objDevice = DBDataContext.Db.TBL_DEVICEs.Where(x => x.DEVICE_ID == intDeviceID).FirstOrDefault();

            try
            {
                if (data.Count == 0)
                {
                    throw new MsgBox.MessageException(Resources.MS_DEVICE_NO_CUSTOMER_COLLECT_USAGE);
                }

                Runner.Instance.Text = string.Format(Resources.MS_SAVING_DATA, "");
                Application.DoEvents();
                DBDataContext.Db.ExecuteCommand("TRUNCATE TABLE TMP_IR_USAGE");
                string str = Runner.Instance.Text;

                //using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                //{
                int k = 0;
                var n = data.Count;
                this.TotalCollectCustomer = 0;
                using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    var bulk = new SqlBulkCopy(Method.GetConnectionString());
                    bulk.BulkCopyTimeout = 60 * 60;// 30-minute
                    bulk.NotifyAfter = 100;
                    bulk.DestinationTableName = "TMP_IR_USAGE";
                    bulk.SqlRowsCopied += delegate (object sender, SqlRowsCopiedEventArgs e)
                    {
                        Runner.Instance.Text = string.Format("Bulk copying...{0:#,##0} ", e.RowsCopied);
                        //Application.DoEvents();
                    };
                    foreach (var task in data)
                    {
                        k++;
                        var tmpUsage = csvToTMP_IR_USAGE(task);
                        bulk.WriteToServer(tmpUsage._ToDataTable());

                        Runner.Instance.Text = string.Format(str + "\nTasks {0} ({1:#.##}%)", k, (float)(k * 100 / n));
                        //Application.DoEvents();
                        this.TotalCollectCustomer += tmpUsage.Count;
                    }
                    tran.Complete();
                    bulk.Close();
                }
                
                
                using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    this.RunIRUsage(intDeviceID, intCollectorID);
                    tran.Complete();
                }
                blnReturn = true;
                DBDataContext.Db = null;
            }
            catch (Exception exp) { throw exp; } 
            
            return blnReturn;

        }

        List<TMP_IR_USAGE> csvToTMP_IR_USAGE(string data)
        {
            List<TMP_IR_USAGE> objs = new List<TMP_IR_USAGE>();
            foreach (var item in csvToDict(data))
            {
                var tmp = new TMP_IR_USAGE();
                tmp.METER_CODE = item["meter_code"];
                tmp.BOX_CODE = item["box_code"];
                tmp.END_USAGE = DataHelper.ParseToDecimal(item["end"]);
                tmp.IS_DIGITAL = DataHelper.ParseToBoolean(item["is_digital"]);
                tmp.IS_METER_RENEW_CYCLE = DataHelper.ParseToBoolean(item["is_new_cycle"]);
                tmp.IS_READ_IR = DataHelper.ParseToBoolean(item["is_read"]);
                tmp.REGISTER_CODE = Method.GetMETER_REGCODE(tmp.METER_CODE, tmp.IS_DIGITAL);
                objs.Add(tmp);
            }
            return objs;

        }

        List<Dictionary<string,string>> csvToDict(string data)
        {
            var rows = data.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            var header = Serializer.SplitCsv(rows.First());

            var dict = from r in rows.Skip(1)
                       let col = Serializer.SplitCsv(r)//r.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                       select new
                       {
                           c = header.Select((a, i) => new { key = a, data = col[i] }).ToDictionary(b => b.key, c => c.data)
                       };
            return dict.Select(x => x.c).ToList();
        }
        
        RestDownloadUsage download(RestProcessUsageModel process, int timeout = 30*3000)
        {
            JsonSerializerSettings js = new JsonSerializerSettings();
            js.DateFormatString = "yyyy-MMM-dd";
            var request = new RestRequest("processes/download", RestSharp.Method.POST);
            _client.Timeout = timeout;
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.AddParameter("application/json", JsonConvert.SerializeObject(process, js), ParameterType.RequestBody);

            var response = _client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<RestDownloadUsage>(response.Content);
            }
            return null;
        }

        public override string GetID()
        {
            _client.Authenticator = new HttpBasicAuthenticator("admin", "admin");
            var req = new RestRequest("suppliers/auth", RestSharp.Method.POST);
            req.AddHeader("Content-Type", "application/json");
            req.AddParameter("application/json", JsonConvert.SerializeObject(this.Supplier), ParameterType.RequestBody);
            var res = _client.Execute(req);

            if (res.StatusCode == System.Net.HttpStatusCode.OK)
            {
                this.Supplier = JsonConvert.DeserializeObject<RestSupplier>(res.Content);
                return Supplier.name;
            }

            return "";
        }

        public override bool Test()
        {
            return true;
        }

        public override bool CheckVersion()
        {
            return true;
        }

        class RestSupplier
        {
            public string id { get; set; } 
            public string code { get; set; }
            public string name { get; set; }
            public string secret_code { get; set; }
        }

        class RestDownloadUsage
        {
            public string data { get; set; }
            public int pending { get; set; }
        }
        
        class RestProcessUsageModel
        {
            public string operation_code { get; set; }
            public int operation { get; set; }
            public string operation_params { get; set; }
            public string format { get; set; }
            public string created_host { get; set; }
            public string created_ip { get; set; }
            public string created_version { get; set; }
            public int status { get; set; }
            public bool completed { get; set; }
            public string note { get; set; }
            public string supplier_code { get; set; }
            public string supplier_secret_code { get; set; }
            public DateTime start_date { get; set; }
            public DateTime end_date { get; set; }
            public int total_record { get; set; }
            public string result { get; set; }
            public string data { get; set; }
            RestProcessTaskUsageMondel[] tasks { get; set; }
        }
        
        class RestProcessTaskUsageMondel
        {
            string text_data { get; set; }
            string format { get; set; }
            int status { get; set; }
            bool completed { get; set; }
            DateTime start_date { get; set; }
            DateTime end_date { get; set; }
            int total_record { get; set; }
            string result { get; set; }
            DateTime updated_date { get; set; }
            string updated_by { get; set; }
            DateTime created_date { get; set; }
            bool is_active { get; set; }
            string created_by { get; set; }
        }
        
        class RestUsageModel
        {
             
            public int supplier_id { get; set; }
            public string process_id { get; set; }
            public int device_id { get; set; }
            public string meter_code { get; set; }
            public string customer_code { get; set; }
            public string customer_name { get; set; }
            public string area_code { get; set; }
            public string pole_code { get; set; }
            public string box_code { get; set; }
            public decimal start { get; set; }
            public decimal end { get; set; }
            public int multiplier { get; set; }
            public decimal last_total_usage { get; set; }
            public string usage_history { get; set; }
            public bool is_read { get; set; }
            public bool is_digital { get; set; }
            public bool is_new_cycle { get; set; }
            public bool is_collect { get; set; }
            public DateTime download_date { get; set; }
            public DateTime read_date { get; set; }
            public DateTime upload_date { get; set; }
            public decimal latitude { get; set; }
            public decimal longitude { get; set; }
        }
        
    }
}
