﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;

namespace EPower.Logic.IRDevice
{ 
    /// <summary>
    /// Thinta Handheld Device
    /// Platform   : Linux 
    /// Communicate: Serial
    /// Models     : TP900, TP800, TP1100
    /// </summary>
    class ThinPad:IRDevice 
    {
        const string PublishVersion = "V2.5 E-Power"; 
        const string SysExecute = "E-Power.mid";
        const string SysMif = "Template\\E-Power.mif";


        #region DLL Declaration 
        internal static int _StepFile = 0; 

        public struct tFileProperties
        {
            public string name; // 以0 作结束符的文件名
            public int size; // 文件大小
            public int date; // 文件日期
            public int time; // 文件时间
            public byte attrib; // 文件属性
            public byte file_type; // 文件类型（没有使用）
            public string reserved; // 保留
        }

        [DllImport("comdll.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int GetHcSysInfo(int nPort, int nBaoudrate, string cSaveAs, IntPtr intBuffer);

        [DllImport("comdll.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int GetHcDirInfo(int nPort, int nBaoudrate, string cHcFileName, string cDirInfoFile, ref  int nFileCount, ref int nSize, int fPrompt);

        [DllImport("comdll.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int GetHcFileInfo(int nPort, int nBaoudrate, string lpszHcFileName, byte[] lpFileInfoBuffer);
 
        [DllImport("COMDLL.DLL", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int GetHcFile(int nPort, int nBaoudrate, string cHcFileName, string cSaveAs, int fPrompt);
         
        [DllImport("COMDLL.DLL", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int SendFileToHc(int nPort, int nBaoudrate, string cHcFileName, string cOpenFileName, int nAttrib, int fPrompt);

        [DllImport("COMDLL.DLL", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern int thrdSendMifToHc(int nPort, int nBaudrate, string lpszOpenFileName, int nAttrib, int fPrompt);
         
        [DllImport("COMDLL.DLL", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int DelHcFile(int nPort, int nBaud, string cHcFileName, int fPrompt);

        [DllImport("comdll.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int GetLastErrCode();

        [DllImport("comdll.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int thrdQueryStatus();

        [DllImport("comdll.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int thrdStop(int intPromft);


        [DllImport("comdll.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int thrdQueryPercent();


        [DllImport("comdll.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int thrdGetHcFile(int nPort, int nBaoudrate, string cHcFileName, string cSaveAs, int fPrompt);

        [DllImport("COMDLL.DLL", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        private static extern int thrdSendFileToHc(int nPort, int nBaoudrate, string cHcFileName, string cOpenFileName, int nAttrib, int fPrompt);
          
        /// <summary>
        /// Get last error message
        /// </summary>
        /// <param name="strErrorMessage"></param>
        /// <returns></returns>
        internal static int GetErrorCode(ref string strErrorMessage)
        {
            int intCode = 0;
            strErrorMessage = Resources.MS_CANNOT_CONNECT_TO_DEVICE;
            try
            {
                intCode = GetLastErrCode();
                if (intCode == 10)
                {
                    strErrorMessage = Resources.MS_PLEASE_CONNECT_DEVICE_TO_PC;
                }
                else if (intCode == 11)
                {
                    strErrorMessage = Resources.MS_ENTER_COMMUNICATION_FUNTION_IN_DEVICE;
                }
                else
                {
                    strErrorMessage = string.Format(Resources.MS_ENTER_COMMUNICATION_FUNTION_IN_DEVICE, intCode.ToString());
                }
            }
            catch { }
            return intCode;
        } 

        /// <summary>
        /// Show percentage of send or receive data from IR Reader in Runner
        /// </summary>
        /// <param name="strErrorMessage">get error message</param>
        /// <returns>
        /// -1 error connection
        /// 0 Send or receive success</returns>
        internal static int ShowSendReceivePer(ref string strErrorMessage, string MsgPerc, int n)
        {
            //get status
            int intStatus = thrdQueryStatus();
            _StepFile = 1;
            //Geting Data from device
            while (intStatus != 0)
            {
                //Error connection
                if (intStatus == 32768)
                {
                    //stop all connection to device
                    //in order to start new connection
                    thrdStop(2);
                    GetErrorCode(ref strErrorMessage);
                    return -1;
                }
                //Prepair to connect
                else if (intStatus == 1 || intStatus == 2 || intStatus == 3)
                {
                    int intPercentage = thrdQueryPercent();
                    string strPercentage = "";
                    if (intPercentage != 0)
                    {
                        strPercentage = ((intPercentage + (_StepFile - 1) * 100) / n).ToString();
                    }
                    if (intPercentage == 100 && _StepFile < n)
                    {
                        _StepFile++;
                    }
                    Runner.Instance.Text = string.Format(MsgPerc, strPercentage,"%");
                }

                Application.DoEvents();
                intStatus = thrdQueryStatus();
            }
            _StepFile = 0;
            return 0;
        }

        
        /// <summary>
        /// Get IR Reader ID
        /// </summary>
        /// <param name="strDeviceId">Device Id</param>
        /// <param name="strErrorMessage">Return Error Message if Error Occur</param>
        /// <returns></returns>
        internal static int TestPort(ref string strDeviceId, ref string strErrorMessage, int intPortNumber, int intBoundRate)
        {
            int intReturn = 1;
            try
            {
                strDeviceId = "";
                strErrorMessage = "";
                string strLine;
                IntPtr b = new IntPtr(1024); 
                int intInfo = GetHcSysInfo(intPortNumber, intBoundRate, "Template/IRInfo.dat", b);
                //if the calling was error 
                //return the error message
                if (intInfo != 0)
                {
                    intReturn = GetErrorCode(ref strErrorMessage);
                    return intReturn;
                }

                //Get Device ID
                StreamReader objFile = new StreamReader("Template/IRInfo.dat");
                while ((strLine = objFile.ReadLine()) != null)
                {
                    if (strLine.Contains("ID="))
                    {
                        //get Device id from file
                        strDeviceId = strLine.Replace("ID=", "");
                        return 0;
                    }
                }
            }
            catch { }
            return intReturn;
        }

        #endregion DLL Declaration

         

        #region DLL Implimentation 

        public ThinPad()
            : base()
        { }
        public override bool SendFile()
        {
            int intReturn = 1;
            bool res = false;
            string strErrorMessage = string.Empty;
            this.UpdateDBInfo();
            this.ConvertToTPFile("Template/DBInfo.dbf");
            this.ConvertToTPFile("Template/DDSc.dbf");
            this.ConvertToTPFile("Template/DDSb.dbf");
            this.CreateMifFileDB(this._TmpPath);
            intReturn = thrdSendMifToHc(this.COM, this.BaudRate, "Template\\DATA.mif", 1, 1);
            if (intReturn != 0)
            {
                intReturn = GetErrorCode(ref strErrorMessage);
                res = false;
                throw new Exception(strErrorMessage);
            }
            intReturn = ShowSendReceivePer(ref strErrorMessage, Resources.MS_SEND_FILE_TO_DEVICE, 2);
            if (intReturn != 0)
            {
                intReturn = GetErrorCode(ref strErrorMessage);
                res = false;
                throw new Exception(strErrorMessage);
            } 
            res = true;
            return res;
        }

        public override int GetUsage(List<TBL_BILLING_CYCLE> cycle,List<TBL_AREA> area, ref string month,bool newDb)
        {
            return base.GetUsage(cycle,area, ref month);
        }

        public override string GetID()
        {
            int intReturn = 1;
            string strDeviceId = "",
                strErrorMessage = "";
            try
            {

                IntPtr b = new IntPtr(1024); 
                int intInfo = GetHcSysInfo(this.COM, this.BaudRate, "Template/IRInfo.dat", b);
                //if the calling was error 
                //return the error message
                if (intInfo != 0)
                {
                    intReturn = GetErrorCode(ref strErrorMessage);
                    throw new Exception(strErrorMessage);
                }

                //Get Device ID
                foreach (string strLine in File.ReadAllLines("Template/IRInfo.dat"))
                {
                    if (strLine.Contains("ID="))
                    {
                        //get Device id from file
                        strDeviceId = strLine.Replace("ID=", "");
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            this.ProductID = strDeviceId;
             
            return this.ProductID; 
        }

        public override bool Test()
        {
            int intReturn = 1;
            bool res=false;
            try
            {
                string strDeviceId = "",
                strErrorMessage = ""; 
                IntPtr b = new IntPtr(1024); 
                int intInfo = GetHcSysInfo(this.COM, this.BaudRate, "Template/IRInfo.dat", b);
                //if the calling was error 
                //return the error message
                if (intInfo != 0)
                {
                    intReturn = GetErrorCode(ref strErrorMessage);
                    throw new Exception(strErrorMessage);
                }

                //Get Device ID 
                foreach (string strLine in File.ReadAllLines("Template/IRInfo.dat"))
                {
                    if (strLine.Contains("ID="))
                    {
                        //get Device id from file
                        strDeviceId = strLine.Replace("ID=", "");
                        res = true;
                        break;
                    }
                }
            }
            catch (Exception exp) { throw exp; }
            return res;
        }

        public override List<TMP_IR_USAGE> GetFile(BackupType backup)
        {
            string strErrorMessage = string.Empty;
            //Get File form IR Reader 
            int intReturn = 1; 
            List<TMP_IR_USAGE> res = base.GetFile(backup); 


            intReturn = thrdGetHcFile(this.COM, this.BaudRate, "DDSEN\\DDSb.dat", Settings.Default.IR_FILE_DB_LOCAL, 1);


            if (intReturn != 0)
            {
                if (backup == BackupType.BeforeSFTD)
                {
                    intReturn = GetErrorCode(ref strErrorMessage);
                    return base.GetFile(backup);
                }
                intReturn = GetErrorCode(ref strErrorMessage); 
                throw new Exception(strErrorMessage);
            } 

            if (ShowSendReceivePer(ref strErrorMessage, Resources.MS_RECEIVING_FILE_FROM_DEVICE, 1) == -1)
            {
                throw new Exception(strErrorMessage);
            }
            //backup file dbf After copy from device            
            string fileBackup = string.Format("{0}{1}{2:yyyyMMddhhmmss}.dbf", backup.ToString(), this.ProductID, DBDataContext.Db.GetSystemDate());
            File.Copy(Settings.Default.IR_FILE_DB_LOCAL, Path.Combine(this.PathBackup, fileBackup), true);
            /*
             * Extract usages.
             * Old version not data to extract.
             */
            return res;
        }

        public override bool CheckVersion()
        {
            int intReturn = 1;  
            byte[] data = new byte[32];
            //intReturn = ThinPad.GetHcFileInfo(this.COM, this.BaudRate, "DDSEN\\" + FileSys, data);
            intReturn = GetHcFile(this.COM, this.BaudRate, "DDSEN\\" + FileSys, FileSys, 1);
            if (intReturn != 0)
            {
                return Update(); 
            }
             
            string buff = File.ReadAllText(FileSys);
            if (buff.Length < 14)
            {
                return Update();
            }
            buff = buff.Substring(2, PublishVersion.Length);

            if (!buff.Contains(PublishVersion))
            {
                return Update();
            } 
            this.ProductVersion = PublishVersion;
            return true;
        }

        private bool Update()
        {
            int intReturn = 1;
             
            string strErrorMessage = string.Empty; 

            if (File.Exists(Path.Combine(_TmpPath,_TmpDDSa))
                && File.Exists(Path.Combine(_TmpPath, _TmpDDSb))
                && File.Exists(Path.Combine(_TmpPath, _TmpDDSc))
                && File.Exists(Path.Combine(_TmpPath, _TmpInfo))
                && File.Exists(Path.Combine(_TmpPath, SysExecute))
                && File.Exists(Path.Combine(_TmpPath, FileSys)))
            {
                File.Copy(Path.Combine(_TmpPath, _TmpDDSa), Path.Combine(_TmpPath, _DDSa),true);
                File.Copy(Path.Combine(_TmpPath, _TmpDDSb), Path.Combine(_TmpPath, "DDSb.dat"), true);
                File.Copy(Path.Combine(_TmpPath, _TmpDDSc), Path.Combine(_TmpPath, "DDSc.dat"), true);
                File.Copy(Path.Combine(_TmpPath, _TmpInfo), Path.Combine(_TmpPath, "DBInfo.dat"), true); 
                CreateMifFileSys();
            }
            else
            {
                throw new Exception(string.Concat(Resources.MS_DEVICE_NEED_TO_UPDATE, "\n", Resources.MS_TAMPLATE_DEVICE_DATABASE_NOT_FOUND)); 
            }



            intReturn = thrdSendMifToHc(this.COM, this.BaudRate, SysMif, 1, 1);
            if (intReturn != 0)
            {
                intReturn = GetErrorCode(ref strErrorMessage);
                throw new Exception(string.Concat(Resources.MS_DEVICE_NEED_TO_UPDATE, "\n", strErrorMessage));
            }
            intReturn = ShowSendReceivePer(ref strErrorMessage, Resources.MS_UPDATING_APPLICATION_IN_DEVICE, 4);
            if (intReturn != 0)
            {
                intReturn = GetErrorCode(ref strErrorMessage);
                throw new Exception(string.Concat(Resources.MS_DEVICE_NEED_TO_UPDATE, "\n", strErrorMessage));
            }

            this.ProductVersion = PublishVersion;
            return true;
            
        }
         
        private void UpdateDBInfo()
        {
            try
            {
                 
                using ( BinaryReader b = new BinaryReader(File.Open(Path.Combine(this._TmpPath, this._DBInfo), FileMode.Open, FileAccess.ReadWrite)))
                {
                    int header, recordNum, recordSize;
                    long seek;
                    byte[] dbfInfo = new byte[4];
                    byte[] data;
                    string no, db_name, record;

                    b.BaseStream.Seek(8, SeekOrigin.Begin);
                    b.Read(dbfInfo, 0, dbfInfo.Length);
                    header = dbfInfo[0] + (dbfInfo[1] << 8);

                    b.BaseStream.Seek(4, SeekOrigin.Begin);
                    b.Read(dbfInfo, 0, dbfInfo.Length);
                    recordNum = dbfInfo[0] + (dbfInfo[1] << 8);

                    b.BaseStream.Seek(10, SeekOrigin.Begin);
                    b.Read(dbfInfo, 0, dbfInfo.Length);
                    recordSize = dbfInfo[0] + (dbfInfo[1] << 8);

                    data = new byte[recordSize];
                    header++;

                    recordNum = (int)(b.BaseStream.Length - header) / recordSize;


                    for (int i = 0; i < recordNum; i++)
                    {
                        seek = header + i * recordSize;
                        b.BaseStream.Seek(seek, SeekOrigin.Begin);
                        b.Read(data, 0, recordSize);
                        Encoding s = Encoding.ASCII;
                        no = new string(s.GetChars(data, 0, 9)).Replace("\0",string.Empty);
                        db_name = new string(s.GetChars(data, 9, 12)).Replace("\0", string.Empty);
                        record = new string(s.GetChars(data, 21, 9)).Replace("\0", string.Empty);
                        if (db_name.ToLower().Contains(this._DDSb.ToLower()))
                        {
                            db_name = "DDSb.dat";
                        }
                        else
	                    {
                            db_name = "DDSc.dat";
	                    }
                        int k = 9;
                        foreach (char item in db_name.ToArray())
                        {
                            data[k++] = (byte)item;
                        }
                        b.BaseStream.Seek(seek, SeekOrigin.Begin);
                        b.BaseStream.Write(data, 0, (int)recordSize);

                    }
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
 
        private void ConvertToTPFile(string file)
        {
            try
            {
                string fileRename = file.Remove(file.Length - 3, 3) + "dat";
                if (File.Exists(fileRename))
                {
                    File.Delete(fileRename);
                }

                if (File.Exists(file))
                {
                    File.Move(file, fileRename);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            
        }

        private void CreateMifFileDB(string path)
        {
            string file = Path.Combine(path, "DATA.mif");
            if (File.Exists(file))
            {
                File.Delete(file);
            }
            File.WriteAllText(file, @"[Common]  
author=Serey
date=2012/04/25
time=16:40:00
ver=1.0

[Send] 
file1 = DDSc.dat
file2 = DDSb.dat
file3 = DBInfo.dat       

pcpath=.\
hcpath=\DDSEN
autorun=0

[Receive]");
        }

        private void CreateMifFileSys()
        {
            if (File.Exists(SysMif))
            {
                File.Delete(SysMif);
            }
            File.WriteAllText(SysMif, @"[Common]  
[Common]
appname=E-Power
author=Serey
date=2012/07/07
time=09:25:00
ver=1.3

[Send]
application=E-Power.mid
file1 = DDSa.dbf      
file2 = DDSb.dat
file3 = DDSc.dat
file4 = sys.dat
pcpath=.\
hcpath=\DDSEN
autorun=1

[Receive]
application=E-Power.mid
pcpath=c:\back");
        }
        #endregion

    }
}
