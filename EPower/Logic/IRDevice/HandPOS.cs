﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Logic.IRDevice
{
    /// <summary>
    /// Hand POS Device
    /// Platform   : Linux 
    /// Communicate: USB Serial Port
    /// Models     : H9300, H9200
    /// </summary>
    class HandPOS : IRDevice
    {
        #region Internal Method
        /// <summary>
        /// Open the specified port, and detects the handheld is connected to this port
        /// </summary>
        /// <param name="port">The port current handset connected to serial port, such as a serial one, two serial two.</param>
        /// <returns>
        /// successful return to the port number (PORT value)
        /// Failure to return the parameters to see the list of errors.
        /// </returns>
        [DllImport("hspos.dll")]
        public static extern Statu openport(int port);

        /// <summary>
        /// Close the open port. 
        /// Used in conjunction with openport.
        /// </summary>
        [DllImport("hspos.dll")]
        public static extern void closeport();


        /// <summary>
        /// This function can download the file to the handheld.
        /// </summary>
        /// <remarks>To transfer a file called [filename] file from the specified port to handheld.</remarks>
        /// <param name="port">Port connection for the handset port.</param>
        /// <param name="filename">[filename] is a string variable that contains the file path.</param>
        /// <returns>returns 0 on success\nFailure to return the parameters to see the list of errors.</returns>
        [DllImport("hspos.dll")]
        public static extern Statu downfile(int port, string filename);

        /// <summary>
        /// Upload handheld files, must make sure that you want to upload files exist in the inside of the handset, otherwise it will return an error. Also note that the current directory of the PC and the file name, otherwise it will be deleted.
        /// </summary>
        /// <remarks>Handheld files to upload a file called [filename] from the specified port to a PC</remarks>
        /// <param name="port">port connected to the handset.</param>
        /// <param name="filename">string variable, because the handset does not have directory information, so the [filename] can not contain the file path.</param>
        /// <returns>returns 0 on success\nFailure to return the parameters to see the list of errors.</returns>
        [DllImport("hspos.dll")]
        public static extern int upfile(int port, string filename);


        /// <summary>
        /// Look for a file called [filename] exists in the handset.
        /// </summary> 
        /// <param name="port">port connected to the handset.</param>
        /// <param name="filename">string variable, because the handset does not have directory information, so the [filename] can not contain the file path.</param>
        /// <returns>returns 0 on success in handhelds\nFailure to return the parameters to see the list of errors.</returns>
        [DllImport("hspos.dll")]
        public static extern Statu findfile(int port, string filename);

        /// <summary>
        /// handheld file deleted a file called [filename].
        /// </summary>
        /// <param name="port">port connected to the handset.</param>
        /// <param name="filename">string variable, because the handset does not have directory information, so the [filename] can not contain the file path.</param>
        /// <returns>returns 0 on success\nFailure to return the parameters to see the list of errors.</returns>
        [DllImport("hspos.dll")]
        public static extern Statu deletefile(int port, string filename);

        /// <summary>
        /// Upload and change the file name
        /// </summary>
        /// <param name="port">connection port for the handset the file name in</param>
        /// <param name="fname">POS machine</param>
        /// <param name="fname2">the file name of the PC side, you can specify the path, but also can not specify the path to save the current directory when you do not specify</param>
        /// <returns>returns 0 on success</returns>
        [DllImport("hspos.dll")]
        public static extern Statu uptofile(int port, string fname, string fname2);

        /// <summary>
        /// downstream and renamed
        /// </summary>
        /// <param name="port">connection port for the handset</param>
        /// <param name="fname">file name of the PC side</param>
        /// <param name="filename">POS machines in the file name</param>
        /// <returns>returns 0 on success\nFailure to return the parameters to see the list of errors.</returns>
        [DllImport("hspos.dll")]
        public static extern Statu downtofile(int port, string fname, string filename);




        /// <summary>  
        /// Returns the file list information.  
        /// The function after the call the getlist (the port),the file allocation table of the dynamic library will be handheld storage for the the\n 
        /// filetmp.tmp temporary file in the current directory, file name and file size on this file, the format of the file length [4] ,the file name [11].
        /// File header information is defined,32BYTE  
        /// char dir_name [11]; file name 11BYTE
        /// U8 dir_attrib; file attributes 1BYTE
        /// The U8 dir_case; reservations 1BYTE
        /// U8 dir_crmonth; month 1BYTE
        /// U8 dir_crdate; day 1BYTE 
        /// The U16 dir_cryear; years 2BYTE
        /// The U16 dir_start; file to start the cluster 2BYTE
        /// The U32 dir_size; File length 4BYTE
        /// </summary>  
        /// <param name="port">port connected to the handset.</param>
        /// <returns>the successful return of the file number (> 0, &lt;128),\nFailure to return the corresponding error code.</returns>
        [DllImport("hspos.dll")]
        public static extern Statu getlist(int port);


        /// <summary>
        /// Set the progress bar. Is to provide secondary development of the user to display the current progress of the operation Opportunity. Specified as follows:
        /// 1:00 i_method parameters, the callback function must be valid, otherwise it will be illegal operations, the other in this function can not appear time-consuming operation of more than two seconds, for example, the messagebox such as modal dialog. Otherwise blocking the entire communication process will lead to time-out wrong.
        /// </summary> 
        /// <param name="i_method">
        /// 0: return 1. No progress; Parameter 2 had no effect.
        /// 1: return 1. The user must provide a callback function. This function play set up the role of their own progress bar progress. In the process of implementation of the dynamic library calls this function and pass this function the current progress (0-100). Parameter 2 that this function pointer, the calling convention of this function must be stdcall, the following prototype: void set_prog_val (unsigned int ui_progress). More specific applications please refer to the examples we provide.
        /// </param>
        /// <param name="hwnd_proc"></param>
        /// <returns>
        /// returns 0 on success
        /// Failure to return the parameters to see the list of errors.
        /// </returns>
        [DllImport("hspos.dll")]
        public static extern int set_progress(int i_method, IntPtr hwnd_proc);

        /// <summary>
        /// Set handset date and time
        /// </summary>
        /// <param name="port">port connected to the handset.</param>
        /// <returns>
        /// returns 0 on success
        /// Failure to return the parameters to see the list of errors.
        /// </returns>
        [DllImport("hspos.dll")]
        public static extern int downsystime(int port);

        /// <summary>
        /// Upload machine from the specified port number to a string to the PC.
        /// If you are using a programming language string passed is difficult, you can use the function int getnumfile (int port); the role of this function with getmno, but there is no string functions, function machine number to save to the current directory a file named the Machno the file.
        /// </summary>
        /// <param name="port">port connected to the handset.</param>
        /// <param name="mno">a string variable to return the machine number.</param>
        /// <returns>returns 0 on success
        /// Failure to return the parameters to see the list of errors.
        /// </returns>
        [DllImport("hspos.dll")]
        public static extern int getmno(int port, ref string mno);

        /// <summary>
        /// Upload machine number string from the specified port to the PC, and saved in the current directory Machno file.
        /// </summary>
        /// <param name="port">connected to the handset port.</param>
        /// <returns>
        /// returns 0 on success
        /// Failure to return the parameters to see the list of errors.
        /// </returns>
        [DllImport("hspos.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern Statu getnumfile(int port);

        /// <summary>
        /// Number and name of the role: to change the handset operator
        /// you can see in the F2 menu  system  machine information download the information is correct.
        /// </summary>
        /// <param name="port">connected to the handset port.</param>
        /// <param name="mno">operator number, up to 6 bytes</param>
        /// <param name="mname">operator name up to 10 bytes</param>
        /// <returns>
        /// returns 0 on success
        /// Failure to return the parameters to see the list of errors </returns>
        [DllImport("hspos.dll")]
        public static extern int down_userinfo(int port, string mno, string mname);


        /**
         * ,
Call parameters: port, 
          Mno, 
          Mname, 
Return: 
         */
        /// <summary>
        /// Upload a handset's number and name of the operator
        /// </summary>
        /// <param name="port">connected to the handset port.</param>
        /// <param name="mno">operator number, up to 6 bytes</param>
        /// <param name="mname">operator name up to 10 bytes</param>
        /// <returns>
        /// returns 0 on success
        /// Failure to return the parameters to see the list of errors
        /// </returns>
        [DllImport("hspos.dll")]
        public static extern int up_userinfo(int port, string mno, string mname);

        /// <summary>
        /// To make the handset to exit the communication status
        /// </summary>
        /// <param name="port">connected to the handset port.</param>
        /// <returns>
        /// returns 0 on success
        /// Failure to return the parameters to see the list of errors
        /// </returns>
        [DllImport("hspos.dll")]
        public static extern int pos_exitcomm(int port);

        /// <summary>
        /// Communication return value
        /// </summary>
        public enum Statu
        {
            /// <summary>
            ///  command is executed normally
            /// </summary>
            Success = 0,

            Invalid_Data = 101,
            /// <summary>
            /// Upload a database successfully
            /// </summary>
            UpDB = 106,
            /// <summary>
            /// download database success
            /// </summary>
            DowDB = 107,
            /// <summary>
            /// files open error
            /// </summary>
            FileOpenErr = 108,
            Memory_Error = 109,
            /// <summary>
            /// communication transmission errors Normal termination of 
            /// </summary>
            CommTranErr = 110,
            /// <summary>
            /// communications
            /// </summary>
            Comm = 111,
            /// <summary>
            /// communications receiver error
            /// </summary> 
            CommRecErr = 112,

            /// <summary>
            /// to transfer files greater than 64K
            /// </summary> 
            TranF64K = 113,

            /// <summary>
            /// invalid file size
            /// </summary> 
            InvalFSize = 114,

            /// <summary>
            /// invalid parameters
            /// </summary> 
            InvalPara = 115,

            /// <summary>
            /// downstream process has been completed
            /// </summary> 
            DowProCom = 116,

            /// <summary>
            /// to delete completed
            /// </summary> 
            DelComp = 117,

            /// <summary>
            /// serial port is set up
            /// </summary> 
            PortSet = 118,


            /// <summary>
            /// handheld no response
            /// </summary>
            HandNoResponse = 119,
            /// <summary>
            /// communications timeout
            /// </summary>
            CommTimeout = 120,

            /// <summary>
            /// file format error
            /// </summary>
            FFormatErr = 123,

            /// <summary>
            /// Command operation failed
            /// </summary>
            CommOperFaild = 200,

            /// <summary>
            /// file not found
            /// </summary>
            FNotFound = 201,

            /// <summary>
            /// file delete failure
            /// </summary>
            FDelErr = 202,

            /// <summary>
            /// port open error
            /// </summary>
            PortOpenErr = 203
        }
        #endregion

        #region External Method
        Statu _ComStatus;
        Dictionary<Statu, string> Message = new Dictionary<Statu, string>();
        const string PublishVersion = "|Version: 2.9|";
        const string Machno = "Machno";
        const string FileList = "filetmp.tmp";
        const string SysExecute = "EPower.bin";
        const string CompanyName = "company.dat";

        const string MeterFilePath = "meter.txt";
        const string BoxFilePath = "box.txt";
        const string UsageFilePath = "usage.txt";
        const string UnknownMeterFilePath = "unknown.txt";



        public HandPOS()
            : base()
        {
            Message[Statu.Success] = "Success.";
            Message[Statu.Invalid_Data] = "Invalid data.";
            Message[Statu.UpDB] = "Upload a database successfully.";
            Message[Statu.DowDB] = "Download database is successfully.";
            Message[Statu.FileOpenErr] = "File Open Error.";
            Message[Statu.Memory_Error] = "Memory errors.";
            Message[Statu.CommTranErr] = "Communication transmission errors.";
            Message[Statu.Comm] = "Corresponding normal termination.";
            Message[Statu.CommRecErr] = "Communications receiver error.";
            Message[Statu.TranF64K] = "Transfer files larger than 64K.";
            Message[Statu.InvalFSize] = "Invalid file size.";
            Message[Statu.InvalPara] = "Invalid parameters.";
            Message[Statu.DowProCom] = "Downstream process has been completed.";
            Message[Statu.DelComp] = "Delete completed.";
            Message[Statu.PortSet] = "Serial port settings is complete.";
            Message[Statu.HandNoResponse] = "Handheld is not responding.";
            Message[Statu.CommTimeout] = "Communication timeout.";
            Message[Statu.FFormatErr] = "File format error.";
            Message[Statu.CommOperFaild] = "Command operation failed.";
            Message[Statu.FNotFound] = "File not found.";
            Message[Statu.FDelErr] = "File Delete failed.";
            Message[Statu.PortOpenErr] = "Port open error.";
        }


        void BcdToAscii(byte[] SourBuff, char[] DentBuff, int RecvCnt)
        {
            int i, j;
            for (i = 0, j = RecvCnt - 1; j >= 0; i += 2, j--)
            {
                DentBuff[i] = (char)(SourBuff[j] >> 4);
                DentBuff[i] += (char)0x30;
                DentBuff[i + 1] = (char)(SourBuff[j] & 0xf);
                DentBuff[i + 1] += (char)0x30;
                if (DentBuff[i] >= 0x3A && DentBuff[i] <= 0X3F) DentBuff[i] += (char)7;
                if (DentBuff[i + 1] >= 0X3A && DentBuff[i + 1] <= 0X3F) DentBuff[i + 1] += (char)7;
            }
        }

        int BcdToInt(byte[] SourBuff, int RecvCnt)
        {
            int i, j;
            char[] DentBuff = new char[12];
            for (i = 0, j = RecvCnt - 1; j >= 0; i += 2, j--)
            {
                DentBuff[i] = (char)(SourBuff[j] >> 4);
                DentBuff[i] += (char)0x30;
                DentBuff[i + 1] = (char)(SourBuff[j] & 0xf);
                DentBuff[i + 1] += (char)0x30;
                if (DentBuff[i] >= 0x3A && DentBuff[i] <= 0X3F) DentBuff[i] += (char)7;
                if (DentBuff[i + 1] >= 0X3A && DentBuff[i + 1] <= 0X3F) DentBuff[i + 1] += (char)7;
            }
            string hex = "";

            for (j = 0; j < i; j++)
            {
                hex += DentBuff[j];
            }
            return int.Parse(hex, NumberStyles.HexNumber);
        }

        static byte[] IntToBCD(int input)
        {
            byte[] bcd = new byte[] {
                (byte)(input>> 8),
                (byte)(input& 0x00FF)
            };
            return bcd;
        }
        /*
        public override bool GetFile(BackupType backup)
        {
            bool res = false;
            _ComStatus = findfile(this.COM, this._DDSb);

            if (_ComStatus != Statu.Success)
            {
                if (backup == BackupType.BeforeSFTD)
                {
                    return true;
                }
                throw new Exception(Message[_ComStatus]);
            }

            _ComStatus = uptofile(this.COM, this._DDSb, Properties.Settings.Default.IR_FILE_DB_LOCAL);
            if (_ComStatus != Statu.Success)
            {
                throw new Exception(Message[_ComStatus]);
            } 
            string fileBackup = string.Format("{0}{1}{2:yyyyMMddhhmmss}.dbf", backup.ToString(), this.ProductID, DBDataContext.Db.GetSystemDate());
            File.Copy(Properties.Settings.Default.IR_FILE_DB_LOCAL, Path.Combine(this.PathBackup, fileBackup), true);
            res = true;
            return res;
        }
        */
        public override List<TMP_IR_USAGE> GetFile(BackupType backup)
        {
            var res = base.GetFile(backup);
            _ComStatus = findfile(this.COM, UsageFilePath);

            if (_ComStatus != Statu.Success)
            {
                if (backup == BackupType.BeforeSFTD)
                {
                    return res;
                }
                throw new Exception(Message[_ComStatus]);
            }

            _ComStatus = uptofile(this.COM, UsageFilePath, Path.Combine(_TmpPath, UsageFilePath));
            if (_ComStatus != Statu.Success)
            {
                throw new Exception(Message[_ComStatus]);
            }
            string backupdate = DBDataContext.Db.GetSystemDate().ToString("yyyyMMddhhmmss");
            string fileBackup = Path.Combine(this.PathBackup, string.Format("{0}{1}{2}usage.txt", backup.ToString(), this.ProductID, backupdate));
            File.Copy(Path.Combine(_TmpPath, UsageFilePath), fileBackup, true);

            _ComStatus = uptofile(this.COM, UnknownMeterFilePath, Path.Combine(_TmpPath, UnknownMeterFilePath));
            if (_ComStatus == Statu.Success)
            {
                var unkonwBackup = string.Format("{0}{1}{2}unknown.txt", backup.ToString(), this.ProductID, backupdate);
                File.Copy(Path.Combine(_TmpPath, UnknownMeterFilePath), Path.Combine(this.PathBackup, unkonwBackup), true);
            }
            /*
             * Extract usages. 
             */
            var rows = HandPOSMetaData.ReadFromFile<Usage>(fileBackup).Where(x => x.IS_COLLECT_USAGE.Contains('1')).ToList();
            res = (from row in rows
                   let usage = 0.0

                   select new TMP_IR_USAGE
                   {
                       METER_CODE = new string(row.METER_CODE),
                       END_USAGE = DataHelper.ParseToDecimal(new string(row.END_USAGE).Trim().IndexOf('\0') > -1 ? new string(row.END_USAGE).Trim().Substring(0, new string(row.END_USAGE).IndexOf('\0'))
                            : new string(row.END_USAGE).Trim()),
                       BOX_CODE = new string(row.BOX_CODE),
                       IS_DIGITAL = DataHelper.ParseToBoolean(new string(row.IS_DIGITAL).Trim()),
                       IS_READ_IR = DataHelper.ParseToBoolean(new string(row.IS_READ_IR).Trim()),
                       IS_METER_RENEW_CYCLE = DataHelper.ParseToDecimal(new string(row.END_USAGE).Trim().IndexOf('\0') > -1 ? new string(row.END_USAGE).Trim().Substring(0, new string(row.END_USAGE).IndexOf('\0'))
                            : new string(row.END_USAGE).Trim()) < DataHelper.ParseToDecimal(new string(row.START_USAGE).Trim())
                   }).ToList();
            return res;
        }

        public override string GetID()
        {
            _ComStatus = getnumfile(this.COM);
            if (_ComStatus == Statu.Success)
            {
                this.ProductID = File.ReadAllText(Machno);
            }
            else
            {
                throw new Exception(Message[_ComStatus]);
            }
            return this.ProductID;
        }

        private Statu SendFile(string PC_File, string POS_File)
        {
            Statu sendState;
            //find file before delete.
            sendState = findfile(this.COM, POS_File);
            if (sendState == Statu.Success)
            {
                //delete file if exists 
                sendState = deletefile(this.COM, POS_File);
            }
            sendState = downtofile(this.COM, PC_File, POS_File);
            return sendState;
        }

        /*
        public override bool SendFile()
        {
            bool res = false;
            _ComStatus = SendFile(Path.Combine(_TmpPath, CompanyName), CompanyName);
            if (_ComStatus != Statu.Success)
            {
                throw new Exception(Message[_ComStatus]);
            }

            _ComStatus = SendFile(Path.Combine(this._TmpPath, this._DBInfo), this._DBInfo);
            if (_ComStatus != Statu.Success)
            {
                throw new Exception(Message[_ComStatus]);
            }

            _ComStatus = SendFile(Path.Combine(this._TmpPath, this._DDSb), this._DDSb);

            if (_ComStatus != Statu.Success)
            {
                throw new Exception(Message[_ComStatus]);
            }

            _ComStatus = SendFile(Path.Combine(this._TmpPath, this._PRICE), this._PRICE);

            if (_ComStatus != Statu.Success)
            {
                throw new Exception(Message[_ComStatus]);
            }

            res = true;
            return res;
        }*/

        public override bool SendFile()
        {
            bool res = false;
            _ComStatus = SendFile(Path.Combine(_TmpPath, UsageFilePath), UsageFilePath);
            if (_ComStatus != Statu.Success)
            {
                throw new Exception(Message[_ComStatus]);
            }

            _ComStatus = SendFile(Path.Combine(this._TmpPath, MeterFilePath), MeterFilePath);
            if (_ComStatus != Statu.Success)
            {
                throw new Exception(Message[_ComStatus]);
            }

            _ComStatus = SendFile(Path.Combine(this._TmpPath, BoxFilePath), BoxFilePath);

            if (_ComStatus != Statu.Success)
            {
                throw new Exception(Message[_ComStatus]);
            }

            _ComStatus = SendFile(Path.Combine(this._TmpPath, UnknownMeterFilePath), UnknownMeterFilePath);

            if (_ComStatus != Statu.Success)
            {
                throw new Exception(Message[_ComStatus]);
            }

            res = true;
            return res;
        }


        /// <summary>  
        /// Returns the file list information.  
        /// The function after the call the getlist (the port),the file allocation table of the dynamic library will be handheld storage for the the\n 
        /// filetmp.tmp temporary file in the current directory, file name and file size on this file, the format of the file length [4] ,the file name [11].
        /// File header information is defined,32BYTE  
        /// char dir_name [11]; file name 11BYTE
        /// U8 dir_attrib; file attributes 1BYTE
        /// The U8 dir_case; reservations 1BYTE
        /// U8 dir_crmonth; month 1BYTE
        /// U8 dir_crdate; day 1BYTE 
        /// The U16 dir_cryear; years 2BYTE
        /// The U16 dir_start; file to start the cluster 2BYTE
        /// The U32 dir_size; File length 4BYTE
        /// </summary>  
        /// <param name="port">port connected to the handset.</param>
        /// <returns>the successful return of the file number (> 0, &lt;128),\nFailure to return the corresponding error code.</returns>
        public override bool CheckVersion()
        {

            #region Check File List in Device
            /*_ComStatus = getlist(this.COM);
            if (_ComStatus <= 0)
            {
                throw new Exception(Message[_ComStatus]);
            }
            //filetmp.tmp
            byte[] buff = File.ReadAllBytes(FileList);



            string filename;
            byte[] buffSize = new byte[4],
                buffName = new byte[8],
                buffEx = new byte[3];

            int size = 0;
            List<FileInfo> files = new List<FileInfo>();

            for (int i = 0; i < buff.Length; i += 32)
            {
                Array.Copy(buff, i + 0, buffName, 0, 8);
                Array.Copy(buff, i + 8, buffEx, 0, 3);
                Array.Copy(buff, i + 28, buffSize, 0, 4);
                filename = Encoding.ASCII.GetString(buffName).Trim();
                filename += "." + Encoding.ASCII.GetString(buffEx);
                size = BcdToInt(buffSize, 4);
            }*/
            #endregion

            //File no found
            _ComStatus = findfile(this.COM, FileSys);
            if (_ComStatus != Statu.Success)
            {
                return Update();
            }

            _ComStatus = uptofile(this.COM, FileSys, FileSys);
            if (_ComStatus != Statu.Success)
            {
                return Update();
            }

            string buff = File.ReadAllText(FileSys, Encoding.ASCII);

            if (!buff.Contains(PublishVersion))
            {
                return Update();
            }

            ProductVersion = PublishVersion;
            return true;
        }

        private bool Update()
        {
            Runner.Instance.Text = string.Format(Resources.MS_UPDATING_APPLICATION_IN_DEVICE, string.Empty, string.Empty);
            if (!File.Exists(Path.Combine(_TmpPath, SysExecute)))
            {
                throw new Exception(string.Concat(Resources.MS_DEVICE_NEED_TO_UPDATE, "\n", Resources.MS_TAMPLATE_DEVICE_DATABASE_NOT_FOUND));
            }

            _ComStatus = SendFile(Path.Combine(_TmpPath, SysExecute), SysExecute);
            if (_ComStatus != Statu.Success)
            {
                throw new Exception(string.Concat(Resources.MS_DEVICE_NEED_TO_UPDATE, "\n", Message[_ComStatus]));
            }

            File.WriteAllText(Path.Combine(_TmpPath, FileSys), PublishVersion, Encoding.ASCII);

            _ComStatus = SendFile(Path.Combine(_TmpPath, FileSys), FileSys);
            if (_ComStatus != Statu.Success)
            {
                throw new Exception(string.Concat(Resources.MS_DEVICE_NEED_TO_UPDATE, "\n", Message[_ComStatus]));
            }

            ProductVersion = PublishVersion;
            return true;
        }


        public override bool Test()
        {
            this._ComStatus = openport(this.COM);
            if ((int)this._ComStatus > 100)
            {
                throw new Exception(Message[(Statu)this.COM]);
            }
            this.COM = (int)this._ComStatus;
            closeport();
            return true;
        }

        public override int GetUsage(List<TBL_BILLING_CYCLE> mCycle, List<TBL_AREA> mArea, ref string month, bool newDb = true)
        {

            int i = 1;

            var tmpIr = (from u in base.GetMonthlyUsages(mCycle, mArea)
                         orderby u.POLE_CODE, u.BOX_CODE, u.METER_CODE, u.AREA_CODE
                         select new
                         {
                             ROW_NO = i++,
                             u.POLE_CODE,
                             u.BOX_CODE,
                             u.METER_CODE,
                             u.AREA_CODE,
                             START_USAGE = (int)u.START_USAGE,
                             END_USAGE = (int)u.END_USAGE,
                             u.MULTIPLIER,
                             LAST_TOTAL_USAGE = (int)u.LAST_TOTAL_USAGE,
                             IS_METER_RENEW_CYCLE = Convert.ToInt16(u.IS_METER_RENEW_CYCLE),
                             IS_COLLECT_USAGE = "0",
                             IS_DIGITAL = Convert.ToInt16(u.IS_DIGITAL),
                             IS_READ_IR = "0"
                         }).ToList();

            var usages = from u in tmpIr
                         orderby u.POLE_CODE, u.BOX_CODE, u.METER_CODE, u.AREA_CODE
                         select new Usage()
                         {
                             ROW_NO = u.ROW_NO.ToString().ToCharArray(),
                             POLE_CODE = u.POLE_CODE.ToCharArray(),
                             BOX_CODE = u.BOX_CODE.ToCharArray(),
                             METER_CODE = u.METER_CODE.ToCharArray(),
                             AREA_CODE = u.AREA_CODE.ToCharArray(),
                             START_USAGE = ((int)u.START_USAGE).ToString().ToCharArray(),
                             END_USAGE = ((int)u.END_USAGE).ToString().ToCharArray(),
                             MULTIPLIER = u.MULTIPLIER.ToString().ToCharArray(),
                             LAST_TOTAL_USAGE = ((int)u.LAST_TOTAL_USAGE).ToString().ToCharArray(),
                             IS_METER_RENEW_CYCLE = Convert.ToInt16(u.IS_METER_RENEW_CYCLE).ToString().ToCharArray(),
                             IS_COLLECT_USAGE = ("0").ToCharArray(),
                             IS_DIGITAL = Convert.ToInt16(u.IS_DIGITAL).ToString().ToCharArray(),
                             IS_READ_IR = ("0").ToCharArray()
                         };

            var meters = from m in tmpIr
                         orderby m.METER_CODE
                         select new Meter()
                         {
                             METER_CODE = m.METER_CODE.ToCharArray(),
                             ROW_NO = m.ROW_NO.ToString().ToCharArray()
                         };

            var boxies = from p in tmpIr
                         group p by p.BOX_CODE into gp
                         orderby gp.Key
                         select new Box()
                         {
                             BOX_CODE = gp.Key.ToCharArray(),
                             ROW_NO = gp.Min(x => x.ROW_NO).ToString().ToCharArray()
                         };

            HandPOSMetaData.WrithToFile(Path.Combine(_TmpPath, UsageFilePath), usages.ToList());
            HandPOSMetaData.WrithToFile(Path.Combine(_TmpPath, MeterFilePath), meters.ToList());
            HandPOSMetaData.WrithToFile(Path.Combine(_TmpPath, BoxFilePath), boxies.ToList());
            File.Create(Path.Combine(_TmpPath, UnknownMeterFilePath)).Close();


            var mC = new List<string>();
            foreach (var c in mCycle)
            {
                mC.Add(Method.GetNextBillingMonth(c.CYCLE_ID).Month.ToString("00"));
            }
            month = string.Join(",", mC.Distinct().ToArray());

            return usages.Count();
        }

        public override bool SaveUsage(int intDeviceID, int intCollectorID)
        {
            bool blnReturn = false;
            TBL_DEVICE objDevice = DBDataContext.Db.TBL_DEVICEs.Where(x => x.DEVICE_ID == intDeviceID).FirstOrDefault();
            try
            {
                var usages = HandPOSMetaData.ReadFromFile<Usage>(Path.Combine(_TmpPath, UsageFilePath)).Where(x => x.IS_COLLECT_USAGE.Contains('1')).ToList();
                var unknowns = HandPOSMetaData.ReadFromFile<Usage>(Path.Combine(_TmpPath, UnknownMeterFilePath));

                if (usages.Count == 0)
                {
                    throw new MsgBox.MessageException("ពុំមានអតិថិជនស្រង់អំណានក្នុងឧបករណ៍ទេ!");
                }

                Runner.Instance.Text = string.Format(Resources.MS_SAVING_DATA, "");
                Application.DoEvents();
                DBDataContext.Db.ExecuteCommand("TRUNCATE TABLE TMP_IR_USAGE");
                string str = Runner.Instance.Text;


                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    int k = 0;
                    var n = usages.Count;
                    // Insert meter's usage.
                    foreach (var row in usages)
                    {
                        var obj = new TMP_IR_USAGE();
                        obj.METER_CODE = new string(row.METER_CODE);
                        var end = new string(row.END_USAGE).Trim();
                        if (end.IndexOf('\0') > -1)
                        {
                            end = end.Substring(0, end.IndexOf('\0'));
                        }

                        obj.END_USAGE = DataHelper.ParseToDecimal(end);
                        obj.BOX_CODE = new string(row.BOX_CODE);
                        obj.IS_DIGITAL = DataHelper.ParseToBoolean(new string(row.IS_DIGITAL).Trim());
                        obj.IS_METER_RENEW_CYCLE = obj.END_USAGE < DataHelper.ParseToDecimal(new string(row.START_USAGE).Trim());
                        obj.IS_READ_IR = DataHelper.ParseToBoolean(new string(row.IS_READ_IR).Trim());
                        obj.REGISTER_CODE = Method.GetMETER_REGCODE(obj.METER_CODE, obj.IS_DIGITAL);
                        obj.TAMPER_STATUS = "00";

                        DBDataContext.Db.TMP_IR_USAGEs.InsertOnSubmit(obj);
                        k++;
                        if (k % 100 == 0 || k % n == 0)
                        {
                            DBDataContext.Db.SubmitChanges();

                            Runner.Instance.Text = string.Format(str + "\nចំនួនអតិថិជន៖ {0} ({1:#.##}%)", k, (float)(k * 100 / n));
                            Application.DoEvents();
                        }
                    }

                    // Insert unknown meter.
                    var un = from u in unknowns
                             group u by new { METER_CODE = new string(u.METER_CODE) } into gu
                             select new
                             {
                                 ROW_NO = gu.Min(x => new string(x.ROW_NO).Trim()),
                                 POLE_CODE = gu.Min(x => new string(x.POLE_CODE).Trim()),
                                 BOX_CODE = gu.Min(x => new string(x.BOX_CODE).Trim()),
                                 METER_CODE = gu.Key.METER_CODE,
                                 AREA_CODE = gu.Min(x => new string(x.AREA_CODE).Trim()),
                                 START_USAGE = gu.Min(x => new string(x.START_USAGE).Trim()),
                                 END_USAGE = gu.Max(x => new string(x.END_USAGE).Trim()),
                                 MULTIPLIER = gu.Min(x => new string(x.MULTIPLIER).Trim()),
                                 LAST_TOTAL_USAGE = gu.Min(x => new string(x.LAST_TOTAL_USAGE).Trim()),
                                 IS_METER_RENEW_CYCLE = gu.Min(x => new string(x.IS_METER_RENEW_CYCLE).Trim()),
                                 IS_COLLECT_USAGE = gu.Min(x => new string(x.IS_COLLECT_USAGE).Trim()),
                                 IS_DIGITAL = gu.Min(x => new string(x.IS_DIGITAL).Trim()),
                                 IS_READ_IR = gu.Min(x => new string(x.IS_READ_IR).Trim())
                             };
                    foreach (var row in un)
                    {
                        var obj = new TMP_IR_USAGE();
                        obj.METER_CODE = row.METER_CODE;
                        obj.END_USAGE = DataHelper.ParseToDecimal(row.END_USAGE);
                        obj.BOX_CODE = row.BOX_CODE;
                        obj.IS_DIGITAL = DataHelper.ParseToBoolean(row.IS_DIGITAL);
                        obj.IS_METER_RENEW_CYCLE = obj.END_USAGE < DataHelper.ParseToDecimal(row.START_USAGE);
                        obj.IS_READ_IR = DataHelper.ParseToBoolean(row.IS_READ_IR);
                        obj.REGISTER_CODE = Method.GetMETER_REGCODE(obj.METER_CODE, obj.IS_DIGITAL);
                        DBDataContext.Db.TMP_IR_USAGEs.InsertOnSubmit(obj);
                    }
                    DBDataContext.Db.SubmitChanges();

                    this.RunIRUsage(objDevice.DEVICE_ID, intCollectorID);
                    this.TotalCollectCustomer = usages.Count();

                    tran.Complete();
                }
                DBDataContext.Db = null;
                return true;

            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /*
        public override int GetUsage(List<TBL_BILLING_CYCLE> cycle, List<TBL_AREA> area, ref string month,bool newDb=true)
        {
            if (File.Exists(Path.Combine(_TmpPath, _TmpHSPOSDDSb))
                && File.Exists(Path.Combine(_TmpPath, _TmpPRICE))
                && File.Exists(Path.Combine(_TmpPath,_TmpInfo)))

            {
                File.WriteAllText(Path.Combine(_TmpPath, CompanyName), DBDataContext.Db.TBL_COMPANies.FirstOrDefault().COMPANY_NAME_EN);
                File.Copy(Path.Combine(_TmpPath, _TmpHSPOSDDSb), Path.Combine(_TmpPath, _DDSb), true);
                File.Copy(Path.Combine(_TmpPath, _TmpPRICE), Path.Combine(_TmpPath, _PRICE), true);
                File.Copy(Path.Combine(_TmpPath, _TmpInfo), Path.Combine(_TmpPath, _DBInfo), true);
            }
            else
            {
                throw new Exception(Resources.MsgTmpDBFNotFound);
            }

            DataTable dtUsage = new DataTable();
             
            foreach (var item in cycle)
            {
                DateTime monthcycle = Logic.Method.GetNextBillingMonth(int.Parse(item.CYCLE_ID.ToString()));
                var reader = from c in DBDataContext.Db.TBL_CUSTOMERs
                             join a in DBDataContext.Db.TBL_AREAs.Where(x=>area.Contains(x)) on c.AREA_ID equals a.AREA_ID
                             join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on c.CUSTOMER_ID equals cm.CUSTOMER_ID into tcm
                             from tmpcm in tcm.DefaultIfEmpty().Where(x => x.IS_ACTIVE)
                             join m in DBDataContext.Db.TBL_METERs on tmpcm.METER_ID equals m.METER_ID into tm
                             from tmpm in tm.DefaultIfEmpty()
                             join d in
                                 (from cdis in DBDataContext.Db.TBL_CUSTOMER_DISCOUNTs
                                  join dis in DBDataContext.Db.TBL_DISCOUNTs on cdis.DISCOUNT_ID equals dis.DISCOUNT_ID
                                  where dis.IS_ACTIVE && cdis.IS_ACTIVE
                                  select new
                                  {
                                      cdis.CUSTOMER_ID,
                                      DISCOUNT_USAGE = dis.IS_PERCENTAGE ? 0 : dis.DISCOUNT,
                                      DISCOUNT_PERCENTAGE = dis.IS_PERCENTAGE ? dis.DISCOUNT : 0
                                  }) on c.CUSTOMER_ID equals d.CUSTOMER_ID into td
                             from tmpd in td.DefaultIfEmpty()
                             join ou in
                                 (from tou in
                                      (from us in DBDataContext.Db.TBL_USAGEs
                                       where us.USAGE_MONTH.Date == monthcycle.Date
                                       select new
                                       {
                                           us.CUSTOMER_ID,
                                           us.METER_ID,
                                           TOTAL_USAGE = us.MULTIPLIER * (us.IS_METER_RENEW_CYCLE
                                                            ? (Convert.ToInt32(new String('9', Convert.ToInt32(us.START_USAGE).ToString().Length))) - us.START_USAGE + 1 + us.END_USAGE
                                                            : us.END_USAGE - us.START_USAGE)
                                       })
                                  group tou by new { tou.CUSTOMER_ID, tou.METER_ID } into tmptou
                                  select new
                                  {
                                      tmptou.Key.CUSTOMER_ID,
                                      tmptou.Key.METER_ID,
                                      TOTAL_USAGE = tmptou.Sum(x => x.TOTAL_USAGE)
                                  }
                             ) on c.CUSTOMER_ID equals ou.CUSTOMER_ID into ou
                             from tmpou in ou.Where(x => x.METER_ID != tmpm.METER_ID).DefaultIfEmpty()
                             join u in
                                 (from us in DBDataContext.Db.TBL_USAGEs
                                  where us.USAGE_MONTH.Date == monthcycle.Date
                                  orderby us.USAGE_ID descending
                                  select new
                                  {
                                      us.CUSTOMER_ID,
                                      us.METER_ID,
                                      us.START_USAGE,
                                      us.MULTIPLIER,
                                      us.END_USAGE
                                  }) on new { c.CUSTOMER_ID, tmpcm.METER_ID } equals new { u.CUSTOMER_ID, u.METER_ID } into tu
                             from tmpu in tu.DefaultIfEmpty()
                             join o in
                                 (from us in DBDataContext.Db.TBL_USAGEs
                                  where us.USAGE_MONTH.Date == monthcycle.Date.AddMonths(-1)
                                  orderby us.USAGE_ID descending
                                  select new
                                  {
                                      us.CUSTOMER_ID,
                                      us.METER_ID,
                                      us.START_USAGE,
                                      us.END_USAGE
                                  }) on new { c.CUSTOMER_ID, tmpm.METER_ID } equals new { o.CUSTOMER_ID, o.METER_ID } into to
                             from tmpo in to.DefaultIfEmpty()
                             join i in
                                 (from ci in DBDataContext.Db.TBL_INVOICEs
                                  where ci.INVOICE_MONTH.Date == monthcycle.Date.AddMonths(-1)
                                  && ci.IS_SERVICE_BILL == false
                                  select new
                                  {
                                      ci.CUSTOMER_ID,
                                      ci.TOTAL_USAGE,
                                      DUE_AMOUNT = ci.SETTLE_AMOUNT - ci.PAID_AMOUNT
                                  }) on c.CUSTOMER_ID equals i.CUSTOMER_ID into ti
                             from tmpi in ti.DefaultIfEmpty()
                             where (c.STATUS_ID == (int)CustomerStatus.Active||c.STATUS_ID ==(int)CustomerStatus.Blocked)
                                  && c.IS_POST_PAID == true
                                  && tmpcm.IS_ACTIVE
                                  && c.BILLING_CYCLE_ID == item.CYCLE_ID 
                             orderby tmpm.METER_CODE ascending
                             select new
                             {
                                 tmpm.METER_CODE,
                                 END_USAGE = tmpu == null ? 0 : tmpu.END_USAGE,
                                 CONSTANT = -1,
                                 CUSTOMER_NAME = string.Concat(c.LAST_NAME, " ", c.FIRST_NAME),
                                 c.CUSTOMER_CODE,
                                 c.PRICE_ID,
                                 START_USAGE = tmpu == null ? (tmpo == null ? 0 : tmpo.END_USAGE) : tmpu.START_USAGE,
                                 LAST_USAGE = tmpi == null ? 0 : tmpi.TOTAL_USAGE,
                                 OLD_METER_USAGE = tmpou == null ? 0 : tmpou.TOTAL_USAGE,
                                 MULITPLIER = tmpu == null ? (tmpm == null ? 1 : tmpm.MULTIPLIER) : tmpu.MULTIPLIER,
                                 TOTAL_USAGE = 0,
                                 DISCOUNT_USAGE = tmpd == null ? 0 : tmpd.DISCOUNT_USAGE,
                                 DISCOUNT_PERCENTAGE = tmpd == null ? 0 : tmpd.DISCOUNT_PERCENTAGE,
                                 DUE_AMOUNT = tmpi == null ? 0 : tmpi.DUE_AMOUNT,
                                 TOTAL_AMOUNT = 0,
                                 tmpm.IS_DIGITAL
                             };

                if (dtUsage.Columns.Count == 0)
                {
                    dtUsage = reader._ToDataTable();
                }
                else
                {
                    dtUsage.Merge(reader._ToDataTable());
                }
                month = string.Concat(month, monthcycle.Date.Month.ToString("00"), ", ");
            }


            var price = from p in DBDataContext.Db.TBL_PRICEs
                        join pd in DBDataContext.Db.TBL_PRICE_DETAILs on p.PRICE_ID equals pd.PRICE_ID
                        where p.IS_ACTIVE
                        orderby p.PRICE_ID
                        select new
                        {
                            p.PRICE_ID,
                            pd.START_USAGE,
                            pd.END_USAGE,
                            pd.PRICE,
                            FIXED_AMOUNT = pd.IS_ALLOW_FIXED_AMOUNT ? pd.FIXED_AMOUNT : 0,
                            p.IS_STANDARD_RATING
                        };

            //get data from local file
            OleDbConnection dbConn = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Template\;Extended Properties=dBASE IV;");

            dbConn.Open();
            OleDbTransaction tran = dbConn.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);

            string strSQLVal = @"INSERT INTO DDSb
                                   (METER_CODE, 
                                    END_USAGE, 
                                    CONSTANT, 
                                    CUS_CODE, 
                                    PRICE_NO,
                                    CUS_NAME,   
                                    S_USAGE, 
                                    L_T_USAGE, 
                                    O_M_USAGE,
                                    MULTI, 
                                    T_USAGE, 
                                    DIS_USAGE, 
                                    DIS_AMOUNT, 
                                    DUE_AMOUNT, 
                                    T_AMOUNT, 
                                    IS_DIGITAL)
                             VALUES
                                   (@METER_CODE,
                                    @END_USAGE, 
                                    @CONSTANT, 
                                    @CUS_CODE,
                                    @PRICE_NO,     
                                    @CUS_NAME, 
                                    @MULTI, 
                                    @S_USAGE, 
                                    @L_T_USAGE, 
                                    @O_M_USAGE, 
                                    @T_USAGE, 
                                    @DIS_USAGE, 
                                    @DIS_AMOUNT, 
                                    @DUE_AMOUNT, 
                                    @T_AMOUNT, 
                                    @IS_DIGITAL);";

            var usag = from u in dtUsage.Select()
                       orderby u["METER_CODE"] ascending
                       select new
                       {
                           METER_CODE = u["METER_CODE"],
                           END_USAGE = u["END_USAGE"],
                           CONSTANT = u["CONSTANT"],
                           CUSTOMER_NAME = u["CUSTOMER_NAME"],
                           CUSTOMER_CODE = u["CUSTOMER_CODE"],
                           PRICE_ID = u["PRICE_ID"],
                           START_USAGE = u["START_USAGE"],
                           LAST_USAGE = u["LAST_USAGE"],
                           OLD_METER_USAGE = u["OLD_METER_USAGE"],
                           MULITPLIER = u["MULITPLIER"],
                           TOTAL_USAGE = u["TOTAL_USAGE"],
                           DISCOUNT_USAGE = u["DISCOUNT_USAGE"],
                           DISCOUNT_PERCENTAGE = u["DISCOUNT_PERCENTAGE"],
                           DUE_AMOUNT = u["DUE_AMOUNT"],
                           TOTAL_AMOUNT = u["TOTAL_AMOUNT"],
                           IS_DIGITAL = u["IS_DIGITAL"]
                       };
            int k = 1,total=usag.Count();
            foreach (var item in usag)
            {
                OleDbCommand cmd = new OleDbCommand(strSQLVal, dbConn, tran);
                cmd.Parameters.Add(new OleDbParameter("@METER_CODE", OleDbType.VarChar, 12) { Value = item.METER_CODE });
                cmd.Parameters.Add(new OleDbParameter("@END_USAGE", OleDbType.Single) { Value = item.END_USAGE });
                cmd.Parameters.Add(new OleDbParameter("@CONSTANT", OleDbType.Integer) { Value = item.CONSTANT });
                cmd.Parameters.Add(new OleDbParameter("@CUS_CODE", OleDbType.VarChar, 6) { Value = item.CUSTOMER_CODE });
                cmd.Parameters.Add(new OleDbParameter("@PRICE_NO", OleDbType.Integer) { Value = item.PRICE_ID });
                cmd.Parameters.Add(new OleDbParameter("@CUS_NAME", OleDbType.VarChar, 12) { Value = item.CUSTOMER_NAME });
                cmd.Parameters.Add(new OleDbParameter("@S_USAGE", OleDbType.Single) { Value = item.START_USAGE });
                cmd.Parameters.Add(new OleDbParameter("@L_T_USAGE", OleDbType.Single) { Value = item.LAST_USAGE });
                cmd.Parameters.Add(new OleDbParameter("@O_M_USAGE", OleDbType.Single) { Value = item.OLD_METER_USAGE });
                cmd.Parameters.Add(new OleDbParameter("@MULTI", OleDbType.Integer) { Value = item.MULITPLIER });
                cmd.Parameters.Add(new OleDbParameter("@T_USAGE", OleDbType.Single) { Value = item.TOTAL_USAGE });
                cmd.Parameters.Add(new OleDbParameter("@DIS_USAGE", OleDbType.Single) { Value = item.DISCOUNT_USAGE });
                cmd.Parameters.Add(new OleDbParameter("@DIS_AMOUNT", OleDbType.Single) { Value = item.DISCOUNT_PERCENTAGE });
                cmd.Parameters.Add(new OleDbParameter("@DUE_AMOUNT", OleDbType.Single) { Value = item.DUE_AMOUNT });
                cmd.Parameters.Add(new OleDbParameter("@T_AMOUNT", OleDbType.Single) { Value = item.TOTAL_AMOUNT });
                cmd.Parameters.Add(new OleDbParameter("@IS_DIGITAL", OleDbType.Boolean) { Value = item.IS_DIGITAL });
                cmd.ExecuteNonQuery();
                Runner.Instance.Text = string.Format(Properties.Resources.MsgDevicePrepairDB, k++  * 100 / total, "%"); 
            }

            strSQLVal = @"INSERT INTO PRICE
                                (PRICE_NO, 
                                S_START, 
                                E_START, 
                                AMOUNT, 
                                FIX_AMOUNT,
                                IS_STANDAR
                                )   
                            VALUES
                                (@PRICE_NO, 
                                 @S_START, 
                                 @E_START, 
                                 @AMOUNT, 
                                 @FIX_AMOUNT,
                                 @IS_STANDAR)";
            foreach (var item in price)
            {
                OleDbCommand cmd = new OleDbCommand(strSQLVal, dbConn, tran);
                cmd.Parameters.Add(new OleDbParameter("@PRICE_NO", OleDbType.Integer) { Value = item.PRICE_ID });
                cmd.Parameters.Add(new OleDbParameter("@S_START", OleDbType.Single) { Value = item.START_USAGE });
                cmd.Parameters.Add(new OleDbParameter("@E_START", OleDbType.Single) { Value = item.END_USAGE });
                cmd.Parameters.Add(new OleDbParameter("@AMOUNT", OleDbType.Single) { Value = item.PRICE });
                cmd.Parameters.Add(new OleDbParameter("@FIX_AMOUNT", OleDbType.Single) { Value = item.FIXED_AMOUNT });
                cmd.Parameters.Add(new OleDbParameter("@IS_STANDAR", OleDbType.Integer) { Value = item.IS_STANDARD_RATING });
                cmd.ExecuteNonQuery();
            }

            strSQLVal = @"INSERT INTO DBInfo
                             VALUES
                                   (@NO
                                   ,@DB_NAME
                                   ,@RECORD);";
            OleDbCommand cmdRec = new OleDbCommand(strSQLVal, dbConn, tran);
            cmdRec.Parameters.Add(new OleDbParameter("@NO", OleDbType.Single) { Value = 1 });
            cmdRec.Parameters.Add(new OleDbParameter("@DB_NAME", OleDbType.VarChar, 12) { Value = _DDSb });
            cmdRec.Parameters.Add(new OleDbParameter("@RECORD", OleDbType.Single) { Value = usag.Count() });
            cmdRec.ExecuteNonQuery();

            cmdRec = new OleDbCommand(strSQLVal, dbConn, tran);
            cmdRec.Parameters.Add(new OleDbParameter("@NO", OleDbType.Single) { Value = 2 });
            cmdRec.Parameters.Add(new OleDbParameter("@DB_NAME", OleDbType.VarChar, 12) { Value = _DDSc });
            cmdRec.Parameters.Add(new OleDbParameter("@RECORD", OleDbType.Single) { Value = usag.Count() });
            cmdRec.ExecuteNonQuery();

            tran.Commit();
            dbConn.Close();
            month  = month.TrimEnd(new char[] { ',', ' ' });
            return usag.Count();
        }
        */
        #endregion 
    }


    /// <summary>
    /// Since: May-2015
    /// By: Prak Serey
    /// Implement to generate data for Ir model H9300-H9200 to multiple meters in BOX.
    /// </summary>
    public abstract class HandPOSMetaData
    {
        public static Dictionary<string, int> BufferLenght = new Dictionary<string, int>()
        {
            {"ROW_NO",8},
            {"POLE_CODE",12},
            {"BOX_CODE",24},
            {"METER_CODE",12},
            {"AREA_CODE",12},
            {"START_USAGE",8},
            {"END_USAGE",8},
            {"MULTIPLIER",8},
            {"LAST_TOTAL_USAGE",8},
            {"IS_METER_RENEW_CYCLE",1},
            {"IS_COLLECT_USAGE",1},
            {"IS_DIGITAL",1},
            {"IS_READ_IR",1},
        };

        public void SetValue(char[] buffer)
        {
            var p = this.GetType().GetProperties();
            int len = 0;
            for (int i = 0; i < p.Length; i++)
            {
                var lBuffer = BufferLenght[p[i].Name];
                var val = buffer.Skip(len + i).Take(lBuffer).ToArray();
                len += lBuffer;
                p[i].SetValue(this, val, null);
            }
        }

        public static List<T> ReadFromFile<T>(string path, int line = 0) where T : HandPOSMetaData
        {
            StreamReader sr = null;
            try
            {
                List<T> ls = new List<T>();
                sr = new StreamReader(path);
                string row;
                while ((row = sr.ReadLine()) != null)
                {
                    T obj = (T)Activator.CreateInstance(typeof(T));
                    obj.SetValue(row.ToCharArray());
                    ls.Add(obj);
                }
                return ls; ;
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                }
            }
        }


        public static void WrithToFile<T>(string path, List<T> objs) where T : HandPOSMetaData
        {
            StreamWriter sw = null;
            try
            {
                var p = objs.FirstOrDefault().GetType().GetProperties();
                sw = new StreamWriter(path, false, Encoding.ASCII);

                foreach (var row in objs)
                {
                    for (int i = 0; i < p.Length; i++)
                    {
                        var lBuffer = BufferLenght[p[i].Name];
                        sw.Write((char[])p[i].GetValue(row, null));
                        if (i < p.Length - 1)
                        {
                            sw.Write(','); // (,)Comma separator
                        }
                    }
                    sw.WriteLine(); // Break line
                }
                sw.Flush();
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }
    }

    public class Usage : HandPOSMetaData
    {
        private char[] rowNo;
        private char[] poleCode;
        private char[] boxCode;
        private char[] meterCode;
        private char[] areaCode;
        private char[] startUsage;
        private char[] endUsage;
        private char[] multiplier;
        private char[] lastTotalUsage;
        private char[] isMeterRenewCycle;
        private char[] isCollectUsage;
        private char[] isDigital;
        private char[] isReadIr;

        public char[] ROW_NO
        {
            get
            {
                return rowNo;
            }
            set
            {
                int len = BufferLenght["ROW_NO"];
                rowNo = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
        public char[] POLE_CODE
        {
            get
            {
                return poleCode;
            }
            set
            {
                int len = BufferLenght["POLE_CODE"];
                poleCode = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
        public char[] BOX_CODE
        {
            get
            {
                return boxCode;
            }
            set
            {
                int len = BufferLenght["BOX_CODE"];
                boxCode = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
        public char[] METER_CODE
        {
            get
            {
                return meterCode;
            }
            set
            {
                int len = BufferLenght["METER_CODE"];
                meterCode = new string(value).PadLeft(len, '0').ToCharArray(0, len);
            }
        }
        public char[] AREA_CODE
        {
            get
            {
                return areaCode;
            }
            set
            {
                int len = BufferLenght["AREA_CODE"];
                areaCode = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
        public char[] START_USAGE
        {
            get
            {
                return startUsage;
            }
            set
            {
                int len = BufferLenght["START_USAGE"];
                startUsage = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
        public char[] END_USAGE
        {
            get
            {
                return endUsage;
            }
            set
            {
                int len = BufferLenght["END_USAGE"];
                endUsage = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
        public char[] MULTIPLIER
        {
            get
            {
                return multiplier;
            }
            set
            {
                int len = BufferLenght["MULTIPLIER"];
                multiplier = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
        public char[] LAST_TOTAL_USAGE
        {
            get
            {
                return lastTotalUsage;
            }
            set
            {
                int len = BufferLenght["LAST_TOTAL_USAGE"];
                lastTotalUsage = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
        public char[] IS_METER_RENEW_CYCLE
        {
            get
            {
                return isMeterRenewCycle;
            }
            set
            {
                int len = BufferLenght["IS_METER_RENEW_CYCLE"];
                isMeterRenewCycle = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
        public char[] IS_COLLECT_USAGE
        {
            get
            {
                return isCollectUsage;
            }
            set
            {
                int len = BufferLenght["IS_COLLECT_USAGE"];
                isCollectUsage = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
        public char[] IS_DIGITAL
        {
            get
            {
                return isDigital;
            }
            set
            {
                int len = BufferLenght["IS_DIGITAL"];
                isDigital = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
        public char[] IS_READ_IR
        {
            get
            {
                return isReadIr;
            }
            set
            {
                int len = BufferLenght["IS_READ_IR"];
                isReadIr = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
    }

    public class Box : HandPOSMetaData
    {
        private char[] boxCode;
        private char[] rowNo;
        public char[] BOX_CODE
        {
            get
            {
                return boxCode;
            }
            set
            {
                int len = BufferLenght["BOX_CODE"];
                boxCode = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
        public char[] ROW_NO
        {
            get
            {
                return rowNo;
            }
            set
            {
                int len = BufferLenght["ROW_NO"];
                rowNo = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
    }

    public class Meter : HandPOSMetaData
    {
        private char[] meterCode;
        private char[] rowNo;
        public char[] METER_CODE
        {
            get
            {
                return meterCode;
            }
            set
            {
                int len = BufferLenght["METER_CODE"];
                meterCode = new string(value).PadLeft(len, '0').ToCharArray(0, len);
            }
        }
        public char[] ROW_NO
        {
            get
            {
                return rowNo;
            }
            set
            {
                int len = BufferLenght["ROW_NO"];
                rowNo = new string(value).PadRight(len).ToCharArray(0, len);
            }
        }
    }
}
