﻿using System;
using System.Collections.Generic;
using System.Linq;
using SoftTech;
using SoftTech.Helper;

namespace EPower.Logic
{
    class AreaLogic
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="areas"></param>
        /// <returns>string of area id "1,2,3,4,5,6,7"</returns>
        public static string ConcatAreaID(IEnumerable<TBL_AREA> areas)
        {
            return string.Join(",", areas.Select(x => x.AREA_ID.ToString()).ToArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="areas"></param>
        /// <returns>string of area name "A01,B01,C01,E01,F01"</returns>
        public static string ConcatAreaCode(IEnumerable<TBL_AREA> areas)
        {
            return string.Join(",", areas.Select(x => x.AREA_CODE).ToArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="areaId">format "1,2,3,4,5,6,7"</param>
        /// <returns>List of 1,2,3,4,5,6,7</returns>
        public static List<int> ParseToIntAreaId(string areaId)
        {
            return areaId.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(DataHelper.ParseToInt).ToList();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="areaId">format "1,2,3,4,5,6,7"</param>
        /// <returns>List object</returns>
        public static List<TBL_AREA> AreasById(string areaId)
        {
            var listId = ParseToIntAreaId(areaId);
            return AreasById(listId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="areaId">format 1,2,3,4,5,6,7</param>
        /// <returns>List object</returns>
        public static List<TBL_AREA> AreasById(List<int> areaId)
        {
            return DBDataContext.Db.TBL_AREAs.Where(x => x.IS_ACTIVE && areaId.Count > 0 && areaId.Contains(x.AREA_ID)).OrderBy(x => x.AREA_CODE).ToList();
        }
    }
}
