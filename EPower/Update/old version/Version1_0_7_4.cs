﻿namespace EPower.Update
{
    /// <summary>
    /// 1. Delete Duplicate Record Capacity 22.0000 KVA
    /// 2. Update ReportCashDialy 
    ///		Add Total Invoice in ReportCashDialy.rpt
    ///	3. Fix When Multi User Print Invoice
    ///	    -Create Table TBL_PRINT_INVOICE & TBL_PRINT_INVOICE_DETAIL
    ///	    -Add New Utility
    ///	    -Update Store REPORT_INVOICE (Manualy)
    ///	4. Add REPORT_PREPAID_VOID_POWER
    ///	5. Update REPORT_CUSTOMER_NO_USAGE add START-END Usage
    ///	6. Update IR GF1100 to Versio 0.9
    /// </summary>
    /// <returns></returns>
     
    class Version1_0_7_4 : Version
    {        
        protected override string GetBatchCommand()
        {
            return @"
GO 
DECLARE @CAPACITY_ID INT,
		@CAPACITY DECIMAL(18,4),
		@IS_ACTIVE BIT

DECLARE c CURSOR FOR
SELECT CAPACITY_ID,CAPACITY,IS_ACTIVE FROM TBL_CAPACITY
WHERE CAPACITY IN (SELECT CAPACITY FROM TBL_CAPACITY WHERE IS_ACTIVE=1 GROUP BY CAPACITY HAVING COUNT(*)>1)
OPEN c 
FETCH NEXT FROM c INTO @CAPACITY_ID,@CAPACITY,@IS_ACTIVE
WHILE @@FETCH_STATUS=0
BEGIN 
	DECLARE @IS_DELETE INT
	SELECT @IS_DELETE=COUNT(*) FROM TBL_TRANSFORMER WHERE IS_ACTIVE=1 AND CAPACITY_ID=@CAPACITY_ID
	IF(@IS_DELETE<=0)
	BEGIN
		UPDATE TBL_CAPACITY SET IS_ACTIVE=0 WHERE CAPACITY_ID=@CAPACITY_ID	AND IS_ACTIVE=1
	END
	FETCH NEXT FROM c INTO @CAPACITY_ID,@CAPACITY,@IS_ACTIVE
END
CLOSE c
DEALLOCATE c
GO

IF OBJECT_ID('REPORT_CASH_DAILY')IS NOT NULL
	DROP PROC REPORT_CASH_DAILY
GO

CREATE PROC REPORT_CASH_DAILY
	@USER_CASH_DRAWER_ID INT=4
AS 
SELECT  u.NOTE,
        CASH_DRAWER_NAME,
		OPEN_DATE,
		CLOSED_DATE,
		CREATE_BY, 
		IS_CLOSED,
		c.CURRENCY_NAME,
		c.CURRENCY_SING,
		d.CASH_FLOAT,
		d.TOTAL_CASH,
		d.ACTUAL_CASH,
		d.SURPLUS,
		PAYMENT = p.Q,
		PAYMENT_AMT = ISNULL(p.AMT,0),
		PAYMENT_VOID = pv.Q,
		PAYMENT_VOID_AMT =ISNULL(pv.AMT,0),
		DEPOSIT = dp.Q,
		DEPOSIT_AMT = ISNULL(dp.AMT,0),
		CUS_BUY = cb.Q,
		CUS_BUY_AMT = ISNULL(cb.AMT,0),
		CUS_BUY_VOID = cbv.Q,
		CUS_BUY_VOID_AMT = ISNULL(cbv.AMT,0),
		ADJ = adj.Q,
		ADJ_AMT = ISNULL(adj.AMT,0),
		INV=inv.Q,
		INV_AMT=ISNULL(inv.AMT,0)
FROM TBL_USER_CASH_DRAWER_DETAIL d
INNER JOIN TBL_USER_CASH_DRAWER u ON u.USER_CASH_DRAWER_ID=d.USER_CASH_DRAWER_ID
INNER JOIN TBL_CASH_DRAWER cd ON cd.CASH_DRAWER_ID=u.CASH_DRAWER_ID
INNER JOIN TLKP_CURRENCY c ON c.CURRENCY_ID=d.CURRENCY_ID
OUTER APPLY(
	SELECT  Q = COUNT(*), AMT = SUM(PAY_AMOUNT) 
	FROM TBL_PAYMENT
	WHERE USER_CASH_DRAWER_ID=d.USER_CASH_DRAWER_ID AND CURRENCY_ID=d.CURRENCY_ID
	      AND IS_ACTIVE=1 AND PAY_AMOUNT>0
) p 
OUTER APPLY(
	SELECT Q = COUNT(*), AMT = SUM(PAY_AMOUNT)
	FROM TBL_PAYMENT
	WHERE USER_CASH_DRAWER_ID=d.USER_CASH_DRAWER_ID AND CURRENCY_ID=d.CURRENCY_ID
	AND IS_ACTIVE=1 AND IS_VOID=1 AND PARENT_ID<>0
) pv
OUTER APPLY(
	SELECT Q=COUNT(*), AMT=SUM(AMOUNT)
	FROM TBL_CUS_DEPOSIT
	WHERE USER_CASH_DRAWER_ID=d.USER_CASH_DRAWER_ID AND CURRENCY_ID=d.CURRENCY_ID
) dp
OUTER APPLY(
	SELECT Q=COUNT(*),AMT=SUM(BUY_AMOUNT) 
	FROM TBL_CUSTOMER_BUY 
	WHERE USER_CASH_DRAWER_ID=d.USER_CASH_DRAWER_ID AND CURRENCY_ID=d.CURRENCY_ID
		AND BUY_AMOUNT > 0
) cb
OUTER APPLY(
	SELECT Q=COUNT(*),AMT=SUM(BUY_AMOUNT) 
	FROM TBL_CUSTOMER_BUY 
	WHERE  USER_CASH_DRAWER_ID=d.USER_CASH_DRAWER_ID AND CURRENCY_ID=d.CURRENCY_ID
	AND IS_VOID=1  AND PARENT_ID<>0
) cbv
OUTER APPLY(
	SELECT Q=COUNT(*),AMT=SUM(ADJUST_AMOUNT)
	FROM TBL_INVOICE_ADJUSTMENT a
	INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=a.INVOICE_ID
	WHERE  USER_CASH_DRAWER_ID=d.USER_CASH_DRAWER_ID
	AND i.CURRENCY_ID=d.CURRENCY_ID
) adj
OUTER APPLY(
	SELECT  Q = COUNT(*), AMT = SUM(pd.PAY_AMOUNT) 
	FROM TBL_PAYMENT p
	INNER JOIN TBL_PAYMENT_DETAIL pd ON pd.PAYMENT_ID=p.PAYMENT_ID
	WHERE p.USER_CASH_DRAWER_ID=d.USER_CASH_DRAWER_ID AND p.CURRENCY_ID=d.CURRENCY_ID
	      AND p.IS_ACTIVE=1 AND p.PAY_AMOUNT>0
)inv	
WHERE d.USER_CASH_DRAWER_ID=@USER_CASH_DRAWER_ID
-- END OF REPORT_CASH_DAILY
GO

IF OBJECT_ID('TBL_PRINT_INVOICE')IS NULL
	CREATE TABLE TBL_PRINT_INVOICE
	(
		PRINT_INVOICE_ID INT IDENTITY(1,1) PRIMARY KEY,
		LOGIN_ID INT NOT NULL,
		PRINT_DATE DATETIME NOT NULL,
	)
GO

IF OBJECT_ID('TBL_PRINT_INVOICE_DETAIL')IS NULL
	CREATE TABLE TBL_PRINT_INVOICE_DETAIL
	(
		PRINT_INVOICE_DETAIL_ID INT IDENTITY(1,1) PRIMARY KEY,
		PRINT_INVOICE_ID INT NOT NULL,
		INVOICE_ID INT NOT NULL,
        PRINT_ORDER INT NOT NULL
	)
GO

IF NOT EXISTS(SELECT * FROM TBL_UTILITY WHERE UTILITY_ID=56)
	INSERT INTO TBL_UTILITY (UTILITY_ID,UTILITY_NAME,DESCRIPTION,UTILITY_VALUE)
	SELECT 56,N'INVOICE_CONCURRENT_PRINTING',N'',0
GO

IF OBJECT_ID('REPORT_INVOICE_1')IS NOT NULL
	DROP PROC REPORT_INVOICE_1
GO

CREATE PROC REPORT_INVOICE_1
	@PRINT_INVOICE_ID INT=0
AS 
-- @SINCE : 2012-04-02
-- support both recurring service & merge invoice
-- @SINCE: 2014-05-28
-- support multi-currency and add currency sign.
-- @SINCE : 2014-06-30
-- support bank payment.
-- @SINCE : 2014-10-18
-- use for TBL_PRINT_INVOICE & TBL_PRINT_INVOICE_DETAIL
-- by : Morm Raksmey
DECLARE @PRE NVARCHAR(50);
SELECT @PRE = UTILITY_VALUE 
FROM TBL_UTILITY
WHERE UTILITY_ID=44;
SET @PRE = CASE WHEN LTRIM(RTRIM(@PRE)) IN('','000') THEN ''
				ELSE @PRE + '-' 
		    END;

SELECT  ip.PRINT_ORDER,i.INVOICE_ID,
		i.INVOICE_MONTH,i.INVOICE_NO,
		METER_CODE=REPLACE(LTRIM(REPLACE(i.METER_CODE,'0',' ')),' ','0'),
		i.START_DATE,
		i.END_DATE,
		i.INVOICE_DATE,
		i.START_USAGE,i.END_USAGE,
		i.CUSTOMER_ID,i.FORWARD_AMOUNT,
		i.TOTAL_AMOUNT,i.SETTLE_AMOUNT,
		i.PAID_AMOUNT,i.TOTAL_USAGE,
		i.CURRENCY_ID,i.CYCLE_ID,
		i.DUE_DATE,i.INVOICE_STATUS,
		i.IS_SERVICE_BILL,i.PRINT_COUNT,
		i.RUN_ID,i.DISCOUNT_USAGE,
		i.DISCOUNT_USAGE_NAME,
		i.DISCOUNT_AMOUNT,
		i.DISCOUNT_AMOUNT_NAME,
		i.INVOICE_TITLE, 
		c.LAST_NAME,c.FIRST_NAME,
		c.LAST_NAME_KH,c.FIRST_NAME_KH, 
		c.ADDRESS,c.HOUSE_NO,c.STREET_NO,
		c.PHONE_1,c.PHONE_2, 
		c.COMPANY_NAME,CUSTOMER_CODE = @PRE+c.CUSTOMER_CODE, 
		a.AREA_ID,a.AREA_NAME,a.AREA_CODE,
		b.BOX_ID,b.BOX_CODE,
		l.POLE_ID,l.POLE_CODE,
		d.*,
		CURRENT_DUE= ISNULL((SELECT SUM(SETTLE_AMOUNT-PAID_AMOUNT) 
							 FROM TBL_INVOICE t
						     INNER JOIN TBL_CUSTOMER tc ON tc.CUSTOMER_ID = t.CUSTOMER_ID
							 WHERE (t.CUSTOMER_ID=i.CUSTOMER_ID OR tc.INVOICE_CUSTOMER_ID = i.CUSTOMER_ID)
									AND t.RUN_ID <> i.RUN_ID 
									AND t.INVOICE_DATE < i.INVOICE_DATE
									AND t.INVOICE_STATUS!=3
									AND t.CURRENCY_ID=i.CURRENCY_ID)
					         ,0),
	
		LAST_PAY_DATE= p.PAY_DATE,
		LAST_PAY= ISNULL(p.PAY_AMOUNT,0),
		ADJUST_AMOUNT= ISNULL((SELECT SUM(AMOUNT) 
			FROM TBL_INVOICE_DETAIL t
			WHERE t.INVOICE_ID=i.INVOICE_ID 
				  AND t.INVOICE_ITEM_ID=-1),0),
		ADJUST_NOTE = (
			SELECT TOP 1  ADJUST_REASON  
			FROM TBL_INVOICE_ADJUSTMENT
			WHERE INVOICE_ID=i.INVOICE_ID
			ORDER BY ADJUST_INVOICE_ID DESC
		), 
		NO_INVOICE_CUSTOMER = ISNULL(ni.NO_INVOICE_CUSTOMER,0),
		NO_USAGE_CUSTOMER = ISNULL(nu.NO_USAGE_CUSTOMER,0),
		i.START_PAY_DATE,
		cx.CURRENCY_SING
FROM TBL_INVOICE i 
INNER JOIN TBL_PRINT_INVOICE_DETAIL ip ON i.INVOICE_ID=ip.INVOICE_ID
LEFT JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
LEFT JOIN TBL_CUSTOMER_METER m ON m.CUSTOMER_ID=c.CUSTOMER_ID AND m.IS_ACTIVE=1
LEFT JOIN TBL_BOX b ON m.BOX_ID=b.BOX_ID
LEFT JOIN TBL_POLE l ON l.POLE_ID = b.POLE_ID
LEFT JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
LEFT JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=i.CURRENCY_ID

-- GET INVOICE DETAIL AND INVOICE SERVICE.
OUTER APPLY
(
 -- 1 : THIS INVOICE POWER
 SELECT x.INVOICE_DETAIL_ID, 
		INVOICE_ID_DETAIL=x.INVOICE_ID,
        START_USAGE_DETAIL=x.START_USAGE,
		END_USAGE_DETAIL=x.END_USAGE,
        USAGE_DETAIL=x.USAGE,
        PRICE=x.PRICE,
        INVOICE_ITEM_ID=x.INVOICE_ITEM_ID,
        AMOUNT=x.AMOUNT,
        CHARGE_DESCRIPTION=x.CHARGE_DESCRIPTION,
		REPORT_GROUP=CASE WHEN x.INVOICE_ITEM_ID=1 THEN 1 ELSE 2 END,
		IS_DETAIL = 1,
		MULTIPLIER= 1
 FROM TBL_INVOICE_DETAIL x
 WHERE	x.INVOICE_ID=i.INVOICE_ID
		-- ADJUST AMOUNT (-1) ARE NOT DISPLAY AS DETAIL
		AND x.INVOICE_ITEM_ID!=-1 

 UNION ALL
 -- OTHER CUSTOMER'S INVOICE TO MERGE
 SELECT INVOICE_DETAIL_ID=0, 
		INVOICE_ID=i.INVOICE_ID,
        START_USAGE_DETAIL=x.START_USAGE,
		END_USAGE_DETAIL=x.END_USAGE,
        USAGE_DETAIL=x.TOTAL_USAGE,
        PRICE = p.PRICE,
        INVOICE_ITEM_ID = 0,
        x.SETTLE_AMOUNT,
        INVOICE_TITLE= (CASE WHEN IS_SERVICE_BILL=1 THEN INVOICE_TITLE ELSE x.METER_CODE  END),
		REPORT_GROUP = (CASE WHEN IS_SERVICE_BILL=1 THEN 2 ELSE 1 END),
		IS_DETAIL = 0,
		MULTIPLIER = u.MULTIPLIER
 FROM TBL_INVOICE x
 INNER JOIN TBL_CUSTOMER xc ON xc.CUSTOMER_ID = x.CUSTOMER_ID
 OUTER APPLY(SELECT TOP 1 PRICE FROM TBL_INVOICE_DETAIL WHERE INVOICE_ID=x.INVOICE_ID) p 
 OUTER APPLY(SELECT TOP 1 MULTIPLIER FROM TBL_INVOICE_USAGE WHERE INVOICE_ID=x.INVOICE_ID ORDER BY INVOICE_USAGE_ID DESC) u
 WHERE -- only power invoice will display other detail item.
	   i.IS_SERVICE_BILL =0
	   -- include only current billing cycle
	   AND x.RUN_ID = i.RUN_ID
	   -- other invoice of power invoice OR all invoice if cusotmer is customer merge
	   AND (x.INVOICE_ID <> i.INVOICE_ID OR c.INVOICE_CUSTOMER_ID<>0) 
	   AND (x.CURRENCY_ID=i.CURRENCY_ID) 
	   -- all invoice of current customer or customer to be merged to current customer
	   AND (xc.CUSTOMER_ID = i.CUSTOMER_ID OR xc.INVOICE_CUSTOMER_ID = i.CUSTOMER_ID)
	   -- merge invoice
	   AND ( -- service invoice will display
			(x.IS_SERVICE_BILL = 1)
             OR 
			 -- power invoice will diplay only when view in parent customer
			(x.IS_SERVICE_BILL=0 AND c.INVOICE_CUSTOMER_ID=c.CUSTOMER_ID)) 
) d
-- GET LAST PAYMENT.
OUTER APPLY(
	SELECT TOP 1 p.PAY_DATE,p.PAY_AMOUNT
	FROM TBL_PAYMENT p
	WHERE p.CUSTOMER_ID=i.CUSTOMER_ID
		  AND p.PAY_DATE<i.INVOICE_DATE	
		  AND p.CURRENCY_ID=i.CURRENCY_ID
    ORDER BY PAY_DATE DESC
) p  
OUTER APPLY(
	SELECT NO_USAGE_CUSTOMER = COUNT(*)
	FROM TBL_INVOICE_USAGE 
	WHERE INVOICE_ID = i.INVOICE_ID
		AND i.IS_SERVICE_BILL = 0
		-- only in power invoice
) nu
OUTER APPLY(
	SELECT NO_INVOICE_CUSTOMER = COUNT(*) 
	FROM TBL_CUSTOMER 
	WHERE INVOICE_CUSTOMER_ID= c.CUSTOMER_ID
		AND i.IS_SERVICE_BILL = 0
		-- only in power invoice
) ni 
WHERE ip.PRINT_INVOICE_ID=@PRINT_INVOICE_ID
ORDER BY ip.PRINT_ORDER, REPORT_GROUP,d.START_USAGE_DETAIL
GO
-- END OF REPORT_INVOICE ---

IF OBJECT_ID('REPORT_BALANCE_CUSTOMER_DEPOSIT')IS NOT NULL
	DROP PROC REPORT_BALANCE_CUSTOMER_DEPOSIT
GO 

IF OBJECT_ID('REPORT_BALANCE_CUSTOMER_CONNECTION_FEE')IS NOT NULL
	DROP PROC REPORT_BALANCE_CUSTOMER_CONNECTION_FEE
GO 

IF OBJECT_ID('REPORT_DEPOSIT_PAYSLIP')IS NOT NULL
	DROP PROC REPORT_DEPOSIT_PAYSLIP
GO

IF OBJECT_ID('REPORT_CUSTOMER_CONNECTION')IS NOT NULL
	DROP PROC REPORT_CUSTOMER_CONNECTION
GO

CREATE PROC REPORT_CUSTOMER_CONNECTION
	@CUSTOMER_ID INT = 3
AS
SELECT * 
INTO #TMP
FROM (
SELECT	C.CUSTOMER_ID,
		FULL_NAME_KH=LAST_NAME_KH+' '+FIRST_NAME_KH,
		FULL_NAME =C.LAST_NAME+' '+ C.FIRST_NAME,
		C.JOB,
		C.HOUSE_NO,
		C.PHONE_1,
		C.CUSTOMER_CODE,
		CUSTOMER_ADDRESS.VILLAGE_NAME,
		CUSTOMER_ADDRESS.COMMUNE_NAME,
		CUSTOMER_ADDRESS.DISTRICT_NAME,
		CUSTOMER_ADDRESS.PROVINCE_NAME,
		CT.CUSTOMER_TYPE_NAME,
		PH.PHASE_NAME,
		VOL.VOLTAGE_NAME,
		A.AMPARE_NAME,
		B.BOX_CODE,
		T.TRANSFORMER_CODE,
		N.COMPANY_NAME,
		N.LICENSE_NUMBER
		,COMPANY_ADDRESS.COMPANY_VILLAGE
		,COMPANY_ADDRESS.COMPANY_COMMUNE
		,COMPANY_ADDRESS.COMPANY_DISTRIC
		,COMPANY_ADDRESS.COMPANY_PROVINCE
FROM TBL_CUSTOMER C  
OUTER APPLY (
		SELECT NV.VILLAGE_NAME
			  ,NCO.COMMUNE_NAME
			  ,ND.DISTRICT_NAME
			  ,NP.PROVINCE_NAME
		FROM TLKP_VILLAGE NV 
		LEFT JOIN TLKP_COMMUNE NCO ON NV.COMMUNE_CODE=NCO.COMMUNE_CODE
		LEFT JOIN TLKP_DISTRICT ND ON NCO.DISTRICT_CODE=ND.DISTRICT_CODE
		LEFT JOIN TLKP_PROVINCE NP ON ND.PROVINCE_CODE=NP.PROVINCE_CODE
		WHERE NV.VILLAGE_CODE=C.VILLAGE_CODE
	)CUSTOMER_ADDRESS

LEFT JOIN TBL_PHASE PH ON C.PHASE_ID=PH.PHASE_ID
LEFT JOIN TBL_CUSTOMER_TYPE CT ON C.CUSTOMER_TYPE_ID=CT.CUSTOMER_TYPE_ID 
LEFT JOIN TBL_CUSTOMER_METER CM ON CM.CUSTOMER_ID=C.CUSTOMER_ID AND CM.IS_ACTIVE=1
LEFT JOIN TBL_POLE PO ON PO.POLE_ID = CM.POLE_ID
LEFT JOIN TBL_TRANSFORMER T ON T.TRANSFORMER_ID = PO.TRANSFORMER_ID
LEFT JOIN TBL_BOX B ON B.BOX_ID = CM.BOX_ID
LEFT JOIN TBL_VOLTAGE VOL ON C.VOL_ID=VOL.VOLTAGE_ID
LEFT JOIN TBL_AMPARE A ON C.AMP_ID=A.AMPARE_ID
LEFT JOIN (
			SELECT M.METER_ID,M.METER_CODE,V.VOLTAGE_NAME,P.PHASE_NAME,A.AMPARE_NAME,MT.METER_TYPE_NAME,M.STATUS_ID FROM TBL_METER M 
			INNER JOIN TBL_METER_TYPE MT ON M.METER_TYPE_ID=MT.METER_TYPE_ID
			INNER JOIN TBL_AMPARE A ON A.AMPARE_ID=MT.METER_AMP_ID
			INNER JOIN TBL_VOLTAGE V ON V.VOLTAGE_ID=MT.METER_VOL_ID
			INNER JOIN TBL_PHASE P ON P.PHASE_ID=MT.METER_PHASE_ID
) TEM1 ON TEM1.METER_ID=CM.METER_ID,
TBL_COMPANY N 
OUTER APPLY (
		SELECT COMPANY_VILLAGE=NV.VILLAGE_NAME
			  ,COMPANY_COMMUNE=NCO.COMMUNE_NAME
			  ,COMPANY_DISTRIC=ND.DISTRICT_NAME
			  ,COMPANY_PROVINCE=NP.PROVINCE_NAME
		FROM TLKP_VILLAGE NV 
		LEFT JOIN TLKP_COMMUNE NCO ON NV.COMMUNE_CODE=NCO.COMMUNE_CODE
		LEFT JOIN TLKP_DISTRICT ND ON NCO.DISTRICT_CODE=ND.DISTRICT_CODE
		LEFT JOIN TLKP_PROVINCE NP ON ND.PROVINCE_CODE=NP.PROVINCE_CODE
		WHERE NV.VILLAGE_CODE=N.VILLAGE_CODE
	)COMPANY_ADDRESS
WHERE C.CUSTOMER_ID=@CUSTOMER_ID
)t



SELECT d.CUSTOMER_ID
		,BALANCE=SUM(d.AMOUNT)
		,d.CURRENCY_ID
		,c.CURRENCY_NAME
		,TYPE_NAME=N'ប្រាក់កក់'
INTO #TMP_CUSTOMER_DEPOSIT
FROM TBL_CUS_DEPOSIT d
INNER JOIN TLKP_CURRENCY c ON c.CURRENCY_ID=d.CURRENCY_ID 
WHERE d.CUSTOMER_ID = @CUSTOMER_ID
		AND d.IS_PAID=1
GROUP BY d.CURRENCY_ID,c.CURRENCY_NAME,d.CUSTOMER_ID

SELECT i.CUSTOMER_ID
		,BALANCE=SUM(d.AMOUNT)
		,i.CURRENCY_ID
		,c.CURRENCY_NAME
		,TYPE_NAME=N'ថ្លៃភ្ជាប់ចរន្ត'
INTO #TMP_CUSTOMER_CONNECTION_FEE
FROM TBL_INVOICE i
INNER JOIN TBL_INVOICE_DETAIL d ON d.INVOICE_ID=i.INVOICE_ID
INNER JOIN TLKP_CURRENCY c ON c.CURRENCY_ID=i.CURRENCY_ID
WHERE i.IS_SERVICE_BILL=1
		AND d.INVOICE_ITEM_ID=3
		AND i.CUSTOMER_ID=@CUSTOMER_ID
GROUP BY i.CURRENCY_ID,c.CURRENCY_NAME,i.CUSTOMER_ID

SELECT t.*
		,cd.BALANCE
		,cd.CURRENCY_ID 
		,cd.TYPE_NAME
		,cd.CURRENCY_NAME
INTO #TMP_CUS_DEPOSIT
FROM #TMP t
LEFT JOIN #TMP_CUSTOMER_DEPOSIT cd ON cd.CUSTOMER_ID=t.CUSTOMER_ID

SELECT t.*
		,cf.BALANCE
		,cf.CURRENCY_ID 
		,cf.TYPE_NAME
		,cf.CURRENCY_NAME
INTO #TMP_CUS_CONNECTION_FEE
FROM #TMP t
LEFT JOIN #TMP_CUSTOMER_CONNECTION_FEE cf ON cf.CUSTOMER_ID=t.CUSTOMER_ID

SELECT DISTINCT *
FROM(
	SELECT * 
	FROM #TMP_CUS_DEPOSIT
	UNION ALL
	SELECT * 
	FROM #TMP_CUS_CONNECTION_FEE
)x

GO
-- END OF REPORT_CUSTOMER_CONNECTION


IF OBJECT_ID('REPORT_CUSTOMER_DEPOSIT_CONNECTION_FEE_PAYSLIP')IS NOT NULL
	DROP PROC REPORT_CUSTOMER_DEPOSIT_CONNECTION_FEE_PAYSLIP
GO

CREATE PROC REPORT_CUSTOMER_DEPOSIT_CONNECTION_FEE_PAYSLIP
	@CUSTOMER_ID INT=35
AS

DECLARE @PRE NVARCHAR(50);
SELECT @PRE = UTILITY_VALUE 
FROM TBL_UTILITY
WHERE UTILITY_ID=44;
SET @PRE = CASE WHEN LTRIM(RTRIM(@PRE)) IN('','000') THEN ''
				ELSE @PRE + '-' 
		    END;

SELECT * 
INTO #TMP
FROM(
		-- GET DEPOSIT
		SELECT	d.CUSTOMER_ID
				,cu.CURRENCY_ID
				,cu.CURRENCY_NAME 
				,DUE_AMOUNT=SUM(d.AMOUNT)
				,TYPE_NAME=N'ប្រាក់កក់' 
		FROM TBL_CUS_DEPOSIT d
		INNER JOIN TLKP_CURRENCY cu ON cu.CURRENCY_ID=d.CURRENCY_ID 
		WHERE d.CUSTOMER_ID=@CUSTOMER_ID 
			AND d.IS_PAID=0
		GROUP BY d.CUSTOMER_ID,cu.CURRENCY_ID,cu.CURRENCY_NAME
		UNION ALL
		---- GET CONNECTION FEE
		SELECT i.CUSTOMER_ID
				,cu.CURRENCY_ID
				,cu.CURRENCY_NAME 
				,DUE_AMOUNT=SUM(i.SETTLE_AMOUNT-i.PAID_AMOUNT)
				,TYPE_ID=N'ប្រាក់ភ្ជាប់ចរន្ត' 
		FROM TBL_INVOICE i
		INNER JOIN TLKP_CURRENCY cu ON cu.CURRENCY_ID=i.CURRENCY_ID 
		OUTER APPLY(
			SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
			FROM TBL_INVOICE_DETAIL d
			INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
			WHERE d.INVOICE_ID = i.INVOICE_ID
		) itm 
		WHERE i.IS_SERVICE_BILL=1
				AND itm.INVOICE_ITEM_ID=3
				AND i.CUSTOMER_ID=@CUSTOMER_ID 
				AND i.PAID_AMOUNT<i.SETTLE_AMOUNT
		GROUP BY i.CUSTOMER_ID,cu.CURRENCY_ID,cu.CURRENCY_NAME
)t 
 

SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		PREFIX=@PRE,
		CUSTOMER_CODE =c.CUSTOMER_CODE,
		LAST_NAME_KH,
		FIRST_NAME_KH,
		AREA_NAME,	
		POLE_CODE,
		BOX_CODE,
		am.AMPARE_NAME,
		c.STATUS_ID,
		t.* , 
		NV.VILLAGE_NAME,
		NCO.COMMUNE_NAME,
		ND.DISTRICT_NAME,
		NP.PROVINCE_NAME,
		EXPIRED_DATE=DATEADD(MONTH,3,c.CREATED_ON),
		ACTIVATE_DATE=c.CREATED_ON
FROM #TMP t
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=t.CUSTOMER_ID
LEFT JOIN TLKP_VILLAGE NV ON NV.VILLAGE_CODE=c.VILLAGE_CODE
LEFT JOIN TLKP_COMMUNE NCO ON NV.COMMUNE_CODE=NCO.COMMUNE_CODE
LEFT JOIN TLKP_DISTRICT ND ON NCO.DISTRICT_CODE=ND.DISTRICT_CODE
LEFT JOIN TLKP_PROVINCE NP ON ND.PROVINCE_CODE=NP.PROVINCE_CODE
LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID=c.CUSTOMER_ID AND cm.IS_ACTIVE=1
LEFT JOIN TBL_BOX b ON b.BOX_ID =cm.BOX_ID
LEFT JOIN TBL_POLE p ON p.POLE_ID = b.POLE_ID
LEFT JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
LEFT JOIN TBL_AMPARE am ON am.AMPARE_ID=c.AMP_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=t.CURRENCY_ID 
-- END OF REPORT_CUSTOMER_DEPOSIT_CONNECTION_FEE_PAYSLIP
GO 

IF OBJECT_ID('REPORT_PAYMENT_DETAIL')IS NOT NULL
	DROP PROC REPORT_PAYMENT_DETAIL
GO

CREATE PROC REPORT_PAYMENT_DETAIL
	@D1 DATETIME='2014-05-01',
	@D2 DATETIME='2014-06-08',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@INCLUDE_DELETE BIT=1,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		CUSTOMER_CODE,
		CUSTOMER_NAME=LAST_NAME_KH + ' ' + FIRST_NAME_KH,
		AREA_NAME,
		INVOICE_NO,
		INVOICE_DATE,
		INVOICE_TITLE,
		PAY_DATE,
		p.CREATE_BY, 
		ISNULL(pd.PAY_AMOUNT,0) AS [AMOUNT]
FROM TBL_PAYMENT_DETAIL pd INNER JOIN TBL_PAYMENT p ON p.PAYMENT_ID=pd.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID = pd.INVOICE_ID 
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID 
INNER JOIN TBL_AREA r ON r.AREA_ID = c.AREA_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
WHERE  (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		AND pd.PAY_AMOUNT>0
        -- filter payment that removed
		AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
ORDER BY PAY_DATE
GO
--END OF REPORT_PAYMENT_DETAIL;

IF OBJECT_ID('REPORT_INVOICE_DETAIL')IS NOT NULL
	DROP PROC REPORT_INVOICE_DETAIL
GO

CREATE PROC REPORT_INVOICE_DETAIL
	@D1 DATETIME='2014-05-01',
	@D2 DATETIME='2014-06-08',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0
AS    
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));
SELECT  cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		i.INVOICE_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME = c.LAST_NAME_KH+' '+c.FIRST_NAME_KH,
		r.AREA_NAME, 
		i.INVOICE_NO,
		i.INVOICE_DATE,
		i.INVOICE_MONTH,
		i.INVOICE_TITLE,  
		BEFORE_ADJUST = i.SETTLE_AMOUNT - ISNULL(adj.ALL_ADJUST,0) ,
		ADJUST_AMOUNT = ISNULL(adj.ALL_ADJUST,0),
		AFTER_ADJUST = i.SETTLE_AMOUNT
FROM TBL_INVOICE i 
OUTER APPLY(
	SELECT  ALL_ADJUST=SUM(ADJUST_AMOUNT)
	FROM TBL_INVOICE_ADJUSTMENT 
	WHERE INVOICE_ID=i.INVOICE_ID
) adj
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID 
INNER JOIN TBL_AREA r ON r.AREA_ID = c.AREA_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=i.CURRENCY_ID
WHERE	INVOICE_DATE BETWEEN @D1 AND @D2 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID) 
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND	(@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
-- END OF REPORT INVOICE DETAIL
GO

IF OBJECT_ID('REPORT_INVOICE_ADJUSTMENT_DETAIL')IS NOT NULL
	DROP PROC REPORT_INVOICE_ADJUSTMENT_DETAIL
GO

CREATE PROC REPORT_INVOICE_ADJUSTMENT_DETAIL
	@D1 DATETIME='2014-05-01',
	@D2 DATETIME='2014-06-08',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));
SELECT  cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		CREATE_ON,
		CREATE_BY,
		CUSTOMER_CODE,
		LAST_NAME_KH + ' ' + FIRST_NAME_KH AS [CUSTOMER_NAME],
		INVOICE_NO,
		ADJUST_USAGE AS USAGE,
		ADJUST_AMOUNT AS [AMOUNT],
		ADJUST_REASON=REPLACE(ADJUST_REASON,N'កែសំរូលវិក័យប័ត្រ៖','')
FROM TBL_INVOICE_ADJUSTMENT a
INNER JOIN TBL_INVOICE i ON a.INVOICE_ID = i.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
INNER JOIN TBL_AREA ar ON ar.AREA_ID=c.AREA_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=i.CURRENCY_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE (a.CREATE_ON BETWEEN @D1 AND @D2)
	AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
	AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
	AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
	AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
ORDER BY CREATE_ON
GO
-- END REPORT_INVOICE_ADJUSTMENT

IF OBJECT_ID('REPORT_PAYMENT_CANCEL_DETAIL')IS NOT NULL
	DROP PROC REPORT_PAYMENT_CANCEL_DETAIL
GO

CREATE PROC REPORT_PAYMENT_CANCEL_DETAIL
	@D1 DATETIME='2014-05-01',
	@D2 DATETIME='2014-04-08',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		CUSTOMER_CODE,
		CUSTOMER_NAME=LAST_NAME_KH + ' ' + FIRST_NAME_KH,
		AREA_NAME,
		INVOICE_NO,
		INVOICE_DATE,
		INVOICE_TITLE,
		PAY_DATE,
		p.CREATE_BY, 
		AMOUNT=ISNULL(pd.PAY_AMOUNT,0)*-1
FROM TBL_PAYMENT_DETAIL pd INNER JOIN TBL_PAYMENT p ON p.PAYMENT_ID=pd.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID = pd.INVOICE_ID 
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=i.CURRENCY_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID 
INNER JOIN TBL_AREA r ON r.AREA_ID = c.AREA_ID
WHERE	(p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		AND p.PAY_AMOUNT<0
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
ORDER BY PAY_DATE
GO
-- REPORT PAYMENT CANCEL


IF OBJECT_ID('REPORT_PAYMENT_SUMMARY')IS NOT NULL
	DROP PROC REPORT_PAYMENT_SUMMARY
GO

CREATE PROC REPORT_PAYMENT_SUMMARY
	@D1 DATETIME='2014-04-01',
	@D2 DATETIME='2014-08-08',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));
DECLARE @DATE DATETIME;
SET @DATE=@D1;

-- PAYMENT
SELECT	p.CURRENCY_ID,
		INVOICE_MONTH,
		TYPE='PAYMENT',
		AMOUNT  = SUM(ISNULL(pd.PAY_AMOUNT,0))
INTO #TMP
FROM TBL_PAYMENT_DETAIL pd 
INNER JOIN TBL_PAYMENT p ON p.PAYMENT_ID=pd.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID = pd.INVOICE_ID 
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE	(p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		AND pd.PAY_AMOUNT>0
GROUP BY INVOICE_MONTH,p.CURRENCY_ID


-- NEW INVOICE
UNION ALL
SELECT  CURRENCY_ID,
		INVOICE_MONTH,
		TYPE = 'NEW_INVOICE',
		BEFORE_ADJUST =SUM( i.SETTLE_AMOUNT - ISNULL(adj.ALL_ADJUST,0))
		--ADJUST_AMOUNT = ISNULL(adj.ALL_ADJUST,0),
		--AFTER_ADJUST = i.SETTLE_AMOUNT
FROM TBL_INVOICE i 
OUTER APPLY(
	SELECT  ALL_ADJUST=SUM(ADJUST_AMOUNT)
	FROM TBL_INVOICE_ADJUSTMENT 
	WHERE INVOICE_ID=i.INVOICE_ID
) adj
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE	INVOICE_DATE BETWEEN @D1 AND @D2 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
GROUP BY INVOICE_MONTH,CURRENCY_ID

-- ADJUSTMENT
UNION ALL
SELECT  CURRENCY_ID,
		INVOICE_MONTH,
		TYPE='ADJUST_INVOICE',
		AMOUNT = SUM(ADJUST_AMOUNT)		
FROM TBL_INVOICE_ADJUSTMENT a
INNER JOIN TBL_INVOICE i ON a.INVOICE_ID = i.INVOICE_ID
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE (a.CREATE_ON BETWEEN @D1 AND @D2)
	AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
	AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID) 
GROUP BY INVOICE_MONTH,CURRENCY_ID

-- CANCEL PAYMENT 
UNION ALL
SELECT	p.CURRENCY_ID,
		INVOICE_MONTH,
		TYPE='CANCEL_PAYMENT',
		AMOUNT=SUM(ISNULL(pd.PAY_AMOUNT,0)*-1)
FROM TBL_PAYMENT_DETAIL pd INNER JOIN TBL_PAYMENT p ON p.PAYMENT_ID=pd.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID = pd.INVOICE_ID 
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE	(p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		AND p.PAY_AMOUNT<0
GROUP BY INVOICE_MONTH,p.CURRENCY_ID
-- REPORT PAYMENT CANCEL

-- BEGIN AR
UNION ALL
SELECT  CURRENCY_ID,
		INVOICE_MONTH,
		TYPE = 'BEGIN_AR',
		BALANCE=SUM( i.SETTLE_AMOUNT - ISNULL(adj.ALL_ADJUST,0) + ISNULL(adj.ADJUST,0) 
					- ISNULL(pay.PAID_AMOUNT,0))
FROM TBL_INVOICE i
OUTER APPLY(
	SELECT PAID_AMOUNT = SUM(dd.PAY_AMOUNT) 
	FROM TBL_PAYMENT dp
	INNER JOIN TBL_PAYMENT_DETAIL dd ON dd.PAYMENT_ID = dp.PAYMENT_ID
	WHERE dd.INVOICE_ID=i.INVOICE_ID  AND dp.PAY_DATE<=@D1
) pay
OUTER APPLY(
	SELECT  ADJUST=SUM(CASE WHEN CREATE_ON<=@D1 THEN ADJUST_AMOUNT ELSE 0 END ),
			ALL_ADJUST=SUM(ADJUST_AMOUNT)
	FROM TBL_INVOICE_ADJUSTMENT 
	WHERE INVOICE_ID=i.INVOICE_ID
) adj
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
WHERE	INVOICE_DATE<@D1
		AND i.SETTLE_AMOUNT - ISNULL(adj.ALL_ADJUST,0) + ISNULL(adj.ADJUST,0)-ISNULL(pay.PAID_AMOUNT,0)>0
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
GROUP BY INVOICE_MONTH,CURRENCY_ID
 
-- ENDING_AR 
UNION ALL
SELECT  CURRENCY_ID,
		INVOICE_MONTH,
		TYPE = 'ENDING_AR',
		BALANCE=SUM( i.SETTLE_AMOUNT - ISNULL(adj.ALL_ADJUST,0) + ISNULL(adj.ADJUST,0) 
					- ISNULL(pay.PAID_AMOUNT,0))
FROM TBL_INVOICE i
OUTER APPLY(
	SELECT PAID_AMOUNT = SUM(dd.PAY_AMOUNT) 
	FROM TBL_PAYMENT dp
	INNER JOIN TBL_PAYMENT_DETAIL dd ON dd.PAYMENT_ID = dp.PAYMENT_ID
	WHERE dd.INVOICE_ID=i.INVOICE_ID  AND dp.PAY_DATE<=@D2
) pay
OUTER APPLY(
	SELECT  ADJUST=SUM(CASE WHEN CREATE_ON<=@D2 THEN ADJUST_AMOUNT ELSE 0 END ),
			ALL_ADJUST=SUM(ADJUST_AMOUNT)
	FROM TBL_INVOICE_ADJUSTMENT 
	WHERE INVOICE_ID=i.INVOICE_ID
) adj
OUTER APPLY(
	SELECT TOP 1 t.INVOICE_ITEM_ID,t.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON d.INVOICE_ITEM_ID=t.INVOICE_ITEM_ID 
	WHERE d.INVOICE_ID = i.INVOICE_ID
) itm
INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID 
WHERE	INVOICE_DATE<@D2
		AND i.SETTLE_AMOUNT - ISNULL(adj.ALL_ADJUST,0) + ISNULL(adj.ADJUST,0)-ISNULL(pay.PAID_AMOUNT,0)>0
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
GROUP BY INVOICE_MONTH,CURRENCY_ID
-- END OF REPORT_AGING_DETAIL


SELECT cx.CURRENCY_ID,
	   cx.CURRENCY_NAME,
	   cx.CURRENCY_SING,
	   INVOICE_MONTH, 
	   BEGIN_AR = SUM(CASE WHEN TYPE='BEGIN_AR' THEN AMOUNT ELSE 0 END),
	   NEW_INVOICE = SUM(CASE WHEN TYPE='NEW_INVOICE' THEN AMOUNT ELSE 0 END),
	   ADJUST_INVOICE = SUM(CASE WHEN TYPE='ADJUST_INVOICE' THEN AMOUNT ELSE 0 END),
	   PAYMENT = SUM(CASE WHEN TYPE='PAYMENT' THEN AMOUNT ELSE 0 END),
	   CANCEL_PAYMENT = SUM(CASE WHEN TYPE='CANCEL_PAYMENT' THEN AMOUNT ELSE 0 END), 
	   ENDING_AR = SUM(CASE WHEN TYPE='ENDING_AR' THEN AMOUNT ELSE 0 END) 
FROM #TMP t
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=t.CURRENCY_ID
GROUP BY INVOICE_MONTH,cx.CURRENCY_ID,cx.CURRENCY_NAME,cx.CURRENCY_SING
GO  
"; 
        }

    }
}
