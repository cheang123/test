﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPower.Properties;

namespace EPower.Update
{
    
    class Version1_1_9_6 : Version
    {
        /// <summary>  
        /// Morm Raksmey
        /// 1. Update REF Invalid Customer for New Tariff 2018
        /// 2. Update DialogAdjustment for New Tariff 2018 when Adjust Usage
        /// 3. Validate on DialogCompany
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"
IF OBJECT_ID('RUN_REF_INVALID_CUSTOMER')IS NOT NULL
	DROP PROC RUN_REF_INVALID_CUSTOMER
GO

CREATE PROC RUN_REF_INVALID_CUSTOMER
	@DATA_MONTH DATETIME='2017-03-01'
AS
/*
@2017-09-06 By Morm Raksmey
1. Clear Data before INSERT DATA

@2018-03-23 By Morm Raksmey
1. Update REF for Tariff 2018
*/
DELETE FROM TBL_REF_INVALID_CUSTOMER WHERE DATA_MONTH=@DATA_MONTH

DECLARE @M DECIMAL(18,4)
SET @M=99999999999999.9999

IF OBJECT_ID('#TMP_VALIDATE') IS NOT NULL
	DROP TABLE #TMP_VALIDATE
CREATE TABLE #TMP_VALIDATE(
	DATA_MONTH DATETIME,
	VALIDATE_NAME NVARCHAR(200),
	START_USAGE DECIMAL(18,4),
	END_USAGE DECIMAL(18,4),
	PRICE DECIMAL(18,4),
	BASED_PRICE DECIMAL(18,4),
	CURRENCY_ID INT,
	CUSTOMER_CONNECTION_TYPE_ID INT,
	CUSTOMER_GROUP_ID INT
)
IF @DATA_MONTH BETWEEN '2016-03-01' AND '2017-02-01'
	INSERT INTO #TMP_VALIDATE
	SELECT @DATA_MONTH,N'លក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',0,10,480,800,1,1,1 UNION ALL
	SELECT @DATA_MONTH,N'លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង  10 kWh/ខែ',11,@M,800,800,1,1,1 UNION ALL
	SELECT @DATA_MONTH,N'លក់លើ MV នាឡិកាស្ទង់ MV',0,@M,0.1675,0.1675,2,4,2 UNION ALL
	SELECT @DATA_MONTH,N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',0,@M,0.1745,0.1745,2,3,2 UNION ALL
	SELECT @DATA_MONTH,N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',0,@M,0.1840,0.1840,2,2,2 UNION ALL
	SELECT @DATA_MONTH,N'លក់ឱ្យអ្នកកាន់អាជ្ញាប័ណ្ណ',0,@M,0.1470,0.1470,2,4,3
ELSE IF @DATA_MONTH BETWEEN '2017-03-01' AND '2018-02-01'
	INSERT INTO #TMP_VALIDATE
	SELECT @DATA_MONTH,N'លក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',0,10,480,790,1,1,1 UNION ALL
	SELECT @DATA_MONTH,N'លក់ឱ្យលំនៅដ្ឋានប្រើពី 11 ដល់ 50 kWh/ខែ',11,50,610,790,1,1,1 UNION ALL
	SELECT @DATA_MONTH,N'លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង  51 kWh/ខែ',51,@M,790,790,1,1,1 UNION ALL
	SELECT @DATA_MONTH,N'លក់លើ MV នាឡិកាស្ទង់ MV',0,@M,0.1650,0.1650,2,4,2 UNION ALL
	SELECT @DATA_MONTH,N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',0,@M,0.1719,0.1719,2,3,2 UNION ALL
	SELECT @DATA_MONTH,N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',0,@M,0.1819,0.1819,2,2,2 UNION ALL
	SELECT @DATA_MONTH,N'លក់ឱ្យអ្នកកាន់អាជ្ញាប័ណ្ណ',0,@M,0.1450,0.1450,2,4,3
ELSE IF @DATA_MONTH BETWEEN '2018-03-01' AND '9999-12-31'
	INSERT INTO #TMP_VALIDATE
	SELECT @DATA_MONTH,N'លក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង  10 kWh/ខែ',0,10,480,770,1,1,1 UNION ALL
	SELECT @DATA_MONTH,N'លក់ឱ្យលំនៅដ្ឋានប្រើពី 11 ដល់ 50 kWh/ខែ',11,50,610,770,1,1,1 UNION ALL
	SELECT @DATA_MONTH,N'លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង  51 kWh/ខែ',51,@M,770,770,1,1,1 UNION ALL
	SELECT @DATA_MONTH,N'លក់លើ MV នាឡិកាស្ទង់ MV',0,@M,0.1640,0.1640,2,4,2 UNION ALL
	SELECT @DATA_MONTH,N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',0,@M,0.1706,0.1706,2,3,2 UNION ALL
	SELECT @DATA_MONTH,N'លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',0,@M,0.1786,0.1786,2,2,2 UNION ALL
	SELECT @DATA_MONTH,N'លក់ឱ្យអ្នកកាន់អាជ្ញាប័ណ្ណ MV',0,@M,0.1440,0.1440,2,4,3 UNION ALL
	SELECT @DATA_MONTH,N'លក់ឱ្យអ្នកកាន់អាជ្ញាប័ណ្ណ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកទិញ)',0,@M,0.1489,0.1489,2,3,3 UNION ALL
	SELECT @DATA_MONTH,N'លក់ឱ្យអ្នកកាន់អាជ្ញាប័ណ្ណ LV ក្រោមនៃត្រង់ស្វូ (វិនិយោគដោយអ្នកលក់)',0,@M,0.1569,0.1569,2,2,3

DELETE FROM TBL_REF_INVALID_CUSTOMER WHERE DATA_MONTH=@DATA_MONTH

INSERT INTO TBL_REF_INVALID_CUSTOMER
SELECT 
	TYPE_ID=i.CUSTOMER_GROUP_ID,
	TYPE_NAME=v.VALIDATE_NAME,
	METER_CODE_FORMAT=REPLACE(LTRIM(REPLACE(ISNULL(i.METER_CODE,'-'),'0',' ')),' ','0'),
	IS_WRONG_BASED_PRICE= CASE WHEN i.BASED_PRICE != v.BASED_PRICE THEN 1 ELSE 0 END,
	i.* 
FROM TBL_REF_INVOICE i
LEFT JOIN #TMP_VALIDATE v ON v.CUSTOMER_CONNECTION_TYPE_ID=i.CUSTOMER_CONNECTION_TYPE_ID AND v.CUSTOMER_GROUP_ID=i.CUSTOMER_GROUP_ID
WHERE i.DATA_MONTH=@DATA_MONTH
	AND (i.PRICE!=v.PRICE OR i.BASED_PRICE!=v.BASED_PRICE OR i.CURRENCY_ID!=v.CURRENCY_ID)
	AND i.IS_ACTIVE=1
	AND i.TOTAL_USAGE BETWEEN v.START_USAGE AND v.END_USAGE
";
        }
    }
}
