﻿namespace EPower.Update
{
    /// <summary>
    /// Morm Raksmey 
    /// 1.Fix Store Report : REPORT_USAGE_MONTHLY
    /// </summary>
    /// <returns></returns>

    class Version1_1_2_8 : Version
    {
        protected override string GetBatchCommand()
        {
            return @"IF NOT EXISTS (SELECT UTILITY_ID FROM TBL_UTILITY WHERE UTILITY_ID = 65)
INSERT INTO TBL_UTILITY(UTILITY_ID, UTILITY_NAME, [DESCRIPTION], UTILITY_VALUE) 
VALUES (65, N'AMR_SERVICE_URL',	N'BLANK IF NOT USE: http://crm.e-power.com.kh:1114/AMRService.svc/ws',N'');

IF NOT EXISTS (SELECT UTILITY_ID FROM TBL_UTILITY WHERE UTILITY_ID = 66)
INSERT INTO TBL_UTILITY(UTILITY_ID, UTILITY_NAME, [DESCRIPTION], UTILITY_VALUE) 
VALUES (66,	N'AMR_ACCESS_KEY',	N'',N''	);";
        }
    }
}
