﻿namespace EPower.Update
{
    /// <summary>
    /// FIX  
    ///  - INCORRECT KHMER WORD IN TABLE ANR.TBL_TABLE6, ANR.TBL_TREE 
    /// </summary>
    /// <returns></returns>

    class Version1_1_0_6 : Version
    {        
        protected override string GetBatchCommand()
        {
            return @"
UPDATE ANR.TBL_TREE SET TREE_NAME = REPLACE(TREE_NAME,N'ពត៌',N'ព័ត៌');
GO
"; 
        }

    }
}
