﻿namespace EPower.Update
{
    /// <summary> 
    /// Fix Readonly file
    /// </summary>
    class Version1_0_7_2 : Version
    {
        /// <summary>
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"
-- last version when PAY_AMOUNT is negative
-- when cancel aleady PAID_AMOUNT is positive
-- run this script to fix this problem
UPDATE TBL_BANK_PAYMENT_DETAIL 
SET PAID_AMOUNT = PAY_AMOUNT
WHERE PAY_AMOUNT<0 AND PAID_AMOUNT=-PAY_AMOUNT;
"; 
        }
    }
}
