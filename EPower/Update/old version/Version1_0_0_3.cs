﻿using System.Linq;
using System.Transactions;
using SoftTech;

namespace EPower.Update
{
    class Version1_0_0_3
    {
        /// <summary>
        /// - Generate new report schedule
        /// - Update REPORT_PREPAID_MONTH_TO_DATE_SALE_SUMMARY
        ///     add filter by AreaID and CustomerTypeID
        /// - Update REPORT_AGING (remove Deposit)
        /// - Add Table TBL_CAPACITY, TBL_DISTRIBUTION_TYPE, DISTRIBUTION_FACILITY, PURCHASE_AGREEMENT ,PURCHASE_POWER_AGREEMENT
        /// - Update TBL_TRANSFORMER (add Column CAPACITY_ID)
        /// - Generate Index
        /// - Add Tab (Power Managemnt to MainForm)
        /// - Generate Permission
        /// </summary>
        public void Update()
        {
            using (TransactionScope tran = new TransactionScope())
            {
                // Update version to 1.0.0.3
                string[] strSqlStatement = new string[]{
                                        "TRUNCATE TABLE TLKP_REPORT;"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(1,N'ReportTrialBalanceDaily',N'ប្រតិបត្តិការសង្ខេបក្នុងខែ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(2,N'ReportTrialBalanceCustomerDetail',N'ប្រតិបត្តិការអតិថិជនលំអិតក្នុងខែ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(3,N'ReportInvoiceDetail',N'ប្រតិបត្តិការបំណុល និង ការបង់ប្រាក់លំអិតក្នុងខែ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(4,N'ReportTrialBalanceDepositDetail',N'ប្រតិបត្តិការប្រាក់កក់លំអិតក្នុងខែ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(5,N'ReportAging',N'បំណុលតាមកាល (Aging Report)');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(6,N'ReportCashDailySummary',N'ប្រតិបត្តិការសាច់ប្រាក់សង្ខេប');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(7,N'ReportCashDailyDetail',N'ប្រតិបត្តិការណ៍សាច់ប្រាក់លំអិត');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(8,N'ReportCustomers',N'បញ្ជីអតិថិជន');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(9,N'ReportPowerIncomeMonthly',N'ចំណូលពីការលក់អគ្គីសនីប្រចាំខែ ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(10,N'ReportOtherRevenueByMonth',N'ចំណូលផ្សេងៗក្រៅពីការលក់អគ្គីសនីប្រចាំខែ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(11,N'ReportDiscountDetail',N'ការលក់ចុះថ្លៃលំអិតប្រចាំខែ ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(13,N'ReportPowerIncomeYearly',N'ចំណូលពីការលក់អគ្គីសនីប្រចាំឆ្នាំ ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(14,N'ReportUsageSummary',N'ការប្រើប្រាស់អគ្គិសនីសង្ខេបប្រចាំខែ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(15,N'ReportBillingSummary',N'ការចេញវិក្ក័យបត្រ័សង្ខេប');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(16,N'ReportMeterUnknown',N'កុងទ័រមិនទាន់មានក្នុងប្រព័ន្ធ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(17,N'ReportMeterUnregister',N'កុងទ័រមិនទាន់ចុះក្នុងបញ្ចី');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(18,N'ReportPowerYearly',N'ការផលិតថាមពលប្រចាំឆ្នាំ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(19,N'ReportPowerMonthly',N'ការផលិតថាមពលប្រចាំខែ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(20,N'ReportUsageMonthly',N'ការប្រើប្រាស់ថាមពល');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(21,N'ReportPaymentDetail',N'ការបង់ប្រាក់លំអិត');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(22,N'ReportPaymentCancelDetail',N'លុបការបង់ប្រាក់');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(23,N'ReportInvoiceAdjustmentDetail',N'កែតំរូវវិក័យប័ត្រ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(24,N'ReportPrepaidDailySale',N'ចំណូលពីការលក់អគ្គិសនីបង់ប្រាក់មុនប្រចាំថ្ងៃ');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(25,N'ReportPrepaidMonthToDateSaleSummary',N'ចំណូលពីការលក់អគ្គិសនីបង់ប្រាក់មុនសង្ខេប');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(26,N'ReportBlockCustomer',N'បញ្ជីអតិថិជនត្រូវផ្តាច់ចរន្ត');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(27,N'ReportCustomerNoUsage',N'បញ្ជីអតិថិជនដែលមិនទាន់ស្រង់');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(28,N'ReportDistributionFacility',N'មធ្យោបាយចែកចាយថាមពលអគ្គិសនី');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(29,N'ReportPurchaseAgreement',N'កិច្ចព្រមព្រៀងការទិញថាមពលអគ្គិសនី');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(30,N'ReportPurchasePowerAgreement',N'ទិញថាមពលអគ្គិសនី');"
                                        ,"INSERT INTO TLKP_REPORT(REPORT_ID,REPORT_NAME,REPORT_LOCAL_NAME) VALUES(31,N'ReportTransformerDetail',N'ព័ត៌មានលំអិតអំពីត្រង់ស្វូ');"
                                        ,@"ALTER PROC [dbo].[REPORT_PREPAID_MONTH_TO_DATE_SALE_SUMMARY]
	                                            @AREA_ID INT=0,	
	                                            @CUSTOMER_TYPE_ID INT=0,
	                                            @START_DATE DATETIME='2010-05-01',
	                                            @END_DATE DATETIME='2011-05-01'
                                            AS
                                            BEGIN
	                                            -- @BY    : PRAK SEREY
	                                            -- @SINCE : 2011-05-08 Add More parameters.

	                                            SELECT  CUSTOMER_CODE = c.CUSTOMER_CODE,
			                                            CUSTOMER_NAME = c.LAST_NAME_KH+' '+c.FIRST_NAME_KH+' '+c.FIRST_NAME+' '+c.LAST_NAME,
			                                            PHONE = c.PHONE_1+' '+c.PHONE_2,
			                                            a.AREA_ID, 
			                                            AREA_NAME = ISNULL(a.AREA_NAME,'-'),
			                                            POLE_NAME = ISNULL(p.POLE_CODE,'-'),
			                                            BOX_CODE = ISNULL(b.BOX_CODE,'-'),
			                                            METER_CODE = ISNULL(m.METER_CODE,'-'),		
			                                            c.ACTIVATE_DATE,
			                                            TOTAL_BUY_POWER=ISNULL( u.TOTAL_BUY_POWER	,0),
			                                            TOTAL_AMOUNT=ISNULL(u.TOTAL_AMOUNT,0),
			                                            TOTAL_DISCOUNT=ISNULL(u.TOTAL_DISCOUNT,0),
			                                            TOTAL_BUY_TIMES=ISNULL(u.TOTAL_BUY_TIMES,0)
	                                            FROM TBL_CUSTOMER c 
	                                            INNER JOIN TBL_PREPAID_CUSTOMER pc ON c.CUSTOMER_ID = pc.CUSTOMER_ID
	                                            LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID=c.CUSTOMER_ID AND cm.IS_ACTIVE=1
	                                            LEFT JOIN TBL_METER m ON cm.METER_ID=m.METER_ID
	                                            LEFT JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
	                                            LEFT JOIN TBL_POLE p ON p.POLE_ID=cm.POLE_ID
	                                            LEFT JOIN TBL_BOX b ON b.BOX_ID=cm.BOX_ID
	                                            OUTER APPLY (
		                                            SELECT SUM(BUY_QTY) AS TOTAL_BUY_POWER, 
			                                               SUM(BUY_AMOUNT) AS TOTAL_AMOUNT,
			                                               SUM(DISCOUNT_QTY) AS TOTAL_DISCOUNT,	
			                                               COUNT(BUY_TIMES) AS TOTAL_BUY_TIMES 
		                                            FROM TBL_CUSTOMER_BUY 
		                                            WHERE CREATE_ON BETWEEN @START_DATE AND @END_DATE
			                                            AND PREPAID_CUS_ID=pc.PREPAID_CUS_ID 
			                                            AND IS_VOID=0	        
                                            			
	                                            ) u
	                                            WHERE (@AREA_ID=0 OR a.AREA_ID=@AREA_ID)	
		                                            AND (@CUSTOMER_TYPE_ID=0 OR c.CUSTOMER_TYPE_ID=@CUSTOMER_TYPE_ID)
		                                            AND c.STATUS_ID != 5
		                                            AND c.IS_POST_PAID=0;
                                            END"
                                        ,@"ALTER PROC [dbo].[REPORT_AGING]
                                            @DATE DATETIME ='31/Jan/2011',
                                            @DAYS INT=10,
                                            @INTERVAL INT=3,
                                            @CUSTOMER_TYPE_ID INT =0
                                            AS

                                            DECLARE @i INT,@START DATETIME,@END DATETIME
                                            SET @DATE = DATEADD(D, 0, DATEDIFF(D, 0, @DATE))
                                            SET @DATE = DATEADD(S,-1,DATEADD(D,1,@DATE))
                                            SET @END = DATEADD(D, 0, DATEDIFF(D, 0, @DATE))
                                            SET @END =  DATEADD(S,-1,DATEADD(D,1,@END))

                                            SET @i = 1
                                            IF OBJECT_ID('tempdb..#AGING') IS NOT NULL
                                            BEGIN
                                              DROP TABLE  #AGING
                                            END

                                            CREATE TABLE #AGING(
                                            CUSTOMER_ID INT,
                                            CUSTOMER_NAME NVARCHAR(100),
                                            ROWHEAD_ID INT,
                                            BALANCE DECIMAL(18,4),
                                            PAYMENT DECIMAL(18,4))

                                            WHILE @i <= @INTERVAL + 1
                                            BEGIN

                                            SET @START = DATEADD(dd,-@DAYS,@END)

                                            INSERT INTO #AGING(CUSTOMER_ID,CUSTOMER_NAME,ROWHEAD_ID,BALANCE,PAYMENT)
                                            SELECT C.CUSTOMER_ID,
                                             LAST_NAME_KH + ' ' + FIRST_NAME_KH [CUSTOMER_NAME],
                                            @i AS [ROWHEAD_ID], 
                                            SUM(SETTLE_AMOUNT - ISNULL(TOTAL_ADJUST.ADJUST_AMOUNT,0) + ISNULL(ACC_ADJUST.ADJUST_AMOUNT,0)  -ISNULL(PAY.PAY_AMOUNT,0)) AS [BALANCE],
                                            ISNULL(SUM(PAY.PAY_AMOUNT),0) AS [PAYMENT]
                                            FROM TBL_INVOICE INV
                                            INNER JOIN TBL_CUSTOMER C ON INV.CUSTOMER_ID = C.CUSTOMER_ID
                                            LEFT JOIN (
	                                            SELECT PD.INVOICE_ID, SUM(PD.PAY_AMOUNT) AS PAY_AMOUNT
	                                            FROM TBL_PAYMENT_DETAIL PD INNER JOIN TBL_PAYMENT P ON P.PAYMENT_ID=PD.PAYMENT_ID
	                                            WHERE P.PAY_DATE <@DATE
	                                            GROUP BY PD.INVOICE_ID
	                                            ) PAY ON PAY.INVOICE_ID=INV.INVOICE_ID
                                            LEFT JOIN (
	                                            SELECT IA.INVOICE_ID , SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
	                                            FROM TBL_INVOICE_ADJUSTMENT IA 
	                                            GROUP BY IA.INVOICE_ID) TOTAL_ADJUST ON TOTAL_ADJUST.INVOICE_ID = INV.INVOICE_ID
                                            LEFT JOIN (
	                                            SELECT IA.INVOICE_ID , SUM(IA.ADJUST_AMOUNT) AS ADJUST_AMOUNT
	                                            FROM TBL_INVOICE_ADJUSTMENT IA 
	                                            WHERE IA.CREATE_ON <=@DATE
	                                            GROUP BY IA.INVOICE_ID) ACC_ADJUST ON ACC_ADJUST.INVOICE_ID = INV.INVOICE_ID
                                            	
                                            WHERE INV.INVOICE_STATUS NOT IN (3)
                                            AND (C.CUSTOMER_TYPE_ID = @CUSTOMER_TYPE_ID OR @CUSTOMER_TYPE_ID = 0)
                                            AND (
	                                            (INVOICE_DATE BETWEEN @START AND @END)
	                                             AND @i<@INTERVAL+1
	                                            )								  
                                            OR								  
	                                            (INVOICE_DATE < @END AND @i=@INTERVAL+1 )
                                               
                                            GROUP BY 
                                            C.CUSTOMER_ID, LAST_NAME_KH , FIRST_NAME_KH

                                            SET @i=@i+1
                                            SET @END = @START

                                            END


                                            SELECT x.CUSTOMER_ID,	
	                                               x.CUSTOMER_NAME,
	                                               c.CUSTOMER_CODE,
	                                               x.ROWHEAD_ID,
	                                               x.BALANCE
                                            FROM 
                                            (
	                                            SELECT 
	                                            CUSTOMER_ID , CUSTOMER_NAME ,ROWHEAD_ID, BALANCE FROM #AGING
	                                            WHERE BALANCE>0
                                            ) x INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=x.CUSTOMER_ID
                                            WHERE  (c.CUSTOMER_TYPE_ID = @CUSTOMER_TYPE_ID OR @CUSTOMER_TYPE_ID = 0);",
                
                "ALTER TABLE TBL_REPORT_SEND ALTER COLUMN IS_ENGLISH BIT NOT NULL;",
                //ADD COLUMN CAPACITY TO TBL_TRANSFORMER
                @"IF NOT EXISTS( SELECT *
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE COLUMN_NAME='CAPACITY_ID' AND TABLE_NAME='TBL_TRANSFORMER')
                BEGIN
                    ALTER TABLE TBL_TRANSFORMER
                    ADD CAPACITY_ID INT NULL;
                END;",
                "UPDATE  TBL_TRANSFORMER SET CAPACITY_ID =1;",
                "ALTER TABLE TBL_TRANSFORMER ALTER COLUMN CAPACITY_ID INT NOT NULL;",
                //CREATE TABLE CAPACITY
                @"IF NOT EXISTS( SELECT *
                FROM INFORMATION_SCHEMA.TABLES
                WHERE TABLE_NAME='TBL_CAPACITY')
                BEGIN
                    CREATE TABLE [dbo].[TBL_CAPACITY](
                        [CAPACITY_ID] [int] IDENTITY(1,1) NOT NULL,
                        [CAPACITY] [nvarchar](100) NOT NULL,
                        [IS_ACTIVE] [bit] NOT NULL,
                     CONSTRAINT [PK_TBL_CAPACITY] PRIMARY KEY CLUSTERED 
                    (
                        [CAPACITY_ID] ASC
                    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                    ) ON [PRIMARY]
                END"
                ,
                "INSERT INTO [TBL_CAPACITY](CAPACITY,IS_ACTIVE) VALUES(N'22kVA',1);"
                ,//CREATE TABLE DISTRIBUTION TYPE
                @"IF NOT EXISTS( SELECT *
                FROM INFORMATION_SCHEMA.TABLES
                WHERE TABLE_NAME='TBL_DISTRIBUTION_TYPE')
                BEGIN
                    CREATE TABLE [dbo].[TBL_DISTRIBUTION_TYPE](
	                    [DISTRIBUTION_ID] [int] IDENTITY(1,1) NOT NULL,
	                    [DISTRIBUTION_TYPE] [nvarchar](100) NOT NULL,
	                    [IS_ACTIVE] [bit] NOT NULL,
                     CONSTRAINT [PK_TBL_DISTRIBUTION_FACILITY] PRIMARY KEY CLUSTERED 
                    (
	                    [DISTRIBUTION_ID] ASC
                    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                    ) ON [PRIMARY]
                END",
                //CREATE TABLE DISTRIBUTION FACILITY
                @"IF NOT EXISTS( SELECT *
                FROM INFORMATION_SCHEMA.TABLES
                WHERE TABLE_NAME='TBL_DISTRIBUTION_FACILITY')
                BEGIN
                    CREATE TABLE [dbo].[TBL_DISTRIBUTION_FACILITY](
	                    [DISTRIBUTION_DETAIL_ID] [int] IDENTITY(1,1) NOT NULL,
	                    [DISTRIBUTION_ID] [int] NOT NULL,
	                    [VOLTAGE_OF_LINE] [decimal](18, 4) NOT NULL,
	                    [PHASE_ID] [int] NOT NULL,
	                    [LENGTH_OF_LINE] [decimal](18, 4) NOT NULL,
	                    [REMARK] [nvarchar](100) NOT NULL,
	                    [IS_ACTIVE] [bit] NOT NULL,
                     CONSTRAINT [PK_TBL_DISTRIBUTION_DETAIL] PRIMARY KEY CLUSTERED 
                    (
	                    [DISTRIBUTION_DETAIL_ID] ASC
                    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                    ) ON [PRIMARY]
                END",
                //CREATE TABLE PURCHASE AGREEMENT
                @"IF NOT EXISTS( SELECT *
                FROM INFORMATION_SCHEMA.TABLES
                WHERE TABLE_NAME='TBL_PURCHASE_AGREEMENT')
                BEGIN
	                CREATE TABLE [dbo].[TBL_PURCHASE_AGREEMENT](
		                [AGREEMENT_ID] [int] IDENTITY(1,1) NOT NULL,
		                [SELLER_NAME] [nvarchar](100) NOT NULL,
		                [SIGN_DATE] [datetime] NOT NULL,
		                [VOLTAGE_ID] [int] NOT NULL,
		                [NO_OF_CONNECTION] [int] NOT NULL,
		                [PRICE] [decimal](18, 4) NOT NULL,
		                [IS_ACTIVE] [bit] NOT NULL,
	                 CONSTRAINT [PK_TBL_PURCHASE_AGREEMENT] PRIMARY KEY CLUSTERED 
	                (
		                [AGREEMENT_ID] ASC
	                )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	                ) ON [PRIMARY]
                END",
                //CREATE TABLE POWER PURCHASE AGREEMENT
                @"IF NOT EXISTS( SELECT *
                FROM INFORMATION_SCHEMA.TABLES
                WHERE TABLE_NAME='TBL_POWER_PURCHASE_AGREEMENT')
                BEGIN
	                CREATE TABLE [dbo].[TBL_POWER_PURCHASE_AGREEMENT](
		                [POWER_PURCHASE_ID] [int] IDENTITY(1,1) NOT NULL,
		                [AGREEMENT_ID] [int] NOT NULL,
		                [MONTH] [datetime] NOT NULL,
		                [POWER_PURCHASE] [decimal](18, 4) NOT NULL,
		                [PRICE] [decimal](18, 4) NOT NULL,
		                [UNIT_OF_PRICE] [nvarchar](100) NOT NULL,
		                [NOTE] [nvarchar](200) NOT NULL,
		                [IS_ACTIVE] [bit] NOT NULL,
	                 CONSTRAINT [PK_TBL_POWER_PURCHASE_AGREEMENT] PRIMARY KEY CLUSTERED 
	                (
		                [POWER_PURCHASE_ID] ASC
	                )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	                ) ON [PRIMARY]
                END",
                //INSERT NEW [TABLE NAME] TO TLKP_TABLE
                @"INSERT INTO TLKP_TABLE
                SELECT TABLE_NAME, TABLE_NAME,1 ,1FROM INFORMATION_SCHEMA.TABLES
                WHERE TABLE_NAME NOT IN (SELECT TABLE_NAME FROM TLKP_TABLE UNION SELECT 'sysdiagrams')",
                //INSERT NEW [FIELD NAME] TO TLKP_FFIELD
                @"INSERT INTO TLKP_TABLE_FIELD
                SELECT col.COLUMN_NAME,col.COLUMN_NAME,tbl.TABLE_ID,1,'','',0
                FROM INFORMATION_SCHEMA.COLUMNS col
                    INNER JOIN TLKP_TABLE tbl ON tbl.TABLE_NAME=col.TABLE_NAME
                WHERE tbl.IS_CHANGE_LOG_USED = 1
                AND
                tbl.TABLE_NAME+'.'+col.COLUMN_NAME NOT IN (
                    SELECT tbl.TABLE_NAME+'.'+col.FIELD_NAME
                    FROM TLKP_TABLE_FIELD col INNER JOIN TLKP_TABLE tbl ON tbl.TABLE_ID=col.TABLE_ID
                )",
                //GENERATE INDEXT INTO TABLE
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_POLE') CREATE INDEX IX_TBL_POLE ON TBL_POLE(AREA_ID);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'TBL_POLE_TRANSFORMER_X') CREATE INDEX TBL_POLE_TRANSFORMER_X ON TBL_POLE(TRANSFORMER_ID);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_USAGE_1') CREATE INDEX IX_TBL_USAGE_1 ON TBL_USAGE(CUSTOMER_ID);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_USAGE') CREATE INDEX IX_TBL_USAGE ON TBL_USAGE(METER_ID);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_CUSTOMER_METER') CREATE INDEX IX_TBL_CUSTOMER_METER ON TBL_CUSTOMER_METER(CUSTOMER_ID);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_CUSTOMER_METER_2') CREATE INDEX IX_TBL_CUSTOMER_METER_2 ON TBL_CUSTOMER_METER(POLE_ID);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_CUSTOMER_METER_3') CREATE INDEX IX_TBL_CUSTOMER_METER_3 ON TBL_CUSTOMER_METER(BOX_ID);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_CUSTOMER_METER_1') CREATE INDEX IX_TBL_CUSTOMER_METER_1 ON TBL_CUSTOMER_METER(METER_ID);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_TMP_USAGE_MOBILE') CREATE INDEX IX_TBL_TMP_USAGE_MOBILE ON TBL_TMP_USAGE_MOBILE(METER_ID);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_CUSTOMER_3') CREATE INDEX IX_TBL_CUSTOMER_3 ON TBL_CUSTOMER(AREA_ID);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_CUSTOMER') CREATE INDEX IX_TBL_CUSTOMER ON TBL_CUSTOMER(BILLING_CYCLE_ID);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_CUSTOMER_2') CREATE INDEX IX_TBL_CUSTOMER_2 ON TBL_CUSTOMER(CUSTOMER_TYPE_ID);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_CUSTOMER_1') CREATE INDEX IX_TBL_CUSTOMER_1 ON TBL_CUSTOMER(CUSTOMER_CODE);",
                "IF NOT EXISTS (SELECT name from sys.indexes WHERE name = N'IX_TBL_CIRCUIT_BREAKER') CREATE INDEX IX_TBL_CIRCUIT_BREAKER ON TBL_CIRCUIT_BREAKER(BREAKER_CODE);",
                @"TRUNCATE TABLE TBL_PERMISSION;

                SET IDENTITY_INSERT TBL_PERMISSION ON;

                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(8,N'បង់ប្រាក់មុន',N'PREPAID',1,0,1); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(9,N'បង់ប្រាក់ក្រោយ',N'POSTPAID',2,0,1); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(10,N'ទទួលប្រាក់បង់',N'PAYMENT',1,9,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(11,N'បញ្ចូលការប្រើប្រាស់',N'INPUTUSAGE',3,9,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(15,N'ឩបករណ៍',N'DEVICE',4,9,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(16,N'ទាញយកការប្រើប្រាស់',N'COLLECTUSAGE',0,15,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(17,N'បញ្ចូលព័ត៌មានថ្មី',N'IMPORTUSAGE',0,15,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(18,N'អតិថិជន និងការទូទាត់',N'CUSTOMERANDBILLING',3,0,1); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(19,N'អតិថិជន',N'CUSTOMER',0,18,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(20,N'គ្រប់គ្រងអតិថិជន',N'REGISTER',0,19,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(21,N'ចាប់ផ្តើមប្រើ',N'ACTIVATE',21,19,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(22,N'ផ្តាច់ចរន្ត',N'BLOCK',0,19,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(23,N'ភ្ជាប់ចរន្ត',N'UNBLOCK',0,19,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(24,N'កំណត់ការទូទាត់',N'SETUPRUNBILL',0,18,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(25,N'តំលៃអគ្គិសនី',N'PRICE',0,24,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(26,N'ជុំនៃការទូទាត់',N'BILLINGCYCLE',0,24,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(27,N'ប្រភេទសេវាកម្ម',N'INVOICEITEMTYPE',0,24,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(28,N'សេវាកម្ម',N'INVOICEITEM',0,24,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(29,N'ការចុះតំលៃ',N'DISCOUNT',0,24,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(30,N'ប្រតិបត្តិការណ៍ទូទាត់',N'BILLINGOPERATION',0,18,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(31,N'ទូទាត់ការប្រើប្រាស់',N'RUNBILL',0,30,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(32,N'បោះពុម្ពវិក័្កយបត្រ',N'PRINTINVOICE',0,30,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(33,N'ប្រវត្តិនៃការទូទាត់',N'BILLINGHISTORY',0,30,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(34,N'គ្រប់គ្រង់ទូទៅ',N'ADMIN',4,0,1); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(35,N'ក្រុមហ៊ុន',N'COMPANY',1,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(36,N'តំបន់',N'AREA',2,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(37,N'បង្កោល និងប្រអប់',N'POLEANDBOX',3,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(38,N'ប្រភេទកុងទ័រ',N'METERTYPE',4,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(39,N'កុងទ័រ',N'METER',5,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(40,N'ប្រភេទឌីសុងទ័រ',N'CIRCUITBREAKERTYPE',6,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(41,N'ឌីសុងទ័រ',N'CIRCUITBREAKER',7,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(42,N'សំភារៈប្រើប្រាស់',N'EQIPMENT',8,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(43,N'ឩបករណ៍',N'DEVICEREGISTER',9,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(44,N'បុគ្គលិក',N'EMPLOYEE',10,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(45,N'កេះដាក់ប្រាក់',N'CASTDRAWER',11,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(46,N'ថ្លៃភ្ជាប់ចរន្ត',N'CONNECTFEE',0,24,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(47,N'ប្រភេទប្រើប្រាស់',N'CUSTOMERTYPE',12,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(48,N'ខ្នាតអគ្គិសនី',N'UNITELECTRICITY',4,132,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(49,N'អាំងតង់ស៊ីតេ',N'AMPARE',2,48,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(50,N'តង់ស្យុង',N'VOLTAGE',1,48,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(51,N'ហ្វា(Phase)',N'PHASE',3,48,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(52,N'កុងស្តង់',N'CONTANT',4,48,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(53,N'លក្ខណៈសំណរ',N'SEAL',5,48,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(54,N'ផ្សេងៗ',N'OTHER',14,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(55,N'លក្ខ័ណប្រើប្រាស់',N'USERCONDITION',0,54,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(57,N'សុវត្ថិភាពប្រព័ន្ធ',N'SECURITY',6,0,1); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(58,N'អ្នកប្រើ',N'USER',0,57,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(59,N'តួនាទី',N'PERMISION',0,57,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(60,N'ទិន្នន័យ',N'DATA',0,57,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(61,N'ចំលងទុក(Backup)',N'DATABACKUP',61,60,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(62,N'ជំនួស(Restore)',N'DATARESTORE',0,60,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(63,N'ការប្រើប្រាស់',N'USING',0,57,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(64,N'ប្រវត្តិប្រតិបត្តិការណ៍',N'VIEWCHANGELOG',0,63,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(66,N'របាយការណ៍',N'REPORT',7,0,1); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(67,N'ប្រតិបត្តិការសង្ខេបក្នុងខែ',N'TRIALBALANCEDAILY',67,66,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(68,N'អតិថិជនលំអិតក្នុងខែ',N'TRIALBALANCEDETAIL',68,66,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(69,N'បំណុល និង ការបង់ប្រាក់ក្នុងខែ',N'TRIALBALANCEARDETAIL',69,66,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(70,N'ការប្រាក់កក់លំអិតក្នុងខែ',N'TRIALBALANCEDEPOSITDETAIL',70,66,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(71,N'បំណុលតាមកាល (Aging Report)',N'AGING',71,66,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(72,N'របាយការណ៍ប្រចាំខែ',N'MONTHLY',101,66,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(73,N'ចំណូលអគ្គីសនី',N'POWERINCOME',1,72,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(74,N'ចំណូលផ្សេងៗ',N'REVENUEBYMONTH',2,72,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(75,N'របាយការណ៍ប្រចាំឆ្នាំ',N'YEARLY',102,66,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(76,N'ចំណូលពីការលក់អគ្គីសនី',N'POWERINCOME',76,75,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(77,N'ការផលិតថាមពល',N'POWERUSAGE',77,75,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(78,N'ទូទាត់ថ្លៃសេវាកម្ម',N'FIXCHARGE',0,20,4); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(79,N'បន្ថែម',N'INSERT',0,20,4); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(80,N'កែប្រែ',N'UPDATE',0,20,4); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(81,N'ឈប់ប្រើប្រាស់',N'CLOSE',0,20,4); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(82,N'កែប្រាក់កក់',N'ADJUSTDESPOSIT',83,80,5); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(83,N'ប្តូរកុងទ័រ',N'CHANGEMETER',0,80,5); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(84,N'ប្តូរឌីសុងទ័រ',N'CHANGECIRCUITBREAKER',0,80,5); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(85,N'ប្តូរប្រអប់',N'CHANGEBOX',0,80,5); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(86,N'កែសំរូលការប្រើប្រាស់',N'ADJUSTUSAGE',0,80,5); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(87,N'កែសំរូលលើបំណុល',N'ADJUSTAMOUNT',0,80,5); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(88,N'បោះពុម្ព',N'PRINT',0,80,5); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(89,N'មើលព័ត៌មានអតិថិជន',N'VIEWCUSTOMER',0,20,4); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(90,N'IR Reader',N'IRREADER',5,9,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(91,N'ទាញយកការប្រើប្រាស់',N'COLLECTUSAGE',91,90,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(92,N'ចុះបញ្ចីកុងទ័រឌីជីថល',N'REGISTERMETER',92,90,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(93,N'កែប្រែការកក់ប្រាក់',N'ADUSTDEPOSIT',0,80,5); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(95,N'កាលវិភាគផ្ងើរអ៊ីមែល',N'SCHEDULEREPORT',95,54,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(96,N'ប្រតិបត្តិការសាច់ប្រាក់សង្ខេប',N'CASHINGSUMARY',72,66,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(97,N'ប្រតិបត្តិការសាច់ប្រាក់លំអិត',N'CASHINGDETAIL',73,66,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(99,N'បញ្ជីអតិថិជន',N'CUSTOMERS',99,72,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(101,N'ការចុះតំលៃ',N'DISCOUNT',101,72,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(102,N'លុបការបង់ប្រាក់',N'CANCELPAYMENT',3,9,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(103,N'គ្រប់គ្រង់អតិថិជន',N'REGISTER',0,8,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(104,N'ទូទាត់ថ្លៃសេវាកម្ម',N'FIXCHARGE',0,103,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(105,N'បន្ថែម',N'INSERT',0,103,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(106,N'កែប្រែ',N'UPDATE',0,103,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(107,N'ប្តូរកុងទ័រ',N'CHANGEMETER',0,106,4); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(108,N'ប្តូរឌីសុងទ័រ',N'CHANGEBREAKER',0,106,4); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(109,N'ប្តូរប្រអប់',N'CHANGEBOX',0,106,4); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(110,N'កែប្រាក់កក់',N'ADJUSTDESPOSIT',0,106,4); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(111,N'ឈប់ប្រើប្រាស់',N'CLOSE',0,103,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(112,N'មើលព័ត៌មានអតិថិជន',N'VIEWCUSTOMER',0,103,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(113,N'ចាប់ផ្តើមប្រើ',N'ACTIVATE',0,103,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(114,N'ប្រតិបត្តិការណប្រចាំថ្ងៃ',N'DIALYOPERATION',0,8,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(115,N'លក់ថាមពលអគ្គិសនី',N'SALEPOWER',0,114,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(116,N'លុបការទិញអគ្គិសនី',N'VOIDPOWER',0,114,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(117,N'បង្កើតកាតថ្មី',N'REMAKECARD',0,114,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(118,N'បង្កើតកាតផ្សេងៗ',N'CREATEOTHERCARD',0,114,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(120,N'កែសំរួលលើបំណុល',N'ADJUSTAMOUNT',0,106,4); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(121,N'បោះពុម្ព',N'PRINT',0,106,4); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(122,N'ការប្រើប្រាស់ថាមពល',N'USAGE',4,72,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(123,N'របាយការណ៏បង់ប្រាក់មុន',N'PREPAID',103,66,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(124,N'ចំណូលពីការលក់អគ្គិសនី',N'SALEPOWERDETAIL',0,123,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(125,N'ចំណូលពីការលក់អគ្គិសនីសង្ខេប',N'SALEPOWERSUMMARY',0,123,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(126,N'ត្រង់ស្វូ',N'TRANSFORMER',2,34,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(127,N'ការផលិតថាមពល',N'POWER',3,72,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(128,N'ទូទាត់តាមធនាគារ',N'BANKPAYMENT',6,9,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(129,N' បង្កើតបញ្ជីបង់ប្រាក់',N'EXPORT',11,128,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(130,N'ទាញយកការបង់ប្រាក់',N'IMPORT',12,128,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(131,N'បោះពុម្ភបង្កាន់ដៃ',N'REPRINTRECIEPT',2,9,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(132,N'គ្រប់គ្រងថាមពល',N'POWER',5,0,1); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(133,N'ប្រភេទបណ្តាញ',N'DISTRIBUTION_TYPE',1,138,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(134,N'មធ្យោបាយចែកចាយ',N'DISTRIBUTION_FACILITY',2,138,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(135,N'កិច្ចព្រមព្រៀងទិញថាមពល',N'AGREEMENT',1,139,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(136,N'ទិញថាមពលអគ្គិសនី',N'POWERAGREEMENT',2,139,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(137,N'កំលាំង(kVA)',N'CAPACITY',0,48,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(138,N'ការចែកចាយថាមពល',N'DISTRIBUTION',1,132,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(139,N'ការទិញថាមពល',N'PURCHASEPOWER',2,132,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(140,N'ការប្រើប្រាស់ថាមពល',N'USEPOWER',3,132,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(141,N'ថាមពលទាំងអស់',N'POWER',0,140,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(142,N'ថាមពលតាមត្រង់ស្វូ',N'POWERBYTRANSOFRMAER',142,140,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(143,N'បញ្ចូនព័ត៌មានថ្មីៗ',N'SENDDATATOIR',0,90,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(144,N'របាយការណ័ទិញថាមពលអគ្គិសនី',N'PURCHASEPOWER',104,66,2); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(145,N'ទិញថាមពលអគ្គិសនី',N'PURCHASEPOWER',0,144,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(146,N'កិច្ចព្រមព្រៀងទិញថាមពលអគ្គិសនី',N'AGREEMENT',1,144,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(147,N'មធ្យោបាយចែកចាយថាមពលអគ្គិសនី',N'DISTRIBUTIONFACILITY',2,144,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(148,N'ព័ត៌មានលំអិតអំពីត្រង់ស្វូ',N'TRANSFORMERDETAIL',3,144,3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(149,N'អំណានចាស់',N'STARTUSAGE',1,11,	3); 
                INSERT INTO TBL_PERMISSION(PERMISSION_ID, PERMISSION_DISPLAY,  PERMISSION_NAME,  PERMISSION_ORDER,  PERMISSION_PARENT_ID,  PERMISSION_LEVEL) VALUES(150,N'អំណានថ្មី',N'ENDUSAGE',	2,	11,	3); 

                SET IDENTITY_INSERT TBL_PERMISSION OFF; 
                 

                TRUNCATE TABLE TBL_ROLE_PERMISSION; 

                INSERT INTO TBL_ROLE_PERMISSION 
                SELECT ROLE_ID=1,PERMISSION_ID,IS_ALLOWED=1
                FROM TBL_PERMISSION 
                WHERE PERMISSION_ID <>149 "};

                foreach (string item in strSqlStatement)
                {
                    DBDataContext.Db.ExecuteCommand(item);
                }
                DBDataContext.Db.TBL_UTILITies.FirstOrDefault(row => row.UTILITY_ID == (int)Utility.VERSION).UTILITY_VALUE = "1.0.0.3";
                DBDataContext.Db.SubmitChanges(); 
                tran.Complete();
            }

        }
    }
}
