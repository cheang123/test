﻿namespace EPower.Update
{
    class Version1_2_2_1 : Version
    {
        /*
         
         @2019-July-29 By Prak Serey
         1. Update Android IR to Version 1.9
            . Fix Issue clear tamper meter.

         @2019-July-29 By Hor Dara
         1. Update default subject mail(report power licensee)
         2. Modify DialogCustomerBatchChangeOption
         3. Modify DialogCustomerBatchChange2
         4. Invisibles Column ROW_DATE on PageCapacity
         5. Add REPORT_PAYMENT_DETAIL_BY_RECIEPT
         6. Modify REPORT_CUSTOMER_USED (CUSTOMER_TYPE_ID->CUSTOMER_CONNECTION_TYPE_ID)
         7. Update REPORT_POWER_SOLD_LICENSEE
         8. Update DialogCustomerCharge (check condition invoice item = new connection)
         9. Update DialogCustomerChangeMeter (add event keydown)
         10. Update PageCustomerActivateList (add flow layout panel for buttons)
         11. Update REPORT_ACCOUNT_TRAN_DETAIL (On Power Purchase Insert detail)
         12. Modify REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE (CUSTOMER_TYPE_ID->CUSTOMER_CONNECTION_TYPE_ID)
         13. Modify REPORT_CUSTOMERS_DEPOSIT_CONNECTION_FEE (CUSTOMER_TYPE_ID->CUSTOMER_CONNECTION_TYPE_ID)
         14. Auto Insert missed invoice detail (When open ref invoice)
         15. alter column 'Note' on 'TBL_CUS_DEPOSIT' at more lengths

         @2019-August-13 By Kheang Kimkhorn
         1. TRUNCATE TABLE TBL_EBL_PROCESS
         2. Downgrade Android IR to Version 1.6
            . Fix Issue clear tamper meter.
        */
        protected override string GetBatchCommand()
        {
            return @"
GO
IF OBJECT_ID('REPORT_PAYMENT_DETAIL_BY_RECIEPT')IS NOT NULL
  DROP PROC REPORT_PAYMENT_DETAIL_BY_RECIEPT
GO
/*
@01-Jul-2019 BY Hor Dara
	1. Create New Report REPORT_PAYMENT_DETAIL_BY_RECIEPT
*/
CREATE PROC [dbo].[REPORT_PAYMENT_DETAIL_BY_RECIEPT]
	@D1 DATETIME='2019-07-01',
	@D2 DATETIME='2019-07-01',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@INCLUDE_DELETE BIT=0,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0,
	@BILLING_CYCLE_ID INT=0, 
	@PAYMENT_ACCOUNT_ID INT=0,
	@DATA_MONTH DATETIME='1900-01-01'
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT DISTINCT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH + ' ' + c.FIRST_NAME_KH,
		r.AREA_NAME,
		p.PAYMENT_NO,
		p.PAY_DATE,
		--INVOICE_TITLE,
		p.CREATE_BY, 
		ISNULL(p.PAY_AMOUNT,0) AS [AMOUNT]
FROM TBL_PAYMENT p 
INNER JOIN TBL_PAYMENT_DETAIL pd ON pd.PAYMENT_ID=p.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID = pd.INVOICE_ID 
OUTER APPLY(
	SELECT TOP(1) invd.INVOICE_ID,itm.INVOICE_ITEM_ID,itm.INVOICE_ITEM_NAME,itm.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL invd 
	INNER JOIN TBL_INVOICE_ITEM itm ON invd.INVOICE_ITEM_ID=itm.INVOICE_ITEM_ID
	WHERE invd.INVOICE_ID=i.INVOICE_ID
)itm
INNER JOIN TBL_CUSTOMER c ON i.CUSTOMER_ID = c.CUSTOMER_ID 
INNER JOIN TBL_AREA r ON r.AREA_ID = c.AREA_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
WHERE  (p.PAY_DATE BETWEEN @D1 AND @D2) 
		AND (@ITEM_ID = 0 OR INVOICE_ITEM_ID=@ITEM_ID)
		AND (@ITEM_TYPE_ID = 0 OR INVOICE_ITEM_TYPE_ID = @ITEM_TYPE_ID)
		-- AND pd.PAY_AMOUNT>0
        -- filter payment that removed
		AND (@INCLUDE_DELETE=1 OR p.IS_VOID = 0)
		AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
		AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND (@PAYMENT_ACCOUNT_ID=0 OR p.PAYMENT_ACCOUNT_ID=@PAYMENT_ACCOUNT_ID)
		AND (@DATA_MONTH='1900-01-01' OR i.INVOICE_MONTH=@DATA_MONTH)
ORDER BY PAY_DATE
GO
IF OBJECT_ID('REPORT_CUSTOMER_USED')IS NOT NULL
  DROP PROC REPORT_CUSTOMER_USED

GO
CREATE PROC REPORT_CUSTOMER_USED
	@MONTH DATETIME='2012-08-01',
	@CYCLE_ID INT=0,
	@AREA_ID INT=1,
	@CUSTOMER_CONNECTION_TYPE_ID INT=0,
	@PRICE_ID INT =0
AS

-- 2014-DEC-29 SMEY : ADD ORDER BY AREA,POLE,BOX
-- 2016-JUL-21 SEREY: GET PREPAID USAGE CUSTOMER FROM RUN_CREDIT
-- 2019-AUG-02 DARA: Modify filter from customer_type_id->customer_connection_type_id

SELECT	c.CUSTOMER_CODE
		,FULL_NAME=c.LAST_NAME_KH+' '+c.FIRST_NAME_KH
		,a.AREA_NAME
		,a.AREA_ID
		,c.ACTIVATE_DATE
		,am.AMPARE_NAME
		,p.POLE_CODE
		,b.BOX_CODE
		,METER_CODE=REPLACE(LTRIM(REPLACE(ISNULL(m.METER_CODE,'-'),'0',' ')),' ','0')
		,t.*
		,RAT = STD*100/NULLIF(AVG,0)
FROM (
--CUSTOMER POSTPAID--
SELECT	c.CUSTOMER_ID,
		
		C0 = SUM(CASE WHEN DATEDIFF(M,USAGE_MONTH,@MONTH)=0 THEN TOTAL_USAGE ELSE 0 END),
		C1 = SUM(CASE WHEN DATEDIFF(M,USAGE_MONTH,@MONTH)=1 THEN TOTAL_USAGE ELSE 0 END),
		C2 = SUM(CASE WHEN DATEDIFF(M,USAGE_MONTH,@MONTH)=2 THEN TOTAL_USAGE ELSE 0 END),
		C3 = SUM(CASE WHEN DATEDIFF(M,USAGE_MONTH,@MONTH)=3 THEN TOTAL_USAGE ELSE 0 END),
		C4 = SUM(CASE WHEN DATEDIFF(M,USAGE_MONTH,@MONTH)=4 THEN TOTAL_USAGE ELSE 0 END),
		C5 = SUM(CASE WHEN DATEDIFF(M,USAGE_MONTH,@MONTH)=5 THEN TOTAL_USAGE ELSE 0 END),
		C6 = SUM(CASE WHEN DATEDIFF(M,USAGE_MONTH,@MONTH)=6 THEN TOTAL_USAGE ELSE 0 END),
		C7 = SUM(CASE WHEN DATEDIFF(M,USAGE_MONTH,@MONTH)=7 THEN TOTAL_USAGE ELSE 0 END),
		C8 = SUM(CASE WHEN DATEDIFF(M,USAGE_MONTH,@MONTH)=8 THEN TOTAL_USAGE ELSE 0 END),
		C9 = SUM(CASE WHEN DATEDIFF(M,USAGE_MONTH,@MONTH)=9 THEN TOTAL_USAGE ELSE 0 END),
		C10 = SUM(CASE WHEN DATEDIFF(M,USAGE_MONTH,@MONTH)=10 THEN TOTAL_USAGE ELSE 0 END),
		C11 = SUM(CASE WHEN DATEDIFF(M,USAGE_MONTH,@MONTH)=11 THEN TOTAL_USAGE ELSE 0 END),
		AVG = AVG(TOTAL_USAGE),
		STD = STDEVP (TOTAL_USAGE)
FROM(
	SELECT	c.CUSTOMER_ID, 
			u.USAGE_MONTH,
			TOTAL_USAGE= u.MULTIPLIER * (CASE 
				WHEN IS_METER_RENEW_CYCLE=0 THEN END_USAGE-START_USAGE
				ELSE (SELECT CONVERT(INT,REPLICATE('9',LEN(CONVERT(NVARCHAR,CONVERT(INT,START_USAGE))))))-START_USAGE+END_USAGE+1
			END) 	
	FROM TBL_USAGE u 
	INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = u.CUSTOMER_ID
	WHERE c.CUSTOMER_ID=u.CUSTOMER_ID AND c.STATUS_ID NOT IN(4, 5) AND c.IS_POST_PAID=1 
		 AND DATEDIFF(M,u.USAGE_MONTH,@MONTH)<12
	
	UNION ALL
	-- PREPAID 
	-- Get from Credit Month--
	--------------------------
	SELECT 
		c.CUSTOMER_ID,
		pc.CREDIT_MONTH,
		pc.USAGE
	FROM TBL_CUSTOMER c
	INNER JOIN TBL_PREPAID_CREDIT pc ON c.CUSTOMER_ID = pc.CUSTOMER_ID
	WHERE DATEDIFF(M,pc.CREDIT_MONTH,@MONTH)<12
		AND pc.CREDIT_MONTH>='2016-03-01' 
	
	-- Get from Customer Buy--
	--------------------------
	UNION ALL
	SELECT	c.CUSTOMER_ID,
			cb.CREATE_ON,
			cb.BUY_QTY 
	FROM TBL_PREPAID_CUSTOMER pc 
	INNER JOIN TBL_CUSTOMER_BUY cb ON pc.PREPAID_CUS_ID = cb.PREPAID_CUS_ID
	INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = pc.CUSTOMER_ID 
	WHERE c.IS_POST_PAID=0 AND c.STATUS_ID NOT IN(4, 5)
	 AND DATEDIFF(M,cb.CREATE_ON,@MONTH)<12
	 AND CONVERT(NVARCHAR(7),cb.CREATE_ON,126)+'-01' <='2016-02-29' 
) x
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = x.CUSTOMER_ID
GROUP BY c.CUSTOMER_ID	

) t
LEFT JOIN TBL_CUSTOMER c ON t.CUSTOMER_ID=c.CUSTOMER_ID
LEFT JOIN TBL_CUSTOMER_METER cm ON c.CUSTOMER_ID = cm.CUSTOMER_ID  AND cm.IS_ACTIVE=1
LEFT JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
LEFT JOIN TBL_POLE p ON p.POLE_ID=cm.POLE_ID
LEFT JOIN TBL_BOX b ON b.BOX_ID=cm.BOX_ID
LEFT JOIN TBL_METER m ON m.METER_ID =cm.METER_ID 
LEFT JOIN TBL_AMPARE am ON am.AMPARE_ID=c.AMP_ID
WHERE (@CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@CYCLE_ID)
  AND (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
  AND (@CUSTOMER_CONNECTION_TYPE_ID=0 OR c.CUSTOMER_CONNECTION_TYPE_ID=@CUSTOMER_CONNECTION_TYPE_ID)
  AND (@PRICE_ID=0 OR c.PRICE_ID=@PRICE_ID)
  
ORDER BY 3,7,8
  --AND STD*100/NULLIF(AVG,0)>50
GO
IF OBJECT_ID('REPORT_POWER_SOLD_LICENSEE')IS NOT NULL
  DROP PROC REPORT_POWER_SOLD_LICENSEE
GO
CREATE PROC [dbo].[REPORT_POWER_SOLD_LICENSEE]
	@MONTH DATETIME='2019-01-01',
	@START DATETIME='2019-01-01',
	@END DATETIME='2019-01-28'
AS
-- BUILD DATA VALUE TABLE
SELECT	i.INVOICE_MONTH,
		i.INVOICE_NO,
		l.LICENSEE_ID,
		li.LICENSEE_NAME,
		li.LICENSEE_NO, 
		TOTAL_USAGE = i.TOTAL_USAGE - ISNULL(adj.ADJUST_USAGE,0),
		d.PRICE,
		i.CURRENCY_ID,
		cu.CURRENCY_NAME,
		cu.CURRENCY_SING,
		i.TOTAL_AMOUNT
FROM TBL_INVOICE i 
INNER JOIN TBL_LICENSEE_CONNECTION l ON l.CUSTOMER_ID = i.CUSTOMER_ID AND l.IS_ACTIVE=1
INNER JOIN TLKP_CURRENCY cu ON i.CURRENCY_ID=cu.CURRENCY_ID
INNER JOIN TBL_LICENSEE li ON li.LICENSEE_ID=l.LICENSEE_ID AND li.IS_ACTIVE=1
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
OUTER APPLY(
	SELECT TOP 1 PRICE 
	FROM TBL_INVOICE_DETAIL 
	WHERE INVOICE_ID = i.INVOICE_ID
	ORDER BY INVOICE_DETAIL_ID DESC
) d
OUTER APPLY(
		SELECT ADJUST_USAGE = SUM(ADJUST_USAGE)
		FROM TBL_INVOICE_ADJUSTMENT   
		WHERE INVOICE_ID =  i.INVOICE_ID
) adj 
WHERE i.INVOICE_MONTH =@MONTH  
	AND i.IS_SERVICE_BILL=0 
	AND c.IS_REACTIVE=0
UNION ALL 
SELECT	CONVERT(NVARCHAR(7),CREATE_ON,126)+'-01',
		b.BUY_NO,
		l.LICENSEE_ID,
		li.LICENSEE_NAME,
		li.LICENSEE_NO,  
		b.BUY_QTY,
		b.PRICE,
		b.CURRENCY_ID,
		cu.CURRENCY_NAME,
		cu.CURRENCY_SING,
		b.BUY_AMOUNT
FROM TBL_CUSTOMER_BUY b
INNER JOIN TBL_PREPAID_CUSTOMER p ON p.PREPAID_CUS_ID = b.PREPAID_CUS_ID
INNER JOIN TBL_LICENSEE_CONNECTION l ON l.CUSTOMER_ID = p.CUSTOMER_ID AND l.IS_ACTIVE=1
INNER JOIN TLKP_CURRENCY cu ON b.CURRENCY_ID=cu.CURRENCY_ID
INNER JOIN TBL_LICENSEE li ON li.LICENSEE_ID=l.LICENSEE_ID AND li.IS_ACTIVE=1
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = l.CUSTOMER_ID
WHERE b.CREATE_ON BETWEEN @START AND @END 
	 AND c.IS_REACTIVE = 0ORDER BY l.LICENSEE_ID
GO
IF OBJECT_ID('REPORT_ACCOUNT_TRAN_DETAIL')IS NOT NULL
  DROP PROC REPORT_ACCOUNT_TRAN_DETAIL
GO
CREATE PROC [dbo].[REPORT_ACCOUNT_TRAN_DETAIL]
	@D1 DATETIME = '2014-01-01',
	@D2 DATETIME = '2015-10-30',
	@TYPE_ID INT = 2,
	@TRAN_ACCOUNT_ID INT = 0,
	@PAYMENT_ACCOUNT_ID INT = 0,
	@TRAN_CURRENCY_ID INT = 0,
	@DISPLAY_CURRENCY_ID INT = 2
AS  
DECLARE @SIGN NVARCHAR(50);
SELECT @SIGN =CURRENCY_SING
FROM TLKP_CURRENCY WHERE CURRENCY_ID=@DISPLAY_CURRENCY_ID;
  
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));
SELECT 	TRAN_ACCOUNT_ID , 
		PAYMENT_ACCOUNT_ID, 
		TRAN_ID = CONVERT(INT,tr.TRAN_ID),
		TRAN_TYPE=CONVERT(NVARCHAR(50),N'ចំណូល/ចំណាយ'),
		tr.TRAN_BY,
		tr.TRAN_DATE,
		AMOUNT = dbo.ROUND_BY_CURRENCY(dbo.GET_EXCHANGE_RATE(tr.CURRENCY_ID,@DISPLAY_CURRENCY_ID,tr.TRAN_DATE)*tr.AMOUNT,@DISPLAY_CURRENCY_ID),
		tr.REF_NO,
		NOTE = CONVERT(NVARCHAR(MAX),NOTE),
		CURRENCY = @SIGN
INTO #DETAIL
FROM TBL_ACCOUNT_TRAN tr
WHERE tr.IS_ACTIVE=1
	  AND tr.TRAN_DATE BETWEEN @D1 AND @D2
	  AND tr.ACCOUNT_TYPE_ID = @TYPE_ID
	  AND tr.TRAN_ACCOUNT_ID IN(SELECT ACCOUNT_ID FROM dbo.GET_ACCOUNT_CHART_ACENSTORS(@TRAN_ACCOUNT_ID))
	  AND tr.PAYMENT_ACCOUNT_ID IN(SELECT ACCOUNT_ID FROM dbo.GET_ACCOUNT_CHART_ACENSTORS(@PAYMENT_ACCOUNT_ID))
	  AND (@TRAN_CURRENCY_ID=0 OR tr.CURRENCY_ID=@TRAN_CURRENCY_ID)

-- POWER PURCHASE
IF @TYPE_ID = 2
INSERT INTO #DETAIL
SELECT	TRAN_ACCOUNT_ID = ACCOUNT_ID,
		PAYMENT_ACCOUNT_ID =0 , 
		TRAN_ID = POWER_PURCHASE_ID,
		TRAN_TYPE=N'ទិញថាមពល',
		TRAN_BY = '-' ,
		TRAN_DATE = p.[MONTH],
		AMOUNT =  ((pd.POWER_PURCHASE+pd.ADJUST_USAGE)*p.PRICE)*dbo.GET_EXCHANGE_RATE(p.CURRENCY_ID,@DISPLAY_CURRENCY_ID,p.MONTH),
		REF_NO = '-' ,
		NOTE = SELLER_NAME,
		CURRENCY = @SIGN  
FROM TBL_POWER_PURCHASE p
OUTER APPLY(
	SELECT 
		 --START_USAGE = SUM(ISNULL(START_USAGE, 0)),
		 --END_USAGE = SUM(ISNULL(END_USAGE, 0)),
		 ADJUST_USAGE = SUM(ISNULL(ADJUST_USAGE, 0)),
		 POWER_PURCHASE=SUM(ISNULL(POWER_PURCHASE,0))
		 --MULTIPLIER = SUM(ISNULL(MULTIPLIER, 0))
	FROM TBL_POWER_PURCHASE_DETAIL
	WHERE POWER_PURCHASE_ID = p.POWER_PURCHASE_ID AND IS_ACTIVE=1
) pd
INNER JOIN TBL_POWER_SOURCE s ON s.SOURCE_ID = p.SOURCE_ID
WHERE p.IS_ACTIVE=1 AND p.MONTH BETWEEN @D1 AND @D2

-- INVOICE POWER 
IF @TYPE_ID = 1
INSERT INTO #DETAIL
SELECT		TRAN_ACCOUNT_ID = t.INCOME_ACCOUNT_ID,
			PAYMENT_ACCOUNT_ID =0 , 
			TRAN_ID = i.INVOICE_ID,
			TRAN_TYPE=N'វិក្កយបត្រអគ្គិសនី',
			TRAN_BY = '-' ,
			TRAN_DATE = i.INVOICE_MONTH,
			AMOUNT =  dbo.ROUND_BY_CURRENCY(i.SETTLE_AMOUNT * dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@DISPLAY_CURRENCY_ID,INVOICE_DATE),@DISPLAY_CURRENCY_ID),
			REF_NO = i.INVOICE_NO ,
			NOTE = i.INVOICE_TITLE + ' ' + c.CUSTOMER_CODE+' ' + c.LAST_NAME_KH+ ' ' + c.FIRST_NAME_KH,
			CURRENCY = @SIGN
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
WHERE INVOICE_MONTH BETWEEN @D1 AND @D2
	 AND IS_SERVICE_BILL=0;

---- POWER PREPAID
IF @TYPE_ID = 1
INSERT INTO #DETAIL
SELECT		TRAN_ACCOUNT_ID = t.INCOME_ACCOUNT_ID,
			PAYMENT_ACCOUNT_ID =0 , 
			TRAN_ID = b.CUSTOMER_BUY_ID,
			TRAN_TYPE= N'លក់  PREPAID',
			TRAN_BY = b.CREATE_BY ,
			TRAN_DATE = b.CREATE_ON,
			AMOUNT =  dbo.ROUND_BY_CURRENCY(BUY_AMOUNT*dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@DISPLAY_CURRENCY_ID,CREATE_ON),@DISPLAY_CURRENCY_ID),
			REF_NO = b.BUY_NO ,
			NOTE = N'ទិញ  Prepaid '+ c.CUSTOMER_CODE+' ' + c.LAST_NAME_KH+ ' ' + c.FIRST_NAME_KH,
			CURRENCY = @SIGN
FROM TBL_CUSTOMER_BUY b
INNER JOIN TBL_PREPAID_CUSTOMER p ON b.PREPAID_CUS_ID=p.PREPAID_CUS_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=p.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
WHERE CREATE_ON BETWEEN @D1 AND @D2

---- INVOICE SERVICE
IF @TYPE_ID = 1
INSERT INTO #DETAIL
SELECT		TRAN_ACCOUNT_ID =d.INCOME_ACCOUNT_ID,
			PAYMENT_ACCOUNT_ID =0 , 
			TRAN_ID = i.INVOICE_ID,
			TRAN_TYPE=N'វិក្កយបត្រសេវាកម្ម',
			TRAN_BY = '-' ,
			TRAN_DATE = i.INVOICE_MONTH,
			AMOUNT =  dbo.ROUND_BY_CURRENCY(i.SETTLE_AMOUNT * dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@DISPLAY_CURRENCY_ID,INVOICE_DATE),@DISPLAY_CURRENCY_ID),
			REF_NO = i.INVOICE_NO ,
			NOTE = i.INVOICE_TITLE + ' ' + c.CUSTOMER_CODE+' ' + c.LAST_NAME_KH+ ' ' + c.FIRST_NAME_KH,
			CURRENCY = @SIGN
FROM TBL_INVOICE i
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = i.CUSTOMER_ID
OUTER APPLY(
	SELECT TOP 1 INCOME_ACCOUNT_ID 
	FROM TBL_INVOICE_DETAIL d
	INNER JOIN TBL_INVOICE_ITEM t ON t.INVOICE_ITEM_ID=d.INVOICE_ITEM_ID
	WHERE INVOICE_ID=i.INVOICE_ID 
) d
WHERE INVOICE_MONTH BETWEEN @D1 AND @D2
	 AND IS_SERVICE_BILL=1;

---- DEPRECIATION
IF @TYPE_ID = 2
INSERT INTO #DETAIL
SELECT		TRAN_ACCOUNT_ID = c.DEPRECIATION_EXPENSE_ID,
			PAYMENT_ACCOUNT_ID = 0 , 
			TRAN_ID = d.DEPRECIATION_ID,
			TRAN_TYPE=N'រំលស់',
			TRAN_BY = '-' ,
			TRAN_DATE =DEPRECIATION_DATE,
			AMOUNT = dbo.ROUND_BY_CURRENCY(d.AMOUNT * dbo.GET_EXCHANGE_RATE(d.CURRENCY_ID,@DISPLAY_CURRENCY_ID,DEPRECIATION_DATE),@DISPLAY_CURRENCY_ID),
			REF_NO = '-',
			NOTE = FIX_ASSET_NAME,
			CURRENCY = @SIGN  
FROM TBL_DEPRECIATION d
INNER JOIN TBL_FIX_ASSET_ITEM i ON i.FIX_ASSET_ITEM_ID=d.FIX_ASSET_ITEM_ID
INNER JOIN TBL_FIX_ASSET_CATEGORY c ON c.CATEGORY_ID=i.CATEGORY_ID
WHERE d.IS_ACTIVE  = 1
 AND DEPRECIATION_MONTH BETWEEN @D1 AND @D2

---- FIX ASSET MAINTENANCE
IF @TYPE_ID = 2
INSERT INTO #DETAIL
SELECT		TRAN_ACCOUNT_ID = EXPENSE_MAINTENANCE_ACCOUNT_ID,
			PAYMENT_ACCOUNT_ID = 0 , 
			TRAN_ID = fm.ID,
			TRAN_TYPE=N'ជួសជុល និងថែទាំអចលនទ្រព្យ',
			TRAN_BY = '-' ,
			TRAN_DATE = TRAN_DATE,
			AMOUNT = dbo.ROUND_BY_CURRENCY(fm.AMOUNT * dbo.GET_EXCHANGE_RATE(fm.CURRENCY_ID,@DISPLAY_CURRENCY_ID,TRAN_DATE),@DISPLAY_CURRENCY_ID),
			REF_NO = '-',
			NOTE = FIX_ASSET_NAME,
			CURRENCY = @SIGN 
FROM TBL_FIX_ASSET_MAINTENANCE fm
INNER JOIN TBL_FIX_ASSET_ITEM i ON i.FIX_ASSET_ITEM_ID = fm.FIX_ASSET_ITEM_ID
WHERE fm.IS_ACTIVE=1 AND TRAN_DATE BETWEEN @D1 AND @D2

---- FIX ASSET DISPOSE
IF @TYPE_ID = 1
INSERT INTO #DETAIL
SELECT		TRAN_ACCOUNT_ID = INCOME_DISPOSE_ACCOUNT_ID,
			PAYMENT_ACCOUNT_ID = 0 , 
			TRAN_ID = DISPOSE_ID,
			TRAN_TYPE=N'រំលស់',
			TRAN_BY = '-' ,
			TRAN_DATE = TRAN_DATE,
			AMOUNT = dbo.ROUND_BY_CURRENCY(SOLD_VALUE-BOOK_VALUE*dbo.GET_EXCHANGE_RATE(fd.CURRENCY_ID,@DISPLAY_CURRENCY_ID,TRAN_DATE),@DISPLAY_CURRENCY_ID),
			REF_NO = '-',
			NOTE = FIX_ASSET_NAME,
			CURRENCY = @SIGN 
FROM TBL_FIX_ASSET_DISPOSE fd
INNER JOIN TBL_FIX_ASSET_ITEM i ON i.FIX_ASSET_ITEM_ID = fd.FIX_ASSET_ITEM_ID
WHERE fd.IS_ACTIVE = 1 AND TRAN_DATE BETWEEN @D1 AND @D2


-- STAFF EXPENSE
IF @TYPE_ID = 2
INSERT INTO #DETAIL
SELECT		TRAN_ACCOUNT_ID = (SELECT TOP 1 ACCOUNT_ID FROM TBL_ACCOUNT_CHART WHERE ACCOUNT_CODE='6010'),
			PAYMENT_ACCOUNT_ID = 0 , 
			TRAN_ID = x.EXPENSE_ID,
			TRAN_TYPE=N'ចំណាយលើប្រាក់ខែបុគ្គលិក',
			TRAN_BY = CREATE_BY ,
			TRAN_DATE = EXPENSE_DATE,
			AMOUNT = dbo.ROUND_BY_CURRENCY(S6010 * dbo.GET_EXCHANGE_RATE(x.CURRENCY_ID,@DISPLAY_CURRENCY_ID,EXPENSE_DATE),@DISPLAY_CURRENCY_ID),
			REF_NO = 'NA',
			NOTE = NOTE,
			CURRENCY = @SIGN 
FROM TBL_STAFF_EXPENSE x
INNER JOIN TBL_STAFF_EXPENSE_DETAIL d ON d.EXPENSE_ID = x.EXPENSE_ID
WHERE x.IS_ACTIVE = 1 AND d.IS_ACTIVE=1  
     AND EXPENSE_DATE BETWEEN @D1 AND @D2 AND S6010>0
UNION ALL
SELECT		TRAN_ACCOUNT_ID = (SELECT TOP 1 ACCOUNT_ID FROM TBL_ACCOUNT_CHART WHERE ACCOUNT_CODE='6020'),
			PAYMENT_ACCOUNT_ID = 0 , 
			TRAN_ID = x.EXPENSE_ID,
			TRAN_TYPE=N'ចំណាយលើប្រាក់ខែបុគ្គលិក',
			TRAN_BY = CREATE_BY ,
			TRAN_DATE = EXPENSE_DATE,
			AMOUNT = dbo.ROUND_BY_CURRENCY(S6020 * dbo.GET_EXCHANGE_RATE(x.CURRENCY_ID,@DISPLAY_CURRENCY_ID,EXPENSE_DATE),@DISPLAY_CURRENCY_ID),
			REF_NO = 'NA',
			NOTE = NOTE,
			CURRENCY = @SIGN 
FROM TBL_STAFF_EXPENSE x
INNER JOIN TBL_STAFF_EXPENSE_DETAIL d ON d.EXPENSE_ID = x.EXPENSE_ID
WHERE x.IS_ACTIVE = 1 AND d.IS_ACTIVE=1  
     AND EXPENSE_DATE BETWEEN @D1 AND @D2 AND S6020>0
UNION ALL
SELECT		TRAN_ACCOUNT_ID = (SELECT TOP 1 ACCOUNT_ID FROM TBL_ACCOUNT_CHART WHERE ACCOUNT_CODE='6040'),
			PAYMENT_ACCOUNT_ID = 0 , 
			TRAN_ID = x.EXPENSE_ID,
			TRAN_TYPE=N'ចំណាយលើប្រាក់ខែបុគ្គលិក',
			TRAN_BY = CREATE_BY ,
			TRAN_DATE = EXPENSE_DATE,
			AMOUNT = dbo.ROUND_BY_CURRENCY(S6040 * dbo.GET_EXCHANGE_RATE(x.CURRENCY_ID,@DISPLAY_CURRENCY_ID,EXPENSE_DATE),@DISPLAY_CURRENCY_ID),
			REF_NO = 'NA',
			NOTE = NOTE,
			CURRENCY = @SIGN 
FROM TBL_STAFF_EXPENSE x
INNER JOIN TBL_STAFF_EXPENSE_DETAIL d ON d.EXPENSE_ID = x.EXPENSE_ID
WHERE x.IS_ACTIVE = 1 AND d.IS_ACTIVE=1  
     AND EXPENSE_DATE BETWEEN @D1 AND @D2 AND S6040>0
UNION ALL 
SELECT		TRAN_ACCOUNT_ID = (SELECT TOP 1 ACCOUNT_ID FROM TBL_ACCOUNT_CHART WHERE ACCOUNT_CODE='6050'),
			PAYMENT_ACCOUNT_ID = 0 , 
			TRAN_ID = x.EXPENSE_ID,
			TRAN_TYPE=N'ចំណាយលើប្រាក់ខែបុគ្គលិក',
			TRAN_BY = CREATE_BY ,
			TRAN_DATE = EXPENSE_DATE,
			AMOUNT = dbo.ROUND_BY_CURRENCY(S6050 * dbo.GET_EXCHANGE_RATE(x.CURRENCY_ID,@DISPLAY_CURRENCY_ID,EXPENSE_DATE),@DISPLAY_CURRENCY_ID),
			REF_NO = 'NA',
			NOTE = NOTE,
			CURRENCY = @SIGN 
FROM TBL_STAFF_EXPENSE x
INNER JOIN TBL_STAFF_EXPENSE_DETAIL d ON d.EXPENSE_ID = x.EXPENSE_ID
WHERE x.IS_ACTIVE = 1 AND d.IS_ACTIVE=1  
     AND EXPENSE_DATE BETWEEN @D1 AND @D2  AND S6050>0
UNION ALL
SELECT		TRAN_ACCOUNT_ID = (SELECT TOP 1 ACCOUNT_ID FROM TBL_ACCOUNT_CHART WHERE ACCOUNT_CODE='6060'),
			PAYMENT_ACCOUNT_ID = 0 , 
			TRAN_ID = x.EXPENSE_ID,
			TRAN_TYPE=N'ចំណាយលើប្រាក់ខែបុគ្គលិក',
			TRAN_BY = CREATE_BY ,
			TRAN_DATE = EXPENSE_DATE,
			AMOUNT = dbo.ROUND_BY_CURRENCY(S6060 * dbo.GET_EXCHANGE_RATE(x.CURRENCY_ID,@DISPLAY_CURRENCY_ID,EXPENSE_DATE),@DISPLAY_CURRENCY_ID),
			REF_NO = 'NA',
			NOTE = NOTE,
			CURRENCY = @SIGN 
FROM TBL_STAFF_EXPENSE x
INNER JOIN TBL_STAFF_EXPENSE_DETAIL d ON d.EXPENSE_ID = x.EXPENSE_ID
WHERE x.IS_ACTIVE = 1 AND d.IS_ACTIVE=1  
     AND EXPENSE_DATE BETWEEN @D1 AND @D2 AND S6060>0
UNION ALL
SELECT		TRAN_ACCOUNT_ID = (SELECT TOP 1 ACCOUNT_ID FROM TBL_ACCOUNT_CHART WHERE ACCOUNT_CODE='6070'),
			PAYMENT_ACCOUNT_ID = 0 , 
			TRAN_ID = x.EXPENSE_ID,
			TRAN_TYPE=N'ចំណាយលើប្រាក់ខែបុគ្គលិក',
			TRAN_BY = CREATE_BY ,
			TRAN_DATE = EXPENSE_DATE,
			AMOUNT = dbo.ROUND_BY_CURRENCY(S6070 * dbo.GET_EXCHANGE_RATE(x.CURRENCY_ID,@DISPLAY_CURRENCY_ID,EXPENSE_DATE),@DISPLAY_CURRENCY_ID),
			REF_NO = 'NA',
			NOTE = NOTE,
			CURRENCY = @SIGN 
FROM TBL_STAFF_EXPENSE x
INNER JOIN TBL_STAFF_EXPENSE_DETAIL d ON d.EXPENSE_ID = x.EXPENSE_ID
WHERE x.IS_ACTIVE = 1 AND d.IS_ACTIVE=1  
     AND EXPENSE_DATE BETWEEN @D1 AND @D2 AND S6070>0

-- INTEREST 
IF @TYPE_ID = 2
INSERT INTO #DETAIL
SELECT		TRAN_ACCOUNT_ID = EXPENSE_ACCOUNT_ID,
			PAYMENT_ACCOUNT_ID = 0 , 
			TRAN_ID = p.PAYMENT_ID,
			TRAN_TYPE=N'ចំណាយលើប្រាក់ខែបុគ្គលិក',
			TRAN_BY = p.CREATE_BY ,
			TRAN_DATE = p.PAY_DATE,
			AMOUNT = dbo.ROUND_BY_CURRENCY(INTEREST_AMOUNT*dbo.GET_EXCHANGE_RATE(CURRENCY_ID,@DISPLAY_CURRENCY_ID,PAY_DATE),@DISPLAY_CURRENCY_ID),
			REF_NO = LOAN_NO,
			NOTE = p.NOTE,
			CURRENCY = @SIGN 
FROM TBL_LOAN_PAYMENT p
INNER JOIN TBL_LOAN l ON l.LOAN_ID = p.LOAN_ID 
WHERE p.IS_ACTIVE=1 AND l.IS_ACTIVE=1
AND PAY_DATE BETWEEN @D1 AND @D2

SELECT	TYPE_ID=0,
		TYPE_NAME='',
		TRAN_ACCOUNT_ID,
		TRAN_ACCOUNT_NAME=c.ACCOUNT_CODE+' ' + c.ACCOUNT_NAME,
		PAYMENT_ACCOUNT_ID=0,
		PAYMENT_ACCOUNT_NAME=TRAN_TYPE,
		TRAN_ID,
		TRAN_BY,
		TRAN_DATE,
		AMOUNT,
		REF_NO,
		NOTE=d.NOTE,
		CURRENCY 
FROM #DETAIL d 
INNER JOIN TBL_ACCOUNT_CHART c ON d.TRAN_ACCOUNT_ID = c.ACCOUNT_ID
WHERE ACCOUNT_ID IN (SELECT ACCOUNT_ID FROM dbo.GET_ACCOUNT_CHART_ACENSTORS(@TRAN_ACCOUNT_ID))
ORDER BY TRAN_DATE
GO
IF OBJECT_ID('REPORT_CUSTOMERS_DEPOSIT_CONNECTION_FEE')IS NOT NULL
  DROP PROC REPORT_CUSTOMERS_DEPOSIT_CONNECTION_FEE
GO
CREATE PROC [dbo].[REPORT_CUSTOMERS_DEPOSIT_CONNECTION_FEE]
	@AREA_ID INT =0,
	@D1 DATETIME='2014-11-01',
	@D2 DATETIME='2014-12-31',
	@CURRENCY_ID INT=0,
	@CYCLE_ID INT=0
AS

-- 2014-12-29 SMEY : SHOW CUSTOMER ADDRESSS
-- 2016-12-29 SOTHEARA: ADD FILTER BILLING CYCLE ID

SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(ms,-1,DATEADD(D,1,DATEDIFF(D,0,@D2))); 

 SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME = c.LAST_NAME_KH+' '+ c.FIRST_NAME_KH,
		 AREA_NAME=a.AREA_CODE,
		CUSTOMER_TYPE_NAME=ct.CUSTOMER_CONNECTION_TYPE_NAME,
		b.BOX_CODE,
		METER_CODE =  REPLACE(LTRIM(REPLACE(m.METER_CODE,'0',' ')),' ','0') ,
		METER_NUMBER=1,
		PHONE = c.PHONE_1,
		AMPARE_NAME,
		DEPOSIT = ISNULL(d.DEPOSIT,0),
		CONNECTION_FEE = ISNULL(f.CONNECTION_FEE,0),
		v.VILLAGE_NAME,
		co.COMMUNE_NAME,
		dis.DISTRICT_NAME,
		p.PROVINCE_NAME
		
FROM ( 
	 SELECT	ITEM =N'ចាប់ផ្តើមប្រើ',
			ITEM_EN = N'New Activated Customers',
			2 AS [ITEM_ID],
			CHNAGE_DATE AS [CREATE_ON],	
			CUSTOMER_ID
	FROM TBL_CUS_STATUS_CHANGE 
	 WHERE NEW_STATUS_ID=2 AND OLD_STATUS_ID=1  
	 AND (CHNAGE_DATE BETWEEN @D1 AND @D2) 
 ) x
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = x.CUSTOMER_ID AND (@AREA_ID = 0 OR c.AREA_ID = @AREA_ID)
INNER JOIN TBL_PRICE px ON px.PRICE_ID=c.PRICE_ID
LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID AND cm.IS_ACTIVE = 1
LEFT JOIN TBL_AREA a ON a.AREA_ID = c.AREA_ID
LEFT JOIN TBL_BOX b ON b.BOX_ID = cm.BOX_ID
LEFT JOIN TBL_METER m ON m.METER_ID = cm.METER_ID
LEFT JOIN TBL_AMPARE am ON am.AMPARE_ID = c.AMP_ID
--LEFT JOIN TBL_CUSTOMER_TYPE ct ON ct.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
LEFT JOIN TLKP_CUSTOMER_CONNECTION_TYPE ct ON ct.CUSTOMER_CONNECTION_TYPE_ID=c.CUSTOMER_CONNECTION_TYPE_ID
CROSS JOIN TLKP_CURRENCY cx 
OUTER APPLY(
	SELECT  DEPOSIT=SUM(AMOUNT)
	FROM TBL_CUS_DEPOSIT 
	WHERE CUSTOMER_ID  = c.CUSTOMER_ID
		  AND IS_PAID = 1
		  AND cx.CURRENCY_ID=CURRENCY_ID
) d
OUTER APPLY(
	SELECT  CONNECTION_FEE = SUM(pd.PAY_AMOUNT) 
	FROM TBL_PAYMENT p 
	INNER JOIN TBL_PAYMENT_DETAIL pd ON pd.PAYMENT_ID=p.PAYMENT_ID
	INNER JOIN TBL_INVOICE i ON i.INVOICE_ID = pd.INVOICE_ID
	WHERE	EXISTS(
				SELECT INVOICE_DETAIL_ID 
				FROM TBL_INVOICE_DETAIL
				WHERE INVOICE_ID=i.INVOICE_ID AND INVOICE_ITEM_ID = 3
			) 
			AND p.PAY_DATE BETWEEN @D1 AND @D2
			AND i.CUSTOMER_ID = c.CUSTOMER_ID
			AND cx.CURRENCY_ID=p.CURRENCY_ID 
) f 

-- SELECT ADDRESS OF CUSTOMER
INNER JOIN TLKP_VILLAGE v ON v.VILLAGE_CODE = c.VILLAGE_CODE
INNER JOIN TLKP_COMMUNE co ON co.COMMUNE_CODE = v.COMMUNE_CODE
INNER JOIN TLKP_DISTRICT dis ON dis.DISTRICT_CODE = co.DISTRICT_CODE
INNER JOIN TLKP_PROVINCE p ON p.PROVINCE_CODE = dis.PROVINCE_CODE

WHERE (	
	-- have deposit or connection
	ISNULL(d.DEPOSIT,0)+ISNULL(f.CONNECTION_FEE,0)>0 
	-- if don't have deposit or connection free, use price currency to display
	OR cx.CURRENCY_ID = px.CURRENCY_ID
)	
AND (@CURRENCY_ID=0 OR cx.CURRENCY_ID=@CURRENCY_ID)
AND (@CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@CYCLE_ID)
-- END REPORT_CUSTOMERS_DEPOSIT_CONNECTION_FEE 
GO
IF OBJECT_ID('REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE')IS NOT NULL
  DROP PROC REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE
GO
CREATE PROC [dbo].[REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE]
	@D1 DATETIME='2012-07-01',
	@D2 DATETIME='2015-07-31',
	@ITEM_TYPE_ID INT=0,
	@ITEM_ID INT =0,
	@CURRENCY_ID INT=0,
	@AREA_ID INT=0,
	@BILLING_CYCLE_ID INT=0
AS   
SET @D1 = DATEADD(D,0,DATEDIFF(D,0,@D1));
SET @D2 = DATEADD(S,-1,DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT  cd.CUSTOMER_ID,
		cd.CURRENCY_ID,
		DEPOSIT_AMOUNT=SUM(AMOUNT)
INTO #TMP_DEPOSIT
FROM TBL_CUS_DEPOSIT cd
WHERE cd.IS_PAID=1
	  AND (@CURRENCY_ID=0 OR cd.CURRENCY_ID=@CURRENCY_ID)
	  AND (DEPOSIT_DATE BETWEEN @D1 AND @D2)
GROUP BY cd.CUSTOMER_ID,cd.CURRENCY_ID

SELECT p.CUSTOMER_ID,
	   p.CURRENCY_ID,
	   CONNECTION_FEE_AMOUNT=SUM(pd.PAY_AMOUNT)
INTO #TMP_CONNECTION_FEE
FROM TBL_PAYMENT p 
INNER JOIN TBL_PAYMENT_DETAIL pd ON pd.PAYMENT_ID=p.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID = pd.INVOICE_ID
WHERE	EXISTS(
			SELECT INVOICE_DETAIL_ID 
			FROM TBL_INVOICE_DETAIL
			WHERE INVOICE_ID=i.INVOICE_ID AND INVOICE_ITEM_ID = 3
		) 
		AND (@CURRENCY_ID=0 OR p.CURRENCY_ID=@CURRENCY_ID) 
		AND p.PAY_DATE BETWEEN @D1 AND @D2
GROUP BY p.CUSTOMER_ID,p.CURRENCY_ID

SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		CUSTOMER_CODE,
		CUSTOMER_NAME = c.LAST_NAME_KH+' '+ c.FIRST_NAME_KH,
		 AREA_NAME=a.AREA_CODE,
		CUSTOMER_TYPE_NAME=ct.CUSTOMER_CONNECTION_TYPE_NAME,
		b.BOX_CODE,
		METER_CODE =  REPLACE(LTRIM(REPLACE(m.METER_CODE,'0',' ')),' ','0') ,
		METER_NUMBER=1,
		  
		PHONE = c.PHONE_1,
		AMPARE_NAME,

		DEPOSIT=ISNULL(d.DEPOSIT_AMOUNT,0),
		CONNECTION_FEE=ISNULL(f.CONNECTION_FEE_AMOUNT,0)
FROM TBL_CUSTOMER c
INNER JOIN TBL_PRICE px ON px.PRICE_ID=c.PRICE_ID
LEFT JOIN TBL_CUSTOMER_METER cm ON cm.CUSTOMER_ID = c.CUSTOMER_ID AND cm.IS_ACTIVE = 1
LEFT JOIN TBL_AREA a ON a.AREA_ID = c.AREA_ID
LEFT JOIN TBL_BOX b ON b.BOX_ID = cm.BOX_ID
LEFT JOIN TBL_METER m ON m.METER_ID = cm.METER_ID
LEFT JOIN TBL_AMPARE am ON am.AMPARE_ID = c.AMP_ID
--LEFT JOIN TBL_CUSTOMER_TYPE ct ON ct.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
LEFT JOIN TLKP_CUSTOMER_CONNECTION_TYPE ct ON ct.CUSTOMER_CONNECTION_TYPE_ID=c.CUSTOMER_CONNECTION_TYPE_ID
CROSS JOIN TLKP_CURRENCY cx 
OUTER APPLY(
	SELECT DEPOSIT_AMOUNT
	FROM #TMP_DEPOSIT
	WHERE CUSTOMER_ID=c.CUSTOMER_ID
		  AND cx.CURRENCY_ID=CURRENCY_ID
)d
OUTER APPLY(
	SELECT CONNECTION_FEE_AMOUNT
	FROM #TMP_CONNECTION_FEE
	WHERE CUSTOMER_ID=c.CUSTOMER_ID
		  AND cx.CURRENCY_ID=CURRENCY_ID 
)f
  
WHERE  (@AREA_ID=0 OR c.AREA_ID=@AREA_ID)
		AND (@BILLING_CYCLE_ID=0 OR c.BILLING_CYCLE_ID=@BILLING_CYCLE_ID)
		AND ISNULL(f.CONNECTION_FEE_AMOUNT,0)+ISNULL(d.DEPOSIT_AMOUNT,0)>0
-- END OF REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE
GO
TRUNCATE TABLE TBL_EBL_PROCESS
GO
IF EXISTS(SELECT * FROM   INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = 'TBL_CUS_DEPOSIT' AND COLUMN_NAME = 'NOTE') 
ALTER TABLE TBL_CUS_DEPOSIT ALTER COLUMN NOTE NVARCHAR(150) NOT NULL
GO
            ";
        }
    }
}
