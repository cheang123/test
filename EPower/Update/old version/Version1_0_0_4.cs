﻿using System.Linq;
using System.Transactions;
using SoftTech;

namespace EPower.Update
{
    class Version1_0_0_4
    {
        /// <summary>
        /// - Add primary key to TBL_CONFIG
        /// - Add ampere to report deposit detail        
        /// </summary>
        public void Update()
        {
            using (TransactionScope tran = new TransactionScope())
            {
                // Update version to 1.0.0.4
                string[] strSqlStatement = new string[]
                {
                    @"IF OBJECT_ID('PK__TBL_CONFIG') IS NULL
                    ALTER TABLE TBL_CONFIG ADD CONSTRAINT PK__TBL_CONFIG PRIMARY KEY (ACTIVATE_DATE)"                                        
                    ,@"ALTER PROCEDURE [dbo].[REPORT_TRIAL_BALANCE_DEPOSIT_DETAIL]
	                        @DATE DATETIME='25-FEB-2011',
	                        @START_DATE DATETIME='25-FEB-2011'
                        AS    
                        --SET in order to fast compare in query
                        SET @DATE =  DATEADD(D, 0, DATEDIFF(D, 0, @DATE))
                        SET @DATE = DATEADD(S,-1,DATEADD(D,1,@DATE))									  

                        SELECT 
		                        ITEM =N'ប្រាក់កក់បានសងអោយអតិថិជនវិញ'
		                        ,ITEM_EN = N'Deposit Refund to Customer'
		                        ,2 AS [ITEM_ID]
		                        ,DEPOSIT_DATE AS [CREATE_ON]
		                        ,AREA_NAME 
		                        ,AP.AMPARE_NAME
		                        ,DEP.DEPOSIT_NO 
		                        ,LAST_NAME_KH + ' ' + FIRST_NAME_KH AS [CUSTOMER_NAME]
	                            ,BOX_CODE = ISNULL(BOX_CODE,'')
		                        ,METER_CODE = ISNULL(M.METER_CODE,'')
		                        ,[AMOUNT] 
	                            FROM TBL_CUS_DEPOSIT DEP
	                            INNER JOIN TBL_CUSTOMER C ON C.CUSTOMER_ID = DEP.CUSTOMER_ID
		                        INNER JOIN TBL_AMPARE AP ON AP.AMPARE_ID = C.AMP_ID
	                            INNER JOIN TBL_AREA A ON A.AREA_ID = C.AREA_ID
		                        LEFT JOIN TBL_CUSTOMER_METER CM ON CM.CUSTOMER_ID = C.CUSTOMER_ID AND CM.IS_ACTIVE=1
		                        LEFT JOIN TBL_BOX B ON B.BOX_ID = CM.BOX_ID
		                        LEFT JOIN TBL_METER M ON M.METER_ID = CM.METER_ID
		                        WHERE DEPOSIT_ACTION_ID = 3
		                        AND DEP.IS_PAID = 1
		                        AND (DEPOSIT_DATE BETWEEN @START_DATE AND @DATE)
                        		
                        UNION ALL 
	                        SELECT 
	                           ITEM =N'ប្រាក់កក់កាត់ទៅទូទាត់វិក័យប័ត្រ'
	                           ,ITEM_EN = N'Deposit Applied to Invoice Payment' 
	                           ,3 AS [ITEM_ID]
		                        ,DEPOSIT_DATE AS [CREATE_ON]
		                        ,AREA_NAME 
		                        ,AP.AMPARE_NAME
		                        ,DEP.DEPOSIT_NO 
		                        ,LAST_NAME_KH + ' ' + FIRST_NAME_KH AS [CUSTOMER_NAME]
		                        ,BOX_CODE = ISNULL(BOX_CODE,'')
		                        ,METER_CODE = ISNULL(M.METER_CODE,'')
		                        ,[AMOUNT] 
		                        FROM TBL_CUS_DEPOSIT DEP
		                        INNER JOIN TBL_CUSTOMER C ON C.CUSTOMER_ID = DEP.CUSTOMER_ID
		                        INNER JOIN TBL_AMPARE AP ON AP.AMPARE_ID = C.AMP_ID
		                        INNER JOIN TBL_AREA A ON A.AREA_ID = C.AREA_ID
		                        LEFT JOIN TBL_CUSTOMER_METER CM ON CM.CUSTOMER_ID = C.CUSTOMER_ID
		                        LEFT JOIN TBL_BOX B ON B.BOX_ID = CM.BOX_ID
		                        LEFT JOIN TBL_METER M ON M.METER_ID = CM.METER_ID
		                        WHERE DEPOSIT_ACTION_ID = 4
		                        AND DEP.IS_PAID = 1
		                        AND ( DEPOSIT_DATE BETWEEN @START_DATE AND @DATE)
                        		
                        UNION ALL 
	                        SELECT 
	                           ITEM =N'ប្រាក់កក់ត្រូវបានកែតំរូវ'
	                           ,ITEM_EN = N'Deposit Adjustment' 
	                           ,4 AS [ITEM_ID]
		                        ,DEPOSIT_DATE AS [CREATE_ON]
		                        ,AREA_NAME 
		                        ,AP.AMPARE_NAME
		                        ,DEP.DEPOSIT_NO 
		                        ,LAST_NAME_KH + ' ' + FIRST_NAME_KH AS [CUSTOMER_NAME]
		                        ,BOX_CODE = ISNULL(BOX_CODE,'')
		                        ,METER_CODE = ISNULL(M.METER_CODE,'')
		                        ,[AMOUNT] 
		                        FROM TBL_CUS_DEPOSIT DEP
		                        INNER JOIN TBL_CUSTOMER C ON C.CUSTOMER_ID = DEP.CUSTOMER_ID
		                        INNER JOIN TBL_AMPARE AP ON AP.AMPARE_ID = C.AMP_ID
		                        INNER JOIN TBL_AREA A ON A.AREA_ID = C.AREA_ID
		                        LEFT JOIN TBL_CUSTOMER_METER CM ON CM.CUSTOMER_ID = C.CUSTOMER_ID AND CM.IS_ACTIVE=1
		                        LEFT JOIN TBL_BOX B ON B.BOX_ID = CM.BOX_ID
		                        LEFT JOIN TBL_METER M ON M.METER_ID = CM.METER_ID
		                        WHERE DEPOSIT_ACTION_ID = 2
		                        AND DEP.IS_PAID = 1
		                        AND ( DEPOSIT_DATE BETWEEN @START_DATE AND @DATE)
                        		
                        UNION ALL 
	                        SELECT 
		                           ITEM =N'ប្រាក់កក់បន្ថែមទទួលបាន'
		                           ,ITEM_EN = N'Deposit From Customer'
		                           ,1 AS [ITEM_ID]
		                           ,DEPOSIT_DATE AS [CREATE_ON]
		                           ,AREA_NAME 
		                           ,AP.AMPARE_NAME
		                           ,DEP.DEPOSIT_NO 
		                           ,LAST_NAME_KH + ' ' + FIRST_NAME_KH AS [CUSTOMER_NAME]
		                           ,BOX_CODE = ISNULL(BOX_CODE,'')
		                           ,METER_CODE = ISNULL(M.METER_CODE,'')
		                           ,[AMOUNT] 
	                        FROM TBL_CUS_DEPOSIT DEP
	                           INNER JOIN TBL_CUSTOMER C ON C.CUSTOMER_ID = DEP.CUSTOMER_ID
	                           INNER JOIN TBL_AMPARE AP ON AP.AMPARE_ID = C.AMP_ID
	                           INNER JOIN TBL_AREA A ON A.AREA_ID = C.AREA_ID
	                           LEFT JOIN TBL_CUSTOMER_METER CM ON CM.CUSTOMER_ID = C.CUSTOMER_ID AND CM.IS_ACTIVE=1
	                           LEFT JOIN TBL_BOX B ON B.BOX_ID = CM.BOX_ID
	                           LEFT JOIN TBL_METER M ON M.METER_ID = CM.METER_ID
	                           WHERE DEPOSIT_ACTION_ID = 1
	                           AND DEP.IS_PAID = 1
	                           AND ( DEPOSIT_DATE BETWEEN @START_DATE AND @DATE)
                        "
                };
                foreach (string item in strSqlStatement)
                {
                    DBDataContext.Db.ExecuteCommand(item);
                }

                DBDataContext.Db.TBL_UTILITies.FirstOrDefault(row => row.UTILITY_ID == (int)Utility.VERSION).UTILITY_VALUE = "1.0.0.4";
                DBDataContext.Db.SubmitChanges(); 
                tran.Complete();
            }

        }
    }
}
