﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EPower.Properties;

namespace EPower.Update
{
    
    class Version1_2_0_2 : Version
    {
        /// <summary>  
        /// Rath Socheat
        /// 1. Add usage hiostry in DialogCollectUsageManually
        /// 2. Invoice service bill if totalAmount =0 will paid
        /// 3. Allow to pay with invoice have total amount = 0
        /// 4. Fix auto move to next customer
        /// 5. Update Exchange Rate Dialog
        /// Prak Serey
        /// 1. Update Support new IR
        /// </summary>
        /// <returns></returns>
        protected override string GetBatchCommand()
        {
            return @"
GO

IF OBJECT_ID('GET_USAGE_HISTORY') IS NOT NULL
	DROP PROC GET_USAGE_HISTORY
GO

CREATE PROC GET_USAGE_HISTORY
    @CUSTOMER_ID INT = 1
AS
SELECT TOP 12 INV_MONTH=i.INVOICE_MONTH,
	USAGE=ISNULL(i.TOTAL_USAGE,0)+ISNULL(a.ADJ_USAGE,0)
INTO #USAGE_HISTORY
FROM TBL_INVOICE i
OUTER APPLY(
	SELECT ADJ_USAGE=SUM(ADJUST_USAGE)
	FROM TBL_INVOICE_ADJUSTMENT
	WHERE INVOICE_ID=i.INVOICE_ID
)a
WHERE CUSTOMER_ID=@CUSTOMER_ID
	AND i.IS_SERVICE_BILL=0	ORDER BY i.INVOICE_MONTH DESC

SELECT [Value] = STUFF(( SELECT '; '	+ CASE WHEN (ROW_NUMBER() OVER (ORDER BY INV_MONTH DESC))=7 		THEN '{0} '		ELSE ''	  END
	+ CONVERT(NVARCHAR(7),INV_MONTH,120)
	+ ':' 
	+ CONVERT(NVARCHAR,(CONVERT(INTEGER, USAGE)))
	+ 'kWh'FROM #USAGE_HISTORYORDER BY INV_MONTH DESC	FOR XML PATH('')),1,1,'')
GO

";
        }
    }
}
