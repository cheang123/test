﻿namespace EPower.Update
{
    /// <summary>
    ///  - Fix RUN_IR_USAGE
    ///     - Recheck Register Meter
    /// </summary>
    /// <returns></returns>

    class Version1_1_1_7 : Version
    {
        protected override string GetBatchCommand()
        {
            return @"IF OBJECT_ID('REPORT_PREPAID_NOT_BUY_POWER') IS NOT NULL
	DROP PROC REPORT_PREPAID_NOT_BUY_POWER;

GO
CREATE PROC REPORT_PREPAID_NOT_BUY_POWER
		@D1 DATETIME='2011-06-01',@D2 DATETIME='2011-07-25' 
AS
	SET @D1 =DATEADD(D,0,DATEDIFF(D,0, @D1)); 
	SET	@D2=DATEADD(S,-1, DATEADD(D,1,DATEDIFF(D,0,@D2)));

SELECT
	c.ACTIVATE_DATE,
	m.METER_CODE, 
	c.CUSTOMER_CODE,
	CUSTOMER_NAME = c.FIRST_NAME_KH+' '+c.LAST_NAME_KH,
	ar.AREA_NAME,
	po.POLE_CODE,
	b.BOX_CODE,
	a.AMPARE_NAME,
	TOTAL_BUY=p.TOTAL_BUY_POWER,
	BUY = ISNULL(t.TOTAL_BUY,0),
	LAST_BUY = l.BUY_QTY,
	LAST_BUY_DATE = l.CREATE_ON,
	LAST_BUY_DAY = DATEDIFF(D,l.CREATE_ON,@D2)
FROM TBL_PREPAID_CUSTOMER p
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = p.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_METER cm ON c.CUSTOMER_ID = cm.CUSTOMER_ID AND cm.IS_ACTIVE=1
INNER JOIN TBL_METER m ON cm.METER_ID = m.METER_ID
INNER JOIN TBL_AREA ar ON c.AREA_ID = ar.AREA_ID
INNER JOIN TBL_POLE po ON cm.POLE_ID = po.POLE_ID
INNER JOIN TBL_BOX b ON cm.BOX_ID = b.BOX_ID
INNER JOIN TBL_AMPARE a ON c.AMP_ID=a.AMPARE_ID
OUTER APPLY(
SELECT TOTAL_BUY = SUM(BUY_QTY)
FROM TBL_CUSTOMER_BUY
WHERE CREATE_ON BETWEEN @D1 AND @D2
	AND PREPAID_CUS_ID=p.PREPAID_CUS_ID
	) t
	OUTER APPLY(
	SELECT TOP 1 BUY_QTY,CREATE_ON
	FROM TBL_CUSTOMER_BUY
	WHERE PREPAID_CUS_ID=p.PREPAID_CUS_ID
	AND CREATE_ON < @D1
	ORDER BY CUSTOMER_BUY_ID DESC

	) l
	WHERE ISNULL(t.TOTAL_BUY,0) = 0
	ORDER BY LAST_BUY_DAY DESC, ISNULL(l.BUY_QTY,0) DESC
--END OF REPORT PREPAID NOT BUY POWER 

GO
IF OBJECT_ID('REPORT_CASH_DAILY_DETAIL') IS NOT NULL
	DROP PROC REPORT_CASH_DAILY_DETAIL
GO
-- @ 03-JUL-2015 BY Morm Raksmey
--   1. Add Area and Group Payment by INVOICE_ITEM_ID
-- @ 27-JUL-2015 BY PRAK SEREY
--   2. Fix Prepaid
CREATE PROC REPORT_CASH_DAILY_DETAIL

	@USER_CASH_DRAWER_ID INT=1194
AS   

-- INVOICE
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		i.INVOICE_ID,
		i.INVOICE_NO,
		i.INVOICE_DATE,
		i.INVOICE_TITLE,
		i.SETTLE_AMOUNT,
		d.PAY_AMOUNT,
		p.PAY_DATE,
		TYPE_ID=itm.INVOICE_ITEM_ID,
		TYPE_NAME=itm.INVOICE_ITEM_NAME
		,a.AREA_NAME
		,a.AREA_ID
		,p.PAYMENT_ID
FROM TBL_PAYMENT p
INNER JOIN TBL_PAYMENT_DETAIL d ON p.PAYMENT_ID=d.PAYMENT_ID
INNER JOIN TBL_INVOICE i ON i.INVOICE_ID=d.INVOICE_ID
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=i.CUSTOMER_ID
INNER JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
OUTER APPLY(
	SELECT TOP(1) invd.INVOICE_ID,itm.INVOICE_ITEM_ID,itm.INVOICE_ITEM_NAME,itm.INVOICE_ITEM_TYPE_ID
	FROM TBL_INVOICE_DETAIL invd 
	INNER JOIN TBL_INVOICE_ITEM itm ON invd.INVOICE_ITEM_ID=itm.INVOICE_ITEM_ID
	WHERE invd.INVOICE_ID=i.INVOICE_ID
)itm
WHERE p.USER_CASH_DRAWER_ID=@USER_CASH_DRAWER_ID 

--- PREPAID
UNION ALL
SELECT	cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		CUSTOMER_BUY_ID,
		BUY_NO = CASE WHEN BUY_NO ='' THEN '-' ELSE BUY_NO END,
		p.CREATE_ON,
		INVOICE_TITLE = CASE WHEN p.IS_VOID = 1 AND PARENT_ID != 0 THEN N'លុបការលក់ថាមពល' ELSE N'លក់ថាមពលបង់ប្រាក់មុន' END,
		BUY_AMOUNT,
		BUY_AMOUNT,
		p.CREATE_ON,
		TYPE_ID=-200,
		TYPE_NAME=CASE WHEN p.IS_VOID = 1 AND PARENT_ID != 0 THEN N'លុបការលក់ថាមពល' ELSE N'បង់ប្រាក់មុន' END,
		a.AREA_NAME,
		a.AREA_ID,
		CUSTOMER_BUY_ID
FROM TBL_CUSTOMER_BUY p
INNER JOIN TBL_PREPAID_CUSTOMER pc ON p.PREPAID_CUS_ID = pc.PREPAID_CUS_ID
INNER JOIN TBL_CUSTOMER c ON pc.CUSTOMER_ID = c.CUSTOMER_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=p.CURRENCY_ID
WHERE p.USER_CASH_DRAWER_ID=@USER_CASH_DRAWER_ID 

--- DEPOSIT
UNION ALL
SELECT  cx.CURRENCY_ID,
		cx.CURRENCY_NAME,
		cx.CURRENCY_SING,
		c.CUSTOMER_ID,
		c.CUSTOMER_CODE,
		CUSTOMER_NAME=c.LAST_NAME_KH+' ' + c.FIRST_NAME_KH,
		t.CUSTOMER_TYPE_NAME,
		cd.CUS_DEPOSIT_ID,
		cd.DEPOSIT_NO,
		cd.DEPOSIT_DATE,
		INVOICE_TITLE=N'ប្រាក់កក់',
		cd.AMOUNT,
		cd.AMOUNT,
		cd.DEPOSIT_DATE,
		TYPE_ID=-100,
		TYPE_NAME=N'ប្រាក់កក់',
		a.AREA_NAME,
		a.AREA_ID,
		cd.CUS_DEPOSIT_ID
FROM TBL_CUS_DEPOSIT cd
INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID=cd.CUSTOMER_ID
INNER JOIN TBL_AREA a ON a.AREA_ID=c.AREA_ID
INNER JOIN TBL_CUSTOMER_TYPE t ON t.CUSTOMER_TYPE_ID=c.CUSTOMER_TYPE_ID
INNER JOIN TLKP_CURRENCY cx ON cx.CURRENCY_ID=cd.CURRENCY_ID
WHERE cd.USER_CASH_DRAWER_ID=@USER_CASH_DRAWER_ID 
	  AND cd.IS_PAID=1
ORDER BY 19
";


        }
    }
}
