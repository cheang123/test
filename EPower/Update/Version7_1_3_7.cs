﻿namespace EPower.Update
{
    class Version7_1_3_7 : Version
    {
        protected override string GetBatchCommand()
        {
            /* 
                * @Vonn Kimputhmunyvorn 2022-07-22
                * 1.Add new template ReportPayment New
                * 2.Add SPACE(4) in RUN_REF_02_2022
            */
            return EPower.Properties.Resources.V7_1_3_3;
        }
    }
}
