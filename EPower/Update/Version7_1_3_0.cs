﻿namespace EPower.Update
{
    class Version7_1_3_0 : Version
    {
        protected override string GetBatchCommand()
        {
			/*
		 * @Vonn Kimputhmunyvorn 2022-06-14
		 * 1. Modify REPORT_PAYMENT_DETAIL_BY_RECIEPT
		 * 2. Modify REPORT_SOLD_BY_CUSTOMER_TYPE
		 * 3. Modify REPORT_QUARTER_SOLD_CONSUMER
		 * 4. Update old accouting eac permission and Add new accouting pointer permission
		 * 5. Modify REPORT_CUSTOMERS
		 * 6. Modify REPORT_TRIAL_BALANCE_CUSTOMER_DETAIL
		 * 7. Modify REPORT_CUSTOMERS_DEPOSIT_CONNECTION_FEE
		 * 8. Modify REPORT_CUSTOMER_CHANGE_AMPARE
		 * 9. Modify REPORT_CUSTOMER_CHANGE_METER
		 * 10.Modify REPORT_CHANGE_SERIAL_METER
		 * 11.Modify REPORT_TRIAL_BALANCE_DEPOSIT_DETAIL
		 * 12.Modify REPORT_USAGE_MONTHLY
		 * 13.Modify REPORT_CUSTOMER_VERIFY_USAGE
		 * 14.Modify REPORT_CASH_RECEIVE_DEPOSIT_CONNECTION_FEE
		 * 15.Modify REPORT_OTHER_INCOME
		 * 16.Modify REPORT_BLOCK_AND_RECONNECT
		 */
			return EPower.Properties.Resources.v7_1_3_0;

		}
    }
}
