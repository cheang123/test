﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.OPSReport
{
    partial class PageOPSReportGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.lblOPS_REPORT = new System.Windows.Forms.Label();
            this.btnPRINT_ALL = new SoftTech.Component.ExButton();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.lblQUARTER = new System.Windows.Forms.Label();
            this.cboQuarter = new System.Windows.Forms.ComboBox();
            this.btnEXPORT_TO_EXCEL = new SoftTech.Component.ExButton();
            this.lblYEAR = new System.Windows.Forms.Label();
            this.btnMAIL = new SoftTech.Component.ExButton();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.ItemSize = new System.Drawing.Size(40, 24);
            this.tabControl1.Location = new System.Drawing.Point(0, 37);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1187, 430);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage11
            // 
            this.tabPage11.Location = new System.Drawing.Point(4, 28);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(1179, 398);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "tabPage11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // tabPage12
            // 
            this.tabPage12.Location = new System.Drawing.Point(4, 28);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(1179, 398);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "X";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // lblOPS_REPORT
            // 
            this.lblOPS_REPORT.AutoSize = true;
            this.lblOPS_REPORT.Font = new System.Drawing.Font("Khmer OS Muol Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOPS_REPORT.Location = new System.Drawing.Point(-2, 1);
            this.lblOPS_REPORT.Name = "lblOPS_REPORT";
            this.lblOPS_REPORT.Size = new System.Drawing.Size(438, 34);
            this.lblOPS_REPORT.TabIndex = 1;
            this.lblOPS_REPORT.Text = "របាយការណ៍ការផ្គត់ផ្គង់ និងសេវាកម្មអគ្គិសនី";
            // 
            // btnPRINT_ALL
            // 
            this.btnPRINT_ALL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPRINT_ALL.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPRINT_ALL.Location = new System.Drawing.Point(1072, 7);
            this.btnPRINT_ALL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPRINT_ALL.Name = "btnPRINT_ALL";
            this.btnPRINT_ALL.Size = new System.Drawing.Size(108, 23);
            this.btnPRINT_ALL.TabIndex = 3;
            this.btnPRINT_ALL.Text = "បោះពុម្ភទាំងអស់";
            this.btnPRINT_ALL.UseVisualStyleBackColor = true;
            this.btnPRINT_ALL.Click += new System.EventHandler(this.btnPrintAll_Click);
            // 
            // cboYear
            // 
            this.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Location = new System.Drawing.Point(490, 5);
            this.cboYear.Name = "cboYear";
            this.cboYear.Size = new System.Drawing.Size(65, 27);
            this.cboYear.TabIndex = 4;
            this.cboYear.SelectedIndexChanged += new System.EventHandler(this.cboYear_SelectedIndexChanged);
            // 
            // lblQUARTER
            // 
            this.lblQUARTER.AutoSize = true;
            this.lblQUARTER.Location = new System.Drawing.Point(558, 9);
            this.lblQUARTER.Name = "lblQUARTER";
            this.lblQUARTER.Size = new System.Drawing.Size(69, 19);
            this.lblQUARTER.TabIndex = 5;
            this.lblQUARTER.Text = "ប្រចាំត្រីមាស";
            // 
            // cboQuarter
            // 
            this.cboQuarter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboQuarter.FormattingEnabled = true;
            this.cboQuarter.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cboQuarter.Location = new System.Drawing.Point(633, 5);
            this.cboQuarter.Name = "cboQuarter";
            this.cboQuarter.Size = new System.Drawing.Size(55, 27);
            this.cboQuarter.TabIndex = 7;
            this.cboQuarter.SelectedIndexChanged += new System.EventHandler(this.cboQuarter_SelectedIndexChanged);
            // 
            // btnEXPORT_TO_EXCEL
            // 
            this.btnEXPORT_TO_EXCEL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEXPORT_TO_EXCEL.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEXPORT_TO_EXCEL.Location = new System.Drawing.Point(935, 7);
            this.btnEXPORT_TO_EXCEL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEXPORT_TO_EXCEL.Name = "btnEXPORT_TO_EXCEL";
            this.btnEXPORT_TO_EXCEL.Size = new System.Drawing.Size(131, 23);
            this.btnEXPORT_TO_EXCEL.TabIndex = 2;
            this.btnEXPORT_TO_EXCEL.Text = "ចម្លងជាឯកសារ Excel";
            this.btnEXPORT_TO_EXCEL.UseVisualStyleBackColor = true;
            this.btnEXPORT_TO_EXCEL.Click += new System.EventHandler(this.btnExportToExcel_Click);
            // 
            // lblYEAR
            // 
            this.lblYEAR.AutoSize = true;
            this.lblYEAR.Location = new System.Drawing.Point(443, 9);
            this.lblYEAR.Name = "lblYEAR";
            this.lblYEAR.Size = new System.Drawing.Size(45, 19);
            this.lblYEAR.TabIndex = 5;
            this.lblYEAR.Text = "ប្រចាំឆ្នាំ";
            // 
            // btnMAIL
            // 
            this.btnMAIL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMAIL.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMAIL.Location = new System.Drawing.Point(851, 7);
            this.btnMAIL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMAIL.Name = "btnMAIL";
            this.btnMAIL.Size = new System.Drawing.Size(78, 23);
            this.btnMAIL.TabIndex = 8;
            this.btnMAIL.Text = "អ៊ីមែល";
            this.btnMAIL.UseVisualStyleBackColor = true;
            this.btnMAIL.Click += new System.EventHandler(this.btnMAIL_Click);
            // 
            // PageOPSReportGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1187, 467);
            this.Controls.Add(this.btnMAIL);
            this.Controls.Add(this.cboQuarter);
            this.Controls.Add(this.lblYEAR);
            this.Controls.Add(this.lblQUARTER);
            this.Controls.Add(this.cboYear);
            this.Controls.Add(this.btnPRINT_ALL);
            this.Controls.Add(this.btnEXPORT_TO_EXCEL);
            this.Controls.Add(this.lblOPS_REPORT);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PageOPSReportGroup";
            this.Text = "PageRunReport";
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage11;
        private TabPage tabPage12;
        private Label lblOPS_REPORT;
        private ExButton btnPRINT_ALL;
        private ComboBox cboYear;
        private Label lblQUARTER;
        private ComboBox cboQuarter;
        private ExButton btnEXPORT_TO_EXCEL;
        private Label lblYEAR;
        private ExButton btnMAIL;
    }
}