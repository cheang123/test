﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.OPSReport
{
    public partial class DialogOPSR02: ExDialog
    {
        private GeneralProcess _flag;
        private TBL_OPS_R02 _objR02= new TBL_OPS_R02();
        private TBL_OPS_R02 _objOldR02=new TBL_OPS_R02();
        public TBL_OPS_R02 OPSR02
        {
            get { return _objR02; }
        }
        
        public DialogOPSR02(GeneralProcess flag, TBL_OPS_R02 objR02)
        {
            InitializeComponent();
            this.SetTextDialog(flag);

            _flag = flag;
            objR02._CopyTo(_objR02);
            objR02._CopyTo(_objOldR02);
            var enable = flag != GeneralProcess.Delete;
            UIHelper.SetEnabled(this,enable);
            btnSELECT_AREA1.Enabled = enable;

            /*
             * Default value and format.
             */
            dtpBLACKOUT_DATE.CustomFormat = UIHelper._DefaultDateFormat;
            dtpBLACKOUT_BEGIN.CustomFormat = dtpBLACKOUT_END.CustomFormat = UIHelper._DefaultShortDateTimeNoSecond;
            /*
             * binding data
             */
            UIHelper.SetDataSourceToComboBox(cboDISTRIBUTION, Lookup.GetDistribution()); 
            /*
             * load data
             */
            read();

            /*
             * Set event UI
             */
            dtpBLACKOUT_BEGIN.ValueChanged += dtpOUTAGES_DURATION_ValueChanged;
            dtpBLACKOUT_END.ValueChanged +=dtpOUTAGES_DURATION_ValueChanged;
        } 

        void dtpOUTAGES_DURATION_ValueChanged(object sender, System.EventArgs e)
        {
            var duration = dtpBLACKOUT_END.Value.Subtract(dtpBLACKOUT_BEGIN.Value).TotalHours;
            txtBLACKOUT_DURATION.Text = duration.ToString(UIHelper._DefaultUsageFormat);
        }

        private void read()
        {
            cboDISTRIBUTION.SelectedValue = _objR02.DISTRIBUTION_ID;
            dtpBLACKOUT_DATE.Value = _objR02.BLACKOUT_DATE;
            dtpBLACKOUT_BEGIN.Value = _objR02.BLACKOUT_BEGIN;
            dtpBLACKOUT_END.Value = _objR02.BLACKOUT_END;
            txtBLACKOUT_DURATION.Text =_objR02.BLACKOUT_END.Subtract(_objR02.BLACKOUT_BEGIN).TotalHours.ToString(UIHelper._DefaultUsageFormat);
            txtBLACKOUT_REASON.Text = _objR02.BLACKOUT_REASON;
            var area = Method.villageByCode(_objR02.AFFECTED_AREAS);

            txtAFFECTED_AREAS.Text = Method.ConcatVillageName(area);
            txtAFFECTED_CONSUMERS.Text = _objR02.AFFECTED_CONSUMERS.ToString(); 
        }

        private bool inValid()
        {
            var r = false;
            this.ClearAllValidation();
            if (cboDISTRIBUTION.SelectedIndex==-1)
            {
                cboDISTRIBUTION.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE,lblDISTRIBUTION.Text));
                r = true;
            }
            if (!DataHelper.IsDecimal(txtBLACKOUT_DURATION.Text))
            {
                txtBLACKOUT_DURATION.SetValidation(string.Format( Resources.REQUIRED,lblOUTAGES_DURATION.Text));
                r = true;
            }
            if (string.IsNullOrEmpty(txtBLACKOUT_REASON.Text.Trim()))
            {
                txtBLACKOUT_REASON.SetValidation(string.Format(Resources.REQUIRED,lblBLACKOUT_REASON.Text));
                r = true;
            }
             
            if (string.IsNullOrEmpty(_objR02.AFFECTED_AREAS))
            {
                txtAFFECTED_AREAS.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE,lblAFFECTED_AREAS.Text));
                r = true;
            }
            if (!DataHelper.IsNumber(txtAFFECTED_CONSUMERS.Text))
            {
                txtAFFECTED_CONSUMERS.SetValidation(string.Format(Resources.REQUIRED, lblAFFECTED_CONSUMERS.Text));
                r = true;
            }
            if (dtpBLACKOUT_END.Value < dtpBLACKOUT_BEGIN.Value)
            {
                dtpBLACKOUT_END.SetValidation(string.Format(Resources.REQUIRED_LOOKUP, lblBLACKOUT_END.Text));
                r = true;
            }
            return r;
        }

        private void write()
        {
            _objR02.DISTRIBUTION_ID = (int)cboDISTRIBUTION.SelectedValue;
            _objR02.BLACKOUT_DATE=dtpBLACKOUT_DATE.Value;
            _objR02.BLACKOUT_BEGIN = dtpBLACKOUT_BEGIN.Value;
            _objR02.BLACKOUT_END = dtpBLACKOUT_END.Value;
            _objR02.BLACKOUT_DURATION = (decimal)_objR02.BLACKOUT_END.Subtract(_objR02.BLACKOUT_BEGIN).TotalMinutes;
            _objR02.BLACKOUT_REASON= txtBLACKOUT_REASON.Text;
            _objR02.AFFECTED_CONSUMERS = DataHelper.ParseToInt(txtAFFECTED_CONSUMERS.Text); 
        }
        
        private void btnOK_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (inValid())
                {
                    return;
                }
                write();
                using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objR02);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOldR02, _objR02);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objR02);
                    }
                    tran.Complete();
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
            DialogResult = DialogResult.OK;
            
        }

        private void btnCLOSE_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.No;
        }

        private void btnCHANGE_LOG_Click(object sender, System.EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(_objR02);
        }

        private void btnSELECT_AREA1_Click(object sender, System.EventArgs e)
        {
            var areas = Method.villageByCode(_objR02.AFFECTED_AREAS);
            var dig = new DialogOPSSelectAffectedAreas(areas.ToList(), _flag);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                _objR02.AFFECTED_AREAS = Method.ConcatVillageCode(dig.SelectedAreas);
                txtAFFECTED_AREAS.Text = Method.ConcatVillageName(dig.SelectedAreas);
                txtAFFECTED_CONSUMERS.Text = dig.TotalCustomers.ToString();
            }
        }

        private void ChangeImeKH(object sender, System.EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        private void ChangeImeEN(object sender, System.EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
 
        private void pbsBlackOutReason_Click(object sender, EventArgs e)
        {
            DialogOPSSuggestion diag = new DialogOPSSuggestion("TBL_OPS_R02", "BLACKOUT_REASON");
            if (diag.ShowDialog() == DialogResult.OK)
            {
                txtBLACKOUT_REASON.Text = txtBLACKOUT_REASON.Text + diag.suggestionName;
                txtBLACKOUT_REASON.Focus();
            }
        }

        private void dtpBLACKOUT_DATE_ValueChanged(object sender, EventArgs e)
        {
            dtpBLACKOUT_BEGIN.Value =
            dtpBLACKOUT_END.Value = dtpBLACKOUT_DATE.Value;
        } 
    }
}