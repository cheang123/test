﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using EPower.Properties;

namespace EPower.Interface.OPSReport
{
    public partial class PageOPSR05 : Form
    {
        bool _load = true;

        #region Constructor
        public PageOPSR05()
        {
            InitializeComponent();
            _load = false;
            UIHelper.DataGridViewProperties(dgv);
            d1.Value = new DateTime(d1.Value.Year, d1.Value.Month,1 ); 
            loadData();
            _load = true;
        }
        #endregion

        #region Method

        private void loadData()
        {
            var DB = from r in DBDataContext.Db.TBL_OPS_R05s
                     join c in DBDataContext.Db.TBL_CUSTOMERs on r.CUSTOMER_ID equals c.CUSTOMER_ID
                     where r.IS_ACTIVE
                            && (r.DEFFECTIVE_DATE.Date >= d1.Value.Date && r.DEFFECTIVE_DATE.Date <= d2.Value.Date)
                            && (r.ACCURACY.ToString() + " " + c.CUSTOMER_CODE + " " + c.LAST_NAME_KH + " " + c.FIRST_NAME_KH).ToUpper().Contains(txtQuickSearch.Text.ToUpper()) 
                     orderby r.DEFFECTIVE_DATE
                     select new
                     {
                         r.DEFFECTIVE_METER_ID 
                         ,c.CUSTOMER_CODE
                         ,CUSTOMER_NAME=c.LAST_NAME_KH+" "+c.FIRST_NAME_KH
                         ,r.DEFFECTIVE_DATE
                         ,r.ACCURACY
                         ,r.REPLACING_DATE
                     };
            dgv.DataSource = DB;
                        
        }

        #endregion

        private void txt_QuickSearch(object sender, EventArgs e)
        {
            if (_load)
            {
                loadData();
            }
        } 
         
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        } 
        private void btnADD_Click(object sender, EventArgs e)
        {
            DateTime dt=DBDataContext.Db.GetSystemDate();
            DialogOPSR05 dia = new DialogOPSR05(GeneralProcess.Insert, new TBL_OPS_R05() { DEFFECTIVE_DATE = dt, REPLACING_DATE = dt });
            if (dia.ShowDialog() == DialogResult.OK)
            {
                loadData();
                UIHelper.SelectRow(dgv, dia.OPSR05.DEFFECTIVE_METER_ID);
            }
        }

        private void btnEDIT_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_OPS_R05 ObjOPSR05 = DBDataContext.Db.TBL_OPS_R05s.FirstOrDefault(x => x.DEFFECTIVE_METER_ID == (int)dgv.SelectedRows[0].Cells[DEFFECTIVE_METER_ID.Name].Value);
                DialogOPSR05 dia = new DialogOPSR05(GeneralProcess.Update, ObjOPSR05);
                if (dia.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, dia.OPSR05.DEFFECTIVE_METER_ID);
                }
            }
        }

        private void btnREMOVE_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                TBL_OPS_R05 ObjOPSR05 = DBDataContext.Db.TBL_OPS_R05s.FirstOrDefault(x => x.DEFFECTIVE_METER_ID == (int)dgv.SelectedRows[0].Cells[DEFFECTIVE_METER_ID.Name].Value);
                DialogOPSR05 dia = new DialogOPSR05(GeneralProcess.Delete, ObjOPSR05);
                if (dia.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, dia.OPSR05.DEFFECTIVE_METER_ID);
                }
            }
        }



        
         
    }
}
