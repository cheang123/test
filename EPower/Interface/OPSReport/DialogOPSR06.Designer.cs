﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.OPSReport
{
    partial class DialogOPSR06
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogOPSR06));
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblREQUEST_DATE = new System.Windows.Forms.Label();
            this.lblTEST_DATE = new System.Windows.Forms.Label();
            this.lblREQUEST_REASON = new System.Windows.Forms.Label();
            this.lblTEST_RESULT = new System.Windows.Forms.Label();
            this.dtpRequestDate = new System.Windows.Forms.DateTimePicker();
            this.dtpTestDate = new System.Windows.Forms.DateTimePicker();
            this.txtRequestReason = new System.Windows.Forms.TextBox();
            this.txtTestResult = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCustomerCodes = new SoftTech.Component.ExTextbox();
            this.txtCustomerNames = new SoftTech.Component.ExTextbox();
            this.pbsComplaintReason = new System.Windows.Forms.PictureBox();
            this.pbsResultDecision = new System.Windows.Forms.PictureBox();
            this.lblNOTIFICATION_DATE = new System.Windows.Forms.Label();
            this.dtpNotificationDate = new System.Windows.Forms.DateTimePicker();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsComplaintReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsResultDecision)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.pbsResultDecision);
            this.content.Controls.Add(this.pbsComplaintReason);
            this.content.Controls.Add(this.txtCustomerNames);
            this.content.Controls.Add(this.txtCustomerCodes);
            this.content.Controls.Add(this.lblTEST_RESULT);
            this.content.Controls.Add(this.dtpNotificationDate);
            this.content.Controls.Add(this.dtpTestDate);
            this.content.Controls.Add(this.dtpRequestDate);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblNOTIFICATION_DATE);
            this.content.Controls.Add(this.lblREQUEST_REASON);
            this.content.Controls.Add(this.lblTEST_DATE);
            this.content.Controls.Add(this.lblREQUEST_DATE);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.txtTestResult);
            this.content.Controls.Add(this.txtRequestReason);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtRequestReason, 0);
            this.content.Controls.SetChildIndex(this.txtTestResult, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblREQUEST_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblTEST_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblREQUEST_REASON, 0);
            this.content.Controls.SetChildIndex(this.lblNOTIFICATION_DATE, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.dtpRequestDate, 0);
            this.content.Controls.SetChildIndex(this.dtpTestDate, 0);
            this.content.Controls.SetChildIndex(this.dtpNotificationDate, 0);
            this.content.Controls.SetChildIndex(this.lblTEST_RESULT, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCodes, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerNames, 0);
            this.content.Controls.SetChildIndex(this.pbsComplaintReason, 0);
            this.content.Controls.SetChildIndex(this.pbsResultDecision, 0);
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblREQUEST_DATE
            // 
            resources.ApplyResources(this.lblREQUEST_DATE, "lblREQUEST_DATE");
            this.lblREQUEST_DATE.Name = "lblREQUEST_DATE";
            // 
            // lblTEST_DATE
            // 
            resources.ApplyResources(this.lblTEST_DATE, "lblTEST_DATE");
            this.lblTEST_DATE.Name = "lblTEST_DATE";
            // 
            // lblREQUEST_REASON
            // 
            resources.ApplyResources(this.lblREQUEST_REASON, "lblREQUEST_REASON");
            this.lblREQUEST_REASON.Name = "lblREQUEST_REASON";
            // 
            // lblTEST_RESULT
            // 
            resources.ApplyResources(this.lblTEST_RESULT, "lblTEST_RESULT");
            this.lblTEST_RESULT.Name = "lblTEST_RESULT";
            // 
            // dtpRequestDate
            // 
            resources.ApplyResources(this.dtpRequestDate, "dtpRequestDate");
            this.dtpRequestDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRequestDate.Name = "dtpRequestDate";
            this.dtpRequestDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // dtpTestDate
            // 
            resources.ApplyResources(this.dtpTestDate, "dtpTestDate");
            this.dtpTestDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTestDate.Name = "dtpTestDate";
            this.dtpTestDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // txtRequestReason
            // 
            resources.ApplyResources(this.txtRequestReason, "txtRequestReason");
            this.txtRequestReason.Name = "txtRequestReason";
            this.txtRequestReason.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // txtTestResult
            // 
            resources.ApplyResources(this.txtTestResult, "txtTestResult");
            this.txtTestResult.Name = "txtTestResult";
            this.txtTestResult.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Name = "label7";
            // 
            // txtCustomerCodes
            // 
            this.txtCustomerCodes.BackColor = System.Drawing.Color.White;
            this.txtCustomerCodes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerCodes, "txtCustomerCodes");
            this.txtCustomerCodes.Name = "txtCustomerCodes";
            this.txtCustomerCodes.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerCodes.AdvanceSearch += new System.EventHandler(this.txtCustomerCodes_AdvanceSearch);
            this.txtCustomerCodes.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCodes_CancelAdvanceSearch);
            this.txtCustomerCodes.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // txtCustomerNames
            // 
            this.txtCustomerNames.BackColor = System.Drawing.Color.White;
            this.txtCustomerNames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerNames, "txtCustomerNames");
            this.txtCustomerNames.Name = "txtCustomerNames";
            this.txtCustomerNames.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerNames.AdvanceSearch += new System.EventHandler(this.txtCustomerNames_AdvanceSearch);
            this.txtCustomerNames.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCodes_CancelAdvanceSearch);
            this.txtCustomerNames.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // pbsComplaintReason
            // 
            this.pbsComplaintReason.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.pbsComplaintReason, "pbsComplaintReason");
            this.pbsComplaintReason.Name = "pbsComplaintReason";
            this.pbsComplaintReason.TabStop = false;
            this.pbsComplaintReason.Click += new System.EventHandler(this.pbsRequestReason_Click);
            // 
            // pbsResultDecision
            // 
            this.pbsResultDecision.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.pbsResultDecision, "pbsResultDecision");
            this.pbsResultDecision.Name = "pbsResultDecision";
            this.pbsResultDecision.TabStop = false;
            this.pbsResultDecision.Click += new System.EventHandler(this.pbsTestResult_Click);
            // 
            // lblNOTIFICATION_DATE
            // 
            resources.ApplyResources(this.lblNOTIFICATION_DATE, "lblNOTIFICATION_DATE");
            this.lblNOTIFICATION_DATE.Name = "lblNOTIFICATION_DATE";
            // 
            // dtpNotificationDate
            // 
            resources.ApplyResources(this.dtpNotificationDate, "dtpNotificationDate");
            this.dtpNotificationDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNotificationDate.Name = "dtpNotificationDate";
            this.dtpNotificationDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // DialogOPSR06
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogOPSR06";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsComplaintReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsResultDecision)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_CODE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label1;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private Label lblTEST_RESULT;
        private DateTimePicker dtpTestDate;
        private DateTimePicker dtpRequestDate;
        private Label label7;
        private Label label6;
        private Label lblREQUEST_REASON;
        private Label lblTEST_DATE;
        private Label lblREQUEST_DATE;
        private TextBox txtTestResult;
        private TextBox txtRequestReason;
        private ExTextbox txtCustomerNames;
        private ExTextbox txtCustomerCodes;
        private PictureBox pbsComplaintReason;
        private PictureBox pbsResultDecision;
        private DateTimePicker dtpNotificationDate;
        private Label lblNOTIFICATION_DATE;
    }
}