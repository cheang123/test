﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.OPSReport
{
    partial class PageOPSR02
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageOPSR02));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.BLACKOUT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BLACKOUT_REASON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISTRIBUTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BLACKOUT_BEGIN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BLACKOUT_END = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BLACKOUT_DURATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AFFECTED_AREAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AFFECTED_CONSUMERS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboDistribution = new System.Windows.Forms.ComboBox();
            this.dtpD2 = new System.Windows.Forms.DateTimePicker();
            this.dtpD1 = new System.Windows.Forms.DateTimePicker();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BLACKOUT_ID,
            this.DATE,
            this.BLACKOUT_REASON,
            this.DISTRIBUTION,
            this.BLACKOUT_BEGIN,
            this.BLACKOUT_END,
            this.BLACKOUT_DURATION,
            this.AFFECTED_AREAS,
            this.AFFECTED_CONSUMERS});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // BLACKOUT_ID
            // 
            this.BLACKOUT_ID.DataPropertyName = "BLACKOUT_ID";
            resources.ApplyResources(this.BLACKOUT_ID, "BLACKOUT_ID");
            this.BLACKOUT_ID.Name = "BLACKOUT_ID";
            this.BLACKOUT_ID.ReadOnly = true;
            // 
            // DATE
            // 
            this.DATE.DataPropertyName = "BLACKOUT_DATE";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.Format = "dd-MM-yyyy";
            dataGridViewCellStyle16.NullValue = null;
            this.DATE.DefaultCellStyle = dataGridViewCellStyle16;
            this.DATE.FillWeight = 52.39455F;
            resources.ApplyResources(this.DATE, "DATE");
            this.DATE.Name = "DATE";
            this.DATE.ReadOnly = true;
            // 
            // BLACKOUT_REASON
            // 
            this.BLACKOUT_REASON.DataPropertyName = "BLACKOUT_REASON";
            this.BLACKOUT_REASON.FillWeight = 70.12203F;
            resources.ApplyResources(this.BLACKOUT_REASON, "BLACKOUT_REASON");
            this.BLACKOUT_REASON.Name = "BLACKOUT_REASON";
            this.BLACKOUT_REASON.ReadOnly = true;
            // 
            // DISTRIBUTION
            // 
            this.DISTRIBUTION.DataPropertyName = "DISTRIBUTION_NAME";
            this.DISTRIBUTION.FillWeight = 70.12203F;
            resources.ApplyResources(this.DISTRIBUTION, "DISTRIBUTION");
            this.DISTRIBUTION.Name = "DISTRIBUTION";
            this.DISTRIBUTION.ReadOnly = true;
            // 
            // BLACKOUT_BEGIN
            // 
            this.BLACKOUT_BEGIN.DataPropertyName = "BLACKOUT_BEGIN";
            dataGridViewCellStyle17.Format = "dd-MM-yyyy hh:mm tt";
            dataGridViewCellStyle17.NullValue = null;
            this.BLACKOUT_BEGIN.DefaultCellStyle = dataGridViewCellStyle17;
            this.BLACKOUT_BEGIN.FillWeight = 70.12203F;
            resources.ApplyResources(this.BLACKOUT_BEGIN, "BLACKOUT_BEGIN");
            this.BLACKOUT_BEGIN.Name = "BLACKOUT_BEGIN";
            this.BLACKOUT_BEGIN.ReadOnly = true;
            // 
            // BLACKOUT_END
            // 
            this.BLACKOUT_END.DataPropertyName = "BLACKOUT_END";
            dataGridViewCellStyle18.Format = "dd-MM-yyyy hh:mm tt";
            dataGridViewCellStyle18.NullValue = null;
            this.BLACKOUT_END.DefaultCellStyle = dataGridViewCellStyle18;
            this.BLACKOUT_END.FillWeight = 70.12203F;
            resources.ApplyResources(this.BLACKOUT_END, "BLACKOUT_END");
            this.BLACKOUT_END.Name = "BLACKOUT_END";
            this.BLACKOUT_END.ReadOnly = true;
            // 
            // BLACKOUT_DURATION
            // 
            this.BLACKOUT_DURATION.DataPropertyName = "BLACKOUT_DURATION";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BLACKOUT_DURATION.DefaultCellStyle = dataGridViewCellStyle19;
            this.BLACKOUT_DURATION.FillWeight = 70.12203F;
            resources.ApplyResources(this.BLACKOUT_DURATION, "BLACKOUT_DURATION");
            this.BLACKOUT_DURATION.Name = "BLACKOUT_DURATION";
            this.BLACKOUT_DURATION.ReadOnly = true;
            // 
            // AFFECTED_AREAS
            // 
            this.AFFECTED_AREAS.DataPropertyName = "AFFECTED_AREAS";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.AFFECTED_AREAS.DefaultCellStyle = dataGridViewCellStyle20;
            this.AFFECTED_AREAS.FillWeight = 70.12203F;
            resources.ApplyResources(this.AFFECTED_AREAS, "AFFECTED_AREAS");
            this.AFFECTED_AREAS.Name = "AFFECTED_AREAS";
            this.AFFECTED_AREAS.ReadOnly = true;
            // 
            // AFFECTED_CONSUMERS
            // 
            this.AFFECTED_CONSUMERS.DataPropertyName = "AFFECTED_CONSUMERS";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.AFFECTED_CONSUMERS.DefaultCellStyle = dataGridViewCellStyle21;
            this.AFFECTED_CONSUMERS.FillWeight = 70.12203F;
            resources.ApplyResources(this.AFFECTED_CONSUMERS, "AFFECTED_CONSUMERS");
            this.AFFECTED_CONSUMERS.Name = "AFFECTED_CONSUMERS";
            this.AFFECTED_CONSUMERS.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboDistribution);
            this.panel1.Controls.Add(this.dtpD2);
            this.panel1.Controls.Add(this.dtpD1);
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboDistribution
            // 
            this.cboDistribution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDistribution.DropDownWidth = 200;
            this.cboDistribution.FormattingEnabled = true;
            resources.ApplyResources(this.cboDistribution, "cboDistribution");
            this.cboDistribution.Name = "cboDistribution";
            // 
            // dtpD2
            // 
            resources.ApplyResources(this.dtpD2, "dtpD2");
            this.dtpD2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpD2.Name = "dtpD2";
            this.dtpD2.ValueChanged += new System.EventHandler(this.dtpD1_ValueChanged);
            // 
            // dtpD1
            // 
            resources.ApplyResources(this.dtpD1, "dtpD1");
            this.dtpD1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpD1.Name = "dtpD1";
            this.dtpD1.ValueChanged += new System.EventHandler(this.dtpD1_ValueChanged);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnREMOVE_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnADD_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEDIT_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "BLACKOUT_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "BLACK_OUT_DATE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BLACKOUT_REASON";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "DISTRIBUTION_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "BLACKOUT_DURATION";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "BLACKOUT_BEGIN";
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "BLACKOUT_END";
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "AFFECTED_AREAS";
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "AFFECTED_CONSUMERS";
            resources.ApplyResources(this.dataGridViewTextBoxColumn9, "dataGridViewTextBoxColumn9");
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // PageOPSR02
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageOPSR02";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        private ExButton btnREMOVE;
        private DateTimePicker dtpD2;
        private DateTimePicker dtpD1;
        private ComboBox cboDistribution;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn BLACKOUT_ID;
        private DataGridViewTextBoxColumn DATE;
        private DataGridViewTextBoxColumn BLACKOUT_REASON;
        private DataGridViewTextBoxColumn DISTRIBUTION;
        private DataGridViewTextBoxColumn BLACKOUT_BEGIN;
        private DataGridViewTextBoxColumn BLACKOUT_END;
        private DataGridViewTextBoxColumn BLACKOUT_DURATION;
        private DataGridViewTextBoxColumn AFFECTED_AREAS;
        private DataGridViewTextBoxColumn AFFECTED_CONSUMERS;
    }
}
