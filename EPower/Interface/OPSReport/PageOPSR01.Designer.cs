﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.OPSReport
{
    partial class PageOPSR01
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageOPSR01));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtpD2 = new System.Windows.Forms.DateTimePicker();
            this.cboDISTRIBUTION = new System.Windows.Forms.ComboBox();
            this.dtpD1 = new System.Windows.Forms.DateTimePicker();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.SCHEDULE_OUTTAGES_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OUTAGES_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JOB_DESCRIPTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISTRIBUTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OUTAGES_BEGIN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OUTAGES_END = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OUTAGES_DURATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTIFICATION_WAYS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AFFECTED_AREAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AFFECTED_CONSUMERS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SCHEDULE_OUTTAGES_ID,
            this.OUTAGES_DATE,
            this.JOB_DESCRIPTION,
            this.DISTRIBUTION,
            this.OUTAGES_BEGIN,
            this.OUTAGES_END,
            this.OUTAGES_DURATION,
            this.NOTIFICATION_WAYS,
            this.AFFECTED_AREAS,
            this.AFFECTED_CONSUMERS});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.dtpD2);
            this.panel1.Controls.Add(this.cboDISTRIBUTION);
            this.panel1.Controls.Add(this.dtpD1);
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // dtpD2
            // 
            resources.ApplyResources(this.dtpD2, "dtpD2");
            this.dtpD2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpD2.Name = "dtpD2";
            // 
            // cboDISTRIBUTION
            // 
            this.cboDISTRIBUTION.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDISTRIBUTION.FormattingEnabled = true;
            resources.ApplyResources(this.cboDISTRIBUTION, "cboDISTRIBUTION");
            this.cboDISTRIBUTION.Name = "cboDISTRIBUTION";
            // 
            // dtpD1
            // 
            resources.ApplyResources(this.dtpD1, "dtpD1");
            this.dtpD1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpD1.Name = "dtpD1";
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnREMOVE_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnADD_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEDIT_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // SCHEDULE_OUTTAGES_ID
            // 
            this.SCHEDULE_OUTTAGES_ID.DataPropertyName = "SCHEDULE_OUTTAGES_ID";
            resources.ApplyResources(this.SCHEDULE_OUTTAGES_ID, "SCHEDULE_OUTTAGES_ID");
            this.SCHEDULE_OUTTAGES_ID.Name = "SCHEDULE_OUTTAGES_ID";
            this.SCHEDULE_OUTTAGES_ID.ReadOnly = true;
            // 
            // OUTAGES_DATE
            // 
            this.OUTAGES_DATE.DataPropertyName = "OUTAGES_DATE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.OUTAGES_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.OUTAGES_DATE.FillWeight = 80F;
            resources.ApplyResources(this.OUTAGES_DATE, "OUTAGES_DATE");
            this.OUTAGES_DATE.Name = "OUTAGES_DATE";
            this.OUTAGES_DATE.ReadOnly = true;
            // 
            // JOB_DESCRIPTION
            // 
            this.JOB_DESCRIPTION.DataPropertyName = "JOB_DESCRIPTION";
            resources.ApplyResources(this.JOB_DESCRIPTION, "JOB_DESCRIPTION");
            this.JOB_DESCRIPTION.Name = "JOB_DESCRIPTION";
            this.JOB_DESCRIPTION.ReadOnly = true;
            // 
            // DISTRIBUTION
            // 
            this.DISTRIBUTION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.DISTRIBUTION.DataPropertyName = "DISTRIBUTION_NAME";
            resources.ApplyResources(this.DISTRIBUTION, "DISTRIBUTION");
            this.DISTRIBUTION.Name = "DISTRIBUTION";
            this.DISTRIBUTION.ReadOnly = true;
            // 
            // OUTAGES_BEGIN
            // 
            this.OUTAGES_BEGIN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.OUTAGES_BEGIN.DataPropertyName = "OUTAGES_BEGIN";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Format = "dd-MM-yyyy hh:mm tt";
            this.OUTAGES_BEGIN.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.OUTAGES_BEGIN, "OUTAGES_BEGIN");
            this.OUTAGES_BEGIN.Name = "OUTAGES_BEGIN";
            this.OUTAGES_BEGIN.ReadOnly = true;
            // 
            // OUTAGES_END
            // 
            this.OUTAGES_END.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.OUTAGES_END.DataPropertyName = "OUTAGES_END";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "dd-MM-yyyy hh:mm tt";
            this.OUTAGES_END.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.OUTAGES_END, "OUTAGES_END");
            this.OUTAGES_END.Name = "OUTAGES_END";
            this.OUTAGES_END.ReadOnly = true;
            // 
            // OUTAGES_DURATION
            // 
            this.OUTAGES_DURATION.DataPropertyName = "OUTAGES_DURATION";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.OUTAGES_DURATION.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.OUTAGES_DURATION, "OUTAGES_DURATION");
            this.OUTAGES_DURATION.Name = "OUTAGES_DURATION";
            this.OUTAGES_DURATION.ReadOnly = true;
            // 
            // NOTIFICATION_WAYS
            // 
            this.NOTIFICATION_WAYS.DataPropertyName = "NOTIFICATION_WAYS";
            resources.ApplyResources(this.NOTIFICATION_WAYS, "NOTIFICATION_WAYS");
            this.NOTIFICATION_WAYS.Name = "NOTIFICATION_WAYS";
            this.NOTIFICATION_WAYS.ReadOnly = true;
            // 
            // AFFECTED_AREAS
            // 
            this.AFFECTED_AREAS.DataPropertyName = "AFFECTED_AREAS";
            resources.ApplyResources(this.AFFECTED_AREAS, "AFFECTED_AREAS");
            this.AFFECTED_AREAS.Name = "AFFECTED_AREAS";
            this.AFFECTED_AREAS.ReadOnly = true;
            // 
            // AFFECTED_CONSUMERS
            // 
            this.AFFECTED_CONSUMERS.DataPropertyName = "AFFECTED_CONSUMERS";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.AFFECTED_CONSUMERS.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.AFFECTED_CONSUMERS, "AFFECTED_CONSUMERS");
            this.AFFECTED_CONSUMERS.Name = "AFFECTED_CONSUMERS";
            this.AFFECTED_CONSUMERS.ReadOnly = true;
            // 
            // PageOPSR01
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageOPSR01";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        private ExButton btnREMOVE;
        private DateTimePicker dtpD2;
        private ComboBox cboDISTRIBUTION;
        private DateTimePicker dtpD1;
        private DataGridViewTextBoxColumn SCHEDULE_OUTTAGES_ID;
        private DataGridViewTextBoxColumn OUTAGES_DATE;
        private DataGridViewTextBoxColumn JOB_DESCRIPTION;
        private DataGridViewTextBoxColumn DISTRIBUTION;
        private DataGridViewTextBoxColumn OUTAGES_BEGIN;
        private DataGridViewTextBoxColumn OUTAGES_END;
        private DataGridViewTextBoxColumn OUTAGES_DURATION;
        private DataGridViewTextBoxColumn NOTIFICATION_WAYS;
        private DataGridViewTextBoxColumn AFFECTED_AREAS;
        private DataGridViewTextBoxColumn AFFECTED_CONSUMERS;
    }
}
