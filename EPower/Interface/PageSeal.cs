﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageSeal : Form
    {

        public TBL_SEAL Seal
        {
            get
            {
                TBL_SEAL objSeal = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int sealID = (int)dgv.SelectedRows[0].Cells["SEAL_ID"].Value;
                        objSeal = DBDataContext.Db.TBL_SEALs.FirstOrDefault(x => x.SEAL_ID == sealID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objSeal;
            }
        }

        #region Constructor
        public PageSeal()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new seal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogSeal dig = new DialogSeal(GeneralProcess.Insert, new TBL_SEAL() { SEAL_NAME = "" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Seal.SEAL_ID);
            }
        }

        /// <summary>
        /// Edit seal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogSeal dig = new DialogSeal(GeneralProcess.Update, Seal);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Seal.SEAL_ID);
                }
            }
        }

        /// <summary>
        /// Remove seal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogSeal dig = new DialogSeal(GeneralProcess.Delete, Seal);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Seal.SEAL_ID - 1);
                }
            }
        }

        /// <summary>
        /// Load data from database and display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = from sa in DBDataContext.Db.TBL_SEALs
                                 where sa.IS_ACTIVE &&
                                 sa.SEAL_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                                 select sa;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            //TODO: can delte.
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
            }
            return val;
        }

        #endregion

        
    }
}
