﻿using DevExpress.Utils.Win;
using DevExpress.XtraEditors.Popup;
using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Management;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class PagePrintInvoice : Form
    {
        bool _loading = false;
        bool printingInvoice = false;
        string newFile;
        string pathFile;
        private bool _isBinded = false;
        public PagePrintInvoice()
        {
            InitializeComponent();
            printingInvoice = DataHelper.ParseToBoolean(Method.Utilities[Utility.INVOICE_CONCURRENT_PRINTING]);
            UIHelper.DataGridViewProperties(this.dgvSummary);
            //this.dgvSummary.DefaultCellStyle.SelectionBackColor = this.dgvSummary.DefaultCellStyle.BackColor;
            //this.dgvSummary.DefaultCellStyle.SelectionForeColor = this.dgvSummary.DefaultCellStyle.ForeColor;
            bindPrinterAndReport();
            _isBinded = true;
            ApplyDefault();
            this.timer.Start();

            //Events
            this.cboAreaCode.Properties.Popup += Properties_Popup; ;
            this.cboAreaCode.EditValueChanged += CboAreaCode_EditValueChanged;
            cboAreaCode.CustomDisplayText += CboAreaCode_CustomDisplayText;
        }

        private void CboAreaCode_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            string items = cboAreaCode.Properties.GetCheckedItems().ToString();
            List<string> checkItems = new List<string>();

            if (items.Contains(","))
            {
                string[] itemValue = items.Split(',');
                foreach (string i in itemValue)
                {
                    checkItems.Add(i);
                }
            }

            if (checkItems != null)
            {
                if (checkItems.Count() == cboAreaCode.Properties.Items.Count)
                {
                    e.DisplayText = Resources.ALL_AREA;
                }
            }
        }

        public void ApplyDefault()
        { 
            // for the first time if user is not set default report for print invoice 
            if (Settings.Default.REPORT_INVOICE == null || Settings.Default.REPORT_INVOICE == "")
            {
                Settings.Default.REPORT_INVOICE = "ReportInvoice.rpt";
                Settings.Default.Save();
            }

            var selectedReport = Settings.Default.REPORT_INVOICE;
            cboReport.SelectedValue = selectedReport;
            if (string.IsNullOrEmpty(selectedReport))
            {
                cboReport.SelectedIndex = 0;
            }
        }
        private void CboAreaCode_EditValueChanged(object sender, EventArgs e)
        {
            bindData();
        }

        private void Properties_Popup(object sender, EventArgs e)
        {
            CheckedPopupContainerForm f = (sender as IPopupControl).PopupWindow as CheckedPopupContainerForm;
            //f.OkButton.ToolTip = Resources.OK;
            f.OkButton.Text = Resources.OK;
            f.CloseButton.Text = Resources.CLOSE;
            //f.CloseButton.ToolTip = Resources.CLOSE;
        }
        
        private void bindPrinterAndReport()
        {
            this.cboPrinterInvoice.Items.Add("");
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                this.cboPrinterInvoice.Items.Add(printer);
            }

            this.cboPrinterInvoice.Text = Settings.Default.PRINTER_INVOICE; 

            string invoices = Method.Utilities[Utility.REPORT_INVOICE_LIST];
            var bill_printingList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BillPrintingListModel>>(invoices);
            cboReport.DataSource = bill_printingList;
            cboReport.DisplayMember = nameof(BillPrintingListModel.TemplateName);
            cboReport.ValueMember = nameof(BillPrintingListModel.TemplateName); 

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
        }

        private void bindData()
        {
            if (_loading)
            {
                return;
            }
            try
            {
                int cycleId = (int)this.cboBillingCycle.SelectedValue;
                //int areaId = (int)this.cboArea.SelectedValue;
                List<int> areaIds = new List<int>();
                if (!string.IsNullOrEmpty(cboAreaCode.EditValue as string))
                {
                    areaIds = (cboAreaCode.EditValue as string)?.Split(',').Select(int.Parse).ToList();
                }
                int billerID = (int)this.cboBiller.SelectedValue;
                int typeId = cboConnectionType.SelectedValue == null ? 0 : (int)cboConnectionType.SelectedValue;
                int priceId = (int)this.cboPrice.SelectedValue;
                int cusGroupId = (int)cboCustomerType.SelectedValue;
                bool ifZero = this.chkIncludeZeroUsage.Checked;
                int invoicestatus = (int)this.cboInvoiceStatus.SelectedValue;
                DateTime month = new DateTime(this.dtpMonth.Value.Year, this.dtpMonth.Value.Month, 1);

                string startBoxNo = this.txtStartBoxNo.Text;
                string endBoxNo = this.txtEndBoxNo.Text;

                bool allInvoice = rdoPRINT_ALL.Checked;
                var dt = new DataTable();
                var dtSummary = new DataTable();

                Runner.Run(delegate ()
                {
                    dt = loadInvoiceToPrint(cycleId, areaIds, billerID, typeId, cusGroupId, priceId, month, startBoxNo, endBoxNo, ifZero, invoicestatus);

                    dtSummary = dt.Rows.Cast<DataRow>().Select(x => new
                    {
                        CURRENCY_ID = (int)x["CURRENCY_ID"],
                        CURRENCY_SING = x["CURRENCY_SING"].ToString(),
                        TOTAL_BALANCE = (decimal)x["TOTAL_BALANCE"],
                        TOTAL_USAGE = (decimal)x["TOTAL_USAGE"]
                    })
                    .OrderBy(x => x.CURRENCY_ID)
                    .GroupBy(x => new { x.CURRENCY_ID, x.CURRENCY_SING })
                    .Select(x => new
                    {
                        x.Key.CURRENCY_ID,
                        x.Key.CURRENCY_SING,
                        TOTAL_INVOICE = x.Count(),
                        TOTAL_USAGE = x.Sum(r => r.TOTAL_USAGE),
                        TOTAL_BALANCE = x.Sum(r => r.TOTAL_BALANCE)
                    })._ToDataTable();

                });
                this.dgvSummary.DataSource = dtSummary;

            }catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }          
        }

        public void Bind()
        {
            this._loading = true;

            this.bindLookUp();

            this._loading = false;

            this.bindData();
        }

        private void bindLookUp()
        {
            UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            this.cboBillingCycle.SelectedIndex = 0;
            if (cboBillingCycle.Items.Count > 1)
            {
                this.cboBillingCycle.SelectedIndex = 1;
                this.dtpMonth.Value = Method.GetNextBillingMonth((int)this.cboBillingCycle.SelectedValue).AddMonths(-1);
            }
            UIHelper.SetDataSourceToComboBox(this.cboInvoiceStatus, Lookup.GetInvoiceStatus(), Resources.ALL_STATUS);
            this.cboInvoiceStatus.SelectedIndex = 0;
            if (cboInvoiceStatus.Items.Count > 1)
            {
                this.cboInvoiceStatus.SelectedIndex = 1;
            }
            UIHelper.SetDataSourceToComboBox(this.cboBiller, Lookup.GetEmployeeDistributers(), Resources.ALL_BILLER);
            //UIHelper.SetDataSourceToComboBox(this.cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(this.cboCustomerType, Lookup.GetCustomerGroup(), Resources.DisplayAllCustomer);
            UIHelper.SetDataSourceToComboBox(this.cboPrice, Lookup.GetPrices(), Resources.ALL_PRICE);
            var area = DBDataContext.Db.TBL_AREAs.Where(x=>x.IS_ACTIVE).Select(x => new AreaModel { AreaId = x.AREA_ID, AreaCode = x.AREA_CODE, AreaName = x.AREA_NAME }).ToList().OrderBy(x=>x.AreaCode);
            this.cboAreaCode.Properties.DataSource = area;
            this.cboAreaCode.Properties.DisplayMember = nameof(AreaModel.AreaName);
            this.cboAreaCode.Properties.ValueMember = nameof(AreaModel.AreaId);
            this.cboAreaCode.Properties.SelectAllItemCaption = Resources.ALL_AREA;
            this.cboAreaCode.CheckAll();
        }

        private void cboArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindData();
        }

        private void cboBiller_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindData();
        }

        private void cboBillingCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindData();
        }

        private void dtpMonth_ValueChanged(object sender, EventArgs e)
        {
            bindData();
        }

        private void rdoByBoxNo_CheckedChanged(object sender, EventArgs e)
        {
            this.txtEndBoxNo.Enabled = this.txtStartBoxNo.Enabled = this.rdoPRINT_BY_BOX.Checked;
            this.txtStartBoxNo.Focus();
            this.bindData();
        }

        private void AdvanceSearch(object sender, EventArgs e)
        {
            ExTextbox txt = (ExTextbox)sender;
            if (txt.Text.Trim() == "")
            {
                txt.AcceptSearch();
                return;
            }

            txt.CancelSearch(false);

            if (txt == this.txtStartBoxNo)
            {
                this.txtEndBoxNo.Focus();
            }
            this.txtEndBoxNo.Enabled = this.txtStartBoxNo.Enabled = true;
            bindData();

        }

        private void txtStartBoxNo_CancelAdvanceSearch(object sender, EventArgs e)
        {
            bindData();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            SupportTaker objSt = new SupportTaker(SupportTaker.Process.PrintInvoice);
            if (objSt.Check())
            {
                return;
            }
            if (dgvSummary.SelectedRows.Count == 0)
            {
                return;
            }

            //if not exists report invoice 
            string path = string.Format(Settings.Default.PATH_REPORT, Settings.Default.LANGUAGE == "en-US" ? "" : "" + Settings.Default.LANGUAGE);
            path = Path.Combine(Directory.GetCurrentDirectory(), path);
            string PathCustomized = path + "Customized\\" + Settings.Default.REPORT_INVOICE;
            if (!File.Exists(PathCustomized))
            {
                if (!File.Exists(path + Settings.Default.REPORT_INVOICE))
                {
                    MsgBox.ShowInformation(string.Format(Resources.NOT_FOUND_INVOICE, "", Settings.Default.REPORT_INVOICE), Resources.INFORMATION);
                    return;
                }
            }
            int cycleId = (int)this.cboBillingCycle.SelectedValue;
            //int areaId = (int)this.cboArea.SelectedValue;
            List<int> areaIds = new List<int>();
            if (cboAreaCode.EditValue != null)
            {
                areaIds = (cboAreaCode.EditValue as string)?.Split(',').Select(int.Parse).ToList();
            }
            int billerID = (int)this.cboBiller.SelectedValue;
            int typeId = cboConnectionType.SelectedValue == null ? 0 : (int)cboConnectionType.SelectedValue;
            int cusGroupId = (int)cboCustomerType.SelectedValue;
            int priceId = (int)this.cboPrice.SelectedValue;
            DateTime month = new DateTime(this.dtpMonth.Value.Year, this.dtpMonth.Value.Month, 1);
            string startBoxNo = this.txtStartBoxNo.Text;
            string endBoxNo = this.txtEndBoxNo.Text;
            DataTable dt = new DataTable();
            bool ifZero = this.chkIncludeZeroUsage.Checked;
            int invoicestatus = (int)this.cboInvoiceStatus.SelectedValue;
            var PintInvoieID = 0;

            Runner.RunNewThread(delegate ()
            {
                try
                {
                    dt = loadInvoiceToPrint(cycleId, areaIds, billerID, typeId, cusGroupId, priceId, month, startBoxNo, endBoxNo, ifZero, invoicestatus);

                    if (printingInvoice)
                    {
                        TBL_PRINT_INVOICE objPrintInvoice = new TBL_PRINT_INVOICE()
                        {
                            PRINT_DATE = DBDataContext.Db.GetSystemDate(),
                            LOGIN_ID = Login.CurrentLogin.LOGIN_ID
                        };
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                        {
                            DBDataContext.Db.TBL_PRINT_INVOICEs.InsertOnSubmit(objPrintInvoice);
                            DBDataContext.Db.SubmitChanges();
                            int i = 1;
                            foreach (DataRow r in dt.Rows)
                            {
                                int invoiceId = int.Parse(r["INVOICE_ID"].ToString());
                                DBDataContext.Db.TBL_PRINT_INVOICE_DETAILs.InsertOnSubmit(new TBL_PRINT_INVOICE_DETAIL()
                                {
                                    PRINT_INVOICE_ID = objPrintInvoice.PRINT_INVOICE_ID,
                                    INVOICE_ID = invoiceId,
                                    PRINT_ORDER = i++
                                });
                            }
                            DBDataContext.Db.SubmitChanges();
                            tran.Complete();
                        }
                        PintInvoieID = objPrintInvoice.PRINT_INVOICE_ID;
                    }
                    else
                    {
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                        {
                            var i = 0;
                            DBDataContext.Db.TBL_INVOICE_TO_PRINTs.DeleteAllOnSubmit(DBDataContext.Db.TBL_INVOICE_TO_PRINTs);
                            foreach (DataRow r in dt.Rows)
                            {
                                var invoiceId = (long)r["INVOICE_ID"];
                                DBDataContext.Db.TBL_INVOICE_TO_PRINTs.InsertOnSubmit(
                                    new TBL_INVOICE_TO_PRINT()
                                    {
                                        INVOICE_ID = invoiceId,
                                        PRINT_ORDER = i++
                                    }
                                );
                                DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(v => v.INVOICE_ID == invoiceId).PRINT_COUNT++;
                            }
                            DBDataContext.Db.SubmitChanges();
                            tran.Complete();
                        }

                        CrystalReportHelper cr = Method.GetInvoiceReport();
                        cr.PrintReport(Settings.Default.PRINTER_INVOICE);
                        cr.Dispose();
                    }
                }catch(Exception ex)
                {
                    MsgBox.LogError(ex);
                    throw new Exception(string.Format(Base.Properties.Resources.YOU_CANNOT_PROCESS, "", Base.Properties.Resources.PRINT_INVOICE));
                }                
            });

            //Check Print New and Old Invocie
            var InvoiceName = Settings.Default.REPORT_INVOICE;
            var PinterInvoiceName = Settings.Default.PRINTER_INVOICE;
            var TemplateString = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == 40).UTILITY_VALUE.ToString();
            var Templates = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BillPrintingListModel>>(TemplateString);
            var template = Templates.FirstOrDefault(x => x.TemplateName == InvoiceName);

            try
            {
                Runner.Run(() =>
                {
                    this.BeginInvoke(new MethodInvoker(delegate
                    {
                        if (!template.IsOld)
                        {
                            ReportLogic.Instance.GetPrintNewInvoice(InvoiceName, PinterInvoiceName, PintInvoieID);
                        }
                        else
                        {
                            CrystalReportHelper cr = Method.GetInvoiceReport(InvoiceName);
                            cr.SetParameter("@PRINT_INVOICE_ID", PintInvoieID);
                            cr.PrintReport(PinterInvoiceName);
                            cr.Dispose();
                        }
                    }));
                });
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(string.Format(Base.Properties.Resources.YOU_CANNOT_PROCESS, "", Base.Properties.Resources.PRINT_INVOICE), Base.Properties.Resources.WARNING);
            }     
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            DataTable source = null;
            int cycleId = (int)this.cboBillingCycle.SelectedValue;
            //int areaId = (int)this.cboArea.SelectedValue;
            List<int> areaIds = new List<int>();
            if(dgvSummary.SelectedRows.Count == 0)
            {
                return;
            }
            if (cboAreaCode.EditValue != null)
            {
                areaIds = (cboAreaCode.EditValue as string)?.Split(',').Select(int.Parse).ToList();
            }
            int billerID = (int)this.cboBiller.SelectedValue;
            int typeId = cboConnectionType.SelectedValue == null ? 0 : (int)cboConnectionType.SelectedValue;
            int cusGroupId = (int)cboCustomerType.SelectedValue;
            int priceId = (int)this.cboPrice.SelectedValue;
            bool ifZero = this.chkIncludeZeroUsage.Checked;
            DateTime month = new DateTime(this.dtpMonth.Value.Year, this.dtpMonth.Value.Month, 1);
            int invoicestatus = (int)this.cboInvoiceStatus.SelectedValue;

            string startBoxNo = this.txtStartBoxNo.Text;
            string endBoxNo = this.txtEndBoxNo.Text;

            Runner.Run(delegate ()
            {
                source = loadInvoiceToPrint(cycleId, areaIds, billerID, typeId, cusGroupId, priceId, month, startBoxNo, endBoxNo, ifZero, invoicestatus);
            });

            if (source != null)
            {
                new DialogPrintInvoice(source).ShowDialog();
            }
        }

        private DataTable loadInvoiceToPrint(int cycleId, List<int> areaIds, int billerID, int typeId, int cusGroupId, int priceId, DateTime month, string startBoxNo, string endBoxNo, bool ifZero, int invStatus)
        {
            // if you wish to change this "qry"
            // consider to change the same  "qry" in bindData() and Print(). 
            var qry = (from inv in DBDataContext.Db.TBL_INVOICEs
                       join cus in DBDataContext.Db.TBL_CUSTOMERs on inv.CUSTOMER_ID equals cus.CUSTOMER_ID
                       join cct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on inv.CUSTOMER_CONNECTION_TYPE_ID equals cct.CUSTOMER_CONNECTION_TYPE_ID
                       join cg in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on cct.NONLICENSE_CUSTOMER_GROUP_ID equals cg.CUSTOMER_GROUP_ID
                       join met in DBDataContext.Db.TBL_CUSTOMER_METERs.Where(c => c.IS_ACTIVE) on cus.CUSTOMER_ID equals met.CUSTOMER_ID
                       join b in DBDataContext.Db.TBL_BOXes on met.BOX_ID equals b.BOX_ID
                       join pol in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals pol.POLE_ID
                       join cx in DBDataContext.Db.TLKP_CURRENCies on inv.CURRENCY_ID equals cx.CURRENCY_ID
                       where inv.INVOICE_STATUS != (int)InvoiceStatus.Cancel
                            && inv.IS_SERVICE_BILL == false
                            && (cycleId == 0 || inv.CYCLE_ID == cycleId)
                            && (invStatus == 0 || inv.INVOICE_STATUS == invStatus)
                            //&& (areaId == 0 || cus.AREA_ID == areaId)
                            && (areaIds == null || areaIds.Contains(cus.AREA_ID))
                            && (billerID == 0 || pol.BILLER_ID == billerID)
                            && (priceId == 0 || cus.PRICE_ID == priceId)
                            && (cusGroupId == 0 || cct.NONLICENSE_CUSTOMER_GROUP_ID == cusGroupId)
                            && (typeId == 0 || inv.CUSTOMER_CONNECTION_TYPE_ID == typeId)
                            && (ifZero == true || inv.TOTAL_AMOUNT != 0)
                            && (inv.INVOICE_MONTH.Date == month.Date)
                            && (rdoPRINT_ALL.Checked || (b.BOX_CODE.CompareTo(startBoxNo) >= 0 && b.BOX_CODE.CompareTo(endBoxNo) <= 0))
                            && ((cus.INVOICE_CUSTOMER_ID == 0 && cus.USAGE_CUSTOMER_ID == 0)
                                      || cus.INVOICE_CUSTOMER_ID == cus.CUSTOMER_ID
                                      || cus.USAGE_CUSTOMER_ID == cus.CUSTOMER_ID)
                       orderby b.BOX_CODE, met.POSITION_IN_BOX, cus.CUSTOMER_CODE
                       select new
                       {
                           INVOICE_ID = inv.INVOICE_ID,
                           INVOICE_NO = inv.INVOICE_NO,
                           INVOICE_DATE = inv.INVOICE_DATE,
                           CUSTOMER_CODE = cus.CUSTOMER_CODE,
                           CUSTOMER_NAME = cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH + " " + cus.LAST_NAME + " " + cus.FIRST_NAME,
                           BOX_CODE = b.BOX_CODE,
                           METER_CODE = inv.METER_CODE,
                           TOTAL_USAGE = inv.TOTAL_USAGE
                                         // adjustment
                                         + (DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs.Where(x => x.INVOICE_ID == inv.INVOICE_ID).Sum(x => (decimal?)x.ADJUST_USAGE) ?? 0),
                           // invoice amount
                           //+ (DBDataContext.Db.TBL_INVOICEs
                           //       .Where(x => DBDataContext.Db.TBL_CUSTOMERs
                           //                       .Where(tc => tc.INVOICE_CUSTOMER_ID != tc.CUSTOMER_ID 
                           //                           && tc.INVOICE_CUSTOMER_ID == inv.CUSTOMER_ID)
                           //                       .Select(tc => tc.CUSTOMER_ID).Contains(x.CUSTOMER_ID) && x.RUN_ID == inv.RUN_ID)
                           //     .Sum(x => (decimal?)x.TOTAL_USAGE) ?? 0),

                           TOTAL_BALANCE =// Balance of Main Customer Merge
                                            DBDataContext.Db.TBL_INVOICEs.Where(x => x.INVOICE_STATUS != (int)InvoiceStatus.Cancel && x.CUSTOMER_ID == cus.CUSTOMER_ID && x.CURRENCY_ID == inv.CURRENCY_ID & x.RUN_ID <= inv.RUN_ID).Sum(x => x.SETTLE_AMOUNT - x.PAID_AMOUNT)
                           // Balance of Child Customer Merge
                           //+ (DBDataContext.Db.TBL_INVOICEs.Where(x => DBDataContext.Db.TBL_CUSTOMERs.Where(tc => tc.INVOICE_CUSTOMER_ID != tc.CUSTOMER_ID && tc.INVOICE_CUSTOMER_ID == inv.CUSTOMER_ID).Select(tc => tc.CUSTOMER_ID).Contains(x.CUSTOMER_ID) & x.RUN_ID <= inv.RUN_ID).Sum(x => (decimal?)x.SETTLE_AMOUNT - (decimal?)x.PAID_AMOUNT) ?? 0)
                           ,
                           cx.CURRENCY_ID,
                           cx.CURRENCY_SING,
                           SETTLE_AMOUNT = inv.SETTLE_AMOUNT
                       }); //.Where(q => q.TOTAL_BALANCE > 0 || q.SETTLE_AMOUNT==0);//.Concat



            return qry._ToDataTable();
        }

        private void cboCustomerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int groupId = DataHelper.ParseToInt(cboCustomerType.SelectedValue.ToString());
            UIHelper.SetDataSourceToComboBox(cboConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                                                                .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId)
                                                                .OrderBy(x => x.DESCRIPTION)
                                                                , Resources.ALL_CONNECTION_TYPE);
            if (cboConnectionType.DataSource != null)
            {
                cboConnectionType.SelectedIndex = 0;
            }
            this.bindData();
        }

        private void cboPrice_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.bindData();
        }

        private void cboPrinterInvoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Default.PRINTER_INVOICE = this.cboPrinterInvoice.Text;
            Settings.Default.Save();
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_isBinded)
            {
                return;
            }
            var selectedValue = this.cboReport.SelectedValue;
            if(selectedValue != null)
            {
                Settings.Default.REPORT_INVOICE = selectedValue?.ToString()??"";
                Settings.Default.Save();
            }
        }

        private void cboConnectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.bindData();
        }

        private void chkIncludeZeroUsage_CheckedChanged(object sender, EventArgs e)
        {
            this.bindData();
        }

        private void cboInvoiceStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.bindData();
        }

        private void btnIMPORT_INVOICE_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "rpt files (*.rpt)|*.rpt";
            if (ofd.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            pathFile = ofd.FileName;
            newFile = ofd.SafeFileName;

            DateTime date = DateTime.Now;
            string path = string.Format(Settings.Default.PATH_REPORT, Settings.Default.LANGUAGE == "en-US" ? "" : "" + Settings.Default.LANGUAGE);

            string backupFileNew = path + "BackupInvoice\\" + newFile;
            string pathBackup = path + "BackupInvoice\\";
            string defaultCustomized = path + "Customized\\";
            string PathCustomizeds = path + "Customized\\" + newFile;
            string fileNew = backupFileNew.Replace(".rpt", date.ToString("yyyyMMddHHmmss") + ".rpt");

            //!Exists path customized
            if (!Directory.Exists(defaultCustomized))
            {
                Directory.CreateDirectory(defaultCustomized);
            }

            //!Exists path backup
            if (!Directory.Exists(pathBackup))
            {
                Directory.CreateDirectory(pathBackup);
            }
            var uTILITY = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == 40);
            TBL_UTILITY UTILITY = uTILITY;
            var invoice_templates = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BillPrintingListModel>>(UTILITY.UTILITY_VALUE);
            
            if (invoice_templates.Select(x=>x.TemplateName).Contains(newFile))
            {
                if (MsgBox.ShowQuestion(Resources.DO_YOU_WANT_TO_RESTORE_INVOICE_REPORT, "") != DialogResult.Yes)
                {
                    return;
                }
            }
            //if File Exists
            if (!File.Exists(PathCustomizeds))
            {
                File.Copy(pathFile, PathCustomizeds, true);
                if (!invoice_templates.Select(x=>x.TemplateName).Contains(newFile))
                {
                    var newTemplate = new BillPrintingListModel()
                    {
                        TemplateName = newFile,
                        IsOld = false
                    };
                    invoice_templates.Add(newTemplate);
                    UTILITY.UTILITY_VALUE = Newtonsoft.Json.JsonConvert.SerializeObject(invoice_templates);
                    DBDataContext.Db.SubmitChanges();
                    MsgBox.ShowInformation(Resources.SUCCESS, Resources.INFORMATION);
                    cboReport.DataSource = invoice_templates;
                }
            }
            else
            {
                File.Copy(PathCustomizeds, fileNew, true);
                File.Copy(pathFile, PathCustomizeds, true);
                MsgBox.ShowInformation(Resources.SUCCESS, Resources.INFORMATION);
            }
        }

        private void btnEXPORT_INVOICE_Click(object sender, EventArgs e)
        {
            string path = string.Format(Settings.Default.PATH_REPORT, Settings.Default.LANGUAGE == "en-US" ? "" : "" + Settings.Default.LANGUAGE);
            path = Path.Combine(Directory.GetCurrentDirectory(), path);
            string PathCustomized = path + "Customized\\" + Settings.Default.REPORT_INVOICE;
            string PathNoCustomized = path + Settings.Default.REPORT_INVOICE;
            string defaultCustomized = path + "Customized\\";
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.FileName = Settings.Default.REPORT_INVOICE;
            saveFile.Filter = "rpt files (*.rpt)|*.rpt";

            if (saveFile.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            if (!Directory.Exists(defaultCustomized))
            {
                Directory.CreateDirectory(defaultCustomized);
                if (!File.Exists(PathNoCustomized))
                {
                    MsgBox.ShowInformation(string.Format(Resources.NOT_FOUND_INVOICE, "", Settings.Default.REPORT_INVOICE), Resources.INFORMATION);
                    return;
                }
                else
                {
                    File.Copy(PathNoCustomized, PathCustomized, true);
                    File.Copy(PathCustomized, saveFile.FileName, true);
                    RunCrystalReport(saveFile.FileName);
                }
            }

            if (saveFile.FileName == PathCustomized)
            {
                MsgBox.ShowWarning(Resources.NO_BACKUP_REPORT, Resources.WARNING);
                return;
            }

            if (!File.Exists(PathCustomized))
            {
                if (!File.Exists(PathNoCustomized))
                {
                    MsgBox.ShowInformation(string.Format(Resources.NOT_FOUND_INVOICE, "", Settings.Default.REPORT_INVOICE), Resources.INFORMATION);
                    return;
                }
                else
                {
                    File.Copy(PathNoCustomized, PathCustomized, true);
                    File.Copy(PathCustomized, saveFile.FileName, true);
                    RunCrystalReport(saveFile.FileName);
                }
            }
            else
            {
                File.Copy(PathCustomized, saveFile.FileName, true);
                RunCrystalReport(saveFile.FileName);
            }
        }

        private void RunCrystalReport(string FileName)
        {
            ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_Product WHERE name='Crystal Reports 10'");
            if (mos != null)
            {
                try
                {
                    System.Diagnostics.Process.Start(FileName);
                }
                catch (Exception ex)
                {
                    MsgBox.ShowInformation(Resources.MSG_NOT_ALREADY_INSTALL_CRYSTAL_REPORT, Resources.INFORMATION);
                    return;
                }
            }
        }
    }

    class AreaModel
    {
        public int AreaId { get; set; }
        public string AreaCode { get; set; }
        public string AreaName { get; set; }
    }
}
