﻿using DevExpress.XtraBars;
using EPower.Base.Helper;
using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Properties;
using SoftTech.Component;
using SoftTech.Security.Logic;
using SoftTech.Security.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EPower.Interface.Setting
{
    public partial class DialogHistory : ExDialog
    {
        private string _tableName;
        private int _primaryKey;
        public DateTime MinAuditDate;
        private string _title;

        public delegate void EventHandler();
        //The Event declaration
        public event EventHandler MyHandler;
        public DialogHistory(string tableName, int primaryKey, string title = "")
        {
            InitializeComponent();
            SetDefaultSetting();
            _tableName = tableName;
            _primaryKey = primaryKey;
            RegisterExportEvent();
            _title = this.Text + title;
        }
        public void RegisterExportEvent()
        {
            btnExcel.ItemClick += (object sender, ItemClickEventArgs e) => { ExportToExcel(); };
            btnPrint.ItemClick += (object sender, ItemClickEventArgs e) => { Print(); };
            btnCsv.ItemClick += (object sender, ItemClickEventArgs e) => { ExportToCsv(); };
            btnPdf.ItemClick += (object sender, ItemClickEventArgs e) => { ExportToPdf(); };
        }
        private  string ReloadFilter()
        {
            var Description = $"{Resources.START_DATE} : {rdpDate.FromDate.ToShortDate()} {Resources.TO} : {rdpDate.ToDate.ToShortDate()} ";
            return Description;
        }

        public void ExportToPdf()
        {
            DevExtension.ToPdf(tgv, _title, ReloadFilter(), _title, true);
        }

        public void Print()
        {
            DevExtension.ShowGridPreview(tgv, _title, ReloadFilter(), true);
        }

        public void ExportToCsv()
        {
            DevExtension.ToCsv(tgv, _title, ReloadFilter(), _title, true);
        }

        public void ExportToExcel()
        {
            DevExtension.ToExcel(tgv, _title, ReloadFilter(), "", _title, false, false);
        }

        public void SetDefaultSetting()
        {
            Dynamic.Helpers.ResourceHelper.ApplyResource(rdpDate);
            tgv.OptionsView.ShowIndentAsRowStyle = false;
            tgv.OptionsView.ShowVertLines = false;
            tgv.OptionsView.ShowHorzLines = false;

            //print
            tgv.OptionsPrint.PrintCheckBoxes = false;
            tgv.OptionsPrint.PrintTree = false;
            tgv.OptionsPrint.PrintTreeButtons = false;
            tgv.AppearancePrint.GroupFooter.Font = tgv.Appearance.GroupFooter.Font;
            tgv.AppearancePrint.HeaderPanel.Font = tgv.Appearance.HeaderPanel.Font;
            tgv.AppearancePrint.Row.Font = tgv.Appearance.Row.Font;
            this.Load += DialogHistory_Load;
        }

        private void DialogHistory_Load(object sender, EventArgs e)
        {
            Reload();
        }

        public void Reload()
        {

            //if tag not null it mean that user click from softtech -> so the value of primary key will take from tag.
            if (this.Tag != null)
            {
                _primaryKey = Dynamic.Helpers.Parse.ToInt(this.Tag?.ToString() ?? "0");
            }
            ReloadFilter();
            var auditTrail = AuditTrailLogic.Instance.GetHistories(_tableName, _primaryKey, rdpDate.FromDate, rdpDate.ToDate) ?? new List<AuditTrailListModel>();
            MinAuditDate = auditTrail.Min(x => x.AuditDate) ?? new DateTime(1900, 01, 01);
            tgv.DataSource = auditTrail;
        }

        private void rdpDate_ValueChanged(object sender, EventArgs e)
        {
            Reload();
        }
    }
}
