﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPriceExtraCharge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPriceExtraCharge));
            this.lblSERVICE = new System.Windows.Forms.Label();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.cboInvoiceItem = new System.Windows.Forms.ComboBox();
            this.lblEXTRA_CHARGE = new System.Windows.Forms.Label();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.cboCharge = new System.Windows.Forms.ComboBox();
            this.chkMERGE_INVOICE = new System.Windows.Forms.CheckBox();
            this.chkCHARGE_ON = new System.Windows.Forms.CheckBox();
            this.lblCHARGE_CONDITION = new System.Windows.Forms.Label();
            this.txtChargeAfter = new System.Windows.Forms.TextBox();
            this.txtChargeBefore = new System.Windows.Forms.TextBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtChargeBefore);
            this.content.Controls.Add(this.txtChargeAfter);
            this.content.Controls.Add(this.lblCHARGE_CONDITION);
            this.content.Controls.Add(this.chkCHARGE_ON);
            this.content.Controls.Add(this.chkMERGE_INVOICE);
            this.content.Controls.Add(this.cboCharge);
            this.content.Controls.Add(this.cboInvoiceItem);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.lblEXTRA_CHARGE);
            this.content.Controls.Add(this.txtValue);
            this.content.Controls.Add(this.lblSERVICE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblSERVICE, 0);
            this.content.Controls.SetChildIndex(this.txtValue, 0);
            this.content.Controls.SetChildIndex(this.lblEXTRA_CHARGE, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.cboInvoiceItem, 0);
            this.content.Controls.SetChildIndex(this.cboCharge, 0);
            this.content.Controls.SetChildIndex(this.chkMERGE_INVOICE, 0);
            this.content.Controls.SetChildIndex(this.chkCHARGE_ON, 0);
            this.content.Controls.SetChildIndex(this.lblCHARGE_CONDITION, 0);
            this.content.Controls.SetChildIndex(this.txtChargeAfter, 0);
            this.content.Controls.SetChildIndex(this.txtChargeBefore, 0);
            // 
            // lblSERVICE
            // 
            resources.ApplyResources(this.lblSERVICE, "lblSERVICE");
            this.lblSERVICE.Name = "lblSERVICE";
            // 
            // txtValue
            // 
            resources.ApplyResources(this.txtValue, "txtValue");
            this.txtValue.Name = "txtValue";
            this.txtValue.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.txtValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressDecimal);
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKeyboard);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // cboInvoiceItem
            // 
            this.cboInvoiceItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInvoiceItem.FormattingEnabled = true;
            resources.ApplyResources(this.cboInvoiceItem, "cboInvoiceItem");
            this.cboInvoiceItem.Name = "cboInvoiceItem";
            this.cboInvoiceItem.SelectedIndexChanged += new System.EventHandler(this.cboInvoiceItem_SelectedIndexChanged);
            // 
            // lblEXTRA_CHARGE
            // 
            resources.ApplyResources(this.lblEXTRA_CHARGE, "lblEXTRA_CHARGE");
            this.lblEXTRA_CHARGE.Name = "lblEXTRA_CHARGE";
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // cboCharge
            // 
            this.cboCharge.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCharge.FormattingEnabled = true;
            resources.ApplyResources(this.cboCharge, "cboCharge");
            this.cboCharge.Name = "cboCharge";
            this.cboCharge.SelectedIndexChanged += new System.EventHandler(this.cboInvoiceItem_SelectedIndexChanged);
            // 
            // chkMERGE_INVOICE
            // 
            resources.ApplyResources(this.chkMERGE_INVOICE, "chkMERGE_INVOICE");
            this.chkMERGE_INVOICE.Name = "chkMERGE_INVOICE";
            this.chkMERGE_INVOICE.UseVisualStyleBackColor = true;
            // 
            // chkCHARGE_ON
            // 
            resources.ApplyResources(this.chkCHARGE_ON, "chkCHARGE_ON");
            this.chkCHARGE_ON.Name = "chkCHARGE_ON";
            this.chkCHARGE_ON.UseVisualStyleBackColor = true;
            this.chkCHARGE_ON.CheckedChanged += new System.EventHandler(this.chkChargeOn_CheckedChanged);
            // 
            // lblCHARGE_CONDITION
            // 
            resources.ApplyResources(this.lblCHARGE_CONDITION, "lblCHARGE_CONDITION");
            this.lblCHARGE_CONDITION.Name = "lblCHARGE_CONDITION";
            // 
            // txtChargeAfter
            // 
            resources.ApplyResources(this.txtChargeAfter, "txtChargeAfter");
            this.txtChargeAfter.Name = "txtChargeAfter";
            // 
            // txtChargeBefore
            // 
            resources.ApplyResources(this.txtChargeBefore, "txtChargeBefore");
            this.txtChargeBefore.Name = "txtChargeBefore";
            // 
            // DialogPriceExtraCharge
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPriceExtraCharge";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtNote;
        private TextBox txtValue;
        private Label lblSERVICE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label1;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private ComboBox cboInvoiceItem;
        private Label lblNOTE;
        private Label lblEXTRA_CHARGE;
        private ComboBox cboCharge;
        private TextBox txtChargeBefore;
        private TextBox txtChargeAfter;
        private Label lblCHARGE_CONDITION;
        private CheckBox chkCHARGE_ON;
        private CheckBox chkMERGE_INVOICE;
    }
}