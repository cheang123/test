﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PagePosition
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PagePosition));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvPosition = new System.Windows.Forms.DataGridView();
            this.POSITION_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POSITION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_PAYROLL = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.JOB_DEPARTMENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvEmployee = new System.Windows.Forms.DataGridView();
            this.EMPLOYEE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_ACTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMPLOYEE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PASSWORD_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnREMOVE_2 = new SoftTech.Component.ExButton();
            this.btnADD_2 = new SoftTech.Component.ExButton();
            this.btnEDIT_2 = new SoftTech.Component.ExButton();
            this.cboLookup = new System.Windows.Forms.ComboBox();
            this.btnREMOVE_1 = new SoftTech.Component.ExButton();
            this.btnADD_1 = new SoftTech.Component.ExButton();
            this.btnEDIT_1 = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPosition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployee)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvPosition);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvEmployee);
            // 
            // dgvPosition
            // 
            this.dgvPosition.AllowUserToAddRows = false;
            this.dgvPosition.AllowUserToDeleteRows = false;
            this.dgvPosition.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPosition.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPosition.BackgroundColor = System.Drawing.Color.White;
            this.dgvPosition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPosition.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvPosition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPosition.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.POSITION_ID,
            this.POSITION,
            this.IS_PAYROLL,
            this.JOB_DEPARTMENT});
            resources.ApplyResources(this.dgvPosition, "dgvPosition");
            this.dgvPosition.EnableHeadersVisualStyles = false;
            this.dgvPosition.Name = "dgvPosition";
            this.dgvPosition.RowHeadersVisible = false;
            this.dgvPosition.RowTemplate.Height = 25;
            this.dgvPosition.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPosition.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPosition_CellDoubleClick);
            this.dgvPosition.SelectionChanged += new System.EventHandler(this.dgvPole_SelectionChanged);
            // 
            // POSITION_ID
            // 
            this.POSITION_ID.DataPropertyName = "POSITION_ID";
            resources.ApplyResources(this.POSITION_ID, "POSITION_ID");
            this.POSITION_ID.Name = "POSITION_ID";
            // 
            // POSITION
            // 
            this.POSITION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.POSITION.DataPropertyName = "POSITION_NAME";
            resources.ApplyResources(this.POSITION, "POSITION");
            this.POSITION.Name = "POSITION";
            // 
            // IS_PAYROLL
            // 
            this.IS_PAYROLL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.IS_PAYROLL.DataPropertyName = "IS_PAYROLL";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.NullValue = false;
            this.IS_PAYROLL.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.IS_PAYROLL, "IS_PAYROLL");
            this.IS_PAYROLL.Name = "IS_PAYROLL";
            this.IS_PAYROLL.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IS_PAYROLL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // JOB_DEPARTMENT
            // 
            this.JOB_DEPARTMENT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.JOB_DEPARTMENT.DataPropertyName = "VALUE_TEXT";
            resources.ApplyResources(this.JOB_DEPARTMENT, "JOB_DEPARTMENT");
            this.JOB_DEPARTMENT.Name = "JOB_DEPARTMENT";
            // 
            // dgvEmployee
            // 
            this.dgvEmployee.AllowUserToAddRows = false;
            this.dgvEmployee.AllowUserToDeleteRows = false;
            this.dgvEmployee.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvEmployee.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvEmployee.BackgroundColor = System.Drawing.Color.White;
            this.dgvEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployee.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EMPLOYEE_ID,
            this.IS_ACTIVE,
            this.EMPLOYEE,
            this.PASSWORD_});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEmployee.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.dgvEmployee, "dgvEmployee");
            this.dgvEmployee.EnableHeadersVisualStyles = false;
            this.dgvEmployee.MultiSelect = false;
            this.dgvEmployee.Name = "dgvEmployee";
            this.dgvEmployee.ReadOnly = true;
            this.dgvEmployee.RowHeadersVisible = false;
            this.dgvEmployee.RowTemplate.Height = 25;
            this.dgvEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEmployee.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmployee_CellDoubleClick);
            // 
            // EMPLOYEE_ID
            // 
            this.EMPLOYEE_ID.DataPropertyName = "EMPLOYEE_ID";
            resources.ApplyResources(this.EMPLOYEE_ID, "EMPLOYEE_ID");
            this.EMPLOYEE_ID.Name = "EMPLOYEE_ID";
            this.EMPLOYEE_ID.ReadOnly = true;
            // 
            // IS_ACTIVE
            // 
            this.IS_ACTIVE.DataPropertyName = "IS_ACTIVE";
            resources.ApplyResources(this.IS_ACTIVE, "IS_ACTIVE");
            this.IS_ACTIVE.Name = "IS_ACTIVE";
            this.IS_ACTIVE.ReadOnly = true;
            // 
            // EMPLOYEE
            // 
            this.EMPLOYEE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EMPLOYEE.DataPropertyName = "EMPLOYEE_NAME";
            resources.ApplyResources(this.EMPLOYEE, "EMPLOYEE");
            this.EMPLOYEE.Name = "EMPLOYEE";
            this.EMPLOYEE.ReadOnly = true;
            // 
            // PASSWORD_
            // 
            this.PASSWORD_.DataPropertyName = "PASSWORD";
            resources.ApplyResources(this.PASSWORD_, "PASSWORD_");
            this.PASSWORD_.Name = "PASSWORD_";
            this.PASSWORD_.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnREMOVE_2);
            this.panel1.Controls.Add(this.btnADD_2);
            this.panel1.Controls.Add(this.btnEDIT_2);
            this.panel1.Controls.Add(this.cboLookup);
            this.panel1.Controls.Add(this.btnREMOVE_1);
            this.panel1.Controls.Add(this.btnADD_1);
            this.panel1.Controls.Add(this.btnEDIT_1);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnREMOVE_2
            // 
            resources.ApplyResources(this.btnREMOVE_2, "btnREMOVE_2");
            this.btnREMOVE_2.Name = "btnREMOVE_2";
            this.btnREMOVE_2.UseVisualStyleBackColor = true;
            this.btnREMOVE_2.Click += new System.EventHandler(this.btnRemoveBox_Click);
            // 
            // btnADD_2
            // 
            resources.ApplyResources(this.btnADD_2, "btnADD_2");
            this.btnADD_2.Name = "btnADD_2";
            this.btnADD_2.UseVisualStyleBackColor = true;
            this.btnADD_2.Click += new System.EventHandler(this.btnNewBox_Click);
            // 
            // btnEDIT_2
            // 
            resources.ApplyResources(this.btnEDIT_2, "btnEDIT_2");
            this.btnEDIT_2.Name = "btnEDIT_2";
            this.btnEDIT_2.UseVisualStyleBackColor = true;
            this.btnEDIT_2.Click += new System.EventHandler(this.btnEditBox_Click);
            // 
            // cboLookup
            // 
            this.cboLookup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLookup.FormattingEnabled = true;
            this.cboLookup.Items.AddRange(new object[] {
            resources.GetString("cboLookup.Items"),
            resources.GetString("cboLookup.Items1"),
            resources.GetString("cboLookup.Items2"),
            resources.GetString("cboLookup.Items3"),
            resources.GetString("cboLookup.Items4")});
            resources.ApplyResources(this.cboLookup, "cboLookup");
            this.cboLookup.Name = "cboLookup";
            this.cboLookup.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            this.cboLookup.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // btnREMOVE_1
            // 
            resources.ApplyResources(this.btnREMOVE_1, "btnREMOVE_1");
            this.btnREMOVE_1.Name = "btnREMOVE_1";
            this.btnREMOVE_1.UseVisualStyleBackColor = true;
            this.btnREMOVE_1.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnADD_1
            // 
            resources.ApplyResources(this.btnADD_1, "btnADD_1");
            this.btnADD_1.Name = "btnADD_1";
            this.btnADD_1.UseVisualStyleBackColor = true;
            this.btnADD_1.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT_1
            // 
            resources.ApplyResources(this.btnEDIT_1, "btnEDIT_1");
            this.btnEDIT_1.Name = "btnEDIT_1";
            this.btnEDIT_1.UseVisualStyleBackColor = true;
            this.btnEDIT_1.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // PagePosition
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Name = "PagePosition";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPosition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployee)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnADD_1;
        private ExButton btnEDIT_1;
        private ExButton btnREMOVE_1;
        public ComboBox cboLookup;
        private SplitContainer splitContainer1;
        private DataGridView dgvPosition;
        private DataGridView dgvEmployee;
        private ExButton btnREMOVE_2;
        private ExButton btnADD_2;
        private ExButton btnEDIT_2;
        private DataGridViewTextBoxColumn EMPLOYEE_ID;
        private DataGridViewTextBoxColumn IS_ACTIVE;
        private DataGridViewTextBoxColumn EMPLOYEE;
        private DataGridViewTextBoxColumn PASSWORD_;
        private DataGridViewTextBoxColumn POSITION_ID;
        private DataGridViewTextBoxColumn POSITION;
        private DataGridViewCheckBoxColumn IS_PAYROLL;
        private DataGridViewTextBoxColumn JOB_DEPARTMENT;
    }
}
