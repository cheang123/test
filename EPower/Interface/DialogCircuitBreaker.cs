﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using SoftTech.Security.Logic;

namespace EPower.Interface
{
    public partial class DialogCircuitBreaker : ExDialog
    {
        
        GeneralProcess _flag;
        TBL_CIRCUIT_BREAKER _objNew = new TBL_CIRCUIT_BREAKER();
        public TBL_CIRCUIT_BREAKER CircuitBraker
        {
            get { return _objNew; }
        }
        TBL_CIRCUIT_BREAKER _objOld = new TBL_CIRCUIT_BREAKER();

        #region Constructor
        public DialogCircuitBreaker(GeneralProcess flag, TBL_CIRCUIT_BREAKER objBraker)
        {
            InitializeComponent();
            _flag = flag;

            dataLookUp();
            objBraker._CopyTo(_objNew);
            objBraker._CopyTo(_objOld);

            if (flag == GeneralProcess.Insert)
            {
                //TODO:change status.
                this.Text = string.Concat(Resources.INSERT, this.Text);
                txtBrakerCode.Text = _objNew.BREAKER_CODE;
                this.cboBrakerType.SelectedIndex = -1;
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
                read();
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                read();
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            
        }
        #endregion

        #region Method
        private void read()
        {            
            txtBrakerCode.Text = _objNew.BREAKER_CODE;
            cboBrakerType.SelectedValue = _objNew.BREAKER_TYPE_ID;
            cboStatus.SelectedValue = _objNew.STATUS_ID;
        }

        private void write()
        {
            _objNew.BREAKER_CODE = txtBrakerCode.Text.Trim();
            _objNew.BREAKER_TYPE_ID = (int)cboBrakerType.SelectedValue;
            _objNew.STATUS_ID = (int)cboStatus.SelectedValue;

            _objNew.CREATE_BY=
            _objOld.CREATE_BY= Login.CurrentLogin.LOGIN_NAME;
            _objNew.CREATE_ON=
            _objOld.CREATE_ON= DBDataContext.Db.GetSystemDate();            
        }

        private void dataLookUp()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {                
                //Get braker type.
                bindBrakerType();
                //Get status.
                UIHelper.SetDataSourceToComboBox(cboStatus,
                    from s in DBDataContext.Db.TLKP_BRAKER_STATUS
                    select new
                    {
                        s.STATUS_ID,
                        s.STATUS_NAME
                    }, "STATUS_ID", "STATUS_NAME");               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            this.Cursor = Cursors.Default;
        }

        private void bindBrakerType()
        {
            UIHelper.SetDataSourceToComboBox(cboBrakerType,
                from b in DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs
                where b.IS_ACTIVE
                select new
                {
                    b.BREAKER_TYPE_ID,
                    b.BREAKER_TYPE_NAME
                }, "BREAKER_TYPE_ID", "BREAKER_TYPE_NAME");
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtBrakerCode.Text.Trim() == string.Empty)
            {
                txtBrakerCode.SetValidation(string.Format(Resources.REQUIRED, lblBREAKER.Text));
                val = true;
            }            
            if (cboBrakerType.SelectedIndex == -1)
            {
                cboBrakerType.SetValidation(string.Format(Resources.REQUIRED, lblBREAKER_TYPE.Text));
                val = true;
            }
            if (cboStatus.SelectedIndex == -1)
            {
                cboStatus.SetValidation(string.Format(Resources.REQUIRED, lblSTATUS.Text));
                val = true;
            }            
            return val;
        } 
        #endregion

        #region Operation
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();

            txtBrakerCode.ClearValidation();
 
            // validate breaker status
            if (this._flag == GeneralProcess.Insert)
            {
                if (this._objNew.STATUS_ID == (int)BreakerStatus.Used)
                {
                    cboStatus.SetValidation(Resources.MS_NEW_BREAKER_STATUS);
                    return;
                } 
            }
            else
            {
                // if change from InStock to Other
                if (this._objOld.STATUS_ID == (int)BreakerStatus.Used
                    && this._objNew.STATUS_ID != (int)BreakerStatus.Used)
                {
                    cboStatus.SetValidation(Resources.MS_BREAKER_CANNOT_CHANGE_TO_OTHER_STATUS);
                    return;
                }
                // change from Other to Instock
                if (this._objOld.STATUS_ID != (int)BreakerStatus.Used
                   && (int)this._objNew.STATUS_ID == (int)BreakerStatus.Used)
                {
                    cboStatus.SetValidation(Resources.MS_BREAKER_STATUS_IN_USE);
                    return;
                }
            }

            if (DBDataContext.Db.IsExits(_objNew, "BREAKER_CODE"))
            {                
                txtBrakerCode.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblBREAKER.Text));                
                return;
            }
            try
            {

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                { 
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                        ////Stock Transaction
                        //TBL_STOCK_TRAN objStockTran = new TBL_STOCK_TRAN();
                        //objStockTran.FROM_STOCK_TYPE_ID =(int) StockType.None;
                        //objStockTran.TO_STOCK_TYPE_ID = (int)StockType.Stock;
                        //objStockTran.ITEM_ID = _objNew.BREAKER_TYPE_ID;
                        //objStockTran.ITEM_TYPE_ID = (int)StockItemType.Breaker;
                        //objStockTran.QTY = 1;
                        //objStockTran.REMARK = _objNew.BREAKER_CODE;
                        //objStockTran.STOCK_TRAN_TYPE_ID = (int)StockTranType.StockIn;
                        //objStockTran.UNIT_PRICE = 0;
                        //objStockTran.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                        //objStockTran.CREATE_ON = DBDataContext.Db.GetSystemDate();
                        //DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(objStockTran);
                        //DBDataContext.Db.SubmitChanges();
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                        //if (_objNew.STATUS_ID!=_objOld.STATUS_ID)
                        //{
                        //    //Stock Transaction
                        //    TBL_STOCK_TRAN objStockTran = new TBL_STOCK_TRAN();
                        //    objStockTran.FROM_STOCK_TYPE_ID = _objOld.STATUS_ID;
                        //    objStockTran.TO_STOCK_TYPE_ID = _objNew.STATUS_ID;
                        //    objStockTran.ITEM_ID = _objNew.BREAKER_TYPE_ID;
                        //    objStockTran.ITEM_TYPE_ID = (int)StockItemType.Breaker;
                        //    objStockTran.QTY = 1;
                        //    objStockTran.REMARK = _objNew.BREAKER_CODE;
                        //    objStockTran.STOCK_TRAN_TYPE_ID = (int)StockTranType.Adjust;
                        //    objStockTran.UNIT_PRICE = 0;
                        //    objStockTran.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                        //    objStockTran.CREATE_ON = DBDataContext.Db.GetSystemDate();
                        //    DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(objStockTran);
                        //    DBDataContext.Db.SubmitChanges();
                        //}
                        //if (_objOld.BREAKER_TYPE_ID != _objNew.BREAKER_TYPE_ID)
                        //{
                        //    DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(new TBL_STOCK_TRAN()
                        //    {
                        //        STOCK_TRAN_TYPE_ID = (int)StockTranType.Adjust,
                        //        FROM_STOCK_TYPE_ID = _objOld.STATUS_ID,
                        //        TO_STOCK_TYPE_ID = (int)StockType.None,
                        //        CREATE_BY = Logic.Login.CurrentLogin.LOGIN_NAME,
                        //        CREATE_ON = DBDataContext.Db.GetSystemDate(),
                        //        ITEM_ID = _objOld.BREAKER_TYPE_ID,
                        //        ITEM_TYPE_ID = (int)StockItemType.Breaker,
                        //        REMARK = _objOld.BREAKER_CODE,
                        //        QTY = 1,
                        //        UNIT_PRICE=0
                        //    });
                        //    DBDataContext.Db.SubmitChanges();
                        //    DBDataContext.Db.TBL_STOCK_TRANs.InsertOnSubmit(new TBL_STOCK_TRAN()
                        //    {
                        //        STOCK_TRAN_TYPE_ID = (int)StockTranType.Adjust,
                        //        FROM_STOCK_TYPE_ID = (int)StockType.None,
                        //        TO_STOCK_TYPE_ID = _objNew.STATUS_ID,
                        //        CREATE_BY = Logic.Login.CurrentLogin.LOGIN_NAME,
                        //        CREATE_ON = DBDataContext.Db.GetSystemDate(),
                        //        ITEM_ID = _objNew.BREAKER_TYPE_ID,
                        //        ITEM_TYPE_ID = (int)StockItemType.Breaker,
                        //        QTY = 1,
                        //        UNIT_PRICE=0,
                        //        REMARK=_objNew.BREAKER_CODE,
                                 
                        //    });
                        //    DBDataContext.Db.SubmitChanges();
                        //}
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                         DBDataContext.Db.Delete(_objNew);                         
                    }
                     
                    
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }        
        #endregion                

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void btnAddCbType_AddItem(object sender, EventArgs e)
        {
            DialogCircuitBreakerType dig = new DialogCircuitBreakerType(GeneralProcess.Insert, new TBL_CIRCUIT_BREAKER_TYPE());
            if (dig.ShowDialog()== DialogResult.OK)
            {
                bindBrakerType();
                cboBrakerType.SelectedValue = dig.CircuitBrakerType.BREAKER_TYPE_ID;
            }
        }
    }
}
