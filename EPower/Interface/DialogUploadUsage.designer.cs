﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogUploadUsage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogUploadUsage));
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.lblTOTAL_CUSTOMER = new System.Windows.Forms.Label();
            this.txtTotalCustomer = new System.Windows.Forms.TextBox();
            this.btnSEND_DATA = new SoftTech.Component.ExButton();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.txtCycle = new System.Windows.Forms.TextBox();
            this.txtArea = new System.Windows.Forms.TextBox();
            this.btnSelectCycle_ = new SoftTech.Component.ExButton();
            this.btnSelectArea_ = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnSelectArea_);
            this.content.Controls.Add(this.btnSelectCycle_);
            this.content.Controls.Add(this.txtCycle);
            this.content.Controls.Add(this.txtArea);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnSEND_DATA);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.lblTOTAL_CUSTOMER);
            this.content.Controls.Add(this.txtTotalCustomer);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.txtTotalCustomer, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_CUSTOMER, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnSEND_DATA, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.txtArea, 0);
            this.content.Controls.SetChildIndex(this.txtCycle, 0);
            this.content.Controls.SetChildIndex(this.btnSelectCycle_, 0);
            this.content.Controls.SetChildIndex(this.btnSelectArea_, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::EPower.Properties.Resources.dock;
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // checkBox1
            // 
            resources.ApplyResources(this.checkBox1, "checkBox1");
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // lblTOTAL_CUSTOMER
            // 
            resources.ApplyResources(this.lblTOTAL_CUSTOMER, "lblTOTAL_CUSTOMER");
            this.lblTOTAL_CUSTOMER.Name = "lblTOTAL_CUSTOMER";
            // 
            // txtTotalCustomer
            // 
            resources.ApplyResources(this.txtTotalCustomer, "txtTotalCustomer");
            this.txtTotalCustomer.Name = "txtTotalCustomer";
            this.txtTotalCustomer.ReadOnly = true;
            // 
            // btnSEND_DATA
            // 
            resources.ApplyResources(this.btnSEND_DATA, "btnSEND_DATA");
            this.btnSEND_DATA.Name = "btnSEND_DATA";
            this.btnSEND_DATA.UseVisualStyleBackColor = true;
            this.btnSEND_DATA.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.BackColor = System.Drawing.Color.Transparent;
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.BackColor = System.Drawing.Color.Transparent;
            this.lblAREA.Name = "lblAREA";
            // 
            // txtCycle
            // 
            resources.ApplyResources(this.txtCycle, "txtCycle");
            this.txtCycle.Name = "txtCycle";
            this.txtCycle.ReadOnly = true;
            // 
            // txtArea
            // 
            resources.ApplyResources(this.txtArea, "txtArea");
            this.txtArea.Name = "txtArea";
            this.txtArea.ReadOnly = true;
            // 
            // btnSelectCycle_
            // 
            resources.ApplyResources(this.btnSelectCycle_, "btnSelectCycle_");
            this.btnSelectCycle_.Name = "btnSelectCycle_";
            this.btnSelectCycle_.UseVisualStyleBackColor = true;
            this.btnSelectCycle_.Click += new System.EventHandler(this.btnSelectCycle_Click);
            // 
            // btnSelectArea_
            // 
            resources.ApplyResources(this.btnSelectArea_, "btnSelectArea_");
            this.btnSelectArea_.Name = "btnSelectArea_";
            this.btnSelectArea_.UseVisualStyleBackColor = true;
            this.btnSelectArea_.Click += new System.EventHandler(this.btnSelectArea_Click);
            // 
            // DialogUploadUsage
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogUploadUsage";
            this.TopMost = true;
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private Panel panel1;
        private Panel panel4;
        
        private Label label1;
        private CheckBox checkBox1;
        private Label lblTOTAL_CUSTOMER;
        private TextBox txtTotalCustomer;
        private ExButton btnSEND_DATA;
        private Label lblCYCLE_NAME;
        private Label lblAREA;
        private TextBox txtCycle;
        private TextBox txtArea;
        private ExButton btnSelectArea_;
        private ExButton btnSelectCycle_;
    }
}