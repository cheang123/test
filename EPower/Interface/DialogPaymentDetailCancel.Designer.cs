﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPaymentDetailCancel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPaymentDetailCancel));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblAPPLY_PAYMENT = new System.Windows.Forms.Label();
            this.dgvPaymentDetail = new System.Windows.Forms.DataGridView();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.txtPaymentAccount = new System.Windows.Forms.TextBox();
            this.txtPayDate = new System.Windows.Forms.TextBox();
            this.txtCreateBy = new System.Windows.Forms.TextBox();
            this.lblCREATE_BY = new System.Windows.Forms.Label();
            this.txtPaymentNo = new System.Windows.Forms.TextBox();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.lblPAID_DATE = new System.Windows.Forms.Label();
            this.lblRECEIPT_NO = new System.Windows.Forms.Label();
            this.lblRECEIPT_INFORMATION = new System.Windows.Forms.Label();
            this.lblPAID_AMOUNT = new System.Windows.Forms.Label();
            this.txtPayAmount = new System.Windows.Forms.TextBox();
            this.INVOICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_TITLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaymentDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblRECEIPT_INFORMATION);
            this.content.Controls.Add(this.txtPaymentAccount);
            this.content.Controls.Add(this.txtPayDate);
            this.content.Controls.Add(this.txtPayAmount);
            this.content.Controls.Add(this.txtCreateBy);
            this.content.Controls.Add(this.lblPAID_AMOUNT);
            this.content.Controls.Add(this.lblCREATE_BY);
            this.content.Controls.Add(this.txtPaymentNo);
            this.content.Controls.Add(this.txtCustomer);
            this.content.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblPAID_DATE);
            this.content.Controls.Add(this.lblRECEIPT_NO);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.lblAPPLY_PAYMENT);
            this.content.Controls.Add(this.dgvPaymentDetail);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.dgvPaymentDetail, 0);
            this.content.Controls.SetChildIndex(this.lblAPPLY_PAYMENT, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblRECEIPT_NO, 0);
            this.content.Controls.SetChildIndex(this.lblPAID_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtCustomer, 0);
            this.content.Controls.SetChildIndex(this.txtPaymentNo, 0);
            this.content.Controls.SetChildIndex(this.lblCREATE_BY, 0);
            this.content.Controls.SetChildIndex(this.lblPAID_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtCreateBy, 0);
            this.content.Controls.SetChildIndex(this.txtPayAmount, 0);
            this.content.Controls.SetChildIndex(this.txtPayDate, 0);
            this.content.Controls.SetChildIndex(this.txtPaymentAccount, 0);
            this.content.Controls.SetChildIndex(this.lblRECEIPT_INFORMATION, 0);
            // 
            // lblAPPLY_PAYMENT
            // 
            resources.ApplyResources(this.lblAPPLY_PAYMENT, "lblAPPLY_PAYMENT");
            this.lblAPPLY_PAYMENT.Name = "lblAPPLY_PAYMENT";
            // 
            // dgvPaymentDetail
            // 
            this.dgvPaymentDetail.AllowUserToAddRows = false;
            this.dgvPaymentDetail.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPaymentDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPaymentDetail.BackgroundColor = System.Drawing.Color.White;
            this.dgvPaymentDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPaymentDetail.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvPaymentDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPaymentDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.INVOICE_ID,
            this.INVOICE_NO,
            this.INVOICE_TITLE,
            this.PAY_AMOUNT,
            this.DUE_AMOUNT,
            this.BALANCE,
            this.CURRENCY_SING_});
            this.dgvPaymentDetail.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvPaymentDetail, "dgvPaymentDetail");
            this.dgvPaymentDetail.Name = "dgvPaymentDetail";
            this.dgvPaymentDetail.ReadOnly = true;
            this.dgvPaymentDetail.RowHeadersVisible = false;
            this.dgvPaymentDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPaymentDetail_CellContentClick);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtPaymentAccount
            // 
            resources.ApplyResources(this.txtPaymentAccount, "txtPaymentAccount");
            this.txtPaymentAccount.Name = "txtPaymentAccount";
            this.txtPaymentAccount.ReadOnly = true;
            // 
            // txtPayDate
            // 
            resources.ApplyResources(this.txtPayDate, "txtPayDate");
            this.txtPayDate.Name = "txtPayDate";
            this.txtPayDate.ReadOnly = true;
            // 
            // txtCreateBy
            // 
            resources.ApplyResources(this.txtCreateBy, "txtCreateBy");
            this.txtCreateBy.Name = "txtCreateBy";
            this.txtCreateBy.ReadOnly = true;
            // 
            // lblCREATE_BY
            // 
            resources.ApplyResources(this.lblCREATE_BY, "lblCREATE_BY");
            this.lblCREATE_BY.Name = "lblCREATE_BY";
            // 
            // txtPaymentNo
            // 
            resources.ApplyResources(this.txtPaymentNo, "txtPaymentNo");
            this.txtPaymentNo.Name = "txtPaymentNo";
            this.txtPaymentNo.ReadOnly = true;
            // 
            // txtCustomer
            // 
            resources.ApplyResources(this.txtCustomer, "txtCustomer");
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.ReadOnly = true;
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // lblPAID_DATE
            // 
            resources.ApplyResources(this.lblPAID_DATE, "lblPAID_DATE");
            this.lblPAID_DATE.Name = "lblPAID_DATE";
            // 
            // lblRECEIPT_NO
            // 
            resources.ApplyResources(this.lblRECEIPT_NO, "lblRECEIPT_NO");
            this.lblRECEIPT_NO.Name = "lblRECEIPT_NO";
            // 
            // lblRECEIPT_INFORMATION
            // 
            resources.ApplyResources(this.lblRECEIPT_INFORMATION, "lblRECEIPT_INFORMATION");
            this.lblRECEIPT_INFORMATION.Name = "lblRECEIPT_INFORMATION";
            // 
            // lblPAID_AMOUNT
            // 
            resources.ApplyResources(this.lblPAID_AMOUNT, "lblPAID_AMOUNT");
            this.lblPAID_AMOUNT.Name = "lblPAID_AMOUNT";
            // 
            // txtPayAmount
            // 
            resources.ApplyResources(this.txtPayAmount, "txtPayAmount");
            this.txtPayAmount.Name = "txtPayAmount";
            this.txtPayAmount.ReadOnly = true;
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.DataPropertyName = "INVOICE_ID";
            resources.ApplyResources(this.INVOICE_ID, "INVOICE_ID");
            this.INVOICE_ID.Name = "INVOICE_ID";
            this.INVOICE_ID.ReadOnly = true;
            // 
            // INVOICE_NO
            // 
            this.INVOICE_NO.DataPropertyName = "INVOICE_NO";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Underline);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Blue;
            this.INVOICE_NO.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.INVOICE_NO, "INVOICE_NO");
            this.INVOICE_NO.Name = "INVOICE_NO";
            this.INVOICE_NO.ReadOnly = true;
            // 
            // INVOICE_TITLE
            // 
            this.INVOICE_TITLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.INVOICE_TITLE.DataPropertyName = "INVOICE_TITLE";
            resources.ApplyResources(this.INVOICE_TITLE, "INVOICE_TITLE");
            this.INVOICE_TITLE.Name = "INVOICE_TITLE";
            this.INVOICE_TITLE.ReadOnly = true;
            // 
            // PAY_AMOUNT
            // 
            this.PAY_AMOUNT.DataPropertyName = "PAY_AMOUNT";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,##0.####";
            this.PAY_AMOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.PAY_AMOUNT, "PAY_AMOUNT");
            this.PAY_AMOUNT.Name = "PAY_AMOUNT";
            this.PAY_AMOUNT.ReadOnly = true;
            // 
            // DUE_AMOUNT
            // 
            this.DUE_AMOUNT.DataPropertyName = "DUE_AMOUNT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,##0.####";
            this.DUE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.DUE_AMOUNT, "DUE_AMOUNT");
            this.DUE_AMOUNT.Name = "DUE_AMOUNT";
            this.DUE_AMOUNT.ReadOnly = true;
            // 
            // BALANCE
            // 
            this.BALANCE.DataPropertyName = "BALANCE";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.####";
            this.BALANCE.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.BALANCE, "BALANCE");
            this.BALANCE.Name = "BALANCE";
            this.BALANCE.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // DialogPaymentDetailCancel
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogPaymentDetailCancel";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaymentDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblAPPLY_PAYMENT;
        private DataGridView dgvPaymentDetail;
        private ExButton btnCLOSE;
        private TextBox txtPaymentAccount;
        private TextBox txtPayDate;
        private TextBox txtCreateBy;
        private Label lblCREATE_BY;
        private TextBox txtPaymentNo;
        private TextBox txtCustomer;
        private Label lblPAYMENT_ACCOUNT;
        private Label lblCUSTOMER_NAME;
        private Label lblPAID_DATE;
        private Label lblRECEIPT_NO;
        private Label lblRECEIPT_INFORMATION;
        private TextBox txtPayAmount;
        private Label lblPAID_AMOUNT;
        private DataGridViewTextBoxColumn INVOICE_ID;
        private DataGridViewTextBoxColumn INVOICE_NO;
        private DataGridViewTextBoxColumn INVOICE_TITLE;
        private DataGridViewTextBoxColumn PAY_AMOUNT;
        private DataGridViewTextBoxColumn DUE_AMOUNT;
        private DataGridViewTextBoxColumn BALANCE;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
    }
}
