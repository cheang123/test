﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerEquipment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerEquipment));
            this.lblEQUIPMENT = new System.Windows.Forms.Label();
            this.cboEquitment = new System.Windows.Forms.ComboBox();
            this.lblQUANTITY = new System.Windows.Forms.Label();
            this.lblWATT = new System.Windows.Forms.Label();
            this.lblTOTAL_WATT = new System.Windows.Forms.Label();
            this.lblCOMMENT = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.txtPower = new System.Windows.Forms.TextBox();
            this.txtTotalPower = new System.Windows.Forms.TextBox();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.label25 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAddEquipment = new SoftTech.Component.ExAddItem();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnAddEquipment);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label25);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.cboEquitment);
            this.content.Controls.Add(this.txtPower);
            this.content.Controls.Add(this.txtTotalPower);
            this.content.Controls.Add(this.lblEQUIPMENT);
            this.content.Controls.Add(this.lblCOMMENT);
            this.content.Controls.Add(this.lblTOTAL_WATT);
            this.content.Controls.Add(this.txtComment);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.lblWATT);
            this.content.Controls.Add(this.lblQUANTITY);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtQuantity);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.txtQuantity, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.lblQUANTITY, 0);
            this.content.Controls.SetChildIndex(this.lblWATT, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.txtComment, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_WATT, 0);
            this.content.Controls.SetChildIndex(this.lblCOMMENT, 0);
            this.content.Controls.SetChildIndex(this.lblEQUIPMENT, 0);
            this.content.Controls.SetChildIndex(this.txtTotalPower, 0);
            this.content.Controls.SetChildIndex(this.txtPower, 0);
            this.content.Controls.SetChildIndex(this.cboEquitment, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label25, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.btnAddEquipment, 0);
            // 
            // lblEQUIPMENT
            // 
            resources.ApplyResources(this.lblEQUIPMENT, "lblEQUIPMENT");
            this.lblEQUIPMENT.Name = "lblEQUIPMENT";
            // 
            // cboEquitment
            // 
            this.cboEquitment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEquitment.FormattingEnabled = true;
            resources.ApplyResources(this.cboEquitment, "cboEquitment");
            this.cboEquitment.Name = "cboEquitment";
            this.cboEquitment.SelectedIndexChanged += new System.EventHandler(this.cboEquitment_SelectedIndexChanged);
            // 
            // lblQUANTITY
            // 
            resources.ApplyResources(this.lblQUANTITY, "lblQUANTITY");
            this.lblQUANTITY.Name = "lblQUANTITY";
            // 
            // lblWATT
            // 
            resources.ApplyResources(this.lblWATT, "lblWATT");
            this.lblWATT.Name = "lblWATT";
            // 
            // lblTOTAL_WATT
            // 
            resources.ApplyResources(this.lblTOTAL_WATT, "lblTOTAL_WATT");
            this.lblTOTAL_WATT.Name = "lblTOTAL_WATT";
            // 
            // lblCOMMENT
            // 
            resources.ApplyResources(this.lblCOMMENT, "lblCOMMENT");
            this.lblCOMMENT.Name = "lblCOMMENT";
            // 
            // txtQuantity
            // 
            resources.ApplyResources(this.txtQuantity, "txtQuantity");
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Enter += new System.EventHandler(this.txtQuantity_Enter);
            this.txtQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumberOnly);
            this.txtQuantity.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtQuantity_KeyUp);
            // 
            // txtPower
            // 
            resources.ApplyResources(this.txtPower, "txtPower");
            this.txtPower.Name = "txtPower";
            this.txtPower.Enter += new System.EventHandler(this.txtQuantity_Enter);
            this.txtPower.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumberOnly);
            this.txtPower.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPower_KeyUp);
            // 
            // txtTotalPower
            // 
            resources.ApplyResources(this.txtTotalPower, "txtTotalPower");
            this.txtTotalPower.Name = "txtTotalPower";
            this.txtTotalPower.ReadOnly = true;
            this.txtTotalPower.TabStop = false;
            // 
            // txtComment
            // 
            resources.ApplyResources(this.txtComment, "txtComment");
            this.txtComment.Name = "txtComment";
            this.txtComment.Enter += new System.EventHandler(this.txtComment_Enter);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Name = "label25";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // btnAddEquipment
            // 
            this.btnAddEquipment.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddEquipment, "btnAddEquipment");
            this.btnAddEquipment.Name = "btnAddEquipment";
            this.btnAddEquipment.AddItem += new System.EventHandler(this.btnAddEquipment_AddItem);
            // 
            // DialogCustomerEquipment
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogCustomerEquipment";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblEQUIPMENT;
        private Label lblCOMMENT;
        private Label lblTOTAL_WATT;
        private Label lblWATT;
        private Label lblQUANTITY;
        private ComboBox cboEquitment;
        private TextBox txtComment;
        private TextBox txtTotalPower;
        private TextBox txtPower;
        private TextBox txtQuantity;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label5;
        private Label label3;
        private Label label1;
        private Label label25;
        private ExAddItem btnAddEquipment;
    }
}
