﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogSelectCycleUsage : ExDialog
    {

        private List<TBL_BILLING_CYCLE> mSelectCycles = new List<TBL_BILLING_CYCLE>();
        public List<TBL_BILLING_CYCLE> SelectCycles
        {
            get { return mSelectCycles; }
            private set { mSelectCycles = value; }
        }

        public string DisplaySelect
        {
            get
            {
                if (SelectCycles.Count == 0)
                {
                    return "";
                }
                else if (SelectCycles.Count == dgv.Rows.Count)
                {
                    return Resources.ALL_CYCLE;
                }
                else
                {
                    return string.Join(", ", SelectCycles.Select(x => x.CYCLE_NAME).ToArray());
                }
            }
        }

        DataTable dtCycle = null;
        #region Constructor
        public DialogSelectCycleUsage(List<TBL_BILLING_CYCLE> cycle)
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            dgv.ReadOnly = false;
            CYCLE_NAME.ReadOnly = true;
            START_DATE.ReadOnly = true;
            END_DATE.ReadOnly = true;
            MONTH.ReadOnly = true;
            SELECT_.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            mSelectCycles = cycle;
        }

        CheckBox ckBox = null;

        void addCheckBoxHeader()
        {
            ckBox = new CheckBox();
            //Get the column header cell bounds
            Rectangle rect = this.dgv.GetCellDisplayRectangle(0, -1, false);
            ckBox.Size = new Size(16, 16);
            Point oPoint = new Point();
            oPoint.X = rect.Location.X + 2;
            oPoint.Y = rect.Location.Y + (rect.Height - ckBox.Height) / 2 + 1;
            if (oPoint.X < rect.X)
            {
                ckBox.Visible = false;
            }
            else
            {
                ckBox.Visible = true;
            }
            //Change the location of the CheckBox to make it stay on the header
            ckBox.Location = oPoint;
            ckBox.CheckedChanged += new EventHandler(ckBox_CheckedChanged);

            dgv.Controls.Add(ckBox);
        }

        void ckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ckBox.CheckState == CheckState.Indeterminate)
            {
                return;
            }

            foreach (DataRow dr in dtCycle.Rows)
            {
                dr[SELECT_.Name] = ckBox.Checked;
            }
            dgv.Refresh();
        }
        #endregion Constructor

        #region Method

        void Bind()
        {
            dtCycle = (from c in DBDataContext.Db.TBL_BILLING_CYCLEs
                       where c.IS_ACTIVE
                       select new
                       {
                           SELECT_ = true,
                           c.CYCLE_ID,
                           c.CYCLE_NAME,
                           START_DATE = DateTime.Now,
                           END_DATE = DateTime.Now,
                           BILLING_MONTH = Method.GetNextBillingMonth(c.CYCLE_ID)
                       })._ToDataTable();


            foreach (DataRow item in dtCycle.Rows)
            {
                DateTime d1 = DateTime.Now;
                DateTime d2 = DateTime.Now;
                DateTime month = DateTime.Now;
                month = Method.GetNextBillingMonth((int)item[CYCLE_ID.Name], ref d1, ref d2);

                if (mSelectCycles.Count == 0)
                {
                    continue;
                }
                item[SELECT_.Name] = mSelectCycles.FirstOrDefault(x => x.CYCLE_ID == (int)item[CYCLE_ID.Name]) != null;
            };

            dgv.DataSource = dtCycle;
        }

        #endregion Method 

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != SELECT_.Index || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = dgv[e.ColumnIndex, e.RowIndex];
            cell.Value = !(bool)cell.Value;
            dgv.EndEdit();
        }

        private void DialogSelectAreaUsage_FormClosing(object sender, FormClosingEventArgs e)
        {
            mSelectCycles = new List<TBL_BILLING_CYCLE>();
            foreach (var item in dtCycle.Select().Where(x => (bool)x[SELECT_.Name]))
            {
                mSelectCycles.Add(DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(x => x.CYCLE_ID == (int)item[CYCLE_ID.Name]));
            }
            if (mSelectCycles.Count == 0)
            {
                MsgBox.ShowWarning(Resources.MS_PLEASE_SELECT_CYCLE_FOR_COLLECT_CUSTOMER_USAGE, this.Text);
                e.Cancel = true;
            }
        }

        private void dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int nRecord = dtCycle.Select().Where(x => (bool)x[SELECT_.Name]).Count();
            if (ckBox == null)
            {
                return;
            }
            if (nRecord == dtCycle.Rows.Count)
            {
                ckBox.CheckState = CheckState.Checked;
            }
            else if (nRecord > 0)
            {
                ckBox.CheckState = CheckState.Indeterminate;
            }
            else
            {
                ckBox.CheckState = CheckState.Unchecked;
            }
        }

        private void DialogSelectCycleUsage_Load(object sender, EventArgs e)
        {
            Bind();
            addCheckBoxHeader();
            int nRecord = dtCycle.Select().Where(x => (bool)x[SELECT_.Name]).Count();
            if (ckBox == null)
            {
                return;
            }
            if (nRecord == dtCycle.Rows.Count)
            {
                ckBox.CheckState = CheckState.Checked;
            }
            else if (nRecord > 0)
            {
                ckBox.CheckState = CheckState.Indeterminate;
            }
            else
            {
                ckBox.CheckState = CheckState.Unchecked;
            }
        }
    }


}
