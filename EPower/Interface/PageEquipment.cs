﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    /// <summary>
    /// 
    /// </summary>
    public partial class PageEquipment : Form
    {
        /// <summary>
        /// 
        /// </summary>
        public TBL_EQUIPMENT Equipment
        {
            get
            {
                TBL_EQUIPMENT objEquipment = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intEquipmentID = (int)dgv.SelectedRows[0].Cells["EQUIPMENT_ID"].Value;
                        objEquipment = DBDataContext.Db.TBL_EQUIPMENTs.FirstOrDefault(x => x.EQUIPMENT_ID == intEquipmentID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objEquipment;
            }
        }

        #region Constructor
         public PageEquipment()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new phase.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {            
            DialogEquipment dig = new DialogEquipment(GeneralProcess.Insert, new TBL_EQUIPMENT() { EQUIPMENT_NAME = "" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Equipment.EQUIPMENT_ID);
            }
        }

        /// <summary>
        /// Edit phase.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogEquipment dig = new DialogEquipment(GeneralProcess.Update, Equipment);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Equipment.EQUIPMENT_ID);
                }
            }
        }

        /// <summary>
        /// Remove phase.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogEquipment dig = new DialogEquipment(GeneralProcess.Delete, Equipment);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Equipment.EQUIPMENT_ID - 1);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            //TODO: can delete record.
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;                                           
            }
            return val;
        }

        /// <summary>
        /// Load data form database and display.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = from ep in DBDataContext.Db.TBL_EQUIPMENTs
                                 where ep.IS_ACTIVE &&
                                 ep.EQUIPMENT_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                                 select ep;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        
    }
}
