﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogUtility
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogUtility));
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.cboPrinterCashDrawer = new System.Windows.Forms.ComboBox();
            this.cboPrinterReciept = new System.Windows.Forms.ComboBox();
            this.cboPrinterInvoice = new System.Windows.Forms.ComboBox();
            this.txtAlarmQTY = new System.Windows.Forms.TextBox();
            this.cboIRBoundRate = new System.Windows.Forms.ComboBox();
            this.cboIRComPort = new System.Windows.Forms.ComboBox();
            this.cboPrepaidComPort = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.tab = new System.Windows.Forms.TabControl();
            this.tabGENERAL = new System.Windows.Forms.TabPage();
            this.chkENABLE_EDIT_INVOICE_ADJUSTMENT = new System.Windows.Forms.CheckBox();
            this.chkENABLE_PREPAYMENT_SERVICE = new System.Windows.Forms.CheckBox();
            this.lblPREPAYMENT = new System.Windows.Forms.Label();
            this.chkENABLE_ADJUST_BASED_PRICE_SUBSIDY = new System.Windows.Forms.CheckBox();
            this.chkENABLE_EDIT_PAYMENT_DATE = new System.Windows.Forms.CheckBox();
            this.chkENABLE_ADJUST_OLD_INVOICE = new System.Windows.Forms.CheckBox();
            this.lblINVOICE = new System.Windows.Forms.Label();
            this.txtDEFAULT_DAY_TO_RUN_REF = new System.Windows.Forms.TextBox();
            this.lblDAY_TO_RUN_REF = new System.Windows.Forms.Label();
            this.lblREPORT_REF_SUBSIDY = new System.Windows.Forms.Label();
            this.txtMAX_DAY_TO_IGNORE_BILLING = new System.Windows.Forms.TextBox();
            this.lblMAX_DAY_IGNORE_ISUUE_INVOICE = new System.Windows.Forms.Label();
            this.lblISSUE_INVOICE = new System.Windows.Forms.Label();
            this.txtINVOICE_DUE_DATE = new System.Windows.Forms.TextBox();
            this.lblINVOICE_DUE_DATE = new System.Windows.Forms.Label();
            this.txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT = new System.Windows.Forms.TextBox();
            this.lblMAX_DAY_TO_IGNORE_FIXED_AMOUNT = new System.Windows.Forms.Label();
            this.lblFIX_AMOUNT = new System.Windows.Forms.Label();
            this.chkDEPOSIT_IS_IGNORED = new System.Windows.Forms.CheckBox();
            this.chkDEPOSIT_IS_PAID_AUTO = new System.Windows.Forms.CheckBox();
            this.lblDEPOSIT = new System.Windows.Forms.Label();
            this.lblCUT_POWER = new System.Windows.Forms.Label();
            this.txtCUT_OFF_AMOUNT = new System.Windows.Forms.TextBox();
            this.lblCUT_OFF_AMOUNT = new System.Windows.Forms.Label();
            this.lblDEFAULT_CUT_OFF_DAY = new System.Windows.Forms.Label();
            this.txtDEFAULT_CUT_OFF_DAY = new System.Windows.Forms.TextBox();
            this.tabDevice = new System.Windows.Forms.TabPage();
            this.cboMetroBaudRate = new System.Windows.Forms.ComboBox();
            this.btnTEST_3 = new System.Windows.Forms.Button();
            this.cboMetroCom = new System.Windows.Forms.ComboBox();
            this.lblDEVICE_BARCODE = new System.Windows.Forms.Label();
            this.btnTEST_2 = new System.Windows.Forms.Button();
            this.btnTEST_1 = new System.Windows.Forms.Button();
            this.lblDEVICE_IC_CARD = new System.Windows.Forms.Label();
            this.lblALARM_QTY = new System.Windows.Forms.Label();
            this.lblDEVICE_COLLECT_USAGE = new System.Windows.Forms.Label();
            this.lblPRINT_CASH_DRAWER = new System.Windows.Forms.Label();
            this.lblPRINT_RECEIPT = new System.Windows.Forms.Label();
            this.lblPRINT_INVOICE = new System.Windows.Forms.Label();
            this.tabSetting = new System.Windows.Forms.TabPage();
            this.chkBACKUP_BEFORE_REVERS_BILL = new System.Windows.Forms.CheckBox();
            this.chkBACKUP_BEFORE_RUN_BILL = new System.Windows.Forms.CheckBox();
            this.lblBACKUP = new System.Windows.Forms.Label();
            this.txtTOTAL_CUSTOMER_REVERS = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTOTAL_INVOICE_REVERS = new System.Windows.Forms.Label();
            this.lblREVERSE_BILL = new System.Windows.Forms.Label();
            this.cboPrepaidSequence = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPREPAID_SEQUENCE = new System.Windows.Forms.Label();
            this.txtExportNumber = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblExportNumber = new System.Windows.Forms.Label();
            this.lblE_Fillings = new System.Windows.Forms.Label();
            this.cboServiceSequence = new System.Windows.Forms.ComboBox();
            this.cboInvoiceSequence = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblINVOICE_SERVICE = new System.Windows.Forms.Label();
            this.lblINVOICE_BILL = new System.Windows.Forms.Label();
            this.lblSEQUENCE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tab.SuspendLayout();
            this.tabGENERAL.SuspendLayout();
            this.tabDevice.SuspendLayout();
            this.tabSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.tab);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.tab, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cboPrinterCashDrawer
            // 
            this.cboPrinterCashDrawer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrinterCashDrawer.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrinterCashDrawer, "cboPrinterCashDrawer");
            this.cboPrinterCashDrawer.Name = "cboPrinterCashDrawer";
            this.cboPrinterCashDrawer.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // cboPrinterReciept
            // 
            this.cboPrinterReciept.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrinterReciept.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrinterReciept, "cboPrinterReciept");
            this.cboPrinterReciept.Name = "cboPrinterReciept";
            this.cboPrinterReciept.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // cboPrinterInvoice
            // 
            this.cboPrinterInvoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrinterInvoice.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrinterInvoice, "cboPrinterInvoice");
            this.cboPrinterInvoice.Name = "cboPrinterInvoice";
            this.cboPrinterInvoice.SelectedIndexChanged += new System.EventHandler(this.cboPrinterInvoice_SelectedIndexChanged);
            this.cboPrinterInvoice.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // txtAlarmQTY
            // 
            resources.ApplyResources(this.txtAlarmQTY, "txtAlarmQTY");
            this.txtAlarmQTY.Name = "txtAlarmQTY";
            this.txtAlarmQTY.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.txtAlarmQTY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeyNumber);
            // 
            // cboIRBoundRate
            // 
            this.cboIRBoundRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIRBoundRate.FormattingEnabled = true;
            resources.ApplyResources(this.cboIRBoundRate, "cboIRBoundRate");
            this.cboIRBoundRate.Name = "cboIRBoundRate";
            this.cboIRBoundRate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // cboIRComPort
            // 
            this.cboIRComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIRComPort.FormattingEnabled = true;
            resources.ApplyResources(this.cboIRComPort, "cboIRComPort");
            this.cboIRComPort.Name = "cboIRComPort";
            this.cboIRComPort.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // cboPrepaidComPort
            // 
            this.cboPrepaidComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrepaidComPort.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrepaidComPort, "cboPrepaidComPort");
            this.cboPrepaidComPort.Name = "cboPrepaidComPort";
            this.cboPrepaidComPort.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // textBox1
            // 
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.Name = "textBox1";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Name = "label23";
            // 
            // label29
            // 
            resources.ApplyResources(this.label29, "label29");
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Name = "label29";
            // 
            // checkBox3
            // 
            resources.ApplyResources(this.checkBox3, "checkBox3");
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            resources.ApplyResources(this.checkBox4, "checkBox4");
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // label55
            // 
            resources.ApplyResources(this.label55, "label55");
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Name = "label55";
            // 
            // label56
            // 
            resources.ApplyResources(this.label56, "label56");
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Name = "label56";
            // 
            // label57
            // 
            resources.ApplyResources(this.label57, "label57");
            this.label57.ForeColor = System.Drawing.Color.Red;
            this.label57.Name = "label57";
            // 
            // textBox2
            // 
            resources.ApplyResources(this.textBox2, "textBox2");
            this.textBox2.Name = "textBox2";
            // 
            // label58
            // 
            resources.ApplyResources(this.label58, "label58");
            this.label58.Name = "label58";
            // 
            // label59
            // 
            resources.ApplyResources(this.label59, "label59");
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Name = "label59";
            // 
            // label60
            // 
            resources.ApplyResources(this.label60, "label60");
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Name = "label60";
            // 
            // tab
            // 
            this.tab.Controls.Add(this.tabGENERAL);
            this.tab.Controls.Add(this.tabDevice);
            this.tab.Controls.Add(this.tabSetting);
            resources.ApplyResources(this.tab, "tab");
            this.tab.Name = "tab";
            this.tab.SelectedIndex = 0;
            // 
            // tabGENERAL
            // 
            this.tabGENERAL.Controls.Add(this.chkENABLE_EDIT_INVOICE_ADJUSTMENT);
            this.tabGENERAL.Controls.Add(this.chkENABLE_PREPAYMENT_SERVICE);
            this.tabGENERAL.Controls.Add(this.lblPREPAYMENT);
            this.tabGENERAL.Controls.Add(this.chkENABLE_ADJUST_BASED_PRICE_SUBSIDY);
            this.tabGENERAL.Controls.Add(this.chkENABLE_EDIT_PAYMENT_DATE);
            this.tabGENERAL.Controls.Add(this.chkENABLE_ADJUST_OLD_INVOICE);
            this.tabGENERAL.Controls.Add(this.lblINVOICE);
            this.tabGENERAL.Controls.Add(this.txtDEFAULT_DAY_TO_RUN_REF);
            this.tabGENERAL.Controls.Add(this.lblDAY_TO_RUN_REF);
            this.tabGENERAL.Controls.Add(this.lblREPORT_REF_SUBSIDY);
            this.tabGENERAL.Controls.Add(this.txtMAX_DAY_TO_IGNORE_BILLING);
            this.tabGENERAL.Controls.Add(this.lblMAX_DAY_IGNORE_ISUUE_INVOICE);
            this.tabGENERAL.Controls.Add(this.lblISSUE_INVOICE);
            this.tabGENERAL.Controls.Add(this.txtINVOICE_DUE_DATE);
            this.tabGENERAL.Controls.Add(this.lblINVOICE_DUE_DATE);
            this.tabGENERAL.Controls.Add(this.txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT);
            this.tabGENERAL.Controls.Add(this.lblMAX_DAY_TO_IGNORE_FIXED_AMOUNT);
            this.tabGENERAL.Controls.Add(this.lblFIX_AMOUNT);
            this.tabGENERAL.Controls.Add(this.chkDEPOSIT_IS_IGNORED);
            this.tabGENERAL.Controls.Add(this.chkDEPOSIT_IS_PAID_AUTO);
            this.tabGENERAL.Controls.Add(this.lblDEPOSIT);
            this.tabGENERAL.Controls.Add(this.lblCUT_POWER);
            this.tabGENERAL.Controls.Add(this.txtCUT_OFF_AMOUNT);
            this.tabGENERAL.Controls.Add(this.lblCUT_OFF_AMOUNT);
            this.tabGENERAL.Controls.Add(this.lblDEFAULT_CUT_OFF_DAY);
            this.tabGENERAL.Controls.Add(this.txtDEFAULT_CUT_OFF_DAY);
            resources.ApplyResources(this.tabGENERAL, "tabGENERAL");
            this.tabGENERAL.Name = "tabGENERAL";
            this.tabGENERAL.UseVisualStyleBackColor = true;
            this.tabGENERAL.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.tabGENERAL_MouseDoubleClick);
            // 
            // chkENABLE_EDIT_INVOICE_ADJUSTMENT
            // 
            resources.ApplyResources(this.chkENABLE_EDIT_INVOICE_ADJUSTMENT, "chkENABLE_EDIT_INVOICE_ADJUSTMENT");
            this.chkENABLE_EDIT_INVOICE_ADJUSTMENT.Name = "chkENABLE_EDIT_INVOICE_ADJUSTMENT";
            this.chkENABLE_EDIT_INVOICE_ADJUSTMENT.UseVisualStyleBackColor = true;
            // 
            // chkENABLE_PREPAYMENT_SERVICE
            // 
            resources.ApplyResources(this.chkENABLE_PREPAYMENT_SERVICE, "chkENABLE_PREPAYMENT_SERVICE");
            this.chkENABLE_PREPAYMENT_SERVICE.Name = "chkENABLE_PREPAYMENT_SERVICE";
            this.chkENABLE_PREPAYMENT_SERVICE.UseVisualStyleBackColor = true;
            // 
            // lblPREPAYMENT
            // 
            resources.ApplyResources(this.lblPREPAYMENT, "lblPREPAYMENT");
            this.lblPREPAYMENT.BackColor = System.Drawing.Color.Transparent;
            this.lblPREPAYMENT.Name = "lblPREPAYMENT";
            // 
            // chkENABLE_ADJUST_BASED_PRICE_SUBSIDY
            // 
            resources.ApplyResources(this.chkENABLE_ADJUST_BASED_PRICE_SUBSIDY, "chkENABLE_ADJUST_BASED_PRICE_SUBSIDY");
            this.chkENABLE_ADJUST_BASED_PRICE_SUBSIDY.Name = "chkENABLE_ADJUST_BASED_PRICE_SUBSIDY";
            this.chkENABLE_ADJUST_BASED_PRICE_SUBSIDY.UseVisualStyleBackColor = true;
            // 
            // chkENABLE_EDIT_PAYMENT_DATE
            // 
            resources.ApplyResources(this.chkENABLE_EDIT_PAYMENT_DATE, "chkENABLE_EDIT_PAYMENT_DATE");
            this.chkENABLE_EDIT_PAYMENT_DATE.Name = "chkENABLE_EDIT_PAYMENT_DATE";
            this.chkENABLE_EDIT_PAYMENT_DATE.UseVisualStyleBackColor = true;
            // 
            // chkENABLE_ADJUST_OLD_INVOICE
            // 
            resources.ApplyResources(this.chkENABLE_ADJUST_OLD_INVOICE, "chkENABLE_ADJUST_OLD_INVOICE");
            this.chkENABLE_ADJUST_OLD_INVOICE.Name = "chkENABLE_ADJUST_OLD_INVOICE";
            this.chkENABLE_ADJUST_OLD_INVOICE.UseVisualStyleBackColor = true;
            // 
            // lblINVOICE
            // 
            resources.ApplyResources(this.lblINVOICE, "lblINVOICE");
            this.lblINVOICE.BackColor = System.Drawing.Color.Transparent;
            this.lblINVOICE.Name = "lblINVOICE";
            // 
            // txtDEFAULT_DAY_TO_RUN_REF
            // 
            resources.ApplyResources(this.txtDEFAULT_DAY_TO_RUN_REF, "txtDEFAULT_DAY_TO_RUN_REF");
            this.txtDEFAULT_DAY_TO_RUN_REF.Name = "txtDEFAULT_DAY_TO_RUN_REF";
            this.txtDEFAULT_DAY_TO_RUN_REF.Enter += new System.EventHandler(this.txtREF_DAYS_TO_EXECUTE_Enter);
            // 
            // lblDAY_TO_RUN_REF
            // 
            resources.ApplyResources(this.lblDAY_TO_RUN_REF, "lblDAY_TO_RUN_REF");
            this.lblDAY_TO_RUN_REF.Name = "lblDAY_TO_RUN_REF";
            // 
            // lblREPORT_REF_SUBSIDY
            // 
            resources.ApplyResources(this.lblREPORT_REF_SUBSIDY, "lblREPORT_REF_SUBSIDY");
            this.lblREPORT_REF_SUBSIDY.BackColor = System.Drawing.Color.Transparent;
            this.lblREPORT_REF_SUBSIDY.Name = "lblREPORT_REF_SUBSIDY";
            // 
            // txtMAX_DAY_TO_IGNORE_BILLING
            // 
            resources.ApplyResources(this.txtMAX_DAY_TO_IGNORE_BILLING, "txtMAX_DAY_TO_IGNORE_BILLING");
            this.txtMAX_DAY_TO_IGNORE_BILLING.Name = "txtMAX_DAY_TO_IGNORE_BILLING";
            // 
            // lblMAX_DAY_IGNORE_ISUUE_INVOICE
            // 
            this.lblMAX_DAY_IGNORE_ISUUE_INVOICE.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblMAX_DAY_IGNORE_ISUUE_INVOICE, "lblMAX_DAY_IGNORE_ISUUE_INVOICE");
            this.lblMAX_DAY_IGNORE_ISUUE_INVOICE.Name = "lblMAX_DAY_IGNORE_ISUUE_INVOICE";
            // 
            // lblISSUE_INVOICE
            // 
            resources.ApplyResources(this.lblISSUE_INVOICE, "lblISSUE_INVOICE");
            this.lblISSUE_INVOICE.BackColor = System.Drawing.Color.Transparent;
            this.lblISSUE_INVOICE.Name = "lblISSUE_INVOICE";
            // 
            // txtINVOICE_DUE_DATE
            // 
            resources.ApplyResources(this.txtINVOICE_DUE_DATE, "txtINVOICE_DUE_DATE");
            this.txtINVOICE_DUE_DATE.Name = "txtINVOICE_DUE_DATE";
            this.txtINVOICE_DUE_DATE.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblINVOICE_DUE_DATE
            // 
            resources.ApplyResources(this.lblINVOICE_DUE_DATE, "lblINVOICE_DUE_DATE");
            this.lblINVOICE_DUE_DATE.Name = "lblINVOICE_DUE_DATE";
            // 
            // txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT
            // 
            resources.ApplyResources(this.txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT, "txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT");
            this.txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT.Name = "txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT";
            this.txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblMAX_DAY_TO_IGNORE_FIXED_AMOUNT
            // 
            this.lblMAX_DAY_TO_IGNORE_FIXED_AMOUNT.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblMAX_DAY_TO_IGNORE_FIXED_AMOUNT, "lblMAX_DAY_TO_IGNORE_FIXED_AMOUNT");
            this.lblMAX_DAY_TO_IGNORE_FIXED_AMOUNT.Name = "lblMAX_DAY_TO_IGNORE_FIXED_AMOUNT";
            // 
            // lblFIX_AMOUNT
            // 
            resources.ApplyResources(this.lblFIX_AMOUNT, "lblFIX_AMOUNT");
            this.lblFIX_AMOUNT.BackColor = System.Drawing.Color.Transparent;
            this.lblFIX_AMOUNT.Name = "lblFIX_AMOUNT";
            // 
            // chkDEPOSIT_IS_IGNORED
            // 
            resources.ApplyResources(this.chkDEPOSIT_IS_IGNORED, "chkDEPOSIT_IS_IGNORED");
            this.chkDEPOSIT_IS_IGNORED.Name = "chkDEPOSIT_IS_IGNORED";
            this.chkDEPOSIT_IS_IGNORED.UseVisualStyleBackColor = true;
            this.chkDEPOSIT_IS_IGNORED.CheckedChanged += new System.EventHandler(this.chkDEPOSIT_IS_IGNORED_CheckedChanged);
            // 
            // chkDEPOSIT_IS_PAID_AUTO
            // 
            resources.ApplyResources(this.chkDEPOSIT_IS_PAID_AUTO, "chkDEPOSIT_IS_PAID_AUTO");
            this.chkDEPOSIT_IS_PAID_AUTO.Checked = true;
            this.chkDEPOSIT_IS_PAID_AUTO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDEPOSIT_IS_PAID_AUTO.Name = "chkDEPOSIT_IS_PAID_AUTO";
            this.chkDEPOSIT_IS_PAID_AUTO.UseVisualStyleBackColor = true;
            // 
            // lblDEPOSIT
            // 
            resources.ApplyResources(this.lblDEPOSIT, "lblDEPOSIT");
            this.lblDEPOSIT.BackColor = System.Drawing.Color.Transparent;
            this.lblDEPOSIT.Name = "lblDEPOSIT";
            // 
            // lblCUT_POWER
            // 
            resources.ApplyResources(this.lblCUT_POWER, "lblCUT_POWER");
            this.lblCUT_POWER.BackColor = System.Drawing.Color.Transparent;
            this.lblCUT_POWER.Name = "lblCUT_POWER";
            // 
            // txtCUT_OFF_AMOUNT
            // 
            resources.ApplyResources(this.txtCUT_OFF_AMOUNT, "txtCUT_OFF_AMOUNT");
            this.txtCUT_OFF_AMOUNT.Name = "txtCUT_OFF_AMOUNT";
            this.txtCUT_OFF_AMOUNT.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblCUT_OFF_AMOUNT
            // 
            resources.ApplyResources(this.lblCUT_OFF_AMOUNT, "lblCUT_OFF_AMOUNT");
            this.lblCUT_OFF_AMOUNT.BackColor = System.Drawing.Color.Transparent;
            this.lblCUT_OFF_AMOUNT.Name = "lblCUT_OFF_AMOUNT";
            // 
            // lblDEFAULT_CUT_OFF_DAY
            // 
            resources.ApplyResources(this.lblDEFAULT_CUT_OFF_DAY, "lblDEFAULT_CUT_OFF_DAY");
            this.lblDEFAULT_CUT_OFF_DAY.BackColor = System.Drawing.Color.Transparent;
            this.lblDEFAULT_CUT_OFF_DAY.Name = "lblDEFAULT_CUT_OFF_DAY";
            // 
            // txtDEFAULT_CUT_OFF_DAY
            // 
            resources.ApplyResources(this.txtDEFAULT_CUT_OFF_DAY, "txtDEFAULT_CUT_OFF_DAY");
            this.txtDEFAULT_CUT_OFF_DAY.Name = "txtDEFAULT_CUT_OFF_DAY";
            this.txtDEFAULT_CUT_OFF_DAY.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // tabDevice
            // 
            this.tabDevice.Controls.Add(this.cboMetroBaudRate);
            this.tabDevice.Controls.Add(this.btnTEST_3);
            this.tabDevice.Controls.Add(this.cboMetroCom);
            this.tabDevice.Controls.Add(this.lblDEVICE_BARCODE);
            this.tabDevice.Controls.Add(this.btnTEST_2);
            this.tabDevice.Controls.Add(this.cboIRBoundRate);
            this.tabDevice.Controls.Add(this.btnTEST_1);
            this.tabDevice.Controls.Add(this.cboPrinterCashDrawer);
            this.tabDevice.Controls.Add(this.lblDEVICE_IC_CARD);
            this.tabDevice.Controls.Add(this.cboPrinterReciept);
            this.tabDevice.Controls.Add(this.cboIRComPort);
            this.tabDevice.Controls.Add(this.lblALARM_QTY);
            this.tabDevice.Controls.Add(this.cboPrinterInvoice);
            this.tabDevice.Controls.Add(this.txtAlarmQTY);
            this.tabDevice.Controls.Add(this.lblDEVICE_COLLECT_USAGE);
            this.tabDevice.Controls.Add(this.lblPRINT_CASH_DRAWER);
            this.tabDevice.Controls.Add(this.lblPRINT_RECEIPT);
            this.tabDevice.Controls.Add(this.lblPRINT_INVOICE);
            this.tabDevice.Controls.Add(this.cboPrepaidComPort);
            resources.ApplyResources(this.tabDevice, "tabDevice");
            this.tabDevice.Name = "tabDevice";
            this.tabDevice.UseVisualStyleBackColor = true;
            // 
            // cboMetroBaudRate
            // 
            this.cboMetroBaudRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMetroBaudRate.FormattingEnabled = true;
            resources.ApplyResources(this.cboMetroBaudRate, "cboMetroBaudRate");
            this.cboMetroBaudRate.Name = "cboMetroBaudRate";
            // 
            // btnTEST_3
            // 
            resources.ApplyResources(this.btnTEST_3, "btnTEST_3");
            this.btnTEST_3.Name = "btnTEST_3";
            this.btnTEST_3.UseVisualStyleBackColor = true;
            // 
            // cboMetroCom
            // 
            this.cboMetroCom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMetroCom.FormattingEnabled = true;
            resources.ApplyResources(this.cboMetroCom, "cboMetroCom");
            this.cboMetroCom.Name = "cboMetroCom";
            // 
            // lblDEVICE_BARCODE
            // 
            resources.ApplyResources(this.lblDEVICE_BARCODE, "lblDEVICE_BARCODE");
            this.lblDEVICE_BARCODE.Name = "lblDEVICE_BARCODE";
            // 
            // btnTEST_2
            // 
            resources.ApplyResources(this.btnTEST_2, "btnTEST_2");
            this.btnTEST_2.Name = "btnTEST_2";
            this.btnTEST_2.UseVisualStyleBackColor = true;
            this.btnTEST_2.Click += new System.EventHandler(this.btnTestPrepaid_Click);
            // 
            // btnTEST_1
            // 
            resources.ApplyResources(this.btnTEST_1, "btnTEST_1");
            this.btnTEST_1.Name = "btnTEST_1";
            this.btnTEST_1.UseVisualStyleBackColor = true;
            this.btnTEST_1.Click += new System.EventHandler(this.btnTestIR_Click);
            // 
            // lblDEVICE_IC_CARD
            // 
            resources.ApplyResources(this.lblDEVICE_IC_CARD, "lblDEVICE_IC_CARD");
            this.lblDEVICE_IC_CARD.BackColor = System.Drawing.Color.Transparent;
            this.lblDEVICE_IC_CARD.Name = "lblDEVICE_IC_CARD";
            // 
            // lblALARM_QTY
            // 
            resources.ApplyResources(this.lblALARM_QTY, "lblALARM_QTY");
            this.lblALARM_QTY.BackColor = System.Drawing.Color.Transparent;
            this.lblALARM_QTY.Name = "lblALARM_QTY";
            // 
            // lblDEVICE_COLLECT_USAGE
            // 
            resources.ApplyResources(this.lblDEVICE_COLLECT_USAGE, "lblDEVICE_COLLECT_USAGE");
            this.lblDEVICE_COLLECT_USAGE.Name = "lblDEVICE_COLLECT_USAGE";
            // 
            // lblPRINT_CASH_DRAWER
            // 
            resources.ApplyResources(this.lblPRINT_CASH_DRAWER, "lblPRINT_CASH_DRAWER");
            this.lblPRINT_CASH_DRAWER.Name = "lblPRINT_CASH_DRAWER";
            // 
            // lblPRINT_RECEIPT
            // 
            resources.ApplyResources(this.lblPRINT_RECEIPT, "lblPRINT_RECEIPT");
            this.lblPRINT_RECEIPT.Name = "lblPRINT_RECEIPT";
            // 
            // lblPRINT_INVOICE
            // 
            resources.ApplyResources(this.lblPRINT_INVOICE, "lblPRINT_INVOICE");
            this.lblPRINT_INVOICE.Name = "lblPRINT_INVOICE";
            // 
            // tabSetting
            // 
            this.tabSetting.Controls.Add(this.chkBACKUP_BEFORE_REVERS_BILL);
            this.tabSetting.Controls.Add(this.chkBACKUP_BEFORE_RUN_BILL);
            this.tabSetting.Controls.Add(this.lblBACKUP);
            this.tabSetting.Controls.Add(this.txtTOTAL_CUSTOMER_REVERS);
            this.tabSetting.Controls.Add(this.label2);
            this.tabSetting.Controls.Add(this.lblTOTAL_INVOICE_REVERS);
            this.tabSetting.Controls.Add(this.lblREVERSE_BILL);
            this.tabSetting.Controls.Add(this.cboPrepaidSequence);
            this.tabSetting.Controls.Add(this.label1);
            this.tabSetting.Controls.Add(this.lblPREPAID_SEQUENCE);
            this.tabSetting.Controls.Add(this.txtExportNumber);
            this.tabSetting.Controls.Add(this.label9);
            this.tabSetting.Controls.Add(this.lblExportNumber);
            this.tabSetting.Controls.Add(this.lblE_Fillings);
            this.tabSetting.Controls.Add(this.cboServiceSequence);
            this.tabSetting.Controls.Add(this.cboInvoiceSequence);
            this.tabSetting.Controls.Add(this.label8);
            this.tabSetting.Controls.Add(this.label7);
            this.tabSetting.Controls.Add(this.lblINVOICE_SERVICE);
            this.tabSetting.Controls.Add(this.lblINVOICE_BILL);
            this.tabSetting.Controls.Add(this.lblSEQUENCE);
            resources.ApplyResources(this.tabSetting, "tabSetting");
            this.tabSetting.Name = "tabSetting";
            this.tabSetting.UseVisualStyleBackColor = true;
            // 
            // chkBACKUP_BEFORE_REVERS_BILL
            // 
            resources.ApplyResources(this.chkBACKUP_BEFORE_REVERS_BILL, "chkBACKUP_BEFORE_REVERS_BILL");
            this.chkBACKUP_BEFORE_REVERS_BILL.Name = "chkBACKUP_BEFORE_REVERS_BILL";
            this.chkBACKUP_BEFORE_REVERS_BILL.UseVisualStyleBackColor = true;
            // 
            // chkBACKUP_BEFORE_RUN_BILL
            // 
            resources.ApplyResources(this.chkBACKUP_BEFORE_RUN_BILL, "chkBACKUP_BEFORE_RUN_BILL");
            this.chkBACKUP_BEFORE_RUN_BILL.Name = "chkBACKUP_BEFORE_RUN_BILL";
            this.chkBACKUP_BEFORE_RUN_BILL.UseVisualStyleBackColor = true;
            // 
            // lblBACKUP
            // 
            resources.ApplyResources(this.lblBACKUP, "lblBACKUP");
            this.lblBACKUP.BackColor = System.Drawing.Color.Transparent;
            this.lblBACKUP.Name = "lblBACKUP";
            // 
            // txtTOTAL_CUSTOMER_REVERS
            // 
            resources.ApplyResources(this.txtTOTAL_CUSTOMER_REVERS, "txtTOTAL_CUSTOMER_REVERS");
            this.txtTOTAL_CUSTOMER_REVERS.Name = "txtTOTAL_CUSTOMER_REVERS";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // lblTOTAL_INVOICE_REVERS
            // 
            this.lblTOTAL_INVOICE_REVERS.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblTOTAL_INVOICE_REVERS, "lblTOTAL_INVOICE_REVERS");
            this.lblTOTAL_INVOICE_REVERS.Name = "lblTOTAL_INVOICE_REVERS";
            // 
            // lblREVERSE_BILL
            // 
            resources.ApplyResources(this.lblREVERSE_BILL, "lblREVERSE_BILL");
            this.lblREVERSE_BILL.BackColor = System.Drawing.Color.Transparent;
            this.lblREVERSE_BILL.Name = "lblREVERSE_BILL";
            // 
            // cboPrepaidSequence
            // 
            this.cboPrepaidSequence.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrepaidSequence.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrepaidSequence, "cboPrepaidSequence");
            this.cboPrepaidSequence.Name = "cboPrepaidSequence";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // lblPREPAID_SEQUENCE
            // 
            this.lblPREPAID_SEQUENCE.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblPREPAID_SEQUENCE, "lblPREPAID_SEQUENCE");
            this.lblPREPAID_SEQUENCE.Name = "lblPREPAID_SEQUENCE";
            // 
            // txtExportNumber
            // 
            resources.ApplyResources(this.txtExportNumber, "txtExportNumber");
            this.txtExportNumber.Name = "txtExportNumber";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // lblExportNumber
            // 
            this.lblExportNumber.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblExportNumber, "lblExportNumber");
            this.lblExportNumber.Name = "lblExportNumber";
            // 
            // lblE_Fillings
            // 
            resources.ApplyResources(this.lblE_Fillings, "lblE_Fillings");
            this.lblE_Fillings.BackColor = System.Drawing.Color.Transparent;
            this.lblE_Fillings.Name = "lblE_Fillings";
            // 
            // cboServiceSequence
            // 
            this.cboServiceSequence.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboServiceSequence.FormattingEnabled = true;
            resources.ApplyResources(this.cboServiceSequence, "cboServiceSequence");
            this.cboServiceSequence.Name = "cboServiceSequence";
            // 
            // cboInvoiceSequence
            // 
            this.cboInvoiceSequence.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInvoiceSequence.FormattingEnabled = true;
            resources.ApplyResources(this.cboInvoiceSequence, "cboInvoiceSequence");
            this.cboInvoiceSequence.Name = "cboInvoiceSequence";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // lblINVOICE_SERVICE
            // 
            this.lblINVOICE_SERVICE.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblINVOICE_SERVICE, "lblINVOICE_SERVICE");
            this.lblINVOICE_SERVICE.Name = "lblINVOICE_SERVICE";
            // 
            // lblINVOICE_BILL
            // 
            this.lblINVOICE_BILL.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblINVOICE_BILL, "lblINVOICE_BILL");
            this.lblINVOICE_BILL.Name = "lblINVOICE_BILL";
            // 
            // lblSEQUENCE
            // 
            resources.ApplyResources(this.lblSEQUENCE, "lblSEQUENCE");
            this.lblSEQUENCE.BackColor = System.Drawing.Color.Transparent;
            this.lblSEQUENCE.Name = "lblSEQUENCE";
            // 
            // DialogUtility
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogUtility";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tab.ResumeLayout(false);
            this.tabGENERAL.ResumeLayout(false);
            this.tabGENERAL.PerformLayout();
            this.tabDevice.ResumeLayout(false);
            this.tabDevice.PerformLayout();
            this.tabSetting.ResumeLayout(false);
            this.tabSetting.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ComboBox cboPrepaidComPort;
        private ComboBox cboIRBoundRate;
        private ComboBox cboIRComPort;
        private TextBox txtAlarmQTY;
        private Label label12;
        private Label label13;
        private TextBox textBox1;
        private Label label23;
        private Label label29;
        private CheckBox checkBox3;
        private CheckBox checkBox4;
        private Label label55;
        private Label label56;
        private Label label57;
        private TextBox textBox2;
        private Label label58;
        private Label label59;
        private Label label60;
        private ComboBox cboPrinterCashDrawer;
        private ComboBox cboPrinterReciept;
        private ComboBox cboPrinterInvoice;
        private TabControl tab;
        private TabPage tabGENERAL;
        private TabPage tabDevice;
        private TextBox txtINVOICE_DUE_DATE;
        private Label lblINVOICE_DUE_DATE;
        private TextBox txtMAX_DAY_TO_IGNORE_FIXED_AMOUNT;
        private Label lblMAX_DAY_TO_IGNORE_FIXED_AMOUNT;
        private Label lblFIX_AMOUNT;
        private CheckBox chkDEPOSIT_IS_IGNORED;
        private CheckBox chkDEPOSIT_IS_PAID_AUTO;
        private Label lblDEPOSIT;
        private Label lblCUT_POWER;
        private TextBox txtCUT_OFF_AMOUNT;
        private Label lblCUT_OFF_AMOUNT;
        private Label lblDEFAULT_CUT_OFF_DAY;
        private TextBox txtDEFAULT_CUT_OFF_DAY;
        private Label lblPRINT_CASH_DRAWER;
        private Label lblPRINT_RECEIPT;
        private Label lblPRINT_INVOICE;
        private Label lblALARM_QTY;
        private Label lblDEVICE_COLLECT_USAGE;
        private Label lblDEVICE_IC_CARD;
        private Button btnTEST_1;
        private Button btnTEST_2;
        private ComboBox cboMetroBaudRate;
        private ComboBox cboMetroCom;
        private Label lblDEVICE_BARCODE;
        private Button btnTEST_3;
        private TextBox txtMAX_DAY_TO_IGNORE_BILLING;
        private Label lblMAX_DAY_IGNORE_ISUUE_INVOICE;
        private Label lblISSUE_INVOICE;
        private Label lblREPORT_REF_SUBSIDY;
        private TextBox txtDEFAULT_DAY_TO_RUN_REF;
        private Label lblDAY_TO_RUN_REF;
        private CheckBox chkENABLE_ADJUST_OLD_INVOICE;
        private Label lblINVOICE;
        private CheckBox chkENABLE_EDIT_PAYMENT_DATE;
        private CheckBox chkENABLE_ADJUST_BASED_PRICE_SUBSIDY;
        private CheckBox chkENABLE_PREPAYMENT_SERVICE;
        private Label lblPREPAYMENT;
        private CheckBox chkENABLE_EDIT_INVOICE_ADJUSTMENT;
        private TabPage tabSetting;
        private ComboBox cboServiceSequence;
        private ComboBox cboInvoiceSequence;
        private Label label8;
        private Label label7;
        private Label lblINVOICE_SERVICE;
        private Label lblINVOICE_BILL;
        private Label lblSEQUENCE;
        private TextBox txtExportNumber;
        private Label label9;
        private Label lblExportNumber;
        private Label lblE_Fillings;
        private ComboBox cboPrepaidSequence;
        private Label label1;
        private Label lblPREPAID_SEQUENCE;
        private Label lblREVERSE_BILL;
        private TextBox txtTOTAL_CUSTOMER_REVERS;
        private Label label2;
        private Label lblTOTAL_INVOICE_REVERS;
        private Label lblBACKUP;
        private CheckBox chkBACKUP_BEFORE_REVERS_BILL;
        private CheckBox chkBACKUP_BEFORE_RUN_BILL;
    }
}