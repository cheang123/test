﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PagePrintInvoice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PagePrintInvoice));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.cboBiller = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboAreaCode = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.cboInvoiceStatus = new System.Windows.Forms.ComboBox();
            this.lblCUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.Label();
            this.cboConnectionType = new System.Windows.Forms.ComboBox();
            this.lblPRICE = new System.Windows.Forms.Label();
            this.cboPrice = new System.Windows.Forms.ComboBox();
            this.lblCUSTOMER_TYPE = new System.Windows.Forms.Label();
            this.cboCustomerType = new System.Windows.Forms.ComboBox();
            this.lblMonth = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblBILLER = new System.Windows.Forms.Label();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkIncludeZeroUsage = new System.Windows.Forms.CheckBox();
            this.txtEndBoxNo = new SoftTech.Component.ExTextbox();
            this.txtStartBoxNo = new SoftTech.Component.ExTextbox();
            this.label12 = new System.Windows.Forms.Label();
            this.rdoPRINT_ALL = new System.Windows.Forms.RadioButton();
            this.rdoPRINT_BY_BOX = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnEXPORT_INVOICE = new SoftTech.Component.ExButton();
            this.btnIMPORT_INVOICE = new SoftTech.Component.ExButton();
            this.dgvSummary = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POWER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_BALANCE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblPRINT_INFORMATION = new System.Windows.Forms.Label();
            this.lblFORM_INVOICE = new System.Windows.Forms.Label();
            this.cboReport = new System.Windows.Forms.ComboBox();
            this.cboPrinterInvoice = new System.Windows.Forms.ComboBox();
            this.lblPRINTER = new System.Windows.Forms.Label();
            this.lblSUMMARY_INFORMATION = new System.Windows.Forms.Label();
            this.lblPRINT = new SoftTech.Component.ExButton();
            this.lblVIEW = new SoftTech.Component.ExButton();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboAreaCode.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.SelectedIndexChanged += new System.EventHandler(this.cboBillingCycle_SelectedIndexChanged);
            // 
            // dtpMonth
            // 
            resources.ApplyResources(this.dtpMonth, "dtpMonth");
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.ValueChanged += new System.EventHandler(this.dtpMonth_ValueChanged);
            // 
            // cboBiller
            // 
            this.cboBiller.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBiller.FormattingEnabled = true;
            resources.ApplyResources(this.cboBiller, "cboBiller");
            this.cboBiller.Name = "cboBiller";
            this.cboBiller.SelectedIndexChanged += new System.EventHandler(this.cboBiller_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.cboAreaCode);
            this.groupBox1.Controls.Add(this.lblSTATUS);
            this.groupBox1.Controls.Add(this.cboInvoiceStatus);
            this.groupBox1.Controls.Add(this.lblCUSTOMER_CONNECTION_TYPE);
            this.groupBox1.Controls.Add(this.cboConnectionType);
            this.groupBox1.Controls.Add(this.lblPRICE);
            this.groupBox1.Controls.Add(this.cboPrice);
            this.groupBox1.Controls.Add(this.lblCUSTOMER_TYPE);
            this.groupBox1.Controls.Add(this.cboCustomerType);
            this.groupBox1.Controls.Add(this.lblMonth);
            this.groupBox1.Controls.Add(this.lblAREA);
            this.groupBox1.Controls.Add(this.dtpMonth);
            this.groupBox1.Controls.Add(this.lblBILLER);
            this.groupBox1.Controls.Add(this.lblCYCLE_NAME);
            this.groupBox1.Controls.Add(this.cboBillingCycle);
            this.groupBox1.Controls.Add(this.cboBiller);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // cboAreaCode
            // 
            resources.ApplyResources(this.cboAreaCode, "cboAreaCode");
            this.cboAreaCode.Name = "cboAreaCode";
            this.cboAreaCode.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboAreaCode.Properties.Appearance.Font")));
            this.cboAreaCode.Properties.Appearance.Options.UseFont = true;
            this.cboAreaCode.Properties.AppearanceDisabled.Font = ((System.Drawing.Font)(resources.GetObject("cboAreaCode.Properties.AppearanceDisabled.Font")));
            this.cboAreaCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.cboAreaCode.Properties.AppearanceDropDown.Font = ((System.Drawing.Font)(resources.GetObject("cboAreaCode.Properties.AppearanceDropDown.Font")));
            this.cboAreaCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboAreaCode.Properties.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("cboAreaCode.Properties.AppearanceFocused.Font")));
            this.cboAreaCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.cboAreaCode.Properties.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("cboAreaCode.Properties.AppearanceReadOnly.Font")));
            this.cboAreaCode.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.cboAreaCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboAreaCode.Properties.Buttons"))))});
            this.cboAreaCode.Properties.DropDownRows = 20;
            this.cboAreaCode.Properties.PopupFormMinSize = new System.Drawing.Size(200, 0);
            this.cboAreaCode.Properties.PopupFormSize = new System.Drawing.Size(400, 0);
            this.cboAreaCode.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // cboInvoiceStatus
            // 
            this.cboInvoiceStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInvoiceStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboInvoiceStatus, "cboInvoiceStatus");
            this.cboInvoiceStatus.Name = "cboInvoiceStatus";
            this.cboInvoiceStatus.SelectedIndexChanged += new System.EventHandler(this.cboInvoiceStatus_SelectedIndexChanged);
            // 
            // lblCUSTOMER_CONNECTION_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CONNECTION_TYPE, "lblCUSTOMER_CONNECTION_TYPE");
            this.lblCUSTOMER_CONNECTION_TYPE.Name = "lblCUSTOMER_CONNECTION_TYPE";
            // 
            // cboConnectionType
            // 
            this.cboConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConnectionType.DropDownWidth = 300;
            this.cboConnectionType.FormattingEnabled = true;
            resources.ApplyResources(this.cboConnectionType, "cboConnectionType");
            this.cboConnectionType.Name = "cboConnectionType";
            this.cboConnectionType.SelectedIndexChanged += new System.EventHandler(this.cboConnectionType_SelectedIndexChanged);
            // 
            // lblPRICE
            // 
            resources.ApplyResources(this.lblPRICE, "lblPRICE");
            this.lblPRICE.Name = "lblPRICE";
            // 
            // cboPrice
            // 
            this.cboPrice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrice.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrice, "cboPrice");
            this.cboPrice.Name = "cboPrice";
            this.cboPrice.SelectedIndexChanged += new System.EventHandler(this.cboPrice_SelectedIndexChanged);
            // 
            // lblCUSTOMER_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_TYPE, "lblCUSTOMER_TYPE");
            this.lblCUSTOMER_TYPE.Name = "lblCUSTOMER_TYPE";
            // 
            // cboCustomerType
            // 
            this.cboCustomerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerType.DropDownWidth = 300;
            this.cboCustomerType.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerType, "cboCustomerType");
            this.cboCustomerType.Name = "cboCustomerType";
            this.cboCustomerType.SelectedIndexChanged += new System.EventHandler(this.cboCustomerType_SelectedIndexChanged);
            // 
            // lblMonth
            // 
            resources.ApplyResources(this.lblMonth, "lblMonth");
            this.lblMonth.Name = "lblMonth";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // lblBILLER
            // 
            resources.ApplyResources(this.lblBILLER, "lblBILLER");
            this.lblBILLER.Name = "lblBILLER";
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Controls.Add(this.chkIncludeZeroUsage);
            this.groupBox2.Controls.Add(this.txtEndBoxNo);
            this.groupBox2.Controls.Add(this.txtStartBoxNo);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.rdoPRINT_ALL);
            this.groupBox2.Controls.Add(this.rdoPRINT_BY_BOX);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // chkIncludeZeroUsage
            // 
            resources.ApplyResources(this.chkIncludeZeroUsage, "chkIncludeZeroUsage");
            this.chkIncludeZeroUsage.Name = "chkIncludeZeroUsage";
            this.chkIncludeZeroUsage.UseVisualStyleBackColor = true;
            this.chkIncludeZeroUsage.CheckedChanged += new System.EventHandler(this.chkIncludeZeroUsage_CheckedChanged);
            // 
            // txtEndBoxNo
            // 
            this.txtEndBoxNo.BackColor = System.Drawing.Color.White;
            this.txtEndBoxNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtEndBoxNo, "txtEndBoxNo");
            this.txtEndBoxNo.Name = "txtEndBoxNo";
            this.txtEndBoxNo.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtEndBoxNo.AdvanceSearch += new System.EventHandler(this.AdvanceSearch);
            this.txtEndBoxNo.CancelAdvanceSearch += new System.EventHandler(this.txtStartBoxNo_CancelAdvanceSearch);
            // 
            // txtStartBoxNo
            // 
            this.txtStartBoxNo.BackColor = System.Drawing.Color.White;
            this.txtStartBoxNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtStartBoxNo, "txtStartBoxNo");
            this.txtStartBoxNo.Name = "txtStartBoxNo";
            this.txtStartBoxNo.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtStartBoxNo.AdvanceSearch += new System.EventHandler(this.AdvanceSearch);
            this.txtStartBoxNo.CancelAdvanceSearch += new System.EventHandler(this.txtStartBoxNo_CancelAdvanceSearch);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // rdoPRINT_ALL
            // 
            resources.ApplyResources(this.rdoPRINT_ALL, "rdoPRINT_ALL");
            this.rdoPRINT_ALL.Checked = true;
            this.rdoPRINT_ALL.Name = "rdoPRINT_ALL";
            this.rdoPRINT_ALL.TabStop = true;
            this.rdoPRINT_ALL.UseVisualStyleBackColor = true;
            // 
            // rdoPRINT_BY_BOX
            // 
            resources.ApplyResources(this.rdoPRINT_BY_BOX, "rdoPRINT_BY_BOX");
            this.rdoPRINT_BY_BOX.Name = "rdoPRINT_BY_BOX";
            this.rdoPRINT_BY_BOX.UseVisualStyleBackColor = true;
            this.rdoPRINT_BY_BOX.CheckedChanged += new System.EventHandler(this.rdoByBoxNo_CheckedChanged);
            // 
            // groupBox3
            // 
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Controls.Add(this.btnEXPORT_INVOICE);
            this.groupBox3.Controls.Add(this.btnIMPORT_INVOICE);
            this.groupBox3.Controls.Add(this.dgvSummary);
            this.groupBox3.Controls.Add(this.lblPRINT_INFORMATION);
            this.groupBox3.Controls.Add(this.lblFORM_INVOICE);
            this.groupBox3.Controls.Add(this.cboReport);
            this.groupBox3.Controls.Add(this.cboPrinterInvoice);
            this.groupBox3.Controls.Add(this.lblPRINTER);
            this.groupBox3.Controls.Add(this.lblSUMMARY_INFORMATION);
            this.groupBox3.Controls.Add(this.lblPRINT);
            this.groupBox3.Controls.Add(this.lblVIEW);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // btnEXPORT_INVOICE
            // 
            resources.ApplyResources(this.btnEXPORT_INVOICE, "btnEXPORT_INVOICE");
            this.btnEXPORT_INVOICE.Name = "btnEXPORT_INVOICE";
            this.btnEXPORT_INVOICE.UseVisualStyleBackColor = true;
            this.btnEXPORT_INVOICE.Click += new System.EventHandler(this.btnEXPORT_INVOICE_Click);
            // 
            // btnIMPORT_INVOICE
            // 
            resources.ApplyResources(this.btnIMPORT_INVOICE, "btnIMPORT_INVOICE");
            this.btnIMPORT_INVOICE.Name = "btnIMPORT_INVOICE";
            this.btnIMPORT_INVOICE.UseVisualStyleBackColor = true;
            this.btnIMPORT_INVOICE.Click += new System.EventHandler(this.btnIMPORT_INVOICE_Click);
            // 
            // dgvSummary
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvSummary.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSummary.BackgroundColor = System.Drawing.Color.White;
            this.dgvSummary.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvSummary.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.INVOICE,
            this.POWER,
            this.TOTAL_BALANCE});
            this.dgvSummary.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvSummary, "dgvSummary");
            this.dgvSummary.Name = "dgvSummary";
            this.dgvSummary.RowHeadersVisible = false;
            this.dgvSummary.RowTemplate.Height = 25;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "CURRENCY_SING";
            this.dataGridViewTextBoxColumn9.FillWeight = 35F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn9, "dataGridViewTextBoxColumn9");
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // INVOICE
            // 
            this.INVOICE.DataPropertyName = "TOTAL_INVOICE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "0";
            this.INVOICE.DefaultCellStyle = dataGridViewCellStyle2;
            this.INVOICE.FillWeight = 89F;
            resources.ApplyResources(this.INVOICE, "INVOICE");
            this.INVOICE.Name = "INVOICE";
            // 
            // POWER
            // 
            this.POWER.DataPropertyName = "TOTAL_USAGE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "n0";
            this.POWER.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.POWER, "POWER");
            this.POWER.Name = "POWER";
            // 
            // TOTAL_BALANCE
            // 
            this.TOTAL_BALANCE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TOTAL_BALANCE.DataPropertyName = "TOTAL_BALANCE";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,##0.####";
            this.TOTAL_BALANCE.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.TOTAL_BALANCE, "TOTAL_BALANCE");
            this.TOTAL_BALANCE.Name = "TOTAL_BALANCE";
            // 
            // lblPRINT_INFORMATION
            // 
            resources.ApplyResources(this.lblPRINT_INFORMATION, "lblPRINT_INFORMATION");
            this.lblPRINT_INFORMATION.Name = "lblPRINT_INFORMATION";
            // 
            // lblFORM_INVOICE
            // 
            resources.ApplyResources(this.lblFORM_INVOICE, "lblFORM_INVOICE");
            this.lblFORM_INVOICE.Name = "lblFORM_INVOICE";
            // 
            // cboReport
            // 
            this.cboReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReport.FormattingEnabled = true;
            resources.ApplyResources(this.cboReport, "cboReport");
            this.cboReport.Name = "cboReport";
            this.cboReport.SelectedIndexChanged += new System.EventHandler(this.cboReport_SelectedIndexChanged);
            // 
            // cboPrinterInvoice
            // 
            this.cboPrinterInvoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrinterInvoice.DropDownWidth = 250;
            this.cboPrinterInvoice.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrinterInvoice, "cboPrinterInvoice");
            this.cboPrinterInvoice.Name = "cboPrinterInvoice";
            this.cboPrinterInvoice.SelectedIndexChanged += new System.EventHandler(this.cboPrinterInvoice_SelectedIndexChanged);
            // 
            // lblPRINTER
            // 
            resources.ApplyResources(this.lblPRINTER, "lblPRINTER");
            this.lblPRINTER.Name = "lblPRINTER";
            // 
            // lblSUMMARY_INFORMATION
            // 
            resources.ApplyResources(this.lblSUMMARY_INFORMATION, "lblSUMMARY_INFORMATION");
            this.lblSUMMARY_INFORMATION.Name = "lblSUMMARY_INFORMATION";
            // 
            // lblPRINT
            // 
            resources.ApplyResources(this.lblPRINT, "lblPRINT");
            this.lblPRINT.Name = "lblPRINT";
            this.lblPRINT.UseVisualStyleBackColor = true;
            this.lblPRINT.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // lblVIEW
            // 
            resources.ApplyResources(this.lblVIEW, "lblVIEW");
            this.lblVIEW.Name = "lblVIEW";
            this.lblVIEW.UseVisualStyleBackColor = true;
            this.lblVIEW.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // timer
            // 
            this.timer.Interval = 1;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "INVOICE_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "INVOICE_DATE";
            dataGridViewCellStyle5.Format = "dd-MM-yyyy";
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn2.FillWeight = 12.69035F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CUSTOMER_NAME";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Format = "0";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn3.FillWeight = 449.2386F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "METER_CODE";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "n0";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn4.FillWeight = 1F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "TOTAL_BALANCE";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "#";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn5.FillWeight = 12.69035F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // PagePrintInvoice
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Name = "PagePrintInvoice";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboAreaCode.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSummary)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private ComboBox cboBiller;
        private ExButton lblPRINT;
        private ExButton lblVIEW;
        private DateTimePicker dtpMonth;
        private ComboBox cboBillingCycle;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private GroupBox groupBox1;
        private Label lblBILLER;
        private Label lblCYCLE_NAME;
        private GroupBox groupBox2;
        private Label lblMonth;
        private Label lblAREA;
        private Label label12;
        private RadioButton rdoPRINT_ALL;
        private RadioButton rdoPRINT_BY_BOX;
        private GroupBox groupBox3;
        private Label lblSUMMARY_INFORMATION;
        private ExTextbox txtEndBoxNo;
        private ExTextbox txtStartBoxNo;
        private Timer timer;
        private Label lblPRICE;
        private ComboBox cboPrice;
        private Label lblCUSTOMER_TYPE;
        private ComboBox cboCustomerType;
        private Label lblPRINTER;
        private ComboBox cboPrinterInvoice;
        private ComboBox cboReport;
        private Label lblPRINT_INFORMATION;
        private Label lblFORM_INVOICE;
        private DataGridView dgvSummary;
        private Label lblCUSTOMER_CONNECTION_TYPE;
        private ComboBox cboConnectionType;
        private CheckBox chkIncludeZeroUsage;
        private Label lblSTATUS;
        private ComboBox cboInvoiceStatus;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private DataGridViewTextBoxColumn INVOICE;
        private DataGridViewTextBoxColumn POWER;
        private DataGridViewTextBoxColumn TOTAL_BALANCE;
        private ExButton btnEXPORT_INVOICE;
        private ExButton btnIMPORT_INVOICE;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cboAreaCode;
    }
}
