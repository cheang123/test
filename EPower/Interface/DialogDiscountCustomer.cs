﻿using System;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogDiscountCustomer : ExDialog
    {
        TBL_DISCOUNT _objDiscount = new TBL_DISCOUNT();
        #region Constructor
        public DialogDiscountCustomer(TBL_DISCOUNT objDiscount)
        {
            InitializeComponent();
            this._objDiscount = objDiscount;
            this.label1.Text = this._objDiscount.DISCOUNT_NAME;
            DataTable q=( from d in DBDataContext.Db.TBL_CUSTOMER_DISCOUNTs
                                  join c in DBDataContext.Db.TBL_CUSTOMERs on d.CUSTOMER_ID equals c.CUSTOMER_ID
                                  where d.DISCOUNT_ID == this._objDiscount.DISCOUNT_ID && d.IS_ACTIVE
                                  select new
                                  {
                                      CUSTOMER_DISCOUNT_ID=d.CUSTOMER_DISCOUNT_ID, 
                                      CUSTOMER_ID = c.CUSTOMER_ID,
                                      CUSTOMER_CODE = c.CUSTOMER_CODE, 
                                      CUSTOMER_NAME=c.LAST_NAME_KH+" " + c.FIRST_NAME_KH + "  "+ c.LAST_NAME+" "+c.FIRST_NAME,
                                      DB=true
                                  })._ToDataTable();
            foreach (DataRow row in q.Rows)
            {
                this.dgv.Rows.Add(row.ItemArray);
            }

        }        
        #endregion

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        } 

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e)
        { 
            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    TBL_CHANGE_LOG log = DBDataContext.Db.Update(this._objDiscount, this._objDiscount);
                    foreach (DataGridViewRow row in this.dgv.Rows)
                    { 
                        TBL_CUSTOMER_DISCOUNT obj = new TBL_CUSTOMER_DISCOUNT()
                        {
                            CUSTOMER_DISCOUNT_ID = (int)row.Cells["CUSTOMER_DISCOUNT_ID"].Value,
                            CUSTOMER_ID = (int)row.Cells["CUSTOMER_ID"].Value,
                            DISCOUNT_ID = this._objDiscount.DISCOUNT_ID
                        }; 

                        if (row.Visible)
                        {  
                            if (!(bool)row.Cells["DB"].Value)
                            {
                                // because the customer discount will be allow only once.
                                // so if it's use with other discount than remove it automatically.
                                TBL_CUSTOMER_DISCOUNT objOtherCustomerDiscount =(
                                       from c in DBDataContext.Db.TBL_CUSTOMER_DISCOUNTs
                                       join d in DBDataContext.Db.TBL_DISCOUNTs on c.DISCOUNT_ID equals d.DISCOUNT_ID
                                       where c.CUSTOMER_ID == obj.CUSTOMER_ID
                                       && c.DISCOUNT_ID != _objDiscount.DISCOUNT_ID
                                       && d.DISCOUNT_TYPE_ID == _objDiscount.DISCOUNT_TYPE_ID
                                       && c.IS_ACTIVE
                                       select c
                                ).FirstOrDefault();

                                if (objOtherCustomerDiscount!=null)
                                { 
                                    DBDataContext.Db.DeleteChild(objOtherCustomerDiscount, this._objDiscount,ref log);
                                } 

                                // insert the new customer discount.
                                DBDataContext.Db.InsertChild(obj, this._objDiscount,ref  log);
                            }
                        }
                        else
                        {
                            if ((bool)row.Cells["DB"].Value)
                            { 
                                // delete customer discount.
                                DBDataContext.Db.DeleteChild(obj, this._objDiscount,ref log);
                            } 
                        }
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }                                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            DialogCustomerSearch diag = new DialogCustomerSearch("", DialogCustomerSearch.PowerType.Postpaid);
            if (diag.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            TBL_CUSTOMER objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == diag.CustomerID);

            // check whether customer are OK.
            if (objCustomer == null)
            {
                return;
            }

            // check existing.
            bool exits = false;
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                if (row.Visible && diag.CustomerID == (int)row.Cells["CUSTOMER_ID"].Value)
                {
                    exits = true;
                    break;
                }
            }

            // if customer that choose are alreay in dgv
            // then tell user and return.
            if (exits)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_ALREADY_DISCOUNT);
                return;
           }
            
           // if customer have in other discount table
           // ask user to delete from that table
           TBL_DISCOUNT objOtherDiscount = (
                from c in DBDataContext.Db.TBL_CUSTOMER_DISCOUNTs 
                join d in DBDataContext.Db.TBL_DISCOUNTs on c.DISCOUNT_ID equals d.DISCOUNT_ID
                where c.CUSTOMER_ID == diag.CustomerID 
                && d.DISCOUNT_ID != _objDiscount.DISCOUNT_ID
                && d.DISCOUNT_TYPE_ID ==_objDiscount.DISCOUNT_TYPE_ID
                && c.IS_ACTIVE
                select d
            ).FirstOrDefault(); 
            if (objOtherDiscount!=null)
            {
                if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_ADD_NEW_CUSTOMER_DISCOUNT, objOtherDiscount.DISCOUNT_NAME),Resources.DISCOUNT  ) == DialogResult.No)
                {
                    return;
                } 
            }

            // add to dgv for later save.
            this.dgv.Rows.Add(0, objCustomer.CUSTOMER_ID, objCustomer.CUSTOMER_CODE, objCustomer.LAST_NAME_KH + " " + objCustomer.FIRST_NAME_KH + " " + objCustomer.LAST_NAME + " " + objCustomer.FIRST_NAME, false);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0)
            {
                return;
            }
            if (MsgBox.ShowQuestion(Resources.MSQ_REMOVE_CUSTOMER_DISCOUNT, Resources.DISCOUNT) == DialogResult.Yes)
            {
                DataGridViewRow row = this.dgv.SelectedRows[0];
                if ((bool)row.Cells["DB"].Value)
                {
                    row.Visible = false;
                }
                else
                {
                    this.dgv.Rows.Remove(row);
                }
            }
        } 
    }
}