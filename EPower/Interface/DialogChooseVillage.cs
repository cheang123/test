﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogChooseVillage : ExDialog
    {
        bool _loading = true;
        public DialogChooseVillage()
        {
            
            _loading = true;
            InitializeComponent();
            try
            {
                UIHelper.SetDataSourceToComboBox(cboProvince, DBDataContext.Db.TLKP_PROVINCEs);
                this.loadVillage(DBDataContext.Db.TBL_COMPANies.FirstOrDefault().VILLAGE_CODE);
                _loading = false;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void cboProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strProvinceCode = cboProvince.SelectedValue.ToString(); 
            UIHelper.SetDataSourceToComboBox(cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(d => d.PROVINCE_CODE == strProvinceCode));
        }

        private void cboDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strDisCode = cboDistrict.SelectedValue.ToString(); 
            UIHelper.SetDataSourceToComboBox(cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(c => c.DISTRICT_CODE == strDisCode));
        }

        private void cboCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strComCode = cboCommune.SelectedValue.ToString(); 
            UIHelper.SetDataSourceToComboBox(cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(v => v.COMMUNE_CODE == strComCode));
        }

        private void cboVillage_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateVillageName();
        }

        private void updateVillageName()
        {
            this.lblVillage.Text = String.Format("{0}{1} {2}{3}", Resources.VILLAGE, this.cboVillage.Text,Resources.COMMUNE, this.cboCommune.Text);
        }
         
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.cboVillage.SelectedIndex != -1)
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void loadVillage(string villageCode)
        {
            this.cboProvince.SelectedIndex = -1;
            this.cboDistrict.SelectedIndex = -1;
            this.cboCommune.SelectedIndex = -1;
            this.cboVillage.SelectedIndex = -1;

            if (DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode) != null)
            {
                string communeCode = DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode).COMMUNE_CODE;
                string districCode = DBDataContext.Db.TLKP_COMMUNEs.FirstOrDefault(row => row.COMMUNE_CODE == communeCode).DISTRICT_CODE;
                string province = DBDataContext.Db.TLKP_DISTRICTs.FirstOrDefault(row => row.DISTRICT_CODE == districCode).PROVINCE_CODE;

                UIHelper.SetDataSourceToComboBox(this.cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(row => row.PROVINCE_CODE == province));
                UIHelper.SetDataSourceToComboBox(this.cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(row => row.DISTRICT_CODE == districCode));
                UIHelper.SetDataSourceToComboBox(this.cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(row => row.COMMUNE_CODE == communeCode));

                this.cboProvince.SelectedValue = province;
                this.cboDistrict.SelectedValue = districCode;
                this.cboCommune.SelectedValue = communeCode;
                this.cboVillage.SelectedValue = villageCode;
            }

            this.updateVillageName();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
