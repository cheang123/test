﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageCustomerUnBlock : Form
    {
        bool _loading = false;
        List<TLKP_CURRENCY> currency = new List<TLKP_CURRENCY>();
        #region Constructor
        public PageCustomerUnBlock()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            currency = DBDataContext.Db.TLKP_CURRENCies.ToList();
            BindReport();
        }
        #endregion Constructor

        #region Method
        public void BindData()
        {
            if (_loading) return;
            _loading = true;
            int customerTypeId = 0;
            int areaID = 0;
            int billingCycleId = 0;
            int currencyId = 0;
            int typeID = cboConnectionType.SelectedValue == null ? 0 : (int)cboConnectionType.SelectedValue;
            int cusGroupId = (int)cboCustomerType.SelectedValue;

            if (cboConnectionType.SelectedIndex != -1)
            {
                customerTypeId = (int)this.cboConnectionType.SelectedValue;
            }
            if (cboArea.SelectedIndex != -1)
            {
                areaID = (int)this.cboArea.SelectedValue;
            }
            if (cboBillingCycle.SelectedIndex != -1)
            {
                billingCycleId = (int)cboBillingCycle.SelectedValue;
            }
            if (cboCurrencyId.SelectedIndex != -1)
            {
                currencyId = (int)cboCurrencyId.SelectedValue;
            }
            string strSearch = this.txtSearchCus.Text.ToLower();
            DateTime datCurrentDate = DBDataContext.Db.GetSystemDate();

            //get cut of amount.
            decimal decCutAmount = Convert.ToDecimal(DBDataContext.Db.TBL_UTILITies
                                .FirstOrDefault(u => u.UTILITY_ID == (int)Utility.CUT_OFF_AMOUNT)
                                .UTILITY_VALUE);
            DBDataContext.Db.CommandTimeout = 20 * 60;
            DataTable dt = new DataTable();
            //Runner.RunNewThread(delegate()
            //{
                var customer = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on c.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                                join cg in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on ct.NONLICENSE_CUSTOMER_GROUP_ID equals cg.CUSTOMER_GROUP_ID
                                join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                                where c.STATUS_ID == (int)CustomerStatus.Blocked
                                && c.IS_POST_PAID
                                && ((c.CUSTOMER_CODE + " " + c.LAST_NAME_KH + " " + c.FIRST_NAME_KH + " " + c.PHONE_1).ToLower().Contains(strSearch))
                                && (cusGroupId == 0 || ct.NONLICENSE_CUSTOMER_GROUP_ID == cusGroupId)
                                && (typeID == 0 || c.CUSTOMER_CONNECTION_TYPE_ID == typeID)
                                && (areaID == 0 || c.AREA_ID == areaID)
                                && (billingCycleId == 0 || c.BILLING_CYCLE_ID == billingCycleId) 
                                select new
                                {
                                    c.CUSTOMER_ID,
                                    a.AREA_NAME,
                                    c.CUT_OFF_DAY,
                                    c.CUSTOMER_CODE,
                                    CUSTOMER_CONNECTION_TYPE_NAME = cg.CUSTOMER_GROUP_ID == 1 || cg.CUSTOMER_GROUP_ID == 2 || cg.CUSTOMER_GROUP_ID == 3 ? cg.CUSTOMER_GROUP_NAME + " (" + ct.CUSTOMER_CONNECTION_TYPE_NAME + ")" : ct.CUSTOMER_CONNECTION_TYPE_NAME,
                                    CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH
                                }).ToList();

                var invoice = (from inv in DBDataContext.Db.TBL_INVOICEs
                               //where
                               // inv.INVOICE_STATUS == 1
                               group inv by new
                               {
                                   inv.CUSTOMER_ID,
                                   inv.CURRENCY_ID
                               } into g
                               select new
                               {
                                   g.Key.CUSTOMER_ID,
                                   g.Key.CURRENCY_ID,
                                   LAST_DUE_DATE = (DateTime)g.Max(o => o.DUE_DATE),
                                   DUE_DATE = (DateTime)g.Min(p => p.DUE_DATE),
                                   BALANCE_DUE = (decimal?)g.Sum(p => p.SETTLE_AMOUNT - p.PAID_AMOUNT)
                               }).ToList();

                dt = (from c in customer
                      join i in invoice on c.CUSTOMER_ID equals i.CUSTOMER_ID
                      join cs in currency on i.CURRENCY_ID equals cs.CURRENCY_ID
                      where (currencyId == 0 || i.CURRENCY_ID == currencyId)
                      select new
                      {
                          c.CUSTOMER_CODE,
                          c.CUSTOMER_ID,
                          CUSTOMER_TYPE_NAME = c.CUSTOMER_CONNECTION_TYPE_NAME,
                          c.AREA_NAME,
                          c.CUSTOMER_NAME,
                          DUE_DATE = (invoice == null ? datCurrentDate : i.DUE_DATE.AddDays(c.CUT_OFF_DAY)),
                          Day = (invoice == null ? 0 : (datCurrentDate.Date - i.DUE_DATE.AddDays(c.CUT_OFF_DAY).Date).TotalDays),
                          BALANCE_DUE = (invoice == null ? 0 : i.BALANCE_DUE),
                          CURRENCY_SING = invoice == null ? "" : cs.CURRENCY_SING
                      })._ToDataTable();
            //});
            dgv.DataSource = dt;
            _loading = false;
        }
        #endregion Method

        private void BindReport()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(0, Resources.REPORT_CUSTOMER_UNBLOCK);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
            cboReport.SelectedIndex = 0;
        }

        #region Event

        private void btnUnblock_Click(object sender, EventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_UNBLOCK_CUSTOMER, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            if (dgv.SelectedRows.Count > 0)
            {
                int intCusId = int.Parse(dgv.SelectedRows[0].Cells[CUSTOMER_ID.Name].Value.ToString());
                TBL_CUSTOMER objCus = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(c => c.CUSTOMER_ID == intCusId);
                DialogCustomerBlock objDiag = new DialogCustomerBlock(CustomerOperation.ACTIVATE_CUSTOMER, objCus);
                objDiag.ShowDialog();
                if (objDiag.DialogResult == DialogResult.OK)
                {
                    BindData();
                }
            }
        }

        private void txtSearchCus_QuickSearch(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            Runner.Run(delegate()
            {
                CrystalReportHelper cr = new CrystalReportHelper("ReportUnblockCustomer.rpt");
                cr.SetParameter("@CUSTOMER_TYPE_ID", (int)cboCustomerType.SelectedValue);
                cr.SetParameter("@AREA_ID", (int)cboArea.SelectedValue);
                cr.SetParameter("@BILLING_CYCLE_ID", (int)cboBillingCycle.SelectedValue);
                cr.SetParameter("@CURRENCY_ID", (int)cboCurrencyId.SelectedValue);
                cr.ViewReport(Resources.REPORT);
                // dispose reprot helper.
                cr.Dispose();
            });
        }

        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion Event     

        private void PageCustomerUnBlock_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                _loading = true;
                UIHelper.SetDataSourceToComboBox(this.cboCustomerType, Lookup.GetCustomerGroup(), Resources.ALL_TYPE);
                cboCustomerType.SelectedValue = cboCustomerType.Tag;
                UIHelper.SetDataSourceToComboBox(this.cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
                cboArea.SelectedValue = cboArea.Tag;
                UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
                cboBillingCycle.SelectedValue = cboBillingCycle.Tag;
                UIHelper.SetDataSourceToComboBox(this.cboCurrencyId, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
                cboCurrencyId.SelectedValue = cboCurrencyId.Tag;
                int groupId = DataHelper.ParseToInt(cboCustomerType.SelectedValue.ToString());
                UIHelper.SetDataSourceToComboBox(cboConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                                                                    .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId)
                                                                    .OrderBy(x => x.DESCRIPTION)
                                                                    , Resources.ALL_CONNECTION_TYPE);
                cboConnectionType.SelectedValue = cboConnectionType.Tag;
                if (cboConnectionType.DataSource != null)
                {
                    cboConnectionType.SelectedIndex = 0;
                }
                _loading = false;
            }
        }

        private void cboSaveState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo.SelectedIndex == -1)
            {
                return;
            }
            cbo.Tag = cbo.SelectedValue;
        }

        private void cboCustomerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int groupId = DataHelper.ParseToInt(cboCustomerType.SelectedValue.ToString());
            UIHelper.SetDataSourceToComboBox(cboConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                                                                .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId)
                                                                .OrderBy(x => x.DESCRIPTION)
                                                                , Resources.ALL_CONNECTION_TYPE);
            if (cboConnectionType.DataSource != null)
            {
                cboConnectionType.SelectedIndex = 0;
            }
            cboSaveState_SelectedIndexChanged(sender, e);
            this.BindData();
        }

        private void cboConnectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboSaveState_SelectedIndexChanged(sender, e);
            this.BindData();
        }

        private void cboCurrencyId_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboSaveState_SelectedIndexChanged(sender, e);
            this.BindData();
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            //viewReport();
        }
        void viewReport()
        {
            if (dgv.SelectedRows.Count == 0)
            {
                return;
            }

            if (cboReport.SelectedIndex == 0)
            {
                Runner.Run(delegate ()
                {
                    CrystalReportHelper cr = new CrystalReportHelper("ReportUnblockCustomer.rpt");
                    cr.SetParameter("@CUSTOMER_TYPE_ID", (int)cboCustomerType.SelectedValue);
                    cr.SetParameter("@AREA_ID", (int)cboArea.SelectedValue);
                    cr.SetParameter("@BILLING_CYCLE_ID", (int)cboBillingCycle.SelectedValue);
                    cr.SetParameter("@CURRENCY_ID", (int)cboCurrencyId.SelectedValue);
                    cr.ViewReport(Resources.REPORT_CUSTOMER_UNBLOCK);
                    cr.Dispose();
                });
            }
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            viewReport();
        }
    }
}
