﻿
namespace EPower.Interface.Report
{
    partial class DialogSellCustomerTypeOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.cboCustomerConnectionType = new System.Windows.Forms.ComboBox();
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.lblCONNECTION = new System.Windows.Forms.Label();
            this.lblCUSTOMER_GROUP = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboArea);
            this.content.Controls.Add(this.cboBillingCycle);
            this.content.Controls.Add(this.cboCustomerConnectionType);
            this.content.Controls.Add(this.cboCustomerGroup);
            this.content.Controls.Add(this.lblCONNECTION);
            this.content.Controls.Add(this.lblCUSTOMER_GROUP);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            this.content.Size = new System.Drawing.Size(342, 225);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_GROUP, 0);
            this.content.Controls.SetChildIndex(this.lblCONNECTION, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerGroup, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerConnectionType, 0);
            this.content.Controls.SetChildIndex(this.cboBillingCycle, 0);
            this.content.Controls.SetChildIndex(this.cboArea, 0);
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.DropDownWidth = 200;
            this.cboArea.FormattingEnabled = true;
            this.cboArea.Location = new System.Drawing.Point(114, 42);
            this.cboArea.Margin = new System.Windows.Forms.Padding(2);
            this.cboArea.Name = "cboArea";
            this.cboArea.Size = new System.Drawing.Size(223, 27);
            this.cboArea.TabIndex = 40;
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.DropDownWidth = 200;
            this.cboBillingCycle.FormattingEnabled = true;
            this.cboBillingCycle.Location = new System.Drawing.Point(114, 11);
            this.cboBillingCycle.Margin = new System.Windows.Forms.Padding(2);
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.Size = new System.Drawing.Size(223, 27);
            this.cboBillingCycle.TabIndex = 41;
            // 
            // cboCustomerConnectionType
            // 
            this.cboCustomerConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerConnectionType.DropDownWidth = 100;
            this.cboCustomerConnectionType.FormattingEnabled = true;
            this.cboCustomerConnectionType.Location = new System.Drawing.Point(114, 135);
            this.cboCustomerConnectionType.Margin = new System.Windows.Forms.Padding(2);
            this.cboCustomerConnectionType.Name = "cboCustomerConnectionType";
            this.cboCustomerConnectionType.Size = new System.Drawing.Size(223, 27);
            this.cboCustomerConnectionType.TabIndex = 43;
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.DropDownWidth = 100;
            this.cboCustomerGroup.FormattingEnabled = true;
            this.cboCustomerGroup.Location = new System.Drawing.Point(114, 104);
            this.cboCustomerGroup.Margin = new System.Windows.Forms.Padding(2);
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.Size = new System.Drawing.Size(223, 27);
            this.cboCustomerGroup.TabIndex = 42;
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
            // 
            // lblCONNECTION
            // 
            this.lblCONNECTION.AutoSize = true;
            this.lblCONNECTION.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCONNECTION.Location = new System.Drawing.Point(3, 139);
            this.lblCONNECTION.Margin = new System.Windows.Forms.Padding(2);
            this.lblCONNECTION.Name = "lblCONNECTION";
            this.lblCONNECTION.Size = new System.Drawing.Size(65, 19);
            this.lblCONNECTION.TabIndex = 34;
            this.lblCONNECTION.Text = "ការភ្ជាប់ចរន្ត";
            this.lblCONNECTION.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCUSTOMER_GROUP
            // 
            this.lblCUSTOMER_GROUP.AutoSize = true;
            this.lblCUSTOMER_GROUP.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCUSTOMER_GROUP.Location = new System.Drawing.Point(3, 108);
            this.lblCUSTOMER_GROUP.Margin = new System.Windows.Forms.Padding(2);
            this.lblCUSTOMER_GROUP.Name = "lblCUSTOMER_GROUP";
            this.lblCUSTOMER_GROUP.Size = new System.Drawing.Size(41, 19);
            this.lblCUSTOMER_GROUP.TabIndex = 36;
            this.lblCUSTOMER_GROUP.Text = "ប្រភេទ";
            this.lblCUSTOMER_GROUP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.DropDownWidth = 100;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.Location = new System.Drawing.Point(114, 73);
            this.cboCurrency.Margin = new System.Windows.Forms.Padding(2);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(223, 27);
            this.cboCurrency.TabIndex = 45;
            // 
            // lblCURRENCY
            // 
            this.lblCURRENCY.AutoSize = true;
            this.lblCURRENCY.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCURRENCY.Location = new System.Drawing.Point(3, 77);
            this.lblCURRENCY.Margin = new System.Windows.Forms.Padding(2);
            this.lblCURRENCY.Name = "lblCURRENCY";
            this.lblCURRENCY.Size = new System.Drawing.Size(58, 19);
            this.lblCURRENCY.TabIndex = 35;
            this.lblCURRENCY.Text = "រូបិយប័ណ្ណ";
            this.lblCURRENCY.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAREA
            // 
            this.lblAREA.AutoSize = true;
            this.lblAREA.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblAREA.Location = new System.Drawing.Point(3, 46);
            this.lblAREA.Margin = new System.Windows.Forms.Padding(2);
            this.lblAREA.Name = "lblAREA";
            this.lblAREA.Size = new System.Drawing.Size(33, 19);
            this.lblAREA.TabIndex = 33;
            this.lblAREA.Text = "តំបន់";
            this.lblAREA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCYCLE_NAME
            // 
            this.lblCYCLE_NAME.AutoSize = true;
            this.lblCYCLE_NAME.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCYCLE_NAME.Location = new System.Drawing.Point(3, 15);
            this.lblCYCLE_NAME.Margin = new System.Windows.Forms.Padding(2);
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            this.lblCYCLE_NAME.Size = new System.Drawing.Size(45, 19);
            this.lblCYCLE_NAME.TabIndex = 32;
            this.lblCYCLE_NAME.Text = "ជុំទូទាត់";
            this.lblCYCLE_NAME.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(-6, 188);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(355, 1);
            this.panel1.TabIndex = 31;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnClose.Location = new System.Drawing.Point(266, 195);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 30;
            this.btnClose.Text = "បិទ";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOK.Location = new System.Drawing.Point(186, 195);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(74, 23);
            this.btnOK.TabIndex = 29;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // DialogSellCustomerTypeOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 248);
            this.Name = "DialogSellCustomerTypeOption";
            this.Text = "ការលក់តាមប្រភេទអតិថិជន";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ComboBox cboArea;
        public System.Windows.Forms.ComboBox cboBillingCycle;
        public System.Windows.Forms.ComboBox cboCustomerConnectionType;
        public System.Windows.Forms.ComboBox cboCustomerGroup;
        private System.Windows.Forms.Label lblCONNECTION;
        private System.Windows.Forms.Label lblCUSTOMER_GROUP;
        public System.Windows.Forms.ComboBox cboCurrency;
        private System.Windows.Forms.Label lblCURRENCY;
        private System.Windows.Forms.Label lblAREA;
        private System.Windows.Forms.Label lblCYCLE_NAME;
        private System.Windows.Forms.Panel panel1;
        private SoftTech.Component.ExButton btnClose;
        private SoftTech.Component.ExButton btnOK;
    }
}