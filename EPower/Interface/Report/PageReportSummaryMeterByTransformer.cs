﻿using System;
using System.IO;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageReportSummaryMeterByTransformer : Form
    {
        private CrystalReportHelper ch = null;

        public PageReportSummaryMeterByTransformer()
        {
            InitializeComponent();
            viewer.DefaultView();
            //datalookup();

        }
        private void datalookup()
        {
            UIHelper.SetDataSourceToComboBox(this.cboCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
        }
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        public CrystalReportHelper ViewSummaryMeterByTransformer( )
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportSummaryMeterByTransformer.rpt");
            c.SetParameter("@BILLING_CYCLE_ID", this.cboCycle.SelectedValue);
            c.SetParameter("@BILLING_CYCLE_NAME", this.cboCycle.Text);
            return c;
        }

        private void viewReport()
        {
            this.ch = ViewSummaryMeterByTransformer( ); 
            this.viewer.ReportSource = ch.Report;; 
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }
        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowWarning(EPower.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Resources.WARNING);
                MsgBox.LogError(ex);
            }
        }

        private void PageReportSummaryMeterByTransformer_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                datalookup();
            }
        }
    }
}
