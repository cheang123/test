﻿using DevExpress.XtraBars;
using EPower.Base.Helper;
using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Models;
using EPower.Base.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class GeneralCustomer : Form
    {
        public GeneralCustomer()
        {
            InitializeComponent();
            ApplySetting();
            Reload();
        }
        private void ApplySetting()
        {
            //Dynamic.Helpers.ResourceHelper.ApplyResource(this);
            // Report header             
            this.reportHeader.ReportTitle = Resources.GENERAL_CUSTOMER_INFO;
            dpExport.ToolTip = Resources.ShowAsExportFuction;
            searchControl.Properties.NullValuePrompt = Resources.SEARCH;
            var company = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            this.reportHeader.Company = company;
            //this.reportHeader.Logo = UIHelper.ConvertBinaryToImage(company.COMPANY_LOGO);
            //this.picLogo.Image = UIHelper.ConvertBinaryToImage(company.COMPANY_LOGO);

            //this.reportHeader.Address = company.ADDRESS;
            reportHeader.SetVisibleReportSub1(false);


            colDATE_OF_BIRTH.FormatShortDate();
            colClosed_Date.FormatShortDate();
            colActivate_Date.FormatShortDate();
            dgvCustomer.OptionsPrint.PrintSelectedRowsOnly = false;
            dgvCustomer.OptionsPrint.PrintDetails = true;
            dgvCustomer.SetDefaultGridview();

        }
        public void Reload()
        {
            Search();
        }
        private List<GeneralCustomerListModel> getData()
        {
            //int i = 1;
            var qCustomerMeters = from cm in DBDataContext.Db.TBL_CUSTOMER_METERs
                                  group cm by cm.CUSTOMER_ID
                                  into g
                                  select new
                                  {
                                      CUSTOMER_ID = g.Key,
                                      CUS_METER_ID = g.Max(x => x.CUS_METER_ID)
                                  };



            var query = from cm in DBDataContext.Db.TBL_CUSTOMER_METERs
                        join q in qCustomerMeters on cm.CUS_METER_ID equals q.CUS_METER_ID
                        orderby cm.CUS_METER_ID descending
                        group cm by new { cm.CUSTOMER_ID, cm.BOX_ID } into tmp
                        select new { tmp.Key.CUSTOMER_ID, tmp.Key.BOX_ID };


            var data = (from c in DBDataContext.Db.TBL_CUSTOMERs
                        join type in DBDataContext.Db.TBL_CUSTOMER_TYPEs on c.CUSTOMER_TYPE_ID equals type.CUSTOMER_TYPE_ID
                        join s in DBDataContext.Db.TLKP_CUS_STATUS on c.STATUS_ID equals s.CUS_STATUS_ID
                        join area in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals area.AREA_ID
                        join cm in (
                                from m in DBDataContext.Db.TBL_METERs
                                join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on m.METER_ID equals cm.METER_ID
                                join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                                join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                                where cm.IS_ACTIVE
                                select new { cm.CUSTOMER_ID, m.METER_CODE, p.POLE_CODE, b.BOX_CODE }
                             ) on c.CUSTOMER_ID equals cm.CUSTOMER_ID into cmTmp
                        from cmt in cmTmp.DefaultIfEmpty()
                        select new GeneralCustomerListModel
                        {
                            Id = c.CUSTOMER_ID,
                            Customer_Code = c.CUSTOMER_CODE,
                            Customer_Name_Kh = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                            Address = c.ADDRESS,
                            Phone = c.PHONE_1,
                            National_Card_No = c.ID_CARD_NO,
                            Birth_Date_Raw = c.BIRTH_DATE,
                            Birth_Place = c.BIRTH_PLACE,
                            Job = c.JOB,
                            House_No = c.HOUSE_NO,
                            Street_No = c.STREET_NO,
                            Box_Code = cmt.BOX_CODE,
                            Area_Name = area.AREA_NAME,
                            Pole_Code = cmt.POLE_CODE,
                            Activate_Date = c.ACTIVATE_DATE,
                            //Closed_Date = c.CLOSED_DATE,
                            Status = s.CUS_STATUS_NAME
                        }).OrderBy(x => x.Area_Name).ThenBy(x => x.Pole_Code).ThenBy(x => x.Box_Code).ToList();//OrderBy(x => x.Customer_Code).ThenByDescending(x => x.Area_Name).ThenByDescending(x => x.Pole_Code).ThenByDescending(x => x.Box_Code).ToList();
            return data;

        }
        private void Search()
        {
            var data = new List<GeneralCustomerListModel>();

            Runner.RunNewThread(() =>
            {
                data = getData();
            }, Resources.DOWNLOADING);

            dgv.DataSource = data;
            dgvCustomer.SelectAll();
        }


        public void ExportToCsv()
        {
            DevExtension.ToCsv(dgv, Resources.GENERAL_CUSTOMER_INFO, "", Resources.GENERAL_CUSTOMER_INFO, true);
        }
        public void Print()
        {
            DevExtension.ShowGridPreview(dgv, Resources.GENERAL_CUSTOMER_INFO, "", true);
        }
        public void ExportToExcel()
        {
            DevExtension.ToExcel(dgv, Resources.GENERAL_CUSTOMER_INFO, "", Resources.GENERAL_CUSTOMER_INFO, true, true);
        }

        public void ExportToPdf()
        {
            DevExtension.ToPdf(dgv, Resources.GENERAL_CUSTOMER_INFO, "", Resources.GENERAL_CUSTOMER_INFO, true);
        }

        private void btnExcel_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToExcel();
        }

        private void btnCsv_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToCsv();
        }

        private void btnPdf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToPdf();
        }

        private void btnPRINT_ItemClick(object sender, ItemClickEventArgs e)
        {
            Print();
        }

        private void dgvCustomer_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                e.Value = (e.ListSourceRowIndex + 1);
            }
        }

        private void GeneralCustomer_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.Reload();
            }
        }

    }
}