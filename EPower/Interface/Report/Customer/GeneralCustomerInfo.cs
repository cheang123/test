﻿using DevExpress.XtraBars;
using EPower.Component;
using EPower.Helper;
using EPower.Interface.Report.Customer.Model;
using EPower.Interface.Report.EFilling.Model;
using EPower.Properties;
using EWater.Helper.DevExpressCustomize;
using SoftTech;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class GeneralCustomerInfo : Form
    {
        public  GeneralCustomerInfo()
        {
            InitializeComponent();
            ApplySetting();
            Reload();
            Control.CheckForIllegalCrossThreadCalls = false;
        }
        
        private void ApplySetting()
        {
            Dynamic.Helpers.ResourceHelper.ApplyResource(this);
            // Report header             
            //dpExport.ToolTip = Resources.ShowAsExportFuction;
            searchControl.Properties.NullValuePrompt = Resources.Search;
            this.reportHeader.Company = DBDataContext.Db.TBL_COMPANies.FirstOrDefault();
            this.reportHeader.ReportTitle = Resources.GeneralCustomerInformation;
            dgv.RefreshDataSource();
            colBirth_Date.FormatShortDate();
            colClosed_Date.FormatShortDate();
            colActivate_Date.FormatShortDate();
            dgvCustomer.OptionsPrint.PrintSelectedRowsOnly = false;
            dgvCustomer.OptionsPrint.PrintDetails = true;
            dgvCustomer.SetDefaultGridview();

        }
        public void Reload()
        {            
            Search();           
        }
        private  List<GeneralCustomerListModel> getData()
        {
            var data = (from c in DBDataContext.Db.TBL_CUSTOMERs
                        join s in DBDataContext.Db.TLKP_CUS_STATUS on c.STATUS_ID equals s.CUS_STATUS_ID
                        join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on c.CUSTOMER_ID equals cm.CUSTOMER_ID into cmj from cm in cmj.DefaultIfEmpty()
                        join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID into aj from a in aj.DefaultIfEmpty()
                        join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID into bj from b in bj.DefaultIfEmpty()
                        join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID into pj from p in pj.DefaultIfEmpty()
                        where cm.IS_ACTIVE 
                        select new GeneralCustomerListModel
                        {
                            Id = c.CUSTOMER_ID,
                            Customer_Code = c.CUSTOMER_CODE,
                            Customer_Name_Kh = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                            Address = c.ADDRESS,
                            Phone = c.PHONE_1,
                            National_Card_No=c.ID_CARD_NO==""?"-":c.ID_CARD_NO,
                            Birth_Date = c.BIRTH_DATE,
                            Birth_Place = c.BIRTH_PLACE==""?"-":c.BIRTH_PLACE,
                            Job = c.JOB==""?"-":c.JOB,
                            House_No = c.HOUSE_NO==""?"-":c.HOUSE_NO,
                            Street_No = c.STREET_NO==""?"-":c.STREET_NO,
                            Latitude = c.LATITUDE,
                            Longitude = c.LONGITUDE,
                            Box_Code = b.BOX_CODE,
                            Area_Name = a.AREA_NAME,
                            Pole_Code = p.POLE_CODE,
                            Activate_Date = c.ACTIVATE_DATE,
                            Closed_Date = c.CLOSED_DATE  ,
                            Status= s.CUS_STATUS_NAME
                        }).OrderBy(x=>x.Customer_Code).ToList();                                 
            return data;

        }
        private void Search()
        {
            try
            {
                Runner.RunDbTran(() =>
                {
                    var data = getData();
                    dgv.DataSource = data;
                }, Resources.Downloading);
            }
            catch (Exception ex)
            {
                MsgBox.ShowWarning(Resources.MsgConnectionError, Resources.Warning);
            }        
        }

       
        public void ExportToCsv()
        {
            DevExtension.ToCsv(dgv, Resources.GeneralCustomerInformation, "", Resources.GeneralCustomerInformation, true);
        }
        public void Print()
        {
            DevExtension.ShowGridPreview(dgv, Resources.GeneralCustomerInformation, "", true);
        }
        public void ExportToExcel()
        {
            DevExtension.ToExcel(dgv, Resources.GeneralCustomerInformation, "", Resources.GeneralCustomerInformation, true, true);
        }
       
        public void ExportToPdf()
        {
            DevExtension.ToPdf(dgv, Resources.GeneralCustomerInformation, "", Resources.GeneralCustomerInformation, true);
        }
     
        private void btnExcel_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToExcel();
        }

        private void btnCsv_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToCsv();
        }

        private void btnPdf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ExportToPdf();
        }

        private void btnPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            Print();
        }
        private void dgvCustomer_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.IsGetData)
            {
                e.Value = (e.ListSourceRowIndex + 1);
            }
        }

        private void GeneralCustomerInfo_Load(object sender, EventArgs e)
        {
            Reload();
        }
    }
}