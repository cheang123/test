﻿using EPower.Component;
using SoftTech;

namespace EPower.Interface
{
    partial class GeneralCustomerInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            SoftTech.TBL_COMPANY tbL_COMPANY1 = new SoftTech.TBL_COMPANY();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeneralCustomerInfo));
            this.panel1 = new System.Windows.Forms.Panel();
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.popupExport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.btnExportEfilingSale = new DevExpress.XtraBars.BarButtonItem();
            this.btnExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnCsv = new DevExpress.XtraBars.BarButtonItem();
            this.btnPdf = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.btnEFilingSale = new DevExpress.XtraBars.BarButtonItem();
            this.btnPurchaseOverSea = new DevExpress.XtraBars.BarButtonItem();
            this.btnIndividualVendor = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.btnCreditNote = new DevExpress.XtraBars.BarButtonItem();
            this.popActions = new DevExpress.XtraBars.PopupMenu(this.components);
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.reportHeader = new EPower.Helper.ReportHeader();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.dgvCustomer = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRowNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomer_Code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomer_Name_Kh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBirth_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBirth_Place = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNational_Card_No = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJob = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHouse_No = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStreet_No = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArea_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPole_Code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Box_Code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActivate_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosed_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.dpExport = new DevExpress.XtraEditors.DropDownButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.searchControl);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1197, 38);
            this.panel1.TabIndex = 18;
            // 
            // searchControl
            // 
            this.searchControl.EditValue = "";
            this.searchControl.Location = new System.Drawing.Point(3, 4);
            this.searchControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            this.searchControl.Size = new System.Drawing.Size(173, 28);
            this.searchControl.TabIndex = 35;
            // 
            // popupExport
            // 
            this.popupExport.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExportEfilingSale, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExcel, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCsv),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPdf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrint, true)});
            this.popupExport.Manager = this.barManager1;
            this.popupExport.Name = "popupExport";
            // 
            // btnExportEfilingSale
            // 
            this.btnExportEfilingSale.Id = 11;
            this.btnExportEfilingSale.Name = "btnExportEfilingSale";
            // 
            // btnExcel
            // 
            this.btnExcel.Caption = "Excel";
            this.btnExcel.Id = 3;
            this.btnExcel.ItemAppearance.Disabled.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnExcel.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Hovered.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnExcel.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Normal.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnExcel.ItemAppearance.Normal.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Pressed.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnExcel.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Disabled.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnExcel.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Hovered.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnExcel.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnExcel.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Pressed.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnExcel.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExcel_ItemClick);
            // 
            // btnCsv
            // 
            this.btnCsv.Caption = "Csv";
            this.btnCsv.Id = 5;
            this.btnCsv.ItemAppearance.Disabled.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnCsv.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Hovered.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnCsv.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Normal.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnCsv.ItemAppearance.Normal.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Pressed.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnCsv.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Disabled.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnCsv.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Hovered.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnCsv.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnCsv.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Pressed.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnCsv.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnCsv.Name = "btnCsv";
            this.btnCsv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCsv_ItemClick);
            // 
            // btnPdf
            // 
            this.btnPdf.Caption = "Pdf";
            this.btnPdf.Id = 2;
            this.btnPdf.ItemAppearance.Disabled.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPdf.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Hovered.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPdf.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Normal.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPdf.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Pressed.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPdf.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Disabled.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPdf.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Hovered.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPdf.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPdf.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Pressed.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPdf.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPdf.Name = "btnPdf";
            this.btnPdf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPdf_ItemClick);
            // 
            // btnPrint
            // 
            this.btnPrint.Caption = "Print";
            this.btnPrint.Id = 4;
            this.btnPrint.ItemAppearance.Disabled.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPrint.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Hovered.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPrint.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Normal.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPrint.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Pressed.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPrint.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Disabled.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPrint.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Hovered.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPrint.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPrint.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Pressed.Font = new System.Drawing.Font("Khmer Kep", 9F);
            this.btnPrint.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrint_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnExcel,
            this.btnCsv,
            this.btnPdf,
            this.btnPrint,
            this.btnEFilingSale,
            this.btnPurchaseOverSea,
            this.btnIndividualVendor,
            this.btnExportEfilingSale,
            this.barButtonItem1,
            this.btnCreditNote});
            this.barManager1.MaxItemId = 14;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1197, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 581);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1197, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 581);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl1.Location = new System.Drawing.Point(1197, 0);
            this.barDockControl1.Manager = this.barManager1;
            this.barDockControl1.Size = new System.Drawing.Size(0, 581);
            // 
            // btnEFilingSale
            // 
            this.btnEFilingSale.Id = 8;
            this.btnEFilingSale.Name = "btnEFilingSale";
            // 
            // btnPurchaseOverSea
            // 
            this.btnPurchaseOverSea.Id = 9;
            this.btnPurchaseOverSea.Name = "btnPurchaseOverSea";
            // 
            // btnIndividualVendor
            // 
            this.btnIndividualVendor.Id = 10;
            this.btnIndividualVendor.Name = "btnIndividualVendor";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 12;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // btnCreditNote
            // 
            this.btnCreditNote.Id = 13;
            this.btnCreditNote.Name = "btnCreditNote";
            // 
            // popActions
            // 
            this.popActions.Name = "popActions";
            // 
            // gridBand1
            // 
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = -1;
            // 
            // gridBand4
            // 
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = -1;
            // 
            // gridBand5
            // 
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = -1;
            // 
            // gridBand2
            // 
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = -1;
            // 
            // gridBand8
            // 
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = -1;
            // 
            // gridBand9
            // 
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = -1;
            // 
            // reportHeader
            // 
            this.reportHeader.AutoSize = true;
            this.reportHeader.BackColor = System.Drawing.Color.White;
            tbL_COMPANY1.ADDRESS = null;
            tbL_COMPANY1.COMPANY_ID = 0;
            tbL_COMPANY1.COMPANY_LOGO = null;
            tbL_COMPANY1.COMPANY_NAME = null;
            tbL_COMPANY1.COMPANY_NAME_EN = null;
            tbL_COMPANY1.LICENSE_NAME = null;
            tbL_COMPANY1.LICENSE_NAME_KH = null;
            tbL_COMPANY1.LICENSE_NUMBER = null;
            tbL_COMPANY1.LICENSE_TYPE = null;
            tbL_COMPANY1.NOTE = null;
            tbL_COMPANY1.PHONE = null;
            tbL_COMPANY1.VATTIN = null;
            tbL_COMPANY1.VILLAGE_CODE = null;
            this.reportHeader.Company = tbL_COMPANY1;
            this.reportHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.reportHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.reportHeader.Location = new System.Drawing.Point(0, 38);
            this.reportHeader.Margin = new System.Windows.Forms.Padding(0);
            this.reportHeader.Name = "reportHeader";
            this.reportHeader.Padding = new System.Windows.Forms.Padding(0, 10, 0, 3);
            this.reportHeader.ReportSubTitle1 = "";
            this.reportHeader.ReportTitle = "ចំណងជើង";
            this.reportHeader.Size = new System.Drawing.Size(1197, 80);
            this.reportHeader.TabIndex = 66;
            // 
            // dgv
            // 
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Font = new System.Drawing.Font("Kh Siemreap", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv.Location = new System.Drawing.Point(0, 118);
            this.dgv.MainView = this.dgvCustomer;
            this.dgv.Name = "dgv";
            this.dgv.Size = new System.Drawing.Size(1197, 463);
            this.dgv.TabIndex = 68;
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvCustomer});
            // 
            // dgvCustomer
            // 
            this.dgvCustomer.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.dgvCustomer.Appearance.DetailTip.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.DetailTip.Options.UseFont = true;
            this.dgvCustomer.Appearance.EvenRow.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.EvenRow.Options.UseFont = true;
            this.dgvCustomer.Appearance.FilterPanel.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.FilterPanel.Options.UseFont = true;
            this.dgvCustomer.Appearance.FocusedCell.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.FocusedCell.Options.UseFont = true;
            this.dgvCustomer.Appearance.FocusedRow.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.FocusedRow.Options.UseFont = true;
            this.dgvCustomer.Appearance.FooterPanel.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.FooterPanel.Options.UseFont = true;
            this.dgvCustomer.Appearance.GroupButton.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.GroupButton.Options.UseFont = true;
            this.dgvCustomer.Appearance.GroupFooter.Font = new System.Drawing.Font("Kh Siemreap", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.GroupFooter.Options.UseFont = true;
            this.dgvCustomer.Appearance.GroupPanel.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.GroupPanel.Options.UseFont = true;
            this.dgvCustomer.Appearance.GroupRow.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.GroupRow.Options.UseFont = true;
            this.dgvCustomer.Appearance.HeaderPanel.Font = new System.Drawing.Font("Kh Siemreap", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.HeaderPanel.Options.UseFont = true;
            this.dgvCustomer.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.HideSelectionRow.Options.UseFont = true;
            this.dgvCustomer.Appearance.HotTrackedRow.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.HotTrackedRow.Options.UseFont = true;
            this.dgvCustomer.Appearance.OddRow.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.OddRow.Options.UseFont = true;
            this.dgvCustomer.Appearance.Preview.Font = new System.Drawing.Font("Kh Siemreap", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.Preview.Options.UseFont = true;
            this.dgvCustomer.Appearance.Row.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.Row.Options.UseFont = true;
            this.dgvCustomer.Appearance.RowSeparator.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.RowSeparator.Options.UseFont = true;
            this.dgvCustomer.Appearance.SelectedRow.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dgvCustomer.Appearance.SelectedRow.Options.UseBackColor = true;
            this.dgvCustomer.Appearance.TopNewRow.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.TopNewRow.Options.UseFont = true;
            this.dgvCustomer.Appearance.ViewCaption.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.Appearance.ViewCaption.Options.UseFont = true;
            this.dgvCustomer.AppearancePrint.EvenRow.Font = new System.Drawing.Font("Kh Siemreap", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.AppearancePrint.EvenRow.Options.UseFont = true;
            this.dgvCustomer.AppearancePrint.FilterPanel.Font = new System.Drawing.Font("Kh Siemreap", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.AppearancePrint.FilterPanel.Options.UseFont = true;
            this.dgvCustomer.AppearancePrint.FooterPanel.Font = new System.Drawing.Font("Kh Siemreap", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.AppearancePrint.FooterPanel.Options.UseFont = true;
            this.dgvCustomer.AppearancePrint.GroupFooter.Font = new System.Drawing.Font("Kh Siemreap", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.AppearancePrint.GroupFooter.Options.UseFont = true;
            this.dgvCustomer.AppearancePrint.GroupRow.Font = new System.Drawing.Font("Kh Siemreap", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.AppearancePrint.GroupRow.Options.UseFont = true;
            this.dgvCustomer.AppearancePrint.HeaderPanel.Font = new System.Drawing.Font("Kh Siemreap", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.AppearancePrint.HeaderPanel.Options.UseFont = true;
            this.dgvCustomer.AppearancePrint.Lines.Font = new System.Drawing.Font("Kh Siemreap", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.AppearancePrint.Lines.Options.UseFont = true;
            this.dgvCustomer.AppearancePrint.OddRow.Font = new System.Drawing.Font("Kh Siemreap", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.AppearancePrint.OddRow.Options.UseFont = true;
            this.dgvCustomer.AppearancePrint.Preview.Font = new System.Drawing.Font("Kh Siemreap", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.AppearancePrint.Preview.Options.UseFont = true;
            this.dgvCustomer.AppearancePrint.Row.Font = new System.Drawing.Font("Kh Siemreap", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvCustomer.AppearancePrint.Row.Options.UseFont = true;
            this.dgvCustomer.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRowNo,
            this.colCustomer_Code,
            this.colCustomer_Name_Kh,
            this.colAddress,
            this.colPhone,
            this.colBirth_Date,
            this.colBirth_Place,
            this.colNational_Card_No,
            this.colJob,
            this.colHouse_No,
            this.colStreet_No,
            this.colArea_Name,
            this.colPole_Code,
            this.Box_Code,
            this.colActivate_Date,
            this.colClosed_Date,
            this.colLatitude,
            this.colLongitude,
            this.colStatus});
            this.dgvCustomer.GridControl = this.dgv;
            this.dgvCustomer.Name = "dgvCustomer";
            this.dgvCustomer.OptionsDetail.EnableMasterViewMode = false;
            this.dgvCustomer.OptionsPrint.PrintFooter = false;
            this.dgvCustomer.OptionsPrint.PrintGroupFooter = false;
            this.dgvCustomer.OptionsSelection.ShowCheckBoxSelectorInPrintExport = DevExpress.Utils.DefaultBoolean.False;
            this.dgvCustomer.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent;
            this.dgvCustomer.OptionsView.ShowFooter = true;
            this.dgvCustomer.OptionsView.WaitAnimationOptions = DevExpress.XtraEditors.WaitAnimationOptions.Indicator;
            // 
            // colRowNo
            // 
            this.colRowNo.AppearanceCell.Font = new System.Drawing.Font("Kh Siemreap", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colRowNo.AppearanceCell.Options.UseFont = true;
            this.colRowNo.AppearanceCell.Options.UseTextOptions = true;
            this.colRowNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRowNo.AppearanceHeader.Font = new System.Drawing.Font("Kh Siemreap", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colRowNo.AppearanceHeader.Options.UseFont = true;
            this.colRowNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colRowNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRowNo.Caption = "ល.រ";
            this.colRowNo.FieldName = "RowNo";
            this.colRowNo.Name = "colRowNo";
            this.colRowNo.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.colRowNo.Width = 59;
            // 
            // colCustomer_Code
            // 
            this.colCustomer_Code.AppearanceCell.Options.UseTextOptions = true;
            this.colCustomer_Code.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCustomer_Code.AppearanceHeader.Options.UseTextOptions = true;
            this.colCustomer_Code.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCustomer_Code.Caption = "លេខកូដ";
            this.colCustomer_Code.FieldName = "Customer_Code";
            this.colCustomer_Code.Name = "colCustomer_Code";
            this.colCustomer_Code.Visible = true;
            this.colCustomer_Code.VisibleIndex = 0;
            this.colCustomer_Code.Width = 90;
            // 
            // colCustomer_Name_Kh
            // 
            this.colCustomer_Name_Kh.Caption = "ឈ្មោះអតិថិជន";
            this.colCustomer_Name_Kh.FieldName = "Customer_Name_Kh";
            this.colCustomer_Name_Kh.Name = "colCustomer_Name_Kh";
            this.colCustomer_Name_Kh.Visible = true;
            this.colCustomer_Name_Kh.VisibleIndex = 1;
            this.colCustomer_Name_Kh.Width = 107;
            // 
            // colAddress
            // 
            this.colAddress.Caption = "អាស័យដ្ឋាន";
            this.colAddress.FieldName = "Address";
            this.colAddress.Name = "colAddress";
            this.colAddress.Visible = true;
            this.colAddress.VisibleIndex = 2;
            this.colAddress.Width = 93;
            // 
            // colPhone
            // 
            this.colPhone.Caption = "លេខទូរស័ព្ទ";
            this.colPhone.FieldName = "Phone";
            this.colPhone.Name = "colPhone";
            this.colPhone.Visible = true;
            this.colPhone.VisibleIndex = 3;
            this.colPhone.Width = 87;
            // 
            // colBirth_Date
            // 
            this.colBirth_Date.Caption = "ថ្ងៃខែឆ្នាំកំណើត";
            this.colBirth_Date.FieldName = "Birth_Date";
            this.colBirth_Date.Name = "colBirth_Date";
            this.colBirth_Date.Visible = true;
            this.colBirth_Date.VisibleIndex = 4;
            this.colBirth_Date.Width = 90;
            // 
            // colBirth_Place
            // 
            this.colBirth_Place.Caption = "ទីកន្លែងកំណើត";
            this.colBirth_Place.FieldName = "Birth_Place";
            this.colBirth_Place.Name = "colBirth_Place";
            this.colBirth_Place.Visible = true;
            this.colBirth_Place.VisibleIndex = 5;
            this.colBirth_Place.Width = 89;
            // 
            // colNational_Card_No
            // 
            this.colNational_Card_No.Caption = "លេខសម្គាល់អត្តសញ្ញាណ";
            this.colNational_Card_No.FieldName = "National_Card_No";
            this.colNational_Card_No.Name = "colNational_Card_No";
            this.colNational_Card_No.Visible = true;
            this.colNational_Card_No.VisibleIndex = 6;
            this.colNational_Card_No.Width = 128;
            // 
            // colJob
            // 
            this.colJob.Caption = "មុខរបរ";
            this.colJob.FieldName = "Job";
            this.colJob.Name = "colJob";
            this.colJob.Visible = true;
            this.colJob.VisibleIndex = 7;
            this.colJob.Width = 66;
            // 
            // colHouse_No
            // 
            this.colHouse_No.Caption = "ផ្ទះលេខ";
            this.colHouse_No.FieldName = "House_No";
            this.colHouse_No.Name = "colHouse_No";
            this.colHouse_No.Width = 51;
            // 
            // colStreet_No
            // 
            this.colStreet_No.Caption = "ផ្លូវលេខ";
            this.colStreet_No.FieldName = "Street_No";
            this.colStreet_No.Name = "colStreet_No";
            this.colStreet_No.Width = 54;
            // 
            // colArea_Name
            // 
            this.colArea_Name.Caption = "តំបន់";
            this.colArea_Name.FieldName = "Area_Name";
            this.colArea_Name.Name = "colArea_Name";
            this.colArea_Name.Visible = true;
            this.colArea_Name.VisibleIndex = 8;
            this.colArea_Name.Width = 68;
            // 
            // colPole_Code
            // 
            this.colPole_Code.Caption = "ផ្លូវ";
            this.colPole_Code.FieldName = "Pole_Code";
            this.colPole_Code.Name = "colPole_Code";
            this.colPole_Code.Visible = true;
            this.colPole_Code.VisibleIndex = 9;
            this.colPole_Code.Width = 65;
            // 
            // Box_Code
            // 
            this.Box_Code.Caption = "លេខទីតាំង";
            this.Box_Code.FieldName = "Box_Code";
            this.Box_Code.Name = "Box_Code";
            this.Box_Code.Visible = true;
            this.Box_Code.VisibleIndex = 10;
            this.Box_Code.Width = 90;
            // 
            // colActivate_Date
            // 
            this.colActivate_Date.Caption = "ថ្ងៃចាប់ផ្ដើមប្រើ";
            this.colActivate_Date.FieldName = "Activate_Date";
            this.colActivate_Date.Name = "colActivate_Date";
            this.colActivate_Date.Width = 86;
            // 
            // colClosed_Date
            // 
            this.colClosed_Date.Caption = "ថ្ងៃឈប់ប្រើ";
            this.colClosed_Date.FieldName = "Closed_Date";
            this.colClosed_Date.Name = "colClosed_Date";
            this.colClosed_Date.Width = 90;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "រយៈទទឹង";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.Visible = true;
            this.colLatitude.VisibleIndex = 11;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "រយៈបណ្ដោយ";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.Visible = true;
            this.colLongitude.VisibleIndex = 12;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "ស្ថានភាព";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 13;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.flowLayoutPanel3.Controls.Add(this.dpExport);
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(1036, 120);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(161, 28);
            this.flowLayoutPanel3.TabIndex = 69;
            // 
            // dpExport
            // 
            this.dpExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpExport.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.Font = new System.Drawing.Font("Kh Siemreap", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpExport.Appearance.Options.UseBackColor = true;
            this.dpExport.Appearance.Options.UseBorderColor = true;
            this.dpExport.Appearance.Options.UseFont = true;
            this.dpExport.AppearanceDisabled.Font = new System.Drawing.Font("Kh Siemreap", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpExport.AppearanceDisabled.Options.UseFont = true;
            this.dpExport.AppearanceDropDown.Font = new System.Drawing.Font("Kh Siemreap", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpExport.AppearanceDropDown.Options.UseFont = true;
            this.dpExport.AppearanceDropDownDisabled.Font = new System.Drawing.Font("Kh Siemreap", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpExport.AppearanceDropDownDisabled.Options.UseFont = true;
            this.dpExport.AppearanceDropDownHovered.Font = new System.Drawing.Font("Kh Siemreap", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpExport.AppearanceDropDownHovered.Options.UseFont = true;
            this.dpExport.AppearanceDropDownPressed.Font = new System.Drawing.Font("Kh Siemreap", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpExport.AppearanceDropDownPressed.Options.UseFont = true;
            this.dpExport.AppearanceHovered.Font = new System.Drawing.Font("Kh Siemreap", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpExport.AppearanceHovered.Options.UseFont = true;
            this.dpExport.AppearancePressed.Font = new System.Drawing.Font("Kh Siemreap", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpExport.AppearancePressed.Options.UseFont = true;
            this.dpExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dpExport.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dpExport.DropDownControl = this.popupExport;
            this.dpExport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dpExport.ImageOptions.Image")));
            this.dpExport.ImageOptions.ImageUri.Uri = "SendCSV;Size32x32;GrayScaled";
            this.dpExport.ImageOptions.SvgImageSize = new System.Drawing.Size(10, 10);
            this.dpExport.Location = new System.Drawing.Point(117, 1);
            this.dpExport.LookAndFeel.SkinMaskColor = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dpExport.Margin = new System.Windows.Forms.Padding(1);
            this.dpExport.Name = "dpExport";
            this.dpExport.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.dpExport.Size = new System.Drawing.Size(43, 26);
            this.dpExport.TabIndex = 19;
            // 
            // GeneralCustomerInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 581);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.reportHeader);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Name = "GeneralCustomerInfo";
            this.Text = "GeneralCustomerInfoPage";
            this.Load += new System.EventHandler(this.GeneralCustomerInfo_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomer)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SearchControl searchControl;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarButtonItem btnExcel;
        private DevExpress.XtraBars.BarButtonItem btnCsv;
        private DevExpress.XtraBars.BarButtonItem btnPdf;
        private DevExpress.XtraBars.BarButtonItem btnPrint;
        private DevExpress.XtraBars.PopupMenu popupExport;
        private DevExpress.XtraBars.PopupMenu popActions;
        private DevExpress.XtraBars.BarButtonItem btnEFilingSale;
        private DevExpress.XtraBars.BarButtonItem btnPurchaseOverSea;
        private DevExpress.XtraBars.BarButtonItem btnIndividualVendor;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraBars.BarButtonItem btnExportEfilingSale;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem btnCreditNote;
        private Helper.ReportHeader reportHeader;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvCustomer;
        private DevExpress.XtraGrid.Columns.GridColumn colRowNo;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomer_Code;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomer_Name_Kh;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colPhone;
        private DevExpress.XtraGrid.Columns.GridColumn colBirth_Date;
        private DevExpress.XtraGrid.Columns.GridColumn colBirth_Place;
        private DevExpress.XtraGrid.Columns.GridColumn colNational_Card_No;
        private DevExpress.XtraGrid.Columns.GridColumn colJob;
        private DevExpress.XtraGrid.Columns.GridColumn colHouse_No;
        private DevExpress.XtraGrid.Columns.GridColumn colStreet_No;
        private DevExpress.XtraGrid.Columns.GridColumn colArea_Name;
        private DevExpress.XtraGrid.Columns.GridColumn colPole_Code;
        private DevExpress.XtraGrid.Columns.GridColumn Box_Code;
        private DevExpress.XtraGrid.Columns.GridColumn colActivate_Date;
        private DevExpress.XtraGrid.Columns.GridColumn colClosed_Date;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.DropDownButton dpExport;
    }
}