﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportOtherRevenueByMonth : Form
    {
        CrystalReportHelper ch = null;
        private int _service_typeId = 0;

        public PageReportOtherRevenueByMonth()
        {
            InitializeComponent();
            viewer.DefaultView();
            TBL_BILLING_CYCLE objBillingCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(row => row.IS_ACTIVE);
            if (objBillingCycle != null)
            {
                DateTime datCurrentMonth = Method.GetNextBillingMonth(objBillingCycle.CYCLE_ID).AddMonths(-1);
                this.dtpDate.Value = new DateTime(datCurrentMonth.Year, datCurrentMonth.Month, 1);
            }

            dataLookup(); 
        }

        private void dataLookup()
        {
            //try
            //{  
            //    UIHelper.SetDataSourceToComboBox(cboArea, DBDataContext.Db.TBL_AREAs.Where(x=>x.IS_ACTIVE).Select(x=>new{x.AREA_ID,x.AREA_NAME}).OrderBy(x=>x.AREA_NAME), Resources.ALL_AREA);
            //    cboArea.SelectedValue = cboArea.Tag;
            //    UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
            //    cboCurrency.SelectedValue = cboCurrency.Tag;
            //    UIHelper.SetDataSourceToComboBox(cboItemType, DBDataContext.Db.TBL_INVOICE_ITEM_TYPEs.Where(x => x.IS_ACTIVE), Resources.ALL_SERVICE_TYPE);
            //    cboItemType.SelectedValue = cboItemType.Tag;
            //    UIHelper.SetDataSourceToComboBox(cboItem, DBDataContext.Db.TBL_INVOICE_ITEMs.Where(x => x.INVOICE_ITEM_TYPE_ID != 0), Resources.ALL_SERVICE);
            //    cboItem.SelectedValue = cboItem.Tag;
            //}
            //catch (Exception exp)
            //{
            //    MsgBox.ShowError(exp);
            //}  

            UIHelper.SetDataSourceToComboBox(cboArea, DBDataContext.Db.TBL_AREAs.Where(x => x.IS_ACTIVE).Select(x => new { x.AREA_ID, x.AREA_NAME }).OrderBy(x => x.AREA_NAME), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
            UIHelper.SetDataSourceToComboBox(cboItemType, DBDataContext.Db.TBL_INVOICE_ITEM_TYPEs.Where(x => x.IS_ACTIVE).Select(x=> new { x.INVOICE_ITEM_TYPE_ID, x.INVOICE_ITEM_TYPE_NAME}).OrderBy(x=>x.INVOICE_ITEM_TYPE_NAME), Resources.ALL_SERVICE_TYPE);
            UIHelper.SetDataSourceToComboBox(cboItem, DBDataContext.Db.TBL_INVOICE_ITEMs.Where(x => x.INVOICE_ITEM_TYPE_ID != 0).Select(x => new { x.INVOICE_ITEM_ID, x.INVOICE_ITEM_NAME }).OrderBy(x => x.INVOICE_ITEM_NAME), Resources.ALL_SERVICE);
        }
       
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        public CrystalReportHelper ViewReportOtherRevenueByMonth(int intArea,DateTime dtByMonth,int typeId,int itemId,string areaName,string itemTypeName,string itemName,int currencyId,string currencyName)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportOtherRevenueByMonth.rpt");
            c.SetParameter("@AREA_ID", intArea);
            c.SetParameter("@MONTH", new DateTime(dtByMonth.Year, dtByMonth.Month, 1));
            c.SetParameter("@ITEM_TYPE_ID", typeId);
            c.SetParameter("@ITEM_ID", itemId);
            c.SetParameter("@AREA_NAME", areaName);
            c.SetParameter("@ITEM_TYPE_NAME", itemTypeName);
            c.SetParameter("@ITEM_NAME", itemName);
            c.SetParameter("@CURRENCY_ID", currencyId);
            c.SetParameter("@CURRENCY_NAME", currencyName);
            return c;
        }

        private void viewReport()
        {
            try
            {
                this.ch = ViewReportOtherRevenueByMonth(
                   (int)cboArea.SelectedValue,
                   dtpDate.Value.Date,
                   (int)cboItemType.SelectedValue,
                   (int)cboItem.SelectedValue,
                   cboArea.Text,
                   cboItemType.Text,
                   cboItem.Text,
                   (int)cboCurrency.SelectedValue,
                   cboCurrency.Text
               );
                this.viewer.ReportSource = ch.Report;

            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void cboItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int typeId = cboItemType.SelectedIndex == -1 ? 0 : (int)this.cboItemType.SelectedValue;
            //UIHelper.SetDataSourceToComboBox(cboItem, DBDataContext.Db.TBL_INVOICE_ITEMs.Where(x => x.INVOICE_ITEM_TYPE_ID == typeId).Select(x => new { x.INVOICE_ITEM_ID,x.INVOICE_ITEM_NAME}), Resources.ALL_SERVICE);
            //cboItem.SelectedValue = cboItem.Tag;
            //cboSaveState_SelectedIndexChanged(sender, e);
        }

        private void PageReportOtherRevenueByMonth_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                dataLookup();
            }
        }
        private void cboSaveState_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ComboBox cbo = (ComboBox)sender;
            //if (cbo.SelectedIndex == -1)
            //{
            //    return;
            //}
            //cbo.Tag = cbo.SelectedValue;
        }

        private void cboItemType_SelectedValueChanged(object sender, EventArgs e)
        {
            int typeId = cboItemType.SelectedIndex == -1 ? 0 : (int)cboItemType.SelectedValue;
            if (typeId == _service_typeId)
            {
                return;
            }
            UIHelper.SetDataSourceToComboBox(cboItem, DBDataContext.Db.TBL_INVOICE_ITEMs.OrderByDescending(x => x.INVOICE_ITEM_ID).Where(x => (x.INVOICE_ITEM_TYPE_ID == typeId || typeId == 0) && x.IS_ACTIVE && x.INVOICE_ITEM_TYPE_ID!=0).Select(x => new { x.INVOICE_ITEM_ID, x.INVOICE_ITEM_NAME }), Resources.ALL_SERVICE);
            cboItem.SelectedValue = 0;
            _service_typeId = typeId;
        }
    }
}
