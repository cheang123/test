﻿using EPower.Base.Logic;
using EPower.Base.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EPower.Interface.Report
{
    public partial class DialogSellCustomerTypeOption : ExDialog
    {
        public DialogSellCustomerTypeOption()
        {
            InitializeComponent();
            Bind();
        }
        public void Bind()
        {
            UIHelper.SetDataSourceToComboBox(this.cboBillingCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            UIHelper.SetDataSourceToComboBox(cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
            UIHelper.SetDataSourceToComboBox(cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.All_CUSTOMER_GROUP);
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            UIHelper.SetDataSourceToComboBox(cboCustomerConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs.Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == (int)cboCustomerGroup.SelectedValue).OrderBy(x => x.DESCRIPTION), Resources.All_CUSTOMER_CUSTOMER_TYPE);
            if (cboCustomerConnectionType.DataSource != null)
            {
                cboCustomerConnectionType.SelectedIndex = 0;
            }
        }
    }
}
