﻿using System;
using System.Collections.Generic;

namespace EPower.Interface.Report.EFilling.Model
{
    class EFiliingModel
    {

    }
    public class SaleEFiling
    {
        public List<SaleEFilingListModel> SaleEFilingListModels { get; set; }
        public List<SaleEFilingListModel> SaleEFilingDetailListModels { get; set; }
        public List<SaleEFilingListModel> CreditNoteListModels { set; get; }
        public List<IndividualCustomerEFilingListModel> IndividualCustomerEFilingListModels { get; set; }
        public List<OverseaCustomerEFilingListModel> OverseaCustomerEFilingListModels { get; set; }
    }
    public class SaleEFilingListModel
    {
        public int Id { get; set; }
        public DateTime TranDate { get; set; }
        public string VendorInvoiceNo { get; set; }
        //partner 
        public int PartnerType { get; set; }
        public string VatIn { get; set; }
        public string NameKH { get; set; }
        public string NameLatin { get; set; }
        public decimal Quantity { get; set; }
        public string TaxCode { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal NoneTaxAmount { get; set; }
        public string FormatDigit { get; set; }
        public TransactionType Type { get; set; }
        public string Note { get; set; }
        public int CurrencyId { get; set; }
        public int IncomeTaxRate { get; set; }
        public int Sector { get; set; }
        public string Description { get; set; }
    }
    public class IndividualCustomerEFilingListModel
    {
        public int Id { get; set; }
        public string VatIn { get; set; }
        public string NameKH { get; set; }
        public string NameLatin { get; set; }
    }

    public class OverseaCustomerEFilingListModel
    {
        public int Id { get; set; }
        public string Vattin { get; set; }
        public string NameKH { get; set; }
        public string NameLatin { get; set; }
        public string CountryCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }

    //public class Company : BasedModel
    //{
    //    public static Dictionary<int, MediaData> MediaCacheds = new Dictionary<int, MediaData>();
    //    public int? LogoId { get; set; }
    //    public string Name { get; set; }
    //    public string Address { get; set; }
    //    public string NameLatin { get; set; }
    //    public string AddressLatin { get; set; }
    //    public string Phone { get; set; }
    //    public string Email { get; set; }
    //    //public PostingSources Source { get; set; } = PostingSources.Manual;
    //    /// <summary>
    //    /// TIN : Tax Identification Number.
    //    /// </summary>
    //    public string TIN { get; set; }
    //    /// <summary>
    //    /// MoCRef : Ministry Of Commerce Registration Number.
    //    /// </summary>
    //    public string MoCRefNo { get; set; }
    //    /// <summary>
    //    /// Company Based Currency Id.
    //    /// </summary>
    //    public int CurrencyId { get; set; }
    //    public CompanyTypes CompanyTypes { get; set; } = CompanyTypes.General;
    //    public  Currency Currency { get; set; }

    //    public string Logo
    //    {
    //        get
    //        {
    //            return null;
    //            //var filePath = "";
    //            //if (!MediaCacheds.ContainsKey(this.Id))
    //            //{
    //            //    MediaCacheds[this.Id] = MediaDataLogic.Instance.Find(this.LogoId ?? 0);
    //            //}
    //            //var media = MediaCacheds[this.Id];
    //            //if (media == null)
    //            //{
    //            //    var path = Path.Combine(Application.StartupPath, "Picture");
    //            //    var imgName = "null_logo.png";
    //            //    if (!Directory.Exists(path)) Directory.CreateDirectory(path);
    //            //    filePath = Path.Combine(Application.StartupPath, AppMode.ResourcePath() + imgName);
    //            //    if (!File.Exists(Path.Combine(path, imgName)))
    //            //    {
    //            //        File.Copy(filePath, Path.Combine(path, imgName), true);
    //            //    }
    //            //    return Path.Combine(path, imgName);
    //            //}
    //            //filePath = Path.Combine(Application.StartupPath, "Picture", media.Name);
    //            //if (!File.Exists(filePath))
    //            //{
    //            //    try
    //            //    {
    //            //        File.WriteAllBytes(filePath, media.Data);
    //            //    }
    //            //    catch (Exception)
    //            //    {
    //            //    }
    //            //}
    //            //return filePath;
    //        }

    //    }
    //    public CompanySetting Setting()
    //    {
    //        return new CompanySetting(this.Id);
    //    }
    //    public override string ToString()
    //    {
    //        return $"{this.Name} ({this.NameLatin})";
    //    }
         
    //}

    public enum TransactionType
    {
        Bill = 1,
        Invoice,
        Transaction,
        CreditNote
    }
    public enum EFilingReport
    {
        Summary = 1,
        Detail,
        IndividualVendor,
        OverSeaVedor,
        CreditNote
    }
    public enum CustomerType
    {
        Taxable = 1,
        TaxFree,
        OverseasCompanies
    }
}
