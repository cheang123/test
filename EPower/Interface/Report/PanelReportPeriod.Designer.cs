﻿using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface
{
    partial class PanelReportPeriod
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cboPeriod = new System.Windows.Forms.ComboBox();
            this.dtDate1 = new System.Windows.Forms.DateTimePicker();
            this.dtDate2 = new System.Windows.Forms.DateTimePicker();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.cboPeriod);
            this.flowLayoutPanel1.Controls.Add(this.dtDate1);
            this.flowLayoutPanel1.Controls.Add(this.dtDate2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(340, 35);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // cboPeriod
            // 
            this.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPeriod.FormattingEnabled = true;
            this.cboPeriod.Items.AddRange(new object[] {
            " ប្រចាំថ្ងៃ",
            "ប្រចាំខែ",
            "ប្រចាំឆ្នាំ",
            "លំអិត"});
            this.cboPeriod.Location = new System.Drawing.Point(3, 4);
            this.cboPeriod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cboPeriod.Name = "cboPeriod";
            this.cboPeriod.Size = new System.Drawing.Size(121, 27);
            this.cboPeriod.TabIndex = 0;
            this.cboPeriod.SelectedIndexChanged += new System.EventHandler(this.cboPeriod_SelectedIndexChanged);
            // 
            // dtDate1
            // 
            this.dtDate1.CustomFormat = "yyyy-MM-dd";
            this.dtDate1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtDate1.Location = new System.Drawing.Point(130, 3);
            this.dtDate1.Name = "dtDate1";
            this.dtDate1.Size = new System.Drawing.Size(100, 27);
            this.dtDate1.TabIndex = 1;
            // 
            // dtDate2
            // 
            this.dtDate2.CustomFormat = "yyyy-MM-dd";
            this.dtDate2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtDate2.Location = new System.Drawing.Point(236, 3);
            this.dtDate2.Name = "dtDate2";
            this.dtDate2.Size = new System.Drawing.Size(100, 27);
            this.dtDate2.TabIndex = 2;
            // 
            // PanelReportPeriod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "PanelReportPeriod";
            this.Size = new System.Drawing.Size(340, 35);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FlowLayoutPanel flowLayoutPanel1;
        public ComboBox cboPeriod;
        public DateTimePicker dtDate1;
        public DateTimePicker dtDate2;
    }
}
