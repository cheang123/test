﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.Report
{
    public partial class DialogReportCustomerUsageSummaryOption : ExDialog
    {
        public DialogReportCustomerUsageSummaryOption()
        {
            InitializeComponent();
            bind();
        }

        #region Method
        private void bind()
        {
            UIHelper.SetDataSourceToComboBox(cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.All_CUSTOMER_GROUP);
            UIHelper.SetDataSourceToComboBox(cboCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            UIHelper.SetDataSourceToComboBox(cboPrice, Lookup.GetPrices(), Resources.ALL_PRICE);
        }
        #endregion

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int groupId = DataHelper.ParseToInt(cboCustomerGroup.SelectedValue.ToString());
            var type = DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                                    .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId)
                                    .OrderBy(x => x.DESCRIPTION);

            UIHelper.SetDataSourceToComboBox(cboCustomerConnection, type, Resources.ALL_CONNECTION_TYPE);
            if (cboCustomerConnection.DataSource != null)
            {
                cboCustomerConnection.SelectedIndex = 0;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}
