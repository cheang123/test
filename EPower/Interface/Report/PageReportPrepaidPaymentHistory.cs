﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EPower.Interface.Report
{
    public partial class PageReportPrepaidPaymentHistory : UserControl
    {
        CrystalReportHelper ch = null;
        public PageReportPrepaidPaymentHistory()
        {
            InitializeComponent();
            bindBillingCycle();
            viewer.DefaultView();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        public CrystalReportHelper ViewReportPrepaidPaymentHistory(int intRunID, DateTime month)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportPrepaidCreditMonthly.rpt");
            c.SetParameter("@RUN_PREPAID_CREDIT_ID", intRunID);
            c.SetParameter("@MONTH", month);
            return c;
        }

        private void viewReport()
        {
            this.ch = ViewReportPrepaidPaymentHistory((int)cboBillingCycle.SelectedValue, dtpRunYear.Value);
            this.viewer.ReportSource = ch.Report;
        }

        private void bindBillingCycle()
        {
            var q = from b in DBDataContext.Db.TBL_BILLING_CYCLEs
                    join r in DBDataContext.Db.TBL_RUN_BILLs on b.CYCLE_ID equals r.CYCLE_ID
                    where b.IS_ACTIVE
                          && DBDataContext.Db.TBL_RUN_PREPAID_CREDITs.Any(x => x.RUN_ID == r.RUN_ID)
                    select new
                    {
                        b.CYCLE_ID,
                        b.CYCLE_NAME
                    };
            UIHelper.SetDataSourceToComboBox(cboBillingCycle, q.Distinct()._ToDataTable(), Resources.ALL_CYCLE);
            cboBillingCycle.SelectedIndex = 0;
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }

        private void sendMail()
        {
            try
            {
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + ch.ReportName + ".xls").FullName;
                ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
    }
}
