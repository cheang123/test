﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Interface.Report;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportCustomer : Form
    {
        CrystalReportHelper ch = null;
        DialogReportCustomerOption dialogOption = new DialogReportCustomerOption();
        public PageReportCustomer()
        {
            InitializeComponent();
            viewer.DefaultView();

            DateTime now = DBDataContext.Db.GetSystemDate();
            this.dtpByMonth.Value = new DateTime(now.Year, now.Month, 1);
            this.cboReport.SelectedIndex = -1;
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));

            DataRow drReportCustomer = dtReport.NewRow();
            drReportCustomer["REPORT_ID"] = 1;
            drReportCustomer["REPORT_NAME"] = Resources.REPORT_CUSTOMERS;
            dtReport.Rows.Add(drReportCustomer);

            DataRow drReportCustomerMerge = dtReport.NewRow();
            drReportCustomerMerge["REPORT_ID"] = 2;
            drReportCustomerMerge["REPORT_NAME"] = Resources.REPORT_CUSTOMER_MERGE;
            dtReport.Rows.Add(drReportCustomerMerge);

            DataRow drReportCustomerMergeStopUsing = dtReport.NewRow();
            drReportCustomerMergeStopUsing["REPORT_ID"] = 3;
            drReportCustomerMergeStopUsing["REPORT_NAME"] = Resources.REPORT_CUSTOMER_MERGE_STOP_USING;
            dtReport.Rows.Add(drReportCustomerMergeStopUsing);

            DataRow drReportCustomerByTransformer = dtReport.NewRow();
            drReportCustomerByTransformer["REPORT_ID"] = 4;
            drReportCustomerByTransformer["REPORT_NAME"] = Resources.REPORT_CUSTOMER_BY_TRANSFORMER;
            dtReport.Rows.Add(drReportCustomerByTransformer);

            if (DataHelper.ParseToBoolean(Method.Utilities[Utility.ARAKAWA]))
            {
                DataRow drReportCustomerArakawa = dtReport.NewRow();
                drReportCustomerArakawa["REPORT_ID"] = 5;
                drReportCustomerArakawa["REPORT_NAME"] = Resources.REPORT_CUSTOMER_ARAKAWA;
                dtReport.Rows.Add(drReportCustomerArakawa);
            }

            UIHelper.SetDataSourceToComboBox(cboReport, dtReport);
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
            Visible();
        }

        public CrystalReportHelper ViewReportCustomers(int report,int intPrice, int intCycle, int intCustomerGroupType, int intCustConnectionType, int intArea
            , DateTime dtMonth, string strPrice, string strCycle, string strCustConnectionType, string strArea, bool withPositionInBox, int cusStatusId, string cusStatusName, string strCustomerGroup, bool Ismarketvendor,int cusAmpereId, string cusAmpereName)
        {
            var reportName="";
            if (report == 0)
            {
                reportName = withPositionInBox ? "ReportCustomers.PositionInBox.rpt" : "ReportCustomers.rpt";
            }
            else if (report == 1)
            {
                reportName = "ReportCustomerMerge.rpt";
            }
            else if (report==2)
            {
                reportName = "ReportCustomerMergeStopUsing.rpt";
            }
            else if (report == 3)
            {
                reportName = "ReportSummaryCustomerByTransformer.rpt";
            }
            else if( report == 4)
            {
                reportName = "ReportCustomersArakawa.rpt";
            }
            CrystalReportHelper c = new CrystalReportHelper(reportName);
            c.SetParameter("@PRICE_ID", intPrice);
            c.SetParameter("@CYCLE_ID", intCycle);
            c.SetParameter("@CUSTOMER_GROUP_TYPE_ID", intCustomerGroupType);
            c.SetParameter("@CUSTOMER_CONNECTION_TYPE_ID", intCustConnectionType);
            c.SetParameter("@AREA_ID", intArea);
            c.SetParameter("@MONTH", dtMonth);
            if (report == 0)
            {
                c.SetParameter("@CUS_STATUS_ID", cusStatusId);
                c.SetParameter("@CUS_STATUS_NAME", cusStatusName);
                c.SetParameter("@IS_MARKET_VENDOR", Ismarketvendor);
                c.SetParameter("@AMPERE_ID", cusAmpereId);
                c.SetParameter("@AMPERE_NAME", cusAmpereName);
            }

            else if( report == 4)
            {
                c.SetParameter("@CUS_STATUS_ID", cusStatusId);
                c.SetParameter("@CUS_STATUS_NAME", cusStatusName);
                c.SetParameter("@AMPERE_ID", cusAmpereId);
                c.SetParameter("@AMPERE_NAME", cusAmpereName);
            }

            c.SetParameter("@PRICE", strPrice);
            c.SetParameter("@CYCLE", strCycle);
            c.SetParameter("@CUSTOMER_GROUP_NAME", strCustomerGroup);
            c.SetParameter("@CUSTOMER_CONNECTION_TYPE_NAME", strCustConnectionType);
            c.SetParameter("@AREA", strArea);
            return c;
        }

        private void viewReport()
        {
            try
            {
                DateTime dt = new DateTime(dtpByMonth.Value.Year, dtpByMonth.Value.Month, 1);
                this.ch = ViewReportCustomers((int)cboReport.SelectedIndex
                        , (int)dialogOption.cboPrice.SelectedValue
                        , (int)dialogOption.cboBillingCycle.SelectedValue
                        , (int)dialogOption.cboCustomerGroup.SelectedValue
                        , (int)dialogOption.cboCustomerConnectionType.SelectedValue
                        , (int)dialogOption.cboArea.SelectedValue, dt
                        , dialogOption.cboPrice.Text
                        , dialogOption.cboBillingCycle.Text
                        , dialogOption.cboCustomerConnectionType.Text
                        , dialogOption.cboArea.Text
                        , this.chkPOSITION_IN_BOX.Checked
                        , (int)dialogOption.cboCustomerStatus.SelectedValue
                        , dialogOption.cboCustomerStatus.Text
                        , dialogOption.cboCustomerGroup.Text
                        , dialogOption.chkIsmarketVendor.Checked
                        , (int)dialogOption.cboAmpare.SelectedValue
                        , dialogOption.cboAmpare.Text
                        );
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void chkWithPositionInBox_CheckedChanged(object sender, EventArgs e)
        {

        }
        private void cboSaveState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo.SelectedIndex == -1)
            {
                return;
            }
            cbo.Tag = cbo.SelectedValue;
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboReport.SelectedIndex == -1)
            {
                return;
            }
            if (cboReport.SelectedIndex == 0)
            {
                chkPOSITION_IN_BOX.Visible = true;
                dtpByMonth.Visible = true;
                btnSetup.Location = new System.Drawing.Point(252, 6);
            }
            else if (cboReport.SelectedIndex == 1)
            {
                chkPOSITION_IN_BOX.Visible = false;
                dtpByMonth.Visible = true;
                btnSetup.Location = new System.Drawing.Point(252, 6);
            }
            else if (cboReport.SelectedIndex == 2)
            {
                chkPOSITION_IN_BOX.Visible = false;
                dtpByMonth.Visible = true;
                btnSetup.Location = new System.Drawing.Point(252, 6);
            }
            else if (cboReport.SelectedIndex == 3)
            {
                chkPOSITION_IN_BOX.Visible = false;
                dtpByMonth.Visible = false;
                btnSetup.Location = new System.Drawing.Point(159,6);
            }
            else if(cboReport.SelectedIndex == 4)
            {
                chkPOSITION_IN_BOX.Visible = false;
                dtpByMonth.Visible = true;
                btnSetup.Location = new System.Drawing.Point(252, 6);
            }
        }

        private void cboCustomerStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo.SelectedIndex == -1)
            {
                return;
            }
            cbo.Tag = cbo.SelectedValue;
        }

        private void dtpByMonth_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            int intReportId = DataHelper.ParseToInt(cboReport.SelectedValue.ToString());
            if (intReportId == 1)
            {
                dialogOption.cboCustomerStatus.Visible = dialogOption.lblCUSTOMER_STATUS.Visible
                    = dialogOption.cboAmpare.Visible = dialogOption.lblAmpare.Visible
                    = dialogOption.chkIsmarketVendor.Visible = true;
                dialogOption.Width = 319;
                dialogOption.Height = 331;
                dialogOption.panel1.Location = new System.Drawing.Point(2, 267);
                dialogOption.btnOK.Location = new System.Drawing.Point(151, 272);
                dialogOption.btnClose.Location = new System.Drawing.Point(231, 272);
            }
            else if (intReportId == 3 || intReportId==2)
            {
                dialogOption.cboCustomerStatus.Visible = dialogOption.lblCUSTOMER_STATUS.Visible
                    = dialogOption.cboAmpare.Visible = dialogOption.lblAmpare.Visible
                    = dialogOption.chkIsmarketVendor.Visible = false;
                dialogOption.Width = 319;
                dialogOption.Height = 250;
                dialogOption.panel1.Location = new System.Drawing.Point(2, 176);
                dialogOption.btnOK.Location = new System.Drawing.Point(151, 181);
                dialogOption.btnClose.Location = new System.Drawing.Point(231, 181);
            }

            else if(intReportId ==4 )
            {
                dialogOption.cboCustomerStatus.Visible = dialogOption.lblCUSTOMER_STATUS.Visible
                   = dialogOption.cboAmpare.Visible = dialogOption.lblAmpare.Visible
                   = dialogOption.chkIsmarketVendor.Visible = false;
                dialogOption.Width = 319;
                dialogOption.Height = 250;
                dialogOption.panel1.Location = new System.Drawing.Point(2, 176);
                dialogOption.btnOK.Location = new System.Drawing.Point(151, 181);
                dialogOption.btnClose.Location = new System.Drawing.Point(231, 181);
            }
            else if(intReportId == 5)
            {
                dialogOption.chkIsmarketVendor.Visible = false;
            }
            
            if (dialogOption.ShowDialog() == DialogResult.OK)
            {
                Runner.Run(this.viewReport);
                Visible();
            }
        }

        private void btnCSV_Click(object sender, EventArgs e)
        {
            if(ch == null)
            {
                return;
            }

            CsvHelper.ExportCsv(ch);
        }

        private void Visible()
        {
            if (DataHelper.ParseToBoolean(Method.Utilities[Utility.ARAKAWA]) && cboReport.SelectedIndex == 4)
            {
                btnCSV.Visible = true;
            }
            else
            {
                btnCSV.Visible = false;
            }
        }
    }
}
