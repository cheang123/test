﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using Process = System.Diagnostics.Process;

namespace EPower.Interface
{
    public partial class DialogReportTaxDetail: ExDialog
    {

        List<string> fileReport = new List<string>();
        CrystalReportHelper chNewCus = null;
        CrystalReportHelper chCusUsage = null;

        TBL_RUN_BILL _objRun = new TBL_RUN_BILL();

        public TBL_RUN_BILL RunHistory
        {
            get { return _objRun; }
            set { _objRun = value; }
        }

        public DialogReportTaxDetail(TBL_RUN_BILL objRun)
        {
            InitializeComponent();
            _objRun = objRun;
            getFile();
            this.Text += objRun.BILLING_MONTH.ToString("MM - yyyy");
            this.viewerNewCus.ApplyDefaultFormat();
            this.viewerCusUsage.ApplyDefaultFormat();
        }  

        public CrystalReportHelper ViewReportNewCustomer()
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportTaxDetail.rpt");
            DateTime start = RunHistory.BILLING_MONTH.Date;
            DateTime end = start.AddMonths(1).AddSeconds(-1);
            c.SetParameter("@START", start);
            c.SetParameter("@END", end); 
            return c;
        } 

        public CrystalReportHelper ViewReportCusUsage()
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportTaxCustomerUsage.rpt");
            c.SetParameter("@MONTH", RunHistory.BILLING_MONTH.Date);
            return c;
        }

        private void viewReport()
        {
            chNewCus= ViewReportNewCustomer();
            this.viewerNewCus.ReportSource = chNewCus.Report; 
            this.viewerNewCus.ViewReport();

            chCusUsage = ViewReportCusUsage();
            this.viewerCusUsage.ReportSource = chCusUsage.Report;
            this.viewerCusUsage.ViewReport(); 
        }


        private void sendMail()
        {
            try
            {
                // if user already view report
                // not need to view it any more!
                if (this.chNewCus == null||this.chCusUsage==null)
                {
                    this.viewReport();
                }
                // export it to excel and add it to mail list.  
                string exportPath0 = new FileInfo(fileReport[0]).FullName;
                string exportPath1 = new FileInfo(fileReport[1]).FullName;
                this.chNewCus.ExportToExcel(exportPath0); 
                this.chCusUsage.ExportToExcel(exportPath1);
                DialogSendMail.Instance.Add(fileReport[0],exportPath0);
                DialogSendMail.Instance.Add(fileReport[1],exportPath1);
                DialogSendMail.Instance.ShowDialog(); 
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        } 

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void getFile()
        {
            fileReport.Clear();
            fileReport.Add(string.Concat(Application.StartupPath,"\\", Settings.Default.PATH_TEMP, "Sale Meter", ".xls"));
            fileReport.Add(string.Concat(Application.StartupPath, "\\",Settings.Default.PATH_TEMP,"Income Monthly",".xls"));
        }



        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (this.chNewCus == null || this.chCusUsage == null)
            {
                this.viewReport();
            }
            bool resul=false;
            // export it to excel and add it to mail list. 
            this.chNewCus.ExportToExcel(fileReport[0]);
            this.chCusUsage.ExportToExcel(fileReport[1]);

            SaveFileDialog sdig = new SaveFileDialog();
            sdig.Filter= "Excel 2007|*.xlsx|Excel 2003-97|*.xls";
            sdig.FileName=string.Format("Meter Sale and Income Monthly {0:MMM-yyyy}.xlsx",RunHistory.BILLING_MONTH);
            if(sdig.ShowDialog()== DialogResult.OK)
            { 
               resul = SoftTech.Helper.ExcelEngine.CombineWorkBooks(sdig.FileName, fileReport.ToArray(), false);  
            }
            if (resul)
            {
                Process.Start("explorer.exe", "/select," + sdig.FileName);
            }
            
        }

        private void DialogReportTaxDetail_Load(object sender, EventArgs e)
        { 
            this.viewReport();  
        }
         
    }
}