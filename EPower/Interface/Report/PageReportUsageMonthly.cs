﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System.Data;
using System.Linq;

namespace EPower.Interface
{
    
    public partial class PageReportUsageMonthly : Form
    {
        enum REPORT
        {
            REPORT_VERIFY_USAGE = 1,
            REPORT_USAGE_MONTHLY = 2
        }
        CrystalReportHelper ch = null; 
        public PageReportUsageMonthly()
        {
            InitializeComponent();
            viewer.DefaultView();

            this.txtValue2.Visible = false;
            this.txtValue1.Text = "0";
            this.cboOperator.SelectedIndex = 0;

            DateTime dt = DBDataContext.Db.GetSystemDate();
            DateTime dateTime = new DateTime(dt.Year, dt.Month, 1);

            diag.dtpM1.Value = dateTime.AddMonths(-1);
            diag.dtpM2.Value = dateTime.AddMonths(-2);
            diag.dtpM3.Value = dateTime.AddMonths(-3);

            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(1, Resources.REPORT_CUSTOMER_VERIFY_USAGE);
            dtReport.Rows.Add(2, Resources.REPORT_USAGE_MONTHLY);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
            this.cboReport.SelectedValue = 2;

            TBL_BILLING_CYCLE objBillingCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.FirstOrDefault(row => row.IS_ACTIVE);
            if (objBillingCycle != null)
            {
                DateTime datCurrentMonth = Method.GetNextBillingMonth(objBillingCycle.CYCLE_ID).AddMonths(-1);
                this.dtpByMonth.Value = new DateTime(datCurrentMonth.Year, datCurrentMonth.Month, 1);
            }
        }
         
        private void btnViewReport_Click(object sender, EventArgs e)
        { 
            Runner.Run(this.viewReport);
        }  
        private void viewReport()
        {
            try
            {
                this.ch = ViewReportReportUsageMonthly(dtpByMonth.Value, getFilter());
                this.viewer.ReportSource = ch.Report;

            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

        public CrystalReportHelper ViewReportReportUsageMonthly(DateTime dtMonth,string strFilter)
        {
            string orderBy = "";
            if ((int)diag.cboOrderType.SelectedValue == 1)
            {
                orderBy = " ORDER BY a.AREA_CODE,p.POLE_CODE,b.BOX_CODE,cm.POSITION_IN_BOX,c.CUSTOMER_CODE ";
            }
            else if ((int)diag.cboOrderType.SelectedValue == 2)
            {
                orderBy = " ORDER BY MONTH_TOTAL_USAGE DESC";
            }
            else if ((int)diag.cboOrderType.SelectedValue == 3)
            {
                orderBy = " ORDER BY MONTH_TOTAL_USAGE ASC";
            }
            DateTime d1=diag.dtpM1.Value;
            DateTime d2=diag.dtpM2.Value;
            DateTime d3=diag.dtpM3.Value;
            string reportName = (int)cboReport.SelectedValue == (int)REPORT.REPORT_VERIFY_USAGE ? "ReportCustomerVerifyUsage.rpt" : "ReportUsageMonthly.rpt";
            CrystalReportHelper c = new CrystalReportHelper(reportName);
            c.SetParameter("@MONTH", new DateTime(dtMonth.Year, dtMonth.Month, 1));
            c.SetParameter("@M1",new DateTime(d1.Year,d1.Month,1));
            c.SetParameter("@M2", new DateTime(d2.Year, d2.Month, 1));
            c.SetParameter("@M3", new DateTime(d3.Year, d3.Month, 1));
            c.SetParameter("@CYCLE_ID", (int)cboCycle.SelectedValue);
            c.SetParameter("@CYCLE_NAME", this.cboCycle.Text);
            c.SetParameter("@AREA_ID", (int)cboArea.SelectedValue);
            c.SetParameter("@AREA_NAME", this.cboArea.Text);
            c.SetParameter("@FILTER", strFilter);
            c.SetParameter("@ORDER_BY",orderBy);
            if((int)cboReport.SelectedValue == (int)REPORT.REPORT_USAGE_MONTHLY)
            {
                c.SetParameter("@CUS_STATUS_ID", (int)diag.cboCustomerStatus.SelectedValue);
                c.SetParameter("@CUS_STATUS_NAME", diag.cboCustomerStatus.Text);
                c.SetParameter("@PRICE_ID", (int)diag.cboPRICE.SelectedValue);
                c.SetParameter("@PRICE_NAME", diag.cboPRICE.Text);
                c.SetParameter("@CUSTOMER_GROUP_NAME", diag.cboCustomerGroup.Text);
                c.SetParameter("@CUSTOMER_GROUP_TYPE_ID", (int)diag.cboCustomerGroup.SelectedValue);
                c.SetParameter("@CUSTOMER_CONNECTION_TYPE_ID", (int)diag.cboConnectionType.SelectedValue);
                c.SetParameter("@CUSTOMER_CONNECTION_TYPE_NAME", diag.cboConnectionType.Text);
            }
            return c;
        }
        private string getFilter()
        {
            string filter = " WHERE r.MONTH_TOTAL_USAGE " + cboOperator.Text + " " + DataHelper.ParseToInt(txtValue1.Text).ToString() + " ";
            if (cboOperator.Text == "between")
            {
                filter += " AND " + DataHelper.ParseToInt(txtValue2.Text).ToString();
            } 
            string option =(int)diag.cboFilterType.SelectedValue == 1?"{0}_TOTAL_USAGE":"{0}_PERCENTAGE";
            if (diag.cboOperatorM1.Text!="")
            {
                if (diag.cboOperatorM1.Text == "+-")
                {
                    filter += " AND " + string.Format(option, "M1") + " NOT BETWEEN " + -1*DataHelper.ParseToInt(diag.txtVal1M1.Text) + " AND " + DataHelper.ParseToInt(diag.txtVal1M1.Text).ToString();
                }
                else
                {
                    filter += " AND  " + string.Format(option, "M1") + " " + diag.cboOperatorM1.Text + " " + DataHelper.ParseToInt(diag.txtVal1M1.Text).ToString() + " ";
                    if (diag.cboOperatorM1.Text == "between")
                    {
                        filter += " AND " + DataHelper.ParseToInt(diag.txtVal2M1.Text).ToString();
                    } 
                }
            }
            if (diag.cboOperatorM2.Text != "")
            {
                if (diag.cboOperatorM2.Text == "+-")
                {
                    filter += " AND " + string.Format(option, "M2") + " NOT BETWEEN " + -1 * DataHelper.ParseToInt(diag.txtVal1M2.Text) + " AND " + DataHelper.ParseToInt(diag.txtVal1M2.Text).ToString();
                }
                else
                {
                    filter += " AND  " + string.Format(option, "M2") + " " + diag.cboOperatorM2.Text + " " + DataHelper.ParseToInt(diag.txtVal1M2.Text).ToString() + " ";
                    if (diag.cboOperatorM2.Text == "between")
                    {
                        filter += " AND " + DataHelper.ParseToInt(diag.txtVal2M2.Text).ToString();
                    } 
                } 
            }
            if (diag.cboOperatorM3.Text != "")
            {
                if (diag.cboOperatorM3.Text == "+-")
                {
                    filter += " AND " + string.Format(option, "M3") + " NOT BETWEEN " + -1 * DataHelper.ParseToInt(diag.txtVal1M3.Text) + " AND " + DataHelper.ParseToInt(diag.txtVal1M3.Text).ToString();
                }
                else
                {
                    filter += " AND  " + string.Format(option, "M3") + " " + diag.cboOperatorM3.Text + " " + DataHelper.ParseToInt(diag.txtVal1M3.Text).ToString() + " ";
                    if (diag.cboOperatorM3.Text == "between")
                    {
                        filter += " AND " + DataHelper.ParseToInt(diag.txtVal2M3.Text).ToString();
                    } 
                }
            }

            return filter;
        } 
        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }
        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }
        private void cboOperator_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtValue2.Visible = this.cboOperator.Text == "between";
            btnSETUP.Location = this.cboOperator.Text == "between" ? new Point(720, 5) : new Point(676, 5);
        } 
        private void txtValue2_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void PageReportUsageMonthly_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            { 
                UIHelper.SetDataSourceToComboBox(this.cboCycle,Lookup.GetBillingCycles(),Resources.ALL_CYCLE);
                UIHelper.SetDataSourceToComboBox(this.cboArea, Lookup.GetAreas(),Resources.ALL_AREA); 
            }
        }

        private void cboSaveState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo.SelectedIndex==-1)
            {
                return;
            }
            cbo.Tag = cbo.SelectedValue;
        }
        DialogReportUsageMonthlyOption diag = new DialogReportUsageMonthlyOption();
        private void btnOption_Click(object sender, EventArgs e)
        {
            if ((int)cboReport.SelectedValue == (int)REPORT.REPORT_VERIFY_USAGE)
            {
                diag.cboCustomerStatus.Visible = diag.cboCustomerGroup.Visible = diag.cboConnectionType.Visible = diag.lblCUSTOMER_CONNECTION_TYPE.Visible = diag.lblCUSTOMER_STATUS.Visible = diag.lblCUSTOMER_GROUP.Visible = diag.lblPRICE.Visible = diag.cboPRICE.Visible = false;
            }
            else
            {
                diag.cboCustomerStatus.Visible = diag.cboCustomerGroup.Visible = diag.cboConnectionType.Visible = diag.lblCUSTOMER_CONNECTION_TYPE.Visible = diag.lblCUSTOMER_STATUS.Visible = diag.lblCUSTOMER_GROUP.Visible = diag.lblPRICE.Visible = diag.cboPRICE.Visible = true;
            }
            if (diag.ShowDialog() == DialogResult.OK)
            {
                viewReport();
            }
        }

        private void dtpByMonth_ValueChanged(object sender, EventArgs e)
        {
            diag.dtpM1.Value = dtpByMonth.Value.AddMonths(-1);
            diag.dtpM2.Value = dtpByMonth.Value.AddMonths(-2);
            diag.dtpM3.Value = dtpByMonth.Value.AddMonths(-3);
        }

        private void txtValue1_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
 
    }
}
