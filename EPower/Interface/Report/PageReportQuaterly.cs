﻿using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageReportQuaterly : Form
    {
        CrystalReportHelper ch = null;
        public int _year = 0;
        public int _quarter = 0;

        public PageReportQuaterly()
        {
            InitializeComponent();

            viewer.DefaultView();

            var dtYears = new DataTable();
            dtYears.Columns.Add("Year", typeof(int));
            dtYears.Columns.Add("Name", typeof(string));
            var y = DateTime.Now.Year;
            while (y > 2008)
            {
                var row = dtYears.NewRow();
                row["Year"] = y;
                row["Name"] = y.ToString();
                dtYears.Rows.Add(row);
                y--;
            }

            UIHelper.SetDataSourceToComboBox(cboYear, dtYears);

            var now = DBDataContext.Db.GetSystemDate(); 
            cboYear.SelectedValue = now.Year; 
            cboQuarter.SelectedIndex = ((int)((now.Month-1)/3)+1)-1;

        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            if (this.cboYear.SelectedIndex == -1)
            {
                MsgBox.ShowInformation(Resources.MS_SELECT_YEAR);
                this.cboYear.Focus();
                return;
            }
            Runner.Run(this.viewReport);
        } 

        private void viewReport()
        {
            try
            {
                this.ch = OpenReport(diagOptions);
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.ShowWarning(Resources.YOU_CANNOT_VIEW_REPORT, Resources.WARNING);
                MsgBox.LogError(ex);
            }
        }
         
        public static CrystalReportHelper OpenReport(DialogReportQuarterOptions diag)
        {
            var doc = new CrystalReportHelper("ReportQuarter.rpt",
                "GEN_FAC","GEN","DIS_FAC","TRANFO","CUS_CSM","CUS_LCS","SLD_CSM","SLD_LCS","PWR_SRC","PWR_PUR","PWR_CVR"
            );
            var year = (int)diag.cboYear.SelectedValue;
            var quarter = DataHelper.ParseToInt(diag.cboQuarter.Text);
            
          
            var date = new DateTime(year, 1, 1);
            date = date.AddMonths((quarter - 1) * 3);
            doc.SetParameter("@CREATE_DATE", diag.dtpCreateDate.Value);
            doc.SetParameter("@DATE", date.Date);
            doc.SetParameter("@SHOW_COVER", diag.chkREPORT_COVER.Checked);
            doc.SetParameter("@SHOW_GENERATION_FACILITY", diag.chkTABLE_GENERATION_FACILITY.Checked);
            doc.SetParameter("@SHOW_POWER_GENERATION", diag.chkTABLE_POWER_GENERATION.Checked);
            doc.SetParameter("@SHOW_DISTRIBUTION_FACILITY", diag.chkTABLE_DISTRIBUTION_FACILTITY.Checked);
            doc.SetParameter("@SHOW_TRANSFORMER", diag.chkTABLE_TRANSFORMER.Checked);
            doc.SetParameter("@SHOW_CUSTOMER_CONSUMER", diag.chkTABLE_CUSTOMER_CONSUMER.Checked);
            doc.SetParameter("@SHOW_CUSTOMER_LICENSEE", diag.chkTABLE_CUSTOMER_LICENSEE.Checked);
            doc.SetParameter("@SHOW_SOLD_CONSUMER", diag.chkTABLE_SOLD_CONSUMER.Checked);
            doc.SetParameter("@SHOW_SOLD_LICENSEE", diag.chkTABLE_SOLD_LICENSEE.Checked);
            doc.SetParameter("@SHOW_POWER_SOURCE", diag.chkTABLE_POWER_SOURCE.Checked);
            doc.SetParameter("@SHOW_POWER_PURCHASE", diag.chkTABLE_POWER_PURCHASE.Checked);
            doc.SetParameter("@SHOW_POWER_COVERAGE", diag.chkTABLE_POWER_COVERAGE.Checked);
            //doc.SetParameter("@SHOW_POWER_OUTSIZE_COVERAGE", diag.chkTABLE_POWER_COVERAGE.Checked);
            return doc;
        }

        private void sendMail()
        {
            try
            {
                if (ch == null)
                {
                    this.viewReport();
                }
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowWarning(Resources.YOU_CANNOT_SEND_EMAIL, Resources.WARNING);
                MsgBox.LogError(ex);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        DialogReportQuarterOptions diagOptions = new DialogReportQuarterOptions(true);
        private void btnSetting_Click(object sender, EventArgs e)
        {
            diagOptions.cboYear.SelectedValue = this.cboYear.SelectedValue;
            diagOptions.cboQuarter.Text = this.cboQuarter.Text;
            if (diagOptions.ShowDialog() == DialogResult.OK)
            {
                this.cboYear.SelectedValue = diagOptions.cboYear.SelectedValue;
                this.cboQuarter.Text = diagOptions.cboQuarter.Text;

                viewReport();
            }
        }

        private void cboYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            diagOptions.cboYear.SelectedValue = this.cboYear.SelectedValue;
        }

        private void cboQuarter_SelectedIndexChanged(object sender, EventArgs e)
        {
            diagOptions.cboQuarter.Text = this.cboQuarter.Text;
        }
    } 
}
