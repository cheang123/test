﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageReportPower : Form
    {
        CrystalReportHelper ch = null;

        public PageReportPower()
        {
            InitializeComponent();
            viewer.DefaultView();
            bind();
            this.chkSHOW_POWER_ADJUST.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ADJUST_POWER_PRODUCTION]); 
            this.cboReport.SelectedIndex = 1;
        }

        private void bind()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(1, Resources.REPORT_POWER_YEARLY);
            dtReport.Rows.Add(2, Resources.REPORT_POWER_MONTHLY);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        { 
            Runner.Run(this.viewReport);
        }
        public CrystalReportHelper ViewReportPowerYearly( int intYear, int intTransormer, string strTransormerCode)
        {
            return ViewReportPowerYearly(this.chkSHOW_POWER_ADJUST.Checked, intYear, intTransormer, strTransormerCode); 
        }

        public CrystalReportHelper ViewReportPowerYearly(bool isAdjustPower , int intYear, int intTransormer, string strTransormerCode)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportPowerYearly.rpt");
            c.SetParameter("@ADJUST", isAdjustPower);
            c.SetParameter("@YEAR", intYear);
            c.SetParameter("@TRANSFORMER_ID", intTransormer);
            c.SetParameter("@TRANSFORMER_CODE", strTransormerCode); 
            return c;
        } 

        public CrystalReportHelper ViewReportPowerMonthly(DateTime dtByMonth)
        {
            CrystalReportHelper c = new CrystalReportHelper("ReportPowerMonthly.rpt");
            c.SetParameter("@MONTH", new DateTime(dtByMonth.Year, dtByMonth.Month, 1).Date);
            return c;
        }

        private void viewReport()
        {
            try
            {
                if (this.cboReport.SelectedIndex == 1)
                {
                    this.ch = ViewReportPowerMonthly(new DateTime(dtpDate.Value.Year, dtpDate.Value.Month, 1));
                }
                else
                {
                    bool isAdjust = chkSHOW_POWER_ADJUST.Checked;
                    this.ch = ViewReportPowerYearly(isAdjust, dtpDate.Value.Date.Year, (int)cboTransformer.SelectedValue, cboTransformer.Text);
                }
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }


        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

 
        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dtpDate.CustomFormat = this.cboReport.SelectedIndex == 0 ? "yyyy" : "MM-yyyy"; 
            this.lblTransformer.Visible =  this.cboTransformer.Visible = this.cboReport.SelectedIndex == 0;
            this.chkSHOW_POWER_ADJUST.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ADJUST_POWER_PRODUCTION])
                                            && cboReport.SelectedIndex == 0;
        }

        private void cboTransformer_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.chkSHOW_POWER_ADJUST.Visible = this.cboTransformer.SelectedIndex == 0 && DataHelper.ParseToBoolean(Method.Utilities[Utility.ADJUST_POWER_PRODUCTION]);
            cboSaveState_SelectedIndexChanged(sender, e);
        }

        private void PageReportPower_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                UIHelper.SetDataSourceToComboBox(this.cboTransformer, DBDataContext.Db.TBL_TRANSFORMERs.Where(row => row.IS_ACTIVE), Resources.ALL_TRANSFORMER);
                cboTransformer.SelectedValue = cboTransformer.Tag;
            }
        }
        private void cboSaveState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo.SelectedIndex==-1)
            {
                return;
            }
            cbo.Tag = cbo.SelectedValue;
        }

    }
}
