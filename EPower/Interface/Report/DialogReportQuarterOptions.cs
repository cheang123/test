﻿using System;
using System.Data;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogReportQuarterOptions: ExDialog
    { 
        public DialogReportQuarterOptions(bool showAll )
        {
            InitializeComponent();

            var dtYears = new DataTable();
            dtYears.Columns.Add("Year", typeof(int));
            dtYears.Columns.Add("Name", typeof(string));
            var y = DateTime.Now.Year;
            while (y > 2008)
            {
                var row = dtYears.NewRow();
                row["Year"] = y;
                row["Name"] = y.ToString();
                dtYears.Rows.Add(row);
                y--;
            }

            UIHelper.SetDataSourceToComboBox(cboYear, dtYears);

            var now = DBDataContext.Db.GetSystemDate();
            cboYear.SelectedValue = now.Year;
            cboQuarter.SelectedIndex = ((int)((now.Month - 1) / 3) + 1) - 1;

            this.chkSHOW_HIDE_ALL.Checked = showAll;
            //this.CHK_SHOW_COVER.Checked = showAll;
            //this.CHK_SHOW_CUSTOMER_CONSUMER.Checked = showAll;
            //this.CHK_SHOW_CUSTOMER_LICENSEE.Checked = showAll;
            //this.CHK_SHOW_DISTRIBUTION_FACILTITY.Checked = showAll;
            //this.CHK_SHOW_GENERATION_FACILITY.Checked = showAll;
            //this.CHK_SHOW_POWER_COVERAGE.Checked = showAll;
            //this.CHK_SHOW_POWER_GENERATION.Checked = showAll;
            //this.CHK_SHOW_POWER_PURCHASE.Checked = showAll;
            //this.CHK_SHOW_POWER_SOURCE.Checked = showAll;
            //this.CHK_SHOW_SOLD_CONSUMER.Checked = showAll;
            //this.CHK_SHOW_SOLD_LICENSEE.Checked = showAll;
            //this.CHK_SHOW_TRANSFORMER.Checked = showAll; 
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void CHK_SHOW_HIDE_ALL_CheckedChanged(object sender, EventArgs e)
        {
            bool t = this.chkSHOW_HIDE_ALL.Checked;
            this.chkREPORT_COVER.Checked = t;
            this.chkTABLE_CUSTOMER_CONSUMER.Checked = t;
            this.chkTABLE_CUSTOMER_LICENSEE.Checked = t;
            this.chkTABLE_DISTRIBUTION_FACILTITY.Checked = t;
            this.chkTABLE_GENERATION_FACILITY.Checked = t;
            this.chkTABLE_POWER_COVERAGE.Checked = t;
            this.chkTABLE_POWER_GENERATION.Checked = t;
            this.chkTABLE_POWER_PURCHASE.Checked = t;
            this.chkTABLE_POWER_SOURCE.Checked = t;
            this.chkTABLE_SOLD_CONSUMER.Checked = t;
            this.chkTABLE_SOLD_LICENSEE.Checked = t;
            this.chkTABLE_TRANSFORMER.Checked = t; 
        }  
    }
}