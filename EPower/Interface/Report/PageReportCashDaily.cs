﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System.Data.Linq;

namespace EPower.Interface
{
    public partial class PageReportCashDaily : Form
    {
        #region Data
        TBL_USER_CASH_DRAWER _objOld = new TBL_USER_CASH_DRAWER();
        TBL_USER_CASH_DRAWER _objNew = new TBL_USER_CASH_DRAWER();
        
        CrystalReportHelper ch = new CrystalReportHelper();
        #endregion

        #region Contructor
        public enum ReportName
        {
            ReportCashDailyTotal=1,
            ReportCashDailyDetail=2,
            ReportCashDailySummary=3,
            ReportCountCash=4,
            ReportUserCashDrawerDaily=5
        }
        
        public PageReportCashDaily()
        {
            InitializeComponent();
          
            // grant permissions
            btnVIEW.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.REPORT_CASHINGDETAIL_LOOKREPORT);
            btnRE_OPEN_CASH_DRAWER.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.REPORT_CASHINGDETAIL_REOPENCASHDRAWER);

            this.dtpStart.Value = DBDataContext.Db.GetSystemDate();
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));

            DataRow drReportTotal = dtReport.NewRow();
            drReportTotal["REPORT_ID"] = 1;
            drReportTotal["REPORT_NAME"] = Resources.REPORT_CASH_DIALY_TOTAL;
            dtReport.Rows.Add(drReportTotal);

            DataRow drReportDetail = dtReport.NewRow();
            drReportDetail["REPORT_ID"] = 2;
            drReportDetail["REPORT_NAME"] = Resources.REPORT_CASH_DIALY_DETAIL;
            dtReport.Rows.Add(drReportDetail);

            DataRow drReportSummary = dtReport.NewRow();
            drReportSummary["REPORT_ID"] = 3;
            drReportSummary["REPORT_NAME"] = Resources.REPORT_CASH_DIALY_SUMMARY;
            dtReport.Rows.Add(drReportSummary);

            DataRow drReportCountCash = dtReport.NewRow();
            drReportCountCash["REPORT_ID"] = 4;
            drReportCountCash["REPORT_NAME"] = Resources.REPORT_COUNT_CASH;
            dtReport.Rows.Add(drReportCountCash);

            DataRow drReportUserCashDrawerDaily = dtReport.NewRow();
            drReportUserCashDrawerDaily["REPORT_ID"] = 5;
            drReportUserCashDrawerDaily["REPORT_NAME"] = Resources.REPORT_USER_CASH_DRAWER_DAILY;
            dtReport.Rows.Add(drReportUserCashDrawerDaily);

            UIHelper.SetDataSourceToComboBox(cboReportName, dtReport);
        }
        #endregion

        #region Events​ & Method
        private void btnViewReport_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0) return;
            Runner.Run(delegate()
            {
                this.viewReport();

                DialogReportViewer diag = new DialogReportViewer(this.ch.Report, "");
                diag.ShowDialog(); 
            });
        }
        private void viewReport()
        {
            try
            {
                if ((int)this.cboReportName.SelectedValue == (int)ReportName.ReportCashDailyTotal)
                {
                    this.viewReportTotal();
                }
                else if ((int)this.cboReportName.SelectedValue == (int)ReportName.ReportCashDailyDetail)
                {
                    this.viewReportDetail();
                }
                else if ((int)this.cboReportName.SelectedValue == (int)ReportName.ReportCashDailySummary)
                {
                    this.viewReportSummary();
                }
                else if ((int)this.cboReportName.SelectedValue == (int)ReportName.ReportCountCash)
                {
                    this.viewCountCash();
                }
                else if ((int)this.cboReportName.SelectedValue == (int)ReportName.ReportUserCashDrawerDaily)
                {
                    this.viewUserCashDrawerDaily();
                }
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }
        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void viewReportTotal()
        {
            if (this.dgv.SelectedRows.Count == 0) return;
            int userCashDrawerID = int.Parse(this.dgv.SelectedRows[0].Cells[USER_CASH_DRAWER_ID.Name].Value.ToString());
            TBL_USER_CASH_DRAWER objUserCashDrawer = DBDataContext.Db.TBL_USER_CASH_DRAWERs.FirstOrDefault(row => row.USER_CASH_DRAWER_ID == userCashDrawerID);
            if (objUserCashDrawer == null) return;
            ch = new CrystalReportHelper("ReportCashDaily.rpt", "Adjustment", "VoidPayment", "VoidCustomerBuy", "VoidPrepayment");
            ch.SetParameter("@USER_CASH_DRAWER_ID", objUserCashDrawer.USER_CASH_DRAWER_ID);
        }
        
        private void viewReportDetail()
        {
            if (this.dgv.SelectedRows.Count == 0) return;

            DataGridViewRow row = this.dgv.SelectedRows[0];
            ch = new CrystalReportHelper("ReportCashDailyDetail.rpt");
            ch.SetParameter("@USER_CASH_DRAWER_ID",row.Cells[this.USER_CASH_DRAWER_ID.Name].Value);
            ch.SetParameter("@USER", row.Cells[this.LOGIN.Name].Value.ToString());
            ch.SetParameter("@CASHDRAWER", row.Cells[this.CASH_DRAWER.Name].Value.ToString());
            ch.SetParameter("@OPEN_DATE", (DateTime)row.Cells[this.OPEN_DATE.Name].Value);
            //ch.SetParameter("@CLOSE_DATE", (DateTime)row.Cells[this.CLOSED_DATE.Name].Value);
            var CloseDate = row.Cells[this.CLOSED_DATE.Name].Value;
            if (CloseDate == null)
            {
                CloseDate = UIHelper._DefaultDate;
            }
            else
            {
                ch.SetParameter("@CLOSE_DATE", (DateTime)row.Cells[this.CLOSED_DATE.Name].Value);
            }
            ch.SetParameter("@CLOSE_DATE", CloseDate);

        } 
        
        private void viewReportSummary()
        {
            if (this.dgv.SelectedRows.Count == 0) return;

            DataGridViewRow row = this.dgv.SelectedRows[0];
            ch = new CrystalReportHelper("ReportCashSummary.rpt");
            ch.SetParameter("@USER_CASH_DRAWER_ID", row.Cells[this.USER_CASH_DRAWER_ID.Name].Value);
            ch.SetParameter("@USER", row.Cells[this.LOGIN.Name].Value.ToString());
            ch.SetParameter("@CASHDRAWER", row.Cells[this.CASH_DRAWER.Name].Value.ToString());
            ch.SetParameter("@OPEN_DATE", (DateTime)row.Cells[this.OPEN_DATE.Name].Value);
            //ch.SetParameter("@CLOSE_DATE", (DateTime)row.Cells[this.CLOSED_DATE.Name].Value);
            var CloseDate = row.Cells[this.CLOSED_DATE.Name].Value;
            if (CloseDate == null)
            {
                CloseDate = UIHelper._DefaultDate;
            }
            else
            {
                ch.SetParameter("@CLOSE_DATE", (DateTime)row.Cells[this.CLOSED_DATE.Name].Value);
            }
            ch.SetParameter("@CLOSE_DATE", CloseDate);
        }
        
        private void viewCountCash()
        {
            if (this.dgv.SelectedRows.Count == 0) return;

            DataGridViewRow row = this.dgv.SelectedRows[0];
            ch = new CrystalReportHelper("ReportCountCash.rpt");
            ch.SetParameter("@USER_CASH_DRAWER_ID", row.Cells[this.USER_CASH_DRAWER_ID.Name].Value);
            ch.SetParameter("@CASH_NAME", row.Cells[this.CASH_DRAWER.Name].Value);
        }

        private void viewUserCashDrawerDaily()
        {
            if (this.dgv.SelectedRows.Count == 0) return;

            DataGridViewRow row = this.dgv.SelectedRows[0];
            ch = new CrystalReportHelper("ReportUserCashDrawerDaily.rpt");
            ch.SetParameter("@USER_CASH_DRAWER_ID", row.Cells[this.USER_CASH_DRAWER_ID.Name].Value);
            ch.SetParameter("@USER", row.Cells[this.LOGIN.Name].Value.ToString());
            ch.SetParameter("@CASHDRAWER", row.Cells[this.CASH_DRAWER.Name].Value.ToString());
            ch.SetParameter("@OPEN_DATE", (DateTime)row.Cells[this.OPEN_DATE.Name].Value);
            //ch.SetParameter("@CLOSE_DATE", (DateTime)row.Cells[this.CLOSED_DATE.Name].Value);
            var CloseDate = row.Cells[this.CLOSED_DATE.Name].Value;
            if (CloseDate == null)
            {
                CloseDate = UIHelper._DefaultDate;
            }
            else
            {
                ch.SetParameter("@CLOSE_DATE", (DateTime)row.Cells[this.CLOSED_DATE.Name].Value);
            }
            ch.SetParameter("@CLOSE_DATE", CloseDate);
        }

        public void BindData()
        { 
            var dat1 = this.dtpStart.Value.Date;
            var dat2 = this.dtpEnd.Value.Date.AddDays(1).AddSeconds(-1);
            var qry = (from uc in DBDataContext.Db.TBL_USER_CASH_DRAWERs
                      join u in DBDataContext.Db.TBL_LOGINs on uc.USER_ID equals u.LOGIN_ID
                      join c in DBDataContext.Db.TBL_CASH_DRAWERs on uc.CASH_DRAWER_ID equals c.CASH_DRAWER_ID
                      where (uc.OPEN_DATE.Date >= dat1 && uc.OPEN_DATE <= dat2) || (uc.CLOSED_DATE.Date >= dat1 && uc.CLOSED_DATE <= dat2) ||
                            !uc.IS_CLOSED
                      select new UserCashDrawer()
                      {
                          USER_CASH_DRAWER_ID = uc.USER_CASH_DRAWER_ID,
                          LOGIN_ID = u.LOGIN_ID,
                          CASH_DRAWER_ID = c.CASH_DRAWER_ID,
                          LOGIN_NAME = u.LOGIN_NAME,
                          CASH_DRAWER_NAME = c.CASH_DRAWER_NAME,
                          NOTE = uc.NOTE,
                          OPEN_DATE = uc.OPEN_DATE,
                          CLOSED_DATE = uc != null ? uc.CLOSED_DATE : UIHelper._DefaultDate,
                          IS_CLOSED = uc.IS_CLOSED,
                          HISTORY = !uc.IS_TAKE_OVER ? "" : Resources.EDIT_DETAIL
                      }).ToList();
            foreach(var item in qry)
            {
                if (item.CLOSED_DATE == UIHelper._DefaultDate)
                {
                    item.CLOSED_DATE = null;
                }
            }
            this.dgv.DataSource = qry;
        }

        private void dtpByMonth_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }

        private void dtpEnd_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }
        private void write()
        {
            _objNew.IS_CLOSED = false;
            _objNew.CLOSED_DATE = Convert.ToDateTime("1900-01-01");
            _objNew.IS_TAKE_OVER = true;
        }
        private void btnRE_OPEN_CASH_DRAWER_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0) return;
            int userCashDrawerID = DataHelper.ParseToInt(this.dgv.SelectedRows[0].Cells[USER_CASH_DRAWER_ID.Name].Value.ToString());
            int login_id = DataHelper.ParseToInt(this.dgv.SelectedRows[0].Cells[LOGIN_ID.Name].Value.ToString());
            /*
             * current user cash drawer not yet close
             */
            TBL_USER_CASH_DRAWER objUserCashDrawer = DBDataContext.Db.TBL_USER_CASH_DRAWERs.FirstOrDefault(row => row.USER_CASH_DRAWER_ID == userCashDrawerID && row.IS_ACTIVE);
            DBDataContext.Db.Refresh(RefreshMode.OverwriteCurrentValues, objUserCashDrawer);
            /*
             * user login already open another cash drawer
             */
            TBL_USER_CASH_DRAWER objUserCashDrawerYetClose = DBDataContext.Db.TBL_USER_CASH_DRAWERs.FirstOrDefault(x => x.USER_ID == login_id && !x.IS_CLOSED && x.IS_ACTIVE);
        
            /*
             * cash drawer being opened by another user login and not yet closed
             */
            TBL_USER_CASH_DRAWER objUserCashDrawerNotAvai = DBDataContext.Db.TBL_USER_CASH_DRAWERs.FirstOrDefault(x => x.CASH_DRAWER_ID == DataHelper.ParseToInt(this.dgv.SelectedRows[0].Cells[CASH_DRAWER_ID.Name].Value.ToString()) && !x.IS_CLOSED && x.IS_ACTIVE);
            
            /*
             * to know login name when cash drawer not available to takeover open
             */
            TBL_LOGIN objLogin = new TBL_LOGIN();
            if (objUserCashDrawerNotAvai != null)
            {
                objLogin = DBDataContext.Db.TBL_LOGINs.FirstOrDefault(l => l.LOGIN_ID == objUserCashDrawerNotAvai.USER_ID);
            }
            if (objUserCashDrawer == null)
            {
                return;
            }
            if (!objUserCashDrawer.IS_CLOSED)
            {
                MsgBox.ShowInformation(Resources.MS_CASH_DRAWER_NOT_YET_CLOSE, Resources.INFORMATION);
                return;
            }
            if (objUserCashDrawerYetClose != null)
            {
                MsgBox.ShowInformation(string.Format(Resources.MS_USER_ALREADY_OPEN_CASH_DRAWER, this.dgv.SelectedRows[0].Cells[LOGIN.Name].Value.ToString()), Resources.INFORMATION);
                return;
            }
            if (objUserCashDrawerNotAvai != null)
            {
                MsgBox.ShowInformation(string.Format(Resources.MS_CASH_DRAWER_NOT_AVAILABLE, this.dgv.SelectedRows[0].Cells[CASH_DRAWER.Name].Value.ToString(), objLogin.LOGIN_NAME, objLogin.LOGIN_NAME), Resources.INFORMATION);
                return;
            }
            if (objUserCashDrawer.IS_CLOSED && objUserCashDrawerYetClose == null && objUserCashDrawerNotAvai == null)
            {
                if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_RE_OPEN_CASH_DRAWER, dgv.SelectedRows[0].Cells[CASH_DRAWER.Name].Value.ToString()), Resources.WARNING) == DialogResult.Yes)
                {
                    Runner.Run(delegate()
                    {
                        /*
                         * PROCESS UPDATE CASH DRAWER & TAKEOVER
                         * 1.ADD RECORD TO TBL_USER_CASH_DRAWER_TAKOVER & TBL_USER_CASH_DRAWER_TAKOVER_DETAIL
                         * 2.UPDATE OLD CASH DRAWER TO NOT YET CLOSED
                         */

                        //1.ADD RECORD TO TBL_USER_CASH_DRAWER_TAKOVER & TBL_USER_CASH_DRAWER_TAKOVER_DETAIL
                        TBL_CHANGE_LOG log = new TBL_CHANGE_LOG();
                        TBL_USER_CASH_DRAWER_TAKEOVER objUserCashDrawerTakeover = new TBL_USER_CASH_DRAWER_TAKEOVER()
                        {
                            USER_CASH_DRAWER_ID = userCashDrawerID,
                            TAKE_OVER_DATE = DBDataContext.Db.GetSystemDate(),
                            TAKE_OVER_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME,
                            CLOSED_DATE = objUserCashDrawer.CLOSED_DATE
                        };
                        log = DBDataContext.Db.Insert(objUserCashDrawerTakeover);
                        foreach (var detail in DBDataContext.Db.TBL_USER_CASH_DRAWER_DETAILs.Where(x => x.USER_CASH_DRAWER_ID == userCashDrawerID))
                        {
                            TBL_USER_CASH_DRAWER_TAKEOVER_DETAIL objUserCashDrawerTakeoverDetail = new TBL_USER_CASH_DRAWER_TAKEOVER_DETAIL()
                            {
                                TAKEOVER_DETAIL_ID = 0,
                                TAKEOVER_ID = objUserCashDrawerTakeover.TAKEOVER_ID,
                                CURRENCY_ID = detail.CURRENCY_ID,
                                CASH_FLOAT = detail.CASH_FLOAT,
                                TOTAL_CASH = detail.TOTAL_CASH,
                                ACTUAL_CASH = detail.ACTUAL_CASH,
                                SURPLUS = detail.SURPLUS
                            };
                            DBDataContext.Db.InsertChild(objUserCashDrawerTakeoverDetail, objUserCashDrawerTakeover, ref log);
                        }

                        //2.UPDATE OLD CASH DRAWER TO NOT YET CLOSED
                        objUserCashDrawer._CopyTo(_objNew);
                        objUserCashDrawer._CopyTo(_objOld);
                        write();
                        DBDataContext.Db.Update(_objOld, _objNew);
                    });
                }
                BindData();
                UIHelper.SelectRow(dgv, userCashDrawerID);
            }
        }

        private void dtpStart_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void dtpEnd_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (this.dgv.Columns[e.ColumnIndex].Name != this.HISTORY_.Name)
            {
                return;
            } 
            int user_cash_drawer_id = DataHelper.ParseToInt(dgv.SelectedRows[0].Cells[USER_CASH_DRAWER_ID.Name].Value.ToString());
            TBL_USER_CASH_DRAWER objUserCashDrawer = DBDataContext.Db.TBL_USER_CASH_DRAWERs.FirstOrDefault(x => x.USER_CASH_DRAWER_ID == user_cash_drawer_id);
            if (objUserCashDrawer == null)
            {
                return;
            }
            this.viewReportTakeOverCashDrawer(user_cash_drawer_id);
            DialogReportViewer viewer = new DialogReportViewer(ch.Report, "");
            viewer.ShowDialog();
        }
        private void viewReportTakeOverCashDrawer(int intUserCashDrawerId)
        {
            ch = new CrystalReportHelper("ReportUserCashDrawerTakeover.rpt");
            ch.SetParameter("@USER_CASH_DRAWER_ID", intUserCashDrawerId);
        }
        #endregion

        public class UserCashDrawer
        {
            public int USER_CASH_DRAWER_ID { get; set; }
            public int LOGIN_ID { get; set; }
            public int CASH_DRAWER_ID { get; set; }
            public string LOGIN_NAME { get; set; }
            public string CASH_DRAWER_NAME { get; set; }
            public string NOTE { get; set; }
            public DateTime OPEN_DATE { get; set; }
            public DateTime? CLOSED_DATE { get; set; }
            public bool IS_CLOSED { get; set; }
            public string HISTORY { get; set; }
        }


    }
}
