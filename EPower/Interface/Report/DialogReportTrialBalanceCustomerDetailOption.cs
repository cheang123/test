﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogReportTrialBalanceCustomerDetailOption : ExDialog
    {
        public DialogReportTrialBalanceCustomerDetailOption()
        {
            InitializeComponent();
            Bind();
        }

        private void DialogReportAgingOptions_Load(object sender, EventArgs e)
        {
            Bind();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }

        #region Method

        public void Bind()
        {
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);
            UIHelper.SetDataSourceToComboBox(cboArea, Lookup.GetAreas(), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(cboCustomerStatus, Lookup.GetCustomerStatuses(), Resources.ALL_STATUS);
            UIHelper.SetDataSourceToComboBox(cboCycle, Lookup.GetBillingCycles(), Resources.ALL_CYCLE);
            UIHelper.SetDataSourceToComboBox(cboMeterType, Lookup.GetMeterType(), Resources.ALl_METER_TYPE);
        }
        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}