﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageReportRecurringService : Form
    {
        enum ReportType
        {
            Customer,
            IncomeDetail,
            IncomeSummary,
            IncomeMonthly
        }
        CrystalReportHelper ch = null; 
        public PageReportRecurringService()
        {
            try
            {
                InitializeComponent();
                viewer.DefaultView();

                this.dtpDate.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                // report name
                this.cboReport.Items.Add(Resources.REPORT_CUSTOMER);
                this.cboReport.Items.Add( Resources.MONTH_DETAIL);
                this.cboReport.Items.Add(Resources.MONTH_SUMMARY);
                this.cboReport.Items.Add(Resources.YEAR);
                this.cboReport.SelectedIndex = 1;

                // Area
                UIHelper.SetDataSourceToComboBox(this.cboArea, DBDataContext.Db.TBL_AREAs.Where(row => row.IS_ACTIVE).OrderBy(row => row.AREA_CODE), Resources.ALL_AREA);
                UIHelper.SetDataSourceToComboBox(this.cboInvoiceItem, DBDataContext.Db.TBL_INVOICE_ITEMs.Where(row => row.IS_RECURRING_SERVICE && row.IS_ACTIVE).Select(row=>new{row.INVOICE_ITEM_ID,row.INVOICE_ITEM_NAME}), Resources.ALL_SERVICE);
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        } 


        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(this.viewReport);
        }

        private void viewReport()
        {
            try
            {
                if (this.cboReport.SelectedIndex == -1)
                    this.cboReport.SelectedIndex = 0;
                if (this.cboReport.SelectedIndex == (int)ReportType.Customer)
                {
                    this.ch = new CrystalReportHelper("ReportRecurringServiceCustomer.rpt");
                }
                else if (this.cboReport.SelectedIndex == (int)ReportType.IncomeDetail)
                {
                    this.ch = new CrystalReportHelper("ReportRecurringServiceIncomeDetaill.rpt");
                    this.ch.SetParameter("@MONTH", new DateTime(this.dtpDate.Value.Year, this.dtpDate.Value.Month, 1));
                }
                else if (this.cboReport.SelectedIndex == (int)ReportType.IncomeSummary)
                {
                    this.ch = new CrystalReportHelper("ReportRecurringServiceIncomeSummary.rpt");
                    this.ch.SetParameter("@MONTH", new DateTime(this.dtpDate.Value.Year, this.dtpDate.Value.Month, 1));

                }
                else if (this.cboReport.SelectedIndex == (int)ReportType.IncomeMonthly)
                {
                    this.ch = new CrystalReportHelper("ReportRecurringServiceIncomeMonthly.rpt");
                    this.ch.SetParameter("@YEAR", this.dtpDate.Value.Year);
                }
                this.ch.SetParameter("@AREA_ID", (int)this.cboArea.SelectedValue);
                this.ch.SetParameter("@AREA_NAME", this.cboArea.Text);
                this.ch.SetParameter("@INVOICE_ITEM_ID", (int)this.cboInvoiceItem.SelectedValue);
                this.ch.SetParameter("@INVOICE_ITEM_NAME", this.cboInvoiceItem.Text);
                this.viewer.ReportSource = ch.Report;
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        } 

        private void btnMail_Click(object sender, EventArgs e)
        {
            this.sendMail();
        }

        private void sendMail()
        {
            try
            {
                this.viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + this.ch.ReportName + ".xls").FullName;
                this.ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(this.ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_SEND_EMAIL, Base.Properties.Resources.WARNING);
            }
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cboReport.SelectedIndex == (int)ReportType.Customer)
            {
                this.dtpDate.Visible = false;
            }
            else if (this.cboReport.SelectedIndex == (int)ReportType.IncomeDetail)
            {
                this.dtpDate.CustomFormat = "yyyy - MM";
                this.dtpDate.Visible = true;
            }
            else if (this.cboReport.SelectedIndex == (int)ReportType.IncomeSummary)
            {
                this.dtpDate.CustomFormat = "yyyy - MM";
                this.dtpDate.Visible = true;
            }
            else if (this.cboReport.SelectedIndex == (int)ReportType.IncomeMonthly)
            {
                this.dtpDate.CustomFormat = "yyyy";
                this.dtpDate.Visible = true;
            } 
        }

      
    }
}
