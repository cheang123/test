﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogReportTrialBalanceARDetailOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogReportTrialBalanceARDetailOption));
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.cboItemType = new System.Windows.Forms.ComboBox();
            this.cboItem = new System.Windows.Forms.ComboBox();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.lblSERVICE_TYPE = new System.Windows.Forms.Label();
            this.lblSERVICE = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblORDER = new System.Windows.Forms.Label();
            this.cboOrderBy = new System.Windows.Forms.ComboBox();
            this.cboCashDrawer = new System.Windows.Forms.ComboBox();
            this.lblCASH_DRAWER = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.cboPaymentAccount = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboPaymentAccount.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboPaymentAccount);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.cboCashDrawer);
            this.content.Controls.Add(this.lblCASH_DRAWER);
            this.content.Controls.Add(this.cboOrderBy);
            this.content.Controls.Add(this.lblORDER);
            this.content.Controls.Add(this.dtpMonth);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.content.Controls.Add(this.cboArea);
            this.content.Controls.Add(this.cboBillingCycle);
            this.content.Controls.Add(this.cboItemType);
            this.content.Controls.Add(this.cboItem);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblSERVICE);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblSERVICE_TYPE);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblSERVICE_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.lblSERVICE, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.cboItem, 0);
            this.content.Controls.SetChildIndex(this.cboItemType, 0);
            this.content.Controls.SetChildIndex(this.cboBillingCycle, 0);
            this.content.Controls.SetChildIndex(this.cboArea, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.dtpMonth, 0);
            this.content.Controls.SetChildIndex(this.lblORDER, 0);
            this.content.Controls.SetChildIndex(this.cboOrderBy, 0);
            this.content.Controls.SetChildIndex(this.lblCASH_DRAWER, 0);
            this.content.Controls.SetChildIndex(this.cboCashDrawer, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.cboPaymentAccount, 0);
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.DropDownWidth = 200;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            // 
            // cboItemType
            // 
            this.cboItemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboItemType.DropDownWidth = 200;
            this.cboItemType.FormattingEnabled = true;
            resources.ApplyResources(this.cboItemType, "cboItemType");
            this.cboItemType.Name = "cboItemType";
            this.cboItemType.SelectedIndexChanged += new System.EventHandler(this.cboItemType_SelectedIndexChanged);
            this.cboItemType.SelectedValueChanged += new System.EventHandler(this.cboItemType_SelectedValueChanged);
            // 
            // cboItem
            // 
            this.cboItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboItem.DropDownWidth = 200;
            this.cboItem.FormattingEnabled = true;
            resources.ApplyResources(this.cboItem, "cboItem");
            this.cboItem.Name = "cboItem";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.DropDownWidth = 100;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // lblSERVICE_TYPE
            // 
            resources.ApplyResources(this.lblSERVICE_TYPE, "lblSERVICE_TYPE");
            this.lblSERVICE_TYPE.Name = "lblSERVICE_TYPE";
            // 
            // lblSERVICE
            // 
            resources.ApplyResources(this.lblSERVICE, "lblSERVICE");
            this.lblSERVICE.Name = "lblSERVICE";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.DropDownWidth = 200;
            this.cboArea.FormattingEnabled = true;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // dtpMonth
            // 
            this.dtpMonth.Checked = false;
            resources.ApplyResources(this.dtpMonth, "dtpMonth");
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.ShowCheckBox = true;
            this.dtpMonth.Enter += new System.EventHandler(this.InputEnglish);
            this.dtpMonth.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtpMonth_MouseDown);
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.Name = "lblMONTH";
            // 
            // lblORDER
            // 
            resources.ApplyResources(this.lblORDER, "lblORDER");
            this.lblORDER.Name = "lblORDER";
            // 
            // cboOrderBy
            // 
            this.cboOrderBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOrderBy.DropDownWidth = 100;
            this.cboOrderBy.FormattingEnabled = true;
            this.cboOrderBy.Items.AddRange(new object[] {
            resources.GetString("cboOrderBy.Items"),
            resources.GetString("cboOrderBy.Items1")});
            resources.ApplyResources(this.cboOrderBy, "cboOrderBy");
            this.cboOrderBy.Name = "cboOrderBy";
            // 
            // cboCashDrawer
            // 
            this.cboCashDrawer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCashDrawer.DropDownWidth = 100;
            this.cboCashDrawer.FormattingEnabled = true;
            resources.ApplyResources(this.cboCashDrawer, "cboCashDrawer");
            this.cboCashDrawer.Name = "cboCashDrawer";
            // 
            // lblCASH_DRAWER
            // 
            resources.ApplyResources(this.lblCASH_DRAWER, "lblCASH_DRAWER");
            this.lblCASH_DRAWER.Name = "lblCASH_DRAWER";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnOK);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // cboPaymentAccount
            // 
            resources.ApplyResources(this.cboPaymentAccount, "cboPaymentAccount");
            this.cboPaymentAccount.Name = "cboPaymentAccount";
            this.cboPaymentAccount.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboPaymentAccount.Properties.Appearance.Font")));
            this.cboPaymentAccount.Properties.Appearance.Options.UseFont = true;
            this.cboPaymentAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboPaymentAccount.Properties.Buttons"))))});
            this.cboPaymentAccount.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboPaymentAccount.Properties.Columns"), resources.GetString("cboPaymentAccount.Properties.Columns1"), ((int)(resources.GetObject("cboPaymentAccount.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboPaymentAccount.Properties.Columns3"))), resources.GetString("cboPaymentAccount.Properties.Columns4"), ((bool)(resources.GetObject("cboPaymentAccount.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboPaymentAccount.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboPaymentAccount.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboPaymentAccount.Properties.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboPaymentAccount.Properties.Columns9"), resources.GetString("cboPaymentAccount.Properties.Columns10"), ((int)(resources.GetObject("cboPaymentAccount.Properties.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboPaymentAccount.Properties.Columns12"))), resources.GetString("cboPaymentAccount.Properties.Columns13"), ((bool)(resources.GetObject("cboPaymentAccount.Properties.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboPaymentAccount.Properties.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboPaymentAccount.Properties.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboPaymentAccount.Properties.Columns17")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboPaymentAccount.Properties.Columns18"), resources.GetString("cboPaymentAccount.Properties.Columns19"), ((int)(resources.GetObject("cboPaymentAccount.Properties.Columns20"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboPaymentAccount.Properties.Columns21"))), resources.GetString("cboPaymentAccount.Properties.Columns22"), ((bool)(resources.GetObject("cboPaymentAccount.Properties.Columns23"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboPaymentAccount.Properties.Columns24"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboPaymentAccount.Properties.Columns25"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboPaymentAccount.Properties.Columns26"))))});
            this.cboPaymentAccount.Properties.NullText = resources.GetString("cboPaymentAccount.Properties.NullText");
            this.cboPaymentAccount.Properties.PopupWidth = 500;
            this.cboPaymentAccount.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboPaymentAccount.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboPaymentAccount.Required = false;
            // 
            // DialogReportTrialBalanceARDetailOption
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogReportTrialBalanceARDetailOption";
            this.Load += new System.EventHandler(this.DialogReportTrialBalanceARDetailOption1_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cboPaymentAccount.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Label lblCYCLE_NAME;
        private Label lblAREA;
        public ComboBox cboBillingCycle;
        public ComboBox cboItemType;
        public ComboBox cboItem;
        public ComboBox cboCurrency;
        public ComboBox cboArea;
        public Label lblPAYMENT_ACCOUNT;
        public Label lblMONTH;
        public DateTimePicker dtpMonth;
        public ComboBox cboOrderBy;
        public Label lblORDER;
        public ComboBox cboCashDrawer;
        public Label lblCASH_DRAWER;
        public Label lblSERVICE;
        public Label lblSERVICE_TYPE;
        public Label lblCURRENCY;
        private Panel panel2;
        private Panel panel1;
        private ExButton btnClose;
        private ExButton btnOK;
        public HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboPaymentAccount;
    }
}