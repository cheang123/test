﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportCustomerUsageStatistic
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportCustomerUsageStatistic));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboReport = new System.Windows.Forms.ComboBox();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.btnMAIL = new SoftTech.Component.ExButton();
            this.dtpYear = new System.Windows.Forms.DateTimePicker();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboReport);
            this.panel1.Controls.Add(this.cboBillingCycle);
            this.panel1.Controls.Add(this.btnMAIL);
            this.panel1.Controls.Add(this.dtpYear);
            this.panel1.Controls.Add(this.btnVIEW);
            this.panel1.Controls.Add(this.cboArea);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboReport
            // 
            this.cboReport.DropDownHeight = 200;
            this.cboReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReport.DropDownWidth = 250;
            this.cboReport.FormattingEnabled = true;
            resources.ApplyResources(this.cboReport, "cboReport");
            this.cboReport.Name = "cboReport";
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownHeight = 200;
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            // 
            // btnMAIL
            // 
            resources.ApplyResources(this.btnMAIL, "btnMAIL");
            this.btnMAIL.Name = "btnMAIL";
            this.btnMAIL.UseVisualStyleBackColor = true;
            this.btnMAIL.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // dtpYear
            // 
            resources.ApplyResources(this.dtpYear, "dtpYear");
            this.dtpYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpYear.Name = "dtpYear";
            this.dtpYear.ShowUpDown = true;
            // 
            // btnVIEW
            // 
            resources.ApplyResources(this.btnVIEW, "btnVIEW");
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // cboArea
            // 
            this.cboArea.DropDownHeight = 200;
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.FormattingEnabled = true;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            this.cboArea.SelectedIndexChanged += new System.EventHandler(this.cboSaveState_SelectedIndexChanged);
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Name = "viewer";
            // 
            // PageReportCustomerUsageStatistic
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportCustomerUsageStatistic";
            this.VisibleChanged += new System.EventHandler(this.PageReportOtherRevenueByMonth_VisibleChanged);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        public ComboBox cboArea;
        private ExButton btnVIEW;
        private DateTimePicker dtpYear;
        private ExButton btnMAIL;
        public ComboBox cboBillingCycle;
        public ComboBox cboReport;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}
