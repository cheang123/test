﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Security = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogPaymentHistory : ExDialog
    {
        TBL_INVOICE _objInvoice = new TBL_INVOICE();
        TLKP_CURRENCY _objCurrency = new TLKP_CURRENCY();
        TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();
        public bool IsDelete = false;
        long invoiceId = 0;
        bool _load = false;
        #region Constructor
        public DialogPaymentHistory(long _invoiceId)
        {
            InitializeComponent();
            invoiceId = _invoiceId;
            UIHelper.DataGridViewProperties(dgvPayment);

            this.btnREMOVE.Enabled = Security.IsAuthorized(Permission.POSTPAID_CANCELPAYMENT);
            this.lblPAYMENT.Enabled = Security.IsAuthorized(Permission.POSTPAID_PAYMENT);
            this.btnPRINT.Enabled = Security.IsAuthorized(Permission.POSTPAID_REPRINTRECIEPT);

            paymentStatus();
            read();
            load(_objInvoice.INVOICE_ID);
            _load = true;
        }

        #endregion Constructor

        #region Method

        private void paymentStatus()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("STATUS_ID", typeof(bool));
            dtReport.Columns.Add("STATUS_NAME", typeof(string));
            dtReport.Rows.Add(true, Resources.ALL_STATUS);
            dtReport.Rows.Add(false, Resources.PAYMENT_SUCCESS);
            dtReport.Rows.Add(true, Resources.DELETE);
            UIHelper.SetDataSourceToComboBox(cboStatus, dtReport, "STATUS_ID", "STATUS_NAME");
            cboStatus.SelectedIndex = 1;
        }

        private void read()
        {
            _objInvoice = DBDataContext.Db.TBL_INVOICEs.FirstOrDefault(x => x.INVOICE_ID == invoiceId);
            _objCurrency = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == _objInvoice.CURRENCY_ID);
            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == _objInvoice.CUSTOMER_ID);
            txtInvoiceNo.Text = _objInvoice.INVOICE_NO;
            txtInvoiceTittle.Text = _objInvoice.INVOICE_TITLE;
            txtPaidAmount.Text = UIHelper.FormatCurrency(_objInvoice.PAID_AMOUNT, _objInvoice.CURRENCY_ID) + " " + _objCurrency.CURRENCY_SING;
            txtSettleAmount.Text = UIHelper.FormatCurrency(_objInvoice.SETTLE_AMOUNT, _objInvoice.CURRENCY_ID) + " " + _objCurrency.CURRENCY_SING;
            txtDueAmount.Text = UIHelper.FormatCurrency(_objInvoice.SETTLE_AMOUNT - _objInvoice.PAID_AMOUNT, _objInvoice.CURRENCY_ID) + " " + _objCurrency.CURRENCY_SING;
        }
        private void load(long invoiceId)
        {
            //Display payment in data grid
            var db = (from p in DBDataContext.Db.TBL_PAYMENTs
                      join pd in DBDataContext.Db.TBL_PAYMENT_DETAILs on p.PAYMENT_ID equals pd.PAYMENT_ID
                      join c in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals c.CURRENCY_ID
                      join acc in DBDataContext.Db.TBL_ACCOUNT_CHARTs on p.PAYMENT_ACCOUNT_ID equals acc.ACCOUNT_ID
                      join ucd in DBDataContext.Db.TBL_USER_CASH_DRAWERs on p.USER_CASH_DRAWER_ID equals ucd.USER_CASH_DRAWER_ID into l
                      from ucd in l.DefaultIfEmpty()
                      join cd in DBDataContext.Db.TBL_CASH_DRAWERs on ucd.CASH_DRAWER_ID equals cd.CASH_DRAWER_ID into ll
                      from cd in ll.DefaultIfEmpty()
                      join bpd in DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs on p.BANK_PAYMENT_DETAIL_ID equals bpd.BANK_PAYMENT_DETAIL_ID into lll
                      from bpd in lll.DefaultIfEmpty()
                      where pd.INVOICE_ID == _objInvoice.INVOICE_ID
                          && (cboStatus.SelectedIndex == 0 || p.IS_VOID == (bool)cboStatus.SelectedValue)
                          && p.IS_ACTIVE​
                      select new PaymentHistory()
                      {
                          PAYMENT_ID = p.PAYMENT_ID,
                          PAY_DATE = p.PAY_DATE,
                          PAYMENT_NO = p.PAYMENT_NO,
                          CREATE_BY = bpd.BANK_ALIAS ?? p.CREATE_BY,
                          ACCOUNT_NAME = acc.ACCOUNT_NAME,
                          CASH_DRAWER_NAME = cd.CASH_DRAWER_NAME ?? "",
                          OPEN_DATE = ucd != null ? ucd.OPEN_DATE : UIHelper._DefaultDate,
                          CLOSED_DATE = ucd != null ? ucd.CLOSED_DATE : UIHelper._DefaultDate,
                          NO_INVOICE = DBDataContext.Db.TBL_PAYMENT_DETAILs.Count(x => x.PAYMENT_ID == p.PAYMENT_ID),
                          PAY_AMOUNT = UIHelper.FormatCurrency(pd.PAY_AMOUNT, c.CURRENCY_ID),
                          CURRENCY_SIGN = c.CURRENCY_SING,
                          IS_VOID = p.IS_VOID
                      }).ToList();

            foreach (var item in db)
            {
                if (item.OPEN_DATE == UIHelper._DefaultDate)
                {
                    item.OPEN_DATE = null;
                }
                if (item.CLOSED_DATE == UIHelper._DefaultDate)
                {
                    item.CLOSED_DATE = null;
                }
            }
            dgvPayment.DataSource = db;

            //set ForeColor to Red if customer is close.
            foreach (DataGridViewRow item in dgvPayment.Rows)
            {
                bool isVoid = (bool)item.Cells[IS_VOID.Name].Value;
                if (isVoid)
                {
                    item.DefaultCellStyle.ForeColor = Color.Red;
                    item.DefaultCellStyle.SelectionForeColor = Color.Red;
                }
            }
        }

        #endregion Method
         
 
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblDeletePayment_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // validate to Open cash drawer before delete
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_DELETE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            if (dgvPayment.SelectedRows.Count == 0)
            {
                MsgBox.ShowInformation(Resources.ROW_EMPTY);
                return;
            }
            // if already void no need to void again
            if ((bool)dgvPayment.SelectedRows[0].Cells[IS_VOID.Name].Value)
            {
                MsgBox.ShowInformation(Resources.MSG_THIS_PAYMENT_IS_ALREADY_VOID);
                return;
            }
            if (dgvPayment.SelectedRows.Count > 0)
            {
                string paymentNo = string.Format(Resources.MSQ_CONFIRM_PAYMENT, dgvPayment.SelectedRows[0].Cells[RECEIPT_NO.Name].Value.ToString());
                DialogPaymentHistoryCancel diag = new DialogPaymentHistoryCancel(paymentNo);
                if (diag.ShowDialog() == DialogResult.Yes)
                {
                    try
                    {
                        int paymentId = (int)dgvPayment.SelectedRows[0].Cells[PAYMENT_ID.Name].Value;
                        TBL_PAYMENT objP = DBDataContext.Db.TBL_PAYMENTs.FirstOrDefault(x => x.PAYMENT_ID == paymentId);
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                        {
                            //to prevent cross thread 
                            var reason = diag.txtREASON.Text;
                            var payDate = diag.dtpPayDate.Value;
                            Runner.RunNewThread(() =>
                            {
                                Payment.CancelPayment(new TBL_PAYMENT() { PAYMENT_ID = paymentId, PAY_DATE = payDate, BANK_PAYMENT_DETAIL_ID = objP.BANK_PAYMENT_DETAIL_ID }, reason);
                            });
                            tran.Complete();

                        }
                        this.IsDelete = true;
                        read();
                        load(_objInvoice.INVOICE_ID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
            }
        }

        private void lblPayment_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Login.CurrentCashDrawer == null)
                    return;
            }
            DialogReceivePayment diag = new DialogReceivePayment(_objCustomer, true);
            if (diag._blnIsCheckedPendingPayment)
            {
                this.IsDelete = true;
                read();
                load(_objInvoice.INVOICE_ID);
                return;
            }
            if (diag.ShowDialog() == DialogResult.OK)
            {
                this.IsDelete = true;
                read();
                load(_objInvoice.INVOICE_ID);
            }
            load(_objInvoice.INVOICE_ID);
        }

        private void lblPrint_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvPayment.SelectedRows.Count > 0)
            {
                int intPaymentID = (int)dgvPayment.SelectedRows[0].Cells[PAYMENT_ID.Name].Value;
                TBL_PAYMENT _objPayment = DBDataContext.Db.TBL_PAYMENTs.Where(p => p.PAYMENT_ID == intPaymentID).FirstOrDefault();
                try
                {
                    CrystalReportHelper ch = new CrystalReportHelper("ReportReceiptPayment.rpt");
                    ch.SetParameter("PAYMENT_ID", (int)_objPayment.PAYMENT_ID);
                    ch.ViewReport(Resources.PRINT_RECEIPT, true);
                    ch.Dispose();
                }
                catch (Exception ex)
                {
                    MsgBox.ShowError(ex);
                }
            }
            
        }

        private void dgvPayment_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1)
            {
                if (this.dgvPayment.Columns[e.ColumnIndex].Name == this.RECEIPT_NO.Name)
                {
                    int intPaymentId = (int)dgvPayment.SelectedRows[0].Cells[PAYMENT_ID.Name].Value;
                    new DialogPaymentDetailCancel(intPaymentId).ShowDialog();
                }
            }
        }

        private void cboStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_load)
            {
                load(invoiceId);
            }
        }
    }

    public class PaymentHistory
    {
        public int PAYMENT_ID { get; set; }
        public DateTime? PAY_DATE { get; set; }
        public string PAYMENT_NO { get; set; }
        public string CREATE_BY { get; set; }
        public string ACCOUNT_NAME { get; set; }
        public string CASH_DRAWER_NAME { get; set; }
        public DateTime? OPEN_DATE { get; set; }
        public DateTime? CLOSED_DATE { get; set; }
        public int NO_INVOICE { get; set; }
        public string PAY_AMOUNT { get; set; }
        public string CURRENCY_SIGN { get; set; }
        public bool IS_VOID { get; set; }
    }
}
