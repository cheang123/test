﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerInBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.CUS_METER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POSITION_IN_BOX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnSAVE = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnSAVE);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.dgv);
            this.content.Size = new System.Drawing.Size(651, 346);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnSAVE, 0);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUS_METER_ID,
            this.CUSTOMER_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.AREA,
            this.METER_CODE,
            this.POSITION_IN_BOX});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Location = new System.Drawing.Point(2, -1);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(648, 311);
            this.dgv.TabIndex = 10;
            this.dgv.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEndEdit);
            this.dgv.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEnter);
            this.dgv.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellValueChanged);
            this.dgv.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_DataError);
            // 
            // CUS_METER_ID
            // 
            this.CUS_METER_ID.DataPropertyName = "CUS_METER_ID";
            this.CUS_METER_ID.HeaderText = "CUS_METER_ID";
            this.CUS_METER_ID.Name = "CUS_METER_ID";
            this.CUS_METER_ID.Visible = false;
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            this.CUSTOMER_ID.HeaderText = "CUSTOMER_ID";
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.Visible = false;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.HeaderText = "លេខកូដ";
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.HeaderText = "ឈ្មោះអតិថិជន";
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            // 
            // AREA
            // 
            this.AREA.DataPropertyName = "AREA_NAME";
            this.AREA.HeaderText = "តំបន់";
            this.AREA.Name = "AREA";
            // 
            // METER_CODE
            // 
            this.METER_CODE.DataPropertyName = "METER_CODE";
            this.METER_CODE.HeaderText = "កុងទ័រ";
            this.METER_CODE.Name = "METER_CODE";
            // 
            // POSITION_IN_BOX
            // 
            this.POSITION_IN_BOX.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.POSITION_IN_BOX.DataPropertyName = "POSITION_IN_BOX";
            this.POSITION_IN_BOX.HeaderText = "ទីតាំងក្នុងប្រអប់";
            this.POSITION_IN_BOX.Name = "POSITION_IN_BOX";
            this.POSITION_IN_BOX.Width = 106;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCLOSE.Location = new System.Drawing.Point(559, 314);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(84, 23);
            this.btnCLOSE.TabIndex = 12;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSAVE
            // 
            this.btnSAVE.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSAVE.Location = new System.Drawing.Point(468, 314);
            this.btnSAVE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSAVE.Name = "btnSAVE";
            this.btnSAVE.Size = new System.Drawing.Size(85, 23);
            this.btnSAVE.TabIndex = 12;
            this.btnSAVE.Text = "រក្សាទុក";
            this.btnSAVE.UseVisualStyleBackColor = true;
            this.btnSAVE.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // DialogCustomerInBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 369);
            this.Name = "DialogCustomerInBox";
            this.Text = "ឈ្មោះអតិថិជនក្នុងប្រអប់";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dgv;
        private ExButton btnCLOSE;
        private ExButton btnSAVE;
        private DataGridViewTextBoxColumn CUS_METER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn METER_CODE;
        private DataGridViewTextBoxColumn POSITION_IN_BOX;
    }
}