﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerChangeBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerChangeBox));
            this.txtOLD_BOX_POLE = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lblCUTTER = new System.Windows.Forms.Label();
            this.lblBILLER = new System.Windows.Forms.Label();
            this.lblCOLLECTOR = new System.Windows.Forms.Label();
            this.txtOLD_BOX_CUTTER = new System.Windows.Forms.TextBox();
            this.txtOLD_BOX_BILLER = new System.Windows.Forms.TextBox();
            this.txtOLD_BOX_COLLECTOR = new System.Windows.Forms.TextBox();
            this.lblBOX = new System.Windows.Forms.Label();
            this.lblOLD_BOX = new System.Windows.Forms.Label();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.txtOLD_BOX = new SoftTech.Component.ExTextbox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNEW_BOX = new System.Windows.Forms.Label();
            this.lblCUSTOMER_INFORMATION = new System.Windows.Forms.Label();
            this.txtCUSTOMER_NAME = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.txtCUSTOMER_CODE = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtNEW_BOX_POLE = new System.Windows.Forms.TextBox();
            this.lblCUTTER_1 = new System.Windows.Forms.Label();
            this.lblBILLER_1 = new System.Windows.Forms.Label();
            this.lblCOLLECTOR_1 = new System.Windows.Forms.Label();
            this.txtNEW_BOX_CUTTER = new System.Windows.Forms.TextBox();
            this.txtNEW_BOX_BILLER = new System.Windows.Forms.TextBox();
            this.txtNEW_BOX_COLLECTOR = new System.Windows.Forms.TextBox();
            this.lblBOX_1 = new System.Windows.Forms.Label();
            this.lblPOLE_1 = new System.Windows.Forms.Label();
            this.txtNEW_BOX = new SoftTech.Component.ExTextbox();
            this.lblCHANGE_DATE = new System.Windows.Forms.Label();
            this.dtpCHANGE_DATE = new System.Windows.Forms.DateTimePicker();
            this.txtNEWPositionInBox = new System.Windows.Forms.TextBox();
            this.lblPOSITION_IN_BOX_1 = new System.Windows.Forms.Label();
            this.lblPOSITION_IN_BOX = new System.Windows.Forms.Label();
            this.txtOLDPositionInBox = new System.Windows.Forms.TextBox();
            this.lblVIEW_BOX_CUSTOMER = new SoftTech.Component.ExLinkLabel(this.components);
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblVIEW_BOX_CUSTOMER);
            this.content.Controls.Add(this.txtOLDPositionInBox);
            this.content.Controls.Add(this.txtNEWPositionInBox);
            this.content.Controls.Add(this.lblPOSITION_IN_BOX);
            this.content.Controls.Add(this.lblPOSITION_IN_BOX_1);
            this.content.Controls.Add(this.dtpCHANGE_DATE);
            this.content.Controls.Add(this.txtNEW_BOX_POLE);
            this.content.Controls.Add(this.lblCHANGE_DATE);
            this.content.Controls.Add(this.lblCUTTER_1);
            this.content.Controls.Add(this.lblBILLER_1);
            this.content.Controls.Add(this.lblCOLLECTOR_1);
            this.content.Controls.Add(this.txtNEW_BOX_CUTTER);
            this.content.Controls.Add(this.txtNEW_BOX_BILLER);
            this.content.Controls.Add(this.txtNEW_BOX_COLLECTOR);
            this.content.Controls.Add(this.lblBOX_1);
            this.content.Controls.Add(this.lblPOLE_1);
            this.content.Controls.Add(this.txtNEW_BOX);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.txtCUSTOMER_CODE);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            this.content.Controls.Add(this.txtCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_INFORMATION);
            this.content.Controls.Add(this.lblNEW_BOX);
            this.content.Controls.Add(this.txtOLD_BOX_POLE);
            this.content.Controls.Add(this.lblCUTTER);
            this.content.Controls.Add(this.lblBILLER);
            this.content.Controls.Add(this.lblCOLLECTOR);
            this.content.Controls.Add(this.txtOLD_BOX_CUTTER);
            this.content.Controls.Add(this.txtOLD_BOX_BILLER);
            this.content.Controls.Add(this.txtOLD_BOX_COLLECTOR);
            this.content.Controls.Add(this.lblBOX);
            this.content.Controls.Add(this.lblOLD_BOX);
            this.content.Controls.Add(this.lblPOLE);
            this.content.Controls.Add(this.txtOLD_BOX);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.label14);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_BOX, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE, 0);
            this.content.Controls.SetChildIndex(this.lblOLD_BOX, 0);
            this.content.Controls.SetChildIndex(this.lblBOX, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_BOX_COLLECTOR, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_BOX_BILLER, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_BOX_CUTTER, 0);
            this.content.Controls.SetChildIndex(this.lblCOLLECTOR, 0);
            this.content.Controls.SetChildIndex(this.lblBILLER, 0);
            this.content.Controls.SetChildIndex(this.lblCUTTER, 0);
            this.content.Controls.SetChildIndex(this.txtOLD_BOX_POLE, 0);
            this.content.Controls.SetChildIndex(this.lblNEW_BOX, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_BOX, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE_1, 0);
            this.content.Controls.SetChildIndex(this.lblBOX_1, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_BOX_COLLECTOR, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_BOX_BILLER, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_BOX_CUTTER, 0);
            this.content.Controls.SetChildIndex(this.lblCOLLECTOR_1, 0);
            this.content.Controls.SetChildIndex(this.lblBILLER_1, 0);
            this.content.Controls.SetChildIndex(this.lblCUTTER_1, 0);
            this.content.Controls.SetChildIndex(this.lblCHANGE_DATE, 0);
            this.content.Controls.SetChildIndex(this.txtNEW_BOX_POLE, 0);
            this.content.Controls.SetChildIndex(this.dtpCHANGE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblPOSITION_IN_BOX_1, 0);
            this.content.Controls.SetChildIndex(this.lblPOSITION_IN_BOX, 0);
            this.content.Controls.SetChildIndex(this.txtNEWPositionInBox, 0);
            this.content.Controls.SetChildIndex(this.txtOLDPositionInBox, 0);
            this.content.Controls.SetChildIndex(this.lblVIEW_BOX_CUSTOMER, 0);
            // 
            // txtOLD_BOX_POLE
            // 
            resources.ApplyResources(this.txtOLD_BOX_POLE, "txtOLD_BOX_POLE");
            this.txtOLD_BOX_POLE.Name = "txtOLD_BOX_POLE";
            this.txtOLD_BOX_POLE.ReadOnly = true;
            this.txtOLD_BOX_POLE.TabStop = false;
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // lblCUTTER
            // 
            resources.ApplyResources(this.lblCUTTER, "lblCUTTER");
            this.lblCUTTER.Name = "lblCUTTER";
            // 
            // lblBILLER
            // 
            resources.ApplyResources(this.lblBILLER, "lblBILLER");
            this.lblBILLER.Name = "lblBILLER";
            // 
            // lblCOLLECTOR
            // 
            resources.ApplyResources(this.lblCOLLECTOR, "lblCOLLECTOR");
            this.lblCOLLECTOR.Name = "lblCOLLECTOR";
            // 
            // txtOLD_BOX_CUTTER
            // 
            resources.ApplyResources(this.txtOLD_BOX_CUTTER, "txtOLD_BOX_CUTTER");
            this.txtOLD_BOX_CUTTER.Name = "txtOLD_BOX_CUTTER";
            this.txtOLD_BOX_CUTTER.ReadOnly = true;
            this.txtOLD_BOX_CUTTER.TabStop = false;
            // 
            // txtOLD_BOX_BILLER
            // 
            resources.ApplyResources(this.txtOLD_BOX_BILLER, "txtOLD_BOX_BILLER");
            this.txtOLD_BOX_BILLER.Name = "txtOLD_BOX_BILLER";
            this.txtOLD_BOX_BILLER.ReadOnly = true;
            this.txtOLD_BOX_BILLER.TabStop = false;
            // 
            // txtOLD_BOX_COLLECTOR
            // 
            resources.ApplyResources(this.txtOLD_BOX_COLLECTOR, "txtOLD_BOX_COLLECTOR");
            this.txtOLD_BOX_COLLECTOR.Name = "txtOLD_BOX_COLLECTOR";
            this.txtOLD_BOX_COLLECTOR.ReadOnly = true;
            this.txtOLD_BOX_COLLECTOR.TabStop = false;
            // 
            // lblBOX
            // 
            resources.ApplyResources(this.lblBOX, "lblBOX");
            this.lblBOX.Name = "lblBOX";
            // 
            // lblOLD_BOX
            // 
            resources.ApplyResources(this.lblOLD_BOX, "lblOLD_BOX");
            this.lblOLD_BOX.Name = "lblOLD_BOX";
            // 
            // lblPOLE
            // 
            resources.ApplyResources(this.lblPOLE, "lblPOLE");
            this.lblPOLE.Name = "lblPOLE";
            // 
            // txtOLD_BOX
            // 
            this.txtOLD_BOX.BackColor = System.Drawing.Color.White;
            this.txtOLD_BOX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtOLD_BOX, "txtOLD_BOX");
            this.txtOLD_BOX.Name = "txtOLD_BOX";
            this.txtOLD_BOX.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtOLD_BOX.AdvanceSearch += new System.EventHandler(this.txtMeter_AdvanceSearch);
            this.txtOLD_BOX.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // lblNEW_BOX
            // 
            resources.ApplyResources(this.lblNEW_BOX, "lblNEW_BOX");
            this.lblNEW_BOX.Name = "lblNEW_BOX";
            // 
            // lblCUSTOMER_INFORMATION
            // 
            resources.ApplyResources(this.lblCUSTOMER_INFORMATION, "lblCUSTOMER_INFORMATION");
            this.lblCUSTOMER_INFORMATION.Name = "lblCUSTOMER_INFORMATION";
            // 
            // txtCUSTOMER_NAME
            // 
            resources.ApplyResources(this.txtCUSTOMER_NAME, "txtCUSTOMER_NAME");
            this.txtCUSTOMER_NAME.Name = "txtCUSTOMER_NAME";
            this.txtCUSTOMER_NAME.ReadOnly = true;
            this.txtCUSTOMER_NAME.TabStop = false;
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // txtCUSTOMER_CODE
            // 
            resources.ApplyResources(this.txtCUSTOMER_CODE, "txtCUSTOMER_CODE");
            this.txtCUSTOMER_CODE.Name = "txtCUSTOMER_CODE";
            this.txtCUSTOMER_CODE.ReadOnly = true;
            this.txtCUSTOMER_CODE.TabStop = false;
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // txtNEW_BOX_POLE
            // 
            resources.ApplyResources(this.txtNEW_BOX_POLE, "txtNEW_BOX_POLE");
            this.txtNEW_BOX_POLE.Name = "txtNEW_BOX_POLE";
            this.txtNEW_BOX_POLE.ReadOnly = true;
            this.txtNEW_BOX_POLE.TabStop = false;
            // 
            // lblCUTTER_1
            // 
            resources.ApplyResources(this.lblCUTTER_1, "lblCUTTER_1");
            this.lblCUTTER_1.Name = "lblCUTTER_1";
            // 
            // lblBILLER_1
            // 
            resources.ApplyResources(this.lblBILLER_1, "lblBILLER_1");
            this.lblBILLER_1.Name = "lblBILLER_1";
            // 
            // lblCOLLECTOR_1
            // 
            resources.ApplyResources(this.lblCOLLECTOR_1, "lblCOLLECTOR_1");
            this.lblCOLLECTOR_1.Name = "lblCOLLECTOR_1";
            // 
            // txtNEW_BOX_CUTTER
            // 
            resources.ApplyResources(this.txtNEW_BOX_CUTTER, "txtNEW_BOX_CUTTER");
            this.txtNEW_BOX_CUTTER.Name = "txtNEW_BOX_CUTTER";
            this.txtNEW_BOX_CUTTER.ReadOnly = true;
            this.txtNEW_BOX_CUTTER.TabStop = false;
            // 
            // txtNEW_BOX_BILLER
            // 
            resources.ApplyResources(this.txtNEW_BOX_BILLER, "txtNEW_BOX_BILLER");
            this.txtNEW_BOX_BILLER.Name = "txtNEW_BOX_BILLER";
            this.txtNEW_BOX_BILLER.ReadOnly = true;
            this.txtNEW_BOX_BILLER.TabStop = false;
            // 
            // txtNEW_BOX_COLLECTOR
            // 
            resources.ApplyResources(this.txtNEW_BOX_COLLECTOR, "txtNEW_BOX_COLLECTOR");
            this.txtNEW_BOX_COLLECTOR.Name = "txtNEW_BOX_COLLECTOR";
            this.txtNEW_BOX_COLLECTOR.ReadOnly = true;
            this.txtNEW_BOX_COLLECTOR.TabStop = false;
            // 
            // lblBOX_1
            // 
            resources.ApplyResources(this.lblBOX_1, "lblBOX_1");
            this.lblBOX_1.Name = "lblBOX_1";
            // 
            // lblPOLE_1
            // 
            resources.ApplyResources(this.lblPOLE_1, "lblPOLE_1");
            this.lblPOLE_1.Name = "lblPOLE_1";
            // 
            // txtNEW_BOX
            // 
            this.txtNEW_BOX.BackColor = System.Drawing.Color.White;
            this.txtNEW_BOX.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtNEW_BOX, "txtNEW_BOX");
            this.txtNEW_BOX.Name = "txtNEW_BOX";
            this.txtNEW_BOX.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtNEW_BOX.AdvanceSearch += new System.EventHandler(this.txtNEW_METER_AdvanceSearch);
            this.txtNEW_BOX.CancelAdvanceSearch += new System.EventHandler(this.txtNEW_METER_CancelAdvanceSearch);
            this.txtNEW_BOX.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblCHANGE_DATE
            // 
            resources.ApplyResources(this.lblCHANGE_DATE, "lblCHANGE_DATE");
            this.lblCHANGE_DATE.Name = "lblCHANGE_DATE";
            // 
            // dtpCHANGE_DATE
            // 
            resources.ApplyResources(this.dtpCHANGE_DATE, "dtpCHANGE_DATE");
            this.dtpCHANGE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCHANGE_DATE.Name = "dtpCHANGE_DATE";
            this.dtpCHANGE_DATE.ValueChanged += new System.EventHandler(this.dtpActivateDate_ValueChanged);
            // 
            // txtNEWPositionInBox
            // 
            resources.ApplyResources(this.txtNEWPositionInBox, "txtNEWPositionInBox");
            this.txtNEWPositionInBox.Name = "txtNEWPositionInBox";
            this.txtNEWPositionInBox.TabStop = false;
            this.txtNEWPositionInBox.Enter += new System.EventHandler(this.InputEnglish);
            this.txtNEWPositionInBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNewPositionInBox_KeyPress);
            // 
            // lblPOSITION_IN_BOX_1
            // 
            resources.ApplyResources(this.lblPOSITION_IN_BOX_1, "lblPOSITION_IN_BOX_1");
            this.lblPOSITION_IN_BOX_1.Name = "lblPOSITION_IN_BOX_1";
            // 
            // lblPOSITION_IN_BOX
            // 
            resources.ApplyResources(this.lblPOSITION_IN_BOX, "lblPOSITION_IN_BOX");
            this.lblPOSITION_IN_BOX.Name = "lblPOSITION_IN_BOX";
            // 
            // txtOLDPositionInBox
            // 
            resources.ApplyResources(this.txtOLDPositionInBox, "txtOLDPositionInBox");
            this.txtOLDPositionInBox.Name = "txtOLDPositionInBox";
            this.txtOLDPositionInBox.ReadOnly = true;
            this.txtOLDPositionInBox.TabStop = false;
            // 
            // lblVIEW_BOX_CUSTOMER
            // 
            resources.ApplyResources(this.lblVIEW_BOX_CUSTOMER, "lblVIEW_BOX_CUSTOMER");
            this.lblVIEW_BOX_CUSTOMER.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblVIEW_BOX_CUSTOMER.Name = "lblVIEW_BOX_CUSTOMER";
            this.lblVIEW_BOX_CUSTOMER.TabStop = true;
            this.lblVIEW_BOX_CUSTOMER.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblViewBoxCustomers_LinkClicked);
            // 
            // DialogCustomerChangeBox
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerChangeBox";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtCUSTOMER_CODE;
        private Label lblCUSTOMER_CODE;
        private TextBox txtCUSTOMER_NAME;
        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_INFORMATION;
        private Label label3;
        private Label lblNEW_BOX;
        private TextBox txtOLD_BOX_POLE;
        private Label label14;
        private Label lblCUTTER;
        private Label lblBILLER;
        private Label lblCOLLECTOR;
        private TextBox txtOLD_BOX_CUTTER;
        private TextBox txtOLD_BOX_BILLER;
        private TextBox txtOLD_BOX_COLLECTOR;
        private Label lblBOX;
        private Label lblOLD_BOX;
        private Label lblPOLE;
        private ExTextbox txtOLD_BOX;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel1;
        private TextBox txtNEW_BOX_POLE;
        private Label lblCUTTER_1;
        private Label lblBILLER_1;
        private Label lblCOLLECTOR_1;
        private TextBox txtNEW_BOX_CUTTER;
        private TextBox txtNEW_BOX_BILLER;
        private TextBox txtNEW_BOX_COLLECTOR;
        private Label lblBOX_1;
        private Label lblPOLE_1;
        private ExTextbox txtNEW_BOX;
        private DateTimePicker dtpCHANGE_DATE;
        private Label lblCHANGE_DATE;
        private TextBox txtOLDPositionInBox;
        private TextBox txtNEWPositionInBox;
        private Label lblPOSITION_IN_BOX;
        private Label lblPOSITION_IN_BOX_1;
        private ExLinkLabel lblVIEW_BOX_CUSTOMER;
    }
}