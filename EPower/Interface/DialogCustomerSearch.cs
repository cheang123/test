﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerSearch : ExDialog
    {
        private bool _loading = false;
        private DataTable _dtSource = null;
        private PowerType _flagPower;

        public enum PowerType
        {
            Prepaid = 0,
            Postpaid = 1,
            AllType
        }

        /// <summary>
        /// Dialog seach customer specify by search keyword, area, and customer type.
        /// </summary>
        /// <param name="search">String to seach in name, khmer name , phone address.</param>
        /// <param name="areaId">Area id to filter (0 for all area).</param>
        /// <param name="typeId">Customer Type Id to filter (0 for all type).</param>
        public DialogCustomerSearch(string search, int areaId, int typeId, PowerType flagPower)
        {
            InitializeComponent();
            _flagPower = flagPower;
            this.txtSearch.TextBox.KeyDown += new KeyEventHandler(TextBox_KeyDown);
            this._loading = true;
            //DataTable customerType = DBDataContext.Db.TBL_CUSTOMER_TYPEs.Where(row=>row.IS_ACTIVE || row.CUSTOMER_TYPE_ID == -1)._ToDataTable();
            //DataRow allType = customerType.NewRow();
            //allType[0] = "0";
            //allType[1] = Resources.DisplayAllCustomer;
            //customerType.Rows.InsertAt(allType, 0);
            UIHelper.SetDataSourceToComboBox(this.cboCustomerGroup, Lookup.GetCustomerGroup(), Resources.All_CUSTOMER_GROUP);

            DataTable area = DBDataContext.Db.TBL_AREAs.Where(row => row.IS_ACTIVE).OrderBy(row => row.AREA_NAME)._ToDataTable();
            DataRow allArea = area.NewRow();
            allArea[0] = "0";
            allArea[1] = Resources.ALL_AREA;
            area.Rows.InsertAt(allArea, 0);
            UIHelper.SetDataSourceToComboBox(cboArea, area);

            // initial search condition.
            this.txtSearch.Text = search;
            this.cboArea.SelectedValue = areaId;
            this.cboConnectionType.SelectedValue = typeId;

            this._loading = false;

            this.bindData();
        }
        void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
            {
                if (this.dgv.Rows.Count > 0)
                {
                    this.dgv.Focus();
                }
            }
            if (e.KeyCode == Keys.Enter)
            {
                if (this.dgv.Rows.Count == 1)
                {
                    this.DialogResult = DialogResult.OK;
                }
            }
        }
        /// <summary>
        /// Seach customer in all area and all type.
        /// </summary>
        /// <param name="search"></param>
        public DialogCustomerSearch(string search, PowerType flagPower) : this(search, 0, 0, flagPower)
        {

        }

        private void bindData()
        {
            this.lblTYPE_TO_SEARCH.Hide();
            this.lblROW_NOT_FOUND.Hide();

            if (_loading)
            {
                return;
            }

            if (this.txtSearch.Text == "")
            {
                if (this._dtSource != null)
                {
                    this._dtSource.Rows.Clear();
                }
                this.lblTYPE_TO_SEARCH.Show();
                return;
            }


            int areaId = (int)this.cboArea.SelectedValue;
            int typeID = cboConnectionType.SelectedValue == null ? 0 : (int)cboConnectionType.SelectedValue;
            int cusGroupId = (int)cboCustomerGroup.SelectedValue;
            int statusID = 0;
            bool powerType = true;
            if (_flagPower != PowerType.AllType)
            {
                powerType = Convert.ToBoolean(_flagPower);
            }

            string search = this.txtSearch.Text.ToLower();

            this._dtSource = (from cus in DBDataContext.Db.TBL_CUSTOMERs
                              .Where(x => x.STATUS_ID != (int)CustomerStatus.Closed && x.STATUS_ID != (int)CustomerStatus.Cancelled)
                              join type in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on cus.CUSTOMER_CONNECTION_TYPE_ID equals type.CUSTOMER_CONNECTION_TYPE_ID
                              join gp in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on type.NONLICENSE_CUSTOMER_GROUP_ID equals gp.CUSTOMER_GROUP_ID
                              join area in DBDataContext.Db.TBL_AREAs on cus.AREA_ID equals area.AREA_ID
                              join cm in
                                  (
                                      from m in DBDataContext.Db.TBL_METERs
                                      join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on m.METER_ID equals cm.METER_ID
                                      join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                                      join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                                      where cm.IS_ACTIVE
                                      select new { cm.CUSTOMER_ID, m.METER_CODE, p.POLE_CODE, b.BOX_CODE }
                                      ) on cus.CUSTOMER_ID equals cm.CUSTOMER_ID into cmTmp
                              from cmt in cmTmp.DefaultIfEmpty()
                              where (cusGroupId == 0 || type.NONLICENSE_CUSTOMER_GROUP_ID == cusGroupId)
                                && (typeID == 0 || cus.CUSTOMER_CONNECTION_TYPE_ID == typeID)
                                && (statusID == 0 || cus.STATUS_ID == statusID)
                                && (areaId == 0 || cus.AREA_ID == areaId)
                                && (_flagPower == PowerType.AllType || cus.IS_POST_PAID == powerType)
                                && ((cus.CUSTOMER_CODE + " " + cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH + " " + cus.LAST_NAME + " " + cus.FIRST_NAME + " " + cus.PHONE_1 + " " + cus.PHONE_2 + " " +
                                     (cmt == null ? "" : cmt.POLE_CODE) + " " +
                                     (cmt == null ? "" : cmt.BOX_CODE) + " " +
                                     (cmt == null ? "" : cmt.METER_CODE)
                                   ).ToLower().Contains(this.txtSearch.Text.ToLower()))
                              select new
                              {
                                  CUSTOMER_ID = cus.CUSTOMER_ID,
                                  CUSTOMER_CODE = cus.CUSTOMER_CODE,
                                  CUSTOMER_NAME = cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH,
                                  CUSTOMER_TYPE = type.CUSTOMER_CONNECTION_TYPE_NAME,
                                  PHONE = cus.PHONE_1,
                                  AREA = area.AREA_NAME,
                                  POLE = cmt == null ? "" : cmt.POLE_CODE,
                                  BOX = cmt == null ? "" : cmt.BOX_CODE,
                                  METER = cmt == null ? "" : cmt.METER_CODE,
                                  CUSTOMER_STATUS = cus.STATUS_ID
                              }).Take(24)._ToDataTable();

            this.dgv.DataSource = this._dtSource;

            //set ForeColor to Red if customer is close.
            foreach (DataGridViewRow item in dgv.Rows)
            {
                int status_id = (int)item.Cells["CUSTOMER_STATUS"].Value;
                if (status_id == (int)CustomerStatus.Closed || status_id == (int)CustomerStatus.Cancelled)
                {
                    item.DefaultCellStyle.ForeColor = Color.Red;
                    item.DefaultCellStyle.SelectionForeColor = Color.Red;
                }
                else if (status_id == (int)CustomerStatus.Blocked)
                {
                    item.DefaultCellStyle.ForeColor = Color.DarkOrange;
                    item.DefaultCellStyle.SelectionForeColor = Color.DarkOrange;
                }
                else if (status_id == (int)CustomerStatus.Pending)
                {
                    item.DefaultCellStyle.ForeColor = Color.DarkBlue;
                    item.DefaultCellStyle.SelectionForeColor = Color.DarkBlue;
                }
            }

            if (this.dgv.Rows.Count == 0)
            {
                this.lblROW_NOT_FOUND.Show();
            }
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            bindData();
        }

        private void cboArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindData();
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindData();
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1)
            {
                if (this.dgv.SelectedRows.Count == 1)
                {
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        public int CustomerID
        {
            get
            {
                int id = 0;
                if (this.dgv.SelectedRows.Count == 1)
                {
                    id = (int)this.dgv.SelectedRows[0].Cells["CUSTOMER_ID"].Value;
                }
                return id;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 1)
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void dgv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                e.Handled = true;
                if (this.dgv.SelectedRows.Count == 1)
                {
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void lblRowNotFound_Click(object sender, EventArgs e)
        {

        }

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int groupId = DataHelper.ParseToInt(cboCustomerGroup.SelectedValue.ToString());
            UIHelper.SetDataSourceToComboBox(cboConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                                                                .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId)
                                                                .OrderBy(x => x.DESCRIPTION)
                                                                , Resources.ALL_CONNECTION_TYPE);
            if (cboConnectionType.DataSource != null)
            {
                cboConnectionType.SelectedIndex = 0;
            }
        }
    }
}
