﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class PageCustomerList : Form
    {
        /// <summary>
        /// Determines the binding are being process on Status ComboBox and Type ComboBox.
        /// </summary>
        private bool _binding = false;

        private DataTable _ds = null;

        public PageCustomerList()
        {
            InitializeComponent();

            //Assign permission
            btnADD_SERVICE.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_FIXCHARGE);
            btnADD.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_INSERT);
            btnEDIT.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE);
            btnCLOSE_CUSTOMER.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_CLOSE);

            UIHelper.DataGridViewProperties(dgv);

            BindCustomerType();

            BindCustomerStatus();

            this.txtStatus.SelectedValue = 0;

            BindData();
        }

        public void BindCustomerStatus()
        {
            _binding = true;
            UIHelper.SetDataSourceToComboBox(this.txtStatus, Lookup.GetCustomerStatuses(), Resources.ALL_STATUS);
            _binding = false;
        }
        public void BindCustomerType()
        {
            _binding = true;
            UIHelper.SetDataSourceToComboBox(this.cboCustomerGroup, DBDataContext.Db.TLKP_CUSTOMER_GROUPs.OrderBy(x => x.DESCRIPTION), Resources.All_CUSTOMER_GROUP);
            _binding = false;
        }

        public void BindData()
        {
            // if statust and type is in binding mode 
            // then do not bind customer list
            // wait until status and customer type 
            // are completed bound.
            if (this._binding)
            {
                return;
            }

            // lock to make sure only one search wash hit to server.
            this._binding = true;

            this.lblROW_NOT_FOUND.Hide();
            this.lblTYPE_TO_SEARCH.Hide();
            if (this.txtSearch.Text == "")
            {
                if (this._ds != null)
                {
                    this._ds.Rows.Clear();
                }
                this.lblTYPE_TO_SEARCH.Show();
                this._binding = false;
                return;
            }

            int statusID = (int)this.txtStatus.SelectedValue;
            int typeID = cboConnectionType.SelectedValue == null ? 0 : (int)cboConnectionType.SelectedValue;
            int cusGroupId = (int)cboCustomerGroup.SelectedValue;

            this._ds = (from cus in DBDataContext.Db.TBL_CUSTOMERs
                        join ct in DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs on cus.CUSTOMER_CONNECTION_TYPE_ID equals ct.CUSTOMER_CONNECTION_TYPE_ID
                        join g in DBDataContext.Db.TLKP_CUSTOMER_GROUPs on ct.NONLICENSE_CUSTOMER_GROUP_ID equals g.CUSTOMER_GROUP_ID
                        join area in DBDataContext.Db.TBL_AREAs on cus.AREA_ID equals area.AREA_ID
                        join cm in (
                                from m in DBDataContext.Db.TBL_METERs
                                join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on m.METER_ID equals cm.METER_ID
                                join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                                join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                                where cm.IS_ACTIVE
                                select new { cm.CUSTOMER_ID, m.METER_CODE, p.POLE_CODE, b.BOX_CODE }
                             ) on cus.CUSTOMER_ID equals cm.CUSTOMER_ID into cmTmp
                        from cmt in cmTmp.DefaultIfEmpty()
                        where (cusGroupId == 0 || ct.NONLICENSE_CUSTOMER_GROUP_ID == cusGroupId)
                          && (typeID == 0 || cus.CUSTOMER_CONNECTION_TYPE_ID == typeID)
                          && (statusID == 0 || cus.STATUS_ID == statusID)
                          && cus.IS_POST_PAID
                          && ((cus.CUSTOMER_CODE + " " + cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH + " " + cus.LAST_NAME + " " + cus.FIRST_NAME + " " + cus.PHONE_1 + " " + cus.PHONE_2 + " " +
                               (cmt == null ? "" : cmt.POLE_CODE) + " " +
                               (cmt == null ? "" : cmt.BOX_CODE) + " " +
                               (cmt == null ? "" : cmt.METER_CODE)
                             ).ToLower().Contains(this.txtSearch.Text.ToLower()))
                        select new
                        {
                            CUSTOMER_ID = cus.CUSTOMER_ID,
                            CUSTOMER_CODE = cus.CUSTOMER_CODE,
                            CUSTOMER_NAME = cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH,
                            CUSTOMER_TYPE = g.CUSTOMER_GROUP_ID != 4 && g.CUSTOMER_GROUP_ID != 5 ? g.CUSTOMER_GROUP_NAME + " (" + ct.CUSTOMER_CONNECTION_TYPE_NAME + ")" : ct.CUSTOMER_CONNECTION_TYPE_NAME,
                            PHONE = cus.PHONE_1,
                            AREA = area.AREA_NAME,
                            POLE = cmt == null ? "" : cmt.POLE_CODE,
                            BOX = cmt == null ? "" : cmt.BOX_CODE,
                            METER = cmt == null ? "" : cmt.METER_CODE,
                            CUSTOMER_STATUS = cus.STATUS_ID
                        }).Take(24)._ToDataTable();

            this.dgv.DataSource = this._ds;
            if (this.dgv.Rows.Count == 0)
            {
                this.lblROW_NOT_FOUND.Show();
            }

            //set ForeColor to Red if customer is close.
            foreach (DataGridViewRow item in dgv.Rows)
            {
                int status_id = (int)item.Cells[CUSTOMER_STATUS.Name].Value;
                if (status_id == (int)CustomerStatus.Closed || status_id == (int)CustomerStatus.Cancelled)
                {
                    item.DefaultCellStyle.ForeColor = Color.Red;
                    item.DefaultCellStyle.SelectionForeColor = Color.Red;
                }
                else if (status_id == (int)CustomerStatus.Blocked)
                {
                    item.DefaultCellStyle.ForeColor = Color.DarkOrange;
                    item.DefaultCellStyle.SelectionForeColor = Color.DarkOrange;
                }
                else if (status_id == (int)CustomerStatus.Pending)
                {
                    item.DefaultCellStyle.ForeColor = Color.DarkBlue;
                    item.DefaultCellStyle.SelectionForeColor = Color.DarkBlue;
                }
            }

            this._binding = false;

        }

        #region Events
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0)
            {
                return;
            }
            int id = (int)this.dgv.SelectedRows[0].Cells[CUSTOMER_ID.Name].Value;
            TBL_CUSTOMER obj = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == id);
            if (obj != null)
            {
                DialogCustomer diag = new DialogCustomer(GeneralProcess.Update, obj);
                if (diag.ShowDialog(this) == DialogResult.OK)
                {
                    BindData();
                    UIHelper.SelectRow(dgv, diag.Object.CUSTOMER_ID);
                }
            }
            PostingLogic.Instance.PostToPointerBg(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogCustomer diag = new DialogCustomer(GeneralProcess.Insert, new TBL_CUSTOMER());
            if (diag.ShowDialog(this) == DialogResult.OK)
            {
                BindData();
                UIHelper.SelectRow(dgv, diag.Object.CUSTOMER_ID);
            }
        }

        private void btnAddCharge_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0)
            {
                return;
            }
            if (Base.Logic.Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_ADD_SERVICE, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Base.Logic.Login.CurrentCashDrawer == null)
                    return;
            }

            int id = (int)this.dgv.SelectedRows[0].Cells[CUSTOMER_ID.Name].Value;

            TBL_CUSTOMER obj = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == id);

            if (obj.STATUS_ID == (int)CustomerStatus.Closed)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_ADD_CHARGE_TO_CLOSE_CUSTOMER);
                return;
            }
            if (obj.STATUS_ID == (int)CustomerStatus.Cancelled)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_ADD_CHARGE_TO_CANCEL_CUSTOMER);
                return;
            }
            if (obj != null)
            {
                (new DialogCustomerCharge(obj, GeneralProcess.Insert)).ShowDialog();
                PostingLogic.Instance.PostToPointerBg(DateTime.Now.Date, DateTime.Now.Date.AddDays(1).AddSeconds(-1));
            }
        }

        private void txtCustomerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        private void txtStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtStatus.SelectedIndex != -1)
            {
                int statusID = (int)txtStatus.SelectedValue;
                btnADD_SERVICE.Enabled = statusID._In(0, (int)CustomerStatus.Active, (int)CustomerStatus.Blocked, (int)CustomerStatus.Pending)
                    && Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_FIXCHARGE);

                btnCLOSE_CUSTOMER.Enabled = statusID._In(0, (int)CustomerStatus.Active, (int)CustomerStatus.Blocked)
                    && Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_CLOSE);
            }
            BindData();
        }

        private void exTextbox1_QuickSearch(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnCloseCustomer_Click(object sender, EventArgs e)
        {
            if (Base.Logic.Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER_BEFORE_RECEIVE_PAYMENT, string.Empty) == DialogResult.Yes)
                    new DialogOpenCashDrawer().ShowDialog();
                if (Base.Logic.Login.CurrentCashDrawer == null)
                    return;
            }
            if (dgv.SelectedRows.Count == 0)
            {
                return;
            }
            int intCustomerId = int.Parse(dgv.SelectedRows[0].Cells[CUSTOMER_ID.Name].Value.ToString());
            TBL_CUSTOMER obj = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(c => c.CUSTOMER_ID == intCustomerId);

            if (obj.STATUS_ID == (int)CustomerStatus.Closed)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_ALREADY_CLOSE);
                return;
            }

            if (obj.STATUS_ID == (int)CustomerStatus.Cancelled)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_ALREDY_CANCEL);
                return;
            }
            if (obj.STATUS_ID == (int)CustomerStatus.Pending)
            {
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_PENDING);
                return;
            }
            if (obj.USAGE_CUSTOMER_ID == obj.CUSTOMER_ID)// || obj.INVOICE_CUSTOMER_ID == obj.CUSTOMER_ID)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE_PARENT_OF_MERGE_CUSTOMER);
                return;
            }
            if (obj.USAGE_CUSTOMER_ID != 0)// || obj.INVOICE_CUSTOMER_ID != 0)
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE_CHILD_OF_MERGE_CUSTOMER);
                return;
            }
            DialogCloseCustomer diaglogCloseCustomer = new DialogCloseCustomer(obj);
            if (diaglogCloseCustomer.ShowDialog() == DialogResult.OK)
            {
                BindData();
            }
        }

        private void txtSearch_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            if (!Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE))
            {
                return;
            }

            btnEdit_Click(null, null);
        }

        private void cboCustomerGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int groupId = DataHelper.ParseToInt(cboCustomerGroup.SelectedValue.ToString());
            UIHelper.SetDataSourceToComboBox(cboConnectionType, DBDataContext.Db.TLKP_CUSTOMER_CONNECTION_TYPEs
                                                                .Where(x => x.NONLICENSE_CUSTOMER_GROUP_ID == groupId)
                                                                .OrderBy(x => x.DESCRIPTION)
                                                                , Resources.ALL_CONNECTION_TYPE);
            if (cboConnectionType.DataSource != null)
            {
                cboConnectionType.SelectedIndex = 0;
            }
        }
        
        #endregion
    }
}
