﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech.Component;
using System;
using System.Drawing.Printing;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogPrintSetup : ExDialog
    {
        public DialogPrintSetup()
        {
            InitializeComponent();
            bind();
        }

        private void bind()
        {
            this.cboPrinterInvoice.Items.Add("");
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                this.cboPrinterInvoice.Items.Add(printer);
            }
            this.cboPrinterInvoice.Text = Settings.Default.PRINTER_INVOICE;

            // for the first time if user is not set default report for print invoice 
            if (Settings.Default.REPORT_INVOICE == null || Settings.Default.REPORT_INVOICE == "")
            {
                Settings.Default.REPORT_INVOICE = "ReportInvoice.rpt";
                Settings.Default.Save();
            }

            string invoices = Method.Utilities[Utility.REPORT_INVOICE_LIST].Replace(" ", "");
            foreach (var inv in invoices.Split(';'))
            {
                if (inv != "")
                {
                    cboReport.Items.Add(inv);
                }
            }
            cboReport.Text = Settings.Default.REPORT_INVOICE;
            if (cboReport.Text == "")
            {
                cboReport.SelectedIndex = 0;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Settings.Default.PRINTER_INVOICE = this.cboPrinterInvoice.Text;
            Settings.Default.REPORT_INVOICE = this.cboReport.Text;
            Settings.Default.Save();
            this.DialogResult = DialogResult.OK;
        }
    }
}