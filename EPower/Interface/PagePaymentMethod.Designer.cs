﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PagePaymentMethod
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PagePaymentMethod));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panel1 = new System.Windows.Forms.Panel();
            this.searchControl = new DevExpress.XtraEditors.SearchControl();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.dgvPaymentMethod = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPAYMENT_METHOD_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPAYMENT_METHOD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCREATE_BY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCREATE_ON = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIS_ACTIVE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colACCOUNT_KHR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.resAccount = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colACCOUNT_USD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colACCOUNT_THB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colACCOUNT_VND = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaymentMethod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.searchControl);
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // searchControl
            // 
            this.searchControl.Client = this.dgv;
            resources.ApplyResources(this.searchControl, "searchControl");
            this.searchControl.Name = "searchControl";
            this.searchControl.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.Appearance.Font")));
            this.searchControl.Properties.Appearance.Options.UseFont = true;
            this.searchControl.Properties.AppearanceDisabled.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceDisabled.Font")));
            this.searchControl.Properties.AppearanceDisabled.Options.UseFont = true;
            this.searchControl.Properties.AppearanceDropDown.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceDropDown.Font")));
            this.searchControl.Properties.AppearanceDropDown.Options.UseFont = true;
            this.searchControl.Properties.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceFocused.Font")));
            this.searchControl.Properties.AppearanceFocused.Options.UseFont = true;
            this.searchControl.Properties.AppearanceItemDisabled.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceItemDisabled.Font")));
            this.searchControl.Properties.AppearanceItemDisabled.Options.UseFont = true;
            this.searchControl.Properties.AppearanceItemHighlight.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceItemHighlight.Font")));
            this.searchControl.Properties.AppearanceItemHighlight.Options.UseFont = true;
            this.searchControl.Properties.AppearanceItemSelected.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceItemSelected.Font")));
            this.searchControl.Properties.AppearanceItemSelected.Options.UseFont = true;
            this.searchControl.Properties.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("searchControl.Properties.AppearanceReadOnly.Font")));
            this.searchControl.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.searchControl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.searchControl.Properties.Client = this.dgv;
            this.searchControl.Properties.DropDownRows = 10;
            this.searchControl.Properties.Padding = new System.Windows.Forms.Padding(1);
            // 
            // dgv
            // 
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EmbeddedNavigator.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dgv.EmbeddedNavigator.Appearance.Font")));
            this.dgv.EmbeddedNavigator.Appearance.Options.UseFont = true;
            this.dgv.MainView = this.dgvPaymentMethod;
            this.dgv.Name = "dgv";
            this.dgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.resAccount});
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.dgvPaymentMethod});
            // 
            // dgvPaymentMethod
            // 
            this.dgvPaymentMethod.Appearance.ColumnFilterButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.ColumnFilterButton.Font")));
            this.dgvPaymentMethod.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.ColumnFilterButtonActive.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.ColumnFilterButtonActive.Font")));
            this.dgvPaymentMethod.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.CustomizationFormHint.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.CustomizationFormHint.Font")));
            this.dgvPaymentMethod.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.DetailTip.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.DetailTip.Font")));
            this.dgvPaymentMethod.Appearance.DetailTip.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.Empty.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.Empty.Font")));
            this.dgvPaymentMethod.Appearance.Empty.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.EvenRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.EvenRow.Font")));
            this.dgvPaymentMethod.Appearance.EvenRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.FilterCloseButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.FilterCloseButton.Font")));
            this.dgvPaymentMethod.Appearance.FilterCloseButton.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.FilterPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.FilterPanel.Font")));
            this.dgvPaymentMethod.Appearance.FilterPanel.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.FixedLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.FixedLine.Font")));
            this.dgvPaymentMethod.Appearance.FixedLine.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.FocusedCell.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.FocusedCell.Font")));
            this.dgvPaymentMethod.Appearance.FocusedCell.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.FocusedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.FocusedRow.Font")));
            this.dgvPaymentMethod.Appearance.FocusedRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.FooterPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.FooterPanel.Font")));
            this.dgvPaymentMethod.Appearance.FooterPanel.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.GroupButton.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.GroupButton.Font")));
            this.dgvPaymentMethod.Appearance.GroupButton.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.GroupFooter.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.GroupFooter.Font")));
            this.dgvPaymentMethod.Appearance.GroupFooter.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.GroupPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.GroupPanel.Font")));
            this.dgvPaymentMethod.Appearance.GroupPanel.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.GroupRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.GroupRow.Font")));
            this.dgvPaymentMethod.Appearance.GroupRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.HeaderPanel.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.HeaderPanel.Font")));
            this.dgvPaymentMethod.Appearance.HeaderPanel.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.HideSelectionRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.HideSelectionRow.Font")));
            this.dgvPaymentMethod.Appearance.HideSelectionRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.HorzLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.HorzLine.Font")));
            this.dgvPaymentMethod.Appearance.HorzLine.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.HotTrackedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.HotTrackedRow.Font")));
            this.dgvPaymentMethod.Appearance.HotTrackedRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.OddRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.OddRow.Font")));
            this.dgvPaymentMethod.Appearance.OddRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.Preview.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.Preview.Font")));
            this.dgvPaymentMethod.Appearance.Preview.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.Row.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.Row.Font")));
            this.dgvPaymentMethod.Appearance.Row.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.RowSeparator.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.RowSeparator.Font")));
            this.dgvPaymentMethod.Appearance.RowSeparator.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.SelectedRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.SelectedRow.Font")));
            this.dgvPaymentMethod.Appearance.SelectedRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.TopNewRow.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.TopNewRow.Font")));
            this.dgvPaymentMethod.Appearance.TopNewRow.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.VertLine.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.VertLine.Font")));
            this.dgvPaymentMethod.Appearance.VertLine.Options.UseFont = true;
            this.dgvPaymentMethod.Appearance.ViewCaption.Font = ((System.Drawing.Font)(resources.GetObject("dgvPaymentMethod.Appearance.ViewCaption.Font")));
            this.dgvPaymentMethod.Appearance.ViewCaption.Options.UseFont = true;
            this.dgvPaymentMethod.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPAYMENT_METHOD_ID,
            this.colPAYMENT_METHOD,
            this.colCREATE_BY,
            this.colCREATE_ON,
            this.colIS_ACTIVE,
            this.colACCOUNT_KHR,
            this.colACCOUNT_USD,
            this.colACCOUNT_THB,
            this.colACCOUNT_VND});
            this.dgvPaymentMethod.GridControl = this.dgv;
            this.dgvPaymentMethod.Name = "dgvPaymentMethod";
            this.dgvPaymentMethod.OptionsBehavior.Editable = false;
            this.dgvPaymentMethod.OptionsBehavior.ReadOnly = true;
            this.dgvPaymentMethod.OptionsView.ShowGroupPanel = false;
            this.dgvPaymentMethod.OptionsView.ShowIndicator = false;
            // 
            // colPAYMENT_METHOD_ID
            // 
            resources.ApplyResources(this.colPAYMENT_METHOD_ID, "colPAYMENT_METHOD_ID");
            this.colPAYMENT_METHOD_ID.FieldName = "PAYMENT_METHOD_ID";
            this.colPAYMENT_METHOD_ID.Name = "colPAYMENT_METHOD_ID";
            // 
            // colPAYMENT_METHOD
            // 
            resources.ApplyResources(this.colPAYMENT_METHOD, "colPAYMENT_METHOD");
            this.colPAYMENT_METHOD.FieldName = "PAYMENT_METHOD_NAME";
            this.colPAYMENT_METHOD.Name = "colPAYMENT_METHOD";
            // 
            // colCREATE_BY
            // 
            resources.ApplyResources(this.colCREATE_BY, "colCREATE_BY");
            this.colCREATE_BY.FieldName = "CREATE_BY";
            this.colCREATE_BY.Name = "colCREATE_BY";
            // 
            // colCREATE_ON
            // 
            resources.ApplyResources(this.colCREATE_ON, "colCREATE_ON");
            this.colCREATE_ON.FieldName = "CREATE_ON";
            this.colCREATE_ON.Name = "colCREATE_ON";
            // 
            // colIS_ACTIVE
            // 
            resources.ApplyResources(this.colIS_ACTIVE, "colIS_ACTIVE");
            this.colIS_ACTIVE.FieldName = "IS_ACTIVE";
            this.colIS_ACTIVE.Name = "colIS_ACTIVE";
            // 
            // colACCOUNT_KHR
            // 
            resources.ApplyResources(this.colACCOUNT_KHR, "colACCOUNT_KHR");
            this.colACCOUNT_KHR.ColumnEdit = this.resAccount;
            this.colACCOUNT_KHR.FieldName = "ACCOUNT_ID_KHR";
            this.colACCOUNT_KHR.Name = "colACCOUNT_KHR";
            // 
            // resAccount
            // 
            this.resAccount.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("resAccount.Appearance.Font")));
            this.resAccount.Appearance.Options.UseFont = true;
            this.resAccount.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("resAccount.AppearanceFocused.Font")));
            this.resAccount.AppearanceFocused.Options.UseFont = true;
            this.resAccount.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("resAccount.AppearanceReadOnly.Font")));
            this.resAccount.AppearanceReadOnly.Options.UseFont = true;
            resources.ApplyResources(this.resAccount, "resAccount");
            resources.ApplyResources(serializableAppearanceObject1, "serializableAppearanceObject1");
            serializableAppearanceObject1.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject3, "serializableAppearanceObject3");
            serializableAppearanceObject3.Options.UseFont = true;
            this.resAccount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("resAccount.Buttons"))), resources.GetString("resAccount.Buttons1"), ((int)(resources.GetObject("resAccount.Buttons2"))), ((bool)(resources.GetObject("resAccount.Buttons3"))), ((bool)(resources.GetObject("resAccount.Buttons4"))), ((bool)(resources.GetObject("resAccount.Buttons5"))), editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, resources.GetString("resAccount.Buttons6"), ((object)(resources.GetObject("resAccount.Buttons7"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("resAccount.Buttons8"))), ((DevExpress.Utils.ToolTipAnchor)(resources.GetObject("resAccount.Buttons9"))))});
            this.resAccount.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resAccount.Columns"), resources.GetString("resAccount.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("resAccount.Columns2"), resources.GetString("resAccount.Columns3"))});
            this.resAccount.DisplayMember = "AccountName";
            this.resAccount.Name = "resAccount";
            this.resAccount.ValueMember = "Id";
            // 
            // colACCOUNT_USD
            // 
            resources.ApplyResources(this.colACCOUNT_USD, "colACCOUNT_USD");
            this.colACCOUNT_USD.ColumnEdit = this.resAccount;
            this.colACCOUNT_USD.FieldName = "ACCOUNT_ID_USD";
            this.colACCOUNT_USD.Name = "colACCOUNT_USD";
            // 
            // colACCOUNT_THB
            // 
            resources.ApplyResources(this.colACCOUNT_THB, "colACCOUNT_THB");
            this.colACCOUNT_THB.ColumnEdit = this.resAccount;
            this.colACCOUNT_THB.FieldName = "ACCOUNT_ID_THB";
            this.colACCOUNT_THB.Name = "colACCOUNT_THB";
            // 
            // colACCOUNT_VND
            // 
            resources.ApplyResources(this.colACCOUNT_VND, "colACCOUNT_VND");
            this.colACCOUNT_VND.ColumnEdit = this.resAccount;
            this.colACCOUNT_VND.FieldName = "ACCOUNT_ID_VND";
            this.colACCOUNT_VND.Name = "colACCOUNT_VND";
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // PagePaymentMethod
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PagePaymentMethod";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPaymentMethod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resAccount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private ExButton btnREMOVE;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView dgvPaymentMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colPAYMENT_METHOD;
        private DevExpress.XtraGrid.Columns.GridColumn colCREATE_BY;
        private DevExpress.XtraGrid.Columns.GridColumn colPAYMENT_METHOD_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colCREATE_ON;
        private DevExpress.XtraGrid.Columns.GridColumn colIS_ACTIVE;
        private DevExpress.XtraGrid.Columns.GridColumn colACCOUNT_KHR;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit resAccount;
        private DevExpress.XtraGrid.Columns.GridColumn colACCOUNT_USD;
        private DevExpress.XtraGrid.Columns.GridColumn colACCOUNT_THB;
        private DevExpress.XtraGrid.Columns.GridColumn colACCOUNT_VND;
        private DevExpress.XtraEditors.SearchControl searchControl;
    }
}
