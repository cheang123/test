﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogPowerGeneration : ExDialog
    {
        GeneralProcess _flag;
        bool _firstLoad = true;
        string _strContext = string.Empty;

        TBL_POWER_GENERATION _objNew = new TBL_POWER_GENERATION();

        public TBL_POWER_GENERATION PowerGenerator
        {
            get { return _objNew; }
        }
        TBL_POWER_GENERATION _objOld = new TBL_POWER_GENERATION();

        #region Constructor
        public DialogPowerGeneration(GeneralProcess flag, TBL_POWER_GENERATION obj)
        {
            InitializeComponent();
            _strContext = this.Text;

            var today = DBDataContext.Db.GetSystemDate().Date;
            UIHelper.SetDataSourceToComboBox(cboGenerator, from g in DBDataContext.Db.TBL_GENERATORs
                                                           where (g.IS_INUSED || g.END_DATE.Date >= today || g.GENERATOR_ID == obj.GENERATOR_ID) && g.IS_ACTIVE
                                                           select new { g.GENERATOR_ID, g.GENERATOR_NO });
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            bindData(flag, obj);
        }

        private void bindData(GeneralProcess flag, TBL_POWER_GENERATION obj)
        {
            if (_flag != GeneralProcess.Delete)
            {
                _flag = flag;
            }
            this.Text = flag.GetText(_strContext);
            obj._CopyTo(_objNew);
            obj._CopyTo(_objOld);
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();

            //if duplicate record.
            txtPower.ClearAllValidation();

            if (checkExit())
            {
                cboGenerator.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblGENERATOR_NO.Text));
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            cboGenerator.SelectedValue = _objNew.GENERATOR_ID == 0 ? 1 : _objNew.GENERATION_ID;
            dtpMonthGeneration.Value = _objNew.GENERATION_MONTH;
            txtPower.Text = _objNew.POWER_GENERATION.ToString("#");
            txtAuxiliaryUse.Text = _objNew.AUXILIARY_USE.ToString("#");
            txtFule.Text = _objNew.FUEL_CONSUMPTION.ToString("#");
            txtLub.Text = _objNew.LUB_COMSUMPTION.ToString("#");
            txtOperationHour.Text = _objNew.OPERATION_HOUR.ToString("#");
            txtBasedRate.Text = _objNew.BASED_TARIFF.ToString();
            cboCurrency.SelectedValue = _objNew.CURRENCY_ID == 0 ? 1 : _objNew.CURRENCY_ID;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.GENERATOR_ID = (int)cboGenerator.SelectedValue;
            _objNew.GENERATION_MONTH = new DateTime(dtpMonthGeneration.Value.Year, dtpMonthGeneration.Value.Month, 1);
            _objNew.POWER_GENERATION = DataHelper.ParseToDecimal(txtPower.Text);
            _objNew.AUXILIARY_USE = DataHelper.ParseToDecimal(txtAuxiliaryUse.Text);
            _objNew.FUEL_CONSUMPTION = DataHelper.ParseToDecimal(txtFule.Text);
            _objNew.LUB_COMSUMPTION = DataHelper.ParseToDecimal(txtLub.Text);
            _objNew.OPERATION_HOUR = DataHelper.ParseToDecimal(txtOperationHour.Text);
            _objNew.BASED_TARIFF = DataHelper.ParseToDecimal(txtBasedRate.Text);
            _objNew.CURRENCY_ID = (int)cboCurrency.SelectedValue;
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (cboGenerator.SelectedIndex == -1)
            {
                cboGenerator.SetValidation(string.Format(Resources.REQUIRED, lblGENERATOR_NO.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(txtPower.Text))
            {
                txtPower.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtPower.Text) <= 0)
            {
                txtPower.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtAuxiliaryUse.Text) <= 0)
            {
                txtAuxiliaryUse.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtFule.Text) <= 0)
            {
                txtFule.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtLub.Text) <= 0)
            {
                txtLub.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtOperationHour.Text) <= 0)
            {
                txtOperationHour.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                val = true;
            }
            if (!DataHelper.IsNumber(txtAuxiliaryUse.Text))
            {
                txtAuxiliaryUse.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (!DataHelper.IsNumber(txtFule.Text))
            {
                txtFule.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (!DataHelper.IsNumber(txtLub.Text))
            {
                txtLub.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (txtOperationHour.Text.Trim() == string.Empty)
            {
                txtOperationHour.SetValidation(string.Format(Resources.REQUIRED, lblOPERATION_HOUR.Text));
                val = true;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblCURRENCY.Text));
                val = true;
            }
            return val;
        }

        private bool checkExit()
        {
            if (_flag == GeneralProcess.Delete) return false;
            bool exit = false;
            var obj = from pg in DBDataContext.Db.TBL_POWER_GENERATIONs
                      where pg.IS_ACTIVE
                           && (pg.GENERATOR_ID == (int)cboGenerator.SelectedValue)
                           && (pg.GENERATION_MONTH) == (dtpMonthGeneration.Value)
                           && pg.GENERATION_ID != _objOld.GENERATION_ID
                      select new { pg.POWER_GENERATION };
            if (obj.Count() > 0)
            {
                exit = true;
            }
            return exit;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void txtKeyEn(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion        

        private void dtpMonthGeneration_ValueChanged(object sender, EventArgs e)
        {
            //if (_firstLoad)
            //{
            //    _firstLoad = false;
            //    return;
            //}
            ////Reverse object
            ////_objNew = _objOld;
            //GeneralProcess flag = GeneralProcess.Update;
            //int generatorID = cboGenerator.SelectedIndex == -1 ? 0 : (int)cboGenerator.SelectedValue;
            //DateTime dtMonth=new DateTime(dtpMonthGeneration.Value.Year,dtpMonthGeneration.Value.Month,1);

            //TBL_POWER_GENERATION obj = DBDataContext.Db.TBL_POWER_GENERATIONs
            //                                .FirstOrDefault(x => x.GENERATION_MONTH.Date == dtMonth.Date 
            //                                    && x.GENERATOR_ID == generatorID
            //                                    && x.IS_ACTIVE);
            //if (obj==null)
            //{
            //    obj = new TBL_POWER_GENERATION()
            //        {
            //            GENERATOR_ID = generatorID,
            //            GENERATION_MONTH = dtMonth,
            //        };
            //    flag = GeneralProcess.Insert; 
            //}
            //bindData(flag, obj); 
        }

        private void txtPower_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        private void txtOperationHour_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        private void InputDecimalOnly(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }



    }
}