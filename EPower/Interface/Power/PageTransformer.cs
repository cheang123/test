﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System.Data;

namespace EPower.Interface
{
    public partial class PageTransformer : Form
    {
        bool isLoad = false;
        public  TBL_TRANSFORMER Transformer
        {
            get
            {
                TBL_TRANSFORMER obj = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intid = (int)dgv.SelectedRows[0].Cells["TRANSFORMER_ID"].Value;
                        obj = DBDataContext.Db.TBL_TRANSFORMERs.FirstOrDefault(x => x.TRANSFORMER_ID == intid);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return obj;
            }
        }

        #region Constructor
        public PageTransformer()
        {
            InitializeComponent();
            isLoad = false;
            //this.btnREPORT.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_QUARTERLY_REPORT]);
           
            UIHelper.DataGridViewProperties(dgv);
            bind();

            isLoad = true;
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Method

        private void bind()
        {
            UIHelper.SetDataSourceToComboBox(cboCapacity, DBDataContext.Db.TBL_CAPACITies.Where(x=>x.CAPACITY_ID>0).ToList().OrderBy(x=>Lookup.ParseCapacity(x.CAPACITY)) ,Resources.ALL_CAPACITY);
            UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPowerPhases(), Resources.ALL_PHASE);

            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(1, Resources.ALL_STATUS);
            dtReport.Rows.Add(2, Resources.OWN);
            dtReport.Rows.Add(3, Resources.NOT_OWN);
            UIHelper.SetDataSourceToComboBox(cboIsProperties, dtReport, "REPORT_ID", "REPORT_NAME");
        }

        #endregion

        
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogTransformer dig = new DialogTransformer(GeneralProcess.Insert, new TBL_TRANSFORMER()
            {
                TRANSFORMER_CODE = string.Empty,
                START_DATE = DBDataContext.Db.GetSystemDate(),
                END_DATE = UIHelper._DefaultDate,
                TRANSFORMER_POSITION_ID=(int)TransfoPositonType.OUTDOOR,
                IS_INUSED = true, 
            } );
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Transformer.TRANSFORMER_ID);
            }
        }
         
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (Transformer==null)
            {
                return;
            }
            DialogTransformer dig = new DialogTransformer(GeneralProcess.Update, Transformer);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Transformer.TRANSFORMER_ID);
            }
        }
         
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (Transformer==null)
            {
                return;
            }
            if (IsDeletable())
            {
                DialogTransformer dig = new DialogTransformer(GeneralProcess.Delete, Transformer);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Transformer.TRANSFORMER_ID - 1);
                }
            }
        }
         
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            if (!isLoad)
            {
                return;
            }
            bool isProperties = (int)cboIsProperties.SelectedValue == 2 ? true : false;
            dgv.DataSource = from t in DBDataContext.Db.TBL_TRANSFORMERs
                             join c in DBDataContext.Db.TBL_CAPACITies on t.CAPACITY_ID equals c.CAPACITY_ID
                             join p in DBDataContext.Db.TBL_PHASEs on t.PHASE_ID equals p.PHASE_ID
                             where t.IS_ACTIVE
                             && ((int)cboCapacity.SelectedValue==0 || t.CAPACITY_ID==(int)cboCapacity.SelectedValue)
                             &&((int)cboPhase.SelectedValue==0 || t.PHASE_ID==(int)cboPhase.SelectedValue)
                             &&((int)cboIsProperties.SelectedValue==1 || t.IS_PROPERTY==isProperties)
                             && string.Concat(t.TRANSFORMER_CODE,' ',c.CAPACITY,' ',p.PHASE_NAME).ToLower().Contains(txtQuickSearch.Text.ToLower())
                             orderby t.TRANSFORMER_CODE,t.START_DATE
                             select new
                             { 
                                 t.TRANSFORMER_ID,
                                 t.TRANSFORMER_CODE,
                                 c.CAPACITY,
                                 p.PHASE_NAME,
                                 t.START_DATE,
                                 END_DATE=t.IS_INUSED?string.Empty:string.Concat( t.END_DATE.Day,"-",t.END_DATE.Month,"-",t.END_DATE.Year),
                                 t.IS_INUSED,
                                 t.NOTE
                             };
                
        }

        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        
        private void btnReport_Click(object sender, EventArgs e)
        {
            var diag = new DialogReportQuarterOptions(false);
            diag.chkTABLE_TRANSFORMER.Checked = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                var doc = PageReportQuaterly.OpenReport(diag);
                doc.ViewReport("");
            }
        }

        private bool IsDeletable()
        {
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
                if (DBDataContext.Db.TBL_POLEs.Where(x => x.TRANSFORMER_ID == Transformer.TRANSFORMER_ID && x.IS_ACTIVE).Count() > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE, Resources.INFORMATION);
                    val = false;
                }
            }
            return val;
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
    }
}
