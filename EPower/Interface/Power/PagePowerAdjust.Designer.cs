﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.Power
{
    partial class PagePowerAdjust
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.COL_ADJUST_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POSTPAID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PREPAID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POWER_PRODUCTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POWER_DISCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COL_IS_ACTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.dtpDate);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(898, 34);
            this.panel1.TabIndex = 1;
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "ឆ្នាំ yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(3, 4);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.ShowCheckBox = true;
            this.dtpDate.Size = new System.Drawing.Size(121, 27);
            this.dtpDate.TabIndex = 12;
            this.dtpDate.ValueChanged += new System.EventHandler(this.dtpDate_ValueChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.btnREMOVE);
            this.flowLayoutPanel1.Controls.Add(this.btnEDIT);
            this.flowLayoutPanel1.Controls.Add(this.btnADD);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(557, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(341, 34);
            this.flowLayoutPanel1.TabIndex = 8;
            // 
            // btnREMOVE
            // 
            this.btnREMOVE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnREMOVE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnREMOVE.Location = new System.Drawing.Point(268, 5);
            this.btnREMOVE.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.Size = new System.Drawing.Size(71, 23);
            this.btnREMOVE.TabIndex = 3;
            this.btnREMOVE.Text = "លុប";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnEDIT
            // 
            this.btnEDIT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEDIT.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnEDIT.Location = new System.Drawing.Point(193, 5);
            this.btnEDIT.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.Size = new System.Drawing.Size(71, 23);
            this.btnEDIT.TabIndex = 1;
            this.btnEDIT.Text = "កែប្រែ";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnADD
            // 
            this.btnADD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnADD.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnADD.Location = new System.Drawing.Point(118, 5);
            this.btnADD.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.btnADD.Name = "btnADD";
            this.btnADD.Size = new System.Drawing.Size(71, 23);
            this.btnADD.TabIndex = 2;
            this.btnADD.Text = "បន្ថែមថ្មី";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.COL_ADJUST_ID,
            this.MONTH,
            this.POSTPAID,
            this.PREPAID,
            this.POWER_PRODUCTION,
            this.POWER_DISCOUNT,
            this.COL_IS_ACTIVE});
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Location = new System.Drawing.Point(0, 34);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(898, 285);
            this.dgv.TabIndex = 2;
            // 
            // COL_ADJUST_ID
            // 
            this.COL_ADJUST_ID.DataPropertyName = "ADJUST_ID";
            this.COL_ADJUST_ID.HeaderText = "POWER_PURCHASE_ID";
            this.COL_ADJUST_ID.Name = "COL_ADJUST_ID";
            this.COL_ADJUST_ID.ReadOnly = true;
            this.COL_ADJUST_ID.Visible = false;
            // 
            // MONTH
            // 
            this.MONTH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.MONTH.DataPropertyName = "ADJUST_MONTH";
            dataGridViewCellStyle2.Format = "MM-yyyy";
            this.MONTH.DefaultCellStyle = dataGridViewCellStyle2;
            this.MONTH.HeaderText = "ប្រចាំខែ";
            this.MONTH.Name = "MONTH";
            this.MONTH.ReadOnly = true;
            // 
            // POSTPAID
            // 
            this.POSTPAID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.POSTPAID.DataPropertyName = "POWER_POSTPAID";
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.POSTPAID.DefaultCellStyle = dataGridViewCellStyle3;
            this.POSTPAID.HeaderText = "Postpaid";
            this.POSTPAID.Name = "POSTPAID";
            this.POSTPAID.ReadOnly = true;
            // 
            // PREPAID
            // 
            this.PREPAID.DataPropertyName = "POWER_PREPAID";
            dataGridViewCellStyle4.Format = "N0";
            dataGridViewCellStyle4.NullValue = null;
            this.PREPAID.DefaultCellStyle = dataGridViewCellStyle4;
            this.PREPAID.HeaderText = "Prepaid";
            this.PREPAID.Name = "PREPAID";
            this.PREPAID.ReadOnly = true;
            this.PREPAID.Width = 200;
            // 
            // POWER_PRODUCTION
            // 
            this.POWER_PRODUCTION.DataPropertyName = "POWER_PRODUCTION";
            dataGridViewCellStyle5.Format = "N0";
            this.POWER_PRODUCTION.DefaultCellStyle = dataGridViewCellStyle5;
            this.POWER_PRODUCTION.FillWeight = 150F;
            this.POWER_PRODUCTION.HeaderText = "ប្រើក្នុងផលិតកម្ម";
            this.POWER_PRODUCTION.Name = "POWER_PRODUCTION";
            this.POWER_PRODUCTION.ReadOnly = true;
            this.POWER_PRODUCTION.Width = 150;
            // 
            // POWER_DISCOUNT
            // 
            this.POWER_DISCOUNT.DataPropertyName = "POWER_DISCOUNT";
            dataGridViewCellStyle6.Format = "N0";
            dataGridViewCellStyle6.NullValue = null;
            this.POWER_DISCOUNT.DefaultCellStyle = dataGridViewCellStyle6;
            this.POWER_DISCOUNT.HeaderText = "ថាមពលបញ្ចុះតម្លៃ";
            this.POWER_DISCOUNT.Name = "POWER_DISCOUNT";
            this.POWER_DISCOUNT.ReadOnly = true;
            this.POWER_DISCOUNT.Width = 200;
            // 
            // COL_IS_ACTIVE
            // 
            this.COL_IS_ACTIVE.DataPropertyName = "IS_ACTIVE";
            this.COL_IS_ACTIVE.HeaderText = "IS_ACTIVE";
            this.COL_IS_ACTIVE.Name = "COL_IS_ACTIVE";
            this.COL_IS_ACTIVE.ReadOnly = true;
            this.COL_IS_ACTIVE.Visible = false;
            // 
            // PagePowerAdjust
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 319);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PagePowerAdjust";
            this.Text = "PagePowerAdjust";
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private DateTimePicker dtpDate;
        private FlowLayoutPanel flowLayoutPanel1;
        private ExButton btnREMOVE;
        private ExButton btnEDIT;
        private ExButton btnADD;
        private DataGridView dgv;
        private DataGridViewTextBoxColumn COL_ADJUST_ID;
        private DataGridViewTextBoxColumn MONTH;
        private DataGridViewTextBoxColumn POSTPAID;
        private DataGridViewTextBoxColumn PREPAID;
        private DataGridViewTextBoxColumn POWER_PRODUCTION;
        private DataGridViewTextBoxColumn POWER_DISCOUNT;
        private DataGridViewTextBoxColumn COL_IS_ACTIVE;

    }
}