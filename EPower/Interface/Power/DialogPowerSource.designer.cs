﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPowerSource
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPowerSource));
            this.lblSTART_USE_DATE = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblTOTAL_CONNECTION = new System.Windows.Forms.Label();
            this.cboVoltage = new System.Windows.Forms.ComboBox();
            this.lblVOLTAGE = new System.Windows.Forms.Label();
            this.lblPRICE = new System.Windows.Forms.Label();
            this.txtNoConnection = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSell = new System.Windows.Forms.TextBox();
            this.lblSELLER = new System.Windows.Forms.Label();
            this.lblEND_USE_DATE = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.lblPOWER_SOURCE = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboPowerSourceLicensee = new System.Windows.Forms.ComboBox();
            this.cboCustomerConnectionType = new System.Windows.Forms.ComboBox();
            this.lblTYPE = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.dtpEndDate);
            this.content.Controls.Add(this.lblEND_USE_DATE);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.lblTYPE);
            this.content.Controls.Add(this.lblPOWER_SOURCE);
            this.content.Controls.Add(this.txtSell);
            this.content.Controls.Add(this.lblSELLER);
            this.content.Controls.Add(this.dtpStartDate);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.lblPRICE);
            this.content.Controls.Add(this.txtNoConnection);
            this.content.Controls.Add(this.cboCustomerConnectionType);
            this.content.Controls.Add(this.cboPowerSourceLicensee);
            this.content.Controls.Add(this.cboVoltage);
            this.content.Controls.Add(this.lblVOLTAGE);
            this.content.Controls.Add(this.lblTOTAL_CONNECTION);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtPrice);
            this.content.Controls.Add(this.lblSTART_USE_DATE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblSTART_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.txtPrice, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_CONNECTION, 0);
            this.content.Controls.SetChildIndex(this.lblVOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.cboVoltage, 0);
            this.content.Controls.SetChildIndex(this.cboPowerSourceLicensee, 0);
            this.content.Controls.SetChildIndex(this.cboCustomerConnectionType, 0);
            this.content.Controls.SetChildIndex(this.txtNoConnection, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.dtpStartDate, 0);
            this.content.Controls.SetChildIndex(this.lblSELLER, 0);
            this.content.Controls.SetChildIndex(this.txtSell, 0);
            this.content.Controls.SetChildIndex(this.lblPOWER_SOURCE, 0);
            this.content.Controls.SetChildIndex(this.lblTYPE, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpEndDate, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            // 
            // lblSTART_USE_DATE
            // 
            resources.ApplyResources(this.lblSTART_USE_DATE, "lblSTART_USE_DATE");
            this.lblSTART_USE_DATE.Name = "lblSTART_USE_DATE";
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            this.txtPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputDecimal);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblTOTAL_CONNECTION
            // 
            resources.ApplyResources(this.lblTOTAL_CONNECTION, "lblTOTAL_CONNECTION");
            this.lblTOTAL_CONNECTION.Name = "lblTOTAL_CONNECTION";
            // 
            // cboVoltage
            // 
            this.cboVoltage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVoltage.FormattingEnabled = true;
            resources.ApplyResources(this.cboVoltage, "cboVoltage");
            this.cboVoltage.Name = "cboVoltage";
            // 
            // lblVOLTAGE
            // 
            resources.ApplyResources(this.lblVOLTAGE, "lblVOLTAGE");
            this.lblVOLTAGE.Name = "lblVOLTAGE";
            // 
            // lblPRICE
            // 
            resources.ApplyResources(this.lblPRICE, "lblPRICE");
            this.lblPRICE.Name = "lblPRICE";
            // 
            // txtNoConnection
            // 
            resources.ApplyResources(this.txtNoConnection, "txtNoConnection");
            this.txtNoConnection.Name = "txtNoConnection";
            this.txtNoConnection.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            this.txtNoConnection.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputNumber);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // dtpStartDate
            // 
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // txtSell
            // 
            resources.ApplyResources(this.txtSell, "txtSell");
            this.txtSell.Name = "txtSell";
            this.txtSell.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblSELLER
            // 
            resources.ApplyResources(this.lblSELLER, "lblSELLER");
            this.lblSELLER.Name = "lblSELLER";
            // 
            // lblEND_USE_DATE
            // 
            resources.ApplyResources(this.lblEND_USE_DATE, "lblEND_USE_DATE");
            this.lblEND_USE_DATE.Name = "lblEND_USE_DATE";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Checked = false;
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.ShowCheckBox = true;
            this.dtpEndDate.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            this.dtpEndDate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtpEndDate_MouseDown);
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // lblPOWER_SOURCE
            // 
            resources.ApplyResources(this.lblPOWER_SOURCE, "lblPOWER_SOURCE");
            this.lblPOWER_SOURCE.Name = "lblPOWER_SOURCE";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // cboPowerSourceLicensee
            // 
            this.cboPowerSourceLicensee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPowerSourceLicensee.FormattingEnabled = true;
            resources.ApplyResources(this.cboPowerSourceLicensee, "cboPowerSourceLicensee");
            this.cboPowerSourceLicensee.Name = "cboPowerSourceLicensee";
            this.cboPowerSourceLicensee.SelectedIndexChanged += new System.EventHandler(this.cboPowerSourceLicensee_SelectedIndexChanged);
            // 
            // cboCustomerConnectionType
            // 
            this.cboCustomerConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerConnectionType.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerConnectionType, "cboCustomerConnectionType");
            this.cboCustomerConnectionType.Name = "cboCustomerConnectionType";
            this.cboCustomerConnectionType.SelectedIndexChanged += new System.EventHandler(this.cboPowerSourceLicensee_SelectedIndexChanged);
            // 
            // lblTYPE
            // 
            resources.ApplyResources(this.lblTYPE, "lblTYPE");
            this.lblTYPE.Name = "lblTYPE";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // DialogPowerSource
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPowerSource";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtPrice;
        private Label lblSTART_USE_DATE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private ComboBox cboVoltage;
        private Label lblVOLTAGE;
        private Label lblTOTAL_CONNECTION;
        private Label label13;
        private Label label12;
        private Label lblPRICE;
        private TextBox txtNoConnection;
        private DateTimePicker dtpStartDate;
        private Label label1;
        private TextBox txtSell;
        private Label lblSELLER;
        private Label lblEND_USE_DATE;
        private DateTimePicker dtpEndDate;
        private TextBox txtNote;
        private Label lblNOTE;
        private ComboBox cboCurrency;
        private Label lblCURRENCY;
        private Label label3;
        private Label lblPOWER_SOURCE;
        private ComboBox cboPowerSourceLicensee;
        private Label label4;
        private Label lblTYPE;
        private ComboBox cboCustomerConnectionType;
    }
}