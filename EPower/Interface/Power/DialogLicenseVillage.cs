﻿using EPower.Base.Helper.DevExpressCustomize;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogLicenseVillage : ExDialog
    {
        bool _isSuccess = false;
        public DialogLicenseVillage()
        {
            InitializeComponent();
            this.treeList.SetDefaultTreeList();
            Bind();
            ApplyDefaultSetting();
            Read();
            //Event
            this.btnOK.Click += btnOK_Click;
            this.btnCLOSE.Click += btnClose_Click;
        }

        #region Method
        private void Bind()
        {
            List<Villages> lst = new List<Villages>();

            var p = (from s in DBDataContext.Db.TLKP_PROVINCEs
                     select new Villages
                     {
                         Code = s.PROVINCE_CODE,
                         Name = s.PROVINCE_NAME,
                         ParentCode = ""
                     }).ToList();

            var d = (from s in DBDataContext.Db.TLKP_DISTRICTs
                     select new Villages
                     {
                         Code = s.DISTRICT_CODE,
                         Name = s.DISTRICT_NAME,
                         ParentCode = s.PROVINCE_CODE
                     }).ToList();
            var c = (from s in DBDataContext.Db.TLKP_COMMUNEs
                     select new Villages
                     {
                         Code = s.COMMUNE_CODE,
                         Name = s.COMMUNE_NAME,
                         ParentCode = s.DISTRICT_CODE
                     }).ToList();
            var v = (from s in DBDataContext.Db.TLKP_VILLAGEs
                     select new Villages
                     {
                         Code = s.VILLAGE_CODE,
                         Name = s.VILLAGE_NAME,
                         ParentCode = s.COMMUNE_CODE,
                         IsVillage = true
                     }).ToList();

            lst.AddRange(p);
            lst.AddRange(d);
            lst.AddRange(c);
            lst.AddRange(v);
            this.treeList.DataSource = lst;

        }
        private void Read()
        {
            var activeVillages = DBDataContext.Db.TBL_LICENSE_VILLAGEs.Where(x => x.IS_ACTIVE).ToList();
            foreach (var village in activeVillages)
            {
                var node = treeList.FindNodeByKeyID(village.VILLAGE_CODE);
                if (node == null)
                {
                    continue;
                }
                treeList.SetNodeCheckState(node, CheckState.Checked, true);
            }
        }
        private void Save()
        {
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted, Timeout = TimeSpan.MaxValue }))
                {
                    //Get checked data
                    List<Villages> lstCheckedVillages = new List<Villages>();
                    var checkedVillage = treeList.GetAllCheckedNodes();
                    foreach (var node in checkedVillage)
                    {
                        lstCheckedVillages.Add((Villages)treeList.GetDataRecordByNode(node));
                    }

                    //Update checked village
                    var newCheckedVilalges = DBDataContext.Db.TBL_LICENSE_VILLAGEs.Where(x => lstCheckedVillages.Select(y => y.Code).Contains(x.VILLAGE_CODE)).ToList();
                    newCheckedVilalges.ForEach(x => x.IS_ACTIVE = true);

                    //Update unchecked villages
                    var uncheckedVillage = DBDataContext.Db.TBL_LICENSE_VILLAGEs.Where(x => !lstCheckedVillages.Select(y => y.Code).Contains(x.VILLAGE_CODE)).ToList();
                    uncheckedVillage.ForEach(x => x.IS_ACTIVE = false);

                    //Insert 
                    var newVillages = lstCheckedVillages.Where(x => x.IsVillage && !DBDataContext.Db.TBL_LICENSE_VILLAGEs.Select(y => y.VILLAGE_CODE).Contains(x.Code)).ToList();
                    foreach (var item in newVillages)
                    {
                        TBL_LICENSE_VILLAGE obj = new TBL_LICENSE_VILLAGE()
                        {
                            VILLAGE_CODE = item.Code,
                            TOTAL_FAMILIES = 0,
                            IS_ACTIVE = true,
                            ROW_DATE = DateTime.Now
                        };
                        DBDataContext.Db.TBL_LICENSE_VILLAGEs.InsertOnSubmit(obj);
                    }
                    DBDataContext.Db.SubmitChanges();
                    tran.Complete();
                    _isSuccess = true;
                }
            }
            catch (Exception exp)
            {
                throw new Exception(Resources.MAXIMUM_DATA_2100);
                MsgBox.LogError(exp);
            }

        }
        private void ForceLoadAllNodes(DevExpress.XtraTreeList.TreeList treeList)
        {
            treeList.BeginUpdate();
            treeList.ExpandAll();
            treeList.CollapseAll();
            treeList.EndUpdate();
        }
        public void ApplyDefaultSetting()
        {
            ForceLoadAllNodes(treeList);
        }
        #endregion

        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            _isSuccess = false;
            Runner.RunNewThread(Save, Resources.SAVING_DATA);
            if (_isSuccess)
            {
                this.DialogResult = DialogResult.OK;
            }
        }


        #endregion
    }
    class Villages
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string ParentCode { get; set; }
        public bool IsVillage { get; set; }
    }
}