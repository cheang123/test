﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageFuelSource : Form
    {        
        
        public  TBL_FUEL_SOURCE FuelSource
        {
            get
            {
                TBL_FUEL_SOURCE objFuelSource = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int fuelSourceId = (int)dgv.SelectedRows[0].Cells[FUEL_SOURCE_ID.Name].Value;
                        objFuelSource = DBDataContext.Db.TBL_FUEL_SOURCEs.FirstOrDefault(x => x.FUEL_SOURCE_ID == fuelSourceId);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objFuelSource;
            }
        }

        #region Constructor
        public PageFuelSource()
        {
            InitializeComponent(); 
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new ampare.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogFuelSource dig = new DialogFuelSource(GeneralProcess.Insert, new TBL_FUEL_SOURCE());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.FuelSource.FUEL_SOURCE_ID);
            }
        }

        /// <summary>
        /// Edit ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogFuelSource dig = new DialogFuelSource(GeneralProcess.Update, FuelSource);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.FuelSource.FUEL_SOURCE_ID);
                }
            }
        }

        /// <summary>
        /// Remove ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (FuelSource==null)
            {
                return;
            }

            if (!IsDeletetable())
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                return;
            }

            DialogFuelSource dig = new DialogFuelSource(GeneralProcess.Delete, FuelSource);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.FuelSource.FUEL_SOURCE_ID - 1);
            }

        }

        private bool IsDeletetable()
        {
            return DBDataContext.Db.TBL_FUEL_SOURCEs.Where(x => x.FUEL_SOURCE_ID == FuelSource.FUEL_SOURCE_ID && x.IS_ACTIVE).Count() == 0;
        }

        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            dgv.DataSource = DBDataContext.Db.TBL_FUEL_SOURCEs.Where(x => x.IS_ACTIVE && (x.FUEL_SOURCE_NAME + " " + x.NOTE).ToUpper().Contains(txtQuickSearch.Text.ToUpper()));
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>.
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion 
    }
}
