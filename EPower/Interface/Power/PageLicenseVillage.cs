﻿using EPower.Base.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageLicenseVillage : Form
    {      
        public PageLicenseVillage()
        {
            InitializeComponent();
            this.btnREPORT.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_QUARTERLY_REPORT]);
            bind();
        }

        private void bind()
        {
            var list = from lv in DBDataContext.Db.TBL_LICENSE_VILLAGEs
                     join v in DBDataContext.Db.TLKP_VILLAGEs on lv.VILLAGE_CODE equals v.VILLAGE_CODE
                     join c in DBDataContext.Db.TLKP_COMMUNEs on v.COMMUNE_CODE equals c.COMMUNE_CODE
                     join d in DBDataContext.Db.TLKP_DISTRICTs on c.DISTRICT_CODE equals d.DISTRICT_CODE
                     join p in DBDataContext.Db.TLKP_PROVINCEs on d.PROVINCE_CODE equals p.PROVINCE_CODE
                     where lv.IS_ACTIVE &&
                     string.Concat(p.PROVINCE_NAME," ",d.DISTRICT_NAME," ",c.COMMUNE_NAME," ",v.VILLAGE_NAME).ToLower().Contains(txtQuickSearch.Text.ToLower())
                     orderby p.PROVINCE_NAME,d.DISTRICT_NAME,c.COMMUNE_NAME,v.VILLAGE_NAME
                     select new
                     {
                         lv.VILLAGE_CODE,
                         p.PROVINCE_NAME,
                         d.DISTRICT_NAME,
                         c.COMMUNE_NAME,
                         v.VILLAGE_NAME,
                         lv.TOTAL_FAMILIES
                     };
            dgv.DataSource = list._ToDataTable(); 
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DialogLicenseVillage dig = new DialogLicenseVillage();
            if (dig.ShowDialog()== DialogResult.OK)
            {
                bind();
            } 
        }

        private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }

        private void dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            DataGridViewRow dr = dgv.Rows[e.RowIndex];
            if (dr == null)
            {
                return;
            }

            try
            {
                string strVillageCode = dr.Cells[this.VILLAGE_CODE.Name].Value.ToString();
                int intTotalFamily = DataHelper.ParseToInt(dr.Cells[this.TOTAL_FAMILY.Name].Value.ToString()); 

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    TBL_LICENSE_VILLAGE objLV = DBDataContext.Db.TBL_LICENSE_VILLAGEs
                                                .FirstOrDefault(x => x.VILLAGE_CODE.Contains(strVillageCode));
                    TBL_LICENSE_VILLAGE objOld = new TBL_LICENSE_VILLAGE();
                    if (objLV == null)
                    {
                        return;
                    }
                    objLV._CopyTo(objOld);
                    objLV.TOTAL_FAMILIES = intTotalFamily; 
                    DBDataContext.Db.Update(objOld, objLV);
                    tran.Complete();
                } 
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            } 

        }

        private void txtQuickSearch_QuickSearch(object sender, EventArgs e)
        {
            bind();
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            var diag = new DialogReportQuarterOptions(false);
            diag.chkTABLE_POWER_COVERAGE.Checked = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                var doc = PageReportQuaterly.OpenReport(diag);
                doc.ViewReport("");
            }

        } 
    }
}
