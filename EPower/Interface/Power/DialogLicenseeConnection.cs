﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogLicenseeConnection: ExDialog
    {
        GeneralProcess _flag; 
        
        TBL_LICENSEE_CONNECTION _objNew = new TBL_LICENSEE_CONNECTION();
        TBL_CUSTOMER _objCustomer =null;

        public TBL_LICENSEE_CONNECTION LicenseeConnection
        {
            get { return _objNew; } 
        } 
        TBL_LICENSEE_CONNECTION _objOld = new TBL_LICENSEE_CONNECTION();

        #region Constructor
        public DialogLicenseeConnection(GeneralProcess flag, TBL_LICENSEE_CONNECTION obj)
        {
            InitializeComponent();
            dtpStartDate.CustomFormat = UIHelper._DefaultDateFormat;
            _flag = flag;
            obj._CopyTo(_objNew);
            obj._CopyTo(_objOld); 

            this.Text = _flag.GetText(this.Text);

            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete)); 
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert; 
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //if duplicate record.
            txtCustomer.ClearAllValidation();
            if (DBDataContext.Db.IsExits(_objNew, "CUSTOMER_ID") && _flag != GeneralProcess.Delete)
            {
                txtCustomer.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblCUSTOMER.Text));
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew); 
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        
        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        { 
            readCus(_objNew.CUSTOMER_ID);
            
            dtpStartDate.Value = _objNew.START_DATE;
            dtpEndDate.Value = _objNew.END_DATE;
            dtpEndDate.Checked = !_objNew.IS_INUSED;
            if (_objNew.IS_INUSED)
            {
                dtpEndDate.ClearValue();
            }
        }

        private void readCus(int CusID)
        {
            var cus = (from c in DBDataContext.Db.TBL_CUSTOMERs
                       join a in DBDataContext.Db.TBL_VOLTAGEs on c.VOL_ID equals a.VOLTAGE_ID
                       where c.CUSTOMER_ID == CusID
                       select new
                       {
                           TBL_CUSTOMER = c,
                           c.CUSTOMER_ID,
                           FULL_NAME = string.Concat(c.LAST_NAME_KH, " ", c.FIRST_NAME_KH),
                           a.VOLTAGE_NAME,
                           PRICE = DBDataContext.Db.TBL_PRICE_DETAILs.FirstOrDefault(x => x.PRICE_ID == c.PRICE_ID).PRICE
                       }).FirstOrDefault();
            if (cus != null)
            {
                txtCustomer.AcceptSearch(false);
                _objCustomer = cus.TBL_CUSTOMER;
                txtCustomer.Text = cus.FULL_NAME; 
            }
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {  
            _objNew.CUSTOMER_ID = _objCustomer.CUSTOMER_ID;
            _objNew.LOCATION = txtLocation.Text; 
            _objNew.START_DATE = dtpStartDate.Value;
            _objNew.IS_INUSED = !dtpEndDate.Checked;
            if (_objNew.IS_INUSED)
            {
                dtpEndDate.Value = UIHelper._DefaultDate;
            }
            _objNew.END_DATE = dtpEndDate.Value; 
              
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation(); 
            if (_objCustomer==null ||_objCustomer.CUSTOMER_ID==0)
            {
                txtCustomer.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER.Text));
                val = true;
            }
            return val;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        } 
        #endregion    

        private void txtCustomer_AdvanceSearch(object sender, EventArgs e)
        {
            DialogCustomerSearch diag = new DialogCustomerSearch(txtCustomer.Text, DialogCustomerSearch.PowerType.Postpaid);
            if (diag.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == diag.CustomerID);
            readCus(_objCustomer.CUSTOMER_ID);
        }

        private void txtCustomer_CancelAdvanceSearch(object sender, EventArgs e)
        {
            _objCustomer = new TBL_CUSTOMER();
        }
        private void dtpEndDate_MouseDown(object sender, MouseEventArgs e)
        {
            if (dtpEndDate.Checked)
            {
                if (dtpEndDate.Value.Date.Equals(UIHelper._DefaultDate))
                {
                    dtpEndDate.SetValue(DBDataContext.Db.GetSystemDate());
                }
                else
                {
                    dtpEndDate.CustomFormat = UIHelper._DefaultDateFormat;
                }
            }
            else
            {
                dtpEndDate.ClearValue();
            }
        }

        private void txtCustomer_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        } 
       
    }
}