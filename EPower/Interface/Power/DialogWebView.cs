﻿using EPower.Base.Logic;
using SoftTech.Component;
using System;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogWebView : ExDialog
    {
        string _text;

        #region Constructor
        public DialogWebView(string _url)
        {
            InitializeComponent();
            // resize screen
            this.WindowState = FormWindowState.Normal;
            this.StartPosition = FormStartPosition.Manual;
            this.Top = 0;
            this.Left = 0;
            this.Height = Screen.GetWorkingArea(this).Height;
            this.Width = Screen.GetWorkingArea(this).Width;
            Method.ChangeIEVersion();
            webBrowser1.Navigate(_url);
        }
        #endregion

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webBrowser1.Document.Body.Style = "font-family:hanuman;";
        }

        private void picCloseWeb_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void picGoNext_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void picRefresh_Click(object sender, EventArgs e)
        {
            webBrowser1.Refresh();
        }

        private void picGoBack_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void DialogWebView_Load(object sender, EventArgs e)
        {

        }
    }
}