﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogTransformer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogTransformer));
            this.lblTRANSFORMER_CODE = new System.Windows.Forms.Label();
            this.txtNo = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.label4 = new System.Windows.Forms.Label();
            this.cboCapacity = new System.Windows.Forms.ComboBox();
            this.lblCAPACITY_AND_PHASE = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboPhase = new System.Windows.Forms.ComboBox();
            this.lblTRANSFORMER_BRAND = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lblSTART_USE_DATE = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblEND_USE_DATE = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.lblTRANSFORMER_TYPE = new System.Windows.Forms.Label();
            this.cboTransfoType = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblMV = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblLV = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cboHighVoltage = new System.Windows.Forms.ComboBox();
            this.cboLowVoltage = new System.Windows.Forms.ComboBox();
            this.lblTRANSFORMER_POSITION = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.cboPositionType = new System.Windows.Forms.ComboBox();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lblOWN = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cboTransformerBrand = new System.Windows.Forms.ComboBox();
            this.lblPOLE_CODE = new System.Windows.Forms.Label();
            this.cboPOLE_CODE = new SoftTech.Component.ExComboBoxSearch();
            this.btnAddTransformerType = new SoftTech.Component.ExAddItem();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnAddTransformerType);
            this.content.Controls.Add(this.cboPOLE_CODE);
            this.content.Controls.Add(this.lblOWN);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.lblEND_USE_DATE);
            this.content.Controls.Add(this.dtpEndDate);
            this.content.Controls.Add(this.label11);
            this.content.Controls.Add(this.lblSTART_USE_DATE);
            this.content.Controls.Add(this.dtpStartDate);
            this.content.Controls.Add(this.label10);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.cboLowVoltage);
            this.content.Controls.Add(this.cboTransformerBrand);
            this.content.Controls.Add(this.cboTransfoType);
            this.content.Controls.Add(this.cboPositionType);
            this.content.Controls.Add(this.cboHighVoltage);
            this.content.Controls.Add(this.cboPhase);
            this.content.Controls.Add(this.lblTRANSFORMER_TYPE);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.lblTRANSFORMER_BRAND);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.cboCapacity);
            this.content.Controls.Add(this.lblCAPACITY_AND_PHASE);
            this.content.Controls.Add(this.label25);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label22);
            this.content.Controls.Add(this.label19);
            this.content.Controls.Add(this.label16);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblTRANSFORMER_POSITION);
            this.content.Controls.Add(this.lblPOLE_CODE);
            this.content.Controls.Add(this.lblLV);
            this.content.Controls.Add(this.lblMV);
            this.content.Controls.Add(this.txtNo);
            this.content.Controls.Add(this.lblTRANSFORMER_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblTRANSFORMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtNo, 0);
            this.content.Controls.SetChildIndex(this.lblMV, 0);
            this.content.Controls.SetChildIndex(this.lblLV, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblTRANSFORMER_POSITION, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label16, 0);
            this.content.Controls.SetChildIndex(this.label19, 0);
            this.content.Controls.SetChildIndex(this.label22, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.label25, 0);
            this.content.Controls.SetChildIndex(this.lblCAPACITY_AND_PHASE, 0);
            this.content.Controls.SetChildIndex(this.cboCapacity, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.lblTRANSFORMER_BRAND, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.lblTRANSFORMER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.cboPhase, 0);
            this.content.Controls.SetChildIndex(this.cboHighVoltage, 0);
            this.content.Controls.SetChildIndex(this.cboPositionType, 0);
            this.content.Controls.SetChildIndex(this.cboTransfoType, 0);
            this.content.Controls.SetChildIndex(this.cboTransformerBrand, 0);
            this.content.Controls.SetChildIndex(this.cboLowVoltage, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.label10, 0);
            this.content.Controls.SetChildIndex(this.dtpStartDate, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.label11, 0);
            this.content.Controls.SetChildIndex(this.dtpEndDate, 0);
            this.content.Controls.SetChildIndex(this.lblEND_USE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.lblOWN, 0);
            this.content.Controls.SetChildIndex(this.cboPOLE_CODE, 0);
            this.content.Controls.SetChildIndex(this.btnAddTransformerType, 0);
            // 
            // lblTRANSFORMER_CODE
            // 
            resources.ApplyResources(this.lblTRANSFORMER_CODE, "lblTRANSFORMER_CODE");
            this.lblTRANSFORMER_CODE.Name = "lblTRANSFORMER_CODE";
            // 
            // txtNo
            // 
            resources.ApplyResources(this.txtNo, "txtNo");
            this.txtNo.Name = "txtNo";
            this.txtNo.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // cboCapacity
            // 
            this.cboCapacity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCapacity.FormattingEnabled = true;
            resources.ApplyResources(this.cboCapacity, "cboCapacity");
            this.cboCapacity.Name = "cboCapacity";
            // 
            // lblCAPACITY_AND_PHASE
            // 
            resources.ApplyResources(this.lblCAPACITY_AND_PHASE, "lblCAPACITY_AND_PHASE");
            this.lblCAPACITY_AND_PHASE.Name = "lblCAPACITY_AND_PHASE";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // cboPhase
            // 
            this.cboPhase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPhase.FormattingEnabled = true;
            resources.ApplyResources(this.cboPhase, "cboPhase");
            this.cboPhase.Name = "cboPhase";
            // 
            // lblTRANSFORMER_BRAND
            // 
            resources.ApplyResources(this.lblTRANSFORMER_BRAND, "lblTRANSFORMER_BRAND");
            this.lblTRANSFORMER_BRAND.Name = "lblTRANSFORMER_BRAND";
            // 
            // dtpStartDate
            // 
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblSTART_USE_DATE
            // 
            resources.ApplyResources(this.lblSTART_USE_DATE, "lblSTART_USE_DATE");
            this.lblSTART_USE_DATE.Name = "lblSTART_USE_DATE";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Name = "label11";
            // 
            // lblEND_USE_DATE
            // 
            resources.ApplyResources(this.lblEND_USE_DATE, "lblEND_USE_DATE");
            this.lblEND_USE_DATE.Name = "lblEND_USE_DATE";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Checked = false;
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.ShowCheckBox = true;
            this.dtpEndDate.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            this.dtpEndDate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dtpEndDate_MouseDown);
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // lblTRANSFORMER_TYPE
            // 
            resources.ApplyResources(this.lblTRANSFORMER_TYPE, "lblTRANSFORMER_TYPE");
            this.lblTRANSFORMER_TYPE.Name = "lblTRANSFORMER_TYPE";
            // 
            // cboTransfoType
            // 
            this.cboTransfoType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransfoType.FormattingEnabled = true;
            resources.ApplyResources(this.cboTransfoType, "cboTransfoType");
            this.cboTransfoType.Name = "cboTransfoType";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Name = "label10";
            // 
            // lblMV
            // 
            resources.ApplyResources(this.lblMV, "lblMV");
            this.lblMV.Name = "lblMV";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Name = "label16";
            // 
            // lblLV
            // 
            resources.ApplyResources(this.lblLV, "lblLV");
            this.lblLV.Name = "lblLV";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Name = "label19";
            // 
            // cboHighVoltage
            // 
            this.cboHighVoltage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHighVoltage.FormattingEnabled = true;
            resources.ApplyResources(this.cboHighVoltage, "cboHighVoltage");
            this.cboHighVoltage.Name = "cboHighVoltage";
            // 
            // cboLowVoltage
            // 
            this.cboLowVoltage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLowVoltage.FormattingEnabled = true;
            resources.ApplyResources(this.cboLowVoltage, "cboLowVoltage");
            this.cboLowVoltage.Name = "cboLowVoltage";
            // 
            // lblTRANSFORMER_POSITION
            // 
            resources.ApplyResources(this.lblTRANSFORMER_POSITION, "lblTRANSFORMER_POSITION");
            this.lblTRANSFORMER_POSITION.Name = "lblTRANSFORMER_POSITION";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Name = "label22";
            // 
            // cboPositionType
            // 
            this.cboPositionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPositionType.FormattingEnabled = true;
            resources.ApplyResources(this.cboPositionType, "cboPositionType");
            this.cboPositionType.Name = "cboPositionType";
            this.cboPositionType.SelectedIndexChanged += new System.EventHandler(this.cboPositionType_SelectedIndexChanged);
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Name = "label25";
            // 
            // lblOWN
            // 
            resources.ApplyResources(this.lblOWN, "lblOWN");
            this.lblOWN.Checked = true;
            this.lblOWN.CheckState = System.Windows.Forms.CheckState.Checked;
            this.lblOWN.Name = "lblOWN";
            this.lblOWN.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // cboTransformerBrand
            // 
            this.cboTransformerBrand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransformerBrand.FormattingEnabled = true;
            resources.ApplyResources(this.cboTransformerBrand, "cboTransformerBrand");
            this.cboTransformerBrand.Name = "cboTransformerBrand";
            // 
            // lblPOLE_CODE
            // 
            resources.ApplyResources(this.lblPOLE_CODE, "lblPOLE_CODE");
            this.lblPOLE_CODE.Name = "lblPOLE_CODE";
            // 
            // cboPOLE_CODE
            // 
            this.cboPOLE_CODE.BackColor = System.Drawing.Color.White;
            this.cboPOLE_CODE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cboPOLE_CODE.DropDownPanel = null;
            resources.ApplyResources(this.cboPOLE_CODE, "cboPOLE_CODE");
            this.cboPOLE_CODE.Name = "cboPOLE_CODE";
            this.cboPOLE_CODE.SearchMode = SoftTech.Component.ExComboBoxSearch.SearchModes.OnKeyUp;
            this.cboPOLE_CODE.Value = null;
            this.cboPOLE_CODE.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // btnAddTransformerType
            // 
            this.btnAddTransformerType.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddTransformerType, "btnAddTransformerType");
            this.btnAddTransformerType.Name = "btnAddTransformerType";
            this.btnAddTransformerType.AddItem += new System.EventHandler(this.btnAddTransformerType_AddItem);
            // 
            // DialogTransformer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogTransformer";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtNo;
        private Label lblTRANSFORMER_CODE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private Label label1;
        private ComboBox cboPhase;
        private Label lblTRANSFORMER_BRAND;
        private Label label4;
        private ComboBox cboCapacity;
        private Label lblCAPACITY_AND_PHASE;
        private Label lblSTART_USE_DATE;
        private DateTimePicker dtpStartDate;
        private Label label11;
        private Label lblEND_USE_DATE;
        private DateTimePicker dtpEndDate;
        private TextBox txtNote;
        private Label lblNOTE;
        private Label label10;
        private ComboBox cboLowVoltage;
        private ComboBox cboTransfoType;
        private ComboBox cboPositionType;
        private ComboBox cboHighVoltage;
        private Label lblTRANSFORMER_TYPE;
        private Label label25;
        private Label label22;
        private Label label19;
        private Label label16;
        private Label lblSTATUS;
        private Label lblTRANSFORMER_POSITION;
        private Label lblLV;
        private Label lblMV;
        private CheckBox lblOWN;
        private ComboBox cboTransformerBrand;
        private Label label6;
        private Label lblPOLE_CODE;
        private ExComboBoxSearch cboPOLE_CODE;
        private ExAddItem btnAddTransformerType;
    }
}