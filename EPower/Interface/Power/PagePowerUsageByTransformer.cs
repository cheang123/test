﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PagePowerUsageByTransformer : Form
    {
        #region Data
        public TBL_POWER Power
        {
            get
            {
                TBL_POWER objPower = null;
                if (dgv.SelectedRows.Count > 0)
                { 
                    int id = (int)dgv.SelectedRows[0].Cells[POWER_ID.Name].Value;
                    objPower = DBDataContext.Db.TBL_POWERs.FirstOrDefault(x => x.POWER_ID == id); 
                }
                return objPower;
            }
        }

        #endregion 

        #region Constructor
        public PagePowerUsageByTransformer()
        {
            InitializeComponent(); 

            UIHelper.DataGridViewProperties(dgv);
            DateTime dt = new DateTime();
            dt = DBDataContext.Db.GetSystemDate();
            dtpMonth.Value = new DateTime(dt.Year, dt.Month, 1);
            loadData();
        }
        #endregion

        #region Operation
        private void PagePowerUsageByTransformer_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                UIHelper.SetDataSourceToComboBox(this.cboTransformer, DBDataContext.Db.TBL_TRANSFORMERs.Where(row => row.IS_ACTIVE), Resources.ALL_TRANSFORMER);
                cboTransformer.SelectedValue = cboTransformer.Tag;
            }
        }
        private void cboSaveState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbo = (ComboBox)sender;
            if (cbo.SelectedIndex == -1)
            {
                return;
            }
            cbo.Tag = cbo.SelectedValue;
        }

        /// <summary>
        /// Add new seal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogPowerUsageByTransformer dig = new DialogPowerUsageByTransformer(GeneralProcess.Insert, new TBL_POWER() {POWER_MONTH=DateTime.Now,MULTIPLIER=1});
            if (dig.ShowDialog() == DialogResult.OK)
            {
                loadData();
                UIHelper.SelectRow(dgv, "POWER_ID", dig.Power.POWER_ID);
            }
        }

        /// <summary>
        /// Edit seal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogPowerUsageByTransformer dig = new DialogPowerUsageByTransformer(GeneralProcess.Update, Power);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, "POWER_ID", dig.Power.POWER_ID);
                }
            }
        }

        /// <summary>
        /// Remove seal.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogPowerUsageByTransformer dig = new DialogPowerUsageByTransformer(GeneralProcess.Delete, Power);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    loadData();
                    UIHelper.SelectRow(dgv, "POWER_ID", dig.Power.POWER_ID - 1);
                }
            }
        }

        private void dtpMonth_ValueChanged(object sender, EventArgs e)
        {
            loadData();
        }

        private void cboTransformer_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboSaveState_SelectedIndexChanged(sender, e);
            loadData();
        }
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }

        
        
        #endregion

        #region Method

        private bool IsDeletable()
        {
            //TODO: can delte.
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
            }
            return val;
        }

        /// <summary>
        /// Load data from database and display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadData()
        {
            try
            {
                int transfoId = 0; 
                if (cboTransformer.SelectedIndex!=-1)
                {
                    transfoId = (int)this.cboTransformer.SelectedValue; 
                }

                

                dgv.DataSource = from p in DBDataContext.Db.TBL_POWERs
                                 join t in DBDataContext.Db.TBL_TRANSFORMERs on p.TRANSFORMER_ID equals t.TRANSFORMER_ID
                                 where p.IS_ACTIVE
                                 && ((p.POWER_MONTH.Date.Month == dtpMonth.Value.Date.Month && p.POWER_MONTH.Year == dtpMonth.Value.Year) || dtpMonth.Checked == false)
                                 && p.TRANSFORMER_ID != 0 // total power
                                 && (transfoId == 0 || p.TRANSFORMER_ID == transfoId)
                                 select new
                                 {
                                     p.POWER_ID,
                                     p.POWER_MONTH,
                                     t.TRANSFORMER_CODE,
                                     p.METER_CODE,
                                     p.START_USAGE,
                                     p.END_USAGE,
                                     p.MULTIPLIER,
                                     p.ADJUST_USAGE,
                                     p.POWER_INPUT
                                 };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        #endregion

       
        
    }
}
