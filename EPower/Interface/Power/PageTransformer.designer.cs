﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageTransformer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageTransformer));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.TRANSFORMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TRANSFORMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAPACITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PHASE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_USE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_USE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_INUSED = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.NOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboIsProperties = new System.Windows.Forms.ComboBox();
            this.cboPhase = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnREPORT = new SoftTech.Component.ExButton();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.cboCapacity = new System.Windows.Forms.ComboBox();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TRANSFORMER_ID,
            this.TRANSFORMER_CODE,
            this.CAPACITY,
            this.PHASE,
            this.START_USE_DATE,
            this.END_USE_DATE,
            this.IS_INUSED,
            this.NOTE});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // TRANSFORMER_ID
            // 
            this.TRANSFORMER_ID.DataPropertyName = "TRANSFORMER_ID";
            resources.ApplyResources(this.TRANSFORMER_ID, "TRANSFORMER_ID");
            this.TRANSFORMER_ID.Name = "TRANSFORMER_ID";
            // 
            // TRANSFORMER_CODE
            // 
            this.TRANSFORMER_CODE.DataPropertyName = "TRANSFORMER_CODE";
            resources.ApplyResources(this.TRANSFORMER_CODE, "TRANSFORMER_CODE");
            this.TRANSFORMER_CODE.Name = "TRANSFORMER_CODE";
            // 
            // CAPACITY
            // 
            this.CAPACITY.DataPropertyName = "CAPACITY";
            resources.ApplyResources(this.CAPACITY, "CAPACITY");
            this.CAPACITY.Name = "CAPACITY";
            // 
            // PHASE
            // 
            this.PHASE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.PHASE.DataPropertyName = "PHASE_NAME";
            dataGridViewCellStyle14.Format = "N0";
            this.PHASE.DefaultCellStyle = dataGridViewCellStyle14;
            this.PHASE.FillWeight = 150F;
            resources.ApplyResources(this.PHASE, "PHASE");
            this.PHASE.Name = "PHASE";
            // 
            // START_USE_DATE
            // 
            this.START_USE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.START_USE_DATE.DataPropertyName = "START_DATE";
            dataGridViewCellStyle15.Format = "dd-MM-yyyy";
            this.START_USE_DATE.DefaultCellStyle = dataGridViewCellStyle15;
            this.START_USE_DATE.FillWeight = 150F;
            resources.ApplyResources(this.START_USE_DATE, "START_USE_DATE");
            this.START_USE_DATE.Name = "START_USE_DATE";
            // 
            // END_USE_DATE
            // 
            this.END_USE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.END_USE_DATE.DataPropertyName = "END_DATE";
            dataGridViewCellStyle16.Format = "dd - MM - yyyy";
            dataGridViewCellStyle16.NullValue = "-";
            this.END_USE_DATE.DefaultCellStyle = dataGridViewCellStyle16;
            resources.ApplyResources(this.END_USE_DATE, "END_USE_DATE");
            this.END_USE_DATE.Name = "END_USE_DATE";
            // 
            // IS_INUSED
            // 
            this.IS_INUSED.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.IS_INUSED.DataPropertyName = "IS_INUSED";
            resources.ApplyResources(this.IS_INUSED, "IS_INUSED");
            this.IS_INUSED.Name = "IS_INUSED";
            // 
            // NOTE
            // 
            this.NOTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NOTE.DataPropertyName = "NOTE";
            resources.ApplyResources(this.NOTE, "NOTE");
            this.NOTE.Name = "NOTE";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboIsProperties);
            this.panel1.Controls.Add(this.cboPhase);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Controls.Add(this.cboCapacity);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboIsProperties
            // 
            this.cboIsProperties.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIsProperties.FormattingEnabled = true;
            resources.ApplyResources(this.cboIsProperties, "cboIsProperties");
            this.cboIsProperties.Name = "cboIsProperties";
            this.cboIsProperties.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // cboPhase
            // 
            this.cboPhase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPhase.FormattingEnabled = true;
            resources.ApplyResources(this.cboPhase, "cboPhase");
            this.cboPhase.Name = "cboPhase";
            this.cboPhase.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btnREPORT);
            this.flowLayoutPanel1.Controls.Add(this.btnREMOVE);
            this.flowLayoutPanel1.Controls.Add(this.btnEDIT);
            this.flowLayoutPanel1.Controls.Add(this.btnADD);
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // btnREPORT
            // 
            resources.ApplyResources(this.btnREPORT, "btnREPORT");
            this.btnREPORT.Name = "btnREPORT";
            this.btnREPORT.UseVisualStyleBackColor = true;
            this.btnREPORT.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // cboCapacity
            // 
            this.cboCapacity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCapacity.FormattingEnabled = true;
            resources.ApplyResources(this.cboCapacity, "cboCapacity");
            this.cboCapacity.Name = "cboCapacity";
            this.cboCapacity.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // PageTransformer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageTransformer";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        private ExButton btnREMOVE;
        private ExButton btnREPORT;
        private FlowLayoutPanel flowLayoutPanel1;
        private ComboBox cboPhase;
        private ComboBox cboCapacity;
        private ComboBox cboIsProperties;
        private DataGridViewTextBoxColumn TRANSFORMER_ID;
        private DataGridViewTextBoxColumn TRANSFORMER_CODE;
        private DataGridViewTextBoxColumn CAPACITY;
        private DataGridViewTextBoxColumn PHASE;
        private DataGridViewTextBoxColumn START_USE_DATE;
        private DataGridViewTextBoxColumn END_USE_DATE;
        private DataGridViewCheckBoxColumn IS_INUSED;
        private DataGridViewTextBoxColumn NOTE;
    }
}
