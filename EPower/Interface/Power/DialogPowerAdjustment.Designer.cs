﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.Power
{
    partial class DialogPowerAdjustment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpMonth = new System.Windows.Forms.DateTimePicker();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblPOWER_PRODUCTION = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPowerProduction = new System.Windows.Forms.TextBox();
            this.lblPOWER_DISCOUNT = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPowerDiscount = new System.Windows.Forms.TextBox();
            this.lblPOSTPAID = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPowerPostpaid = new System.Windows.Forms.TextBox();
            this.lblPREPAID = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPowerPrepaid = new System.Windows.Forms.TextBox();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtPowerPrepaid);
            this.content.Controls.Add(this.txtPowerPostpaid);
            this.content.Controls.Add(this.txtPowerDiscount);
            this.content.Controls.Add(this.txtPowerProduction);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.lblPREPAID);
            this.content.Controls.Add(this.lblPOSTPAID);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.lblPOWER_DISCOUNT);
            this.content.Controls.Add(this.dtpMonth);
            this.content.Controls.Add(this.lblPOWER_PRODUCTION);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.label15);
            this.content.Size = new System.Drawing.Size(373, 240);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.label15, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblPOWER_PRODUCTION, 0);
            this.content.Controls.SetChildIndex(this.dtpMonth, 0);
            this.content.Controls.SetChildIndex(this.lblPOWER_DISCOUNT, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.lblPOSTPAID, 0);
            this.content.Controls.SetChildIndex(this.lblPREPAID, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.txtPowerProduction, 0);
            this.content.Controls.SetChildIndex(this.txtPowerDiscount, 0);
            this.content.Controls.SetChildIndex(this.txtPowerPostpaid, 0);
            this.content.Controls.SetChildIndex(this.txtPowerPrepaid, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            // 
            // dtpMonth
            // 
            this.dtpMonth.CustomFormat = "MM-yyyy";
            this.dtpMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMonth.Location = new System.Drawing.Point(156, 17);
            this.dtpMonth.Margin = new System.Windows.Forms.Padding(2);
            this.dtpMonth.Name = "dtpMonth";
            this.dtpMonth.Size = new System.Drawing.Size(197, 27);
            this.dtpMonth.TabIndex = 7;
            // 
            // lblMONTH
            // 
            this.lblMONTH.AutoSize = true;
            this.lblMONTH.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblMONTH.Location = new System.Drawing.Point(14, 21);
            this.lblMONTH.Margin = new System.Windows.Forms.Padding(2);
            this.lblMONTH.Name = "lblMONTH";
            this.lblMONTH.Size = new System.Drawing.Size(45, 19);
            this.lblMONTH.TabIndex = 8;
            this.lblMONTH.Text = "ប្រចាំខែ";
            // 
            // lblPOWER_PRODUCTION
            // 
            this.lblPOWER_PRODUCTION.AutoSize = true;
            this.lblPOWER_PRODUCTION.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblPOWER_PRODUCTION.Location = new System.Drawing.Point(14, 114);
            this.lblPOWER_PRODUCTION.Margin = new System.Windows.Forms.Padding(2);
            this.lblPOWER_PRODUCTION.Name = "lblPOWER_PRODUCTION";
            this.lblPOWER_PRODUCTION.Size = new System.Drawing.Size(85, 19);
            this.lblPOWER_PRODUCTION.TabIndex = 8;
            this.lblPOWER_PRODUCTION.Text = "ប្រើក្នុងផលិតកម្ម";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(139, 113);
            this.label5.Margin = new System.Windows.Forms.Padding(2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "*";
            // 
            // txtPowerProduction
            // 
            this.txtPowerProduction.Location = new System.Drawing.Point(156, 110);
            this.txtPowerProduction.Margin = new System.Windows.Forms.Padding(2);
            this.txtPowerProduction.MaxLength = 50;
            this.txtPowerProduction.Name = "txtPowerProduction";
            this.txtPowerProduction.Size = new System.Drawing.Size(197, 27);
            this.txtPowerProduction.TabIndex = 72;
            this.txtPowerProduction.Text = "0";
            this.txtPowerProduction.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPowerProduction.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPowerTotal_KeyPress);
            // 
            // lblPOWER_DISCOUNT
            // 
            this.lblPOWER_DISCOUNT.AutoSize = true;
            this.lblPOWER_DISCOUNT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblPOWER_DISCOUNT.Location = new System.Drawing.Point(14, 145);
            this.lblPOWER_DISCOUNT.Margin = new System.Windows.Forms.Padding(2);
            this.lblPOWER_DISCOUNT.Name = "lblPOWER_DISCOUNT";
            this.lblPOWER_DISCOUNT.Size = new System.Drawing.Size(95, 19);
            this.lblPOWER_DISCOUNT.TabIndex = 8;
            this.lblPOWER_DISCOUNT.Text = "ថាមពលបញ្ចុះតម្លៃ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(139, 144);
            this.label9.Margin = new System.Windows.Forms.Padding(2);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "*";
            // 
            // txtPowerDiscount
            // 
            this.txtPowerDiscount.Location = new System.Drawing.Point(156, 141);
            this.txtPowerDiscount.Margin = new System.Windows.Forms.Padding(2);
            this.txtPowerDiscount.MaxLength = 50;
            this.txtPowerDiscount.Name = "txtPowerDiscount";
            this.txtPowerDiscount.Size = new System.Drawing.Size(197, 27);
            this.txtPowerDiscount.TabIndex = 72;
            this.txtPowerDiscount.Text = "0";
            this.txtPowerDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPowerDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPowerTotal_KeyPress);
            // 
            // lblPOSTPAID
            // 
            this.lblPOSTPAID.AutoSize = true;
            this.lblPOSTPAID.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblPOSTPAID.Location = new System.Drawing.Point(14, 52);
            this.lblPOSTPAID.Margin = new System.Windows.Forms.Padding(2);
            this.lblPOSTPAID.Name = "lblPOSTPAID";
            this.lblPOSTPAID.Size = new System.Drawing.Size(86, 19);
            this.lblPOSTPAID.TabIndex = 8;
            this.lblPOSTPAID.Text = "លក់ POSTPAID";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label12.Location = new System.Drawing.Point(139, 51);
            this.label12.Margin = new System.Windows.Forms.Padding(2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 20);
            this.label12.TabIndex = 8;
            this.label12.Text = "*";
            // 
            // txtPowerPostpaid
            // 
            this.txtPowerPostpaid.Location = new System.Drawing.Point(156, 48);
            this.txtPowerPostpaid.Margin = new System.Windows.Forms.Padding(2);
            this.txtPowerPostpaid.MaxLength = 50;
            this.txtPowerPostpaid.Name = "txtPowerPostpaid";
            this.txtPowerPostpaid.Size = new System.Drawing.Size(197, 27);
            this.txtPowerPostpaid.TabIndex = 72;
            this.txtPowerPostpaid.Text = "0";
            this.txtPowerPostpaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPowerPostpaid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPowerTotal_KeyPress);
            // 
            // lblPREPAID
            // 
            this.lblPREPAID.AutoSize = true;
            this.lblPREPAID.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblPREPAID.Location = new System.Drawing.Point(14, 83);
            this.lblPREPAID.Margin = new System.Windows.Forms.Padding(2);
            this.lblPREPAID.Name = "lblPREPAID";
            this.lblPREPAID.Size = new System.Drawing.Size(78, 19);
            this.lblPREPAID.TabIndex = 8;
            this.lblPREPAID.Text = "លក់ PREPAID";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label15.Location = new System.Drawing.Point(139, 82);
            this.label15.Margin = new System.Windows.Forms.Padding(2);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(15, 20);
            this.label15.TabIndex = 8;
            this.label15.Text = "*";
            // 
            // txtPowerPrepaid
            // 
            this.txtPowerPrepaid.Location = new System.Drawing.Point(156, 79);
            this.txtPowerPrepaid.Margin = new System.Windows.Forms.Padding(2);
            this.txtPowerPrepaid.MaxLength = 50;
            this.txtPowerPrepaid.Name = "txtPowerPrepaid";
            this.txtPowerPrepaid.Size = new System.Drawing.Size(197, 27);
            this.txtPowerPrepaid.TabIndex = 72;
            this.txtPowerPrepaid.Text = "0";
            this.txtPowerPrepaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPowerPrepaid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPowerTotal_KeyPress);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.Location = new System.Drawing.Point(212, 210);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 77;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCLOSE.Location = new System.Drawing.Point(290, 210);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 77;
            this.btnCLOSE.Text = "បិត";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnCHANGE_LOG
            // 
            this.btnCHANGE_LOG.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCHANGE_LOG.Location = new System.Drawing.Point(8, 210);
            this.btnCHANGE_LOG.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.Size = new System.Drawing.Size(75, 23);
            this.btnCHANGE_LOG.TabIndex = 77;
            this.btnCHANGE_LOG.Text = "ប្រវត្តិកែប្រែ";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Location = new System.Drawing.Point(1, 204);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(371, 1);
            this.panel2.TabIndex = 78;
            // 
            // DialogPowerAdjustment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 263);
            this.Name = "DialogPowerAdjustment";
            this.Text = "កំណត់ថាមពល";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DateTimePicker dtpMonth;
        private Label lblMONTH;
        private TextBox txtPowerPrepaid;
        private TextBox txtPowerPostpaid;
        private TextBox txtPowerDiscount;
        private TextBox txtPowerProduction;
        private Label label15;
        private Label label12;
        private Label label9;
        private Label lblPREPAID;
        private Label lblPOSTPAID;
        private Label label5;
        private Label lblPOWER_DISCOUNT;
        private Label lblPOWER_PRODUCTION;
        private ExButton btnOK;
        private ExButton btnCLOSE;
        private ExButton btnCHANGE_LOG;
        private Panel panel2;
    }
}