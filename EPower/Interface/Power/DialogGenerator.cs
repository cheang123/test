﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogGenerator : ExDialog
    {
        GeneralProcess _flag;

        TBL_GENERATOR _objNew = new TBL_GENERATOR();

        public TBL_GENERATOR Generator
        {
            get { return _objNew; }
        }
        TBL_GENERATOR _objOld = new TBL_GENERATOR();

        #region Constructor
        public DialogGenerator(GeneralProcess flag, TBL_GENERATOR objGenerator)
        {
            InitializeComponent();
            _flag = flag;
            objGenerator._CopyTo(_objNew);
            objGenerator._CopyTo(_objOld);

            UIHelper.SetDataSourceToComboBox(cboCapacity, DBDataContext.Db.TBL_CAPACITies.Where(x => x.CAPACITY_ID > 0).ToList().OrderBy(x => Lookup.ParseCapacity(x.CAPACITY)));
            UIHelper.SetDataSourceToComboBox(cboFuleType, Lookup.GetFuelType());

            this.Text = flag.GetText(this.Text);
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //if duplicate record.
            txtGeneratorNo.ClearAllValidation();
            if (DBDataContext.Db.IsExits(_objNew, "GENERATOR_NO"))
            {
                txtGeneratorNo.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblGENERATOR_NO.Text));
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {
            txtGeneratorNo.Text = _objNew.GENERATOR_NO;
            cboCapacity.SelectedValue = _objNew.CAPACITY_ID;
            cboFuleType.SelectedValue = _objNew.FUEL_TYPE_ID;
            dtpStartDate.Value = _objNew.START_DATE;
            dtpEndDate.Value = _objNew.END_DATE;
            dtpEndDate.Checked = !_objNew.IS_INUSED;
            txtNote.Text = _objNew.NOTE;
            if (_objNew.IS_INUSED)
            {
                dtpEndDate.ClearValue();
            }
            txtHorsePower.Text = _objNew.HORSE_POWER.ToString();
            txtShift1.Text = _objNew.SHIFT_1;
            txtShift2.Text = _objNew.SHIFT_2;
            txtOperationHour.Text = _objNew.OPERATION_HOUR.ToString();
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            btnOK.Focus();
            _objNew.GENERATOR_NO = txtGeneratorNo.Text.Trim();
            _objNew.CAPACITY_ID = (int)cboCapacity.SelectedValue;
            _objNew.FUEL_TYPE_ID = (int)cboFuleType.SelectedValue;
            _objNew.START_DATE = dtpStartDate.Value;
            _objNew.IS_INUSED = !dtpEndDate.Checked;
            _objNew.NOTE = txtNote.Text;
            if (_objNew.IS_INUSED)
            {
                dtpEndDate.Value = UIHelper._DefaultDate;
            }
            _objNew.END_DATE = dtpEndDate.Value;
            _objNew.HORSE_POWER = DataHelper.ParseToDecimal(txtHorsePower.Text);
            _objNew.SHIFT_1 = txtShift1.Text;
            _objNew.SHIFT_2 = txtShift2.Text;
            _objNew.OPERATION_HOUR = DataHelper.ParseToDecimal(txtOperationHour.Text);
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (txtGeneratorNo.Text.Trim() == string.Empty)
            {
                txtGeneratorNo.SetValidation(string.Format(Resources.REQUIRED, lblGENERATOR_NO.Text));
                val = true;
            }
            if (cboCapacity.SelectedIndex == -1)
            {
                cboCapacity.SetValidation(string.Format(Resources.REQUIRED, lblFUEL_TYPE_AND_CAPACITY.Text));
                val = true;
            }
            if (cboFuleType.SelectedIndex == -1)
            {
                cboFuleType.SetValidation(string.Format(Resources.REQUIRED, lblFUEL_TYPE_AND_CAPACITY.Text));
                val = true;
            }
            if (txtHorsePower.Text.Trim() == string.Empty)
            {
                txtHorsePower.SetValidation(string.Format(Resources.REQUIRED, lblHORSE_POWER.Text));
                val = true;
            }
            if (txtOperationHour.Text.Trim() == string.Empty)
            {
                txtOperationHour.SetValidation(string.Format(Resources.REQUIRED, lblOPERATION_HOUR.Text));
                val = true;
            }

            return val;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }
        #endregion

        private void dtpEndDate_MouseDown(object sender, MouseEventArgs e)
        {
            if (dtpEndDate.Checked)
            {
                if (dtpEndDate.Value.Date.Equals(UIHelper._DefaultDate))
                {
                    dtpEndDate.SetValue(DBDataContext.Db.GetSystemDate());
                }
                else
                {
                    dtpEndDate.CustomFormat = UIHelper._DefaultDateFormat;
                }
            }
            else
            {
                dtpEndDate.ClearValue();
            }
        }

        private void txtOperationHour_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }
    }
}