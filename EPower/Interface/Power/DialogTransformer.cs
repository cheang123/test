﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;


namespace EPower.Interface
{
    public partial class DialogTransformer : ExDialog
    {
        GeneralProcess _flag;

        TBL_TRANSFORMER _objNew = new TBL_TRANSFORMER();

        public TBL_TRANSFORMER Transformer
        {
            get { return _objNew; }
        }
        TBL_TRANSFORMER _objOld = new TBL_TRANSFORMER();
        bool _load = false;
        #region Constructor
        public DialogTransformer(GeneralProcess flag, TBL_TRANSFORMER obj)
        {
            InitializeComponent();
            _load = false;
            dtpStartDate.CustomFormat = UIHelper._DefaultDateFormat;
            _flag = flag;
            obj._CopyTo(_objNew);
            obj._CopyTo(_objOld);

            UIHelper.SetDataSourceToComboBox(cboCapacity, DBDataContext.Db.TBL_CAPACITies.Where(x => x.CAPACITY_ID > 0).ToList().OrderBy(x => Lookup.ParseCapacity(x.CAPACITY)));
            //UIHelper.SetDataSourceToComboBox(cboCapacity,Lookup.GetPowerCapacity());
            UIHelper.SetDataSourceToComboBox(cboPhase, Lookup.GetPowerPhases());
            UIHelper.SetDataSourceToComboBox(cboTransfoType, Lookup.GetLookUpValue((int)LookUp.TRANSFORMER_TYPE));
            UIHelper.SetDataSourceToComboBox(cboPositionType, Lookup.GetLookUpValue((int)LookUp.TRANSFORMER_POSITION));
            UIHelper.SetDataSourceToComboBox(cboHighVoltage, Lookup.GetPowerVoltage());
            UIHelper.SetDataSourceToComboBox(cboLowVoltage, Lookup.GetPowerVoltage());
            UIHelper.SetDataSourceToComboBox(cboTransformerBrand, Lookup.GetTransfomerBrand());
            bindPole();

            this.Text = flag.GetText(this.Text);
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
            _load = true;
        }
        #endregion

        #region Method

        private void read()
        {
            txtNo.Text = _objNew.TRANSFORMER_CODE;
            cboCapacity.SelectedValue = _objNew.CAPACITY_ID;
            cboPhase.SelectedValue = _objNew.PHASE_ID;
            cboTransformerBrand.SelectedValue = _objNew.TRANSFORMER_BRAND_ID;
            dtpStartDate.Value = _objNew.START_DATE;
            dtpEndDate.Value = _objNew.END_DATE;
            dtpEndDate.Checked = !_objNew.IS_INUSED;
            txtNote.Text = _objNew.NOTE;
            if (_objNew.IS_INUSED)
            {
                dtpEndDate.ClearValue();
            }
            cboHighVoltage.SelectedValue = _objNew.HIGH_VOLTAGE_ID;
            cboLowVoltage.SelectedValue = _objNew.LOW_VOLTAGE_ID;
            cboTransfoType.SelectedValue = _objNew.TRANSFORMER_TYPE_ID;
            cboPositionType.SelectedValue = _objNew.TRANSFORMER_POSITION_ID;
            lblOWN.Checked = _objNew.IS_PROPERTY;
            cboPOLE_CODE.Value = _objNew.POLE_ID;
        }

        private void write()
        {
            _objNew.TRANSFORMER_CODE = txtNo.Text.Trim();
            _objNew.CAPACITY_ID = (int)cboCapacity.SelectedValue;
            _objNew.PHASE_ID = (int)cboPhase.SelectedValue;
            _objNew.TRANSFORMER_BRAND_ID = (int)cboTransformerBrand.SelectedValue;
            _objNew.START_DATE = dtpStartDate.Value;
            _objNew.IS_INUSED = !dtpEndDate.Checked;
            _objNew.NOTE = txtNote.Text;
            if (_objNew.IS_INUSED)
            {
                dtpEndDate.Value = UIHelper._DefaultDate;
            }
            _objNew.END_DATE = dtpEndDate.Value;
            _objNew.HIGH_VOLTAGE_ID = (int)cboHighVoltage.SelectedValue;
            _objNew.LOW_VOLTAGE_ID = (int)cboLowVoltage.SelectedValue;
            _objNew.TRANSFORMER_TYPE_ID = (int)cboTransfoType.SelectedValue;
            _objNew.TRANSFORMER_POSITION_ID = (int)cboPositionType.SelectedValue;
            _objNew.IS_PROPERTY = lblOWN.Checked;
            _objNew.POLE_ID = _objNew.TRANSFORMER_POSITION_ID == (int)TransfoPositonType.OUTDOOR ? (int)cboPOLE_CODE.Value : 0;
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (txtNo.Text.Trim() == string.Empty)
            {
                txtNo.SetValidation(string.Format(Resources.REQUIRED, lblTRANSFORMER_CODE.Text));
                val = true;
            }
            if (cboCapacity.SelectedIndex == -1)
            {
                cboCapacity.SetValidation(string.Format(Resources.REQUIRED, lblCAPACITY_AND_PHASE.Text));
                val = true;
            }
            if (cboCapacity.SelectedIndex == -1)
            {
                cboCapacity.SetValidation(string.Format(Resources.REQUIRED, lblCAPACITY_AND_PHASE.Text));
                val = true;
            }
            if (cboPhase.SelectedIndex == -1)
            {
                cboPhase.SetValidation(string.Format(Resources.REQUIRED, lblCAPACITY_AND_PHASE.Text));
                val = true;
            }
            if (cboTransformerBrand.SelectedIndex == -1)
            {
                cboTransformerBrand.SetValidation(string.Format(Resources.REQUIRED, lblTRANSFORMER_BRAND.Text));
                val = true;
            }
            if (cboHighVoltage.SelectedIndex == -1)
            {
                cboHighVoltage.SetValidation(string.Format(Resources.REQUIRED, lblMV.Text));
                val = true;
            }
            if (cboLowVoltage.SelectedIndex == -1)
            {
                cboLowVoltage.SetValidation(string.Format(Resources.REQUIRED, lblLV.Text));
                val = true;
            }
            if (cboTransfoType.SelectedIndex == -1)
            {
                cboTransfoType.SetValidation(string.Format(Resources.REQUIRED, lblTRANSFORMER_TYPE.Text));
                val = true;
            }
            if (cboPositionType.SelectedIndex == -1)
            {
                cboPositionType.SetValidation(string.Format(Resources.REQUIRED, lblTRANSFORMER_POSITION.Text));
                val = true;
            }
            if ((int)cboPositionType.SelectedValue == (int)TransfoPositonType.OUTDOOR && cboPOLE_CODE.Value == null)
            {
                cboPOLE_CODE.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, lblPOLE_CODE.Text));
                val = true;
            }
            return val;
        }

        private void bindPole()
        {
            var pole = (from p in DBDataContext.Db.TBL_POLEs
                        join a in DBDataContext.Db.TBL_AREAs on p.AREA_ID equals a.AREA_ID
                        orderby a.AREA_CODE, p.POLE_CODE
                        where p.IS_ACTIVE
                        select new DropDownItem.DropDownItemModel()
                        {
                            Id = p.POLE_ID,
                            Name = a.AREA_CODE + " " + p.POLE_CODE,
                            DisplayText = p.POLE_CODE,
                            ItemObject = p
                        }).ToList();
            cboPOLE_CODE.DropDownPanel = new DropDownItem(pole.ToList());
        }

        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //if duplicate record.
            txtNo.ClearAllValidation();
            if (DBDataContext.Db.IsExits(_objNew, "TRANSFORMER_CODE"))
            {
                txtNo.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblTRANSFORMER_CODE.Text));
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void dtpEndDate_MouseDown(object sender, MouseEventArgs e)
        {
            if (dtpEndDate.Checked)
            {
                if (dtpEndDate.Value.Date.Equals(UIHelper._DefaultDate))
                {
                    dtpEndDate.SetValue(DBDataContext.Db.GetSystemDate());
                }
                else
                {
                    dtpEndDate.CustomFormat = UIHelper._DefaultDateFormat;
                }
            }
            else
            {
                dtpEndDate.ClearValue();
            }
        }

        private void cboPositionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_load)
            {
                return;
            }
            if ((int)cboPositionType.SelectedValue == (int)TransfoPositonType.OUTDOOR)
            {
                cboPOLE_CODE.Visible =
                lblPOLE_CODE.Visible = true;
            }
            else
            {
                cboPOLE_CODE.Visible =
                lblPOLE_CODE.Visible = false;
            }
        }
        private void bindTransformerBrand()
        {
            UIHelper.SetDataSourceToComboBox(cboTransformerBrand,
                from t in DBDataContext.Db.TBL_TRANSFORMER_BRANDs
                where t.IS_ACTIVE
                orderby t.TRANSFORMER_BRAND_NAME
                select new
                {
                    t.TRANSFORMER_BRAND_ID,
                    t.TRANSFORMER_BRAND_NAME
                }, "TRANSFORMER_BRAND_ID", "TRANSFORMER_BRAND_NAME");
        }

        private void btnAddTransformerType_AddItem(object sender, EventArgs e)
        {
            DialogTransformerBrand dig = new DialogTransformerBrand(GeneralProcess.Insert, new TBL_TRANSFORMER_BRAND());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                bindTransformerBrand();
                cboTransformerBrand.SelectedValue = dig.TranBrand.TRANSFORMER_BRAND_ID;
            }
        }
    }
}