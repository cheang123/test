﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using SoftTech;

namespace EPower.Interface
{
    public partial class DialogHistorySendReport : ExDialog
    {
        public DialogHistorySendReport(DateTime month)
        {
            InitializeComponent();
            Load(month);
        }

        private void Load(DateTime month)
        {
            var result = (from pps in DBDataContext.Db.TBL_POWER_PURCHASE_SENDs
                         orderby pps.CREATED_ON descending
                         where pps.DATA_MONTH == month
                         select new
                         {
                             CREATED_ON = pps.CREATED_ON,
                             CREATE_BY = pps.CREATE_BY,
                             MONTH = pps.DATA_MONTH,
                             REASON = pps.NOTE,
                             NO_INVOICE = pps.TOTAL_INVOICE
                         }).ToList();
            dgv.DataSource = result;
        }

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
