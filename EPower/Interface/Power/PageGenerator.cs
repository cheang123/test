﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageGenerator : Form
    {
        public  TBL_GENERATOR Generator
        {
            get
            {
                TBL_GENERATOR objDisFaci = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intid = (int)dgv.SelectedRows[0].Cells[GENERATOR_ID.Name].Value;
                        objDisFaci = DBDataContext.Db.TBL_GENERATORs.FirstOrDefault(x => x.GENERATOR_ID == intid);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objDisFaci;
            }
        }
        private bool _binging = false;
        #region Constructor
        public PageGenerator()
        {
            InitializeComponent();
            this.btnREPORT.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_QUARTERLY_REPORT]);
            UIHelper.DataGridViewProperties(dgv);
            lookup();
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation 
        private void btnNew_Click(object sender, EventArgs e)
        { 
            DialogGenerator dig = new DialogGenerator(GeneralProcess.Insert, new TBL_GENERATOR() 
            {
                GENERATOR_NO=string.Empty,
                START_DATE=DBDataContext.Db.GetSystemDate(),
                END_DATE=UIHelper._DefaultDate,
                IS_INUSED=true, 
            } );
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Generator.GENERATOR_ID);
            }
        }
         
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (Generator==null)
            {
                return;
            }
            DialogGenerator dig = new DialogGenerator(GeneralProcess.Update, Generator);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Generator.GENERATOR_ID);
            }
        }
         
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (Generator==null)
            {
                return;
            }
            if (!IsDeletable())
            {
                MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                return;
            }
            DialogGenerator dig = new DialogGenerator(GeneralProcess.Delete, Generator);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Generator.GENERATOR_ID - 1);
            } 
        }

        private bool IsDeletable()
        {
            return DBDataContext.Db.TBL_POWER_GENERATIONs.Where(x => x.GENERATOR_ID == Generator.GENERATOR_ID && x.IS_ACTIVE).Count()==0; 
        }
         
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                if (_binging) return;
                int capacityId = 0;
                int fuelTypeId = 0;
                if (cboCapacity.SelectedIndex != -1)
                {
                    capacityId = int.Parse(cboCapacity.SelectedValue.ToString());
                }
                if (cboFuleType.SelectedIndex != -1)
                {
                    fuelTypeId = int.Parse(cboFuleType.SelectedValue.ToString());
                }

                var db = from g in DBDataContext.Db.TBL_GENERATORs
                                 join c in DBDataContext.Db.TBL_CAPACITies on g.CAPACITY_ID equals c.CAPACITY_ID
                                 join f in DBDataContext.Db.TLKP_FUEL_TYPEs on g.FUEL_TYPE_ID equals f.FUEL_TYPE_ID
                                 where g.IS_ACTIVE
                                        && (capacityId == 0 || g.CAPACITY_ID == capacityId)
                                        && (fuelTypeId == 0 || g.FUEL_TYPE_ID == fuelTypeId)
                                        && string.Concat(g.GENERATOR_NO, ' ', f.FUEL_TYPE_NAME).ToLower().Contains(txtQuickSearch.Text.ToLower())
                                 orderby g.GENERATOR_NO, g.START_DATE
                                 select new
                                 {
                                     g.GENERATOR_ID,
                                     g.GENERATOR_NO,
                                     c.CAPACITY,
                                     f.FUEL_TYPE_NAME,
                                     g.START_DATE,
                                     END_DATE = g.IS_INUSED ? string.Empty : string.Concat(g.END_DATE.Day, "-", g.END_DATE.Month, "-", g.END_DATE.Year),
                                     g.IS_INUSED,
                                     g.HORSE_POWER,
                                     g.OPERATION_HOUR
                                 };
                dgv.DataSource = db;
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
            }
            

        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        private void btnGenerationOperation_Click(object sender, EventArgs e)
        {
            TBL_GENERATOR_OPERATION obj = DBDataContext.Db.TBL_GENERATOR_OPERATIONs.FirstOrDefault();
            GeneralProcess flag = GeneralProcess.Update;
            if (obj==null)
            {
                flag = GeneralProcess.Insert;
                obj = new TBL_GENERATOR_OPERATION()
                {
                    HAVE_METER = true,
                    YEAR_METER = DBDataContext.Db.GetSystemDate()
                };
            }
            new DialogGeneratorOperation(flag, obj).ShowDialog();
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            var diag = new DialogReportQuarterOptions(false);
            diag.chkTABLE_GENERATION_FACILITY.Checked = true;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                var doc = PageReportQuaterly.OpenReport(diag);
                doc.ViewReport("");
            }
        }

        public void lookup()
        {
            _binging = true;
            UIHelper.SetDataSourceToComboBox(cboFuleType, Lookup.GetFuelType(), Resources.ALL_FUEL_TYPE);
            UIHelper.SetDataSourceToComboBox(cboCapacity, DBDataContext.Db.TBL_CAPACITies.Where(x => x.CAPACITY_ID > 0).ToList().OrderBy(x => Lookup.ParseCapacity(x.CAPACITY)), Resources.ALL_CAPACITY);
            _binging = false;
        } 
    }
}
