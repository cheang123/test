﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPaymentHistoryCancel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblWarning = new System.Windows.Forms.Label();
            this.dtpPayDate = new System.Windows.Forms.DateTimePicker();
            this.lblDATE = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtREASON = new System.Windows.Forms.TextBox();
            this.lblREASON = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.txtREASON);
            this.content.Controls.Add(this.lblREASON);
            this.content.Controls.Add(this.lblDATE);
            this.content.Controls.Add(this.dtpPayDate);
            this.content.Controls.Add(this.lblWarning);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Size = new System.Drawing.Size(443, 203);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblWarning, 0);
            this.content.Controls.SetChildIndex(this.dtpPayDate, 0);
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.lblREASON, 0);
            this.content.Controls.SetChildIndex(this.txtREASON, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(0, 166);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(442, 1);
            this.panel1.TabIndex = 21;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(355, 172);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 2;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOK.Location = new System.Drawing.Point(276, 172);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.Location = new System.Drawing.Point(47, 10);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(45, 19);
            this.lblWarning.TabIndex = 27;
            this.lblWarning.Text = "សម្គាល់";
            // 
            // dtpPayDate
            // 
            this.dtpPayDate.CustomFormat = "dd-MM​-yyyy hh:mm tt";
            this.dtpPayDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPayDate.Location = new System.Drawing.Point(262, 42);
            this.dtpPayDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtpPayDate.Name = "dtpPayDate";
            this.dtpPayDate.Size = new System.Drawing.Size(168, 27);
            this.dtpPayDate.TabIndex = 348;
            // 
            // lblDATE
            // 
            this.lblDATE.AutoSize = true;
            this.lblDATE.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblDATE.Location = new System.Drawing.Point(178, 48);
            this.lblDATE.Margin = new System.Windows.Forms.Padding(2);
            this.lblDATE.Name = "lblDATE";
            this.lblDATE.Size = new System.Drawing.Size(65, 19);
            this.lblDATE.TabIndex = 349;
            this.lblDATE.Text = "កាលបរិច្ឆេទ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(70, 51);
            this.label9.Margin = new System.Windows.Forms.Padding(1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 20);
            this.label9.TabIndex = 352;
            this.label9.Text = "*";
            this.label9.Visible = false;
            // 
            // txtREASON
            // 
            this.txtREASON.Location = new System.Drawing.Point(20, 74);
            this.txtREASON.Margin = new System.Windows.Forms.Padding(2);
            this.txtREASON.Multiline = true;
            this.txtREASON.Name = "txtREASON";
            this.txtREASON.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtREASON.Size = new System.Drawing.Size(410, 77);
            this.txtREASON.TabIndex = 351;
            // 
            // lblREASON
            // 
            this.lblREASON.AutoSize = true;
            this.lblREASON.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblREASON.Location = new System.Drawing.Point(16, 49);
            this.lblREASON.Margin = new System.Windows.Forms.Padding(2);
            this.lblREASON.Name = "lblREASON";
            this.lblREASON.Size = new System.Drawing.Size(53, 19);
            this.lblREASON.TabIndex = 350;
            this.lblREASON.Text = "មូលហេតុ";
            // 
            // DialogPaymentHistoryCancel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 226);
            this.Name = "DialogPaymentHistoryCancel";
            this.Text = "លុបការបង់ប្រាក់";
            this.Load += new System.EventHandler(this.DialogPaymentHistoryCancel_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblWarning;
        private Label lblDATE;
        public DateTimePicker dtpPayDate;
        private Label label9;
        public TextBox txtREASON;
        private Label lblREASON;
    }
}