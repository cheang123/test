﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Base.Properties;
using HB01.Domain.Enums;
using HB01.Domain.ListModels;
using SoftTech;
using SoftTech.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PagePaymentMethod : Form
    {
        List<Base.Logic.Currency> currencies = new List<Base.Logic.Currency>();
        public PagePaymentMethod()
        {
            InitializeComponent();
            this.searchControl.Properties.NullValuePrompt = Resources.SEARCH;
            currencies = SettingLogic.Currencies;
            this.dgvPaymentMethod.Columns[colACCOUNT_KHR.FieldName].Visible = currencies.Any(x => x.CURRENCY_ID == (int)Currency.KHR);
            this.dgvPaymentMethod.Columns[colACCOUNT_USD.FieldName].Visible = currencies.Any(x => x.CURRENCY_ID == (int)Currency.USD);
            this.dgvPaymentMethod.Columns[colACCOUNT_THB.FieldName].Visible = currencies.Any(x => x.CURRENCY_ID == (int)Currency.THB);
            this.dgvPaymentMethod.Columns[colACCOUNT_VND.FieldName].Visible = currencies.Any(x => x.CURRENCY_ID == (int)Currency.VND);
            bind();
        }

        #region Method
        private void bind()
        {
            resAccount.DataSource = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Asset } });
            try
            {
                dgv.DataSource = DBDataContext.Db.TBL_PAYMENT_METHODs.Where(x => x.IS_ACTIVE).ToList();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        #endregion

        #region Operation
        private void btnEdit_Click(object sender, EventArgs e)
        {
            TBL_PAYMENT_METHOD obj = (TBL_PAYMENT_METHOD)dgvPaymentMethod.GetFocusedRow();
            if (obj != null)
            {
                DialogPaymentMethod diag = new DialogPaymentMethod(obj, GeneralProcess.Update);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    bind();
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogPaymentMethod diag = new DialogPaymentMethod(new TBL_PAYMENT_METHOD()
            {
                PAYMENT_METHOD_ID = 0,
                //PAYMENT_METHOD_NAME = "",
                IS_ACTIVE = true,
                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                CREATE_ON = DBDataContext.Db.GetSystemDate()
            }, GeneralProcess.Insert);

            if (diag.ShowDialog() == DialogResult.OK)
            {
                bind();
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            TBL_PAYMENT_METHOD obj = (TBL_PAYMENT_METHOD)dgvPaymentMethod.GetFocusedRow();
            if (obj != null)
            {
                DialogPaymentMethod diag = new DialogPaymentMethod(obj, GeneralProcess.Delete);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    this.bind();
                }
            }
        }

        #endregion

    }
}
