﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageConstant : Form
    {
        public TBL_CONSTANT Constant
        {
            get
            {
                TBL_CONSTANT objConstant = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int constantID = (int)dgv.SelectedRows[0].Cells["CONSTANT_ID"].Value;
                        objConstant = DBDataContext.Db.TBL_CONSTANTs.FirstOrDefault(x => x.CONSTANT_ID == constantID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objConstant;
            }
        }

        #region Constructor
        public PageConstant()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new constant.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogConstant dig = new DialogConstant(GeneralProcess.Insert, new TBL_CONSTANT() { CONSTANT_NAME = "" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Constant.CONSTANT_ID);
            }
        }

        /// <summary>
        /// Edit constant.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogConstant dig = new DialogConstant(GeneralProcess.Update, Constant);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Constant.CONSTANT_ID);
                }
            }
        }

        /// <summary>
        /// Remove constant
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogConstant dig = new DialogConstant(GeneralProcess.Delete, Constant);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Constant.CONSTANT_ID-1);
                }
            }
        }

        /// <summary>
        /// Load data from database and display.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                 dgv.DataSource = from con in DBDataContext.Db.TBL_CONSTANTs
                                             where con.IS_ACTIVE &&
                                             con.CONSTANT_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                                             select con;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
           
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            //TODO: can delete record.
            
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
                if (DBDataContext.Db.TBL_METER_TYPEs.Where(x=>x.METER_CONST_ID==Constant.CONSTANT_ID && x.IS_ACTIVE).Count()>0 ||
                    DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs.Where(x=>x.BREAKER_CONST_ID== Constant.CONSTANT_ID && x.IS_ACTIVE).Count()>0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    val = false;
                }
            }

            return val;
        }
        #endregion

        
    }
}