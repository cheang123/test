﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PagePosition : Form
    {
         
        public TBL_POSITION Position
        {
            get
            {
                TBL_POSITION objPosition = null;
                if (dgvPosition.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intPositionId = (int)dgvPosition.SelectedRows[0].Cells[POSITION_ID.Name].Value;
                        objPosition = DBDataContext.Db.TBL_POSITIONs.FirstOrDefault(x => x.POSITION_ID == intPositionId && x.IS_ACTIVE);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objPosition;
            }
        }

        public TBL_EMPLOYEE Employee
        {
            get
            {
                TBL_EMPLOYEE objEmp = null;
                if (dgvEmployee.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intEmpId = (int)dgvEmployee.SelectedRows[0].Cells[EMPLOYEE_ID.Name].Value;
                        objEmp = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault(x => x.EMPLOYEE_ID == intEmpId && x.IS_ACTIVE);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objEmp;
            }
        }
        
        bool isLoad = false;

        #region Constructor
        public PagePosition()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgvPosition);
            lookup();
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                if (!isLoad) return;
                int valueId = 0;
                if (cboLookup.SelectedIndex != -1)
                {
                    valueId = (int)cboLookup.SelectedValue;
                }

                dgvPosition.DataSource = from p in DBDataContext.Db.TBL_POSITIONs
                                     join l in DBDataContext.Db.TBL_LOOKUP_VALUEs on p.BUSINESS_DIVISION_ID equals l.VALUE_ID
                                         where (p.IS_ACTIVE && l.IS_ACTIVE && (valueId == 0 || p.BUSINESS_DIVISION_ID == valueId)) &&
                                            (p.POSITION_NAME.ToLower()+l.VALUE_TEXT.ToLower()).Contains(txtQuickSearch.Text.ToLower().Trim())
                                     orderby l.VALUE_TEXT
                                     select new
                                     {
                                         p.POSITION_ID,
                                         p.POSITION_NAME,
                                         p.IS_PAYROLL,
                                         l.VALUE_TEXT
                                     };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// Add new pole.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            TBL_POSITION objPosition = new TBL_POSITION() { POSITION_NAME = "" };
            if (cboLookup.SelectedIndex!=-1)
            {
                objPosition.BUSINESS_DIVISION_ID = (int)cboLookup.SelectedValue;
            }

            DialogPosition dig = new DialogPosition(GeneralProcess.Insert, objPosition);
            if (dig.ShowDialog() == DialogResult.OK)
            {                
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgvPosition, dig.Position.POSITION_ID);
            }
        }

        /// <summary>
        /// Edit pole.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvPosition.SelectedRows.Count > 0)
            {
                DialogPosition dig = new DialogPosition(GeneralProcess.Update, Position);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvPosition, dig.Position.POSITION_ID);
                }
            }
        }

        /// <summary>
        /// Remove pole.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsPositionDeletable())
            {
                DialogPosition dig = new DialogPosition(GeneralProcess.Delete, Position);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvPosition, dig.Position.POSITION_ID - 1);
                }    
            }                
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void dgvPole_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvPosition.SelectedRows.Count > 0)
            {
                int intPositionId = (int)dgvPosition.SelectedRows[0].Cells[POSITION_ID.Name].Value;
               var tmp = from emp in DBDataContext.Db.TBL_EMPLOYEEs
                                 where (emp.IS_ACTIVE &&
                                 emp.EMPLOYEE_NAME.ToLower().Contains(txtQuickSearch.Text.Trim().ToLower())) && 
                                  
                                 (DBDataContext.Db.TBL_EMPLOYEE_POSITIONs.
                                 Where(x => x.POSITION_ID == intPositionId).
                                 Any(x => x.EMPLOYEE_ID == emp.EMPLOYEE_ID) || intPositionId == 0)

                                 select emp;

                dgvEmployee.DataSource = tmp; 
            }
        }

        private void btnNewBox_Click(object sender, EventArgs e)
        {
            DialogCollector dig = new DialogCollector(GeneralProcess.Insert, new TBL_EMPLOYEE() { EMPLOYEE_NAME = "", PASSWORD = "" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgvEmployee, dig.Employee.EMPLOYEE_ID);
            }
        }

        private void btnEditBox_Click(object sender, EventArgs e)
        {
            if (dgvEmployee.SelectedRows.Count > 0)
            {
                DialogCollector dig = new DialogCollector(GeneralProcess.Update, Employee);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvEmployee, dig.Employee.EMPLOYEE_ID);
                }
            }
        }

        private void btnRemoveBox_Click(object sender, EventArgs e)
        {
            if (IsEmployeeDeletable())
            {
                DialogCollector dig = new DialogCollector(GeneralProcess.Delete, Employee);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvEmployee, dig.Employee.EMPLOYEE_ID - 1);
                }
            }
        }
        private void dgvPosition_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        private void dgvEmployee_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEditBox_Click(null, null);
        }
        #endregion

        #region Method
        public bool IsEmployeeDeletable()
        {
            bool val = false;
            //TODO: check database;
            if (dgvEmployee.SelectedRows.Count > 0)
            {
                val = true;
                if (DBDataContext.Db.TBL_POLEs.Where(x => (x.COLLECTOR_ID == Employee.EMPLOYEE_ID || x.BILLER_ID == Employee.EMPLOYEE_ID || x.CUTTER_ID == Employee.EMPLOYEE_ID) && x.IS_ACTIVE).Count() > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    val = false;
                }
            }
            return val;
        }

        public bool IsPositionDeletable()
        {
            bool val = false;
            //TODO: check database;
            if (dgvPosition.SelectedRows.Count>0)
            {
                val = true;
                if (DBDataContext.Db.TBL_EMPLOYEE_POSITIONs.Where(x => x.POSITION_ID == Position.POSITION_ID).Count() > 0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    val = false;
                }
            }
            return val;
        }

        public void lookup()
        {
            isLoad = false;
            UIHelper.SetDataSourceToComboBox(cboLookup, Lookup.GetLookUpValue((int)LookUp.BUSINESS_DIVISION), Resources.ALL_POSITION_TYPE);
            isLoad = true;
        }

        #endregion

         
    }
}
