﻿using EPower.Base.Logic;
using EPower.Properties;
using HB01.Base.Logics;
using HB01.Logics;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCompany : ExDialog
    {

        #region Private Data

        bool _blnIsFirstStart = false;
        TBL_COMPANY _objNew = new TBL_COMPANY();
        TBL_COMPANY _objOld = new TBL_COMPANY();
        TBL_TARIFF _objNewTariff = new TBL_TARIFF();
        TBL_TARIFF _objOldTariff = new TBL_TARIFF();
        DateTime dt = DBDataContext.Db.GetSystemDate();
        bool _loading = false;

        #endregion Private Data

        #region Constructor
        public DialogCompany()
        {
            InitializeComponent();

            _loading = true;

            var tmp = DBDataContext.Db.TBL_COMPANies.FirstOrDefault(c => c.COMPANY_ID == 1);
            tmp._CopyTo(_objNew);
            tmp._CopyTo(_objOld);

            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());
            var tmpTariff = DBDataContext.Db.TBL_TARIFFs.FirstOrDefault(x => x.DATA_MONTH.Year == dt.Year && x.DATA_MONTH.Month == dt.Month);
            if (tmpTariff != null)
            {
                tmpTariff._CopyTo(_objNewTariff);
                tmpTariff._CopyTo(_objOldTariff);
            }

            read();
            readTariff();
            _loading = false;
        }
        #endregion Constructor

        #region Method

        bool invalid()
        {

            bool blnReturn = false;

            this.ClearAllValidation();

            if (txtCompanyName.Text.Trim() == "")
            {
                txtCompanyName.SetValidation(string.Format(Resources.REQUIRED, lblCOMPANY_NAME.Text));
                blnReturn = true;
            }
            if (txtLiceseNo.Text.Trim() == string.Empty)
            {
                txtLiceseNo.SetValidation(string.Format(Resources.REQUIRED, lblLICENSE_NO_AND_COMPANY_TYPE.Text));
                blnReturn = true;
            }

            if (txtLicenseName.Text.Trim() == string.Empty)
            {
                txtLicenseName.SetValidation(string.Format(Resources.REQUIRED, lblLICENSE_NO_AND_COMPANY_TYPE.Text));
                blnReturn = true;
            }

            if (string.IsNullOrEmpty(txtVAT_TIN.Text.Trim()))
            {
                txtVAT_TIN.SetValidation(string.Format(Resources.REQUIRED, lblVAT_TIN.Text));
                blnReturn = true;
            }

            if (txtRequestTitle.Text.Trim() == "")
            {
                txtRequestTitle.SetValidation(string.Format(Resources.REQUIRED, lblLICENSEE_NAME_KH_AND_EN.Text));
                blnReturn = true;
            }
            if (txtLicenseName.Text.Trim() == string.Empty)
            {
                txtLicenseName.SetValidation(string.Format(Resources.REQUIRED, lblLICENSEE_NAME_KH_AND_EN.Text));
                blnReturn = true;
            }
            if (cboProvince.SelectedIndex == -1)
            {
                cboProvince.SetValidation(string.Format(Resources.REQUIRED, lblPROVINCE.Text));
                blnReturn = true;
            }
            if (cboDistrict.SelectedIndex == -1)
            {
                cboDistrict.SetValidation(string.Format(Resources.REQUIRED, lblDISTRICT.Text));
                blnReturn = true;
            }
            if (cboCommune.SelectedIndex == -1)
            {
                cboCommune.SetValidation(string.Format(Resources.REQUIRED, lblCOMMUNE.Text));
                blnReturn = true;
            }
            if (cboVillage.SelectedIndex == -1)
            {
                cboVillage.SetValidation(string.Format(Resources.REQUIRED, lblVILLAGE.Text));
                blnReturn = true;
            }
            if (DataHelper.ParseToDecimal(txtBasedTariff.Text) < 0 || string.IsNullOrEmpty(txtBasedTariff.Text))
            {
                txtBasedTariff.SetValidation(string.Format(Resources.REQUIRED_POSITIVE_NUMBER));
                blnReturn = true;
            }
            if (DataHelper.ParseToDecimal(txtSubsidyTariff.Text) < 0 || string.IsNullOrEmpty(txtSubsidyTariff.Text))
            {
                txtSubsidyTariff.SetValidation(string.Format(Resources.REQUIRED_POSITIVE_NUMBER));
                blnReturn = true;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED_SELECT_VALUE, Resources.CURRENCY));
                blnReturn = true;
            }
            if (txtAddressReceiver.Text.Trim() == "")
            {
                txtAddressReceiver.SetValidation(string.Format(Resources.REQUIRED, lblADDRESS_RECEIVER.Text));
                blnReturn = true;
            }
            if (txtFacebookLink.Text.Trim() == "")
            {
                txtFacebookLink.SetValidation(string.Format(Resources.REQUIRED, lblFACEBOOK_ACCOUNT.Text));
                blnReturn = true;
            }
            if (cboLicenseType.SelectedIndex == -1)
            {
                cboLicenseType.SetValidation(string.Format(Resources.REQUIRED, lblLICENSE_NO_AND_COMPANY_TYPE));
                blnReturn = true;
            }
            return blnReturn;
        }

        void bind()
        {
            UIHelper.SetDataSourceToComboBox(cboProvince, DBDataContext.Db.TLKP_PROVINCEs);
            UIHelper.SetDataSourceToComboBox(cboLicenseType, Lookup.GetLicenseType());
        }

        void read()
        {
            txtCompanyName.Text = _objNew.COMPANY_NAME;
            txtCompanyEng.Text = _objNew.COMPANY_NAME_EN;
            txtAddressCompany.Text = _objNew.ADDRESS;
            txtRequestTitle.Text = _objNew.LICENSE_NAME_KH;
            txtLiceseNo.Text = _objNew.LICENSE_NUMBER;
            txtLicenseName.Text = _objNew.LICENSE_NAME;
            txtAddressReceiver.Text = _objNew.ADDRESS_RECEIVER;
            txtFacebookLink.Text = _objNew.LINK_FACEBOOK;
            txtVAT_TIN.Text = _objNew.VATTIN;

            if (_objNew.COMPANY_LOGO != null)
            {
                picLogo.Image = UIHelper.ConvertBinaryToImage(_objNew.COMPANY_LOGO);
            }

            bind();

            loadVillage(this._objNew.VILLAGE_CODE);


        }

        void readTariff()
        {
            if (_objNewTariff != null)
            {
                txtBasedTariff.Text = _objNewTariff.BASED_TARIFF.ToString("#,###");
                txtSubsidyTariff.Text = _objNewTariff.SUBSIDY_TARIFF.ToString("#,###");
                cboCurrency.SelectedValue = _objNewTariff.CURRENCY_ID;
                cboLicenseType.SelectedValue = _objNewTariff.LICENSE_TYPE_ID;
            }
        }

        private void loadVillage(string villageCode)
        {
            this.cboProvince.SelectedIndex = -1;
            this.cboDistrict.SelectedIndex = -1;
            this.cboCommune.SelectedIndex = -1;
            this.cboVillage.SelectedIndex = -1;

            if (DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode) != null)
            {
                string communeCode = DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode).COMMUNE_CODE;
                string districCode = DBDataContext.Db.TLKP_COMMUNEs.FirstOrDefault(row => row.COMMUNE_CODE == communeCode).DISTRICT_CODE;
                string province = DBDataContext.Db.TLKP_DISTRICTs.FirstOrDefault(row => row.DISTRICT_CODE == districCode).PROVINCE_CODE;

                UIHelper.SetDataSourceToComboBox(this.cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(row => row.PROVINCE_CODE == province));
                UIHelper.SetDataSourceToComboBox(this.cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(row => row.DISTRICT_CODE == districCode));
                UIHelper.SetDataSourceToComboBox(this.cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(row => row.COMMUNE_CODE == communeCode));

                this.cboProvince.SelectedValue = province;
                this.cboDistrict.SelectedValue = districCode;
                this.cboCommune.SelectedValue = communeCode;
                this.cboVillage.SelectedValue = villageCode;
            }
        }
        void write()
        {
            _objNew.COMPANY_NAME = txtCompanyName.Text;
            _objNew.COMPANY_NAME_EN = txtCompanyEng.Text;
            _objNew.LICENSE_NUMBER = txtLiceseNo.Text;
            _objNew.ADDRESS = txtAddressCompany.Text;
            _objNew.LICENSE_NAME_KH = txtRequestTitle.Text;
            _objNew.LICENSE_NAME = txtLicenseName.Text;
            _objNew.VILLAGE_CODE = cboVillage.SelectedValue.ToString();
            _objNew.LICENSE_TYPE = cboLicenseType.Text;
            _objNew.ADDRESS_RECEIVER = txtAddressReceiver.Text;
            _objNew.LINK_FACEBOOK = txtFacebookLink.Text;
            _objNew.VATTIN = txtVAT_TIN.Text;
            if (picLogo.Image != null)
            {
                _objNew.COMPANY_LOGO = UIHelper.ConvertImageToBinary(picLogo.Image);
            }

            if (PointerLogic.isConnectedPointer && Current.Company != null)
            {
                FileObject fileObj = new FileObject()
                {
                    File = picLogo.ImageLocation,
                    FileName = Path.GetFileName(picLogo.ImageLocation),
                    MediaTypeId = 1,
                    Note = $"Profile / Indentification Picture"
                };
                Media media = null;
                try
                {
                    media = MediaAPI.Instance.UploadFile(fileObj);
                }
                catch (Exception ex)
                {
                    //  picEdit.PostExecute();
                    //throw new Exception(ex.Message);
                }
                Current.Company.Name = txtCompanyName.Text;
                Current.Company.NameLatin = txtCompanyEng.Text;
                Current.Company.TIN = txtVAT_TIN.Text;
                Current.Company.Address = txtAddressCompany.Text;
                Current.Company.LogoId = media?.Url;
                CompanyLogic.Instance.Edit(Current.Company);
            }
        }

        #endregion Method

        private void cboProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loading && cboProvince.SelectedIndex != -1 && !_blnIsFirstStart)
            {
                string strProvinceCode = cboProvince.SelectedValue.ToString();
                //distick
                UIHelper.SetDataSourceToComboBox(cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(d => d.PROVINCE_CODE == strProvinceCode));
            }
        }

        private void cboDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loading && cboProvince.SelectedIndex != -1 && !_blnIsFirstStart)
            {
                string strDisCode = cboDistrict.SelectedValue.ToString();
                //communte
                UIHelper.SetDataSourceToComboBox(cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(c => c.DISTRICT_CODE == strDisCode));
            }
        }

        private void cboCommune_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loading && cboCommune.SelectedIndex != -1 && !_blnIsFirstStart)
            {
                string strComCode = cboCommune.SelectedValue.ToString();
                //village
                UIHelper.SetDataSourceToComboBox(cboVillage, DBDataContext.Db.TLKP_VILLAGEs.Where(v => v.COMMUNE_CODE == strComCode));
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (invalid())
            {
                return;
            }
            //Runner.RunNewThread(delegate ()
            //{
                write();
            //});
            try
            {
                int tariffCurrency = (int)cboCurrency.SelectedValue;
                int tariffLicensetypeId = (int)cboLicenseType.SelectedValue;
                Runner.RunNewThread(delegate
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                        DateTime dt = DBDataContext.Db.GetSystemDate();
                        for (int i = 1; i <= 12; i++)
                        {
                            DateTime dataMonth;
                            dataMonth = new DateTime(dt.Year, i, 1).AddMonths(2);
                            if (dataMonth.Year >= 2019)
                            {
                                dataMonth = new DateTime(dt.Year, i, 1);
                            }
                            TBL_TARIFF objTariff = DBDataContext.Db.TBL_TARIFFs.FirstOrDefault(x => x.DATA_MONTH == dataMonth);
                            DateTime Jan_tariff = new DateTime(2020, 1, 1);
                            DateTime July_tarif = new DateTime(2020, 7, 1);
                            TBL_TARIFF last_tariff = DBDataContext.Db.TBL_TARIFFs.FirstOrDefault(x => x.DATA_MONTH == Jan_tariff.AddMonths(-1));
                            if (objTariff == null)
                            {
                                if (dataMonth == Jan_tariff)
                                {
                                    if (last_tariff != null)
                                    {
                                        DBDataContext.Db.Insert(new TBL_TARIFF()
                                        {
                                            BASED_TARIFF = last_tariff.BASED_TARIFF,
                                            DATA_MONTH = Jan_tariff,
                                            SUBSIDY_TARIFF = last_tariff.SUBSIDY_TARIFF,
                                            CURRENCY_ID = last_tariff.CURRENCY_ID,
                                            LICENSE_TYPE_ID = last_tariff.LICENSE_TYPE_ID
                                        });
                                    }
                                }
                                else
                                {
                                    DBDataContext.Db.Insert(new TBL_TARIFF()
                                    {
                                        BASED_TARIFF = DataHelper.ParseToDecimal(txtBasedTariff.Text),
                                        DATA_MONTH = dataMonth,
                                        SUBSIDY_TARIFF = DataHelper.ParseToDecimal(txtSubsidyTariff.Text),
                                        CURRENCY_ID = tariffCurrency,
                                        LICENSE_TYPE_ID = (int)cboLicenseType.SelectedValue
                                    });
                                }

                            }
                            else
                            {
                                if (dataMonth == Jan_tariff)
                                {
                                    if (last_tariff != null)
                                    {
                                        objTariff._CopyTo(_objNewTariff);
                                        _objNewTariff.BASED_TARIFF = last_tariff.BASED_TARIFF;
                                        _objNewTariff.DATA_MONTH = Jan_tariff;
                                        _objNewTariff.SUBSIDY_TARIFF = last_tariff.SUBSIDY_TARIFF;
                                        _objNewTariff.CURRENCY_ID = last_tariff.CURRENCY_ID;
                                        _objNewTariff.LICENSE_TYPE_ID = last_tariff.LICENSE_TYPE_ID;
                                        DBDataContext.Db.Update(objTariff, _objNewTariff);
                                    }
                                }
                                else if (dataMonth > Jan_tariff && dataMonth < July_tarif)
                                {
                                    objTariff._CopyTo(_objNewTariff);
                                    _objNewTariff.BASED_TARIFF = DataHelper.ParseToDecimal(txtBasedTariff.Text);
                                    _objNewTariff.DATA_MONTH = dataMonth;
                                    _objNewTariff.SUBSIDY_TARIFF = DataHelper.ParseToDecimal(txtSubsidyTariff.Text);
                                    _objNewTariff.CURRENCY_ID = tariffCurrency;
                                    _objNewTariff.LICENSE_TYPE_ID = objTariff.LICENSE_TYPE_ID;
                                    DBDataContext.Db.Update(objTariff, _objNewTariff);
                                }
                                else
                                {
                                    objTariff._CopyTo(_objNewTariff);
                                    _objNewTariff.BASED_TARIFF = DataHelper.ParseToDecimal(txtBasedTariff.Text);
                                    _objNewTariff.DATA_MONTH = dataMonth;
                                    _objNewTariff.SUBSIDY_TARIFF = DataHelper.ParseToDecimal(txtSubsidyTariff.Text);
                                    _objNewTariff.CURRENCY_ID = tariffCurrency;
                                    _objNewTariff.LICENSE_TYPE_ID = tariffLicensetypeId;
                                    DBDataContext.Db.Update(objTariff, _objNewTariff);
                                }

                            }
                            DBDataContext.Db.SubmitChanges();
                        }

                        tran.Complete();
                        this.DialogResult = DialogResult.OK;
                    }
                });
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void txtCompanyName_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void picLogo_DoubleClick(object sender, EventArgs e)
        {
            OpenFileDialog objDialog = new OpenFileDialog();
            objDialog.Filter = "(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            objDialog.ShowDialog();
            if (objDialog.FileName != "")
            {
                picLogo.ImageLocation = objDialog.FileName;
            }
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objOld);
        }

        private void txt_Enter_EnglishKey(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void InputDecimalOnly(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void TxtFacebookLink_MouseEnter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void lblLINK_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string _url = txtFacebookLink.Text == "" ? "m.facebook.com/me" : txtFacebookLink.Text;
            InputLanguage.CurrentInputLanguage = UIHelper.English;
            DialogLinkFacebook diag = new DialogLinkFacebook(_url);
            diag.ShowDialog();
            txtFacebookLink.Text = diag.text == null ? _url : diag.text;
        }
    }
}
