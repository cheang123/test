﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageTamper
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageTamper));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.btnRECEIVE_DATA = new SoftTech.Component.ExButton();
            this.txtCycle = new System.Windows.Forms.ComboBox();
            this.cboConnectionType = new System.Windows.Forms.ComboBox();
            this.btnSEND_DATA = new SoftTech.Component.ExButton();
            this.txtSearch = new SoftTech.Component.ExTextbox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvListCustomer = new System.Windows.Forms.DataGridView();
            this.lblCUSTOMER_TO_CLEAR = new System.Windows.Forms.Label();
            this.dgvClear = new System.Windows.Forms.DataGridView();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_BLUETOOTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_TAMPER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CYCLE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TAMPER_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TAMPER_STATUS_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COLLECTOR_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEVICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_TAMPER_ID_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON1 = new SoftTech.Component.DataGridViewTimeColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POLE_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_BLUETOOTH_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClear)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboCustomerGroup);
            this.panel1.Controls.Add(this.btnRECEIVE_DATA);
            this.panel1.Controls.Add(this.txtCycle);
            this.panel1.Controls.Add(this.cboConnectionType);
            this.panel1.Controls.Add(this.btnSEND_DATA);
            this.panel1.Controls.Add(this.txtSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.DropDownWidth = 300;
            this.cboCustomerGroup.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerGroup, "cboCustomerGroup");
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
            // 
            // btnRECEIVE_DATA
            // 
            resources.ApplyResources(this.btnRECEIVE_DATA, "btnRECEIVE_DATA");
            this.btnRECEIVE_DATA.Name = "btnRECEIVE_DATA";
            this.btnRECEIVE_DATA.UseVisualStyleBackColor = true;
            this.btnRECEIVE_DATA.Click += new System.EventHandler(this.btnRECEIVE_DATA_Click);
            // 
            // txtCycle
            // 
            this.txtCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtCycle.FormattingEnabled = true;
            resources.ApplyResources(this.txtCycle, "txtCycle");
            this.txtCycle.Name = "txtCycle";
            this.txtCycle.SelectedIndexChanged += new System.EventHandler(this.txtCycle_SelectedIndexChanged);
            // 
            // cboConnectionType
            // 
            this.cboConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConnectionType.DropDownWidth = 300;
            this.cboConnectionType.FormattingEnabled = true;
            resources.ApplyResources(this.cboConnectionType, "cboConnectionType");
            this.cboConnectionType.Name = "cboConnectionType";
            this.cboConnectionType.SelectedIndexChanged += new System.EventHandler(this.txtCustomerType_SelectedIndexChanged);
            // 
            // btnSEND_DATA
            // 
            resources.ApplyResources(this.btnSEND_DATA, "btnSEND_DATA");
            this.btnSEND_DATA.Name = "btnSEND_DATA";
            this.btnSEND_DATA.UseVisualStyleBackColor = true;
            this.btnSEND_DATA.Click += new System.EventHandler(this.btnSEND_DATA_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearch, "txtSearch");
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearch.QuickSearch += new System.EventHandler(this.txtSearch_QuickSearch);
            this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CUSTOMER_TYPE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "AREA";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "PHONE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.ControlLight;
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvListCustomer);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lblCUSTOMER_TO_CLEAR);
            this.splitContainer1.Panel2.Controls.Add(this.dgvClear);
            // 
            // dgvListCustomer
            // 
            this.dgvListCustomer.AllowDrop = true;
            this.dgvListCustomer.AllowUserToAddRows = false;
            this.dgvListCustomer.AllowUserToDeleteRows = false;
            this.dgvListCustomer.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvListCustomer.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvListCustomer.BackgroundColor = System.Drawing.Color.White;
            this.dgvListCustomer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvListCustomer.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvListCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListCustomer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_ID,
            this.IS_BLUETOOTH,
            this.METER_TAMPER_ID,
            this.CREATE_ON,
            this.METER_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.METER_CODE,
            this.CUSTOMER_TYPE,
            this.AREA,
            this.POLE,
            this.BOX,
            this.CYCLE_NAME,
            this.TAMPER_STATUS,
            this.TAMPER_STATUS_CODE,
            this.COLLECTOR_ID,
            this.DEVICE_ID,
            this.CREATE_BY});
            resources.ApplyResources(this.dgvListCustomer, "dgvListCustomer");
            this.dgvListCustomer.EnableHeadersVisualStyles = false;
            this.dgvListCustomer.Name = "dgvListCustomer";
            this.dgvListCustomer.ReadOnly = true;
            this.dgvListCustomer.RowHeadersVisible = false;
            this.dgvListCustomer.RowTemplate.Height = 25;
            this.dgvListCustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListCustomer.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvListCustomer_MouseDown);
            // 
            // lblCUSTOMER_TO_CLEAR
            // 
            resources.ApplyResources(this.lblCUSTOMER_TO_CLEAR, "lblCUSTOMER_TO_CLEAR");
            this.lblCUSTOMER_TO_CLEAR.BackColor = System.Drawing.Color.White;
            this.lblCUSTOMER_TO_CLEAR.ForeColor = System.Drawing.Color.Black;
            this.lblCUSTOMER_TO_CLEAR.Name = "lblCUSTOMER_TO_CLEAR";
            // 
            // dgvClear
            // 
            this.dgvClear.AllowDrop = true;
            this.dgvClear.AllowUserToAddRows = false;
            this.dgvClear.AllowUserToDeleteRows = false;
            this.dgvClear.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvClear.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvClear.BackgroundColor = System.Drawing.Color.White;
            this.dgvClear.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvClear.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvClear.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClear.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.METER_TAMPER_ID_1,
            this.CREATE_ON1,
            this.Column3,
            this.CUSTOMER_CODE_1,
            this.CUSTOMER_NAME_1,
            this.METER_CODE_1,
            this.AREA_1,
            this.POLE_1,
            this.BOX_1,
            this.IS_BLUETOOTH_1});
            resources.ApplyResources(this.dgvClear, "dgvClear");
            this.dgvClear.EnableHeadersVisualStyles = false;
            this.dgvClear.Name = "dgvClear";
            this.dgvClear.ReadOnly = true;
            this.dgvClear.RowHeadersVisible = false;
            this.dgvClear.RowTemplate.Height = 25;
            this.dgvClear.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvClear.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvClear_DragDrop);
            this.dgvClear.DragEnter += new System.Windows.Forms.DragEventHandler(this.dgvClear_DragEnter);
            this.dgvClear.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvClear_KeyDown);
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.CUSTOMER_ID, "CUSTOMER_ID");
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.ReadOnly = true;
            // 
            // IS_BLUETOOTH
            // 
            this.IS_BLUETOOTH.DataPropertyName = "IS_BLUETOOTH";
            resources.ApplyResources(this.IS_BLUETOOTH, "IS_BLUETOOTH");
            this.IS_BLUETOOTH.Name = "IS_BLUETOOTH";
            this.IS_BLUETOOTH.ReadOnly = true;
            // 
            // METER_TAMPER_ID
            // 
            this.METER_TAMPER_ID.DataPropertyName = "METER_TAMPER_ID";
            resources.ApplyResources(this.METER_TAMPER_ID, "METER_TAMPER_ID");
            this.METER_TAMPER_ID.Name = "METER_TAMPER_ID";
            this.METER_TAMPER_ID.ReadOnly = true;
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            resources.ApplyResources(this.CREATE_ON, "CREATE_ON");
            this.CREATE_ON.Name = "CREATE_ON";
            this.CREATE_ON.ReadOnly = true;
            // 
            // METER_ID
            // 
            this.METER_ID.DataPropertyName = "METER_ID";
            resources.ApplyResources(this.METER_ID, "METER_ID");
            this.METER_ID.Name = "METER_ID";
            this.METER_ID.ReadOnly = true;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "FULL_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // METER_CODE
            // 
            this.METER_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.METER_CODE.DataPropertyName = "METER_CODE";
            resources.ApplyResources(this.METER_CODE, "METER_CODE");
            this.METER_CODE.Name = "METER_CODE";
            this.METER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_TYPE
            // 
            this.CUSTOMER_TYPE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CUSTOMER_TYPE.DataPropertyName = "CUSTOMER_TYPE_NAME";
            resources.ApplyResources(this.CUSTOMER_TYPE, "CUSTOMER_TYPE");
            this.CUSTOMER_TYPE.Name = "CUSTOMER_TYPE";
            this.CUSTOMER_TYPE.ReadOnly = true;
            // 
            // AREA
            // 
            this.AREA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.AREA.DataPropertyName = "AREA_CODE";
            resources.ApplyResources(this.AREA, "AREA");
            this.AREA.Name = "AREA";
            this.AREA.ReadOnly = true;
            // 
            // POLE
            // 
            this.POLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.POLE.DataPropertyName = "POLE_CODE";
            resources.ApplyResources(this.POLE, "POLE");
            this.POLE.Name = "POLE";
            this.POLE.ReadOnly = true;
            // 
            // BOX
            // 
            this.BOX.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.BOX.DataPropertyName = "BOX_CODE";
            resources.ApplyResources(this.BOX, "BOX");
            this.BOX.Name = "BOX";
            this.BOX.ReadOnly = true;
            // 
            // CYCLE_NAME
            // 
            this.CYCLE_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CYCLE_NAME.DataPropertyName = "CYCLE_NAME";
            dataGridViewCellStyle2.Format = "dd-MMM-yyyy";
            this.CYCLE_NAME.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.CYCLE_NAME, "CYCLE_NAME");
            this.CYCLE_NAME.Name = "CYCLE_NAME";
            this.CYCLE_NAME.ReadOnly = true;
            // 
            // TAMPER_STATUS
            // 
            this.TAMPER_STATUS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TAMPER_STATUS.DataPropertyName = "TAMPER_STATUS";
            resources.ApplyResources(this.TAMPER_STATUS, "TAMPER_STATUS");
            this.TAMPER_STATUS.Name = "TAMPER_STATUS";
            this.TAMPER_STATUS.ReadOnly = true;
            // 
            // TAMPER_STATUS_CODE
            // 
            this.TAMPER_STATUS_CODE.DataPropertyName = "TAMPER_STATUS_CODE";
            resources.ApplyResources(this.TAMPER_STATUS_CODE, "TAMPER_STATUS_CODE");
            this.TAMPER_STATUS_CODE.Name = "TAMPER_STATUS_CODE";
            this.TAMPER_STATUS_CODE.ReadOnly = true;
            // 
            // COLLECTOR_ID
            // 
            this.COLLECTOR_ID.DataPropertyName = "COLLECTOR_ID";
            resources.ApplyResources(this.COLLECTOR_ID, "COLLECTOR_ID");
            this.COLLECTOR_ID.Name = "COLLECTOR_ID";
            this.COLLECTOR_ID.ReadOnly = true;
            // 
            // DEVICE_ID
            // 
            this.DEVICE_ID.DataPropertyName = "DEVICE_ID";
            resources.ApplyResources(this.DEVICE_ID, "DEVICE_ID");
            this.DEVICE_ID.Name = "DEVICE_ID";
            this.DEVICE_ID.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // METER_TAMPER_ID_1
            // 
            this.METER_TAMPER_ID_1.DataPropertyName = "ID";
            resources.ApplyResources(this.METER_TAMPER_ID_1, "METER_TAMPER_ID_1");
            this.METER_TAMPER_ID_1.Name = "METER_TAMPER_ID_1";
            this.METER_TAMPER_ID_1.ReadOnly = true;
            // 
            // CREATE_ON1
            // 
            this.CREATE_ON1.DataPropertyName = "CREATE_ON";
            resources.ApplyResources(this.CREATE_ON1, "CREATE_ON1");
            this.CREATE_ON1.Name = "CREATE_ON1";
            this.CREATE_ON1.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column3.DataPropertyName = "TAMPER_STATUS";
            resources.ApplyResources(this.Column3, "Column3");
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // CUSTOMER_CODE_1
            // 
            this.CUSTOMER_CODE_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.CUSTOMER_CODE_1.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE_1, "CUSTOMER_CODE_1");
            this.CUSTOMER_CODE_1.Name = "CUSTOMER_CODE_1";
            this.CUSTOMER_CODE_1.ReadOnly = true;
            // 
            // CUSTOMER_NAME_1
            // 
            this.CUSTOMER_NAME_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME_1.DataPropertyName = "FULL_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME_1, "CUSTOMER_NAME_1");
            this.CUSTOMER_NAME_1.Name = "CUSTOMER_NAME_1";
            this.CUSTOMER_NAME_1.ReadOnly = true;
            // 
            // METER_CODE_1
            // 
            this.METER_CODE_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.METER_CODE_1.DataPropertyName = "METER_CODE";
            resources.ApplyResources(this.METER_CODE_1, "METER_CODE_1");
            this.METER_CODE_1.Name = "METER_CODE_1";
            this.METER_CODE_1.ReadOnly = true;
            // 
            // AREA_1
            // 
            this.AREA_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.AREA_1.DataPropertyName = "AREA_CODE";
            resources.ApplyResources(this.AREA_1, "AREA_1");
            this.AREA_1.Name = "AREA_1";
            this.AREA_1.ReadOnly = true;
            // 
            // POLE_1
            // 
            this.POLE_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.POLE_1.DataPropertyName = "POLE_CODE";
            resources.ApplyResources(this.POLE_1, "POLE_1");
            this.POLE_1.Name = "POLE_1";
            this.POLE_1.ReadOnly = true;
            // 
            // BOX_1
            // 
            this.BOX_1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.BOX_1.DataPropertyName = "BOX_CODE";
            resources.ApplyResources(this.BOX_1, "BOX_1");
            this.BOX_1.Name = "BOX_1";
            this.BOX_1.ReadOnly = true;
            // 
            // IS_BLUETOOTH_1
            // 
            this.IS_BLUETOOTH_1.DataPropertyName = "IS_BLUETOOTH";
            resources.ApplyResources(this.IS_BLUETOOTH_1, "IS_BLUETOOTH_1");
            this.IS_BLUETOOTH_1.Name = "IS_BLUETOOTH_1";
            this.IS_BLUETOOTH_1.ReadOnly = true;
            // 
            // PageTamper
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Name = "PageTamper";
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClear)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtSearch;
        private ExButton btnSEND_DATA;
        private ComboBox cboConnectionType;
        private ComboBox txtCycle;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private ExButton btnRECEIVE_DATA;
        private ComboBox cboCustomerGroup;
        private SplitContainer splitContainer1;
        private DataGridView dgvListCustomer;
        private DataGridView dgvClear;
        private Label lblCUSTOMER_TO_CLEAR;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn IS_BLUETOOTH;
        private DataGridViewTextBoxColumn METER_TAMPER_ID;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn METER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn METER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_TYPE;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn POLE;
        private DataGridViewTextBoxColumn BOX;
        private DataGridViewTextBoxColumn CYCLE_NAME;
        private DataGridViewTextBoxColumn TAMPER_STATUS;
        private DataGridViewTextBoxColumn TAMPER_STATUS_CODE;
        private DataGridViewTextBoxColumn COLLECTOR_ID;
        private DataGridViewTextBoxColumn DEVICE_ID;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn METER_TAMPER_ID_1;
        private DataGridViewTimeColumn CREATE_ON1;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewTextBoxColumn CUSTOMER_CODE_1;
        private DataGridViewTextBoxColumn CUSTOMER_NAME_1;
        private DataGridViewTextBoxColumn METER_CODE_1;
        private DataGridViewTextBoxColumn AREA_1;
        private DataGridViewTextBoxColumn POLE_1;
        private DataGridViewTextBoxColumn BOX_1;
        private DataGridViewTextBoxColumn IS_BLUETOOTH_1;
    }
}
