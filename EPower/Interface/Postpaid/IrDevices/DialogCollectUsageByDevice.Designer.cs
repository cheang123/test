﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCollectUsageByDevice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCollectUsageByDevice));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnRECEIVE = new SoftTech.Component.ExButton();
            this.lblTOTAL_CUSTOMER = new System.Windows.Forms.Label();
            this.lblCUSTOMER_USAGE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NO_USAGE = new System.Windows.Forms.Label();
            this.txtAllCustomer = new System.Windows.Forms.TextBox();
            this.txtHaveUsageCustomer = new System.Windows.Forms.TextBox();
            this.txtNoUsageCustomer = new System.Windows.Forms.TextBox();
            this.btnCHECK_2 = new SoftTech.Component.ExButton();
            this.lblDEVICE_NAME = new System.Windows.Forms.Label();
            this.lblMONTH = new System.Windows.Forms.Label();
            this.lblDEVICE_CODE = new System.Windows.Forms.Label();
            this.txtMonth = new System.Windows.Forms.TextBox();
            this.txtDeviceCode = new System.Windows.Forms.TextBox();
            this.txtDeviceName = new System.Windows.Forms.TextBox();
            this.lblMETER_UNKNOWN = new System.Windows.Forms.Label();
            this.txtUnknownMeter = new System.Windows.Forms.TextBox();
            this.btnCHECK_1 = new SoftTech.Component.ExButton();
            this.lblTOTAL_METER_NEW_CYCLE = new System.Windows.Forms.Label();
            this.txtNewCycle = new System.Windows.Forms.TextBox();
            this.btnCHECK_3 = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnCHECK_3);
            this.content.Controls.Add(this.txtNewCycle);
            this.content.Controls.Add(this.lblTOTAL_METER_NEW_CYCLE);
            this.content.Controls.Add(this.btnCHECK_1);
            this.content.Controls.Add(this.txtUnknownMeter);
            this.content.Controls.Add(this.lblMETER_UNKNOWN);
            this.content.Controls.Add(this.btnRECEIVE);
            this.content.Controls.Add(this.txtDeviceName);
            this.content.Controls.Add(this.txtDeviceCode);
            this.content.Controls.Add(this.txtMonth);
            this.content.Controls.Add(this.lblDEVICE_CODE);
            this.content.Controls.Add(this.lblMONTH);
            this.content.Controls.Add(this.lblDEVICE_NAME);
            this.content.Controls.Add(this.btnCHECK_2);
            this.content.Controls.Add(this.txtNoUsageCustomer);
            this.content.Controls.Add(this.txtHaveUsageCustomer);
            this.content.Controls.Add(this.txtAllCustomer);
            this.content.Controls.Add(this.lblCUSTOMER_NO_USAGE);
            this.content.Controls.Add(this.lblCUSTOMER_USAGE);
            this.content.Controls.Add(this.lblTOTAL_CUSTOMER);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_CUSTOMER, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_USAGE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NO_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtAllCustomer, 0);
            this.content.Controls.SetChildIndex(this.txtHaveUsageCustomer, 0);
            this.content.Controls.SetChildIndex(this.txtNoUsageCustomer, 0);
            this.content.Controls.SetChildIndex(this.btnCHECK_2, 0);
            this.content.Controls.SetChildIndex(this.lblDEVICE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblMONTH, 0);
            this.content.Controls.SetChildIndex(this.lblDEVICE_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtMonth, 0);
            this.content.Controls.SetChildIndex(this.txtDeviceCode, 0);
            this.content.Controls.SetChildIndex(this.txtDeviceName, 0);
            this.content.Controls.SetChildIndex(this.btnRECEIVE, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_UNKNOWN, 0);
            this.content.Controls.SetChildIndex(this.txtUnknownMeter, 0);
            this.content.Controls.SetChildIndex(this.btnCHECK_1, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_METER_NEW_CYCLE, 0);
            this.content.Controls.SetChildIndex(this.txtNewCycle, 0);
            this.content.Controls.SetChildIndex(this.btnCHECK_3, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRECEIVE
            // 
            resources.ApplyResources(this.btnRECEIVE, "btnRECEIVE");
            this.btnRECEIVE.Name = "btnRECEIVE";
            this.btnRECEIVE.UseVisualStyleBackColor = true;
            this.btnRECEIVE.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lblTOTAL_CUSTOMER
            // 
            resources.ApplyResources(this.lblTOTAL_CUSTOMER, "lblTOTAL_CUSTOMER");
            this.lblTOTAL_CUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.lblTOTAL_CUSTOMER.Name = "lblTOTAL_CUSTOMER";
            // 
            // lblCUSTOMER_USAGE
            // 
            resources.ApplyResources(this.lblCUSTOMER_USAGE, "lblCUSTOMER_USAGE");
            this.lblCUSTOMER_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.lblCUSTOMER_USAGE.Name = "lblCUSTOMER_USAGE";
            // 
            // lblCUSTOMER_NO_USAGE
            // 
            resources.ApplyResources(this.lblCUSTOMER_NO_USAGE, "lblCUSTOMER_NO_USAGE");
            this.lblCUSTOMER_NO_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.lblCUSTOMER_NO_USAGE.Name = "lblCUSTOMER_NO_USAGE";
            // 
            // txtAllCustomer
            // 
            resources.ApplyResources(this.txtAllCustomer, "txtAllCustomer");
            this.txtAllCustomer.Name = "txtAllCustomer";
            this.txtAllCustomer.ReadOnly = true;
            // 
            // txtHaveUsageCustomer
            // 
            resources.ApplyResources(this.txtHaveUsageCustomer, "txtHaveUsageCustomer");
            this.txtHaveUsageCustomer.Name = "txtHaveUsageCustomer";
            this.txtHaveUsageCustomer.ReadOnly = true;
            // 
            // txtNoUsageCustomer
            // 
            resources.ApplyResources(this.txtNoUsageCustomer, "txtNoUsageCustomer");
            this.txtNoUsageCustomer.Name = "txtNoUsageCustomer";
            this.txtNoUsageCustomer.ReadOnly = true;
            // 
            // btnCHECK_2
            // 
            resources.ApplyResources(this.btnCHECK_2, "btnCHECK_2");
            this.btnCHECK_2.Name = "btnCHECK_2";
            this.btnCHECK_2.UseVisualStyleBackColor = true;
            this.btnCHECK_2.Click += new System.EventHandler(this.btnCheckNoUsage_Click);
            // 
            // lblDEVICE_NAME
            // 
            resources.ApplyResources(this.lblDEVICE_NAME, "lblDEVICE_NAME");
            this.lblDEVICE_NAME.Name = "lblDEVICE_NAME";
            // 
            // lblMONTH
            // 
            resources.ApplyResources(this.lblMONTH, "lblMONTH");
            this.lblMONTH.BackColor = System.Drawing.Color.Transparent;
            this.lblMONTH.Name = "lblMONTH";
            // 
            // lblDEVICE_CODE
            // 
            resources.ApplyResources(this.lblDEVICE_CODE, "lblDEVICE_CODE");
            this.lblDEVICE_CODE.Name = "lblDEVICE_CODE";
            // 
            // txtMonth
            // 
            resources.ApplyResources(this.txtMonth, "txtMonth");
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.ReadOnly = true;
            // 
            // txtDeviceCode
            // 
            resources.ApplyResources(this.txtDeviceCode, "txtDeviceCode");
            this.txtDeviceCode.Name = "txtDeviceCode";
            this.txtDeviceCode.ReadOnly = true;
            // 
            // txtDeviceName
            // 
            resources.ApplyResources(this.txtDeviceName, "txtDeviceName");
            this.txtDeviceName.Name = "txtDeviceName";
            this.txtDeviceName.ReadOnly = true;
            // 
            // lblMETER_UNKNOWN
            // 
            resources.ApplyResources(this.lblMETER_UNKNOWN, "lblMETER_UNKNOWN");
            this.lblMETER_UNKNOWN.BackColor = System.Drawing.Color.Transparent;
            this.lblMETER_UNKNOWN.Name = "lblMETER_UNKNOWN";
            // 
            // txtUnknownMeter
            // 
            resources.ApplyResources(this.txtUnknownMeter, "txtUnknownMeter");
            this.txtUnknownMeter.Name = "txtUnknownMeter";
            this.txtUnknownMeter.ReadOnly = true;
            // 
            // btnCHECK_1
            // 
            resources.ApplyResources(this.btnCHECK_1, "btnCHECK_1");
            this.btnCHECK_1.Name = "btnCHECK_1";
            this.btnCHECK_1.UseVisualStyleBackColor = true;
            this.btnCHECK_1.Click += new System.EventHandler(this.btnUnknowMeter_Click);
            // 
            // lblTOTAL_METER_NEW_CYCLE
            // 
            resources.ApplyResources(this.lblTOTAL_METER_NEW_CYCLE, "lblTOTAL_METER_NEW_CYCLE");
            this.lblTOTAL_METER_NEW_CYCLE.Name = "lblTOTAL_METER_NEW_CYCLE";
            // 
            // txtNewCycle
            // 
            resources.ApplyResources(this.txtNewCycle, "txtNewCycle");
            this.txtNewCycle.Name = "txtNewCycle";
            this.txtNewCycle.ReadOnly = true;
            // 
            // btnCHECK_3
            // 
            resources.ApplyResources(this.btnCHECK_3, "btnCHECK_3");
            this.btnCHECK_3.Name = "btnCHECK_3";
            this.btnCHECK_3.UseVisualStyleBackColor = true;
            this.btnCHECK_3.Click += new System.EventHandler(this.btnCheckNewCycle_Click);
            // 
            // DialogCollectUsageByDevice
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCollectUsageByDevice";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCHECK_3;
        private TextBox txtNewCycle;
        private Label lblTOTAL_METER_NEW_CYCLE;
        private ExButton btnCHECK_1;
        private TextBox txtUnknownMeter;
        private Label lblMETER_UNKNOWN;
        private ExButton btnRECEIVE;
        private TextBox txtDeviceName;
        private TextBox txtDeviceCode;
        private TextBox txtMonth;
        private Label lblDEVICE_CODE;
        private Label lblMONTH;
        private Label lblDEVICE_NAME;
        private ExButton btnCHECK_2;
        private TextBox txtNoUsageCustomer;
        private TextBox txtHaveUsageCustomer;
        private TextBox txtAllCustomer;
        private Label lblCUSTOMER_NO_USAGE;
        private Label lblCUSTOMER_USAGE;
        private Label lblTOTAL_CUSTOMER;
        private ExButton btnClose;
        private Panel panel1;
    }
}