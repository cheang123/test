﻿using EPower.Base.Logic;
using EPower.HB02.Model;
using EPower.HB02.REST;
using EPower.Logic.IRDevice;
using EPower.Properties;
using Newtonsoft.Json;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogImportDataToIRReader : ExDialog
    {
        TransmissionOption _tranType;

        public DialogImportDataToIRReader(TransmissionOption tranType = TransmissionOption.WIRE)
        {
            InitializeComponent();
            _tranType = tranType;
            txtArea.Text = Resources.ALL_AREA;
            txtCycle.Text = Resources.ALL_CYCLE;
            bool isMultipleDatabase = DataHelper.ParseToBoolean(Method.GetUtilityValue(Utility.IR_MULTIPLE_DATABASE));

            if (isMultipleDatabase)
            {
                chkNEW_DB_IN_IR.Visible = true;
                chkNEW_DB_IN_IR.Checked = false;
            }
            else
            {
                chkNEW_DB_IN_IR.Visible = false;
                chkNEW_DB_IN_IR.Checked = true;
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (MsgBox.ShowQuestion(Resources.MSG_CONFIRM_BEFORE_SEND_DATA_TO_DEVICE, Resources.MSG_VERIFY_BEFORE_CONFIRM) != DialogResult.Yes)
            {
                return;
            }
            if (_tranType == TransmissionOption.WIRE && txtSD_PATH.Text == "")
            {
                wire();
            }
            else if (_tranType == TransmissionOption.WIRE && txtSD_PATH.Text != "")
            {
                SentToSDCard();
            }
            else
            {
                internet();
            }



        }
        private void internet()
        {
            var isCompleted = false;
            var totalCus = 0;
            var months = "";
            var area = txtArea.Text;
            var cycle = txtCycle.Text;
            var deviceCode = "Online";
            var deviceName = deviceCode;

            Runner.RunNewThread(() =>
            {
                bool isOverride = true;
                string licenseCode = DBDataContext.Db.TBL_CONFIGs.FirstOrDefault().AREA_CODE;
                var ir = new IRDevice();
                var data = ir.GetMonthlyUsages(mCycle, mArea);

                //var rawData = rows.Select((item, index) => new { Item = item, Index = index });

                //10000, 5000

                var totalRecord = data.Count;
                var maxRecordInList = 5000m;

                var parts = Math.Ceiling(totalRecord / maxRecordInList);

                var splitedData = data.Select((item, index) => new { Item = item, Index = index })
                 .GroupBy(pair => pair.Index % parts, element => element.Item)
                 .Select(group => group.AsEnumerable());

                var customerId = 1;
                var tamperMeterId = 1;
                var usageId = 1;
                var usageHistoryId = 1;
                foreach (var rows in splitedData)
                {

                    var customers = (from u in rows
                                     select new IrMobile.Models.TBL_CUSTOMER
                                     {
                                         ID = customerId++,
                                         AREA_CODE = u.AREA_CODE,
                                         POLE_CODE = u.POLE_CODE,
                                         BOX_CODE = u.BOX_CODE,
                                         CUSTOMER_CODE = u.CUSTOMER_CODE,
                                         FULL_NAME = u.FULL_NAME,
                                         LAST_TOTAL_USAGE = u.LAST_TOTAL_USAGE,
                                         LICENSE_CODE = licenseCode,
                                         METER_CODE = u.METER_CODE,
                                         START_USAGE = u.START_USAGE,
                                         MULTIPLIER = u.MULTIPLIER,
                                         PHASE_ID = u.PHASE_ID
                                     })._ToDataTable();

                    var tamperMeter = (from u in rows
                                       select new TMP_IR_TAMPER_METER
                                       {
                                           ID = tamperMeterId++,
                                           CREATE_ON = new DateTime(1900, 1, 1),
                                           TAMPER_STATUS = ""
                                       })._ToDataTable();

                    var usages = (from u in rows
                                  select new Android.TBL_USAGE
                                  {
                                      ID = usageId++,
                                      DATE_COLLECT = new DateTime(1900, 1, 1),
                                      END_USAGE = u.END_USAGE,
                                      IS_COLLECT_USAGE = false,
                                      IS_DIGITAL = u.IS_DIGITAL,
                                      IS_METER_REGISTER = u.IS_METER_REGISTER,
                                      IS_METER_RENEW_CYCLE = u.IS_METER_RENEW_CYCLE,
                                      IS_READ_IR = u.IS_READ_IR,
                                      IS_UPDATE = false,
                                      IS_BLUETOOTH = u.IS_BLUETOOTH
                                  })._ToDataTable();

                    var histories = (from u in rows
                                     select new IrMobile.Models.TBL_USAGE_HISTORY
                                     {
                                         ID = usageHistoryId++,
                                         USAGE_HISTORY = u.USAGE_HISTORY
                                     })._ToDataTable();
                    customers.TableName = "customer";
                    usages.TableName = "usage";
                    histories.TableName = "usageHistory";
                    tamperMeter.TableName = "tamperMeter";



                    var ds = new DataSet();
                    ds.Tables.Add(customers);
                    ds.Tables.Add(tamperMeter);
                    ds.Tables.Add(usages);
                    ds.Tables.Add(histories);

                    var uploadBody = new UploadBody()
                    {
                        destination_id = "*",
                        override_last = isOverride,
                        data = ds
                    };

                    Runner.Instance.Text = Resources.PROCESSING;
                    HB02_API.Instance.Upload(uploadBody);
                    //isCompleted = true;

                    isOverride = false;
                }
                isCompleted = true;
                totalCus = data.Count();
                months = string.Join(",", mCycle.Select(x => Method.GetNextBillingMonth(x.CYCLE_ID).Month.ToString("00")).ToArray());
                /*
                 * Log upload usage transaction 
                 */
                var usageDeviceCollection = ir.UsageDeviceCollection;
                usageDeviceCollection.AREA = area;
                usageDeviceCollection.CYCLE = cycle;
                usageDeviceCollection.MONTH = months;
                usageDeviceCollection.DEVICE_CODE = deviceCode;
                usageDeviceCollection.DEVICE_NAME = deviceName;
                usageDeviceCollection.RECORD = totalCus;
                usageDeviceCollection.TRANSMISSION_TYPE_ID = (int)TransmissionOption.Internet;
                usageDeviceCollection.DATA_BACKUP = Encoding.Unicode.GetBytes(JsonConvert.SerializeObject(new List<TMP_IR_USAGE>(), new JsonSerializerSettings()
                {
                    DateFormatString = "dd-MM-yyyy HH:mm:ss",
                    Formatting = Newtonsoft.Json.Formatting.Indented
                }));
                ir.LogUploadUsage(usageDeviceCollection);
            });
            if (isCompleted)
            {
                MsgBox.ShowInformation(Resources.SUCCESS);
                txtDeviceCode.Text = deviceCode;
                txtDeviceName.Text = deviceName;
                txtTotalCustomer.Text = totalCus.ToString();
                txtMonth.Text = months;
            }
        }

        private void wire()
        {
            txtDeviceCode.Text = txtDeviceName.Text = txtMonth.Text = txtTotalCustomer.Text = string.Empty;
            var area = txtArea.Text;
            var cycle = txtCycle.Text;
            var deviceCode = string.Empty;
            var deviceName = string.Empty;
            var months = "";
            int intRecord = -1;

            bool result = false;
            IRDevice objd = null;

            if (chkNEW_DB_IN_IR.Checked && chkNEW_DB_IN_IR.Visible)
            {
                DialogResult dig = MsgBox.ShowQuestion(Resources.MS_REPLACE_NEW_DATA_TO_DEVICE, this.Text);
                if (dig == DialogResult.No || dig == DialogResult.Cancel)
                {
                    return;
                }
            }

            Runner.RunNewThread(() =>
            {
                var dataBackup = new List<TMP_IR_USAGE>();

                // connect to device 
                objd = IRDevice.Connect();
                deviceCode = objd.ProductID;
                deviceName = objd.DeviceOEM;

                // validate device registration & employee
                IRDevice.Validate(objd);

                // backup file before send
                Runner.Instance.Text = string.Format(Resources.MS_BACKUP_DATA_IN_DEVICE, string.Empty, string.Empty);
                dataBackup = objd.GetFile(IRDevice.BackupType.BeforeSFTD);

                // check update version on device
                Runner.Instance.Text = Resources.MS_CHECKING_VERSION;
                objd.CheckVersion();

                // get usage and other info to send to device
                Runner.Instance.Text = string.Format(Resources.MS_PREPARING_DATABASE, string.Empty, string.Empty);
                intRecord = objd.GetUsage(mCycle, mArea, ref months, chkNEW_DB_IN_IR.Checked);

                // no record found!
                if (intRecord == 0)
                {
                    throw new Exception(Resources.MS_DEVICE_NO_CUSTOMER_COLLECT_USAGE);
                }

                // send file to device.
                Runner.Instance.Text = string.Format(Resources.MS_SEND_FILE_TO_DEVICE, string.Empty, string.Empty);
                objd.SendFile();

                /*
                 * Log upload usage transaction 
                 */
                var json = JsonConvert.SerializeObject(dataBackup, new JsonSerializerSettings()
                {
                    DateFormatString = "dd-MM-yyyy HH:mm:ss",
                    Formatting = Newtonsoft.Json.Formatting.Indented
                });
                var usageDeviceCollection = objd.UsageDeviceCollection;
                usageDeviceCollection.AREA = area;
                usageDeviceCollection.CYCLE = cycle;
                usageDeviceCollection.MONTH = months;
                usageDeviceCollection.DEVICE_CODE = deviceCode;
                usageDeviceCollection.DEVICE_NAME = deviceName;
                usageDeviceCollection.RECORD = intRecord;
                usageDeviceCollection.TRANSMISSION_TYPE_ID = (int)TransmissionOption.WIRE;
                usageDeviceCollection.DATA_BACKUP = Encoding.Unicode.GetBytes(json);
                objd.LogUploadUsage(usageDeviceCollection);
                result = true;
            });


            try
            {
                this.txtDeviceCode.Text = deviceCode;
                this.txtMonth.Text = months;
                this.txtTotalCustomer.Text = intRecord == -1 ? "" : intRecord.ToString();
                this.txtDeviceName.Text = deviceName;


                if (result)
                {
                    objd.Complete();
                    MsgBox.ShowInformation(Resources.UPLOAD_DATA_SUCCESS);
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        List<TBL_BILLING_CYCLE> mCycle = DBDataContext.Db.TBL_BILLING_CYCLEs.Where(x => x.IS_ACTIVE).ToList();
        List<TBL_AREA> mArea = DBDataContext.Db.TBL_AREAs.Where(x => x.IS_ACTIVE).ToList();

        private void btnSelectCycle_Click(object sender, EventArgs e)
        {
            DialogSelectCycleUsage dig = new DialogSelectCycleUsage(mCycle);
            dig.ShowDialog();
            mCycle = dig.SelectCycles;
            if (mCycle.Count == DBDataContext.Db.TBL_BILLING_CYCLEs.Where(x => x.IS_ACTIVE).Count())
            {
                txtCycle.Text = Resources.ALL_CYCLE;
            }
            else
            {
                txtCycle.Text = "";
                foreach (var item in mCycle)
                {
                    txtCycle.Text += string.IsNullOrEmpty(txtCycle.Text) ? "" : ", ";
                    txtCycle.Text += item.CYCLE_NAME;
                }
            }

        }

        private void btnSelectArea_Click(object sender, EventArgs e)
        {
            DialogSelectAreaUsage dig = new DialogSelectAreaUsage(mArea);
            dig.ShowDialog();
            mArea = dig.SelectedAreas;
            if (mArea.Count == DBDataContext.Db.TBL_AREAs.Where(x => x.IS_ACTIVE).Count())
            {
                txtArea.Text = Resources.ALL_AREA;
            }
            else
            {
                txtArea.Text = "";
                foreach (var item in mArea)
                {
                    txtArea.Text += string.IsNullOrEmpty(txtArea.Text) ? "" : ", ";
                    txtArea.Text += item.AREA_CODE;
                }
            }
        }

        private void SentToSDCard()
        {
            try
            {
                if (!Directory.Exists(txtSD_PATH.Text))
                {
                    throw new Exception(Resources.MS_CAN_NOT_FIND_SD_PATH);
                }
                var Device = DBDataContext.Db.TBL_DEVICEs.Where(x => x.DRIVER == @"EPower.Logic.IRDevice.WinCE");
                if (Device.Count() == 0)
                {
                    throw new Exception(Resources.MS_CAN_NOT_FIND_DEVICE_REGISTERED);
                }
                string DeviceCode = "";
                string DeviceName = "";
                foreach (var item in Device)
                {
                    DeviceCode += (DeviceCode == "" ? "" : "; ") + item.DEVICE_CODE;
                    DeviceName += (DeviceName == "" ? "" : "; ") + item.DEVICE_OEM_INFO;
                }
                var months = "";
                int intRecord = -1;
                bool result = false;
                Runner.RunNewThread(() =>
                {
                    Runner.Instance.Text = Resources.MS_PREPARING_DATA;
                    txtDeviceCode.Text = txtDeviceName.Text = txtMonth.Text = txtTotalCustomer.Text = string.Empty;
                    var area = txtArea.Text;
                    var cycle = txtCycle.Text;
                    WinCE Win = new WinCE();
                    Runner.Instance.Text = string.Format(Resources.MS_BACKUP_DATA_IN_DEVICE, string.Empty, string.Empty);
                    var DataBackUp = Win.BackUp(txtSD_PATH.Text + @"\EPower.IrMobile\MobileDB.sdf", IRDevice.BackupType.BeforeSFTD);
                    intRecord = Win.GetUsage(mCycle, mArea, ref months, chkNEW_DB_IN_IR.Checked);

                    Runner.Instance.Text = Resources.MS_UNLOADING;
                    CopyFile();

                    var json = JsonConvert.SerializeObject(DataBackUp, new JsonSerializerSettings()
                    {
                        DateFormatString = "dd-MM-yyyy HH:mm:ss",
                        Formatting = Newtonsoft.Json.Formatting.Indented
                    });
                    var usageDeviceCollection = Win.UsageDeviceCollection;
                    usageDeviceCollection.AREA = area;
                    usageDeviceCollection.CYCLE = cycle;
                    usageDeviceCollection.MONTH = months;
                    usageDeviceCollection.DEVICE_CODE = DeviceCode;
                    usageDeviceCollection.DEVICE_NAME = DeviceName;
                    usageDeviceCollection.RECORD = intRecord;
                    usageDeviceCollection.TRANSMISSION_TYPE_ID = (int)TransmissionOption.WIRE;
                    usageDeviceCollection.DATA_BACKUP = Encoding.Unicode.GetBytes(json);
                    Win.LogUploadUsage(usageDeviceCollection);
                    result = true;
                    Win.Complete();

                });

                this.txtDeviceCode.Text = DeviceCode;
                this.txtMonth.Text = months;
                this.txtTotalCustomer.Text = intRecord == -1 ? "" : intRecord.ToString();
                this.txtDeviceName.Text = DeviceName;

                if (result)
                {
                    MsgBox.ShowInformation(Resources.UPLOAD_DATA_SUCCESS);
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void CopyFile()
        {
            string sourcePath = Application.StartupPath + @"\Template\GF1100";
            string targetPath = txtSD_PATH.Text + @"\EPower.IrMobile";
            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }
            foreach (var srcPath in Directory.GetFiles(sourcePath))
            {
                File.Copy(srcPath, srcPath.Replace(sourcePath, targetPath), true);
            }
            File.Copy(Application.StartupPath + @"\MobileDB\MobileDB.sdf", targetPath + @"\MobileDB.sdf", true);
            //File Resource
            string PathResource = Application.StartupPath + @"\Template\GF1100\en-GB";
            string targetResource = txtSD_PATH.Text + @"\EPower.IrMobile\en-GB";
            if (!Directory.Exists(targetResource))
            {
                Directory.CreateDirectory(targetResource);
            }
            foreach (var srcPath in Directory.GetFiles(PathResource))
            {
                File.Copy(srcPath, srcPath.Replace(PathResource, targetResource), true);
            }
        }

        private void btnBROWS_SD_PATH_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog frm = new FolderBrowserDialog();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                txtSD_PATH.Text = frm.SelectedPath;
            }
        }

        private void picHelp_MouseEnter(object sender, EventArgs e)
        {
            ToolTip tp = new ToolTip
            {
                AutoPopDelay = 15000,
                ShowAlways = true,
                OwnerDraw = true,
            };
            tp.SetToolTip(picHelp, Resources.MS_SD_PATH_HELPER);
            tp.Draw += new DrawToolTipEventHandler(toolTip1_Draw);
            tp.Popup += new PopupEventHandler(toolTip1_Popup);
        }

        void toolTip1_Popup(object sender, PopupEventArgs e)
        {
            e.ToolTipSize = TextRenderer.MeasureText(Resources.MS_SD_PATH_HELPER, new Font("Khmer OS System", 8.5f));
        }

        void toolTip1_Draw(object sender, DrawToolTipEventArgs e)
        {
            using (e.Graphics)
            {
                Font f = new Font("Khmer OS System", 8.5f);
                e.DrawBackground();
                e.DrawBorder();
                e.Graphics.DrawString(e.ToolTipText, f, Brushes.Black, new PointF(2, 2));

            }
        }
    }
}