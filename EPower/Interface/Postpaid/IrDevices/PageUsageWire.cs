﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Helper;
using SoftTech.Component;
using EPower.Properties;
using EPower.HB02.REST;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace EPower.Interface
{
    public partial class PageUsageWire : Form
    {
        Dictionary<int,string> transferType = new Dictionary<int, string>();
        Dictionary<int, string> transmissionType = new Dictionary<int, string>();

        public PageUsageWire()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            CREATE_ON.DefaultCellStyle.Format = UIHelper._DefaultShortDatetime;
            dtp1.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dtp2.Value = dtp1.Value.AddMonths(1).AddDays(-1);
            transferType[(int)TransferType.UPLOAD_USAGE] = Resources.SEND_DATA;
            transferType[(int)TransferType.DOWNLOD_USAGE] = Resources.RECEIVE_DATA;
            transmissionType[(int)TransmissionOption.WIRE] = Resources.WIRE;
            transmissionType[(int)TransmissionOption.Internet] = Resources.INTERNET;
            transmissionType[(int)TransmissionOption.GPRS] = Resources.GPRS;
            transmissionType[(int)TransmissionOption.SD] = Resources.RECIEVE_SD;
            UIHelper.SetDataSourceToComboBox(cboGroup,transferType.Select(x=>new { x.Key, x.Value }), "");
            bind();

            cboGroup.SelectedIndexChanged += txtQuickSearch_QuickSearch;
            dtp1.ValueChanged += txtQuickSearch_QuickSearch;
            dtp2.ValueChanged += txtQuickSearch_QuickSearch;
        }

        void bind()
        {
            var transfer = 0;
            if (cboGroup.SelectedIndex!=-1)
            {
                transfer = (int)cboGroup.SelectedValue;
            }

            var deviceCollections = from dc in DBDataContext.Db.TBL_USAGE_DEVICE_COLLECTIONs
                                    where (dc.TRANSFER_TYPE_ID == transfer || transfer == 0)
                                    && dc.CREATE_ON >= dtp1.Value
                                    && dc.CREATE_ON <= dtp2.Value
                                    && (dc.TRANSMISSION_TYPE_ID == (int)TransmissionOption.WIRE
                                       || dc.TRANSMISSION_TYPE_ID == (int)TransmissionOption.SD)
                                    select new
                                    {
                                        dc.USAGE_DEVICE_COLLECTION_ID,
                                        TRANSFER_TYPE = transferType[dc.TRANSFER_TYPE_ID],
                                        TRANSMISSION_TYPE = transmissionType[ dc.TRANSMISSION_TYPE_ID],
                                        dc.CREATE_BY,
                                        dc.CREATE_ON,
                                        dc.DEVICE_CODE,
                                        dc.DEVICE_NAME,
                                        CYCLE_NAME= dc.CYCLE,
                                        AREA_NAME = dc.AREA,
                                        dc.MONTH,
                                        dc.RECORD,
                                        DATA_BACKUP = Resources.DATA_BACKUP
                                        
                                    };
            dgv.DataSource = deviceCollections;
        }

        private void btnUploadUsage_Click(object sender, EventArgs e)
        {
            var dig = new DialogImportDataToIRReader(TransmissionOption.WIRE);
            dig.ShowDialog();
            bind();
        }

        private void btnDownloadUsage_Click(object sender, EventArgs e)
        {
            var dig = new DialogCollectUsageByIRReader(TransmissionOption.WIRE);
            dig.ShowDialog();
            bind();
        }

        private void txtQuickSearch_QuickSearch(object sender, EventArgs e)
        {
            bind();
        } 

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv.SelectedRows.Count ==0)
            {
                return;
            }
            if (e.ColumnIndex == dgv.Columns[RECORD.Name].Index)
            {
                var deviceUsage = DBDataContext.Db.TBL_USAGE_DEVICE_COLLECTIONs.FirstOrDefault(x => x.USAGE_DEVICE_COLLECTION_ID == (int)dgv.SelectedRows[0].Cells[USAGE_DEVICE_COLLECTION_ID.Name].Value);
                var dig = new DialogUsageDeviceCollection(deviceUsage);
                dig.ShowDialog();
            }else if(e.ColumnIndex == dgv.Columns[DATA_BACKUP.Name].Index)
            {
                var deviceUsage = DBDataContext.Db.TBL_USAGE_DEVICE_COLLECTIONs.FirstOrDefault(x => x.USAGE_DEVICE_COLLECTION_ID == (int)dgv.SelectedRows[0].Cells[USAGE_DEVICE_COLLECTION_ID.Name].Value);
                var usages = JsonConvert.DeserializeObject<List<TMP_IR_USAGE>>
                (Encoding.Unicode.GetString(deviceUsage.DATA_BACKUP.ToArray()), new JsonSerializerSettings()
                {
                    DateFormatString = "dd-MM-yyyy HH:mm:ss",
                    Formatting = Newtonsoft.Json.Formatting.Indented
                });                 
                var dig = new DialogUsageDeviceCollectionBackup(usages);
                dig.ShowDialog();
            }

            
        }
    }
}
