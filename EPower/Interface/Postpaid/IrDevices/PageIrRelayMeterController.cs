﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EPower.Interface.Postpaid.IrDevices;
using EPower.Properties;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PageIrRelayMeterController : Form
    {

        enum RelayUI
        {
            History = 1,
            OrderCloseOpenMeter = 2,
        }

        Dictionary<RelayUI, string> databind = new Dictionary<RelayUI, string>() { { RelayUI.History, Resources.TRANSACTION_HISTORY }, { RelayUI.OrderCloseOpenMeter, Resources.ORDER_BLOCK_AND_UNBLOCK } };

        public PageIrRelayMeterController()
        {
            InitializeComponent();
            cboGroup.DisplayMember = "Value";
            cboGroup.ValueMember = "Key";
            cboGroup.DataSource = databind.ToList();
        }

        private void cboGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedItem = cboGroup.SelectedValue;
            switch ((RelayUI)selectedItem)
            {
                case RelayUI.History:
                    this.showQueryOption(QueryOrderRelayMeterLog.Instant);
                    this.showContent(OrderRelayMeterLog.Instant);
                    QueryOrderRelayMeterLog.Instant.search();
                    this.txtQuickSearch.Enabled = false;
                    this.txtQuickSearch.BackColor = Color.LightGray;
                    break;
                case RelayUI.OrderCloseOpenMeter:
                    this.showQueryOption(QueryOrderRelayMeter.Instant);
                    this.showContent(OrderRelayMeter.Instant);
                    this.txtQuickSearch.Enabled = true;
                    this.txtQuickSearch.BackColor = Color.White;
                    break;
                default:
                    break;
            }
        }

        void showContent(UserControl ui)
        {
            if (!content.Controls.Contains(ui))
            {
                content.Controls.Add(ui);
                ResourceHelper.ApplyResource(ui);
            }
            content.Controls.SetChildIndex(ui, 0);
            ui.Show();
        }

        void showQueryOption(UserControl ui)
        {
            if (!panelQueryOption.Controls.Contains(ui))
            {
                panelQueryOption.Controls.Add(ui);
                ResourceHelper.ApplyResource(ui);
            }
            panelQueryOption.Controls.SetChildIndex(ui, 0);
            ui.Show();
        }

        private void txtQuickSearch_QuickSearch(object sender, EventArgs e)
        {
            if ((RelayUI)cboGroup.SelectedValue == RelayUI.OrderCloseOpenMeter)
            {
                QueryOrderRelayMeter.Instant.search(txtQuickSearch.Text);
            }
        }

        private void txtQuickSearch_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
    }
}
