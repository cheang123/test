﻿using EPower.Base.Logic;
using EPower.Logic.IRDevice;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.Postpaid.IrDevices
{
    public partial class OrderRelayMeter : UserControl
    {
        private static OrderRelayMeter self = new OrderRelayMeter();
        public static OrderRelayMeter Instant { get { return self; } }

        private List<TBL_RELAY_METER> rows = new List<TBL_RELAY_METER>();

        private OrderRelayMeter()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            UIHelper.DataGridViewProperties(this.dgvListCustomer, this.dgvClose, this.dgvOpen);
            this.dgvListCustomer.MultiSelect
                = this.dgvClose.MultiSelect
                = this.dgvOpen.MultiSelect
                = true;
            this.dgvClose.DefaultCellStyle.ForeColor = System.Drawing.Color.Red;
            this.dgvOpen.DefaultCellStyle.ForeColor = System.Drawing.Color.Blue;
        }

        private void dgvListCustomer_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                List<DataGridViewRow> rows = new List<DataGridViewRow>();
                foreach (var item in dgvListCustomer.SelectedRows)
                {
                    rows.Add((DataGridViewRow)item);
                }
                dgvListCustomer.DoDragDrop(rows, DragDropEffects.Copy);
            }
        }

        private void dgvClose_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }
        private void dgvClose_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(List<DataGridViewRow>)))
            {
                var data = e.Data.GetData(typeof(List<DataGridViewRow>));

                foreach (var item in (List<DataGridViewRow>)data)
                {
                    if (rows.Select(x => x.CUSTOMER_ID).Contains((int)item.Cells[CUSTOMER_ID.Name].Value))
                    {
                        continue;
                    }
                    rows.Add(new TBL_RELAY_METER()
                    {
                        RELAY_METER_ID = Guid.NewGuid(),
                        CUSTOMER_ID = (int)item.Cells[CUSTOMER_ID.Name].Value,
                        METER_ID = (int)item.Cells[METER_ID.Name].Value,
                        CUSTOMER_CODE = item.Cells[CUSTOMER_CODE.Name].Value.ToString(),
                        METER_CODE = item.Cells[METER_CODE.Name].Value.ToString(),
                        FULL_NAME = item.Cells[CUSTOMER_NAME.Name].Value.ToString(),
                        BOX_CODE = item.Cells[BOX.Name].Value.ToString(),
                        AREA_CODE = item.Cells[AREA.Name].Value.ToString(),
                        POLE_CODE = item.Cells[POLE.Name].Value.ToString(),
                        CREATE_ON = UIHelper._DefaultDate.Date,
                        ORDER_RELAY_STATUS = (int)RelayControl.Disconnect,
                        METER_RELAY_STATUS = 0,
                        SEND_ORDER_RELAY_METER_ID = 0,
                        GET_ORDER_RELAY_METER_ID = 0,
                        CHNAGE_ID = 0
                    });
                }
                dgvClose.DataSource = (from r in rows.Where(x => x.ORDER_RELAY_STATUS == (int)RelayControl.Disconnect)
                                       orderby r.AREA_CODE, r.POLE_CODE, r.POLE_CODE, r.METER_CODE
                                       select r).ToList();
                lblCUSTOMER_TO_BLOCK.Visible = dgvClose.Rows.Count == 0;
            }

        }

        private void dgvOpen_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(List<DataGridViewRow>)))
            {
                var data = e.Data.GetData(typeof(List<DataGridViewRow>));

                foreach (var item in (List<DataGridViewRow>)data)
                {
                    if (rows.Select(x => x.CUSTOMER_ID).Contains((int)item.Cells[CUSTOMER_ID.Name].Value))
                    {
                        continue;
                    }
                    rows.Add(new TBL_RELAY_METER()
                    {
                        RELAY_METER_ID = Guid.NewGuid(),
                        CUSTOMER_ID = (int)item.Cells[CUSTOMER_ID.Name].Value,
                        METER_ID = (int)item.Cells[METER_ID.Name].Value,
                        CUSTOMER_CODE = item.Cells[CUSTOMER_CODE.Name].Value.ToString(),
                        METER_CODE = item.Cells[METER_CODE.Name].Value.ToString(),
                        FULL_NAME = item.Cells[CUSTOMER_NAME.Name].Value.ToString(),
                        BOX_CODE = item.Cells[BOX.Name].Value.ToString(),
                        AREA_CODE = item.Cells[AREA.Name].Value.ToString(),
                        POLE_CODE = item.Cells[POLE.Name].Value.ToString(),
                        CREATE_ON = UIHelper._DefaultDate.Date,
                        ORDER_RELAY_STATUS = (int)RelayControl.Connect,
                        METER_RELAY_STATUS = 0,
                        SEND_ORDER_RELAY_METER_ID = 0,
                        GET_ORDER_RELAY_METER_ID = 0,
                        CHNAGE_ID = 0
                    });
                }
                dgvOpen.DataSource = (from r in rows.Where(x => x.ORDER_RELAY_STATUS == (int)RelayControl.Connect)
                                      orderby r.AREA_CODE, r.POLE_CODE, r.BOX_CODE, r.METER_CODE
                                      select r).ToList();
                lblCUSTOMER_TO_OPEN.Visible = dgvOpen.Rows.Count == 0;
            }
        }

        private void dgvClose_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                foreach (DataGridViewRow item in dgvClose.SelectedRows)
                {
                    var cId = (int)item.Cells["CLOSE_" + CUSTOMER_ID.Name].Value;
                    rows.Remove(rows.FirstOrDefault(x => x.CUSTOMER_ID == cId));

                }
                dgvClose.DataSource = rows.Where(x => x.ORDER_RELAY_STATUS == (int)RelayControl.Disconnect).ToList();
            }
        }

        private void dgvOpen_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                foreach (DataGridViewRow item in dgvOpen.SelectedRows)
                {
                    var cId = (int)item.Cells["OPEN_" + CUSTOMER_ID.Name].Value;
                    rows.Remove(rows.FirstOrDefault(x => x.CUSTOMER_ID == cId));
                }
                dgvOpen.DataSource = rows.Where(x => x.ORDER_RELAY_STATUS == (int)RelayControl.Connect).ToList();
            }

        }

        private void dgvClose_DataSourceChanged(object sender, EventArgs e)
        {
            lblTotalCloseCustomer_.Text = rows.Where(x => x.ORDER_RELAY_STATUS == (int)RelayControl.Disconnect).Count().ToString();
        }

        private void dgvOpen_DataSourceChanged(object sender, EventArgs e)
        {
            lblTotalOpenCustomer_.Text = rows.Where(x => x.ORDER_RELAY_STATUS == (int)RelayControl.Connect).Count().ToString();
        }

        public void Search(string strSearch = "", int areaId = 0, int cusTypeId = 0, int cycleId = 0, int take = 24, bool allCus = false)
        {

            var decCutAmount = Convert.ToDecimal(DBDataContext.Db.TBL_UTILITies
                                .FirstOrDefault(u => u.UTILITY_ID == (int)Utility.CUT_OFF_AMOUNT)
                                .UTILITY_VALUE);
            var onlyPower = DataHelper.ParseToBoolean(Method.Utilities[Utility.USE_ONLY_POWER_INVOICE_TO_BLOCK_CUSTOMER]);

            var dateCurrentDate = DBDataContext.Db.GetSystemDate();

            var dt = from c in DBDataContext.Db.TBL_CUSTOMERs.Where(x =>
                x.IS_POST_PAID
                && (x.STATUS_ID == (int)CustomerStatus.Active || x.STATUS_ID == (int)CustomerStatus.Blocked)
                &&
                ((x.CUSTOMER_CODE + " " + x.LAST_NAME_KH + " " + x.FIRST_NAME_KH + " " + x.PHONE_1).ToLower()
                    .Contains(strSearch))
                && (cycleId == 0 || x.BILLING_CYCLE_ID == cycleId))


                     join cm in DBDataContext.Db.TBL_CUSTOMER_METERs.Where(x => x.IS_ACTIVE) on c.CUSTOMER_ID equals
                         cm.CUSTOMER_ID
                     join m in DBDataContext.Db.TBL_METERs on cm.METER_ID equals m.METER_ID
                     join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                     join p in DBDataContext.Db.TBL_POLEs on cm.POLE_ID equals p.POLE_ID
                     join ct in
                         DBDataContext.Db.TBL_CUSTOMER_TYPEs.Where(x => x.CUSTOMER_TYPE_ID == cusTypeId || cusTypeId == 0) on
                         c.CUSTOMER_TYPE_ID equals ct.CUSTOMER_TYPE_ID
                     join a in DBDataContext.Db.TBL_AREAs.Where(x => x.AREA_ID == areaId || areaId == 0) on c.AREA_ID equals
                         a.AREA_ID

                     join inv in
                         (from i in DBDataContext.Db.TBL_INVOICEs
                          join cur in DBDataContext.Db.TLKP_CURRENCies on i.CURRENCY_ID equals cur.CURRENCY_ID
                          where
                         allCus
                         || (i.INVOICE_STATUS == (int)InvoiceStatus.Open
                             && (!onlyPower || i.IS_SERVICE_BILL == false))
                          group i by new { i.CUSTOMER_ID, cur.CURRENCY_ID, cur.CURRENCY_SING }
                             into invoice
                          select (from row in invoice
                                  orderby row.DUE_DATE
                                  select new
                                  {
                                      row.CUSTOMER_ID,
                                      invoice.Key.CURRENCY_ID,
                                      invoice.Key.CURRENCY_SING,
                                      BALANCE_DUE = invoice.Sum(row1 => row1.SETTLE_AMOUNT - row1.PAID_AMOUNT),
                                      row.DUE_DATE,
                                      LAST_DUE_DATE = invoice.Max(x => x.DUE_DATE)
                                      //invoice.OrderByDescending(x=>x.DUE_DATE).FirstOrDefault().DUE_DATE
                                  }).First()
                             ) on c.CUSTOMER_ID equals inv.CUSTOMER_ID
                     orderby a.AREA_CODE, p.POLE_CODE, b.BOX_CODE, m.METER_CODE
                     where inv.BALANCE_DUE >= decCutAmount
                           && inv.DUE_DATE.AddDays(c.CUT_OFF_DAY).Date <= dateCurrentDate.Date
                     select new
                     {
                         c.CUSTOMER_ID,
                         m.METER_ID,
                         c.CUSTOMER_CODE,
                         m.METER_CODE,
                         ct.CUSTOMER_TYPE_NAME,
                         a.AREA_CODE,
                         p.POLE_CODE,
                         b.BOX_CODE,
                         FULL_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                         DUE_DATE = allCus ? inv.LAST_DUE_DATE.AddDays(c.CUT_OFF_DAY) : inv.DUE_DATE.AddDays(c.CUT_OFF_DAY),
                         Day =
                             (int)(dateCurrentDate -
                              (allCus ? inv.LAST_DUE_DATE : inv.DUE_DATE).AddDays(c.CUT_OFF_DAY)).TotalDays,
                         BALANCE_DUE = inv.BALANCE_DUE,
                         inv.CURRENCY_SING
                     };
            dgvListCustomer.DataSource = dt.Take(take).ToList();
            dgvOpen.DataSource = (from r in rows.Where(x => x.ORDER_RELAY_STATUS == (int)RelayControl.Connect
                                    && (x.CUSTOMER_CODE + " " + x.FULL_NAME + " " + x.METER_CODE + " " + x.AREA_CODE + " " + x.POLE_CODE + " " + x.BOX_CODE + " ").ToLower().Contains(strSearch.ToLower()))
                                  orderby r.AREA_CODE, r.POLE_CODE, r.BOX_CODE, r.METER_CODE
                                  select r).ToList();
            dgvClose.DataSource = (from r in rows.Where(x => x.ORDER_RELAY_STATUS == (int)RelayControl.Disconnect
                                    && (x.CUSTOMER_CODE + " " + x.FULL_NAME + " " + x.METER_CODE + " " + x.AREA_CODE + " " + x.POLE_CODE + " " + x.BOX_CODE + " ").ToLower().Contains(strSearch.ToLower()))
                                   orderby r.AREA_CODE, r.POLE_CODE, r.BOX_CODE, r.METER_CODE
                                   select r).ToList();

        }

        public void SendOrderRelayToIR(bool internet)
        {
            if (IRDevice.SendOrderRelayToIR(rows, internet))
            {
                dgvClose.DataSource = new List<TBL_RELAY_METER>();
                dgvClose.Refresh();
                dgvOpen.DataSource = new List<TBL_RELAY_METER>();
                dgvOpen.Refresh();
                rows.Clear();
                lblCUSTOMER_TO_OPEN.Visible = true;
                lblCUSTOMER_TO_BLOCK.Visible = true;
                lblTOTAL_CUSTOMER_TO_CLOSE.Text = "0";
                lblTOTAL_CUSTOMER_TO_OPEN.Text = "0";
            }

        }

        public void ReceiveOrderRelayFromIR(bool internet)
        {
            if (internet)
            {
                IRDevice.ReceiveRelayByInternet();
            }
            else
            {
                IRDevice.ReceiveRelayByWired();
            }
        }
        private void OrderRelayMeter_Load(object sender, EventArgs e)
        {
            Runner.Run(() => { this.Search(); });
        }
    }
}
