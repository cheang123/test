﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PagerRef : Form
    {
        public PagerRef()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            UIHelper.SetDataSourceToComboBox(cboGroup, DBDataContext.Db.TBL_AUDITTRIAL_GROUPs.Where(x => x.AUDIT_GROUP_ID == (int)AuditTrialGroup.SendUsage || x.AUDIT_GROUP_ID == (int)AuditTrialGroup.ReceiveUsage).Select(x => new { x.AUDIT_GROUP_ID, x.AUDIT_GROUP_NAME_LOCAL }), "");
            dtp1.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dtp2.Value = dtp1.Value.AddMonths(1).AddDays(-1); 
        }

        void bind()
        { 
            return;
            int g = 0;
            if (cboGroup.SelectedIndex!=-1)
            {
                g = (int)cboGroup.SelectedValue;
            }
            var query = from aud in DBDataContext.Db.TBL_AUDITTRIALs.Where(x=>x.AUDIT_GROUP_ID==(int)AuditTrialGroup.SendUsage || x.AUDIT_GROUP_ID==(int)AuditTrialGroup.ReceiveUsage)
                        join aug in DBDataContext.Db.TBL_AUDITTRIAL_GROUPs on aud.AUDIT_GROUP_ID equals aug.AUDIT_GROUP_ID
                        join l in DBDataContext.Db.TBL_LOGINs on aud.LOGIN_ID equals l.LOGIN_ID
                        where (aud.AUDIT_DATE.Date >= dtp1.Value.Date && aud.AUDIT_DATE.Date <= dtp2.Value.Date)
                            && (g==0 || aud.AUDIT_GROUP_ID ==g)
                        orderby aud.AUDIT_TRIAL_ID descending
                        select new
                        {
                            AUDIT_TRIAL_ID = aud.AUDIT_TRIAL_ID,
                            DATE = aud.AUDIT_DATE,
                            CONTEXT = aud.CONTEXT,
                            l.LOGIN_NAME,
                            GROUP = aug.AUDIT_GROUP_NAME_LOCAL
                        };
            dgv.DataSource = query;
        }

        private void btnUploadUsage_Click(object sender, EventArgs e)
        {
            new DialogUploadRef().ShowDialog();
        }

        private void btnDownloadUsage_Click(object sender, EventArgs e)
        {
            new DialogDownloadRef().ShowDialog();
        }

        private void txtQuickSearch_QuickSearch(object sender, EventArgs e)
        {
            return;
            bind();
        }

        private void btnRegisterMeter_Click(object sender, EventArgs e)
        {
            return;
            DialogMeterUnregister objDialog = new DialogMeterUnregister();
            objDialog.ShowDialog();
        }
         
    }
}
