﻿using EPower.Properties;
using Newtonsoft.Json;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static EPower.Logic.IRDevice.IRDevice;

namespace EPower.Interface
{
    public partial class DialogUsageDeviceCollection : ExDialog
    {
        TBL_USAGE_DEVICE_COLLECTION _obj;
        List<GET_MONTHLY_USAGE_RESULT> _monthlyUsage = new List<GET_MONTHLY_USAGE_RESULT>();
        List<TMP_IR_USAGE> _usage = new List<TMP_IR_USAGE>();
        public DialogUsageDeviceCollection(TBL_USAGE_DEVICE_COLLECTION obj)
        {
            InitializeComponent();
            _obj = obj;
            load();
        }

        void load()
        {
            if (_obj.TRANSFER_TYPE_ID == (int)TransferType.UPLOAD_USAGE)
            {
                _monthlyUsage = JsonConvert.DeserializeObject<List<GET_MONTHLY_USAGE_RESULT>>
                (Encoding.Unicode.GetString(_obj.DATA.ToArray()), new JsonSerializerSettings()
                {
                    DateFormatString = "dd-MM-yyyy HH:mm:ss",
                    Formatting = Newtonsoft.Json.Formatting.Indented
                });
                dgv.DataSource = _monthlyUsage._ToDataTable();
                chkMETER_TAMPER.Visible = false;
                dgv.Columns["PHASE_ID"].Visible = false;
                btnReport.Visible = false;
            }
            else
            {
                _usage = JsonConvert.DeserializeObject<List<TMP_IR_USAGE>>
                (Encoding.Unicode.GetString(_obj.DATA.ToArray()), new JsonSerializerSettings()
                {
                    DateFormatString = "dd-MM-yyyy HH:mm:ss",
                    Formatting = Newtonsoft.Json.Formatting.Indented
                });
                var data = from u in _usage
                           join t in DBDataContext.Db.TLKP_TAMPER_STATUS on u.TAMPER_STATUS equals t.TAMPER_STATUS_CODE into l
                           from s in l.DefaultIfEmpty()
                           select new
                           {
                               u.METER_CODE,
                               u.END_USAGE,
                               u.BOX_CODE,
                               u.IS_METER_RENEW_CYCLE,
                               TAMPER_STATUS = (s == null) ? "" : s.TAMPER_STATUS_NAME_KH
                           };

                dgv.DataSource = data._ToDataTable();
                dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                chkMETER_TAMPER.Visible = true;
                btnReport.Visible = false;
            }
        }

        private void txtQuickSearch_QuickSearch(object sender, EventArgs e)
        {
            if (_obj.TRANSFER_TYPE_ID == (int)TransferType.UPLOAD_USAGE)
            {
                dgv.DataSource = (from u in _monthlyUsage
                                  where (u.CUSTOMER_CODE + " " + u.FULL_NAME + " " + u.AREA_CODE + " " +
                                  u.POLE_CODE + " " + u.BOX_CODE + " " + u.METER_CODE).ToLower().Contains(txtQuickSearch.Text.ToLower())
                                  select u)._ToDataTable();
                dgv.Columns["PHASE_ID"].Visible = false;
            }
            else
            {
                dgv.DataSource = (from u in _usage
                                  join t in DBDataContext.Db.TLKP_TAMPER_STATUS on u.TAMPER_STATUS equals t.TAMPER_STATUS_CODE into l
                                  from s in l.DefaultIfEmpty()
                                  where (u.METER_CODE + " " + u.BOX_CODE).ToLower().Contains(txtQuickSearch.Text.ToLower())
                                    && (!chkMETER_TAMPER.Checked || (u.PHASE_ID == 3 && u.TAMPER_STATUS != ""))
                                  select new
                                  {
                                      u.METER_CODE,
                                      u.END_USAGE,
                                      u.BOX_CODE,
                                      u.IS_METER_RENEW_CYCLE,
                                      TAMPER_STATUS = (s == null) ? "" : s.TAMPER_STATUS_NAME_KH
                                  })._ToDataTable();
            }
        }

        private void DialogUsageDeviceCollection_Load(object sender, EventArgs e)
        {
            this.Text = _obj.TRANSFER_TYPE_ID == (int)TransferType.UPLOAD_USAGE ? Resources.SEND_DATA : Resources.RECEIVE_DATA;
            lblRECORD.Text = string.Format("{0}:{1}", Resources.RECORD, _obj.RECORD);
        }

        private void chkMETER_TAMPER_CheckedChanged(object sender, EventArgs e)
        {
            txtQuickSearch_QuickSearch(null, null);
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            CrystalReportHelper Report = new CrystalReportHelper("ReportMeterDetail.rpt");
            this.SendToBack();
            Report.SetParameter("@CHECK_TAMPER", (bool)!this.chkMETER_TAMPER.Checked);
            Report.ViewReport("");
            this.BringToFront();
        }
    }
}