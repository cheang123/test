﻿using EPower.Base.Logic;
using EPower.Properties;
using OpenNETCF.Desktop.Communication;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Data.SqlServerCe;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogImportDataToDevice : ExDialog
    {
        TBL_DEVICE _objDevice = null;

        bool isSuccess = false;

        public DialogImportDataToDevice()
        {
            InitializeComponent();
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            // if user not connect device to computer or microsoft device center is not initialized. 
            bool? conn = Devices.Connect();
            if ((bool)conn == false)
            {
                MsgBox.ShowInformation(Resources.REQUIRED_CONNECT_TO_DEVICE);
                return;
            }

            //If device never register in system.
            _objDevice = DBDataContext.Db.TBL_DEVICEs.FirstOrDefault
                        (x => x.DEVICE_HARDWARE_ID.ToLower() == Devices.MyDevice[DeviceInfo.DeviceHardwareId].ToLower() && x.STATUS_ID == (int)DeviceStatus.Used);

            if (_objDevice == null)
            {
                MsgBox.ShowInformation(Resources.MSG_DEVICE_NOT_REGISTERED);
                return;
            }

            //If there are no employee can use devices.
            int intCollector = (from emp in DBDataContext.Db.TBL_EMPLOYEEs
                                join Emppos in DBDataContext.Db.TBL_EMPLOYEE_POSITIONs on emp.EMPLOYEE_ID equals Emppos.EMPLOYEE_ID
                                join pos in DBDataContext.Db.TBL_POSITIONs on Emppos.POSITION_ID equals pos.POSITION_ID
                                join EmDe in DBDataContext.Db.TBL_EMPLOYEE_DEVICEs on emp.EMPLOYEE_ID equals EmDe.EMPLOYEE_ID
                                join de in DBDataContext.Db.TBL_DEVICEs on EmDe.DEVICE_ID equals de.DEVICE_ID
                                where emp.IS_ACTIVE
                                && (pos.IS_ACTIVE && pos.IS_USE_DEVICE)
                                && de.DEVICE_ID == _objDevice.DEVICE_ID
                                select emp.EMPLOYEE_ID).Count();
            if (intCollector == 0)
            {
                MsgBox.ShowInformation(Resources.MS_EMPLOYEE_CANNOT_USE_THIS_DEVICE);
                return;
            }

            txtDeviceCode.Text = _objDevice.DEVICE_CODE;
            txtDeviceName.Text = _objDevice.DEVICE_OEM_INFO;


            // copy file from device to pc
            // and backup that file.
            bool success = false;
            Runner.Run(delegate ()
            {
                success = false;
                this.copyFileFromDevice();
                success = true;
            }, Resources.MS_INITIALIZING_PROGRESS);


            // if copy file success
            // then update collector and usage.
            if (!success) return;
            Runner.Run(delegate ()
            {
                success = false;
                this.updateCollectorAndUsage();
                success = true;
            });


            // if update success
            // then copy file from pc to device.
            if (!success) return;
            Runner.Run(delegate ()
            {
                success = false;
                this.copyFileToDevice();
                success = true;
            }, Resources.MS_FINALLIZING_PROGRESS);


            // show dialog success if 
            // all step complete success.
            if (success)
            {
                MsgBox.ShowInformation(Resources.UPLOAD_DATA_SUCCESS);
            }
        }

        private void copyFileToDevice()
        {
            RAPI rp = new RAPI();
            try
            {
                rp.Connect();
                string path = Devices.FileMobileDatabase();
                rp.CopyFileToDevice(path, Settings.Default.PDB, true);
            }
            catch (RAPIException ex)
            {
                if (ex.Win32Error == 3)
                {
                    throw new Exception(Resources.MS_DEVICE_DONT_HAVE_APPLICATION);
                }
                else if (ex.Win32Error == 32)
                {
                    throw new Exception(Resources.MS_CLOSE_APPLICATION_IN_DEVICE);
                }
                else
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rp.Disconnect();
            }
        }
        private void copyFileFromDevice()
        {
            RAPI rp = new RAPI();
            try
            {
                rp.Connect();
                string path = Devices.FileMobileDatabase();
                string pathBackup = Path.Combine(Settings.Default.PATH_BACKUP, "EPower MobileDb " + DBDataContext.Db.GetSystemDate().ToString("yyyyMMddHHmmss") + ".sdf");
                rp.CopyFileFromDevice(path, Settings.Default.PDB, true);
                File.Copy(path, pathBackup, true);
            }
            catch (RAPIException ex)
            {
                if (ex.Win32Error == 3)
                {
                    throw new Exception(Resources.MS_DEVICE_DONT_HAVE_APPLICATION);
                }
                else if (ex.Win32Error == 32)
                {
                    throw new Exception(Resources.MS_CLOSE_APPLICATION_IN_DEVICE);
                }
                else
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            finally
            {
                rp.Disconnect();
            }
        }
        private void updateCollectorAndUsage()
        {
            // txtMonth.Text = ""; 
            Devices db = new Devices();

            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
            {
                int intTotalCustomer = 0;
                #region Clear database
                db.ExecuteCommand("DELETE FROM TBL_DEVICE_USAGE;");
                db.ExecuteCommand("DELETE FROM TBL_METER_INFO;");
                db.ExecuteCommand("DELETE FROM TBL_COLLECTOR;");
                #endregion

                #region InsertStatement
                string insertCollector = @"INSERT INTO TBL_COLLECTOR
                                               ([COLLECTOR_ID]
                                               ,[COLLECTOR_NAME]
                                               ,[PASSWORD])
                                         VALUES
                                               (@CollectorID
                                               ,@CollectorName
                                               ,@Password);";

                string insertMeterInfo = @"INSERT INTO TBL_METER_INFO
                                               ([METER_ID]
                                               ,[AREA_CODE]
                                               ,[POLE_CODE]
                                               ,[BOX_CODE]
                                               ,[METER_CODE]
                                               ,[CUSTOMER_NAME]
                                               ,[ADDRESS]
                                               ,[MAX_USAGE]
                                               ,[CUSTOMER_ID]
                                               ,[CUSTOMER_CODE])
                                         VALUES
                                               (@MeterID
                                               ,@AreaCode
                                               ,@PoleCode
                                               ,@BoxCode
                                               ,@MeterCode
                                               ,@CustomerName
                                               ,@Address
                                               ,@MaxUsage
                                               ,@CustomerID
                                               ,@CustomerCode)";

                string insertDeviceUsage = @"INSERT INTO TBL_DEVICE_USAGE
                                               ([METER_ID]
                                               ,[MONTH]
                                               ,[CREATE_ON]
                                               ,[START_USAGE]
                                               ,[END_USAGE]
                                               ,[COLLECTOR_ID]
                                               ,[IS_METER_RENEW_CYCLE]
                                               ,[IS_UPDATE])
                                         VALUES
                                               (@MeterID
                                               ,@Month 
                                               ,@CreateOn
                                               ,@StartUsage
                                               ,@EndUsage
                                               ,@Collector_ID
                                               ,@IsMeterRC
                                               ,@IsUpdate);";
                #endregion

                #region Insert Collector Info
                var dtCollector = from e in DBDataContext.Db.TBL_EMPLOYEEs
                                  join ed in DBDataContext.Db.TBL_EMPLOYEE_DEVICEs
                                  on e.EMPLOYEE_ID equals ed.EMPLOYEE_ID
                                  where e.IS_ACTIVE && ed.DEVICE_ID == _objDevice.DEVICE_ID
                                  select new
                                  {
                                      e.EMPLOYEE_ID,
                                      e.EMPLOYEE_NAME,
                                      e.PASSWORD
                                  };


                foreach (var item in dtCollector)
                {
                    Application.DoEvents();

                    SqlCeParameter DevCollectorID = new SqlCeParameter("@CollectorID", SqlDbType.Int, 4);
                    SqlCeParameter CollectorName = new SqlCeParameter("@CollectorName", SqlDbType.NVarChar, 100);
                    SqlCeParameter Password = new SqlCeParameter("@Password", SqlDbType.NVarChar, 100);

                    DevCollectorID.Value = item.EMPLOYEE_ID;
                    CollectorName.Value = item.EMPLOYEE_NAME;
                    Password.Value = item.PASSWORD;

                    db.ExecuteCommand(insertCollector, DevCollectorID, CollectorName, Password);
                }
                #endregion

                var objCycle = from b in DBDataContext.Db.TBL_BILLING_CYCLEs
                               where b.IS_ACTIVE
                               select new { b.CYCLE_ID };
                foreach (var item in objCycle)
                {
                    DateTime dtpCycle = Method.GetNextBillingMonth(int.Parse(item.CYCLE_ID.ToString()));
                    DateTime dtpMonth = new DateTime(dtpCycle.Year, dtpCycle.Month, 1);
                    DateTime dtplastMont = dtpMonth.AddMonths(-1);

                    // binding source.
                    var usage = from c in DBDataContext.Db.TBL_CUSTOMERs.Where(c => c.STATUS_ID != (int)CustomerStatus.Closed && c.STATUS_ID != (int)CustomerStatus.Pending && c.BILLING_CYCLE_ID == item.CYCLE_ID)
                                join cm in DBDataContext.Db.TBL_CUSTOMER_METERs.Where(m => m.IS_ACTIVE) on c.CUSTOMER_ID equals cm.CUSTOMER_ID
                                join m in DBDataContext.Db.TBL_METERs.Where(m => m.STATUS_ID == (int)MeterStatus.Used) on cm.METER_ID equals m.METER_ID
                                join mt in DBDataContext.Db.TBL_METER_TYPEs on m.METER_TYPE_ID equals mt.METER_TYPE_ID
                                join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                                join p in DBDataContext.Db.TBL_POLEs on cm.POLE_ID equals p.POLE_ID
                                join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                                join u in DBDataContext.Db.TBL_USAGEs.Where(row => row.USAGE_MONTH.Date == dtpMonth.Date) on new { c.CUSTOMER_ID, cm.METER_ID } equals new { u.CUSTOMER_ID, u.METER_ID } into tmpUsages
                                from ju in tmpUsages.DefaultIfEmpty()
                                select new
                                {
                                    m.METER_ID,
                                    a.AREA_CODE,
                                    p.POLE_CODE,
                                    b.BOX_CODE,
                                    m.METER_CODE,
                                    CUSTOMER_NAME = string.Concat(c.LAST_NAME_KH, " ", c.FIRST_NAME_KH),
                                    c.ADDRESS,
                                    c.CUSTOMER_ID,
                                    c.CUSTOMER_CODE,
                                    mt.MAX_USAGE,
                                    MONTH = dtpMonth.Date,
                                    CREATE_ON = UIHelper._DefaultDate,
                                    COLLECTOR_ID = ju == null ? 0 : ju.COLLECTOR_ID,


                                    // ju == null usage not yet have. 
                                    USAGE_ID = ju == null ? 0 : ju.USAGE_ID,

                                    // if not yet posted then start usage is endusage of last month.
                                    START_USAGE = ju == null ? (from tmp in DBDataContext.Db.TBL_USAGEs
                                                                where tmp.USAGE_MONTH.Date == dtplastMont.Date && tmp.CUSTOMER_ID == c.CUSTOMER_ID
                                                                orderby tmp.USAGE_ID descending
                                                                select (decimal?)tmp.END_USAGE).FirstOrDefault() ?? 0
                                                             : ju.START_USAGE,

                                    // if not yet posted then end usage is 0.
                                    END_USAGE = ju == null ? 0 : ju.END_USAGE,
                                    IS_METER_RENEW_CYCLE = ju == null ? false : ju.IS_METER_RENEW_CYCLE,

                                };

                    intTotalCustomer += usage.Count();

                    foreach (var us in usage)
                    {
                        Application.DoEvents();

                        //MeterInfo Parameter.
                        SqlCeParameter MeterID = new SqlCeParameter("@MeterID", SqlDbType.Int, 4);
                        SqlCeParameter AreaCode = new SqlCeParameter("@AreaCode", SqlDbType.NVarChar, 100);
                        SqlCeParameter PoleCode = new SqlCeParameter("@PoleCode", SqlDbType.NVarChar, 100);
                        SqlCeParameter BoxCode = new SqlCeParameter("@BoxCode", SqlDbType.NVarChar, 100);
                        SqlCeParameter MeterCode = new SqlCeParameter("@MeterCode", SqlDbType.NVarChar, 100);
                        SqlCeParameter CustomerName = new SqlCeParameter("@CustomerName", SqlDbType.NVarChar, 100);
                        SqlCeParameter Address = new SqlCeParameter("@Address", SqlDbType.NVarChar, 100);
                        SqlCeParameter MaxUsage = new SqlCeParameter("@MaxUsage", SqlDbType.Float, 8);
                        SqlCeParameter CustomerID = new SqlCeParameter("@CustomerID", SqlDbType.Int, 4);
                        SqlCeParameter CustomerCode = new SqlCeParameter("@CustomerCode", SqlDbType.NVarChar, 100);
                        MeterID.Value = us.METER_ID;
                        AreaCode.Value = us.AREA_CODE;
                        PoleCode.Value = us.POLE_CODE;
                        BoxCode.Value = us.BOX_CODE;
                        MeterCode.Value = us.METER_CODE.PadLeft(Settings.Default.METER_DIGIT, '0');
                        CustomerName.Value = us.CUSTOMER_NAME;
                        Address.Value = us.ADDRESS;
                        MaxUsage.Value = us.MAX_USAGE;
                        CustomerID.Value = us.CUSTOMER_ID;
                        CustomerCode.Value = us.CUSTOMER_CODE;

                        //Insert to Meter Info.
                        db.ExecuteCommand(insertMeterInfo,
                                          MeterID,
                                          AreaCode,
                                          PoleCode,
                                          BoxCode,
                                          MeterCode,
                                          CustomerName,
                                          Address,
                                          MaxUsage,
                                          CustomerID,
                                          CustomerCode);



                        //Usage parameter.   
                        SqlCeParameter MeterDeviceUsage = new SqlCeParameter("@MeterID", SqlDbType.Int, 4);
                        SqlCeParameter Month = new SqlCeParameter("@Month", SqlDbType.DateTime);
                        SqlCeParameter StartUsage = new SqlCeParameter("@StartUsage", SqlDbType.Float, 8);
                        SqlCeParameter CreateOn = new SqlCeParameter("@CreateOn", SqlDbType.DateTime);
                        SqlCeParameter EndUsage = new SqlCeParameter("@EndUsage", SqlDbType.Float, 8);
                        SqlCeParameter CollectorID = new SqlCeParameter("@Collector_ID", SqlDbType.Int, 4);
                        SqlCeParameter IsMeterRC = new SqlCeParameter("@IsMeterRC", SqlDbType.Int, 4);
                        SqlCeParameter IsUpdate = new SqlCeParameter("@IsUpdate", SqlDbType.Bit);

                        MeterDeviceUsage.Value = us.METER_ID;
                        Month.Value = us.MONTH;
                        StartUsage.Value = us.START_USAGE;
                        CreateOn.Value = us.CREATE_ON;
                        EndUsage.Value = us.END_USAGE;
                        CollectorID.Value = us.COLLECTOR_ID;
                        IsMeterRC.Value = us.IS_METER_RENEW_CYCLE;
                        IsUpdate.Value = false;

                        //Insert to Device Usage.
                        db.ExecuteCommand(insertDeviceUsage,
                                          MeterDeviceUsage,
                                          Month,
                                          StartUsage,
                                          CreateOn,
                                          EndUsage,
                                          CollectorID,
                                          IsMeterRC,
                                          IsUpdate);
                    }
                    txtMonth.Text = string.Concat(txtMonth.Text, dtpMonth.Date.Month.ToString("00"), ", ");
                }
                tran.Complete();
                db.Connection.Close();
                isSuccess = true;

                txtTotalCustomer.Text = intTotalCustomer.ToString();
                txtMonth.Text = txtMonth.Text.TrimEnd(new char[] { ',', ' ' });
            }
        }


        /// <summary>
        /// Delete file database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DialogImportCollectUsageByDevice_FormClosing(object sender, FormClosingEventArgs e)
        {
            string dbMobile = Devices.FileMobileDatabase();
            if (File.Exists(dbMobile))
            {
                File.Delete(dbMobile);
            }
        }

        private void logTransaction(TBL_DEVICE objDevice, DataRow dr)
        {
            string strFile = string.Concat(Devices.PathDatabase, "\\", "logPosting.txt");
            if (!File.Exists(strFile))
            {
                try
                {
                    File.CreateText(strFile);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            StringBuilder st = new StringBuilder();
            st.AppendLine("========<SoftTech>=========");
            st.AppendLine(string.Format("\t\t\tCreate on:{0}", DBDataContext.Db.GetSystemDate()));
            st.AppendLine(string.Format("Device code\t:\t{0}", objDevice.DEVICE_CODE));
            st.AppendLine(string.Format("Device Name\t:\t{0}", objDevice.DEVICE_OEM_INFO));
            st.AppendLine(string.Format("Device Hardware ID:\t{0}", objDevice.DEVICE_CODE, objDevice.DEVICE_OEM_INFO, objDevice.DEVICE_HARDWARE_ID));
            int row = 1;
            st.AppendLine("No \t METER_CODE \t CUSTOMER_NAME \t START_USAGE \t END_USAGE \t COLLECTOR_NAME");
            st.AppendLine(string.Format("{0} \t {1} \t {2} \t {3} \t {4} \t {5} \t {6}",
                row++
                , dr["METER_CODE"]
                , dr["CUSTOMER_NAME"]
                , dr["START_USAGE"]
                , dr["END_USAGE"]
                , dr["COLLECTOR_NAME"]));

            File.AppendAllText(strFile, st.ToString(), Encoding.UTF8);
        }
    }
}



















