﻿using EPower.Base.Logic;
using EPower.Properties;
using OpenNETCF.Desktop.Communication;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogCollectUsageByDevice : ExDialog
    {
        TBL_DEVICE _objDevice = null;
        DataSet _dsMobileDB = new DataSet();
        string monthsList = "";

        public DialogCollectUsageByDevice()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        /// <summary>
        /// Get total usage of meter.
        /// </summary>
        /// <param name="objUsage"></param>
        /// <returns></returns>
        private decimal getTotalUsage(TBL_USAGE objUsage)
        {
            decimal totalUsage = 0;
            if (objUsage.START_USAGE > objUsage.END_USAGE)
            {
                decimal MaximumUsage = (from m in DBDataContext.Db.TBL_METERs
                                        join mt in DBDataContext.Db.TBL_METER_TYPEs on m.METER_TYPE_ID equals mt.METER_TYPE_ID
                                        where m.METER_ID == objUsage.METER_ID
                                        select new { mt.MAX_USAGE }).FirstOrDefault().MAX_USAGE;
                totalUsage = ((MaximumUsage - objUsage.START_USAGE) + objUsage.END_USAGE);
            }
            else //Not new cycle.
            {
                totalUsage = (objUsage.END_USAGE - objUsage.START_USAGE);
            }
            return totalUsage;
        }

        private void btnCheckNoUsage_Click(object sender, EventArgs e)
        {
            DialogCustomerNoUsage dig = new DialogCustomerNoUsage(0);
            dig.ShowDialog();
        }

        private void btnUnknowMeter_Click(object sender, EventArgs e)
        {
            new DialogMeterUnknown().ShowDialog();
        }

        private void btnCheckNewCycle_Click(object sender, EventArgs e)
        {
            new DialogNewCycleCustomer().ShowDialog();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            // if user not connect device to computer 
            // or microsoft device center was not initialized.
            bool? conn = Devices.Connect();
            if (conn == false)
            {
                MsgBox.ShowInformation(Resources.REQUIRED_CONNECT_TO_DEVICE);
                return;
            }

            // if device not register in software.
            _objDevice = DBDataContext.Db.TBL_DEVICEs.FirstOrDefault
                        (x => x.DEVICE_HARDWARE_ID.ToLower() == Devices.MyDevice[DeviceInfo.DeviceHardwareId].ToLower()
                           && x.STATUS_ID == (int)DeviceStatus.Used);
            if (_objDevice == null)
            {
                MsgBox.ShowInformation(Resources.MSG_DEVICE_NOT_REGISTERED);
                return;
            }

            // display device info.
            txtDeviceCode.Text = _objDevice.DEVICE_CODE;
            txtDeviceName.Text = _objDevice.DEVICE_OEM_INFO;


            // copy remote file from device.
            bool success = false;
            Runner.Run(delegate ()
            {
                success = false;
                this.copyFileFromDevice();
                success = true;
            }, Resources.MS_INITIALIZING_PROGRESS);


            // load data from *.sdf file.
            if (!success) return;
            Runner.Run(delegate ()
            {
                success = false;
                this.loadData();
                success = true;
            });


            // process data import and update.
            if (!success) return;
            Runner.Run(delegate ()
            {
                success = false;
                this.importDataToServer();
                success = true;
            });


            if (success)
            {
                int intCustomerTotal = _dsMobileDB.Tables["TBL_METER_INFO"].Rows.Count;
                int intCustomerHaveUsage = this.getNumberOfCustomerHaveUsage();
                int intCustomerNoUsage = intCustomerTotal - intCustomerHaveUsage;

                //Show summary information
                this.txtAllCustomer.Text = intCustomerTotal.ToString();
                this.txtHaveUsageCustomer.Text = intCustomerHaveUsage.ToString();
                this.txtNoUsageCustomer.Text = intCustomerNoUsage.ToString();
                this.txtUnknownMeter.Text = DBDataContext.Db.TBL_METER_UNKNOWNs.Count().ToString();
                this.txtNewCycle.Text = this.getNumberOfMeterNewCycle().ToString();
                this.txtMonth.Text = this.monthsList;

                MsgBox.ShowInformation(Resources.UPLOAD_DATA_SUCCESS);
            }

        }

        private void copyFileFromDevice()
        {
            RAPI rp = new RAPI();
            try
            {
                rp.Connect();
                string FullName = Devices.FileDatabase(_objDevice.DEVICE_CODE);
                rp.CopyFileFromDevice(FullName, Settings.Default.PDB, true);
            }
            catch (RAPIException ex)
            {
                if (ex.Win32Error == 3)
                {
                    throw new Exception(Resources.MS_DEVICE_DONT_HAVE_APPLICATION);
                }
                else if (ex.Win32Error == 32)
                {
                    throw new Exception(Resources.MS_CLOSE_APPLICATION_IN_DEVICE);
                }
                else
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                rp.Disconnect();
            }
        }

        private void loadData()
        {
            //load data
            Devices db = new Devices(_objDevice.DEVICE_CODE);

            // prepare dataset for use.
            _dsMobileDB = new DataSet();
            DataTable[] dtMobile = new DataTable[] { new DataTable("TBL_COLLECTOR"), new DataTable("TBL_DEVICE_USAGE"), new DataTable("TBL_METER_INFO") };
            dtMobile[0] = db.ExecuteDataTable("SELECT * FROM TBL_COLLECTOR");
            dtMobile[1] = db.ExecuteDataTable("SELECT * FROM TBL_DEVICE_USAGE");
            dtMobile[2] = db.ExecuteDataTable("SELECT * FROM TBL_METER_INFO");
            _dsMobileDB.Tables.AddRange(dtMobile);


            // calculate months list.
            DataTable dtMonth = new DataTable();
            dtMonth = db.ExecuteDataTable("SELECT DISTINCT MONTH FROM TBL_DEVICE_USAGE");
            this.monthsList = "";
            foreach (DataRow item in dtMonth.Rows)
            {
                this.monthsList = string.Concat(this.monthsList, DateTime.Parse(item["MONTH"].ToString()).Month.ToString("00"), ", ");
            }
            this.monthsList = monthsList.TrimEnd(new char[] { ',', ' ' });


            if (db.Connection.State == ConnectionState.Open)
            {
                db.Connection.Close();
            }
        }

        private void importDataToServer()
        {
            if (_dsMobileDB.Tables["TBL_METER_INFO"].Rows.Count == 0)
            {
                return;
            }

            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
            {
                //update password employee.
                foreach (DataRow dr in _dsMobileDB.Tables["TBL_COLLECTOR"].Rows)
                {
                    TBL_EMPLOYEE emp = DBDataContext.Db.TBL_EMPLOYEEs.FirstOrDefault
                        (x => x.EMPLOYEE_ID == (int)dr["COLLECTOR_ID"] && x.IS_ACTIVE);
                    if (emp != null)
                    {
                        if (emp.PASSWORD != dr["PASSWORD"].ToString())
                        {
                            emp.PASSWORD = dr["PASSWORD"].ToString();
                        }
                    }
                }

                DateTime datCurrentDate = DBDataContext.Db.GetSystemDate();
                var dtTmpUsage = from mi in _dsMobileDB.Tables["TBL_METER_INFO"].Select()
                                 join du in _dsMobileDB.Tables["TBL_DEVICE_USAGE"].Select() on mi["METER_ID"] equals du["METER_ID"]
                                 where (bool)du["IS_UPDATE"]
                                 select new
                                 {
                                     METER_ID = DataHelper.ParseToInt(mi["METER_ID"].ToString()),
                                     CUSTOMER_ID = DataHelper.ParseToInt(mi["CUSTOMER_ID"].ToString()),
                                     METER_CODE = mi["METER_CODE"].ToString(),
                                     DEVICE_CODE = _objDevice.DEVICE_CODE,
                                     START_USAGE = DataHelper.ParseToDecimal(du["START_USAGE"].ToString()),
                                     END_USAGE = DataHelper.ParseToDecimal(du["END_USAGE"].ToString()),
                                     IS_METER_RENEW_CYCLE = DataHelper.ParseToBoolean(du["IS_METER_RENEW_CYCLE"].ToString()),
                                     USAGE_MONTH = DateTime.Parse(du["MONTH"].ToString()),
                                     COLLECTOR_ID = DataHelper.ParseToInt(du["COLLECTOR_ID"].ToString()),
                                     AREA_CODE = mi["AREA_CODE"].ToString(),
                                     POLE_CODE = mi["POLE_CODE"].ToString(),
                                     BOX_CODE = mi["BOX_CODE"].ToString()
                                 };
                //Insert all usage from Mobile Database to TBL_TMP_USAGE_MOBILE
                foreach (var drTmpUage in dtTmpUsage)
                {
                    TBL_TMP_USAGE_MOBILE objTmp = new TBL_TMP_USAGE_MOBILE()
                    {
                        METER_ID = drTmpUage.METER_ID,
                        CUSTOMER_ID = drTmpUage.CUSTOMER_ID,
                        METER_CODE = Method.FormatMeterCode(drTmpUage.METER_CODE),
                        DEVICE_CODE = drTmpUage.DEVICE_CODE,
                        START_USAGE = drTmpUage.START_USAGE,
                        END_USAGE = drTmpUage.END_USAGE,
                        IS_METER_RENEW_CYCLE = drTmpUage.IS_METER_RENEW_CYCLE,
                        USAGE_MONTH = drTmpUage.USAGE_MONTH,
                        COLLECTOR_ID = drTmpUage.COLLECTOR_ID,
                        AREA_CODE = drTmpUage.AREA_CODE,
                        POLE_CODE = drTmpUage.POLE_CODE,
                        BOX_CODE = drTmpUage.POLE_CODE
                    };
                    DBDataContext.Db.TBL_TMP_USAGE_MOBILEs.InsertOnSubmit(objTmp);
                }
                DBDataContext.Db.SubmitChanges();


                // Insert unknown meter to TBL_METER_UNKNOWN
                // Delete and unknown meter from TBL_TMP_USAGE
                string sqlUnknownMeter = @"  
                                -- Insert Unknown meter to TBL_METER_UNKNOWN
                                DECLARE @DEVICE_CODE NVARCHAR(50)
                                SET @DEVICE_CODE = {0};
                                
                                INSERT INTO TBL_METER_UNKNOWN
                                SELECT  t.METER_CODE ,
                                        t.END_USAGE,
                                        COLLECTOR_ID,
                                        DEVICE_CODE ,
                                        AREA_CODE ,
                                        POLE_CODE ,
                                        BOX_CODE ,
                                        DEVICE_TYPE_ID = 1
                                FROM TBL_TMP_USAGE_MOBILE t
                                WHERE DEVICE_CODE = @DEVICE_CODE
                                      AND t.CUSTOMER_ID=0
                                      AND METER_CODE  NOT IN (SELECT METER_CODE FROM TBL_METER_UNKNOWN )
                                
                                -- delete unknown meter from TBL_TMP_USAGE
                                DELETE FROM TBL_TMP_USAGE_MOBILE 
                                WHERE  DEVICE_CODE = @DEVICE_CODE 
                                       AND ( METER_CODE IN (SELECT METER_CODE FROM TBL_METER_UNKNOWN))";

                DBDataContext.Db.ExecuteCommand(sqlUnknownMeter, _objDevice.DEVICE_CODE);


                string sqlUpdate = @"
                                DECLARE @CREATE_BY NVARCHAR(50),
                                        @DEVICE_ID INT;

                                SET @CREATE_BY = {0};
                                SET @DEVICE_ID={1};  

                                UPDATE TBL_USAGE
                                SET	
                                    END_USAGE=t.END_USAGE,
                                    DEVICE_ID=@DEVICE_ID,
	                                COLLECTOR_ID=t.COLLECTOR_ID,
                                    IS_METER_RENEW_CYCLE= t.IS_METER_RENEW_CYCLE,
                                    MULTIPLIER=0
                                FROM TBL_CUSTOMER_METER cm 
                                INNER JOIN TBL_TMP_USAGE_MOBILE t ON t.METER_ID=cm.METER_ID AND cm.IS_ACTIVE=1
                                INNER JOIN TBL_USAGE u ON cm.CUSTOMER_ID=u.CUSTOMER_ID AND cm.METER_ID=u.METER_ID AND u.USAGE_MONTH=t.USAGE_MONTH;

                                -- UPDATE MULTIPLIER AND TOTAL_USAGE
					            UPDATE TBL_USAGE
					            SET MULTIPLIER =  m.MULTIPLIER 
					            FROM TBL_USAGE u INNER JOIN TBL_METER m ON m.METER_ID = u.METER_ID
					            WHERE u.MULTIPLIER=0;


                ";
                DBDataContext.Db.ExecuteCommand(sqlUpdate, Login.CurrentLogin.LOGIN_NAME, _objDevice.DEVICE_ID);



                foreach (TBL_BILLING_CYCLE item in DBDataContext.Db.TBL_BILLING_CYCLEs.Where(x => x.IS_ACTIVE))
                {
                    DateTime datStart = DateTime.Now, datEnd = DateTime.Now, datMonth;
                    datMonth = Method.GetNextBillingMonth(item.CYCLE_ID, ref datStart, ref datEnd);

                    //Insert usage to customer
                    string sqlInsertByCycle = @"
                                DECLARE @CREATE_BY NVARCHAR(50),
                                        @DEVICE_ID INT,
                                        @CYCLE_ID INT,
                                        @USAGE_MONTH DATETIME,
                                        @START_DATE DATETIME,
                                        @END_DATE DATETIME;

                                SET @CREATE_BY = {0};
                                SET @DEVICE_ID={1};
                                SET @CYCLE_ID={2};
                                SET @USAGE_MONTH={3};
                                SET @START_DATE={4};
                                SET @END_DATE={5}; 

                                INSERT INTO TBL_USAGE
                                SELECT		CUSTOMER_ID=cm.CUSTOMER_ID,
                                            METER_ID=cm.METER_ID,
                                            START_USAGE=t.START_USAGE,
                                            END_USAGE=t.END_USAGE,
                                            USAGE_MONTH=@USAGE_MONTH,
                                            START_USE_DATE=@START_DATE,
                                            END_USE_DATE=@END_DATE,
                                            DEVICE_ID=@DEVICE_ID,
                                            COLLECTOR_ID=t.COLLECTOR_ID,
                                            CREATE_BY=@CREATE_BY,
                                            POSTING_DATE=GETDATE(),
                                            IS_METER_RENEW_CYCLE=t.IS_METER_RENEW_CYCLE,
                                            MULTIPLIER=0,
                                            IS_READ_IR=0
                                FROM TBL_CUSTOMER_METER cm  
                                INNER JOIN TBL_TMP_USAGE_MOBILE t ON t.METER_ID=cm.METER_ID AND cm.IS_ACTIVE=1
                                INNER JOIN TBL_CUSTOMER c ON c.CUSTOMER_ID = cm.CUSTOMER_ID
                                        
                                WHERE c.BILLING_CYCLE_ID=@CYCLE_ID 
                                      AND  NOT  EXISTS( SELECT USAGE_ID FROM TBL_USAGE
                                                        WHERE CUSTOMER_ID = cm.CUSTOMER_ID AND METER_ID=cm.METER_ID 
                                                        AND USAGE_MONTH=t.USAGE_MONTH);

                                -- UPDATE MULTIPLIER AND TOTAL_USAGE
					            UPDATE TBL_USAGE
                    SET MULTIPLIER =  m.MULTIPLIER 
                    FROM TBL_USAGE u INNER JOIN TBL_METER m ON m.METER_ID = u.METER_ID
                    WHERE u.MULTIPLIER=0;
                                ";
                    DBDataContext.Db.ExecuteCommand(sqlInsertByCycle, Login.CurrentLogin.LOGIN_NAME, _objDevice.DEVICE_ID, item.CYCLE_ID, datMonth, datStart, datEnd);
                    Application.DoEvents();
                }

                //Delete temp table with the current connected device 
                DBDataContext.Db.ExecuteCommand("DELETE FROM TBL_TMP_USAGE_MOBILE WHERE DEVICE_CODE={0};", _objDevice.DEVICE_CODE);

                tran.Complete();
                DBDataContext.Db = null;
            }
        }

        private int getNumberOfMeterNewCycle()
        {
            int intTotalNewCycleMeter = 0;
            foreach (int cycleId in DBDataContext.Db.TBL_BILLING_CYCLEs.Where(row => row.IS_ACTIVE).Select(row => row.CYCLE_ID))
            {
                DateTime datMonth = Method.GetNextBillingMonth(cycleId);
                intTotalNewCycleMeter += (from u in DBDataContext.Db.TBL_USAGEs
                                          join c in DBDataContext.Db.TBL_CUSTOMERs on u.CUSTOMER_ID equals c.CUSTOMER_ID
                                          where u.USAGE_MONTH == datMonth && c.BILLING_CYCLE_ID == cycleId
                                          && u.IS_METER_RENEW_CYCLE
                                          select u.CUSTOMER_ID).Distinct().Count();
            }
            return intTotalNewCycleMeter;
        }

        public int getNumberOfCustomerHaveUsage()
        {
            int result = 0;
            foreach (int cycleId in DBDataContext.Db.TBL_BILLING_CYCLEs.Where(row => row.IS_ACTIVE).Select(row => row.CYCLE_ID))
            {
                DateTime datMonth = Method.GetNextBillingMonth(cycleId);
                result += (from u in DBDataContext.Db.TBL_USAGEs
                           join c in DBDataContext.Db.TBL_CUSTOMERs on u.CUSTOMER_ID equals c.CUSTOMER_ID
                           where u.USAGE_MONTH == datMonth && c.BILLING_CYCLE_ID == cycleId
                                   && u.COLLECTOR_ID != 0
                           select u.CUSTOMER_ID).Distinct().Count();
            }
            return result;
        }
    }
}