﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomersCharge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomersCharge));
            this.lblSERVICE = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.lblAMOUNT_PENALTY = new System.Windows.Forms.Label();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.cboCurrencyId = new System.Windows.Forms.ComboBox();
            this.TOTAL_CUSTOMER = new System.Windows.Forms.Label();
            this.lblCustomerSelected_ = new System.Windows.Forms.Label();
            this.lblDUE_DATE = new System.Windows.Forms.Label();
            this.dtpDueDate = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblTAX = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.lblTOTAL_AMOUNT = new System.Windows.Forms.Label();
            this.dtpInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.lblINVOICE_DATE = new System.Windows.Forms.Label();
            this.cboTax = new DevExpress.XtraEditors.LookUpEdit();
            this.cboInvoiceItem = new DevExpress.XtraEditors.LookUpEdit();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboInvoiceItem.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboInvoiceItem);
            this.content.Controls.Add(this.cboTax);
            this.content.Controls.Add(this.dtpInvoiceDate);
            this.content.Controls.Add(this.lblINVOICE_DATE);
            this.content.Controls.Add(this.txtTotalAmount);
            this.content.Controls.Add(this.lblTOTAL_AMOUNT);
            this.content.Controls.Add(this.lblTAX);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.dtpDueDate);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.cboCurrencyId);
            this.content.Controls.Add(this.txtAmount);
            this.content.Controls.Add(this.lblCustomerSelected_);
            this.content.Controls.Add(this.TOTAL_CUSTOMER);
            this.content.Controls.Add(this.lblDUE_DATE);
            this.content.Controls.Add(this.lblAMOUNT_PENALTY);
            this.content.Controls.Add(this.lblSERVICE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblSERVICE, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT_PENALTY, 0);
            this.content.Controls.SetChildIndex(this.lblDUE_DATE, 0);
            this.content.Controls.SetChildIndex(this.TOTAL_CUSTOMER, 0);
            this.content.Controls.SetChildIndex(this.lblCustomerSelected_, 0);
            this.content.Controls.SetChildIndex(this.txtAmount, 0);
            this.content.Controls.SetChildIndex(this.cboCurrencyId, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.dtpDueDate, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblTAX, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtTotalAmount, 0);
            this.content.Controls.SetChildIndex(this.lblINVOICE_DATE, 0);
            this.content.Controls.SetChildIndex(this.dtpInvoiceDate, 0);
            this.content.Controls.SetChildIndex(this.cboTax, 0);
            this.content.Controls.SetChildIndex(this.cboInvoiceItem, 0);
            // 
            // lblSERVICE
            // 
            resources.ApplyResources(this.lblSERVICE, "lblSERVICE");
            this.lblSERVICE.Name = "lblSERVICE";
            // 
            // txtAmount
            // 
            resources.ApplyResources(this.txtAmount, "txtAmount");
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Enter += new System.EventHandler(this.txtAmount_Enter);
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // lblAMOUNT_PENALTY
            // 
            resources.ApplyResources(this.lblAMOUNT_PENALTY, "lblAMOUNT_PENALTY");
            this.lblAMOUNT_PENALTY.Name = "lblAMOUNT_PENALTY";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // cboCurrencyId
            // 
            this.cboCurrencyId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrencyId.DropDownWidth = 200;
            this.cboCurrencyId.FormattingEnabled = true;
            this.cboCurrencyId.Items.AddRange(new object[] {
            resources.GetString("cboCurrencyId.Items"),
            resources.GetString("cboCurrencyId.Items1"),
            resources.GetString("cboCurrencyId.Items2"),
            resources.GetString("cboCurrencyId.Items3"),
            resources.GetString("cboCurrencyId.Items4")});
            resources.ApplyResources(this.cboCurrencyId, "cboCurrencyId");
            this.cboCurrencyId.Name = "cboCurrencyId";
            this.cboCurrencyId.Tag = "0";
            // 
            // TOTAL_CUSTOMER
            // 
            resources.ApplyResources(this.TOTAL_CUSTOMER, "TOTAL_CUSTOMER");
            this.TOTAL_CUSTOMER.Name = "TOTAL_CUSTOMER";
            // 
            // lblCustomerSelected_
            // 
            resources.ApplyResources(this.lblCustomerSelected_, "lblCustomerSelected_");
            this.lblCustomerSelected_.Name = "lblCustomerSelected_";
            // 
            // lblDUE_DATE
            // 
            resources.ApplyResources(this.lblDUE_DATE, "lblDUE_DATE");
            this.lblDUE_DATE.Name = "lblDUE_DATE";
            // 
            // dtpDueDate
            // 
            resources.ApplyResources(this.dtpDueDate, "dtpDueDate");
            this.dtpDueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDueDate.Name = "dtpDueDate";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btnCLOSE);
            this.panel1.Controls.Add(this.btnOK);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // groupBox2
            // 
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // lblTAX
            // 
            resources.ApplyResources(this.lblTAX, "lblTAX");
            this.lblTAX.Name = "lblTAX";
            // 
            // txtTotalAmount
            // 
            resources.ApplyResources(this.txtTotalAmount, "txtTotalAmount");
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            // 
            // lblTOTAL_AMOUNT
            // 
            resources.ApplyResources(this.lblTOTAL_AMOUNT, "lblTOTAL_AMOUNT");
            this.lblTOTAL_AMOUNT.Name = "lblTOTAL_AMOUNT";
            // 
            // dtpInvoiceDate
            // 
            resources.ApplyResources(this.dtpInvoiceDate, "dtpInvoiceDate");
            this.dtpInvoiceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInvoiceDate.Name = "dtpInvoiceDate";
            // 
            // lblINVOICE_DATE
            // 
            resources.ApplyResources(this.lblINVOICE_DATE, "lblINVOICE_DATE");
            this.lblINVOICE_DATE.Name = "lblINVOICE_DATE";
            // 
            // cboTax
            // 
            resources.ApplyResources(this.cboTax, "cboTax");
            this.cboTax.Name = "cboTax";
            this.cboTax.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboTax.Properties.Appearance.Font")));
            this.cboTax.Properties.Appearance.Options.UseFont = true;
            this.cboTax.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboTax.Properties.Buttons"))))});
            this.cboTax.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboTax.Properties.Columns"), resources.GetString("cboTax.Properties.Columns1"))});
            this.cboTax.Properties.DisplayMember = "TAX_NAME";
            this.cboTax.Properties.NullText = resources.GetString("cboTax.Properties.NullText");
            this.cboTax.Properties.ValueMember = "TAX_ID";
            // 
            // cboInvoiceItem
            // 
            resources.ApplyResources(this.cboInvoiceItem, "cboInvoiceItem");
            this.cboInvoiceItem.Name = "cboInvoiceItem";
            this.cboInvoiceItem.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboInvoiceItem.Properties.Appearance.Font")));
            this.cboInvoiceItem.Properties.Appearance.Options.UseFont = true;
            this.cboInvoiceItem.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboInvoiceItem.Properties.Buttons"))))});
            this.cboInvoiceItem.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboInvoiceItem.Properties.Columns"), resources.GetString("cboInvoiceItem.Properties.Columns1")),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboInvoiceItem.Properties.Columns2"), resources.GetString("cboInvoiceItem.Properties.Columns3"))});
            this.cboInvoiceItem.Properties.DisplayMember = "INVOICE_ITEM_NAME";
            this.cboInvoiceItem.Properties.NullText = resources.GetString("cboInvoiceItem.Properties.NullText");
            this.cboInvoiceItem.Properties.ValueMember = "INVOICE_ITEM_ID";
            // 
            // DialogCustomersCharge
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomersCharge";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cboTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboInvoiceItem.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Label lblSERVICE;
        private TextBox txtAmount;
        private Label lblAMOUNT_PENALTY;
        private Label lblCURRENCY;
        public ComboBox cboCurrencyId;
        private Label lblCustomerSelected_;
        private Label TOTAL_CUSTOMER;
        private DateTimePicker dtpDueDate;
        private Label lblDUE_DATE;
        private DateTimePicker dtpInvoiceDate;
        private Label lblINVOICE_DATE;
        private TextBox txtTotalAmount;
        private Label lblTOTAL_AMOUNT;
        private Label lblTAX;
        private Panel panel1;
        private GroupBox groupBox2;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private DevExpress.XtraEditors.LookUpEdit cboTax;
        private DevExpress.XtraEditors.LookUpEdit cboInvoiceItem;
    }
}