﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPrice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPrice));
            this.lblPRICE_NAME = new System.Windows.Forms.Label();
            this.txtPriceName = new System.Windows.Forms.TextBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.dgvPriceDetail = new System.Windows.Forms.DataGridView();
            this.PRICE_DETAIL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FROM_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TO_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ALLOW_FIXED_AMOUNT = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FIXED_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnADD = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnREMOVE = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblTERM_OF_PRICE = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chkSTANDARD_PRICING = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCAPACITY_PRICE = new System.Windows.Forms.Label();
            this.txtCAPACITY_PRICE = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.picHelp = new System.Windows.Forms.PictureBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPriceDetail)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.picHelp);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.txtCAPACITY_PRICE);
            this.content.Controls.Add(this.lblCAPACITY_PRICE);
            this.content.Controls.Add(this.panel3);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.chkSTANDARD_PRICING);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.lblTERM_OF_PRICE);
            this.content.Controls.Add(this.dgvPriceDetail);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.txtPriceName);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.lblPRICE_NAME);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblPRICE_NAME, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.txtPriceName, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.dgvPriceDetail, 0);
            this.content.Controls.SetChildIndex(this.lblTERM_OF_PRICE, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.chkSTANDARD_PRICING, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.panel3, 0);
            this.content.Controls.SetChildIndex(this.lblCAPACITY_PRICE, 0);
            this.content.Controls.SetChildIndex(this.txtCAPACITY_PRICE, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.picHelp, 0);
            // 
            // lblPRICE_NAME
            // 
            resources.ApplyResources(this.lblPRICE_NAME, "lblPRICE_NAME");
            this.lblPRICE_NAME.Name = "lblPRICE_NAME";
            // 
            // txtPriceName
            // 
            resources.ApplyResources(this.txtPriceName, "txtPriceName");
            this.txtPriceName.Name = "txtPriceName";
            this.txtPriceName.Enter += new System.EventHandler(this.txtPriceName_Enter);
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // dgvPriceDetail
            // 
            this.dgvPriceDetail.AllowUserToAddRows = false;
            this.dgvPriceDetail.AllowUserToDeleteRows = false;
            this.dgvPriceDetail.AllowUserToResizeColumns = false;
            this.dgvPriceDetail.AllowUserToResizeRows = false;
            this.dgvPriceDetail.BackgroundColor = System.Drawing.Color.White;
            this.dgvPriceDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPriceDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPriceDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PRICE_DETAIL_ID,
            this.FROM_USAGE,
            this.TO_USAGE,
            this.PRICE,
            this.ALLOW_FIXED_AMOUNT,
            this.FIXED_AMOUNT,
            this.STATUS});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPriceDetail.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvPriceDetail.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvPriceDetail.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvPriceDetail, "dgvPriceDetail");
            this.dgvPriceDetail.MultiSelect = false;
            this.dgvPriceDetail.Name = "dgvPriceDetail";
            this.dgvPriceDetail.RowHeadersVisible = false;
            this.dgvPriceDetail.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvPriceDetail_CellBeginEdit);
            this.dgvPriceDetail.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPriceDetail_CellContentClick);
            this.dgvPriceDetail.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPriceDetail_CellEnter);
            this.dgvPriceDetail.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPriceDetail_CellValueChanged);
            this.dgvPriceDetail.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvPriceDetail_DataError);
            // 
            // PRICE_DETAIL_ID
            // 
            this.PRICE_DETAIL_ID.DataPropertyName = "PRICE_DETAIL_ID";
            resources.ApplyResources(this.PRICE_DETAIL_ID, "PRICE_DETAIL_ID");
            this.PRICE_DETAIL_ID.Name = "PRICE_DETAIL_ID";
            // 
            // FROM_USAGE
            // 
            this.FROM_USAGE.DataPropertyName = "START_USAGE";
            resources.ApplyResources(this.FROM_USAGE, "FROM_USAGE");
            this.FROM_USAGE.Name = "FROM_USAGE";
            this.FROM_USAGE.ReadOnly = true;
            this.FROM_USAGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TO_USAGE
            // 
            this.TO_USAGE.DataPropertyName = "END_USAGE";
            resources.ApplyResources(this.TO_USAGE, "TO_USAGE");
            this.TO_USAGE.Name = "TO_USAGE";
            this.TO_USAGE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // PRICE
            // 
            this.PRICE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PRICE.DataPropertyName = "PRICE";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,##0.#####";
            dataGridViewCellStyle4.NullValue = null;
            this.PRICE.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.PRICE, "PRICE");
            this.PRICE.Name = "PRICE";
            this.PRICE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ALLOW_FIXED_AMOUNT
            // 
            this.ALLOW_FIXED_AMOUNT.DataPropertyName = "ALLOW_FIXED_AMOUNT";
            resources.ApplyResources(this.ALLOW_FIXED_AMOUNT, "ALLOW_FIXED_AMOUNT");
            this.ALLOW_FIXED_AMOUNT.Name = "ALLOW_FIXED_AMOUNT";
            this.ALLOW_FIXED_AMOUNT.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // FIXED_AMOUNT
            // 
            this.FIXED_AMOUNT.DataPropertyName = "FIXED_AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.####";
            this.FIXED_AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.FIXED_AMOUNT, "FIXED_AMOUNT");
            this.FIXED_AMOUNT.Name = "FIXED_AMOUNT";
            this.FIXED_AMOUNT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS";
            resources.ApplyResources(this.STATUS, "STATUS");
            this.STATUS.Name = "STATUS";
            this.STATUS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD.Name = "btnADD";
            this.btnADD.TabStop = true;
            this.btnADD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAdd_LinkClicked);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.TabStop = true;
            this.btnREMOVE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRemove_LinkClicked);
            // 
            // lblTERM_OF_PRICE
            // 
            resources.ApplyResources(this.lblTERM_OF_PRICE, "lblTERM_OF_PRICE");
            this.lblTERM_OF_PRICE.Name = "lblTERM_OF_PRICE";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // chkSTANDARD_PRICING
            // 
            resources.ApplyResources(this.chkSTANDARD_PRICING, "chkSTANDARD_PRICING");
            this.chkSTANDARD_PRICING.Name = "chkSTANDARD_PRICING";
            this.chkSTANDARD_PRICING.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnADD);
            this.panel2.Controls.Add(this.btnREMOVE);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnCHANGE_LOG);
            this.panel3.Controls.Add(this.btnCLOSE);
            this.panel3.Controls.Add(this.btnOK);
            this.panel3.Controls.Add(this.panel1);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblCAPACITY_PRICE
            // 
            resources.ApplyResources(this.lblCAPACITY_PRICE, "lblCAPACITY_PRICE");
            this.lblCAPACITY_PRICE.Name = "lblCAPACITY_PRICE";
            // 
            // txtCAPACITY_PRICE
            // 
            resources.ApplyResources(this.txtCAPACITY_PRICE, "txtCAPACITY_PRICE");
            this.txtCAPACITY_PRICE.Name = "txtCAPACITY_PRICE";
            this.txtCAPACITY_PRICE.Enter += new System.EventHandler(this.txtCAPACITY_PRICE_Enter);
            this.txtCAPACITY_PRICE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDecimalOnly);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // picHelp
            // 
            this.picHelp.Cursor = System.Windows.Forms.Cursors.Default;
            this.picHelp.Image = global::EPower.Properties.Resources.question_mark;
            resources.ApplyResources(this.picHelp, "picHelp");
            this.picHelp.Name = "picHelp";
            this.picHelp.TabStop = false;
            this.picHelp.MouseEnter += new System.EventHandler(this.picHelp_MouseEnter);
            // 
            // DialogPrice
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPrice";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPriceDetail)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picHelp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblCURRENCY;
        private TextBox txtPriceName;
        private Label lblPRICE_NAME;
        private ComboBox cboCurrency;
        private DataGridView dgvPriceDetail;
        private ExLinkLabel btnADD;
        private ExLinkLabel btnREMOVE;
        private Label lblTERM_OF_PRICE;
        private Label label3;
        private Label label9;
        private CheckBox chkSTANDARD_PRICING;
        private Panel panel2;
        private DataGridViewTextBoxColumn PRICE_DETAIL_ID;
        private DataGridViewTextBoxColumn FROM_USAGE;
        private DataGridViewTextBoxColumn TO_USAGE;
        private DataGridViewTextBoxColumn PRICE;
        private DataGridViewCheckBoxColumn ALLOW_FIXED_AMOUNT;
        private DataGridViewTextBoxColumn FIXED_AMOUNT;
        private DataGridViewTextBoxColumn STATUS;
        private Label label2;
        private TextBox txtCAPACITY_PRICE;
        private Label lblCAPACITY_PRICE;
        private Panel panel3;
        private ExButton btnCHANGE_LOG;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel1;
        private PictureBox picHelp;
    }
}