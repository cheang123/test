﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogInvoiceItemType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSERVICE_TYPE = new System.Windows.Forms.Label();
            this.txtInvoiceItemType = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtInvoiceItemType);
            this.content.Controls.Add(this.lblSERVICE_TYPE);
            this.content.Size = new System.Drawing.Size(348, 112);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.lblSERVICE_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtInvoiceItemType, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            // 
            // lblSERVICE_TYPE
            // 
            this.lblSERVICE_TYPE.AutoSize = true;
            this.lblSERVICE_TYPE.Location = new System.Drawing.Point(16, 21);
            this.lblSERVICE_TYPE.Name = "lblSERVICE_TYPE";
            this.lblSERVICE_TYPE.Size = new System.Drawing.Size(40, 19);
            this.lblSERVICE_TYPE.TabIndex = 6;
            this.lblSERVICE_TYPE.Text = "ឈ្មោះ";
            // 
            // txtInvoiceItemType
            // 
            this.txtInvoiceItemType.Location = new System.Drawing.Point(123, 17);
            this.txtInvoiceItemType.MaxLength = 50;
            this.txtInvoiceItemType.Name = "txtInvoiceItemType";
            this.txtInvoiceItemType.Size = new System.Drawing.Size(209, 27);
            this.txtInvoiceItemType.TabIndex = 0;
            this.txtInvoiceItemType.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(0, 78);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(348, 1);
            this.panel1.TabIndex = 21;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(267, 83);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(75, 23);
            this.btnCLOSE.TabIndex = 2;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOK.Location = new System.Drawing.Point(188, 83);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(106, 20);
            this.label9.Margin = new System.Windows.Forms.Padding(1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 20);
            this.label9.TabIndex = 71;
            this.label9.Text = "*";
            // 
            // btnCHANGE_LOG
            // 
            this.btnCHANGE_LOG.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCHANGE_LOG.Location = new System.Drawing.Point(4, 83);
            this.btnCHANGE_LOG.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.Size = new System.Drawing.Size(75, 23);
            this.btnCHANGE_LOG.TabIndex = 3;
            this.btnCHANGE_LOG.Text = "ប្រវត្តិកែប្រែ";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // DialogInvoiceItemType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 135);
            this.Name = "DialogInvoiceItemType";
            this.Text = "ប្រភេទសេវាកម្ម";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtInvoiceItemType;
        private Label lblSERVICE_TYPE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label9;
        private ExButton btnCHANGE_LOG;
    }
}