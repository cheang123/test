﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.BankPayment
{
    public partial class DialogPendingPayment : ExDialog
    {
        public DialogPendingPayment()
        {
            InitializeComponent();

            UIHelper.DataGridViewProperties(dgv);
            this.dgv.DefaultCellStyle.SelectionForeColor =
            this.dgv.DefaultCellStyle.ForeColor = Color.Red;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            bind();
        }

        private void bind()
        {
            DataTable data = new DataTable();
            Runner.RunNewThread(delegate ()
            {
                data = (from p in DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs
                         join c in DBDataContext.Db.TBL_CUSTOMERs on p.CUSTOMER_ID equals c.CUSTOMER_ID into JoinCust
                         from dt in JoinCust.DefaultIfEmpty()
                         where p.RESULT == (int)BankPaymentImportResult.RecordSuccess
                             && (p.CUSTOMER_CODE + dt.CUSTOMER_CODE + dt.LAST_NAME_KH + dt.FIRST_NAME_KH).ToUpper().Contains(this.txtSearch.Text.ToUpper())
                             && p.PAID_AMOUNT != p.PAY_AMOUNT
                             && p.IS_VOID == false
                             && p.BANK != "NA"
                         orderby p.CURRENCY, p.PAY_DATE
                         select new
                         {
                             p.BANK_PAYMENT_DETAIL_ID,
                             p.CUSTOMER_CODE,
                             CUSTOMER_NAME = dt.LAST_NAME_KH + " " + dt.FIRST_NAME_KH,
                             p.PAY_AMOUNT,
                             p.PAY_DATE,
                             p.CURRENCY,
                             p.PAID_AMOUNT,
                             p.PAYMENT_TYPE,
                             p.PAYMENT_METHOD,
                             p.CASHIER,
                             p.BRANCH,
                             BANK = p.BANK_ALIAS
                         })._ToDataTable();
            });
            dgv.DataSource = data;
        }

        private void DialogBankPaymentHistoryDetail_Load(object sender, EventArgs e)
        {
            bind();
        }

        private void btnResettlement_Click(object sender, EventArgs e)
        {
            TBL_USER_CASH_DRAWER userCashDrawer = null;
            Runner.RunNewThread(() => { userCashDrawer = Login.CurrentCashDrawer; });
            if (Runner.Instance.Error != null)
            {
                return;
            }
            if (userCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER, string.Empty) == DialogResult.Yes)
                {
                    //new DialogOpenCashDrawer().ShowDialog();
                    var diag = new DialogOpenCashDrawer();
                    diag.isBank = true;
                    diag.ShowDialog();
                }

                if (Login.CurrentCashDrawer == null)
                    return;
            }
            TBL_CASH_DRAWER cashDrawer = new TBL_CASH_DRAWER();
            Runner.RunNewThread(() => { cashDrawer = DBDataContext.Db.TBL_CASH_DRAWERs.FirstOrDefault(x => x.CASH_DRAWER_ID == Login.CurrentCashDrawer.CASH_DRAWER_ID); });
            if (Runner.Instance.Error != null)
            {
                return;
            }
            if (!cashDrawer.IS_BANK)
            {
                if (MsgBox.ShowQuestion(Resources.MS_THIS_CASH_DRAWER_IS_NOT_FOR_BANK, Resources.WARNING) != DialogResult.Yes)
                    return;
            }
            var paymentOperation = new PaymentOperation();
            paymentOperation.UpdateLabel = (string val) =>
            {
                Application.DoEvents();
                Runner.Instance.Text = val;
            };
            var lastMessage = "";
            paymentOperation.ShowMessage = (string val) =>
            {
                //MsgBox.ShowInformation(val);
                lastMessage = val;
            };
            bool success = false;
            Runner.RunNewThread(delegate ()
            {
                paymentOperation.ClearPayment();
                success = true;
                this.DialogResult = DialogResult.OK;
            });
            var error = Runner.Instance.Error;
            if (lastMessage != "")
            {
                MsgBox.ShowInformation(lastMessage);
            }
            bind();
        }

        private void btnADD_TO_PREPAYMENT_Click(object sender, EventArgs e)
        {
            if (Login.CurrentCashDrawer == null)
            {
                if (MsgBox.ShowQuestion(Properties.Resources.MS_YOU_MUST_OPEN_CASH_DRAWER, string.Empty) == DialogResult.Yes)
                {
                    var diag = new DialogOpenCashDrawer();
                    diag.isBank = true;
                    diag.ShowDialog();
                }

                if (Login.CurrentCashDrawer == null)
                {
                    return;
                }
            }
            if (MsgBox.ShowQuestion(Resources.MSG_CONFIRM_ADD_TO_PREPAYMENT, string.Empty) == DialogResult.Yes)
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    Runner.RunNewThread(delegate ()
                    {
                        DBDataContext.Db = null;
                        var dNow = DBDataContext.Db.GetSystemDate();

                        var db = (from p in DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs
                                  where p.RESULT == (int)BankPaymentImportResult.RecordSuccess
                                      && p.PAID_AMOUNT != p.PAY_AMOUNT
                                      && p.IS_VOID == false
                                      && p.BANK != "NA"
                                  //          orderby p.CURRENCY, p.PAY_DATE
                                  select p).ToList();
                        db = db ?? new List<TBL_BANK_PAYMENT_DETAIL>();

                        var cusPrepay = new PrepaymentLogic().getPrepayCustomer();


                        List<TBL_CUS_PREPAYMENT> objPrepay = new List<TBL_CUS_PREPAYMENT>();
                        foreach (var item in db)
                        {
                            DateTime now = DBDataContext.Db.GetSystemDate();
                            var amount = item?.PAY_AMOUNT ?? 0 - item?.PAID_AMOUNT ?? 0;
                            //var balance = cusPrepay.FirstOrDefault(x => x.CUSTOMER_ID == item.CUSTOMER_ID && x.CURRENCY_ID == item.CURRENCY_ID) == null ? 0 : cusPrepay.FirstOrDefault(x => x.CUSTOMER_ID == item.CUSTOMER_ID && x.CURRENCY_ID == item.CURRENCY_ID).BALANCE;
                            var balance = new PrepaymentLogic().getLastestPrepayCus(item.CUSTOMER_ID, item.CURRENCY_ID)?.BALANCE ?? 0m;

                            var obj = new PrepaymentLogic().AddCustomerPrepayment(now, item.CUSTOMER_ID, amount, balance + amount, item.CURRENCY_ID, Method.GetNextSequence(Sequence.Receipt, true), "(BANK)", PrepaymentAction.AddPrepayment, 0, item.BANK_PAYMENT_DETAIL_ID);
                            obj.PAYMENT_ACCOUNT_ID = 73;//CASH ON BANK
                            obj.CREATE_BY = item.BANK_ALIAS;
                            objPrepay.Add(obj);
                            item.PAID_AMOUNT = item.PAY_AMOUNT;
                            DBDataContext.Db.SubmitChanges();
                        }
                        DBDataContext.Db.BulkCopy(objPrepay);
                        DBDataContext.Db.SubmitChanges();

                    });
                    tran.Complete();
                }
            }

            bind();
        }

        private void content_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (btnADD_TO_PREPAYMENT.Visible)
                {
                    btnADD_TO_PREPAYMENT.Visible = false;
                }
                else
                {
                    btnADD_TO_PREPAYMENT.Visible = true;
                }
            }
        }

        private void btnREPORT_Click(object sender, EventArgs e)
        {

        }
    }
}
