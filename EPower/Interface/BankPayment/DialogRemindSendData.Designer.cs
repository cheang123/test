﻿using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface.BankPayment
{
    partial class DialogRemindSendData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblREQUIRED_INTERNET_BEFORE_SEND = new System.Windows.Forms.Label();
            this.chkAUTO_SEND = new System.Windows.Forms.CheckBox();
            this._btnSEND = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this._btnSEND);
            this.content.Controls.Add(this.chkAUTO_SEND);
            this.content.Controls.Add(this.lblREQUIRED_INTERNET_BEFORE_SEND);
            this.content.Size = new System.Drawing.Size(434, 144);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.lblREQUIRED_INTERNET_BEFORE_SEND, 0);
            this.content.Controls.SetChildIndex(this.chkAUTO_SEND, 0);
            this.content.Controls.SetChildIndex(this._btnSEND, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            // 
            // lblREQUIRED_INTERNET_BEFORE_SEND
            // 
            this.lblREQUIRED_INTERNET_BEFORE_SEND.Font = new System.Drawing.Font("Khmer OS", 14F, System.Drawing.FontStyle.Bold);
            this.lblREQUIRED_INTERNET_BEFORE_SEND.Location = new System.Drawing.Point(10, 19);
            this.lblREQUIRED_INTERNET_BEFORE_SEND.Name = "lblREQUIRED_INTERNET_BEFORE_SEND";
            this.lblREQUIRED_INTERNET_BEFORE_SEND.Size = new System.Drawing.Size(406, 41);
            this.lblREQUIRED_INTERNET_BEFORE_SEND.TabIndex = 6;
            this.lblREQUIRED_INTERNET_BEFORE_SEND.Text = "សូមភ្ជាប់អ៊ីនធើណេត  ដើម្បីបញ្ជូនទិន្នន័យទៅកាន់ធនាគារ !";
            // 
            // chkAUTO_SEND
            // 
            this.chkAUTO_SEND.Checked = true;
            this.chkAUTO_SEND.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAUTO_SEND.Font = new System.Drawing.Font("Khmer OS", 8.25F);
            this.chkAUTO_SEND.Location = new System.Drawing.Point(12, 107);
            this.chkAUTO_SEND.Name = "chkAUTO_SEND";
            this.chkAUTO_SEND.Size = new System.Drawing.Size(163, 34);
            this.chkAUTO_SEND.TabIndex = 7;
            this.chkAUTO_SEND.Text = "បញ្ជូនដោយស្វ័យប្រវត្តិ";
            this.chkAUTO_SEND.UseVisualStyleBackColor = true;
            this.chkAUTO_SEND.CheckedChanged += new System.EventHandler(this.chkAutoSend_CheckedChanged);
            // 
            // _btnSEND
            // 
            this._btnSEND.Font = new System.Drawing.Font("Khmer OS", 8.5F, System.Drawing.FontStyle.Bold);
            this._btnSEND.Location = new System.Drawing.Point(272, 104);
            this._btnSEND.Name = "_btnSEND";
            this._btnSEND.Size = new System.Drawing.Size(76, 34);
            this._btnSEND.TabIndex = 8;
            this._btnSEND.Text = "បញ្ជូន";
            this._btnSEND.UseVisualStyleBackColor = true;
            this._btnSEND.Click += new System.EventHandler(this.btnSendNow_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Khmer OS", 8.25F);
            this.btnClose.Location = new System.Drawing.Point(351, 104);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 34);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "បិទ";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // DialogRemindSendData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 167);
            this.Name = "DialogRemindSendData";
            this.Text = "បង់ប្រាក់តាមធនាគារ";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private CheckBox chkAUTO_SEND;
        private Label lblREQUIRED_INTERNET_BEFORE_SEND;
        private Button btnClose;
        private Button _btnSEND;
    }
}