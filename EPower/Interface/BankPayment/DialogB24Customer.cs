﻿using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.BankPayment
{
    public partial class DialogB24Customer : ExDialog
    {
        public DialogB24Customer()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            load();
        }

        private void load()
        {
            var db = from b in DBDataContext.Db.TBL_B24_CUSTOMERs
                     join c in DBDataContext.Db.TBL_CUSTOMERs on b.CUSTOMER_ID equals c.CUSTOMER_ID
                     join a in DBDataContext.Db.TBL_AREAs on c.AREA_ID equals a.AREA_ID
                     join p in DBDataContext.Db.TBL_PRICEs on c.PRICE_ID equals p.PRICE_ID
                     join cu in DBDataContext.Db.TLKP_CURRENCies on p.CURRENCY_ID equals cu.CURRENCY_ID
                     where b.IS_ACTIVE && (c.LAST_NAME_KH + " " + c.FIRST_NAME_KH + " " + a.AREA_NAME + " " + p.PRICE_NAME).ToUpper().Contains(txtSearch.Text.ToUpper())
                     select new
                     {
                         b.B24_CUSTOMER_ID,
                         c.CUSTOMER_CODE,
                         CUSTOMER_NAME = c.LAST_NAME_KH + " " + c.FIRST_NAME_KH,
                         a.AREA_NAME,
                         PRICE_NAME = p.PRICE_NAME + "(" + cu.CURRENCY_SING + ")"
                     };
            dgv.DataSource = db;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnREMOVE_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgv.SelectedRows.Count == 0)
            {
                return;
            }

            if (MsgBox.ShowQuestion(Resources.MSQ_REMOVE_CUSTOMER, this.Text) == DialogResult.No)
            {
                return;
            }

            int id = (int)dgv.SelectedRows[0].Cells[B24_CUSTOMER_ID.Name].Value;
            DateTime dt = DBDataContext.Db.GetSystemDate();
            TBL_B24_CUSTOMER objOldB24Customer = DBDataContext.Db.TBL_B24_CUSTOMERs.FirstOrDefault(x => x.B24_CUSTOMER_ID == id);
            TBL_B24_CUSTOMER objNewB24Customer = new TBL_B24_CUSTOMER();
            objOldB24Customer._CopyTo(objNewB24Customer);
            objNewB24Customer.IS_ACTIVE = false;
            objNewB24Customer.CLOSED_DATE = dt;
            objNewB24Customer.ROW_DATE = dt;
            DBDataContext.Db.Update(objOldB24Customer, objNewB24Customer);
            load();
        }

        private void btnADD_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogCustomerSearch diag = new DialogCustomerSearch("", DialogCustomerSearch.PowerType.Postpaid);
            if (diag.ShowDialog() != DialogResult.OK) return;
            TBL_CUSTOMER objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == diag.CustomerID);
            TBL_B24_CUSTOMER objB24Customer = DBDataContext.Db.TBL_B24_CUSTOMERs.FirstOrDefault(x => x.IS_ACTIVE && x.CUSTOMER_ID == diag.CustomerID);
            if (objB24Customer != null)
            {
                MsgBox.ShowInformation(string.Format(Resources.MS_CUSTOMER_ALREADY_EXISTS_IN_MERGE_CUSTOMER, string.Concat(objCustomer.LAST_NAME_KH, " ", objCustomer.FIRST_NAME_KH)), this.Text);
                return;
            }
            DateTime dt = DBDataContext.Db.GetSystemDate();
            DBDataContext.Db.Insert(new TBL_B24_CUSTOMER()
            {
                CUSTOMER_ID = objCustomer.CUSTOMER_ID,
                CREATE_ON = dt,
                CLOSED_DATE = UIHelper._DefaultDate,
                ROW_DATE = dt,
                CREATE_BY = ""
            });
            load();
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            load();
        }
    }
}
