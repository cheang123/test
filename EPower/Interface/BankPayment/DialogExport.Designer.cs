﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class DialogExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogExport));
            this.btnOK = new SoftTech.Component.ExButton();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.lblFILE_NAME = new System.Windows.Forms.Label();
            this.btnBrowse_ = new System.Windows.Forms.Button();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboExportType = new System.Windows.Forms.ComboBox();
            this.lblTYPE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboExportType);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnBrowse_);
            this.content.Controls.Add(this.lblTYPE);
            this.content.Controls.Add(this.lblFILE_NAME);
            this.content.Controls.Add(this.txtFileName);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.txtFileName, 0);
            this.content.Controls.SetChildIndex(this.lblFILE_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblTYPE, 0);
            this.content.Controls.SetChildIndex(this.btnBrowse_, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.cboExportType, 0);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // txtFileName
            // 
            this.txtFileName.BackColor = System.Drawing.Color.LightYellow;
            resources.ApplyResources(this.txtFileName, "txtFileName");
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.ReadOnly = true;
            // 
            // lblFILE_NAME
            // 
            resources.ApplyResources(this.lblFILE_NAME, "lblFILE_NAME");
            this.lblFILE_NAME.Name = "lblFILE_NAME";
            // 
            // btnBrowse_
            // 
            resources.ApplyResources(this.btnBrowse_, "btnBrowse_");
            this.btnBrowse_.Name = "btnBrowse_";
            this.btnBrowse_.UseVisualStyleBackColor = true;
            this.btnBrowse_.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // cboExportType
            // 
            this.cboExportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboExportType.FormattingEnabled = true;
            resources.ApplyResources(this.cboExportType, "cboExportType");
            this.cboExportType.Name = "cboExportType";
            // 
            // lblTYPE
            // 
            resources.ApplyResources(this.lblTYPE, "lblTYPE");
            this.lblTYPE.Name = "lblTYPE";
            // 
            // DialogExport
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogExport";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnOK;
        private Label lblFILE_NAME;
        private TextBox txtFileName;
        private ExButton btnCLOSE;
        private Button btnBrowse_;
        private Panel panel2;
        private ComboBox cboExportType;
        private Label lblTYPE;
    }
}