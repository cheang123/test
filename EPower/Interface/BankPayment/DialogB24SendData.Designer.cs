﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class DialogB24SendData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogB24SendData));
            this.grpDATA = new System.Windows.Forms.GroupBox();
            this.btnB24_CUSTOMER = new System.Windows.Forms.Button();
            this.chkRESEND_ALL_DATA = new System.Windows.Forms.CheckBox();
            this.btnSEND_DATA = new System.Windows.Forms.Button();
            this.lblInvoice = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grpDATA.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.grpDATA);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.grpDATA, 0);
            // 
            // grpDATA
            // 
            this.grpDATA.Controls.Add(this.btnB24_CUSTOMER);
            this.grpDATA.Controls.Add(this.chkRESEND_ALL_DATA);
            this.grpDATA.Controls.Add(this.btnSEND_DATA);
            this.grpDATA.Controls.Add(this.lblInvoice);
            resources.ApplyResources(this.grpDATA, "grpDATA");
            this.grpDATA.Name = "grpDATA";
            this.grpDATA.TabStop = false;
            // 
            // btnB24_CUSTOMER
            // 
            resources.ApplyResources(this.btnB24_CUSTOMER, "btnB24_CUSTOMER");
            this.btnB24_CUSTOMER.Name = "btnB24_CUSTOMER";
            this.btnB24_CUSTOMER.UseVisualStyleBackColor = true;
            this.btnB24_CUSTOMER.Click += new System.EventHandler(this.btnB24_CUSTOMER_Click);
            // 
            // chkRESEND_ALL_DATA
            // 
            resources.ApplyResources(this.chkRESEND_ALL_DATA, "chkRESEND_ALL_DATA");
            this.chkRESEND_ALL_DATA.Name = "chkRESEND_ALL_DATA";
            this.chkRESEND_ALL_DATA.UseVisualStyleBackColor = true;
            // 
            // btnSEND_DATA
            // 
            resources.ApplyResources(this.btnSEND_DATA, "btnSEND_DATA");
            this.btnSEND_DATA.Name = "btnSEND_DATA";
            this.btnSEND_DATA.UseVisualStyleBackColor = true;
            this.btnSEND_DATA.Click += new System.EventHandler(this.btnSEND_DATA_Click);
            // 
            // lblInvoice
            // 
            resources.ApplyResources(this.lblInvoice, "lblInvoice");
            this.lblInvoice.ForeColor = System.Drawing.Color.Gray;
            this.lblInvoice.Name = "lblInvoice";
            // 
            // DialogB24SendData
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogB24SendData";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grpDATA.ResumeLayout(false);
            this.grpDATA.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox grpDATA;
        private Button btnB24_CUSTOMER;
        private CheckBox chkRESEND_ALL_DATA;
        private Button btnSEND_DATA;
        private Label lblInvoice;
    }
}