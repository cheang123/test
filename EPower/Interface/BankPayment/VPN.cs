﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech.Helper;

namespace EPower.Interface.BankPayment
{
    class VPN: VpnClient
    {
        const string VPN_KEY = "VPN_Nétw0®k";
        const string PHONE_BOOK_FILE = "GatewayVPN.pbk"; 
        static VPN mVpn = null; 
        public static VPN Instance
        {
            get
            {
                if (mVpn==null)
                {
                    Crypto mSecurity = new Crypto();
                    mVpn = new VPN();
                    mVpn.VpnName = "GatewayVPN";
                    mVpn.UserName = Crypto.Decrypt(Method.Utilities[Utility.BANK_PAYMENT_VPN_USER], VPN_KEY);
                    mVpn.Password = Crypto.Decrypt(Method.Utilities[Utility.BANK_PAYMENT_VPN_PASSWORD], VPN_KEY); 
                }
                return mVpn;
            }
        }

        private VPN(){}
         
        public override void Connect()
        { 
            this.PhoneBookFile = ExtractResource.Extract(Resources.GatewayVPN, PHONE_BOOK_FILE); 
            base.Connect(); 
        }

        public override void Disconnect()
        { 
            base.Disconnect();
            ExtractResource.RemoveResouce(PHONE_BOOK_FILE);
        }
          
    }
}
