﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.BankPayment
{
    public partial class DialogHistoryDetail : ExDialog
    {
        int BankPaymentId = 0;
        public DialogHistoryDetail(int bankPaymentId)
        {
            InitializeComponent();
            BankPaymentId = bankPaymentId;

            UIHelper.SetDataSourceToComboBox(this.cboResult, DBDataContext.Db.TBL_BANK_PAYMENT_RESULTs,lblRESULT.Text);  

            UIHelper.DataGridViewProperties(dgv);
            this.dgvSummary.DefaultCellStyle.SelectionBackColor = this.dgvSummary.DefaultCellStyle.BackColor;
            this.dgvSummary.DefaultCellStyle.SelectionForeColor = this.dgvSummary.DefaultCellStyle.ForeColor;

            this.dataGridView1.DefaultCellStyle.SelectionBackColor = this.dataGridView1.DefaultCellStyle.BackColor;
            this.dataGridView1.DefaultCellStyle.SelectionForeColor = this.dataGridView1.DefaultCellStyle.ForeColor;
            
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        } 

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            quickSearch();
        }

        private void quickSearch()
        {
            int result = (int)(this.cboResult.SelectedValue ?? 0);
            var db = (from d in DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs
                      join r in DBDataContext.Db.TBL_BANK_PAYMENT_RESULTs on d.RESULT equals r.RESULT_ID
                      join c in DBDataContext.Db.TBL_CUSTOMERs on d.CUSTOMER_ID equals c.CUSTOMER_ID into JoinCust
                      from dt in JoinCust.DefaultIfEmpty()
                      where d.BANK_PAYMENT_ID == BankPaymentId
                              && (d.CUSTOMER_CODE).ToUpper().Contains(txtSearch.Text.ToUpper())
                              && (result == 0 || d.RESULT == result)
                              && !d.IS_VOID
                      select new
                      {
                          d.CUSTOMER_CODE,
                          CUSTOMER_NAME = dt.LAST_NAME_KH + " " + dt.FIRST_NAME_KH,
                          d.PAY_AMOUNT,
                          d.PAY_DATE,
                          d.CURRENCY,
                          d.BRANCH,
                          BANK = d.BANK_ALIAS,
                          d.PAID_AMOUNT,
                          d.PAYMENT_TYPE,
                          d.PAYMENT_METHOD,
                          d.CASHIER,
                          r.RESULT_ID,
                          r.RESULT_NAME,
                          d.RESULT_NOTE
                      });
            dgv.DataSource = db;
            this.dgvSummary.DataSource = db.GroupBy(x => x.CURRENCY).Select(x => new
            {
                CURRENCY_CODE = x.Key,
                TOTAL_RECORD = x.Count(),
                TOTAL_PAID = x.Sum(r => r.PAID_AMOUNT),
                TOTAL_PAY = x.Sum(r => r.PAY_AMOUNT)
            })._ToDataTable();
            this.dataGridView1.DataSource = db.GroupBy(x => x.RESULT_NAME).Select(x => new
            {
                RESULT = x.Key,
                TOTAL = x.Count()
            });
            markColor();
        }

        private void DialogBankPaymentHistoryDetail_Load(object sender, EventArgs e)
        {
            txtSearch_QuickSearch(null, null);
        }
        private void markColor()
        {
            this.dgv.Refresh();
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                var result = (int)row.Cells[this.RESULT_ID.Name].Value;
                var color = Color.Red;
                if (result == 1)
                {  
                    color = Color.Red;
                }else if (result==2){
                    color = Color.Blue;
                }else if (result == 3){
                    color = Color.Green;
                }
                row.DefaultCellStyle.SelectionForeColor
                    = row.DefaultCellStyle.ForeColor
                    = color;
            }
        }

        private void dgv_Sorted(object sender, EventArgs e)
        {
            markColor();
        }

        private void cboResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            quickSearch();
        }
    }
}
