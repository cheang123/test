﻿using System;
using System.IO;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    public partial class DialogUserAgreement : ExDialog
    {
        public DialogUserAgreement()
        {
            InitializeComponent();
            this.txtUserAgreeement.LoadFile(Path.Combine(Application.StartupPath, "Template/BPUA.dat")); 
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.chkREMEMBER_MY_AGREE.Checked)
            {
                Settings.Default.REMEMBER_BANK_PAYMENT_USER_AGREEMENT = true;
                Settings.Default.Save();
            }
            this.DialogResult = DialogResult.OK; 
        } 
    }
}
