﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class DialogImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogImport));
            this.btnOK = new SoftTech.Component.ExButton();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.lblFILE_NAME = new System.Windows.Forms.Label();
            this.btnBrowse_ = new System.Windows.Forms.Button();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblRESULT = new System.Windows.Forms.Label();
            this.lblCUSTOMER_ERROR = new System.Windows.Forms.Label();
            this.lblCUSTOMER_SUCCESS = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NOT_FOUND = new System.Windows.Forms.Label();
            this.lblCUSTOMER_PAID = new System.Windows.Forms.Label();
            this.txtSuccess_ = new System.Windows.Forms.Label();
            this.txtNotFound_ = new System.Windows.Forms.Label();
            this.txtPaid_ = new System.Windows.Forms.Label();
            this.btnSHOW_DETAIL = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblTOTAL = new System.Windows.Forms.Label();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.txtPaidAmount_ = new System.Windows.Forms.Label();
            this.txtNotFoundAmount_ = new System.Windows.Forms.Label();
            this.txtErrorAmount_ = new System.Windows.Forms.Label();
            this.txtSuccessAmount_ = new System.Windows.Forms.Label();
            this.txtErrors_ = new System.Windows.Forms.Label();
            this.cboImportType = new System.Windows.Forms.ComboBox();
            this.lblTYPE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboImportType);
            this.content.Controls.Add(this.lblTYPE);
            this.content.Controls.Add(this.txtErrors_);
            this.content.Controls.Add(this.txtPaidAmount_);
            this.content.Controls.Add(this.txtNotFoundAmount_);
            this.content.Controls.Add(this.txtErrorAmount_);
            this.content.Controls.Add(this.txtSuccessAmount_);
            this.content.Controls.Add(this.lblAMOUNT);
            this.content.Controls.Add(this.lblTOTAL);
            this.content.Controls.Add(this.btnSHOW_DETAIL);
            this.content.Controls.Add(this.txtPaid_);
            this.content.Controls.Add(this.txtNotFound_);
            this.content.Controls.Add(this.txtSuccess_);
            this.content.Controls.Add(this.lblCUSTOMER_PAID);
            this.content.Controls.Add(this.lblCUSTOMER_NOT_FOUND);
            this.content.Controls.Add(this.lblCUSTOMER_SUCCESS);
            this.content.Controls.Add(this.lblCUSTOMER_ERROR);
            this.content.Controls.Add(this.lblRESULT);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnBrowse_);
            this.content.Controls.Add(this.lblFILE_NAME);
            this.content.Controls.Add(this.txtFileName);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.txtFileName, 0);
            this.content.Controls.SetChildIndex(this.lblFILE_NAME, 0);
            this.content.Controls.SetChildIndex(this.btnBrowse_, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.lblRESULT, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_ERROR, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_SUCCESS, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NOT_FOUND, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_PAID, 0);
            this.content.Controls.SetChildIndex(this.txtSuccess_, 0);
            this.content.Controls.SetChildIndex(this.txtNotFound_, 0);
            this.content.Controls.SetChildIndex(this.txtPaid_, 0);
            this.content.Controls.SetChildIndex(this.btnSHOW_DETAIL, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtSuccessAmount_, 0);
            this.content.Controls.SetChildIndex(this.txtErrorAmount_, 0);
            this.content.Controls.SetChildIndex(this.txtNotFoundAmount_, 0);
            this.content.Controls.SetChildIndex(this.txtPaidAmount_, 0);
            this.content.Controls.SetChildIndex(this.txtErrors_, 0);
            this.content.Controls.SetChildIndex(this.lblTYPE, 0);
            this.content.Controls.SetChildIndex(this.cboImportType, 0);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // txtFileName
            // 
            this.txtFileName.BackColor = System.Drawing.Color.LightYellow;
            resources.ApplyResources(this.txtFileName, "txtFileName");
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.ReadOnly = true;
            // 
            // lblFILE_NAME
            // 
            resources.ApplyResources(this.lblFILE_NAME, "lblFILE_NAME");
            this.lblFILE_NAME.Name = "lblFILE_NAME";
            // 
            // btnBrowse_
            // 
            resources.ApplyResources(this.btnBrowse_, "btnBrowse_");
            this.btnBrowse_.Name = "btnBrowse_";
            this.btnBrowse_.UseVisualStyleBackColor = true;
            this.btnBrowse_.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // lblRESULT
            // 
            resources.ApplyResources(this.lblRESULT, "lblRESULT");
            this.lblRESULT.Name = "lblRESULT";
            // 
            // lblCUSTOMER_ERROR
            // 
            resources.ApplyResources(this.lblCUSTOMER_ERROR, "lblCUSTOMER_ERROR");
            this.lblCUSTOMER_ERROR.Name = "lblCUSTOMER_ERROR";
            // 
            // lblCUSTOMER_SUCCESS
            // 
            resources.ApplyResources(this.lblCUSTOMER_SUCCESS, "lblCUSTOMER_SUCCESS");
            this.lblCUSTOMER_SUCCESS.Name = "lblCUSTOMER_SUCCESS";
            // 
            // lblCUSTOMER_NOT_FOUND
            // 
            resources.ApplyResources(this.lblCUSTOMER_NOT_FOUND, "lblCUSTOMER_NOT_FOUND");
            this.lblCUSTOMER_NOT_FOUND.Name = "lblCUSTOMER_NOT_FOUND";
            // 
            // lblCUSTOMER_PAID
            // 
            resources.ApplyResources(this.lblCUSTOMER_PAID, "lblCUSTOMER_PAID");
            this.lblCUSTOMER_PAID.Name = "lblCUSTOMER_PAID";
            // 
            // txtSuccess_
            // 
            this.txtSuccess_.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.txtSuccess_, "txtSuccess_");
            this.txtSuccess_.Name = "txtSuccess_";
            // 
            // txtNotFound_
            // 
            this.txtNotFound_.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.txtNotFound_, "txtNotFound_");
            this.txtNotFound_.Name = "txtNotFound_";
            // 
            // txtPaid_
            // 
            this.txtPaid_.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.txtPaid_, "txtPaid_");
            this.txtPaid_.Name = "txtPaid_";
            // 
            // btnSHOW_DETAIL
            // 
            resources.ApplyResources(this.btnSHOW_DETAIL, "btnSHOW_DETAIL");
            this.btnSHOW_DETAIL.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSHOW_DETAIL.Name = "btnSHOW_DETAIL";
            this.btnSHOW_DETAIL.TabStop = true;
            this.btnSHOW_DETAIL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnShowDetail_LinkClicked);
            // 
            // lblTOTAL
            // 
            resources.ApplyResources(this.lblTOTAL, "lblTOTAL");
            this.lblTOTAL.Name = "lblTOTAL";
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // txtPaidAmount_
            // 
            this.txtPaidAmount_.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.txtPaidAmount_, "txtPaidAmount_");
            this.txtPaidAmount_.Name = "txtPaidAmount_";
            // 
            // txtNotFoundAmount_
            // 
            this.txtNotFoundAmount_.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.txtNotFoundAmount_, "txtNotFoundAmount_");
            this.txtNotFoundAmount_.Name = "txtNotFoundAmount_";
            // 
            // txtErrorAmount_
            // 
            this.txtErrorAmount_.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.txtErrorAmount_, "txtErrorAmount_");
            this.txtErrorAmount_.Name = "txtErrorAmount_";
            // 
            // txtSuccessAmount_
            // 
            this.txtSuccessAmount_.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.txtSuccessAmount_, "txtSuccessAmount_");
            this.txtSuccessAmount_.Name = "txtSuccessAmount_";
            // 
            // txtErrors_
            // 
            this.txtErrors_.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.txtErrors_, "txtErrors_");
            this.txtErrors_.Name = "txtErrors_";
            // 
            // cboImportType
            // 
            this.cboImportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboImportType.FormattingEnabled = true;
            resources.ApplyResources(this.cboImportType, "cboImportType");
            this.cboImportType.Name = "cboImportType";
            this.cboImportType.SelectedIndexChanged += new System.EventHandler(this.cboImportType_SelectedIndexChanged);
            // 
            // lblTYPE
            // 
            resources.ApplyResources(this.lblTYPE, "lblTYPE");
            this.lblTYPE.Name = "lblTYPE";
            // 
            // DialogImport
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogImport";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnOK;
        private Label lblFILE_NAME;
        private TextBox txtFileName;
        private ExButton btnCLOSE;
        private Button btnBrowse_;
        private Panel panel2;
        private Label lblCUSTOMER_ERROR;
        private Label lblRESULT;
        private Label lblCUSTOMER_PAID;
        private Label lblCUSTOMER_NOT_FOUND;
        private Label lblCUSTOMER_SUCCESS;
        private Label txtPaid_;
        private Label txtNotFound_;
        private Label txtError;
        private Label txtSuccess_;
        private ExLinkLabel btnSHOW_DETAIL;
        private Label txtPaidAmount_;
        private Label txtNotFoundAmount_;
        private Label txtErrorAmount_;
        private Label txtSuccessAmount_;
        private Label lblAMOUNT;
        private Label lblTOTAL;
        private Label txtErrors_;
        private ComboBox cboImportType;
        private Label lblTYPE;
    }
}