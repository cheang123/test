﻿using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using System;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.BankPayment
{
    public partial class DialogB24SendData : ExDialog
    {
        public DialogB24SendData()
        {
            InitializeComponent();
        }

        private void btnB24_CUSTOMER_Click(object sender, EventArgs e)
        {
            new DialogB24Customer().ShowDialog();
        }

        private void btnSEND_DATA_Click(object sender, EventArgs e)
        {
            Runner.RunNewThread(delegate ()
            {
                using (var tran = new TransactionScope(TransactionScopeOption.Required, DBDataContext.Db.TransactionOption()))
                {
                    EPower.Logic.B24_Integration push = new EPower.Logic.B24_Integration();
                    push.Push(chkRESEND_ALL_DATA.Checked);
                    tran.Complete();
                }
            }, Resources.PROCESSING);
            if (Runner.Instance.Error == null)
            {
                MsgBox.ShowInformation(Resources.SUCCESS);
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
