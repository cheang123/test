﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface.BankPayment
{

    public partial class DialogAcledaAgreement : ExDialog
    {
        TBL_COMPANY objCompany = new TBL_COMPANY();
        TBL_ACLEDA_AGREEMENT objAcleda = new TBL_ACLEDA_AGREEMENT();
        bool _loading = false;

        string LicenseNo;
        public DialogAcledaAgreement()
        {
            InitializeComponent();
            _loading = true;
            load();
            _loading = false;
        }

        #region Method

        bool invalid()
        {
            bool val = false;

            this.ClearAllValidation();
            //Check account name
            if ((this.txtAccNameKHR.Text.Trim() == string.Empty || this.txtAccNoKHR.Text.Trim() == string.Empty)
                && (this.txtAccNameUSD.Text.Trim() == string.Empty || this.txtAccNoUSD.Text.Trim() == string.Empty)
                && (this.txtAccNameTHB.Text.Trim()==string.Empty || this.txtAccNoTHB.Text.Trim()==string.Empty))
            {
                MsgBox.ShowInformation(Resources.MSG_PLEASE_FILL_ACCOUNT_AT_LEASE_ONE);
                val = true;
            }
            return val; 
        }

        private void load()
        {
            objAcleda = DBDataContext.Db.TBL_ACLEDA_AGREEMENTs.OrderByDescending(x=>x.SEND_ID).FirstOrDefault();
            UIHelper.SetDataSourceToComboBox(cboProvince, DBDataContext.Db.TLKP_PROVINCEs);
            if (objAcleda == null)
            {
                //get company info
                objCompany = DBDataContext.Db.TBL_COMPANies.FirstOrDefault(x => x.COMPANY_ID == 1);
                LicenseNo = objCompany.LICENSE_NUMBER;
                this.txtCompany.Text = objCompany.LICENSE_NUMBER + " " + objCompany.COMPANY_NAME;
                this.txtRepresentative.Text = objCompany.LICENSE_NAME_KH;

                //set address info
                loadVillage(objCompany.VILLAGE_CODE);
            }
            else
            {
                txtCompany.Text = objAcleda.COMPANY_NAME;
                txtBuildingNo.Text = objAcleda.BUILDING_NO;
                txtStreet.Text = objAcleda.STREET_NO;
                loadVillage(objAcleda.COMMUNE_CODE);
                txtPhone.Text = objAcleda.PHONE;
                txtEmail.Text = objAcleda.EMAIL;
                txtFax.Text = objAcleda.FAX;
                txtRepresentative.Text = objAcleda.REPRESENTATION;
                txtPosition.Text = objAcleda.POSITION;
                txtAccNameKHR.Text = objAcleda.ACCOUNT_NAME_KHR;
                txtAccNameUSD.Text = objAcleda.ACCOUNT_NAME_USD;
                txtAccNoKHR.Text = objAcleda.ACCOUNT_NO_KHR;
                txtAccNoUSD.Text = objAcleda.ACCOUNT_NO_USD;
                txtAccNameTHB.Text = objAcleda.ACCOUNT_NAME_THB;
                txtAccNoTHB.Text = objAcleda.ACCOUNT_NO_THB;
            }
            loadVillage(objCompany.VILLAGE_CODE);
        }

        private void loadVillage(string villageCode)
        {
            //string communeCode = villageCode;
            //if (villageCode.Length >6)
            //{
            //    communeCode = DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode).COMMUNE_CODE;
            //}

            //string districCode = DBDataContext.Db.TLKP_COMMUNEs.FirstOrDefault(row => row.COMMUNE_CODE == (villageCode.Length>5?communeCode:villageCode)).DISTRICT_CODE;
            //string province = DBDataContext.Db.TLKP_DISTRICTs.FirstOrDefault(row => row.DISTRICT_CODE == districCode).PROVINCE_CODE;

            //UIHelper.SetDataSourceToComboBox(this.cboProvince, DBDataContext.Db.TLKP_PROVINCEs);
            //UIHelper.SetDataSourceToComboBox(this.cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(row => row.PROVINCE_CODE == province));
            //UIHelper.SetDataSourceToComboBox(this.cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(row => row.DISTRICT_CODE == districCode));

            //this.cboProvince.SelectedValue = province;
            //this.cboDistrict.SelectedValue = districCode;
            //this.cboCommune.SelectedValue = communeCode;

            this.cboProvince.SelectedValue = -1;
            this.cboDistrict.SelectedValue = -1;
            this.cboCommune.SelectedValue = -1;

            if (DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode) != null)
            {
                string communeCode = DBDataContext.Db.TLKP_VILLAGEs.FirstOrDefault(row => row.VILLAGE_CODE == villageCode).COMMUNE_CODE;
                string districCode = DBDataContext.Db.TLKP_COMMUNEs.FirstOrDefault(row => row.COMMUNE_CODE == communeCode).DISTRICT_CODE;
                string province = DBDataContext.Db.TLKP_DISTRICTs.FirstOrDefault(row => row.DISTRICT_CODE == districCode).PROVINCE_CODE;

                UIHelper.SetDataSourceToComboBox(this.cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(row => row.PROVINCE_CODE == province));
                UIHelper.SetDataSourceToComboBox(this.cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(row => row.DISTRICT_CODE == districCode));
                this.cboProvince.SelectedValue = province;
                this.cboDistrict.SelectedValue = districCode;
                this.cboCommune.SelectedValue = communeCode;
            }
        }
        
        private CrystalReportHelper viewReport()
        {
            CrystalReportHelper ch = new CrystalReportHelper("ReportAcledaAgreement.rpt");
            //Add parameterd value to report
            ch.SetParameter("@COMPANY_NAME_EDIT", txtCompany.Text);
            ch.SetParameter("@BUILDING_NO", this.txtBuildingNo.Text);
            ch.SetParameter("@STREET", this.txtStreet.Text);
            ch.SetParameter("@COMMUNE", this.cboCommune.Text);
            ch.SetParameter("@DISTRICT", this.cboDistrict.Text);
            ch.SetParameter("@PROVINCE", this.cboProvince.Text);
            ch.SetParameter("@LICENSE_REPRESENTATIVE", this.txtRepresentative.Text);
            ch.SetParameter("@PHONE_NO", this.txtPhone.Text);
            ch.SetParameter("@POSITION", this.txtPosition.Text);
            ch.SetParameter("@FAX", this.txtFax.Text);
            ch.SetParameter("@EMAIL", this.txtEmail.Text);
            ch.SetParameter("@ACC_NAME_KHR", this.txtAccNameKHR.Text);
            ch.SetParameter("@ACC_NO_KHR", this.txtAccNoKHR.Text);
            ch.SetParameter("@ACC_NAME_USD", this.txtAccNameUSD.Text);
            ch.SetParameter("@ACC_NO_USD", this.txtAccNoUSD.Text);
            ch.SetParameter("@ACC_NAME_THB", this.txtAccNameTHB.Text);
            ch.SetParameter("@ACC_NO_THB", this.txtAccNoTHB.Text);
            ch.SetParameter("@LICENSE_NO", DBDataContext.Db.TBL_COMPANies.FirstOrDefault().LICENSE_NUMBER);
            ch.SetParameter("@POOL_ONLY", DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs.Where(x => x.BANK == "ACL").Any());
            return ch;
        }
        #endregion

        private void btnPRINT_Click(object sender, EventArgs e)
        {
            //Check require fields
            if (invalid())
            {
                return;
            }

            CrystalReportHelper ch = viewReport();
            ch.ViewReport("");
        }
        
        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(!_loading && cboProvince.SelectedIndex != -1)
            {
                string strProvinceCode = cboProvince.SelectedValue.ToString();
                //distick
                UIHelper.SetDataSourceToComboBox(cboDistrict, DBDataContext.Db.TLKP_DISTRICTs.Where(d => d.PROVINCE_CODE == strProvinceCode));
            }
        }

        private void cboDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(!_loading && cboDistrict.SelectedIndex !=-1)
            {
                string strDisCode = cboDistrict.SelectedValue.ToString();
                //communte
                UIHelper.SetDataSourceToComboBox(cboCommune, DBDataContext.Db.TLKP_COMMUNEs.Where(c => c.DISTRICT_CODE == strDisCode));
            }
        }
        
        private void btnEMAIL_TO_BANK_Click(object sender, EventArgs e)
        {
            //Check require fields
            if (invalid())
            {
                return;
            }
            if (DBDataContext.Db.TBL_ACLEDA_AGREEMENTs.Count(x => x.IS_ACTIVE && x.IS_USE_POOL) > 0)
            {
                MsgBox.ShowInformation(Resources.MSG_DATA_ALREADY_SEND_TO_BANK_CAN_NOT_SEND_AGAIN);
                return;
            }
            //string sendTo = "system@ubill24.com";
            string sendTo = Method.utilities[Utility.ACL_AGGREEMENT_EMAIL_ADDRESS];
            objCompany = DBDataContext.Db.TBL_COMPANies.FirstOrDefault(x => x.COMPANY_ID == 1);
            LicenseNo = objCompany.LICENSE_NUMBER;
            string subject = "Licence#" + LicenseNo + " Online Registration – Reference#" + DateTime.Now.ToString("yyyyMMdd");
            string body = @"Dear Bill24,

សេវាករអគ្គីសនីខាងក្រោមបានចុះឈ្មោះបង់ប្រាក់ធនាគារអនឡាញតាមប្រព័ន្ធប៊ីល២៤ (Electricity Licensee Provider below has registered for Bill24 Online Payment):
លេខសម្គាល់សេវាករអគ្គីសនី/License ID: {LicenseId}
ឈ្មោះសេវាករអគ្គីសនី/License Name: {LicenseName}
អាស័យដ្ឋានសេវាករអគ្គីសនី/License Address: {Address}

ពត៌មានទំនាក់ទំនងសេវាករអគ្គីសនី/Contact Information:
    1. ទូរស័ព្ទ/Phone: {Phone}
    2. ទូរសារ/Fax: {Fax}
    3. អ៊ីម៉េល/Email: {Email}

សេវាករអគ្គីសនីបានផ្តល់លេខគណនីធនាគារដូចមានខាងក្រោម (The Licensee has provided the following accounts information):
   1. ធនាគារ អេស៊ីលីដា ភីអិលស៊ី/Acleda:
   {ACCOUNT_KHR}{ACCOUNT_USD}{ACCOUNT_THB}
ការចុះឈ្មោះបង់ប្រាក់ធនាគារអនឡាញតាមប្រព័ន្ធប៊ីល២៤នេះត្រួវបានធ្វើឡើងតាមរយ:កម្មវិធី E-Power billing systemដែលបានបញ្ជារដោយអ្នកប្រើប្រាស់ឈ្មោះ {UserName} នៅថ្ងៃទី {UsedDate} (This registration event has been executed from E-Power billing system by the user with UserID: {UserName} at {UsedDate})។
ខ្លឹមសារនៅក្នុងអ៊ីម៉េលត្រូវបានចុះផ្សាយនៅក្នុងកម្មវិធីទូរស័ព្ទរបស់សេវាករដើម្បីបង្ហាញជាសាក្ខីនិងផ្ទៀងផ្ទាត់ដោយសេវាករផ្ទាល់ (This email contents has been posted in E-Power Mobile App to show to the Licensee that the Licensee has registered the service and verify that the provided information is correct).

Best Regards,

E-Power billing system Team.

*****************************************************************************************************************
អ៊ីម៉ែលនេះបានបង្កើតឡើងដោយស្វ័យប្រវត្តិក្នុងពេលអ្នកប្រើប្រាស់ E-Power billing system ចុះឈ្មោះបង់ប្រាក់ធនាគារអនឡាញតាមប្រព័ន្ធប៊ីល២៤។ 
កិច្ចព្រមព្រៀងនេះត្រូវបានរក្សាទុកនៅក្នុង E-Power billing system និង E-Power Mobile App ជាមួយនឹងសេចក្តីយោង(Reference)ដែលបានផ្តល់ឱ្យនៅក្នុងប្រធានបទ(Subject)នេះ 
(This email is system auto-generated on the event of CRM user signing up for Bill24 Online Payment. 
The agreement is stored in E-Power billing system and E-Power Mobile App with the reference given in the subject).
*****************************************************************************************************************

";
            //body = body.Replace("{", "<strong>{").Replace("}", "}</strong>");
            //replace string
            body = body.Replace("{LicenseId}",LicenseNo);
            body = body.Replace("{LicenseName}", txtCompany.Text);
            body = body.Replace("{Address}", txtBuildingNo.Text + " " + txtStreet.Text + "​ " + cboCommune.Text + "  " + cboDistrict.Text + "  " + cboProvince.Text);
            body = body.Replace("{Phone}", txtPhone.Text);
            body = body.Replace("{Fax}", txtFax.Text);
            body = body.Replace("{Email}", txtEmail.Text);
            body = body.Replace("{UserName}", Login.CurrentLogin.LOGIN_NAME);
            body = body.Replace("{UsedDate}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
            body = body.Replace("\r\n", Environment.NewLine);

            //Bank account
            //KH
            if (txtAccNameKHR.Text != "" && txtAccNoKHR.Text != "")
            {
                body = body.Replace("{ACCOUNT_KHR}", Environment.NewLine + "លេខគណនី / Account Number: " + txtAccNoKHR.Text + Environment.NewLine
                                                    + "ឈ្មោះគណនី / Account Name:" + txtAccNameKHR.Text +   Environment.NewLine
                                                    + "រូបិយប័ណ្ណគណនី / Account Currency: រៀល/riels" + Environment.NewLine);
            }
            else
            {
                body = body.Replace("{ACCOUNT_KHR}", "");
            }

            //USD
            if (txtAccNameUSD.Text != "" && txtAccNoUSD.Text != "")
            {
                body = body.Replace("{ACCOUNT_USD}", Environment.NewLine+"លេខគណនី / Account Number: " + txtAccNoUSD.Text + Environment.NewLine
                                                    + "ឈ្មោះគណនី / Account Name:" + txtAccNameUSD.Text + Environment.NewLine
                                                    + "រូបិយប័ណ្ណគណនី / Account Currency: ដុល្លារ/dollar" + Environment.NewLine);
            }
            else
            {
                body = body.Replace("{ACCOUNT_USD}", "");
            }

            //THB
            if (txtAccNameTHB.Text != "" && txtAccNoTHB.Text != "")
            {
                body = body.Replace("{ACCOUNT_THB}", Environment.NewLine+"  លេខគណនី / Account Number: " + txtAccNoTHB.Text + Environment.NewLine
                                                    + " ឈ្មោះគណនី / Account Name:" + txtAccNameTHB.Text + Environment.NewLine
                                                    + " រូបិយប័ណ្ណគណនី / Account Currency: បាត/baht" + Environment.NewLine);
            }
            else
            {
                body = body.Replace("{ACCOUNT_THB}", "");
            }

            // user HTML format
            //body  = "<html><body><font face=\"Khmer OS Siemreap, Kh Siemreap,Arial\" >" +  body.Replace(Environment.NewLine, "<br /> ") +"</font></body></html>";

            bool success = false;
            string exportPath = "";
            try
            {
                // export report
                CrystalReportHelper ch = this.viewReport();
                exportPath = new FileInfo(Settings.Default.PATH_TEMP + ch.ReportName + ".pdf").FullName;
                ch.ExportToPDF(exportPath);
            }
            catch (Exception)
            {
                exportPath = "";
                subject = "Error Export " + subject;
            } 

            Runner.RunNewThread(delegate ()
            {
               WebHelper.SendMail(sendTo, subject, body, true, false, exportPath); 
                success = true;
            }, Resources.PROCESSING);
            if (success)
            {
                DBDataContext.Db.Insert(new TBL_ACLEDA_AGREEMENT()
                {
                    COMPANY_NAME = txtCompany.Text,
                    BUILDING_NO = txtBuildingNo.Text,
                    STREET_NO = txtStreet.Text,
                    COMMUNE_CODE = (string)cboCommune.SelectedValue,
                    PHONE = txtPhone.Text,
                    EMAIL = txtEmail.Text,
                    FAX = txtFax.Text,
                    REPRESENTATION = txtRepresentative.Text,
                    POSITION = txtPosition.Text,
                    ACCOUNT_NAME_KHR = txtAccNameKHR.Text,
                    ACCOUNT_NAME_USD = txtAccNameUSD.Text,
                    ACCOUNT_NO_KHR = txtAccNoKHR.Text,
                    ACCOUNT_NO_USD = txtAccNoUSD.Text,
                    ACCOUNT_NAME_THB=txtAccNameTHB.Text,
                    ACCOUNT_NO_THB=txtAccNoTHB.Text,
                    CREATE_ON=DBDataContext.Db.GetSystemDate(),
                    IS_USE_POOL = true
                });
            }
            MsgBox.ShowInformation(Resources.SUCCESS);
            this.Close();
        }

        private void btnCHANGE_LOG_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this.objAcleda);
        }

        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void content_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && Control.ModifierKeys == Keys.Control)
            {
                btnCLEAR_FLAG.Visible = !btnCLEAR_FLAG.Visible;
            }
        }

        private void btnCLEAR_FLAG_Click(object sender, EventArgs e)
        {
            TBL_ACLEDA_AGREEMENT obj = DBDataContext.Db.TBL_ACLEDA_AGREEMENTs.FirstOrDefault(x => x.IS_ACTIVE);
            if (obj != null)
            {
                obj.IS_ACTIVE = false;
                DBDataContext.Db.SubmitChanges();
                MsgBox.ShowInformation(Resources.SUCCESS);
            }
            else
            {
                MsgBox.ShowInformation(Resources.MSG_NO_DATA_TO_CLEAR);
            }
        }
    }
}
