﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.BankPayment
{
    partial class DialogPendingPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPendingPayment));
            this.btnClose = new SoftTech.Component.ExButton();
            this.txtSearch = new SoftTech.Component.ExTextbox();
            this.btnREPORT = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.BANK_PAYMENT_DETAIL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAID_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CASHIER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BANK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRANCH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENT_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENT_METHOD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblCONTACT_BANK_SUPPORT = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnRESETTLEMENT = new SoftTech.Component.ExButton();
            this.btnADD_TO_PREPAYMENT = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnADD_TO_PREPAYMENT);
            this.content.Controls.Add(this.btnRESETTLEMENT);
            this.content.Controls.Add(this.lblCONTACT_BANK_SUPPORT);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.btnREPORT);
            this.content.Controls.Add(this.txtSearch);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.label2);
            resources.ApplyResources(this.content, "content");
            this.content.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.content_MouseDoubleClick);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.txtSearch, 0);
            this.content.Controls.SetChildIndex(this.btnREPORT, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.lblCONTACT_BANK_SUPPORT, 0);
            this.content.Controls.SetChildIndex(this.btnRESETTLEMENT, 0);
            this.content.Controls.SetChildIndex(this.btnADD_TO_PREPAYMENT, 0);
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearch, "txtSearch");
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearch.QuickSearch += new System.EventHandler(this.txtSearch_QuickSearch);
            // 
            // btnREPORT
            // 
            resources.ApplyResources(this.btnREPORT, "btnREPORT");
            this.btnREPORT.Name = "btnREPORT";
            this.btnREPORT.UseVisualStyleBackColor = true;
            this.btnREPORT.Click += new System.EventHandler(this.btnREPORT_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BANK_PAYMENT_DETAIL_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.PAY_AMOUNT,
            this.CURRENCY_,
            this.PAID_AMOUNT,
            this.PAY_DATE,
            this.CASHIER,
            this.BANK,
            this.BRANCH,
            this.PAYMENT_TYPE,
            this.PAYMENT_METHOD});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Red;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // BANK_PAYMENT_DETAIL_ID
            // 
            this.BANK_PAYMENT_DETAIL_ID.DataPropertyName = "BANK_PAYMENT_DETAIL_ID";
            resources.ApplyResources(this.BANK_PAYMENT_DETAIL_ID, "BANK_PAYMENT_DETAIL_ID");
            this.BANK_PAYMENT_DETAIL_ID.Name = "BANK_PAYMENT_DETAIL_ID";
            this.BANK_PAYMENT_DETAIL_ID.ReadOnly = true;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // PAY_AMOUNT
            // 
            this.PAY_AMOUNT.DataPropertyName = "PAY_AMOUNT";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##0.####";
            dataGridViewCellStyle2.NullValue = null;
            this.PAY_AMOUNT.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.PAY_AMOUNT, "PAY_AMOUNT");
            this.PAY_AMOUNT.Name = "PAY_AMOUNT";
            this.PAY_AMOUNT.ReadOnly = true;
            // 
            // CURRENCY_
            // 
            this.CURRENCY_.DataPropertyName = "CURRENCY";
            resources.ApplyResources(this.CURRENCY_, "CURRENCY_");
            this.CURRENCY_.Name = "CURRENCY_";
            this.CURRENCY_.ReadOnly = true;
            // 
            // PAID_AMOUNT
            // 
            this.PAID_AMOUNT.DataPropertyName = "PAID_AMOUNT";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,##0.####";
            this.PAID_AMOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.PAID_AMOUNT, "PAID_AMOUNT");
            this.PAID_AMOUNT.Name = "PAID_AMOUNT";
            this.PAID_AMOUNT.ReadOnly = true;
            // 
            // PAY_DATE
            // 
            this.PAY_DATE.DataPropertyName = "PAY_DATE";
            dataGridViewCellStyle4.Format = "dd-MM-yyyy HH:mm:ss";
            this.PAY_DATE.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.PAY_DATE, "PAY_DATE");
            this.PAY_DATE.Name = "PAY_DATE";
            this.PAY_DATE.ReadOnly = true;
            // 
            // CASHIER
            // 
            this.CASHIER.DataPropertyName = "CASHIER";
            resources.ApplyResources(this.CASHIER, "CASHIER");
            this.CASHIER.Name = "CASHIER";
            this.CASHIER.ReadOnly = true;
            // 
            // BANK
            // 
            this.BANK.DataPropertyName = "BANK";
            resources.ApplyResources(this.BANK, "BANK");
            this.BANK.Name = "BANK";
            this.BANK.ReadOnly = true;
            // 
            // BRANCH
            // 
            this.BRANCH.DataPropertyName = "BRANCH";
            resources.ApplyResources(this.BRANCH, "BRANCH");
            this.BRANCH.Name = "BRANCH";
            this.BRANCH.ReadOnly = true;
            // 
            // PAYMENT_TYPE
            // 
            this.PAYMENT_TYPE.DataPropertyName = "PAYMENT_TYPE";
            resources.ApplyResources(this.PAYMENT_TYPE, "PAYMENT_TYPE");
            this.PAYMENT_TYPE.Name = "PAYMENT_TYPE";
            this.PAYMENT_TYPE.ReadOnly = true;
            // 
            // PAYMENT_METHOD
            // 
            this.PAYMENT_METHOD.DataPropertyName = "PAYMENT_METHOD";
            resources.ApplyResources(this.PAYMENT_METHOD, "PAYMENT_METHOD");
            this.PAYMENT_METHOD.Name = "PAYMENT_METHOD";
            this.PAYMENT_METHOD.ReadOnly = true;
            // 
            // lblCONTACT_BANK_SUPPORT
            // 
            resources.ApplyResources(this.lblCONTACT_BANK_SUPPORT, "lblCONTACT_BANK_SUPPORT");
            this.lblCONTACT_BANK_SUPPORT.Name = "lblCONTACT_BANK_SUPPORT";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // btnRESETTLEMENT
            // 
            resources.ApplyResources(this.btnRESETTLEMENT, "btnRESETTLEMENT");
            this.btnRESETTLEMENT.Name = "btnRESETTLEMENT";
            this.btnRESETTLEMENT.UseVisualStyleBackColor = true;
            this.btnRESETTLEMENT.Click += new System.EventHandler(this.btnResettlement_Click);
            // 
            // btnADD_TO_PREPAYMENT
            // 
            resources.ApplyResources(this.btnADD_TO_PREPAYMENT, "btnADD_TO_PREPAYMENT");
            this.btnADD_TO_PREPAYMENT.Name = "btnADD_TO_PREPAYMENT";
            this.btnADD_TO_PREPAYMENT.UseVisualStyleBackColor = true;
            this.btnADD_TO_PREPAYMENT.Click += new System.EventHandler(this.btnADD_TO_PREPAYMENT_Click);
            // 
            // DialogPendingPayment
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPendingPayment";
            this.Load += new System.EventHandler(this.DialogBankPaymentHistoryDetail_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnClose;
        private ExTextbox txtSearch;
        private ExButton btnREPORT;
        private DataGridView dgv;
        private Label label2;
        private Label lblCONTACT_BANK_SUPPORT;
        private ExButton btnRESETTLEMENT;
        private ExButton btnADD_TO_PREPAYMENT;
        private DataGridViewTextBoxColumn BANK_PAYMENT_DETAIL_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn PAY_AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_;
        private DataGridViewTextBoxColumn PAID_AMOUNT;
        private DataGridViewTextBoxColumn PAY_DATE;
        private DataGridViewTextBoxColumn CASHIER;
        private DataGridViewTextBoxColumn BANK;
        private DataGridViewTextBoxColumn BRANCH;
        private DataGridViewTextBoxColumn PAYMENT_TYPE;
        private DataGridViewTextBoxColumn PAYMENT_METHOD;
    }
}