﻿using System;
using System.Data;
using EPower.Properties;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.BankPayment
{
    public partial class DialogImportResult : ExDialog
    {
        DataTable dtResult = null;
        public DialogImportResult(DataTable dtResult)
        {
            InitializeComponent();

            this.dtResult = dtResult;

            // bind combobox. 
            UIHelper.SetDataSourceToComboBox(this.cboResult, this.getResultTable());

            this.bindData();
        
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private DataTable getResultTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RESULT_ID", typeof(int));
            dt.Columns.Add("RESULT_NAME", typeof(string));

            DataRow rowSuccess = dt.NewRow();
            rowSuccess[0] = (int)BankPaymentImportResult.RecordSuccess;
            rowSuccess[1] = Resources.MS_BANK_PAYMENT_CUSTOMER_SUCCESS;

            DataRow rowError = dt.NewRow();
            rowError[0] = (int)BankPaymentImportResult.RecordError;
            rowError[1] = Resources.MS_BANK_PAYMENT_CUSTOMER_ERROR;

            DataRow rowNotFound = dt.NewRow();
            rowNotFound[0] = (int)BankPaymentImportResult.RecordNotFound;
            rowNotFound[1] = Resources.MS_BANK_PAYMENT_CUSTOMER_NOT_FOUND;

            DataRow rowPaid = dt.NewRow();
            rowPaid[0] = (int)BankPaymentImportResult.RecordPaid;
            rowPaid[1] = Resources.MS_BANK_PAYMENT_CUSTOMER_PAID;

            dt.Rows.Add(rowSuccess);
            dt.Rows.Add(rowPaid);
            dt.Rows.Add(rowNotFound);
            dt.Rows.Add(rowError);
            return dt;
        }

        private void cboResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindData();
        }
        private void bindData()
        {
            if (dtResult.Rows.Count==0)
            {
                return;
            }
            string condition = "RESULT=" + this.cboResult.SelectedValue.ToString();
            this.dtResult.DefaultView.RowFilter = condition;
            this.dgvInvoiceItem.DataSource = dtResult.DefaultView;
            this.lblCount.Text = this.dtResult.DefaultView.Count.ToString();
            this.lblAmount.Text = DataHelper.ParseToDecimal(this.dtResult.Compute("SUM(PAY_AMOUNT2)", condition).ToString()).ToString("N2");
        }
    }
}
