﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EPower.Base.Logic;
using EPower.Base.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.BankPayment
{
    public partial class FormEPowerClient : ExDialog
    {
        public FormEPowerClient()
        {
            InitializeComponent();
            this.btnAREA_SETTING.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_BANK_PAYMENT_LOG_CUSTOMER_BY_AREA]);
        }

        private void FormEPowerClient_Load(object sender, EventArgs e)
        {
            lblDOWNLOAD_PAYMENT_.Text = Resources.LOADING_NEW_PAYMENT;
            var timer = new Timer();
            timer.Tick += delegate (object ts, EventArgs te)
            {
                timer.Stop();
                //btnVpnConnect_Click(btnVPN_CONNECT, null);
                this.checkVPNStatus();
                Application.DoEvents();
                try
                {
                    var n = new PaymentOperation().GetPendingPaymentCount();
                    lblDOWNLOAD_PAYMENT_.Text = string.Format(Resources.MS_HAVE_X_PENDING_PAYMENT_FOR_DOWNLOAD, n);
                    this.grpDATA.Enabled = true;
                }
                catch (Exception ex)
                {
                    lblDOWNLOAD_PAYMENT_.Text = "";
                    MsgBox.LogError(ex);
                    MsgBox.ShowWarning(Resources.MSG_ERROR_BANK_SERVICE, Resources.WARNING);
                }
            };
            timer.Start();
        }


        private void btnUpdateCustomer_Click(object sender, EventArgs e)
        {
            // show Bank Payment User's Agreement if not yet click remember check box.
            if (!Properties.Settings.Default.REMEMBER_BANK_PAYMENT_USER_AGREEMENT)
            {
                var diag = new DialogUserAgreement();
                if (diag.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            UpdateCustomerData(this.chkRESEND_ALL_DATA.Checked);


        }

        private void UpdateCustomerData(bool forceAllCustomer)
        {
            var paymentOperation = new PaymentOperation();
            //paymentOperation.GetActivatedBank();
            paymentOperation.UpdateLabel = (string val) => { Application.DoEvents(); lblCustomer.Text = val; };
            paymentOperation.ShowMessage = (string val) => MsgBox.ShowInformation(val);

            paymentOperation.UpdateLabel = (string val) =>
            {
                Application.DoEvents();
                Runner.Instance.Text = val;
            };
            var lastMessage = "";
            paymentOperation.ShowMessage = (string val) =>
            {
                lastMessage = val;
            };
            Runner.RunNewThread(delegate ()
            {
                paymentOperation.UpdateCustomerData(forceAllCustomer);
            });
            if (lastMessage != "")
            {
                MsgBox.ShowInformation(lastMessage);
            }
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            int pendingRecords = 0;
            try
            {
                if (Login.CurrentCashDrawer == null)
                {
                    if (MsgBox.ShowQuestion(Resources.MS_YOU_MUST_OPEN_CASH_DRAWER, string.Empty) == DialogResult.Yes)
                    {
                        //new DialogOpenCashDrawer().ShowDialog();
                        var diag = new DialogOpenCashDrawer();
                        diag.isBank = true;
                        diag.ShowDialog();
                    }

                    if (Login.CurrentCashDrawer == null)
                    {
                        return;
                    }
                }
                if (DBDataContext.Db.TBL_CASH_DRAWERs.Where(x => x.CASH_DRAWER_ID == Login.CurrentCashDrawer.CASH_DRAWER_ID && x.IS_BANK != true).Any())
                {
                    if (MsgBox.ShowQuestion(Resources.MS_THIS_CASH_DRAWER_IS_NOT_FOR_BANK, Resources.WARNING) != DialogResult.Yes)
                    {
                        return;
                    }
                }

                // Backup DB
                //Runner.RunNewThread(delegate()
                //{
                //    Runner.Instance.Text = "កំពុងចំលងទុក...";
                //    SoftTech.Security.Logic.DBA.BackupToDisk(Properties.Settings.Default.PATH_BACKUP, String.Format("{0} {1:yyyyMMddHHmmss} Before download payment.bak", DBDataContext.Db.Connection.Database, DBDataContext.Db.GetSystemDate()));
                //});


                var paymentOperation = new PaymentOperation();
                paymentOperation.UpdateLabel = (string val) =>
                {
                    Application.DoEvents();
                    Runner.Instance.Text = val;
                };
                var lastMessage = "";
                paymentOperation.ShowMessage = (string val) =>
                {
                    //MsgBox.ShowInformation(val);
                    lastMessage = val;
                };
                Runner.RunNewThread(delegate ()
                {
                    paymentOperation.GetPayment();
                    paymentOperation.ClearPayment();
                });
                var error = Runner.Instance.Error;
                if (lastMessage != "")
                {
                    MsgBox.ShowInformation(lastMessage);
                }

                lblDOWNLOAD_PAYMENT_.Text = string.Format(Resources.MS_HAVE_X_PENDING_PAYMENT_FOR_DOWNLOAD, new PaymentOperation().GetPendingPaymentCount());

                pendingRecords = DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs.Count(x => x.IS_VOID == false
                                                                                    && x.RESULT == (int)BankPaymentImportResult.RecordSuccess
                                                                                    && x.PAY_AMOUNT > x.PAID_AMOUNT);
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(string.Format(Resources.MSG_ERROR, Resources.DOWNLOAD),Resources.DOWNLOAD);
            }
            
            if (pendingRecords > 0)
            {
                new DialogPendingPayment().ShowDialog();
            }
        }

        private void btnVpnConnect_Click(object sender, EventArgs e)
        {
            lblVPN_STATUS_.Text = Resources.VPN_STATUS + " : " + Resources.VPN_CONNECTING;
            lblVPN_STATUS_.ForeColor = Color.Blue;
            Application.DoEvents();
            VPN.Instance.Connect();
            checkVPNStatus();
        }

        private void btnVpnDisconnect_Click(object sender, EventArgs e)
        {
            lblVPN_STATUS_.Text = Resources.VPN_STATUS + " : " + Resources.VPN_DISCOUNNECTING;
            lblVPN_STATUS_.ForeColor = Color.Blue;
            Application.DoEvents();


            VPN.Instance.Disconnect();
            checkVPNStatus();
        }

        private void btnVpnCheckStatus_Click(object sender, EventArgs e)
        {
            checkVPNStatus();
        }

        private void checkVPNStatus()
        {
            var status = SoftTech.Helper.VpnClient.VpnState.Disconnected;// VPN.Instance.State;
            if (SoftTech.Helper.TCPHelper.IsInternetConnected())
            {
                status = SoftTech.Helper.VpnClient.VpnState.Connected;
            }

            if (status == SoftTech.Helper.VpnClient.VpnState.Connected)
            {
                lblVPN_STATUS_.Text = Properties.Resources.VPN_CONNECTED;
                lblVPN_STATUS_.ForeColor = Color.Green;
            }
            else if (status == SoftTech.Helper.VpnClient.VpnState.Connecting)
            {
                lblVPN_STATUS_.Text = Properties.Resources.VPN_CONNECTING;
                lblVPN_STATUS_.ForeColor = Color.Blue;
            }
            else if (status == SoftTech.Helper.VpnClient.VpnState.Disconnected)
            {
                lblVPN_STATUS_.Text = Properties.Resources.VPN_DISCONNECTED;
                lblVPN_STATUS_.ForeColor = Color.Red;
            }
            else if (status == SoftTech.Helper.VpnClient.VpnState.Disconnecting)
            {
                lblVPN_STATUS_.Text = Properties.Resources.VPN_DISCOUNNECTING;
                lblVPN_STATUS_.ForeColor = Color.Blue;
            }
            lblVPN_STATUS_.Text = Properties.Resources.VPN_STATUS + " : " + lblVPN_STATUS_.Text;
            this.grpDATA.Enabled = status == SoftTech.Helper.VpnClient.VpnState.Connected;
        }

        private void FormEPowerClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            VPN.Instance.Disconnect();
        }

        private void btnAREA_SETTING_Click(object sender, EventArgs e)
        {
            DialogSelectAreaToBank diag = new DialogSelectAreaToBank();
            diag.ShowDialog();
        }

        private void btnUPDATE_CUSTOMER_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && Control.ModifierKeys == Keys.Control)
            {
                btnAREA_SETTING.Visible = btnAREA_SETTING.Visible == true ? false : true;
                TBL_UTILITY utility = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == (int)Utility.ENABLE_BANK_PAYMENT_LOG_CUSTOMER_BY_AREA);
                utility.UTILITY_VALUE = btnAREA_SETTING.Visible == true ? "1" : "0";
                DBDataContext.Db.SubmitChanges();
            }
        }
    }
}

