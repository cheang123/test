﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogSelectAreaToBank : ExDialog
    {
        #region Constructor
        public DialogSelectAreaToBank()
        {
            InitializeComponent();
        }
        #endregion Constructor

        #region Method
        private void bind()
        {
            var dt = (from a in DBDataContext.Db.TBL_AREAs
                  join c in DBDataContext.Db.TBL_CUSTOMERs on a.AREA_ID equals c.AREA_ID
                  join log in DBDataContext.Db.TBL_BANK_PAYMENT_LOG_CUSTOMER_BY_AREAs on a.AREA_ID equals log.AREA_ID into ljoinLog
                  from lj in ljoinLog.DefaultIfEmpty()
                  where (c.STATUS_ID == (int)CustomerStatus.Active || c.STATUS_ID == (int)CustomerStatus.Blocked) && a.IS_ACTIVE
                  group c by new { a.AREA_ID, a.AREA_CODE, a.AREA_NAME, lj.IS_SEND } into gArea
                  orderby gArea.Key.AREA_CODE
                  select new
                  {
                      gArea.Key.AREA_ID,
                      SELECT_ = gArea.Key.IS_SEND == null || gArea.Key.IS_SEND==false? false : true,
                      gArea.Key.AREA_CODE,
                      gArea.Key.AREA_NAME,
                      TOTAL_CUSTOMER = gArea.Count()
                  });
            dgv.DataSource = dt._ToDataTable();
        }

        #endregion Method 

        private void DialogSelectAreaToBank_Load(object sender, EventArgs e)
        {
            bind();
        }

        private void CHK_ALL_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dgv.Rows)
            {
                item.Cells[SELECT_.Name].Value = CHK_ALL.Checked;
            }
        }

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in dgv.Rows)
            {
                int areaId = (int)item.Cells[AREA_ID.Name].Value;
                bool isCheck = (bool)(item.Cells[SELECT_.Name].Value);
                TBL_BANK_PAYMENT_LOG_CUSTOMER_BY_AREA obj = DBDataContext.Db.TBL_BANK_PAYMENT_LOG_CUSTOMER_BY_AREAs.FirstOrDefault(x => x.AREA_ID == areaId);
                
                if (obj == null)
                {
                    DBDataContext.Db.Insert(new TBL_BANK_PAYMENT_LOG_CUSTOMER_BY_AREA() { AREA_ID=areaId,IS_SEND=isCheck});
                }
                else
                {
                    TBL_BANK_PAYMENT_LOG_CUSTOMER_BY_AREA objOLD = new TBL_BANK_PAYMENT_LOG_CUSTOMER_BY_AREA();
                    obj._CopyTo(objOLD);
                    obj.IS_SEND = isCheck;
                    DBDataContext.Db.Update(objOLD, obj);
                }
            }
            
            this.Close();
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1 && this.dgv.Columns[e.ColumnIndex].Name == this.SELECT_.Name)
            {
                this.dgv[this.SELECT_.Name, e.RowIndex].Value = !(bool)(this.dgv[this.SELECT_.Name, e.RowIndex].Value);
            }   
        }
    }

    

}
