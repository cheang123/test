﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogCashDrawer : ExDialog
    {
        GeneralProcess _flag ;        
        TBL_CASH_DRAWER _objCashDrawer = new TBL_CASH_DRAWER();
        public TBL_CASH_DRAWER CashDrawer
        {
            get { return _objCashDrawer; }
        }
        TBL_CASH_DRAWER _oldCashDrawer = new TBL_CASH_DRAWER();

        #region Constructor
        public DialogCashDrawer(GeneralProcess flag, TBL_CASH_DRAWER objCashDrawer)
        {
            InitializeComponent();
            _flag = flag;

            dataLookUp();
            objCashDrawer._CopyTo(_objCashDrawer);
            objCashDrawer._CopyTo(_oldCashDrawer);
            if (flag == GeneralProcess.Insert)
            {
                _objCashDrawer.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            chk.ReadOnly = false;
            dgv.ReadOnly = false;
            chk.ValueType = typeof(bool);
            read();           

        }

        
        #endregion

        #region Method
        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {                 
            txtCashDrawName.Text = _objCashDrawer.CASH_DRAWER_NAME;
            cboCurrency.SelectedValue = _objCashDrawer.CURRENCY_ID;
            txtDefaultFloat.Text = UIHelper.FormatCurrency(_objCashDrawer.DEFAULT_FLOATING_CASH, _objCashDrawer.CURRENCY_ID);
            if (txtDefaultFloat.Text == "")
            {
                txtDefaultFloat.Text = "0";
            }
            chkIS_BANK.Checked = _objCashDrawer.IS_BANK;
            txtDefaultDayClose.Text = _objCashDrawer.DAY.ToString();

            var user = DBDataContext.Db.TBL_LOGINs.Where(x => x.IS_ACTIVE && x.IS_CAN_LOG);
            var userDrawer = DBDataContext.Db.TBL_LOGIN_CASH_DRAWERs.Where(x=>x.CASH_DRAWER_ID==_objCashDrawer.CASH_DRAWER_ID);
            var sourse = (from s in user
                          join c in userDrawer on s.LOGIN_ID equals c.LOGIN_ID
                         into g
                         select new
                         {
                             chk = g.Any(x => x.CASH_DRAWER_ID == _objCashDrawer.CASH_DRAWER_ID),
                             s.LOGIN_ID,
                             s.LOGIN_NAME
                         })._ToDataTable();
            
            dgv.DataSource = sourse;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objCashDrawer.CASH_DRAWER_NAME = txtCashDrawName.Text.Trim();
            _objCashDrawer.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objCashDrawer.DEFAULT_FLOATING_CASH = DataHelper.ParseToDecimal(txtDefaultFloat.Text);
            _objCashDrawer.IS_BANK = chkIS_BANK.Checked;
            _objCashDrawer.DAY = DataHelper.ParseToInt(this.txtDefaultDayClose.Text);
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                val = true;
            }
            if (txtCashDrawName.Text.Trim().Length <= 0)
            {
                txtCashDrawName.SetValidation(string.Format(Resources.REQUIRED, lblCASH_DRAWER.Text));
                val = true;
            }            
            if (txtDefaultFloat.Text.Trim().Length <= 0)
            {
                txtDefaultFloat.SetValidation(string.Format(Resources.REQUIRED, lblDEFAULT_FLOATING_CASH.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(this.txtDefaultDayClose.Text))
            {                
                this.txtDefaultDayClose.SetValidation(string.Format(Resources.REQUIRED_INPUT_NUMBER, this.txtDefaultDayClose.Text));
                val = true;
            }

            return val;
        }

        private void dataLookUp()
        {
            try
            {
                //Get currency.
                UIHelper.SetDataSourceToComboBox(cboCurrency,
                    from c in DBDataContext.Db.TLKP_CURRENCies
                    select new 
                    {
                        c.CURRENCY_ID,
                        c.CURRENCY_NAME
                    }, "CURRENCY_ID", "CURRENCY_NAME");    
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }        
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //If duplicat data.
            txtCashDrawName.ClearValidation();
            if (DBDataContext.Db.IsExits(_objCashDrawer,"CASH_DRAWER_NAME"))
            {
                txtCashDrawName.SetValidation(string.Format(Resources.MS_IS_EXISTS,lblCASH_DRAWER.Text));
                return;
            }
            try
            {
                using (TransactionScope tran=new TransactionScope( TransactionScopeOption.Required,TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objCashDrawer);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldCashDrawer, _objCashDrawer);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objCashDrawer);
                    }

                    foreach (DataGridViewRow r in this.dgv.Rows)
                    {
                        int login_id = (int)r.Cells[LOGIN_ID.Name].Value;
                        if ((bool)r.Cells[chk.Name].Value)
                        {
                            if (DBDataContext.Db.TBL_LOGIN_CASH_DRAWERs.Count(row => row.CASH_DRAWER_ID == this._objCashDrawer.CASH_DRAWER_ID && row.LOGIN_ID == login_id) == 0)
                            {
                                var obj = new TBL_LOGIN_CASH_DRAWER()
                                {
                                    LOGIN_ID = login_id,
                                    CASH_DRAWER_ID = _objCashDrawer.CASH_DRAWER_ID
                                };

                                DBDataContext.Db.TBL_LOGIN_CASH_DRAWERs.InsertOnSubmit(obj);
                            }
                        }
                        else
                        {
                            if (DBDataContext.Db.TBL_LOGIN_CASH_DRAWERs.Count(row => row.CASH_DRAWER_ID == this._objCashDrawer.CASH_DRAWER_ID && row.LOGIN_ID == login_id) > 0)
                            {
                                var obj = DBDataContext.Db.TBL_LOGIN_CASH_DRAWERs.FirstOrDefault(row => row.LOGIN_ID == login_id && row.CASH_DRAWER_ID == this._objCashDrawer.CASH_DRAWER_ID);
                                DBDataContext.Db.TBL_LOGIN_CASH_DRAWERs.DeleteOnSubmit(obj);
                            }
                        }
                    }
                    DBDataContext.Db.SubmitChanges();
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Input number Decimal only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNumberDecimal(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objCashDrawer);
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                this.dgv.Rows[e.RowIndex].Cells[chk.Name].Value = !(bool)this.dgv.Rows[e.RowIndex].Cells[chk.Name].Value;
            }
        }
    }
}
