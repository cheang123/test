﻿using System;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogVoltage: ExDialog
    {
        #region Data
        GeneralProcess _flag = GeneralProcess.Insert;
        TBL_VOLTAGE _objOld=new TBL_VOLTAGE();
        TBL_VOLTAGE _objNew=new TBL_VOLTAGE();
        public TBL_VOLTAGE Voltage
        {
            get
            {
                return _objNew;
            }
        }

        #endregion Data

        #region Constructor

        public DialogVoltage(TBL_VOLTAGE obj,GeneralProcess flag)
        {
            InitializeComponent();

            this._flag = flag;
            
            obj._CopyTo(this._objNew);
            obj._CopyTo(this._objOld);
            this.read(); 
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            if (flag == GeneralProcess.Delete)
            {
                UIHelper.SetEnabled(this,false);
            }
        }

        #endregion Constructor

        #region Method

        public bool invalid()
        {
            bool result = false;
            
            this.ClearAllValidation();

            if (this.txtVOLTAGE_NAME.Text == "")
            {
                this.txtVOLTAGE_NAME.SetValidation(string.Format(Resources.REQUIRED, this.lblVOLTAGE.Text));
                this.txtVOLTAGE_NAME.Focus();
                return true;
            }

            return result;
        }

        public void read()
        {
            this.txtVOLTAGE_NAME.Text = this._objNew.VOLTAGE_NAME;
        }
 
        public void write()
        {
            this._objNew.VOLTAGE_NAME = this.txtVOLTAGE_NAME.Text;
        }

        #endregion Method

        #region Event

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        { 
            if (this.invalid())
            {
                return;
            }

            this.write();
            txtVOLTAGE_NAME.ClearValidation();
            if (DBDataContext.Db.IsExits(this._objNew, "VOLTAGE_NAME"))
            {
                txtVOLTAGE_NAME.SetValidation(string.Format(Resources.MS_IS_EXISTS, this.lblVOLTAGE.Text));
                return;
            } 
            
            if (this._flag == GeneralProcess.Insert)
            {
                DBDataContext.Db.Insert(this._objNew);
            }
            else if (this._flag == GeneralProcess.Update)
            {
                DBDataContext.Db.Update(this._objOld, this._objNew);
            }
            else if (this._flag == GeneralProcess.Delete)
            {
                DBDataContext.Db.Delete(this._objNew);
            }
            this.DialogResult = DialogResult.OK; 
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void txtVOLTAGE_NAME_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        #endregion Event
    }
}
