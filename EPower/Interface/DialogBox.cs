﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class DialogBox : ExDialog
    {
        GeneralProcess _flag;
        TBL_BOX _objNew = new TBL_BOX();
        TBL_BOX _objOld = new TBL_BOX();

        public TBL_BOX Box
        {
            get { return _objNew; }
        }

        #region Constructor
        public DialogBox(GeneralProcess flag, TBL_BOX objBox)
        {
            InitializeComponent();
            btnAddPole.Enabled = Login.IsAuthorized(Permission.ADMIN_POLEANDBOX);
            _flag = flag;

            dataLockup();
            objBox._CopyTo(_objNew);
            objBox._CopyTo(_objOld);
            read();
            this.Text = flag.GetText(this.Text);
            UIHelper.SetEnabled(this, this._flag != GeneralProcess.Delete);
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
        }
        #endregion

        #region Method
        private void dataLockup()
        {
            try
            {
                UIHelper.SetDataSourceToComboBox(cboPole, Lookup.GetPoles(0));
                var s = from b in DBDataContext.Db.TLKP_BOX_SATUS
                        where _flag != GeneralProcess.Delete || b.STATUS_ID != (int)BoxStatus.Used
                        select new
                        {
                            b.STATUS_ID,
                            b.STATUS_NAME
                        };
                UIHelper.SetDataSourceToComboBox(cboStatus, s);
                //if (_flag == GeneralProcess.Delete)
                //{
                //    UIHelper.SetDataSourceToComboBox(cboStatus, s, "");
                //}
                UIHelper.SetDataSourceToComboBox(cboBoxType, Lookup.GetBoxType());
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void read()
        {
            txtCodeBox.Text = _objNew.BOX_CODE;
            cboPole.SelectedValue = _objNew.POLE_ID;
            cboStatus.SelectedValue = _flag == GeneralProcess.Delete ? (int)BoxStatus.Unavailable : _objNew.STATUS_ID;
            cboStatus.Enabled = !(_flag == GeneralProcess.Update && _objOld.STATUS_ID == (int)BoxStatus.Used);
            cboBoxType.SelectedValue = _objNew.BOX_TYPE_ID;
            dtpStartDate.Value = _objNew.STARTED_DATE;
            dtpEndDate.SetValue(_objNew.END_DATE);
        }

        private void write()
        {
            _objNew.BOX_CODE = txtCodeBox.Text.Trim();
            _objNew.POLE_ID = (int)cboPole.SelectedValue;
            _objNew.STATUS_ID = (int)cboStatus.SelectedValue;
            _objNew.STARTED_DATE = dtpStartDate.Value;
            _objNew.END_DATE = dtpEndDate.Value;
            _objNew.BOX_TYPE_ID = (int)cboBoxType.SelectedValue;
        }

        private bool invalid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (txtCodeBox.Text.Trim() == string.Empty)
            {
                txtCodeBox.SetValidation(string.Format(Resources.REQUIRED, lblBOX_CODE.Text));
                val = true;
            }
            if (cboPole.SelectedIndex == -1)
            {
                cboPole.SetValidation(string.Format(Resources.REQUIRED, lblPOLE.Text));
                val = true;
            }
            if (cboStatus.SelectedIndex == -1 || (int)cboStatus.SelectedValue == 0)
            {
                cboStatus.SetValidation(string.Format(Resources.REQUIRED, lblSTATUS.Text));
                val = true;
            }
            if (cboBoxType.SelectedIndex == -1 || (int)cboBoxType.SelectedValue == 0)
            {
                cboBoxType.SetValidation(string.Format(Resources.REQUIRED, lblBOX_TYPE.Text));
                val = true;
            }
            return val;
        }
        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (invalid())
            {
                return;
            }

            write();
            //If record is duplicate.
            txtCodeBox.ClearValidation();
            if (DBDataContext.Db.IsExits(_objNew, "BOX_CODE"))
            {
                txtCodeBox.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblBOX_CODE.Text));
                return;
            }

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    // if user change pole of the box then
                    // update customer meter of the current box.
                    if (this._objOld.POLE_ID != this._objNew.POLE_ID)
                    {
                        TBL_POLE objPole = DBDataContext.Db.TBL_POLEs.FirstOrDefault(x => x.POLE_ID == _objNew.POLE_ID);
                        foreach (var objCM in DBDataContext.Db.TBL_CUSTOMER_METERs.Where(x => x.BOX_ID == this._objNew.BOX_ID && x.IS_ACTIVE))
                        {
                            objCM.POLE_ID = _objNew.POLE_ID;
                            TBL_CUSTOMER objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(x => x.CUSTOMER_ID == objCM.CUSTOMER_ID);
                            objCustomer.AREA_ID = objPole.AREA_ID;
                            DBDataContext.Db.SubmitChanges();
                        }
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void btnAddPole_AddItem(object sender, EventArgs e)
        {
            DialogPole dig = new DialogPole(GeneralProcess.Insert,
                new TBL_POLE()
                {
                    POLE_CODE = "",
                    STARTED_DATE = DBDataContext.Db.GetSystemDate(),
                    END_DATE = UIHelper._DefaultDate,
                    VILLAGE_CODE = DBDataContext.Db.TBL_COMPANies.FirstOrDefault().VILLAGE_CODE
                });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                UIHelper.SetDataSourceToComboBox(cboPole,
                    from p in DBDataContext.Db.TBL_POLEs
                    where p.IS_ACTIVE
                    select new
                    {
                        p.POLE_ID,
                        p.POLE_CODE
                    });
                cboPole.SelectedValue = dig.Pole.POLE_ID;
            }
        }

        private void dtpEndDate_MouseDown(object sender, MouseEventArgs e)
        {
            if (dtpEndDate.Checked)
            {
                if (dtpEndDate.Value.Date.Equals(UIHelper._DefaultDate))
                {
                    dtpEndDate.SetValue(DBDataContext.Db.GetSystemDate());
                }
                else
                {
                    dtpEndDate.CustomFormat = UIHelper._DefaultDateFormat;
                }
            }
            else
            {
                dtpEndDate.ClearValue();
            }
        }

        private void btnBrowse__Click(object sender, EventArgs e)
        {
            new DialogPageBoxType().ShowDialog();
        }

        private void content_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && Control.ModifierKeys == Keys.Control)
            {
                btnBrowse_.Visible = btnBrowse_.Visible == true ? false : true;
                cboBoxType.Width = btnBrowse_.Visible == true ? 190 : 220;
            }
        }
    }
}
