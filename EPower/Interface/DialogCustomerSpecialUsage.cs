﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerSpecialUsage : ExDialog
    {
        private List<Usage_ListModel> _localResult = new List<Usage_ListModel>();
        private bool _isLoading = true;
        public DialogCustomerSpecialUsage()
        {
            InitializeComponent();
            UIHelper.SetDataSourceToComboBox(this.cboCycle, DBDataContext.Db.TBL_BILLING_CYCLEs.Where(row => row.IS_ACTIVE)); 
            this.bindData(); 
            this.Shown += DialogCustomerSpecialUsage_Shown;
        } 
        private void DialogCustomerSpecialUsage_Shown(object sender, EventArgs e)
        {
            if (this.IsHandleCreated)
            {
                this._isLoading = false;
                bindData();  
            }
        } 
        private void bindData()
        {
            if (_isLoading)
            {
                return;
            } 
            if (this.cboOperator.SelectedIndex == -1) this.cboOperator.SelectedIndex = 0; 
            DateTime month = new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month, 1);
            int cycleId = (int)this.cboCycle.SelectedValue;
            int usage = DataHelper.ParseToInt(this.txtUsageValue.Text);
            string opt = this.cboOperator.Text;

            string ConnectionString = Method.GetConnectionString(Settings.Default.CONNECTION);
            var dt = new DataTable();
            Runner.RunNewThread(() =>
            { 
                using (var cnn = new SqlConnection(ConnectionString))
                {
                    if (cnn.State == ConnectionState.Closed)
                    {
                        cnn.Open();
                    }
                    using (var cmd = new SqlCommand("dbo.VIEW_USAGE", cnn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        cmd.Parameters.AddWithValue("@USAGE_MONTH", month);
                        cmd.Parameters.AddWithValue("@BILLING_CYCLE_ID", cycleId);
                        dt.Load(cmd.ExecuteReader());
                    }
                }
                _localResult = dt.ToDataListModel<Usage_ListModel>(); 
            });

            BillLogic.RemoveDuplicatedUsage();
            RefreshFilter();
            SetForeColor();
        }

        private void RefreshFilter()
        {
            int usage = DataHelper.ParseToInt(this.txtUsageValue.Text);
            string opt = this.cboOperator.Text;
            var q = _localResult;
            //Meter
            if (this.chkIS_NEW_CYCLE.Checked)
            {
                q = q.Where(x => x.IS_METER_RENEW_CYCLE == this.chkIS_NEW_CYCLE.Checked).ToList();
            }
            
            //temper
            q = q.Where(x => (x.PHASE_ID == 3 && x.TAMPER_STATUS != "") || !chkMETER_TAMPER.Checked).ToList(); 
            q = q.Where(x => !this.chkUSAGE.Checked || ((opt == ">" && x.TOTAL_USAGE > usage) || (opt == "=" && x.TOTAL_USAGE == usage))).ToList();
            dgv.DataSource = q.OrderByDescending(x=>x.TOTAL_USAGE).ToList();     
             
        }
        private void SetForeColor()
        {
            //set ForeColor to Red if customer is close.
            foreach (DataGridViewRow item in dgv.Rows)
            {
                string tamper = (string)item.Cells[TAMPER_STATUS.Name].Value;

                if (!String.IsNullOrEmpty((string)item.Cells[TAMPER_STATUS.Name].Value) && (int)item.Cells[PHASE_ID.Name].Value == 3)
                {
                    item.DefaultCellStyle.ForeColor = Color.Red;
                    item.DefaultCellStyle.SelectionForeColor = Color.Red;
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkShowGreaterThan_CheckedChanged(object sender, EventArgs e)
        {
            RefreshFilter();
        }

        private void chkShowNewCycle_CheckedChanged(object sender, EventArgs e)
        {
            RefreshFilter();
           // SetForeColor();
        }
        private void chkMETER_TAMPER_CheckedChanged(object sender, EventArgs e)
        {
            RefreshFilter();
        }
         
        private void txtUsageValue_TextChanged(object sender, EventArgs e)
        {
            RefreshFilter();
        }

        private void dtpMonth_ValueChanged(object sender, EventArgs e)
        {
            this.bindData(); 
        }

        private void cboCycle_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.dtpMonth.Value = Method.GetNextBillingMonth((int)this.cboCycle.SelectedValue);
            this.bindData();
        }
        private void cboOperator_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshFilter();
           // SetForeColor();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                int CustomerID = (int)this.dgv.SelectedRows[0].Cells[this.CUSTOMER_ID.Name].Value;
                if (new DialogCollectUsageManually(CustomerID).ShowDialog() == DialogResult.OK)
                {
                    this.bindData();
                }
            }
        } 
        private void btnReport_Click(object sender, EventArgs e)
        {
            if (this.cboOperator.SelectedIndex == -1)
                this.cboOperator.SelectedIndex = 0;

            string filter = " WHERE r.MONTH_TOTAL_USAGE < 0 ";
            if (this.chkIS_NEW_CYCLE.Checked)
            {
                filter += " OR (r.IS_METER_RENEW_CYCLE=1) ";
            }
            if (this.chkUSAGE.Checked)
            {
                filter += " OR (r.MONTH_TOTAL_USAGE " + this.cboOperator.Text + " " + DataHelper.ParseToInt(this.txtUsageValue.Text).ToString() + " )";
            }
            try
            {
                DateTime dt = new DateTime(dtpMonth.Value.Year, dtpMonth.Value.Month, 1);
                CrystalReportHelper report = new CrystalReportHelper("ReportCustomerVerifyUsage.rpt");
                report.SetParameter("@MONTH", dt);
                report.SetParameter("@CYCLE_ID", (int)cboCycle.SelectedValue);
                report.SetParameter("@FILTER", filter);
                report.SetParameter("@AREA_ID", 0);
                report.SetParameter("@CYCLE_NAME", cboCycle.Text);
                report.SetParameter("@AREA_NAME", Resources.ALL_AREA);
                report.SetParameter("@M1", dt.AddMonths(-1));
                report.SetParameter("@M2", dt.AddMonths(-2));
                report.SetParameter("@M3", dt.AddMonths(-3));
                report.SetParameter("@ORDER_BY", " ORDER BY r.MONTH_TOTAL_USAGE DESC ");
                report.ViewReport("");
            }
            catch(Exception ex)
            {
                MsgBox.LogError(ex);
                MsgBox.ShowWarning(Base.Properties.Resources.YOU_CANNOT_VIEW_REPORT, Base.Properties.Resources.WARNING);
            }
        }

    }

    public class Usage_ListModel
    {
        public int CUSTOMER_ID { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }
        public string AREA_NAME { get; set; }
        public string POLE_CODE { get; set; }
        public string BOX_CODE { get; set; }
        public string METER_CODE { get; set; }
        public int PHASE_ID { get; set; }
        public string AMPARE_NAME { get; set; }
        public decimal START_USAGE { get; set; }
        public decimal END_USAGE { get; set; }
        public int MULTIPLIER { get; set; }
        public bool IS_METER_RENEW_CYCLE { get; set; }
        public string TAMPER_STATUS { get; set; }
        public decimal TOTAL_USAGE
        {
            get
            {
                return MULTIPLIER * (IS_METER_RENEW_CYCLE ?
                    (Convert.ToInt32(new String('9', Convert.ToInt32(START_USAGE).ToString().Length))) -
                    START_USAGE + 1 + END_USAGE : END_USAGE - START_USAGE);
            }
        } 
    }
}
