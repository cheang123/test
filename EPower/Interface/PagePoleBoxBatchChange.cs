﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PagePoleBoxBatchChange : Form
    {
        
        int areaID = 0;
        int transfoId = 0;
        
        #region Constructor
        public PagePoleBoxBatchChange()
        {
            InitializeComponent();
            txt_QuickSearch(null, null);
            txtSearchBox_QuickSearch(null, null);
            bind();
        }

        #endregion

        #region Method

        public void bind()
        {
            UIHelper.SetDataSourceToComboBox(cboArea, DBDataContext.Db.TBL_AREAs.Where(x => x.IS_ACTIVE), Resources.ALL_AREA);
            UIHelper.SetDataSourceToComboBox(cboTransfo, DBDataContext.Db.TBL_TRANSFORMERs.Where(x => x.IS_ACTIVE), Resources.ALL_TRANSFORMER);
        }

        private void loadPole()
        {
            try
            {
                if (cboArea.SelectedIndex != -1)
                {
                    areaID = (int)cboArea.SelectedValue;
                }
                if (cboTransfo.SelectedIndex != -1)
                {
                    transfoId = (int)cboTransfo.SelectedValue;
                }

                dgvPole.DataSource = from p in DBDataContext.Db.TBL_POLEs
                                     join a in DBDataContext.Db.TBL_AREAs on p.AREA_ID equals a.AREA_ID
                                     join t in DBDataContext.Db.TBL_TRANSFORMERs on p.TRANSFORMER_ID equals t.TRANSFORMER_ID
                                     where (p.IS_ACTIVE && a.IS_ACTIVE && (areaID == 0 || a.AREA_ID == areaID)) &&
                                            (transfoId == 0 || p.TRANSFORMER_ID == transfoId) &&
                                            (p.POLE_CODE.ToLower()).Contains(txtQuickSearch.Text.ToLower().Trim())
                                     orderby p.POLE_CODE
                                     select new
                                     {
                                         p.POLE_ID,
                                         p.POLE_CODE,
                                         a.AREA_NAME,
                                         t.TRANSFORMER_CODE
                                     };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void loadBox()
        {
            try
            {
                if (cboArea.SelectedIndex != -1)
                {
                    areaID = (int)cboArea.SelectedValue;
                }
                if (cboTransfo.SelectedIndex != -1)
                {
                    transfoId = (int)cboTransfo.SelectedValue;
                }

                dgvBox.DataSource = from b in DBDataContext.Db.TBL_BOXes
                                    join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                                    join a in DBDataContext.Db.TBL_AREAs on p.AREA_ID equals a.AREA_ID
                                    join t in DBDataContext.Db.TBL_TRANSFORMERs on p.TRANSFORMER_ID equals t.TRANSFORMER_ID
                                    where (p.IS_ACTIVE && a.IS_ACTIVE && (areaID == 0 || a.AREA_ID == areaID)) &&
                                           (transfoId == 0 || p.TRANSFORMER_ID == transfoId) &&
                                           (b.BOX_CODE.ToLower()).Contains(txtSearchBox.Text.ToLower().Trim())
                                    orderby p.POLE_CODE
                                    select new
                                    {
                                        b.BOX_ID,
                                        b.BOX_CODE
                                    };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        #endregion
        public int AreaID
        {
            get { return areaID; }
            private set { areaID = value; }
        }
        public void LoadArea()
        {
            int tempArea = AreaID;
            DataTable dt = (from a in DBDataContext.Db.TBL_AREAs
                            where a.IS_ACTIVE
                            orderby a.AREA_NAME
                            select a)._ToDataTable();
            //Create Row Templet
            DataRow dr = dt.NewRow();
            dr["AREA_ID"] = 0;
            dr["AREA_NAME"] = Resources.ALL_AREA;
            dt.Rows.InsertAt(dr, 0);

            UIHelper.SetDataSourceToComboBox(cboArea, dt);
            cboArea.SelectedValue = tempArea;
        }
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            loadPole();
        }
           
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtSearchBox_QuickSearch(object sender, EventArgs e)
        {
            loadBox();
        }

        private void cboArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadPole();
            loadBox();
        }

        private void btnPOLE_BATCH_CHANGE_Click(object sender, EventArgs e)
        {
            var pole = this.dgvPole.SelectedRows.Cast<DataGridViewRow>().Select(x => (int)x.Cells[POLE_ID.Name].Value).ToList();
            var diag = new Interface.DialogPoleBatchChange();
            diag.SetPole(pole);
            diag.ShowDialog();
        }

        private void btnBOX_BATCH_CHANGE_Click(object sender, EventArgs e)
        {
            var box= this.dgvBox.SelectedRows.Cast<DataGridViewRow>().Select(x => (int)x.Cells[BOX_ID.Name].Value).ToList();
            var diag = new Interface.DialogBoxBatchChange();
            diag.SetBox(box);
            diag.ShowDialog();
            if (diag.DialogResult == DialogResult.OK)
                loadBox();
        }

        private void dgvPole_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvPole.SelectedRows.Count > 0)
            {
                int intPoleId = int.Parse(dgvPole.SelectedRows[0].Cells["POLE_ID"].Value.ToString());
                var box = from b in DBDataContext.Db.TBL_BOXes
                          where b.POLE_ID == intPoleId
                          orderby b.BOX_CODE
                          select new { b.BOX_ID, b.BOX_CODE, b.STATUS_ID };
                
                dgvBox.DataSource = box;
                dgvBox.Columns[STATUS_ID.Name].Visible = false;

                if (box.Where(x => x.STATUS_ID != (int)BoxStatus.Used).Count() > 0)
                {
                    foreach (DataGridViewRow r in dgvBox.Rows)
                    {
                        int sBox = (int)r.Cells[STATUS_ID.Name].Value;
                        if (sBox == (int)BoxStatus.Unavailable)
                            r.DefaultCellStyle.ForeColor = Color.Red;
                        else if (sBox == (int)BoxStatus.Stock)
                            r.DefaultCellStyle.ForeColor = Color.Blue;
                    }
                }
            }
        }
    }
}
