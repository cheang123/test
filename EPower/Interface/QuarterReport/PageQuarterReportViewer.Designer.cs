﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageQuarterReportViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnMAIL = new SoftTech.Component.ExButton();
            this.btnSetup = new SoftTech.Component.ExButton();
            this.cboQuarter = new System.Windows.Forms.ComboBox();
            this.lblYEAR = new System.Windows.Forms.Label();
            this.lblQUARTER = new System.Windows.Forms.Label();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.lblPRINT_QUARTER_REPORT = new System.Windows.Forms.Label();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnMAIL);
            this.panel1.Controls.Add(this.btnSetup);
            this.panel1.Controls.Add(this.cboQuarter);
            this.panel1.Controls.Add(this.lblYEAR);
            this.panel1.Controls.Add(this.lblQUARTER);
            this.panel1.Controls.Add(this.cboYear);
            this.panel1.Controls.Add(this.btnVIEW);
            this.panel1.Controls.Add(this.lblPRINT_QUARTER_REPORT);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1058, 33);
            this.panel1.TabIndex = 7;
            // 
            // btnMAIL
            // 
            this.btnMAIL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMAIL.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnMAIL.Location = new System.Drawing.Point(984, 5);
            this.btnMAIL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMAIL.Name = "btnMAIL";
            this.btnMAIL.Size = new System.Drawing.Size(71, 23);
            this.btnMAIL.TabIndex = 16;
            this.btnMAIL.Text = "អ៊ីមែល";
            this.btnMAIL.UseVisualStyleBackColor = true;
            this.btnMAIL.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // btnSetup
            // 
            this.btnSetup.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnSetup.Location = new System.Drawing.Point(488, 5);
            this.btnSetup.Margin = new System.Windows.Forms.Padding(2);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(75, 23);
            this.btnSetup.TabIndex = 15;
            this.btnSetup.Text = "ការកំណត់";
            this.btnSetup.UseVisualStyleBackColor = true;
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // cboQuarter
            // 
            this.cboQuarter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboQuarter.FormattingEnabled = true;
            this.cboQuarter.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cboQuarter.Location = new System.Drawing.Point(428, 3);
            this.cboQuarter.Name = "cboQuarter";
            this.cboQuarter.Size = new System.Drawing.Size(55, 27);
            this.cboQuarter.TabIndex = 11;
            // 
            // lblYEAR
            // 
            this.lblYEAR.AutoSize = true;
            this.lblYEAR.Location = new System.Drawing.Point(238, 7);
            this.lblYEAR.Name = "lblYEAR";
            this.lblYEAR.Size = new System.Drawing.Size(45, 19);
            this.lblYEAR.TabIndex = 9;
            this.lblYEAR.Text = "ប្រចាំឆ្នាំ";
            // 
            // lblQUARTER
            // 
            this.lblQUARTER.AutoSize = true;
            this.lblQUARTER.Location = new System.Drawing.Point(353, 7);
            this.lblQUARTER.Name = "lblQUARTER";
            this.lblQUARTER.Size = new System.Drawing.Size(69, 19);
            this.lblQUARTER.TabIndex = 10;
            this.lblQUARTER.Text = "ប្រចាំត្រីមាស";
            // 
            // cboYear
            // 
            this.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Location = new System.Drawing.Point(285, 3);
            this.cboYear.Name = "cboYear";
            this.cboYear.Size = new System.Drawing.Size(65, 27);
            this.cboYear.TabIndex = 8;
            // 
            // btnVIEW
            // 
            this.btnVIEW.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVIEW.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVIEW.Location = new System.Drawing.Point(900, 4);
            this.btnVIEW.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.Size = new System.Drawing.Size(78, 23);
            this.btnVIEW.TabIndex = 6;
            this.btnVIEW.Text = "មើល";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnView_Click);
            // 
            // lblPRINT_QUARTER_REPORT
            // 
            this.lblPRINT_QUARTER_REPORT.AutoSize = true;
            this.lblPRINT_QUARTER_REPORT.Font = new System.Drawing.Font("Kh Muol", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPRINT_QUARTER_REPORT.Location = new System.Drawing.Point(3, 3);
            this.lblPRINT_QUARTER_REPORT.Name = "lblPRINT_QUARTER_REPORT";
            this.lblPRINT_QUARTER_REPORT.Size = new System.Drawing.Size(220, 27);
            this.lblPRINT_QUARTER_REPORT.TabIndex = 5;
            this.lblPRINT_QUARTER_REPORT.Text = "បោះពុម្ភរបាយការណ៍ត្រីមាស";
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            this.viewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewer.Location = new System.Drawing.Point(0, 33);
            this.viewer.Name = "viewer";
            this.viewer.Size = new System.Drawing.Size(1058, 390);
            this.viewer.TabIndex = 8;
            this.viewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // PageQuarterReportViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1058, 423);
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PageQuarterReportViewer";
            this.Text = "PageRunReport";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private Label lblPRINT_QUARTER_REPORT;
        private ExButton btnVIEW;
        private ComboBox cboQuarter;
        private Label lblYEAR;
        private Label lblQUARTER;
        private ComboBox cboYear;
        private ExButton btnSetup;
        private ExButton btnMAIL;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}