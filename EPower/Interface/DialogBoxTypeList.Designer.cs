﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogBoxTypeList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogBoxTypeList));
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.dgvBoxType = new System.Windows.Forms.DataGridView();
            this.BOX_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POLE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBoxType)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.dgvBoxType);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.dgvBoxType, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txtQuickSearch_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeEnglishKeyboard);
            // 
            // dgvBoxType
            // 
            this.dgvBoxType.AllowUserToAddRows = false;
            this.dgvBoxType.AllowUserToDeleteRows = false;
            this.dgvBoxType.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvBoxType.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvBoxType.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBoxType.BackgroundColor = System.Drawing.Color.White;
            this.dgvBoxType.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvBoxType.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvBoxType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBoxType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BOX_CODE,
            this.POLE_CODE,
            this.AREA_CODE,
            this.STATUS});
            resources.ApplyResources(this.dgvBoxType, "dgvBoxType");
            this.dgvBoxType.EnableHeadersVisualStyles = false;
            this.dgvBoxType.MultiSelect = false;
            this.dgvBoxType.Name = "dgvBoxType";
            this.dgvBoxType.ReadOnly = true;
            this.dgvBoxType.RowHeadersVisible = false;
            this.dgvBoxType.RowTemplate.Height = 25;
            this.dgvBoxType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // BOX_CODE
            // 
            this.BOX_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BOX_CODE.DataPropertyName = "BOX_CODE";
            resources.ApplyResources(this.BOX_CODE, "BOX_CODE");
            this.BOX_CODE.Name = "BOX_CODE";
            this.BOX_CODE.ReadOnly = true;
            // 
            // POLE_CODE
            // 
            this.POLE_CODE.DataPropertyName = "POLE_CODE";
            resources.ApplyResources(this.POLE_CODE, "POLE_CODE");
            this.POLE_CODE.Name = "POLE_CODE";
            this.POLE_CODE.ReadOnly = true;
            // 
            // AREA_CODE
            // 
            this.AREA_CODE.DataPropertyName = "AREA_CODE";
            resources.ApplyResources(this.AREA_CODE, "AREA_CODE");
            this.AREA_CODE.Name = "AREA_CODE";
            this.AREA_CODE.ReadOnly = true;
            // 
            // STATUS
            // 
            this.STATUS.DataPropertyName = "STATUS_NAME";
            resources.ApplyResources(this.STATUS, "STATUS");
            this.STATUS.Name = "STATUS";
            this.STATUS.ReadOnly = true;
            // 
            // DialogBoxTypeList
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogBoxTypeList";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBoxType)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private DataGridView dgvBoxType;
        private DataGridViewTextBoxColumn BOX_CODE;
        private DataGridViewTextBoxColumn POLE_CODE;
        private DataGridViewTextBoxColumn AREA_CODE;
        private DataGridViewTextBoxColumn STATUS;
    }
}