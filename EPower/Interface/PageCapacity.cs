﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class PageCapacity : Form
    {
        public PageCapacity()
        {
            InitializeComponent();
            bind(); 
        }

        #region Method
        private void bind()
        {
            btnADD.Visible = false;
            btnEDIT.Visible = false;
            btnREMOVE.Visible = false;
            try
            {
                dgv.DataSource = (from ca in DBDataContext.Db.TBL_CAPACITies
                                  where ca.IS_ACTIVE &&
                                  ca.CAPACITY.ToString().ToLower().Contains(txtSearch.Text.ToLower().Trim())
                                  select ca)
                                  .ToList()
                                  .OrderBy(x => Lookup.ParseCapacity(x.CAPACITY))
                                  .ToList();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }     
        }

        #endregion

        #region Operation
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                int id = (int) this.dgv.SelectedRows[0].Cells[0].Value;
                TBL_CAPACITY obj = DBDataContext.Db.TBL_CAPACITies.Single(row => row.CAPACITY_ID == id);
                
                DialogCapacity diag = new DialogCapacity(obj, GeneralProcess.Update);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    this.bind();
                    UIHelper.SelectRow(this.dgv,diag.Capacity.CAPACITY_ID);
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogCapacity diag = new DialogCapacity(new TBL_CAPACITY()
            {
                CAPACITY_ID=0,
                CAPACITY="",
                IS_ACTIVE=true 
            }, GeneralProcess.Insert);

            if (diag.ShowDialog() == DialogResult.OK)
            {
                bind();
                UIHelper.SelectRow(this.dgv,diag.Capacity.CAPACITY_ID);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count > 0)
            {
                int id = (int)this.dgv.SelectedRows[0].Cells[0].Value;
                if (isDeletable(id))
                {
                    TBL_CAPACITY obj = DBDataContext.Db.TBL_CAPACITies.Single(row => row.CAPACITY_ID == id);
                    DialogCapacity diag = new DialogCapacity(obj, GeneralProcess.Delete);
                    if (diag.ShowDialog() == DialogResult.OK)
                    {
                        this.bind();
                        UIHelper.SelectRow(this.dgv, diag.Capacity.CAPACITY_ID - 1);
                    }
                }
                else
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE); 
                }
            }
        }

        private void txtSearch_QuickSearch(object sender, EventArgs e)
        {
            bind();
        }

        private bool isDeletable(int id)
        {
            return DBDataContext.Db.TBL_TRANSFORMERs.Count(row => row.IS_ACTIVE && row.CAPACITY_ID == id) == 0;                    
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.RowIndex == -1)
            //{
            //    return;
            //}
            //btnEdit_Click(null, null);
        }

        #endregion


    }
}
