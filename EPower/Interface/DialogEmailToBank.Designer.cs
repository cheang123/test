﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogEmailToBank
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogEmailToBank));
            this.lblADDRESS = new System.Windows.Forms.Label();
            this.lblATTACHMENT = new System.Windows.Forms.Label();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblADD = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.FILE_PATH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FILE_SIZE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblREMOVE = new System.Windows.Forms.Label();
            this.cboBank = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.lblBANK = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBank.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.BackColor = System.Drawing.Color.White;
            this.content.Controls.Add(this.lblBANK);
            this.content.Controls.Add(this.cboBank);
            this.content.Controls.Add(this.lblREMOVE);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.txtAddress);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblADD);
            this.content.Controls.Add(this.lblATTACHMENT);
            this.content.Controls.Add(this.lblADDRESS);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblADDRESS, 0);
            this.content.Controls.SetChildIndex(this.lblATTACHMENT, 0);
            this.content.Controls.SetChildIndex(this.lblADD, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.txtAddress, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.lblREMOVE, 0);
            this.content.Controls.SetChildIndex(this.cboBank, 0);
            this.content.Controls.SetChildIndex(this.lblBANK, 0);
            // 
            // lblADDRESS
            // 
            resources.ApplyResources(this.lblADDRESS, "lblADDRESS");
            this.lblADDRESS.Name = "lblADDRESS";
            // 
            // lblATTACHMENT
            // 
            resources.ApplyResources(this.lblATTACHMENT, "lblATTACHMENT");
            this.lblATTACHMENT.Name = "lblATTACHMENT";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblADD
            // 
            resources.ApplyResources(this.lblADD, "lblADD");
            this.lblADD.Name = "lblADD";
            this.lblADD.Click += new System.EventHandler(this.lblADD_Click);
            // 
            // txtAddress
            // 
            resources.ApplyResources(this.txtAddress, "txtAddress");
            this.txtAddress.Name = "txtAddress";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FILE_PATH,
            this.FILE_SIZE});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // FILE_PATH
            // 
            this.FILE_PATH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FILE_PATH.DataPropertyName = "FILE_PATH";
            resources.ApplyResources(this.FILE_PATH, "FILE_PATH");
            this.FILE_PATH.Name = "FILE_PATH";
            this.FILE_PATH.ReadOnly = true;
            this.FILE_PATH.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // FILE_SIZE
            // 
            this.FILE_SIZE.DataPropertyName = "FILE_SIZE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.FILE_SIZE.DefaultCellStyle = dataGridViewCellStyle2;
            this.FILE_SIZE.FillWeight = 150F;
            resources.ApplyResources(this.FILE_SIZE, "FILE_SIZE");
            this.FILE_SIZE.Name = "FILE_SIZE";
            this.FILE_SIZE.ReadOnly = true;
            this.FILE_SIZE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // lblREMOVE
            // 
            resources.ApplyResources(this.lblREMOVE, "lblREMOVE");
            this.lblREMOVE.Name = "lblREMOVE";
            this.lblREMOVE.Click += new System.EventHandler(this.lblREMOVE_Click);
            // 
            // cboBank
            // 
            this.cboBank.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.cboBank, "cboBank");
            this.cboBank.Name = "cboBank";
            this.cboBank.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboBank.Properties.Appearance.Font")));
            this.cboBank.Properties.Appearance.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject1, "serializableAppearanceObject1");
            serializableAppearanceObject1.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject2, "serializableAppearanceObject2");
            serializableAppearanceObject2.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject3, "serializableAppearanceObject3");
            serializableAppearanceObject3.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject4, "serializableAppearanceObject4");
            serializableAppearanceObject4.Options.UseFont = true;
            this.cboBank.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboBank.Properties.Buttons"))), resources.GetString("cboBank.Properties.Buttons1"), ((int)(resources.GetObject("cboBank.Properties.Buttons2"))), ((bool)(resources.GetObject("cboBank.Properties.Buttons3"))), ((bool)(resources.GetObject("cboBank.Properties.Buttons4"))), ((bool)(resources.GetObject("cboBank.Properties.Buttons5"))), editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, resources.GetString("cboBank.Properties.Buttons6"), ((object)(resources.GetObject("cboBank.Properties.Buttons7"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("cboBank.Properties.Buttons8"))), ((DevExpress.Utils.ToolTipAnchor)(resources.GetObject("cboBank.Properties.Buttons9"))))});
            this.cboBank.Properties.DropDownRows = 10;
            this.cboBank.Properties.LookAndFeel.SkinName = "DevExpress Style";
            this.cboBank.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cboBank.Properties.Tag = "";
            // 
            // lblBANK
            // 
            resources.ApplyResources(this.lblBANK, "lblBANK");
            this.lblBANK.Name = "lblBANK";
            // 
            // DialogEmailToBank
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogEmailToBank";
            this.Load += new System.EventHandler(this.DialogEmailToBank_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBank.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblATTACHMENT;
        private Label lblADDRESS;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblADD;
        private TextBox txtAddress;
        private DataGridView dgv;
        private Label lblREMOVE;
        private DataGridViewTextBoxColumn FILE_PATH;
        private DataGridViewTextBoxColumn FILE_SIZE;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cboBank;
        private Label lblBANK;
    }
}