﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogEAC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogEAC));
            this.btnSEND = new SoftTech.Component.ExButton();
            this.txtLOG = new System.Windows.Forms.TextBox();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.lblSTATUS1 = new System.Windows.Forms.Label();
            this.chkAUTO_SENDING = new System.Windows.Forms.CheckBox();
            this.chkSHOW_PROCESS = new System.Windows.Forms.CheckBox();
            this.lblLastSuccessUpload_ = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblLastSuccessUpload_);
            this.content.Controls.Add(this.chkSHOW_PROCESS);
            this.content.Controls.Add(this.chkAUTO_SENDING);
            this.content.Controls.Add(this.lblSTATUS1);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.txtLOG);
            this.content.Controls.Add(this.btnSEND);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnSEND, 0);
            this.content.Controls.SetChildIndex(this.txtLOG, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS1, 0);
            this.content.Controls.SetChildIndex(this.chkAUTO_SENDING, 0);
            this.content.Controls.SetChildIndex(this.chkSHOW_PROCESS, 0);
            this.content.Controls.SetChildIndex(this.lblLastSuccessUpload_, 0);
            // 
            // btnSEND
            // 
            resources.ApplyResources(this.btnSEND, "btnSEND");
            this.btnSEND.Name = "btnSEND";
            this.btnSEND.UseVisualStyleBackColor = true;
            this.btnSEND.Click += new System.EventHandler(this.btnSEND_Click);
            // 
            // txtLOG
            // 
            this.txtLOG.BackColor = System.Drawing.SystemColors.Window;
            resources.ApplyResources(this.txtLOG, "txtLOG");
            this.txtLOG.Name = "txtLOG";
            this.txtLOG.ReadOnly = true;
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // lblSTATUS1
            // 
            resources.ApplyResources(this.lblSTATUS1, "lblSTATUS1");
            this.lblSTATUS1.ForeColor = System.Drawing.Color.Blue;
            this.lblSTATUS1.Name = "lblSTATUS1";
            // 
            // chkAUTO_SENDING
            // 
            resources.ApplyResources(this.chkAUTO_SENDING, "chkAUTO_SENDING");
            this.chkAUTO_SENDING.Name = "chkAUTO_SENDING";
            this.chkAUTO_SENDING.UseVisualStyleBackColor = true;
            // 
            // chkSHOW_PROCESS
            // 
            resources.ApplyResources(this.chkSHOW_PROCESS, "chkSHOW_PROCESS");
            this.chkSHOW_PROCESS.Checked = true;
            this.chkSHOW_PROCESS.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSHOW_PROCESS.Name = "chkSHOW_PROCESS";
            this.chkSHOW_PROCESS.UseVisualStyleBackColor = true;
            this.chkSHOW_PROCESS.CheckedChanged += new System.EventHandler(this.chkSHOW_PROCESS_CheckedChanged);
            // 
            // lblLastSuccessUpload_
            // 
            resources.ApplyResources(this.lblLastSuccessUpload_, "lblLastSuccessUpload_");
            this.lblLastSuccessUpload_.Name = "lblLastSuccessUpload_";
            this.lblLastSuccessUpload_.Click += new System.EventHandler(this.lblLastSuccessUpload__Click);
            // 
            // DialogEAC
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogEAC";
            this.Shown += new System.EventHandler(this.DialogEAC_Shown);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtLOG;
        private ExButton btnSEND;
        private CheckBox chkSHOW_PROCESS;
        private CheckBox chkAUTO_SENDING;
        private Label lblSTATUS1;
        private Label lblSTATUS;
        private Label lblLastSuccessUpload_;

    }
}