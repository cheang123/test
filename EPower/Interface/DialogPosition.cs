﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogPosition : ExDialog
    {
        GeneralProcess _flag;
        TBL_POSITION _objNew = new TBL_POSITION();
        public TBL_POSITION Position
        {
            get { return _objNew; }
        }
        TBL_POSITION _objOld = new TBL_POSITION();

        #region Constructor
        public DialogPosition(GeneralProcess flag, TBL_POSITION objArea)
        {
            InitializeComponent();
            _flag = flag;
            objArea._CopyTo(_objNew);
            objArea._CopyTo(_objOld);
            UIHelper.SetDataSourceToComboBox(cboValue, Lookup.GetLookUpValue((int)LookUp.BUSINESS_DIVISION));
            this.Text = flag.GetText(this.Text);
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            UIHelper.SetEnabled(this, _flag == GeneralProcess.Delete ? false : true);
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //If record is duplicate.
            txtPositionName.ClearValidation();
            if (DBDataContext.Db.IsExits(_objNew, "POSITION_NAME"))
            {
                txtPositionName.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblPOSITION.Text));
                return;
            }

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtPositionName.Text.Trim().Length <= 0)
            {
                txtPositionName.SetValidation(string.Format(Resources.REQUIRED, lblPOSITION.Text));
                val = true;
            }

            if (cboValue.SelectedIndex == -1)
            {
                cboValue.SetValidation(string.Format(Resources.REQUIRED, lblJOB_DEPARTMENT.Text));
                val = true;
            }
            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            txtPositionName.Text = _objNew.POSITION_NAME;
            cboValue.SelectedValue = _objNew.BUSINESS_DIVISION_ID;
            chkIS_PAYROLL.Checked = _objNew.IS_PAYROLL;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.POSITION_NAME = txtPositionName.Text.Trim();
            _objNew.BUSINESS_DIVISION_ID = (int)cboValue.SelectedValue;
            _objNew.IS_PAYROLL = chkIS_PAYROLL.Checked;
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }


    }
}