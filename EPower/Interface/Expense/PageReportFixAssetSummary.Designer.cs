﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageReportFixAsset
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportFixAsset));
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtp = new System.Windows.Forms.DateTimePicker();
            this.lblSHOW_AS = new System.Windows.Forms.Label();
            this.cboDisplayCurrency = new System.Windows.Forms.ComboBox();
            this.cboPaymentAccount = new SoftTech.Component.TreeComboBox();
            this.cboCategory = new SoftTech.Component.TreeComboBox();
            this.cboTranCurrency = new System.Windows.Forms.ComboBox();
            this.btnMAIL = new SoftTech.Component.ExButton();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.dtp);
            this.panel1.Controls.Add(this.lblSHOW_AS);
            this.panel1.Controls.Add(this.cboDisplayCurrency);
            this.panel1.Controls.Add(this.cboPaymentAccount);
            this.panel1.Controls.Add(this.cboCategory);
            this.panel1.Controls.Add(this.cboTranCurrency);
            this.panel1.Controls.Add(this.btnMAIL);
            this.panel1.Controls.Add(this.btnVIEW);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // dtp
            // 
            resources.ApplyResources(this.dtp, "dtp");
            this.dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp.Name = "dtp";
            this.dtp.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblSHOW_AS
            // 
            resources.ApplyResources(this.lblSHOW_AS, "lblSHOW_AS");
            this.lblSHOW_AS.Name = "lblSHOW_AS";
            // 
            // cboDisplayCurrency
            // 
            this.cboDisplayCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDisplayCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboDisplayCurrency, "cboDisplayCurrency");
            this.cboDisplayCurrency.Name = "cboDisplayCurrency";
            // 
            // cboPaymentAccount
            // 
            this.cboPaymentAccount.AbsoluteChildrenSelectableOnly = true;
            this.cboPaymentAccount.BranchSeparator = "/";
            this.cboPaymentAccount.Imagelist = null;
            resources.ApplyResources(this.cboPaymentAccount, "cboPaymentAccount");
            this.cboPaymentAccount.Name = "cboPaymentAccount";
            this.cboPaymentAccount.PopupHeight = 250;
            this.cboPaymentAccount.PopupWidth = 350;
            this.cboPaymentAccount.SelectedNode = null;
            // 
            // cboCategory
            // 
            this.cboCategory.AbsoluteChildrenSelectableOnly = false;
            this.cboCategory.BranchSeparator = "/";
            this.cboCategory.Imagelist = null;
            resources.ApplyResources(this.cboCategory, "cboCategory");
            this.cboCategory.Name = "cboCategory";
            this.cboCategory.PopupHeight = 250;
            this.cboCategory.PopupWidth = 350;
            this.cboCategory.SelectedNode = null;
            // 
            // cboTranCurrency
            // 
            this.cboTranCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTranCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboTranCurrency, "cboTranCurrency");
            this.cboTranCurrency.Name = "cboTranCurrency";
            // 
            // btnMAIL
            // 
            resources.ApplyResources(this.btnMAIL, "btnMAIL");
            this.btnMAIL.Name = "btnMAIL";
            this.btnMAIL.UseVisualStyleBackColor = true;
            this.btnMAIL.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // btnVIEW
            // 
            resources.ApplyResources(this.btnVIEW, "btnVIEW");
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Name = "viewer";
            // 
            // PageReportFixAsset
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportFixAsset";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnVIEW;
        private ExButton btnMAIL;
        public ComboBox cboTranCurrency;
        private TreeComboBox cboCategory;
        private TreeComboBox cboPaymentAccount;
        private Label lblSHOW_AS;
        public ComboBox cboDisplayCurrency;
        private DateTimePicker dtp;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}
