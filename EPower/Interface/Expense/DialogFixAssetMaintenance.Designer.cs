﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogFixAssetRepairMaintenance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogFixAssetRepairMaintenance));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnAdd = new SoftTech.Component.ExButton();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblPaymentAccount = new System.Windows.Forms.Label();
            this.cboPaymentAccount = new SoftTech.Component.TreeComboBox();
            this.txtTranBy = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.lblRefNo = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.lblSoldAmount = new System.Windows.Forms.Label();
            this.dtpTranDate = new System.Windows.Forms.DateTimePicker();
            this.lblTranDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTranBy = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNote = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFixAssetName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cboAccountExpense = new SoftTech.Component.TreeComboBox();
            this.lblAccountExpense = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.label17);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.lblAccountExpense);
            this.content.Controls.Add(this.lblPaymentAccount);
            this.content.Controls.Add(this.cboAccountExpense);
            this.content.Controls.Add(this.cboPaymentAccount);
            this.content.Controls.Add(this.txtFixAssetName);
            this.content.Controls.Add(this.txtTranBy);
            this.content.Controls.Add(this.label15);
            this.content.Controls.Add(this.label8);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.label16);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.label11);
            this.content.Controls.Add(this.txtRefNo);
            this.content.Controls.Add(this.lblRefNo);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.txtAmount);
            this.content.Controls.Add(this.lblSoldAmount);
            this.content.Controls.Add(this.dtpTranDate);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.lblTranDate);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.lblTranBy);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.lblNote);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnAdd);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnAdd, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblNote, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.lblTranBy, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.lblTranDate, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.dtpTranDate, 0);
            this.content.Controls.SetChildIndex(this.lblSoldAmount, 0);
            this.content.Controls.SetChildIndex(this.txtAmount, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.lblRefNo, 0);
            this.content.Controls.SetChildIndex(this.txtRefNo, 0);
            this.content.Controls.SetChildIndex(this.label11, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.label16, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label8, 0);
            this.content.Controls.SetChildIndex(this.label15, 0);
            this.content.Controls.SetChildIndex(this.txtTranBy, 0);
            this.content.Controls.SetChildIndex(this.txtFixAssetName, 0);
            this.content.Controls.SetChildIndex(this.cboPaymentAccount, 0);
            this.content.Controls.SetChildIndex(this.cboAccountExpense, 0);
            this.content.Controls.SetChildIndex(this.lblPaymentAccount, 0);
            this.content.Controls.SetChildIndex(this.lblAccountExpense, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.label17, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAdd
            // 
            resources.ApplyResources(this.btnAdd, "btnAdd");
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.Items.AddRange(new object[] {
            resources.GetString("cboCurrency.Items"),
            resources.GetString("cboCurrency.Items1"),
            resources.GetString("cboCurrency.Items2")});
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // lblPaymentAccount
            // 
            resources.ApplyResources(this.lblPaymentAccount, "lblPaymentAccount");
            this.lblPaymentAccount.Name = "lblPaymentAccount";
            // 
            // cboPaymentAccount
            // 
            this.cboPaymentAccount.AbsoluteChildrenSelectableOnly = true;
            this.cboPaymentAccount.BranchSeparator = "/";
            this.cboPaymentAccount.Imagelist = null;
            resources.ApplyResources(this.cboPaymentAccount, "cboPaymentAccount");
            this.cboPaymentAccount.Name = "cboPaymentAccount";
            this.cboPaymentAccount.PopupHeight = 250;
            this.cboPaymentAccount.PopupWidth = 350;
            this.cboPaymentAccount.SelectedNode = null;
            // 
            // txtTranBy
            // 
            this.txtTranBy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTranBy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtTranBy, "txtTranBy");
            this.txtTranBy.Name = "txtTranBy";
            this.txtTranBy.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Name = "label15";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // txtRefNo
            // 
            resources.ApplyResources(this.txtRefNo, "txtRefNo");
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblRefNo
            // 
            resources.ApplyResources(this.lblRefNo, "lblRefNo");
            this.lblRefNo.Name = "lblRefNo";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // txtAmount
            // 
            resources.ApplyResources(this.txtAmount, "txtAmount");
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // lblSoldAmount
            // 
            resources.ApplyResources(this.lblSoldAmount, "lblSoldAmount");
            this.lblSoldAmount.Name = "lblSoldAmount";
            // 
            // dtpTranDate
            // 
            resources.ApplyResources(this.dtpTranDate, "dtpTranDate");
            this.dtpTranDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTranDate.Name = "dtpTranDate";
            this.dtpTranDate.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblTranDate
            // 
            resources.ApplyResources(this.lblTranDate, "lblTranDate");
            this.lblTranDate.Name = "lblTranDate";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // lblTranBy
            // 
            resources.ApplyResources(this.lblTranBy, "lblTranBy");
            this.lblTranBy.Name = "lblTranBy";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // lblNote
            // 
            resources.ApplyResources(this.lblNote, "lblNote");
            this.lblNote.Name = "lblNote";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // txtFixAssetName
            // 
            this.txtFixAssetName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtFixAssetName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtFixAssetName, "txtFixAssetName");
            this.txtFixAssetName.Name = "txtFixAssetName";
            this.txtFixAssetName.ReadOnly = true;
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Name = "label8";
            // 
            // cboAccountExpense
            // 
            this.cboAccountExpense.AbsoluteChildrenSelectableOnly = true;
            this.cboAccountExpense.BranchSeparator = "/";
            this.cboAccountExpense.Imagelist = null;
            resources.ApplyResources(this.cboAccountExpense, "cboAccountExpense");
            this.cboAccountExpense.Name = "cboAccountExpense";
            this.cboAccountExpense.PopupHeight = 250;
            this.cboAccountExpense.PopupWidth = 350;
            this.cboAccountExpense.SelectedNode = null;
            // 
            // lblAccountExpense
            // 
            resources.ApplyResources(this.lblAccountExpense, "lblAccountExpense");
            this.lblAccountExpense.Name = "lblAccountExpense";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // DialogFixAssetRepairMaintenance
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogFixAssetRepairMaintenance";
            this.Load += new System.EventHandler(this.DialogAccountCategory_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnClose;
        private ExButton btnAdd;
        private ComboBox cboCurrency;
        private Label label6;
        private Label lblPaymentAccount;
        private TreeComboBox cboPaymentAccount;
        private TextBox txtTranBy;
        private Label label15;
        private Label label14;
        private Label label13;
        private Label label5;
        private Label label11;
        private TextBox txtRefNo;
        private Label lblRefNo;
        private Label label9;
        private TextBox txtAmount;
        private Label lblSoldAmount;
        private DateTimePicker dtpTranDate;
        private Label lblTranDate;
        private Label label2;
        private Label lblTranBy;
        private TextBox txtNote;
        private Label label3;
        private Label lblNote;
        private TextBox txtFixAssetName;
        private Label label7;
        private Label label4;
        private Label label16;
        private Label label12;
        private Label label1;
        private Label label17;
        private Label lblAccountExpense;
        private TreeComboBox cboAccountExpense;
        private Label label8;
    }
}