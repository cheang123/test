﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogLoan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogLoan));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblBANK = new System.Windows.Forms.Label();
            this.lblLOAN_DATE = new System.Windows.Forms.Label();
            this.dtpLoanDate = new System.Windows.Forms.DateTimePicker();
            this.txtLoanAmount = new System.Windows.Forms.TextBox();
            this.lblCLOSE_DATE_I = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtBankNo = new System.Windows.Forms.TextBox();
            this.cboTransAccount = new SoftTech.Component.TreeComboBox();
            this.lblLOAN_ACCOUNT = new System.Windows.Forms.Label();
            this.cboPaymentAccount = new SoftTech.Component.TreeComboBox();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblLOAN_AMOUNT = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpCloseDate = new System.Windows.Forms.DateTimePicker();
            this.lblLOAN_NO = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtLoanNo = new System.Windows.Forms.TextBox();
            this.cboBusinessDivension = new System.Windows.Forms.ComboBox();
            this.lblBUSINESS_DIVISION = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblANNUAL_INTEREST_RATE = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtInterestRate = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabLOAD_INFORMATION = new System.Windows.Forms.TabPage();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.lblPAID_AMOUNT = new System.Windows.Forms.Label();
            this.txtLoanPayment = new System.Windows.Forms.TextBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.tabPAYMENT = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnADD = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnEDIT = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnREMOVE = new SoftTech.Component.ExLinkLabel(this.components);
            this.label21 = new System.Windows.Forms.Label();
            this.txtTotalCapital = new System.Windows.Forms.TextBox();
            this.txtTotalInterest = new System.Windows.Forms.TextBox();
            this.txtPayment = new System.Windows.Forms.TextBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.PAYMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EXPENSE_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENT_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CAPITAL_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INTEREST_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAY_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabLOAD_INFORMATION.SuspendLayout();
            this.tabPAYMENT.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.tabControl1);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.tabControl1, 0);
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblBANK
            // 
            resources.ApplyResources(this.lblBANK, "lblBANK");
            this.lblBANK.Name = "lblBANK";
            // 
            // lblLOAN_DATE
            // 
            resources.ApplyResources(this.lblLOAN_DATE, "lblLOAN_DATE");
            this.lblLOAN_DATE.Name = "lblLOAN_DATE";
            // 
            // dtpLoanDate
            // 
            resources.ApplyResources(this.dtpLoanDate, "dtpLoanDate");
            this.dtpLoanDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLoanDate.Name = "dtpLoanDate";
            this.dtpLoanDate.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // txtLoanAmount
            // 
            resources.ApplyResources(this.txtLoanAmount, "txtLoanAmount");
            this.txtLoanAmount.Name = "txtLoanAmount";
            this.txtLoanAmount.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtLoanAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterestRate_KeyPress);
            // 
            // lblCLOSE_DATE_I
            // 
            resources.ApplyResources(this.lblCLOSE_DATE_I, "lblCLOSE_DATE_I");
            this.lblCLOSE_DATE_I.Name = "lblCLOSE_DATE_I";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // txtBankNo
            // 
            this.txtBankNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtBankNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtBankNo, "txtBankNo");
            this.txtBankNo.Name = "txtBankNo";
            this.txtBankNo.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // cboTransAccount
            // 
            this.cboTransAccount.AbsoluteChildrenSelectableOnly = true;
            this.cboTransAccount.BranchSeparator = "/";
            this.cboTransAccount.Imagelist = null;
            resources.ApplyResources(this.cboTransAccount, "cboTransAccount");
            this.cboTransAccount.Name = "cboTransAccount";
            this.cboTransAccount.PopupHeight = 250;
            this.cboTransAccount.PopupWidth = 400;
            this.cboTransAccount.SelectedNode = null;
            // 
            // lblLOAN_ACCOUNT
            // 
            resources.ApplyResources(this.lblLOAN_ACCOUNT, "lblLOAN_ACCOUNT");
            this.lblLOAN_ACCOUNT.Name = "lblLOAN_ACCOUNT";
            // 
            // cboPaymentAccount
            // 
            this.cboPaymentAccount.AbsoluteChildrenSelectableOnly = true;
            this.cboPaymentAccount.BranchSeparator = "/";
            this.cboPaymentAccount.Imagelist = null;
            resources.ApplyResources(this.cboPaymentAccount, "cboPaymentAccount");
            this.cboPaymentAccount.Name = "cboPaymentAccount";
            this.cboPaymentAccount.PopupHeight = 250;
            this.cboPaymentAccount.PopupWidth = 0;
            this.cboPaymentAccount.SelectedNode = null;
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.Items.AddRange(new object[] {
            resources.GetString("cboCurrency.Items"),
            resources.GetString("cboCurrency.Items1"),
            resources.GetString("cboCurrency.Items2")});
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // lblLOAN_AMOUNT
            // 
            resources.ApplyResources(this.lblLOAN_AMOUNT, "lblLOAN_AMOUNT");
            this.lblLOAN_AMOUNT.Name = "lblLOAN_AMOUNT";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // dtpCloseDate
            // 
            resources.ApplyResources(this.dtpCloseDate, "dtpCloseDate");
            this.dtpCloseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCloseDate.Name = "dtpCloseDate";
            this.dtpCloseDate.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblLOAN_NO
            // 
            resources.ApplyResources(this.lblLOAN_NO, "lblLOAN_NO");
            this.lblLOAN_NO.Name = "lblLOAN_NO";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Name = "label10";
            // 
            // txtLoanNo
            // 
            this.txtLoanNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtLoanNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtLoanNo, "txtLoanNo");
            this.txtLoanNo.Name = "txtLoanNo";
            this.txtLoanNo.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // cboBusinessDivension
            // 
            this.cboBusinessDivension.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBusinessDivension.FormattingEnabled = true;
            this.cboBusinessDivension.Items.AddRange(new object[] {
            resources.GetString("cboBusinessDivension.Items"),
            resources.GetString("cboBusinessDivension.Items1"),
            resources.GetString("cboBusinessDivension.Items2")});
            resources.ApplyResources(this.cboBusinessDivension, "cboBusinessDivension");
            this.cboBusinessDivension.Name = "cboBusinessDivension";
            // 
            // lblBUSINESS_DIVISION
            // 
            resources.ApplyResources(this.lblBUSINESS_DIVISION, "lblBUSINESS_DIVISION");
            this.lblBUSINESS_DIVISION.Name = "lblBUSINESS_DIVISION";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Name = "label16";
            // 
            // lblANNUAL_INTEREST_RATE
            // 
            resources.ApplyResources(this.lblANNUAL_INTEREST_RATE, "lblANNUAL_INTEREST_RATE");
            this.lblANNUAL_INTEREST_RATE.Name = "lblANNUAL_INTEREST_RATE";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Name = "label19";
            // 
            // txtInterestRate
            // 
            this.txtInterestRate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtInterestRate.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtInterestRate, "txtInterestRate");
            this.txtInterestRate.Name = "txtInterestRate";
            this.txtInterestRate.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtInterestRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterestRate_KeyPress);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabLOAD_INFORMATION);
            this.tabControl1.Controls.Add(this.tabPAYMENT);
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            // 
            // tabLOAD_INFORMATION
            // 
            this.tabLOAD_INFORMATION.Controls.Add(this.lblLOAN_NO);
            this.tabLOAD_INFORMATION.Controls.Add(this.lblNOTE);
            this.tabLOAD_INFORMATION.Controls.Add(this.lblBANK);
            this.tabLOAD_INFORMATION.Controls.Add(this.lblANNUAL_INTEREST_RATE);
            this.tabLOAD_INFORMATION.Controls.Add(this.label1);
            this.tabLOAD_INFORMATION.Controls.Add(this.cboBusinessDivension);
            this.tabLOAD_INFORMATION.Controls.Add(this.cboCurrency);
            this.tabLOAD_INFORMATION.Controls.Add(this.lblBUSINESS_DIVISION);
            this.tabLOAD_INFORMATION.Controls.Add(this.lblLOAN_DATE);
            this.tabLOAD_INFORMATION.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.tabLOAD_INFORMATION.Controls.Add(this.cboPaymentAccount);
            this.tabLOAD_INFORMATION.Controls.Add(this.dtpLoanDate);
            this.tabLOAD_INFORMATION.Controls.Add(this.lblLOAN_ACCOUNT);
            this.tabLOAD_INFORMATION.Controls.Add(this.dtpCloseDate);
            this.tabLOAD_INFORMATION.Controls.Add(this.cboTransAccount);
            this.tabLOAD_INFORMATION.Controls.Add(this.lblPAID_AMOUNT);
            this.tabLOAD_INFORMATION.Controls.Add(this.lblLOAN_AMOUNT);
            this.tabLOAD_INFORMATION.Controls.Add(this.txtLoanNo);
            this.tabLOAD_INFORMATION.Controls.Add(this.txtLoanPayment);
            this.tabLOAD_INFORMATION.Controls.Add(this.txtLoanAmount);
            this.tabLOAD_INFORMATION.Controls.Add(this.txtInterestRate);
            this.tabLOAD_INFORMATION.Controls.Add(this.txtNote);
            this.tabLOAD_INFORMATION.Controls.Add(this.txtBankNo);
            this.tabLOAD_INFORMATION.Controls.Add(this.lblCLOSE_DATE_I);
            this.tabLOAD_INFORMATION.Controls.Add(this.label10);
            this.tabLOAD_INFORMATION.Controls.Add(this.label16);
            this.tabLOAD_INFORMATION.Controls.Add(this.label19);
            this.tabLOAD_INFORMATION.Controls.Add(this.label13);
            this.tabLOAD_INFORMATION.Controls.Add(this.label14);
            resources.ApplyResources(this.tabLOAD_INFORMATION, "tabLOAD_INFORMATION");
            this.tabLOAD_INFORMATION.Name = "tabLOAD_INFORMATION";
            this.tabLOAD_INFORMATION.UseVisualStyleBackColor = true;
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // lblPAID_AMOUNT
            // 
            resources.ApplyResources(this.lblPAID_AMOUNT, "lblPAID_AMOUNT");
            this.lblPAID_AMOUNT.Name = "lblPAID_AMOUNT";
            // 
            // txtLoanPayment
            // 
            resources.ApplyResources(this.txtLoanPayment, "txtLoanPayment");
            this.txtLoanPayment.Name = "txtLoanPayment";
            this.txtLoanPayment.ReadOnly = true;
            this.txtLoanPayment.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // txtNote
            // 
            this.txtNote.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtNote.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // tabPAYMENT
            // 
            this.tabPAYMENT.Controls.Add(this.panel1);
            this.tabPAYMENT.Controls.Add(this.label21);
            this.tabPAYMENT.Controls.Add(this.txtTotalCapital);
            this.tabPAYMENT.Controls.Add(this.txtTotalInterest);
            this.tabPAYMENT.Controls.Add(this.txtPayment);
            this.tabPAYMENT.Controls.Add(this.dgv);
            resources.ApplyResources(this.tabPAYMENT, "tabPAYMENT");
            this.tabPAYMENT.Name = "tabPAYMENT";
            this.tabPAYMENT.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.btnREMOVE);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD.Name = "btnADD";
            this.btnADD.TabStop = true;
            this.btnADD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblAdd_LinkClicked);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.TabStop = true;
            this.btnEDIT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblEdit_LinkClicked);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.TabStop = true;
            this.btnREMOVE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblDelete_LinkClicked);
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // txtTotalCapital
            // 
            resources.ApplyResources(this.txtTotalCapital, "txtTotalCapital");
            this.txtTotalCapital.Name = "txtTotalCapital";
            this.txtTotalCapital.ReadOnly = true;
            // 
            // txtTotalInterest
            // 
            resources.ApplyResources(this.txtTotalInterest, "txtTotalInterest");
            this.txtTotalInterest.Name = "txtTotalInterest";
            this.txtTotalInterest.ReadOnly = true;
            // 
            // txtPayment
            // 
            resources.ApplyResources(this.txtPayment, "txtPayment");
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.ReadOnly = true;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PAYMENT_ID,
            this.PAY_DATE,
            this.EXPENSE_ACCOUNT,
            this.PAYMENT_ACCOUNT,
            this.CAPITAL_AMOUNT,
            this.INTEREST_AMOUNT,
            this.PAY_AMOUNT,
            this.CURRENCY_SING_});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            // 
            // PAYMENT_ID
            // 
            this.PAYMENT_ID.DataPropertyName = "PAYMENT_ID";
            resources.ApplyResources(this.PAYMENT_ID, "PAYMENT_ID");
            this.PAYMENT_ID.Name = "PAYMENT_ID";
            this.PAYMENT_ID.ReadOnly = true;
            // 
            // PAY_DATE
            // 
            this.PAY_DATE.DataPropertyName = "PAY_DATE";
            dataGridViewCellStyle2.Format = "dd-MM-yyyy";
            this.PAY_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.PAY_DATE, "PAY_DATE");
            this.PAY_DATE.Name = "PAY_DATE";
            this.PAY_DATE.ReadOnly = true;
            // 
            // EXPENSE_ACCOUNT
            // 
            this.EXPENSE_ACCOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EXPENSE_ACCOUNT.DataPropertyName = "ACCOUNT_NAME";
            resources.ApplyResources(this.EXPENSE_ACCOUNT, "EXPENSE_ACCOUNT");
            this.EXPENSE_ACCOUNT.Name = "EXPENSE_ACCOUNT";
            this.EXPENSE_ACCOUNT.ReadOnly = true;
            // 
            // PAYMENT_ACCOUNT
            // 
            this.PAYMENT_ACCOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.PAYMENT_ACCOUNT.DataPropertyName = "PAYMENT_ACCOUNT";
            resources.ApplyResources(this.PAYMENT_ACCOUNT, "PAYMENT_ACCOUNT");
            this.PAYMENT_ACCOUNT.Name = "PAYMENT_ACCOUNT";
            this.PAYMENT_ACCOUNT.ReadOnly = true;
            // 
            // CAPITAL_AMOUNT
            // 
            this.CAPITAL_AMOUNT.DataPropertyName = "CAPITAL_AMOUNT";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,##0.####";
            dataGridViewCellStyle3.NullValue = null;
            this.CAPITAL_AMOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CAPITAL_AMOUNT, "CAPITAL_AMOUNT");
            this.CAPITAL_AMOUNT.Name = "CAPITAL_AMOUNT";
            this.CAPITAL_AMOUNT.ReadOnly = true;
            // 
            // INTEREST_AMOUNT
            // 
            this.INTEREST_AMOUNT.DataPropertyName = "INTEREST_AMOUNT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,##0.####";
            dataGridViewCellStyle4.NullValue = null;
            this.INTEREST_AMOUNT.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.INTEREST_AMOUNT, "INTEREST_AMOUNT");
            this.INTEREST_AMOUNT.Name = "INTEREST_AMOUNT";
            this.INTEREST_AMOUNT.ReadOnly = true;
            // 
            // PAY_AMOUNT
            // 
            this.PAY_AMOUNT.DataPropertyName = "PAY_AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.####";
            dataGridViewCellStyle5.NullValue = null;
            this.PAY_AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.PAY_AMOUNT, "PAY_AMOUNT");
            this.PAY_AMOUNT.Name = "PAY_AMOUNT";
            this.PAY_AMOUNT.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // DialogLoan
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogLoan";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabLOAD_INFORMATION.ResumeLayout(false);
            this.tabLOAD_INFORMATION.PerformLayout();
            this.tabPAYMENT.ResumeLayout(false);
            this.tabPAYMENT.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnClose;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private Label lblCLOSE_DATE_I;
        private TextBox txtLoanAmount;
        private DateTimePicker dtpLoanDate;
        private Label lblLOAN_DATE;
        private Label lblBANK;
        private Label label14;
        private Label label13;
        private Label lblLOAN_ACCOUNT;
        private TreeComboBox cboTransAccount;
        private TextBox txtBankNo;
        private Label lblPAYMENT_ACCOUNT;
        private TreeComboBox cboPaymentAccount;
        private ComboBox cboCurrency;
        private Label lblLOAN_AMOUNT;
        private Label label1;
        private DateTimePicker dtpCloseDate;
        private TextBox txtLoanNo;
        private Label label10;
        private Label lblLOAN_NO;
        private ComboBox cboBusinessDivension;
        private Label lblBUSINESS_DIVISION;
        private TextBox txtInterestRate;
        private Label label16;
        private Label label19;
        private Label lblANNUAL_INTEREST_RATE;
        private TabControl tabControl1;
        private TabPage tabLOAD_INFORMATION;
        private TabPage tabPAYMENT;
        private DataGridView dgv;
        private Label lblNOTE;
        private Label lblPAID_AMOUNT;
        private TextBox txtLoanPayment;
        private TextBox txtNote;
        private Label label21;
        private TextBox txtPayment;
        private ExLinkLabel btnADD;
        private ExLinkLabel btnEDIT;
        private ExLinkLabel btnREMOVE;
        private TextBox txtTotalCapital;
        private TextBox txtTotalInterest;
        private Panel panel1;
        private DataGridViewTextBoxColumn PAYMENT_ID;
        private DataGridViewTextBoxColumn PAY_DATE;
        private DataGridViewTextBoxColumn EXPENSE_ACCOUNT;
        private DataGridViewTextBoxColumn PAYMENT_ACCOUNT;
        private DataGridViewTextBoxColumn CAPITAL_AMOUNT;
        private DataGridViewTextBoxColumn INTEREST_AMOUNT;
        private DataGridViewTextBoxColumn PAY_AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
    }
}