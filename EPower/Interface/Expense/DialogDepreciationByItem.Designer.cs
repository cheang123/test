﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDepreciationByItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDepreciationByItem));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.txtFixAssetName = new System.Windows.Forms.TextBox();
            this.lblFIX_ASSET = new System.Windows.Forms.Label();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.txtAccDepre = new System.Windows.Forms.TextBox();
            this.lblACCUMULATE_DEPRECIATION_1 = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.lblACCUMULATE_DEPRECIATION = new System.Windows.Forms.Label();
            this.lblUSE_MONTH = new System.Windows.Forms.Label();
            this.dtpSuggestionDate = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkCheckAll_ = new System.Windows.Forms.CheckBox();
            this.DEPRECIATION_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_DEPRECIATION_ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DEPRECIATION_MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACCUMULATE_DEPRECIATION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.chkCheckAll_);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.lblACCUMULATE_DEPRECIATION);
            this.content.Controls.Add(this.lblUSE_MONTH);
            this.content.Controls.Add(this.lblACCUMULATE_DEPRECIATION_1);
            this.content.Controls.Add(this.txtTotalAmount);
            this.content.Controls.Add(this.txtAccDepre);
            this.content.Controls.Add(this.lblAMOUNT);
            this.content.Controls.Add(this.txtAmount);
            this.content.Controls.Add(this.dtpSuggestionDate);
            this.content.Controls.Add(this.txtFixAssetName);
            this.content.Controls.Add(this.lblFIX_ASSET);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.lblFIX_ASSET, 0);
            this.content.Controls.SetChildIndex(this.txtFixAssetName, 0);
            this.content.Controls.SetChildIndex(this.dtpSuggestionDate, 0);
            this.content.Controls.SetChildIndex(this.txtAmount, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtAccDepre, 0);
            this.content.Controls.SetChildIndex(this.txtTotalAmount, 0);
            this.content.Controls.SetChildIndex(this.lblACCUMULATE_DEPRECIATION_1, 0);
            this.content.Controls.SetChildIndex(this.lblUSE_MONTH, 0);
            this.content.Controls.SetChildIndex(this.lblACCUMULATE_DEPRECIATION, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.chkCheckAll_, 0);
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtFixAssetName
            // 
            resources.ApplyResources(this.txtFixAssetName, "txtFixAssetName");
            this.txtFixAssetName.Name = "txtFixAssetName";
            this.txtFixAssetName.ReadOnly = true;
            // 
            // lblFIX_ASSET
            // 
            resources.ApplyResources(this.lblFIX_ASSET, "lblFIX_ASSET");
            this.lblFIX_ASSET.Name = "lblFIX_ASSET";
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // txtAmount
            // 
            resources.ApplyResources(this.txtAmount, "txtAmount");
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.ReadOnly = true;
            // 
            // txtAccDepre
            // 
            resources.ApplyResources(this.txtAccDepre, "txtAccDepre");
            this.txtAccDepre.Name = "txtAccDepre";
            this.txtAccDepre.ReadOnly = true;
            // 
            // lblACCUMULATE_DEPRECIATION_1
            // 
            resources.ApplyResources(this.lblACCUMULATE_DEPRECIATION_1, "lblACCUMULATE_DEPRECIATION_1");
            this.lblACCUMULATE_DEPRECIATION_1.Name = "lblACCUMULATE_DEPRECIATION_1";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DEPRECIATION_ID,
            this.IS_DEPRECIATION_,
            this.DEPRECIATION_MONTH,
            this.CREATE_BY,
            this.ACCUMULATE_DEPRECIATION,
            this.STATUS_ID,
            this.CURRENCY_SING_});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(205)))), ((int)(((byte)(239)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            this.dgv.Sorted += new System.EventHandler(this.dgv_Sorted);
            // 
            // txtTotalAmount
            // 
            resources.ApplyResources(this.txtTotalAmount, "txtTotalAmount");
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            // 
            // lblACCUMULATE_DEPRECIATION
            // 
            resources.ApplyResources(this.lblACCUMULATE_DEPRECIATION, "lblACCUMULATE_DEPRECIATION");
            this.lblACCUMULATE_DEPRECIATION.Name = "lblACCUMULATE_DEPRECIATION";
            // 
            // lblUSE_MONTH
            // 
            resources.ApplyResources(this.lblUSE_MONTH, "lblUSE_MONTH");
            this.lblUSE_MONTH.Name = "lblUSE_MONTH";
            // 
            // dtpSuggestionDate
            // 
            resources.ApplyResources(this.dtpSuggestionDate, "dtpSuggestionDate");
            this.dtpSuggestionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSuggestionDate.Name = "dtpSuggestionDate";
            this.dtpSuggestionDate.ValueChanged += new System.EventHandler(this.cboStatus_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // chkCheckAll_
            // 
            resources.ApplyResources(this.chkCheckAll_, "chkCheckAll_");
            this.chkCheckAll_.Name = "chkCheckAll_";
            this.chkCheckAll_.UseVisualStyleBackColor = true;
            this.chkCheckAll_.CheckedChanged += new System.EventHandler(this.chkCheckAll_CheckedChanged);
            // 
            // DEPRECIATION_ID
            // 
            this.DEPRECIATION_ID.DataPropertyName = "DEPRECIATION_ID";
            resources.ApplyResources(this.DEPRECIATION_ID, "DEPRECIATION_ID");
            this.DEPRECIATION_ID.Name = "DEPRECIATION_ID";
            this.DEPRECIATION_ID.ReadOnly = true;
            // 
            // IS_DEPRECIATION_
            // 
            this.IS_DEPRECIATION_.DataPropertyName = "IS_DEPRECIATION";
            resources.ApplyResources(this.IS_DEPRECIATION_, "IS_DEPRECIATION_");
            this.IS_DEPRECIATION_.Name = "IS_DEPRECIATION_";
            this.IS_DEPRECIATION_.ReadOnly = true;
            // 
            // DEPRECIATION_MONTH
            // 
            this.DEPRECIATION_MONTH.DataPropertyName = "DEPRECIATION_MONTH";
            dataGridViewCellStyle2.Format = "MM-yyyy";
            dataGridViewCellStyle2.NullValue = null;
            this.DEPRECIATION_MONTH.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.DEPRECIATION_MONTH, "DEPRECIATION_MONTH");
            this.DEPRECIATION_MONTH.Name = "DEPRECIATION_MONTH";
            this.DEPRECIATION_MONTH.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "DEPRECIATION_BY";
            dataGridViewCellStyle3.Format = "yyyy-MM-dd";
            this.CREATE_BY.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // ACCUMULATE_DEPRECIATION
            // 
            this.ACCUMULATE_DEPRECIATION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ACCUMULATE_DEPRECIATION.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,##0.####";
            dataGridViewCellStyle4.NullValue = null;
            this.ACCUMULATE_DEPRECIATION.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.ACCUMULATE_DEPRECIATION, "ACCUMULATE_DEPRECIATION");
            this.ACCUMULATE_DEPRECIATION.Name = "ACCUMULATE_DEPRECIATION";
            this.ACCUMULATE_DEPRECIATION.ReadOnly = true;
            // 
            // STATUS_ID
            // 
            this.STATUS_ID.DataPropertyName = "STATUS_ID";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.STATUS_ID.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.STATUS_ID, "STATUS_ID");
            this.STATUS_ID.Name = "STATUS_ID";
            this.STATUS_ID.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CURRENCY_SING_.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // DialogDepreciationByItem
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogDepreciationByItem";
            this.Load += new System.EventHandler(this.DialogDepreciationByItem_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnClose;
        private ExButton btnOK;
        private TextBox txtFixAssetName;
        private Label lblFIX_ASSET;
        private Label lblACCUMULATE_DEPRECIATION_1;
        private TextBox txtAccDepre;
        private Label lblAMOUNT;
        private TextBox txtAmount;
        private DataGridView dgv;
        private Label lblACCUMULATE_DEPRECIATION;
        private TextBox txtTotalAmount;
        private Label lblUSE_MONTH;
        private DateTimePicker dtpSuggestionDate;
        private Panel panel1;
        private CheckBox chkCheckAll_;
        private DataGridViewTextBoxColumn DEPRECIATION_ID;
        private DataGridViewCheckBoxColumn IS_DEPRECIATION_;
        private DataGridViewTextBoxColumn DEPRECIATION_MONTH;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn ACCUMULATE_DEPRECIATION;
        private DataGridViewTextBoxColumn STATUS_ID;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
    }
}