﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDisposeFixAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDisposeFixAsset));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.cboPaymentAccount = new SoftTech.Component.TreeComboBox();
            this.txtTranBy = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.lblREF_NO = new System.Windows.Forms.Label();
            this.txtSoldAmount = new System.Windows.Forms.TextBox();
            this.lblDISPOSE_SOLD_VALUE = new System.Windows.Forms.Label();
            this.dtpTranDate = new System.Windows.Forms.DateTimePicker();
            this.lblDATE = new System.Windows.Forms.Label();
            this.lblCREATE_BY = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.lblFIX_ASSET = new System.Windows.Forms.Label();
            this.txtFixAssetName = new System.Windows.Forms.TextBox();
            this.lblBOOK_VALUE = new System.Windows.Forms.Label();
            this.txtBookValue = new System.Windows.Forms.TextBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.lblGAIN_LOSS = new System.Windows.Forms.Label();
            this.txtGainLoss = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cboAccountIncome = new SoftTech.Component.TreeComboBox();
            this.lblINCOME_ACCOUNT = new System.Windows.Forms.Label();
            this.lblQUANTITY = new System.Windows.Forms.Label();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.lblUNIT_PRICE = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.lblQTY_ASSET_IN_USE = new System.Windows.Forms.Label();
            this.txtQtyBalance = new System.Windows.Forms.TextBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblINCOME_ACCOUNT);
            this.content.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.content.Controls.Add(this.cboAccountIncome);
            this.content.Controls.Add(this.cboPaymentAccount);
            this.content.Controls.Add(this.txtFixAssetName);
            this.content.Controls.Add(this.txtBookValue);
            this.content.Controls.Add(this.txtTranBy);
            this.content.Controls.Add(this.label15);
            this.content.Controls.Add(this.label19);
            this.content.Controls.Add(this.label26);
            this.content.Controls.Add(this.label23);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.txtRefNo);
            this.content.Controls.Add(this.lblREF_NO);
            this.content.Controls.Add(this.txtQtyBalance);
            this.content.Controls.Add(this.txtGainLoss);
            this.content.Controls.Add(this.txtPrice);
            this.content.Controls.Add(this.lblUNIT_PRICE);
            this.content.Controls.Add(this.txtQty);
            this.content.Controls.Add(this.lblQUANTITY);
            this.content.Controls.Add(this.txtSoldAmount);
            this.content.Controls.Add(this.lblDISPOSE_SOLD_VALUE);
            this.content.Controls.Add(this.dtpTranDate);
            this.content.Controls.Add(this.lblQTY_ASSET_IN_USE);
            this.content.Controls.Add(this.lblGAIN_LOSS);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.lblDATE);
            this.content.Controls.Add(this.lblFIX_ASSET);
            this.content.Controls.Add(this.lblBOOK_VALUE);
            this.content.Controls.Add(this.lblCREATE_BY);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.lblCREATE_BY, 0);
            this.content.Controls.SetChildIndex(this.lblBOOK_VALUE, 0);
            this.content.Controls.SetChildIndex(this.lblFIX_ASSET, 0);
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.lblGAIN_LOSS, 0);
            this.content.Controls.SetChildIndex(this.lblQTY_ASSET_IN_USE, 0);
            this.content.Controls.SetChildIndex(this.dtpTranDate, 0);
            this.content.Controls.SetChildIndex(this.lblDISPOSE_SOLD_VALUE, 0);
            this.content.Controls.SetChildIndex(this.txtSoldAmount, 0);
            this.content.Controls.SetChildIndex(this.lblQUANTITY, 0);
            this.content.Controls.SetChildIndex(this.txtQty, 0);
            this.content.Controls.SetChildIndex(this.lblUNIT_PRICE, 0);
            this.content.Controls.SetChildIndex(this.txtPrice, 0);
            this.content.Controls.SetChildIndex(this.txtGainLoss, 0);
            this.content.Controls.SetChildIndex(this.txtQtyBalance, 0);
            this.content.Controls.SetChildIndex(this.lblREF_NO, 0);
            this.content.Controls.SetChildIndex(this.txtRefNo, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label23, 0);
            this.content.Controls.SetChildIndex(this.label26, 0);
            this.content.Controls.SetChildIndex(this.label19, 0);
            this.content.Controls.SetChildIndex(this.label15, 0);
            this.content.Controls.SetChildIndex(this.txtTranBy, 0);
            this.content.Controls.SetChildIndex(this.txtBookValue, 0);
            this.content.Controls.SetChildIndex(this.txtFixAssetName, 0);
            this.content.Controls.SetChildIndex(this.cboPaymentAccount, 0);
            this.content.Controls.SetChildIndex(this.cboAccountIncome, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblINCOME_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.Items.AddRange(new object[] {
            resources.GetString("cboCurrency.Items"),
            resources.GetString("cboCurrency.Items1"),
            resources.GetString("cboCurrency.Items2")});
            this.cboCurrency.Name = "cboCurrency";
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // cboPaymentAccount
            // 
            this.cboPaymentAccount.AbsoluteChildrenSelectableOnly = true;
            this.cboPaymentAccount.BranchSeparator = "/";
            this.cboPaymentAccount.Imagelist = null;
            resources.ApplyResources(this.cboPaymentAccount, "cboPaymentAccount");
            this.cboPaymentAccount.Name = "cboPaymentAccount";
            this.cboPaymentAccount.PopupHeight = 250;
            this.cboPaymentAccount.PopupWidth = 350;
            this.cboPaymentAccount.SelectedNode = null;
            // 
            // txtTranBy
            // 
            this.txtTranBy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTranBy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtTranBy, "txtTranBy");
            this.txtTranBy.Name = "txtTranBy";
            this.txtTranBy.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Name = "label15";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // txtRefNo
            // 
            resources.ApplyResources(this.txtRefNo, "txtRefNo");
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblREF_NO
            // 
            resources.ApplyResources(this.lblREF_NO, "lblREF_NO");
            this.lblREF_NO.Name = "lblREF_NO";
            // 
            // txtSoldAmount
            // 
            resources.ApplyResources(this.txtSoldAmount, "txtSoldAmount");
            this.txtSoldAmount.Name = "txtSoldAmount";
            this.txtSoldAmount.ReadOnly = true;
            this.txtSoldAmount.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtSoldAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // lblDISPOSE_SOLD_VALUE
            // 
            resources.ApplyResources(this.lblDISPOSE_SOLD_VALUE, "lblDISPOSE_SOLD_VALUE");
            this.lblDISPOSE_SOLD_VALUE.Name = "lblDISPOSE_SOLD_VALUE";
            // 
            // dtpTranDate
            // 
            resources.ApplyResources(this.dtpTranDate, "dtpTranDate");
            this.dtpTranDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTranDate.Name = "dtpTranDate";
            this.dtpTranDate.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // lblCREATE_BY
            // 
            resources.ApplyResources(this.lblCREATE_BY, "lblCREATE_BY");
            this.lblCREATE_BY.Name = "lblCREATE_BY";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // lblFIX_ASSET
            // 
            resources.ApplyResources(this.lblFIX_ASSET, "lblFIX_ASSET");
            this.lblFIX_ASSET.Name = "lblFIX_ASSET";
            // 
            // txtFixAssetName
            // 
            this.txtFixAssetName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtFixAssetName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtFixAssetName, "txtFixAssetName");
            this.txtFixAssetName.Name = "txtFixAssetName";
            this.txtFixAssetName.ReadOnly = true;
            // 
            // lblBOOK_VALUE
            // 
            resources.ApplyResources(this.lblBOOK_VALUE, "lblBOOK_VALUE");
            this.lblBOOK_VALUE.Name = "lblBOOK_VALUE";
            // 
            // txtBookValue
            // 
            this.txtBookValue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtBookValue.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtBookValue, "txtBookValue");
            this.txtBookValue.Name = "txtBookValue";
            this.txtBookValue.ReadOnly = true;
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // lblGAIN_LOSS
            // 
            resources.ApplyResources(this.lblGAIN_LOSS, "lblGAIN_LOSS");
            this.lblGAIN_LOSS.Name = "lblGAIN_LOSS";
            // 
            // txtGainLoss
            // 
            resources.ApplyResources(this.txtGainLoss, "txtGainLoss");
            this.txtGainLoss.Name = "txtGainLoss";
            this.txtGainLoss.ReadOnly = true;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Name = "label19";
            // 
            // cboAccountIncome
            // 
            this.cboAccountIncome.AbsoluteChildrenSelectableOnly = true;
            this.cboAccountIncome.BranchSeparator = "/";
            this.cboAccountIncome.Imagelist = null;
            resources.ApplyResources(this.cboAccountIncome, "cboAccountIncome");
            this.cboAccountIncome.Name = "cboAccountIncome";
            this.cboAccountIncome.PopupHeight = 250;
            this.cboAccountIncome.PopupWidth = 350;
            this.cboAccountIncome.SelectedNode = null;
            // 
            // lblINCOME_ACCOUNT
            // 
            resources.ApplyResources(this.lblINCOME_ACCOUNT, "lblINCOME_ACCOUNT");
            this.lblINCOME_ACCOUNT.Name = "lblINCOME_ACCOUNT";
            // 
            // lblQUANTITY
            // 
            resources.ApplyResources(this.lblQUANTITY, "lblQUANTITY");
            this.lblQUANTITY.Name = "lblQUANTITY";
            // 
            // txtQty
            // 
            resources.ApplyResources(this.txtQty, "txtQty");
            this.txtQty.Name = "txtQty";
            this.txtQty.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            this.txtQty.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtQty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Name = "label23";
            // 
            // lblUNIT_PRICE
            // 
            resources.ApplyResources(this.lblUNIT_PRICE, "lblUNIT_PRICE");
            this.lblUNIT_PRICE.Name = "lblUNIT_PRICE";
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            this.txtPrice.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // label26
            // 
            resources.ApplyResources(this.label26, "label26");
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Name = "label26";
            // 
            // lblQTY_ASSET_IN_USE
            // 
            resources.ApplyResources(this.lblQTY_ASSET_IN_USE, "lblQTY_ASSET_IN_USE");
            this.lblQTY_ASSET_IN_USE.Name = "lblQTY_ASSET_IN_USE";
            // 
            // txtQtyBalance
            // 
            resources.ApplyResources(this.txtQtyBalance, "txtQtyBalance");
            this.txtQtyBalance.Name = "txtQtyBalance";
            this.txtQtyBalance.ReadOnly = true;
            // 
            // DialogDisposeFixAsset
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogDisposeFixAsset";
            this.Load += new System.EventHandler(this.DialogAccountCategory_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnClose;
        private ExButton btnOK;
        private ComboBox cboCurrency;
        private Label lblPAYMENT_ACCOUNT;
        private TreeComboBox cboPaymentAccount;
        private TextBox txtTranBy;
        private Label label15;
        private Label label14;
        private Label label13;
        private TextBox txtRefNo;
        private Label lblREF_NO;
        private TextBox txtSoldAmount;
        private Label lblDISPOSE_SOLD_VALUE;
        private DateTimePicker dtpTranDate;
        private Label lblDATE;
        private Label lblCREATE_BY;
        private TextBox txtNote;
        private Label lblNOTE;
        private TextBox txtFixAssetName;
        private Label lblFIX_ASSET;
        private TextBox txtBookValue;
        private Label lblBOOK_VALUE;
        private Label lblCURRENCY;
        private TextBox txtGainLoss;
        private Label lblGAIN_LOSS;
        private Label label1;
        private Label lblINCOME_ACCOUNT;
        private TreeComboBox cboAccountIncome;
        private Label label19;
        private Label label26;
        private Label label23;
        private TextBox txtQtyBalance;
        private TextBox txtPrice;
        private Label lblUNIT_PRICE;
        private TextBox txtQty;
        private Label lblQUANTITY;
        private Label lblQTY_ASSET_IN_USE;
    }
}