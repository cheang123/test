﻿using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Windows.Forms;


namespace EPower.Interface
{
    public partial class PageAccountSetting : Form
    {
        #region Contructor
        public PageAccountSetting()
        {
            InitializeComponent();
            txt_QuickSearch(null, null);

            //Set utility from b Smey
            this.btnEDIT.Enabled = true; //DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_EDIT_ACCOUNT_CHART]);
        }
        #endregion Contructor

        /// <summary>
        /// Load data from database
        /// </summary>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = from cf in DBDataContext.Db.TBL_ACCOUNT_CONFIGs
                                 where cf.CONFIG_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower())
                                 orderby cf.CONFIG_NAME
                                 select cf;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnEDIT_Click(object sender, EventArgs e)
        {
            int id = (int)this.dgv.SelectedRows[0].Cells[CONFIG_ID.Name].Value;
            TBL_ACCOUNT_CONFIG obj = DBDataContext.Db.TBL_ACCOUNT_CONFIGs.FirstOrDefault(x => x.CONFIG_ID == id);
            if (obj == null)
            {
                return;
            }
            DialogAccountConfig dig = new DialogAccountConfig(GeneralProcess.Update, obj);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, id);
            }
        }
    }
}
