﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogLoanPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogLoanPayment));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnAdd = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblLOAN_NO = new System.Windows.Forms.Label();
            this.lblDATE = new System.Windows.Forms.Label();
            this.dtpTranDate = new System.Windows.Forms.DateTimePicker();
            this.txtInterestAmount = new System.Windows.Forms.TextBox();
            this.txtCapitalAmount = new System.Windows.Forms.TextBox();
            this.lblCAPITAL_AMOUNT = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtLoanNo = new System.Windows.Forms.TextBox();
            this.cboTransAccount = new SoftTech.Component.TreeComboBox();
            this.lblEXPENSE_ACCOUNT = new System.Windows.Forms.Label();
            this.cboPaymentAccount = new SoftTech.Component.TreeComboBox();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblINTEREST_AMOUNT = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPAID_AMOUNT = new System.Windows.Forms.Label();
            this.txtPaidAmount = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.content.Controls.Add(this.cboPaymentAccount);
            this.content.Controls.Add(this.lblEXPENSE_ACCOUNT);
            this.content.Controls.Add(this.cboTransAccount);
            this.content.Controls.Add(this.txtLoanNo);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.label10);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.txtPaidAmount);
            this.content.Controls.Add(this.lblPAID_AMOUNT);
            this.content.Controls.Add(this.txtCapitalAmount);
            this.content.Controls.Add(this.lblCAPITAL_AMOUNT);
            this.content.Controls.Add(this.txtInterestAmount);
            this.content.Controls.Add(this.lblINTEREST_AMOUNT);
            this.content.Controls.Add(this.dtpTranDate);
            this.content.Controls.Add(this.lblDATE);
            this.content.Controls.Add(this.lblLOAN_NO);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnAdd);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnAdd, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.lblLOAN_NO, 0);
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.dtpTranDate, 0);
            this.content.Controls.SetChildIndex(this.lblINTEREST_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtInterestAmount, 0);
            this.content.Controls.SetChildIndex(this.lblCAPITAL_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtCapitalAmount, 0);
            this.content.Controls.SetChildIndex(this.lblPAID_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtPaidAmount, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.label10, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.txtLoanNo, 0);
            this.content.Controls.SetChildIndex(this.cboTransAccount, 0);
            this.content.Controls.SetChildIndex(this.lblEXPENSE_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.cboPaymentAccount, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.Name = "btnClose";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAdd
            // 
            resources.ApplyResources(this.btnAdd, "btnAdd");
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // lblLOAN_NO
            // 
            resources.ApplyResources(this.lblLOAN_NO, "lblLOAN_NO");
            this.lblLOAN_NO.Name = "lblLOAN_NO";
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // dtpTranDate
            // 
            resources.ApplyResources(this.dtpTranDate, "dtpTranDate");
            this.dtpTranDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTranDate.Name = "dtpTranDate";
            this.dtpTranDate.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // txtInterestAmount
            // 
            resources.ApplyResources(this.txtInterestAmount, "txtInterestAmount");
            this.txtInterestAmount.Name = "txtInterestAmount";
            this.txtInterestAmount.TextChanged += new System.EventHandler(this.txtInterestAmount_TextChanged);
            this.txtInterestAmount.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtInterestAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterestAmount_KeyPress);
            // 
            // txtCapitalAmount
            // 
            resources.ApplyResources(this.txtCapitalAmount, "txtCapitalAmount");
            this.txtCapitalAmount.Name = "txtCapitalAmount";
            this.txtCapitalAmount.TextChanged += new System.EventHandler(this.txtInterestAmount_TextChanged);
            this.txtCapitalAmount.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            this.txtCapitalAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInterestAmount_KeyPress);
            // 
            // lblCAPITAL_AMOUNT
            // 
            resources.ApplyResources(this.lblCAPITAL_AMOUNT, "lblCAPITAL_AMOUNT");
            this.lblCAPITAL_AMOUNT.Name = "lblCAPITAL_AMOUNT";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // txtLoanNo
            // 
            this.txtLoanNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtLoanNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtLoanNo, "txtLoanNo");
            this.txtLoanNo.Name = "txtLoanNo";
            this.txtLoanNo.ReadOnly = true;
            this.txtLoanNo.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // cboTransAccount
            // 
            this.cboTransAccount.AbsoluteChildrenSelectableOnly = true;
            this.cboTransAccount.BranchSeparator = "/";
            this.cboTransAccount.Imagelist = null;
            resources.ApplyResources(this.cboTransAccount, "cboTransAccount");
            this.cboTransAccount.Name = "cboTransAccount";
            this.cboTransAccount.PopupHeight = 250;
            this.cboTransAccount.PopupWidth = 300;
            this.cboTransAccount.SelectedNode = null;
            // 
            // lblEXPENSE_ACCOUNT
            // 
            resources.ApplyResources(this.lblEXPENSE_ACCOUNT, "lblEXPENSE_ACCOUNT");
            this.lblEXPENSE_ACCOUNT.Name = "lblEXPENSE_ACCOUNT";
            // 
            // cboPaymentAccount
            // 
            this.cboPaymentAccount.AbsoluteChildrenSelectableOnly = true;
            this.cboPaymentAccount.BranchSeparator = "/";
            this.cboPaymentAccount.Imagelist = null;
            resources.ApplyResources(this.cboPaymentAccount, "cboPaymentAccount");
            this.cboPaymentAccount.Name = "cboPaymentAccount";
            this.cboPaymentAccount.PopupHeight = 250;
            this.cboPaymentAccount.PopupWidth = 300;
            this.cboPaymentAccount.SelectedNode = null;
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.Items.AddRange(new object[] {
            resources.GetString("cboCurrency.Items"),
            resources.GetString("cboCurrency.Items1"),
            resources.GetString("cboCurrency.Items2")});
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // lblINTEREST_AMOUNT
            // 
            resources.ApplyResources(this.lblINTEREST_AMOUNT, "lblINTEREST_AMOUNT");
            this.lblINTEREST_AMOUNT.Name = "lblINTEREST_AMOUNT";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // lblPAID_AMOUNT
            // 
            resources.ApplyResources(this.lblPAID_AMOUNT, "lblPAID_AMOUNT");
            this.lblPAID_AMOUNT.Name = "lblPAID_AMOUNT";
            // 
            // txtPaidAmount
            // 
            resources.ApplyResources(this.txtPaidAmount, "txtPaidAmount");
            this.txtPaidAmount.Name = "txtPaidAmount";
            this.txtPaidAmount.ReadOnly = true;
            this.txtPaidAmount.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Name = "label10";
            // 
            // DialogLoanPayment
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogLoanPayment";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnClose;
        private ExButton btnAdd;
        private ExButton btnCHANGE_LOG;
        private TextBox txtNote;
        private Label lblNOTE;
        private TextBox txtCapitalAmount;
        private Label lblCAPITAL_AMOUNT;
        private TextBox txtInterestAmount;
        private DateTimePicker dtpTranDate;
        private Label lblDATE;
        private Label lblLOAN_NO;
        private Label label14;
        private Label label13;
        private Label lblEXPENSE_ACCOUNT;
        private TreeComboBox cboTransAccount;
        private TextBox txtLoanNo;
        private Label lblPAYMENT_ACCOUNT;
        private TreeComboBox cboPaymentAccount;
        private ComboBox cboCurrency;
        private Label lblINTEREST_AMOUNT;
        private Label label1;
        private TextBox txtPaidAmount;
        private Label lblPAID_AMOUNT;
        private Label label10;
    }
}