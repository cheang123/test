﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;

namespace EPower.Interface  
{
    public class FixAssetCategoryPopulator
    {
        List<TBL_FIX_ASSET_CATEGORY> categories = new List<TBL_FIX_ASSET_CATEGORY>();
        TreeView tvw = null;
        public void PopluateTree(TreeView tvw)
        {
            this.tvw = tvw;
            this.tvw.Nodes.Clear();
            this.categories = DBDataContext.Db.TBL_FIX_ASSET_CATEGORies.Where(x => x.IS_ACTIVE).OrderBy(x=>x.CATEGORY_CODE).ToList();
            var node = new TreeNode();
            node.Text = string.Format(Resources.ALL_FIX_ASSET_CATEGORY);
            node.Tag = 0; // parent categothisry
            this.tvw.Nodes.Add(node);
            addChildNode(node);
        }

        private void addChildNode(TreeNode node)
        {
            var id = (int)node.Tag;
            foreach (var category in categories.Where(x => x.PARENT_ID == id && x.IS_ACTIVE))
            {
                var childNode = new TreeNode();
                childNode.Text = category.CATEGORY_CODE + " " + category.CATEGORY_NAME;
                childNode.Tag = category.CATEGORY_ID;
                node.Nodes.Add(childNode);
                addChildNode(childNode);
            }
        }
    }
}
