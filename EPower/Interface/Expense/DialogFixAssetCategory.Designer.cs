﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogFixAssetCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogFixAssetCategory));
            this.lblFIX_ASSET_TYPE = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboParent = new SoftTech.Component.TreeComboBox();
            this.lblPARENT_FIX_ASSET = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.lblFIX_ASSET_TYPE_CODE = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cboAssetAccount = new System.Windows.Forms.ComboBox();
            this.lblDEFAULT_USEFUL_LIFE = new System.Windows.Forms.Label();
            this.txtUsefulLife = new System.Windows.Forms.TextBox();
            this.lblFIX_ASSET_ACCOUNT = new System.Windows.Forms.Label();
            this.lblACCUMULATED_DEPRECIATION_ACCOUNT = new System.Windows.Forms.Label();
            this.cboAccumulatedDepreciationAccount = new System.Windows.Forms.ComboBox();
            this.cboDepreciationExpenseAccount = new System.Windows.Forms.ComboBox();
            this.lblDEPRECIATION_EXPENSE_ACCOUNT = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblDEPRECIATION_EXPENSE_ACCOUNT);
            this.content.Controls.Add(this.lblACCUMULATED_DEPRECIATION_ACCOUNT);
            this.content.Controls.Add(this.cboDepreciationExpenseAccount);
            this.content.Controls.Add(this.lblFIX_ASSET_ACCOUNT);
            this.content.Controls.Add(this.cboAccumulatedDepreciationAccount);
            this.content.Controls.Add(this.txtUsefulLife);
            this.content.Controls.Add(this.lblDEFAULT_USEFUL_LIFE);
            this.content.Controls.Add(this.label8);
            this.content.Controls.Add(this.cboAssetAccount);
            this.content.Controls.Add(this.lblFIX_ASSET_TYPE_CODE);
            this.content.Controls.Add(this.txtCode);
            this.content.Controls.Add(this.lblPARENT_FIX_ASSET);
            this.content.Controls.Add(this.cboParent);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtName);
            this.content.Controls.Add(this.lblFIX_ASSET_TYPE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblFIX_ASSET_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtName, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.cboParent, 0);
            this.content.Controls.SetChildIndex(this.lblPARENT_FIX_ASSET, 0);
            this.content.Controls.SetChildIndex(this.txtCode, 0);
            this.content.Controls.SetChildIndex(this.lblFIX_ASSET_TYPE_CODE, 0);
            this.content.Controls.SetChildIndex(this.cboAssetAccount, 0);
            this.content.Controls.SetChildIndex(this.label8, 0);
            this.content.Controls.SetChildIndex(this.lblDEFAULT_USEFUL_LIFE, 0);
            this.content.Controls.SetChildIndex(this.txtUsefulLife, 0);
            this.content.Controls.SetChildIndex(this.cboAccumulatedDepreciationAccount, 0);
            this.content.Controls.SetChildIndex(this.lblFIX_ASSET_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.cboDepreciationExpenseAccount, 0);
            this.content.Controls.SetChildIndex(this.lblACCUMULATED_DEPRECIATION_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblDEPRECIATION_EXPENSE_ACCOUNT, 0);
            // 
            // lblFIX_ASSET_TYPE
            // 
            resources.ApplyResources(this.lblFIX_ASSET_TYPE, "lblFIX_ASSET_TYPE");
            this.lblFIX_ASSET_TYPE.Name = "lblFIX_ASSET_TYPE";
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            this.txtName.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Name = "label7";
            // 
            // cboParent
            // 
            this.cboParent.AbsoluteChildrenSelectableOnly = false;
            this.cboParent.BranchSeparator = "/";
            this.cboParent.Imagelist = null;
            resources.ApplyResources(this.cboParent, "cboParent");
            this.cboParent.Name = "cboParent";
            this.cboParent.PopupHeight = 250;
            this.cboParent.PopupWidth = 0;
            this.cboParent.SelectedNode = null;
            this.cboParent.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblPARENT_FIX_ASSET
            // 
            resources.ApplyResources(this.lblPARENT_FIX_ASSET, "lblPARENT_FIX_ASSET");
            this.lblPARENT_FIX_ASSET.Name = "lblPARENT_FIX_ASSET";
            // 
            // txtCode
            // 
            resources.ApplyResources(this.txtCode, "txtCode");
            this.txtCode.Name = "txtCode";
            this.txtCode.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblFIX_ASSET_TYPE_CODE
            // 
            resources.ApplyResources(this.lblFIX_ASSET_TYPE_CODE, "lblFIX_ASSET_TYPE_CODE");
            this.lblFIX_ASSET_TYPE_CODE.Name = "lblFIX_ASSET_TYPE_CODE";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Name = "label8";
            // 
            // cboAssetAccount
            // 
            this.cboAssetAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAssetAccount.FormattingEnabled = true;
            this.cboAssetAccount.Items.AddRange(new object[] {
            resources.GetString("cboAssetAccount.Items"),
            resources.GetString("cboAssetAccount.Items1"),
            resources.GetString("cboAssetAccount.Items2"),
            resources.GetString("cboAssetAccount.Items3"),
            resources.GetString("cboAssetAccount.Items4")});
            resources.ApplyResources(this.cboAssetAccount, "cboAssetAccount");
            this.cboAssetAccount.Name = "cboAssetAccount";
            this.cboAssetAccount.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblDEFAULT_USEFUL_LIFE
            // 
            resources.ApplyResources(this.lblDEFAULT_USEFUL_LIFE, "lblDEFAULT_USEFUL_LIFE");
            this.lblDEFAULT_USEFUL_LIFE.Name = "lblDEFAULT_USEFUL_LIFE";
            // 
            // txtUsefulLife
            // 
            resources.ApplyResources(this.txtUsefulLife, "txtUsefulLife");
            this.txtUsefulLife.Name = "txtUsefulLife";
            this.txtUsefulLife.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblFIX_ASSET_ACCOUNT
            // 
            resources.ApplyResources(this.lblFIX_ASSET_ACCOUNT, "lblFIX_ASSET_ACCOUNT");
            this.lblFIX_ASSET_ACCOUNT.Name = "lblFIX_ASSET_ACCOUNT";
            // 
            // lblACCUMULATED_DEPRECIATION_ACCOUNT
            // 
            resources.ApplyResources(this.lblACCUMULATED_DEPRECIATION_ACCOUNT, "lblACCUMULATED_DEPRECIATION_ACCOUNT");
            this.lblACCUMULATED_DEPRECIATION_ACCOUNT.Name = "lblACCUMULATED_DEPRECIATION_ACCOUNT";
            // 
            // cboAccumulatedDepreciationAccount
            // 
            this.cboAccumulatedDepreciationAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAccumulatedDepreciationAccount.FormattingEnabled = true;
            this.cboAccumulatedDepreciationAccount.Items.AddRange(new object[] {
            resources.GetString("cboAccumulatedDepreciationAccount.Items"),
            resources.GetString("cboAccumulatedDepreciationAccount.Items1"),
            resources.GetString("cboAccumulatedDepreciationAccount.Items2"),
            resources.GetString("cboAccumulatedDepreciationAccount.Items3"),
            resources.GetString("cboAccumulatedDepreciationAccount.Items4")});
            resources.ApplyResources(this.cboAccumulatedDepreciationAccount, "cboAccumulatedDepreciationAccount");
            this.cboAccumulatedDepreciationAccount.Name = "cboAccumulatedDepreciationAccount";
            // 
            // cboDepreciationExpenseAccount
            // 
            this.cboDepreciationExpenseAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDepreciationExpenseAccount.FormattingEnabled = true;
            this.cboDepreciationExpenseAccount.Items.AddRange(new object[] {
            resources.GetString("cboDepreciationExpenseAccount.Items"),
            resources.GetString("cboDepreciationExpenseAccount.Items1"),
            resources.GetString("cboDepreciationExpenseAccount.Items2"),
            resources.GetString("cboDepreciationExpenseAccount.Items3"),
            resources.GetString("cboDepreciationExpenseAccount.Items4")});
            resources.ApplyResources(this.cboDepreciationExpenseAccount, "cboDepreciationExpenseAccount");
            this.cboDepreciationExpenseAccount.Name = "cboDepreciationExpenseAccount";
            // 
            // lblDEPRECIATION_EXPENSE_ACCOUNT
            // 
            resources.ApplyResources(this.lblDEPRECIATION_EXPENSE_ACCOUNT, "lblDEPRECIATION_EXPENSE_ACCOUNT");
            this.lblDEPRECIATION_EXPENSE_ACCOUNT.Name = "lblDEPRECIATION_EXPENSE_ACCOUNT";
            // 
            // DialogFixAssetCategory
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogFixAssetCategory";
            this.Load += new System.EventHandler(this.DialogAccountCategory_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtName;
        private Label lblFIX_ASSET_TYPE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ExButton btnCHANGE_LOG;
        private TextBox txtNote;
        private Label lblNOTE;
        private Label label7;
        private Label lblPARENT_FIX_ASSET;
        private TreeComboBox cboParent;
        private Label lblFIX_ASSET_TYPE_CODE;
        private TextBox txtCode;
        private Label label8;
        private TextBox txtUsefulLife;
        private Label lblDEFAULT_USEFUL_LIFE;
        public ComboBox cboAssetAccount;
        private Label lblDEPRECIATION_EXPENSE_ACCOUNT;
        public ComboBox cboDepreciationExpenseAccount;
        public ComboBox cboAccumulatedDepreciationAccount;
        private Label lblACCUMULATED_DEPRECIATION_ACCOUNT;
        private Label lblFIX_ASSET_ACCOUNT;
    }
}