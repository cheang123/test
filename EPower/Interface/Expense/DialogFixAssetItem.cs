﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogFixAssetItem : ExDialog
    {
        GeneralProcess _flag;

        TBL_FIX_ASSET_ITEM _objNew = new TBL_FIX_ASSET_ITEM();
        TBL_FIX_ASSET_ITEM _objOld = new TBL_FIX_ASSET_ITEM();
        bool load = false;
        public TBL_FIX_ASSET_ITEM FixAssetItem
        {
            get { return _objNew; }
        }
        

        #region Constructor
        public DialogFixAssetItem(GeneralProcess flag, TBL_FIX_ASSET_ITEM objFixAssetItem,int selectedTabIndex)
        {
            InitializeComponent();

            //UIHelper.DataGridViewProperties(dgvMaintenance);
            //UIHelper.DataGridViewProperties(dgv);

            tabControl.SelectedIndex = selectedTabIndex;
            load = false;
            _flag = flag;
            objFixAssetItem._CopyTo(_objNew);
            objFixAssetItem._CopyTo(_objOld);
            this.Text = flag.GetText(this.Text);
            lookUp();
            read();
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));

            // Set Focus Textbox
            txtFixAssetName.Select();

            // remove tab.
            tabControl.TabPages.Remove(tabSERVICE_AND_MAINTENAINCE);

            // never update category
            if (flag == GeneralProcess.Insert)
            {
                tabControl.TabPages.Remove(tabDEPRECIATION);
                tabControl.TabPages.Remove(tabDISPOSE_DEPRECIATION);
               
                if (objFixAssetItem.CATEGORY_ID > 0)
                {
                   txtUseFulLife.Text = DBDataContext.Db.TBL_FIX_ASSET_CATEGORies.FirstOrDefault(x => x.CATEGORY_ID == objFixAssetItem.CATEGORY_ID).DEFAULT_USEFUL_LIFE.ToString(UIHelper._DefaultUsageFormat);
                }

                // Set Defual Currency
                cboCurrency.SelectedValue = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY).CURRENCY_ID;

            }
            else if (flag == GeneralProcess.Update)
            {
                if (objFixAssetItem.ACCUMULATED_DEPRECIATION > 0 || DBDataContext.Db.TBL_FIX_ASSET_DISPOSEs.Where(x=>x.FIX_ASSET_ITEM_ID==objFixAssetItem.FIX_ASSET_ITEM_ID && x.IS_ACTIVE).Count()>0)
                {
                    disabledAfterDepreciation();
                }
                
                readRepairMaintenance();
                readDepreciation();
                readDispose();

                // Disable Control
                lblDEPRECIATION.Enabled =
                btnADD_1.Enabled =
                btnEDIT_1.Enabled =
                btnDELETE_1.Enabled = _objNew.STATUS_ID == (int)FixAssetItemStatus.USING;

                bool xx = _objNew.STATUS_ID == (int)FixAssetItemStatus.USING;

                // Visible Control
                dtpSuggestionDate.Visible = false;
                 
            }
            else
            {
                tabControl.TabPages.Remove(tabSERVICE_AND_MAINTENAINCE); 
                lblDEPRECIATION.Enabled =
                lblREMOVE_DEPRECIATION.Enabled =
                btnREMOVE_2.Enabled =
                btnADD_2.Enabled =
                btnEDIT_2.Enabled = false;
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            load = true;
        }  
        

        #endregion

        #region Method
         
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (cboPaymentAccount.TreeView.SelectedNode == null)
            {
                cboPaymentAccount.SetValidation(string.Format(Resources.REQUIRED, lblPAYMENT_ACCOUNT.Text));
                val = true;
            }
            
            if (txtFixAssetName.Text.Trim() == string.Empty)
            {
                txtFixAssetName.SetValidation(string.Format(Resources.REQUIRED, lblFIX_ASSET.Text));
                val = true;
            }
            if (cboCategory.TreeView.SelectedNode==null || cboCategory.TreeView.SelectedNode.Nodes.Count > 0)
            {
                cboCategory.SetValidation(Resources.MS_PLEASE_SELECT_SMALL_TYPE);
                val = true;
            }
            if (txtUseFulLife.Text.Trim() == string.Empty)
            {
                txtUseFulLife.SetValidation(string.Format(Resources.REQUIRED, lblDEPRECIATION_PERIOD.Text));
                val = true;
            }
            if (txtDepreciationRate.Text.Trim() == string.Empty)
            {
                txtDepreciationRate.SetValidation(string.Format(Resources.REQUIRED, lblANNUAL_DEPRECIATION_RATE.Text));
                val = true;
            }
            if (txtUnit.Text.Trim() == string.Empty)
            {
                txtUnit.SetValidation(string.Format(Resources.REQUIRED, lblUNIT.Text));
                val = true;
            }
            if (txtQty.Text.Trim() == string.Empty)
            {
                txtQty.SetValidation(string.Format(Resources.REQUIRED, lblQUANTITY.Text));
                val = true;
            }
            if (txtPrice.Text.Trim() == string.Empty)
            {
                txtPrice.SetValidation(string.Format(Resources.REQUIRED, lblPRICE_PER_UNIT.Text));
                val = true;
            }
            if (cboCurrency.Text ==string.Empty)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtAmount.Text) <= 0)
            {
                txtAmount.SetValidation(string.Format(Resources.REQUIRED_POSITIVE_NUMBER, lblAMOUNT.Text));
                val = true;
            }
            return val;
        } 
        private void read()
        {
            cboPaymentAccount.TreeView.SelectedNode = this.cboPaymentAccount.TreeView.GetNodesIncludeAncestors()
                                                   .FirstOrDefault(x => (int)x.Tag == this._objNew.PAYMENT_ACCOUNT_ID);
            if (cboPaymentAccount.TreeView.SelectedNode != null)
            {
                this.cboPaymentAccount.Text = cboPaymentAccount.TreeView.SelectedNode.Text;
            }

            cboCategory.TreeView.SelectedNode = this.cboCategory.TreeView.GetNodesIncludeAncestors()
                                                   .FirstOrDefault(x => (int)x.Tag == this._objNew.CATEGORY_ID);
            if (cboCategory.TreeView.SelectedNode != null)
            {
                this.cboCategory.Text = cboCategory.TreeView.SelectedNode.Text;
            } 

            txtFixAssetName.Text = _objNew.FIX_ASSET_NAME;
            txtBrand.Text = _objNew.BRAND;
            txtSource.Text = _objNew.SOURCE;
            txtProductionYear.Text = _objNew.YEAR;
            dtpUseDate.Value = _objNew.USE_DATE;
            txtProductStatus.Text = _objNew.USE_STATUS;
            txtUseFulLife.Text = _objNew.USEFUL_LIFE.ToString() ;
            txtDepreciationRate.Text = _objNew.ANNUAL_DEPRECIATION_RATE.ToString();
            txtUnit.Text = _objNew.UNIT;
            cboCurrency.SelectedValue = _objNew.CURRENCY_ID;
            txtPrice.Text =UIHelper.FormatCurrency(_objNew.PRICE,_objNew.CURRENCY_ID);
            txtQty.Text = _objNew.QUANTITY.ToString("#.##");
            txtAmount.Text =UIHelper.FormatCurrency(_objNew.TOTAL_COST,_objNew.CURRENCY_ID);
            txtNote.Text = _objNew.NOTE;
            txtSalvageValue.Text = UIHelper.FormatCurrency(_objNew.SALVAGE_VALUE,_objNew.CURRENCY_ID);
            txtAccDepreciation.Text = UIHelper.FormatCurrency(_objNew.ACCUMULATED_DEPRECIATION, _objNew.CURRENCY_ID);
        } 
        
        private void write()
        {
            _objNew.CATEGORY_ID = (int)cboCategory.TreeView.SelectedNode.Tag;
            _objNew.FIX_ASSET_NAME = txtFixAssetName.Text;
            _objNew.BRAND=txtBrand.Text ;
            _objNew.SOURCE=txtSource.Text ;
            _objNew.YEAR = txtProductionYear.Text;
            _objNew.USE_DATE = dtpUseDate.Value;
            _objNew.USE_STATUS=txtProductStatus.Text ;
            _objNew.USEFUL_LIFE = DataHelper.ParseToDecimal(txtUseFulLife.Text);
            _objNew.ANNUAL_DEPRECIATION_RATE=DataHelper.ParseToDecimal(txtDepreciationRate.Text) ;
            _objNew.UNIT=txtUnit.Text  ;
            _objNew.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objNew.PRICE=DataHelper.ParseToDecimal(txtPrice.Text);
            _objNew.QUANTITY=DataHelper.ParseToDecimal(txtQty.Text) ;
            _objNew.TOTAL_COST=DataHelper.ParseToDecimal(txtAmount.Text);
            _objNew.NOTE=txtNote.Text;
            _objNew.SALVAGE_VALUE = DataHelper.ParseToDecimal(txtSalvageValue.Text);
            _objNew.STATUS_ID = (int)FixAssetItemStatus.USING;
            _objNew.ACCUMULATED_DEPRECIATION = DataHelper.ParseToDecimal(txtAccDepreciation.Text);
            _objNew.PAYMENT_ACCOUNT_ID = (int)cboPaymentAccount.TreeView.SelectedNode.Tag;
        }

        private void lookUp()
        {
            new FixAssetCategoryPopulator().PopluateTree(cboCategory.TreeView);
            new AccountChartPopulator().PopluateTree(cboPaymentAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS));
            
            UIHelper.SetDataSourceToComboBox(cboDepreciationStatus, Lookup.GetDepreciationStatus(), Resources.ALL_STATUS);
            cboDepreciationStatus.SelectedIndex = (int)DepreciationStatus.ReadyDepreciation; 

            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies(),"");
            UIHelper.SetDataSourceToComboBox(cboCurrencyMaintenance, Lookup.GetCurrencies(), Resources.ALL_CURRENCY);

            var brand = from a in DBDataContext.Db.TBL_FIX_ASSET_ITEMs
                        where a.IS_ACTIVE
                        select new { a.BRAND };
            foreach (var item in brand.Distinct())
            {
                txtBrand.AutoCompleteCustomSource.Add(item.BRAND);
            }

            var source = from a in DBDataContext.Db.TBL_FIX_ASSET_ITEMs
                        where a.IS_ACTIVE
                        select new { a.SOURCE };
            foreach (var item in source.Distinct())
            {
                txtSource.AutoCompleteCustomSource.Add(item.SOURCE);
            }

            var statusProduct = from a in DBDataContext.Db.TBL_FIX_ASSET_ITEMs
                         where a.IS_ACTIVE
                         select new { a.USE_STATUS };
            foreach (var item in statusProduct.Distinct())
            {
                txtProductStatus.AutoCompleteCustomSource.Add(item.USE_STATUS);
            }

            var unit = from a in DBDataContext.Db.TBL_FIX_ASSET_ITEMs
                                where a.IS_ACTIVE
                                select new { a.UNIT };
            foreach (var item in unit.Distinct())
            {
                txtUnit.AutoCompleteCustomSource.Add(item.UNIT);
            }


        }

        private void readDepreciation()
        {
                Runner.Run(delegate
                {
                    DataTable depreciation = (from d in DBDataContext.Db.TBL_DEPRECIATIONs
                                              join c in DBDataContext.Db.TLKP_CURRENCies on d.CURRENCY_ID equals c.CURRENCY_ID
                                              where d.IS_ACTIVE
                                                   && d.FIX_ASSET_ITEM_ID == _objNew.FIX_ASSET_ITEM_ID
                                                   && (cboDepreciationStatus.SelectedIndex == 0 || d.STATUS_ID == cboDepreciationStatus.SelectedIndex)
                                                   && (dtpSuggestionDate.Visible == false || d.DEPRECIATION_MONTH <= dtpSuggestionDate.Value)
                                              select new
                                              {
                                                   d.DEPRECIATION_ID,
                                                   IS_CHECK=false,
                                                   ROW_NO = 0,
                                                  IS_DEPRECIATION = d.STATUS_ID == (int)DepreciationStatus.NotYet ? false : true,
                                                  d.DEPRECIATION_DATE,
                                                  d.DEPRECIATION_MONTH,
                                                  d.DEPRECIATION_BY, 
                                                  AMOUNT = d.AMOUNT,
                                                  ACCUMULATED = 0.0m,
                                                  BOOK_VALUE = 0.0m,
                                                  c.CURRENCY_SING,
                                                  d.STATUS_ID,
                                              })._ToDataTable();
                    var bookValue = this._objNew.TOTAL_COST - this._objNew.SALVAGE_VALUE;
                    var accumulated = 0.0m;
                    var i = 0;
                    foreach (DataRow dr in depreciation.Rows)
                    {
                        bookValue -= (decimal)dr["AMOUNT"];
                        accumulated += (decimal)dr["AMOUNT"];
                        dr["BOOK_VALUE"] = bookValue;
                        dr["ACCUMULATED"] = accumulated;
                        dr["ROW_NO"] = ++i;
                    } 
                    dgv.DataSource = depreciation;

                    // current value & book value.
                    accumulated = this.getTotalDepreciation();
                    bookValue = this._objNew.TOTAL_COST - this._objNew.SALVAGE_VALUE - accumulated;
                    this.txtTotalAccumulated.Text =UIHelper.FormatCurrency( accumulated ,_objNew.CURRENCY_ID);
                    this.txtAccDepreciation.Text = txtTotalAccumulated.Text;
                    this.txtBookValue.Text = UIHelper.FormatCurrency(bookValue, _objNew.CURRENCY_ID) ;
                }, Resources.PROCESSING);
        }

        private decimal getTotalDepreciation()
        {
            decimal tmp = 0.0m;
            foreach (DataGridViewRow row in this.dgv.Rows)
            {
                //Only sum all invoice due amount
                if ((int)row.Cells[STATUS_ID.Name].Value == (int)DepreciationStatus.ReadyDepreciation)
                {
                    tmp += DataHelper.ParseToDecimal(row.Cells[this.ACCUMULATE_DEPRECIATION_1.Name].Value.ToString());
                }
            }
            return tmp;
        }

        private void readRepairMaintenance()
        {
            int currencyId=(int)cboCurrencyMaintenance.SelectedValue;
            var db = from f in DBDataContext.Db.TBL_FIX_ASSET_ITEMs
                     join m in DBDataContext.Db.TBL_FIX_ASSET_MAINTENANCEs on f.FIX_ASSET_ITEM_ID equals m.FIX_ASSET_ITEM_ID
                     join a in DBDataContext.Db.TBL_ACCOUNT_CHARTs on m.PAYMENT_ACCOUNT_ID equals a.ACCOUNT_ID
                     join c in DBDataContext.Db.TLKP_CURRENCies on m.CURRENCY_ID equals c.CURRENCY_ID
                     where f.FIX_ASSET_ITEM_ID == _objNew.FIX_ASSET_ITEM_ID && f.IS_ACTIVE
                        && (currencyId==0 || c.CURRENCY_ID==currencyId)
                     select new
                     {
                         m.ID,
                         f.FIX_ASSET_ITEM_ID,
                         m.TRAN_DATE,
                         m.TRAN_BY,
                         m.REF_NO,
                         a.ACCOUNT_NAME,
                         m.AMOUNT,
                         c.CURRENCY_SING,
                        m.CURRENCY_ID
                     };
            dgvMaintenance.DataSource = db;
            var dBalance = (from f in DBDataContext.Db.TBL_FIX_ASSET_MAINTENANCEs
                            join c in DBDataContext.Db.TLKP_CURRENCies on f.CURRENCY_ID equals c.CURRENCY_ID
                            where f.FIX_ASSET_ITEM_ID == _objNew.FIX_ASSET_ITEM_ID && f.IS_ACTIVE
                                && (currencyId==0 || c.CURRENCY_ID==currencyId)
                            select new
                            {
                                c.CURRENCY_ID,
                                c.CURRENCY_SING,
                                f.AMOUNT
                            }).GroupBy(x => new { x.CURRENCY_ID,x.CURRENCY_SING })
                             .Select(x => new 
                             { 
                                 x.Key.CURRENCY_ID,
                                 x.Key.CURRENCY_SING,
                                 TOTAL_BALANCE_MAINTENANCE = x.Sum(r => r.AMOUNT)
                             });
            dgvTotalAmountMaintenance.DataSource = dBalance;  
        }

        private void readDispose()
        {
            var data = DBDataContext.Db.TBL_FIX_ASSET_DISPOSEs.Where(x => x.FIX_ASSET_ITEM_ID == _objNew.FIX_ASSET_ITEM_ID && x.IS_ACTIVE)
                                                              .Select(x => new
                                                              {
                                                                  x.DISPOSE_ID,
                                                                  DISPOSE_TRAN_DATE=x.TRAN_DATE,
                                                                  x.CREATE_BY,
                                                                  x.QTY,
                                                                  x.PRICE,
                                                                  DISPOSE_SOLD_VALUE=x.QTY * x.PRICE,
                                                                  DISPOSE_BOOK_VALUE=x.BOOK_VALUE,
                                                                  DISPOSE_GAIN_LOSS=x.SOLD_VALUE-x.BOOK_VALUE,
                                                                  x.CURRENCY_ID
                                                              });
            dgvDispose.DataSource = data;

            txtTotalDisposeSoldValue.Text = UIHelper.FormatCurrency(data.Sum(x => (decimal?)x.DISPOSE_SOLD_VALUE) ?? 0, _objNew.CURRENCY_ID);
            txtTotalDisposeBookValue.Text = UIHelper.FormatCurrency(data.Sum(x => (decimal?)x.DISPOSE_BOOK_VALUE) ?? 0, _objNew.CURRENCY_ID);
            txtTotalDisposeGainLoss.Text = UIHelper.FormatCurrency(data.Sum(x => (decimal?)x.DISPOSE_GAIN_LOSS) ?? 0, _objNew.CURRENCY_ID);
            if (DataHelper.ParseToDecimal(txtTotalDisposeGainLoss.Text) > 0)
            {
                disabledAfterDepreciation();
                lblDEPRECIATION.Enabled = DBDataContext.Db.TBL_FIX_ASSET_ITEMs.FirstOrDefault(x => x.FIX_ASSET_ITEM_ID == _objNew.FIX_ASSET_ITEM_ID).STATUS_ID == (int)FixAssetItemStatus.USING;
            }
        }

        #endregion

        private void InputerNumberOnly(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputNumberOnly(sender, e);
        }

        private void ChangeKeyboardKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeKeyboardEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }    

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
             //If record is duplicate.
            this.ClearAllValidation();
            if (DBDataContext.Db.TBL_FIX_ASSET_ITEMs.Where(x =>x.IS_ACTIVE
                && x.CATEGORY_ID == (int)this._objNew.CATEGORY_ID
                && x.FIX_ASSET_NAME.ToLower() == txtFixAssetName.Text.ToLower()
                && x.BRAND==txtBrand.Text
                && x.SOURCE==txtSource.Text
                && x.USE_DATE==dtpUseDate.Value).Count() > 0 && _flag==GeneralProcess.Insert)
            {
                txtFixAssetName.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblFIX_ASSET.Text));
                return;
            }

            try
            {
                Runner.RunNewThread(delegate
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        if (_flag == GeneralProcess.Insert)
                        {
                            DBDataContext.Db.Insert(_objNew);
                        }
                        else if (_flag == GeneralProcess.Update)
                        {
                            DBDataContext.Db.Update(_objOld, _objNew);
                        }
                        else if (_flag == GeneralProcess.Delete)
                        {
                            DBDataContext.Db.Delete(_objNew);
                            // Delete Dispose
                            DBDataContext.Db.ExecuteCommand("UPDATE TBL_FIX_ASSET_DISPOSE SET IS_ACTIVE=0 WHERE FIX_ASSET_ITEM_ID={0}", _objNew.FIX_ASSET_ITEM_ID);
                            // Delete Depreciation
                            DBDataContext.Db.ExecuteCommand("UPDATE TBL_DEPRECIATION SET IS_ACTIVE=0 WHERE FIX_ASSET_ITEM_ID={0}", _objNew.FIX_ASSET_ITEM_ID);
                        }
                        // Generate Schedule
                        if (_flag == GeneralProcess.Insert || _objNew.USE_DATE!=_objOld.USE_DATE || _objNew.CURRENCY_ID!=_objOld.CURRENCY_ID || _objNew.USEFUL_LIFE!=_objOld.USEFUL_LIFE || _objNew.ANNUAL_DEPRECIATION_RATE!=_objOld.ANNUAL_DEPRECIATION_RATE || _objNew.TOTAL_COST!=_objOld.TOTAL_COST)
                        {
                            DBDataContext.Db.ExecuteCommand(@"EXEC GENERATE_DEPRECIATION @FIX_ASSET_ITEM_ID={0},@CREATE_BY={1} ;", _objNew.FIX_ASSET_ITEM_ID, Login.CurrentLogin.LOGIN_NAME);
                        }
                        tran.Complete();
                        this.DialogResult = DialogResult.OK;
                    }
                }, Resources.PROCESSING);
                                                               
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }            
        }
       
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }  

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }  

        private void txtQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void txtPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            txtAmount.Text =UIHelper.FormatCurrency(DataHelper.ParseToDecimal(txtQty.Text) * DataHelper.ParseToDecimal(txtPrice.Text),DataHelper.ParseToInt(cboCurrency.SelectedValue.ToString()));
        } 
        

        private void txtDeperciationPeriod_TextChanged(object sender, EventArgs e)
        {
            if (DataHelper.ParseToDecimal(txtUseFulLife.Text) > 0)
            {
                txtDepreciationRate.Text = ((1 / DataHelper.ParseToDecimal(txtUseFulLife.Text))*100).ToString("0.00");
            }
            else
            {
                txtDepreciationRate.Text = "0";
            }
        }


        private void cboStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (load)
            {
                if ((int)cboDepreciationStatus.SelectedValue ==(int)DepreciationStatus.ReadyDepreciation)
                {
                    dtpSuggestionDate.Visible = false;
                    lblREMOVE_DEPRECIATION.Visible = true;
                }
                else if ((int)cboDepreciationStatus.SelectedValue == (int)DepreciationStatus.NotYet)
                {
                    dtpSuggestionDate.Visible = true;
                    lblREMOVE_DEPRECIATION.Visible = false;
                }
                else
                {
                    dtpSuggestionDate.Visible = false;
                    lblREMOVE_DEPRECIATION.Visible = false;
                }
                readDepreciation();
            }
        }

        private void disabledAfterDepreciation()
        {
            txtQty.Enabled =
            dtpUseDate.Enabled =
            cboCurrency.Enabled =
            txtUseFulLife.Enabled =
            txtDepreciationRate.Enabled =
            txtPrice.Enabled =
            txtSalvageValue.Enabled = false;
        }

        private void txtUseFulLife_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            CrystalReportHelper ch = new CrystalReportHelper("ReportDepreciationByItem.rpt");
            ch.SetParameter("@FIX_ASSET_ITEM_ID", _objNew.FIX_ASSET_ITEM_ID);
            ch.SetParameter("@STATUS_TEXT", cboDepreciationStatus.Text);
            ch.SetParameter("@STATUS_ID",(int)cboDepreciationStatus.SelectedValue);
            ch.ViewReport("");
            ch.Dispose();
        }

        private void lblAddRepairMaintenance_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            TBL_FIX_ASSET_MAINTENANCE objFixAssetRepairMaintenance = new TBL_FIX_ASSET_MAINTENANCE()
            {
                CURRENCY_ID=_objNew.CURRENCY_ID,
                PAYMENT_ACCOUNT_ID=_objNew.PAYMENT_ACCOUNT_ID,
                TRAN_DATE=DBDataContext.Db.GetSystemDate()
            };
            DialogFixAssetRepairMaintenance dig = new DialogFixAssetRepairMaintenance(GeneralProcess.Insert, _objNew, objFixAssetRepairMaintenance);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                readRepairMaintenance();
            }
        }

        private void cboCurrencyMaintenance_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (load)
            {
                readRepairMaintenance();
            }
        }

        private void lblEditRepairMaintenance_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvMaintenance.Rows.Count > 0)
            {
                int id=(int)dgvMaintenance.SelectedRows[0].Cells[ID.Name].Value;
                TBL_FIX_ASSET_MAINTENANCE objFixAssetRepairMaintenance = DBDataContext.Db.TBL_FIX_ASSET_MAINTENANCEs.FirstOrDefault(x => x.ID == id);
                DialogFixAssetRepairMaintenance dig = new DialogFixAssetRepairMaintenance(GeneralProcess.Update, _objNew, objFixAssetRepairMaintenance);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    readRepairMaintenance();
                    UIHelper.SelectRow(dgvMaintenance, id);
                }
            }
        }

        private void lblDeleteRepairMaintenance_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvMaintenance.Rows.Count > 0)
            {
                int id = (int)dgvMaintenance.SelectedRows[0].Cells[ID.Name].Value;
                TBL_FIX_ASSET_MAINTENANCE objFixAssetRepairMaintenance = DBDataContext.Db.TBL_FIX_ASSET_MAINTENANCEs.FirstOrDefault(x => x.ID == id);
                DialogFixAssetRepairMaintenance dig = new DialogFixAssetRepairMaintenance(GeneralProcess.Delete, _objNew, objFixAssetRepairMaintenance);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    readRepairMaintenance();
                    UIHelper.SelectRow(dgvMaintenance, id-1);
                }
            }
        }

        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            var row = this.dgv.Rows[e.RowIndex];
            int statusId = (int)row.Cells[STATUS_ID.Name].Value;
            DateTime depreciationMonth = (DateTime)row.Cells[MONTH.Name].Value;

            // SET TRUE TO CHECK 
            if (statusId == (int)DepreciationStatus.ReadyDepreciation)
            {
                row.Cells[e.ColumnIndex].Style.ForeColor = Color.Green;
                row.Cells[e.ColumnIndex].Style.SelectionForeColor = Color.Green;
            }
            else if (statusId == (int)DepreciationStatus.NotYet && depreciationMonth <= dtpSuggestionDate.Value)
            {
                // SET COLOR 
                row.Cells[e.ColumnIndex].Style.ForeColor = Color.Blue;
                row.Cells[e.ColumnIndex].Style.SelectionForeColor = Color.Blue;
            }
            else
            {
                // SET COLOR 
                row.Cells[e.ColumnIndex].Style.ForeColor = Color.Black;
                row.Cells[e.ColumnIndex].Style.SelectionForeColor = Color.Black;
            } 
        }

        private void lblDepreciation_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            TBL_FIX_ASSET_ITEM objFixAssetItem = DBDataContext.Db.TBL_FIX_ASSET_ITEMs.FirstOrDefault(x => x.FIX_ASSET_ITEM_ID == _objNew.FIX_ASSET_ITEM_ID);
            DialogDepreciationByItem dig = new DialogDepreciationByItem(objFixAssetItem);
            if (dig.ShowDialog() == DialogResult.OK)
            {
                cboStatus_SelectedIndexChanged(null, null);
                disabledAfterDepreciation();
            } 
        }

        private void cbCheckAll_CheckedChanged(object sender, EventArgs e)
        {
            if (load)
            {
                foreach (DataGridViewRow row in this.dgv.Rows)
                {
                    row.Cells[IS_CHECK_.Name].Value = this.chbCheckAll_.Checked;
                }
            }
            
        }

        private void lblDeleteDepreciation_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                bool IsHasDepreciation = false;
                foreach (DataGridViewRow row in this.dgv.Rows)
                {
                    if ((bool)row.Cells[IS_CHECK_.Name].Value == true)
                    {
                        IsHasDepreciation = true;
                        break;
                    }
                }
                if (IsHasDepreciation)
                {
                    
                    if (MsgBox.ShowQuestion(Resources.MSQ_CONFIRM_TO_DELETE_DEPRICIATION, Resources.DELETE) == DialogResult.Yes)
                    {
                        using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                        {
                            DateTime date = DBDataContext.Db.GetSystemDate();
                            foreach (DataGridViewRow row in this.dgv.Rows)
                            {
                                if ((bool)row.Cells[IS_CHECK_.Name].Value == true)
                                {
                                    int depreciationId = (int)row.Cells[DEPRECIATION_ID.Name].Value;
                                    TBL_DEPRECIATION objDepreciation = DBDataContext.Db.TBL_DEPRECIATIONs.FirstOrDefault(x => x.DEPRECIATION_ID == depreciationId); 

                                    if (DBDataContext.Db.TBL_FIX_ASSET_DISPOSEs.Where(x => x.FIX_ASSET_ITEM_ID == _objNew.FIX_ASSET_ITEM_ID && x.IS_ACTIVE
                                                                                            && x.TRAN_DATE > objDepreciation.DEPRECIATION_DATE).Count() > 0)
                                    {
                                        MsgBox.ShowInformation(string.Format(Resources.MS_CANNOT_DELETE_DEPRICIATION,objDepreciation.DEPRECIATION_MONTH.ToString("MM-yyyy")),Resources.DELETE);
                                        return;
                                    }
                                    
                                    objDepreciation.STATUS_ID = (int)DepreciationStatus.NotYet;
                                    objDepreciation.DEPRECIATION_DATE = objDepreciation.CREATE_DATE;
                                    objDepreciation.DEPRECIATION_BY = "";
                                    objDepreciation.IS_ACTIVE = true;
                                    objDepreciation.ROW_DATE = date;

                                    TBL_FIX_ASSET_ITEM objFixAssetItem = DBDataContext.Db.TBL_FIX_ASSET_ITEMs.FirstOrDefault(x => x.FIX_ASSET_ITEM_ID == objDepreciation.FIX_ASSET_ITEM_ID);
                                    objFixAssetItem.ACCUMULATED_DEPRECIATION -= objDepreciation.AMOUNT;
                                    objFixAssetItem.ROW_DATE = date;

                                    DBDataContext.Db.SubmitChanges();
                                }
                            }
                            tran.Complete();
                        }
                        readDepreciation();
                    }
                }
                
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
                
               
        }

        private void lblAddDepose_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DateTime date = DBDataContext.Db.GetSystemDate();
            int AccountId = AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_DEFAULT_ACCOUNTS).FirstOrDefault().ACCOUNT_ID;
            DialogDisposeFixAsset diag = new DialogDisposeFixAsset(GeneralProcess.Insert, _objNew,
                                                                    new TBL_FIX_ASSET_DISPOSE() { TRAN_DATE = date, PAYMENT_ACCOUNT_ID = AccountId });
            if (diag.ShowDialog() == DialogResult.OK)
            {
                readDispose(); 
            }
        }

        private void lblUpdateDepose_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvDispose.Rows.Count > 0)
            {
                int disposeId = (int)dgvDispose.SelectedRows[0].Cells[DISPOSE_ID.Name].Value;
                TBL_FIX_ASSET_DISPOSE objFixAssetDispose = DBDataContext.Db.TBL_FIX_ASSET_DISPOSEs.FirstOrDefault(x => x.DISPOSE_ID == disposeId);
                DialogDisposeFixAsset diag = new DialogDisposeFixAsset(GeneralProcess.Update, _objNew, objFixAssetDispose);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    readDispose(); 
                }
            }
        }

        private void lblDeleteDepose_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvDispose.Rows.Count > 0)
            {
                int disposeId = (int)dgvDispose.SelectedRows[0].Cells[DISPOSE_ID.Name].Value;
                TBL_FIX_ASSET_DISPOSE objFixAssetDispose = DBDataContext.Db.TBL_FIX_ASSET_DISPOSEs.FirstOrDefault(x => x.DISPOSE_ID == disposeId);
                DialogDisposeFixAsset diag = new DialogDisposeFixAsset(GeneralProcess.Delete, _objNew, objFixAssetDispose);
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    readDispose();
                }
            }
        }

         
         
    }
}