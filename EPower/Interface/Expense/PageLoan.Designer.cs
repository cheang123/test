﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageLoan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageLoan));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboLookup = new System.Windows.Forms.ComboBox();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.LOAN_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOAN_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOAN_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLOSE_DATE_I = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BANK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOAN_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANNUAL_INTEREST_RATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOAN_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAID_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboLookup);
            this.panel1.Controls.Add(this.cboCurrency);
            this.panel1.Controls.Add(this.cboStatus);
            this.panel1.Controls.Add(this.txtQuickSearch);
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboLookup
            // 
            this.cboLookup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLookup.FormattingEnabled = true;
            resources.ApplyResources(this.cboLookup, "cboLookup");
            this.cboLookup.Name = "cboLookup";
            this.cboLookup.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboStatus, "cboStatus");
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LOAN_ID,
            this.LOAN_NO,
            this.LOAN_DATE,
            this.CLOSE_DATE_I,
            this.BANK,
            this.LOAN_ACCOUNT,
            this.ANNUAL_INTEREST_RATE,
            this.LOAN_AMOUNT,
            this.PAID_AMOUNT,
            this.CURRENCY_SING_});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // LOAN_ID
            // 
            this.LOAN_ID.DataPropertyName = "LOAN_ID";
            resources.ApplyResources(this.LOAN_ID, "LOAN_ID");
            this.LOAN_ID.Name = "LOAN_ID";
            // 
            // LOAN_NO
            // 
            this.LOAN_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.LOAN_NO.DataPropertyName = "LOAN_NO";
            resources.ApplyResources(this.LOAN_NO, "LOAN_NO");
            this.LOAN_NO.Name = "LOAN_NO";
            // 
            // LOAN_DATE
            // 
            this.LOAN_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.LOAN_DATE.DataPropertyName = "LOAN_DATE";
            dataGridViewCellStyle2.Format = "dd-MM-yyyy";
            dataGridViewCellStyle2.NullValue = null;
            this.LOAN_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.LOAN_DATE, "LOAN_DATE");
            this.LOAN_DATE.Name = "LOAN_DATE";
            // 
            // CLOSE_DATE_I
            // 
            this.CLOSE_DATE_I.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CLOSE_DATE_I.DataPropertyName = "CLOSE_DATE";
            dataGridViewCellStyle3.Format = "dd-MM-yyyy";
            dataGridViewCellStyle3.NullValue = null;
            this.CLOSE_DATE_I.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CLOSE_DATE_I, "CLOSE_DATE_I");
            this.CLOSE_DATE_I.Name = "CLOSE_DATE_I";
            // 
            // BANK
            // 
            this.BANK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.BANK.DataPropertyName = "BANK_NAME";
            resources.ApplyResources(this.BANK, "BANK");
            this.BANK.Name = "BANK";
            // 
            // LOAN_ACCOUNT
            // 
            this.LOAN_ACCOUNT.DataPropertyName = "ACCOUNT_NAME";
            resources.ApplyResources(this.LOAN_ACCOUNT, "LOAN_ACCOUNT");
            this.LOAN_ACCOUNT.Name = "LOAN_ACCOUNT";
            // 
            // ANNUAL_INTEREST_RATE
            // 
            this.ANNUAL_INTEREST_RATE.DataPropertyName = "ANNUAL_INTEREST_RATE";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N4";
            dataGridViewCellStyle4.NullValue = null;
            this.ANNUAL_INTEREST_RATE.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.ANNUAL_INTEREST_RATE, "ANNUAL_INTEREST_RATE");
            this.ANNUAL_INTEREST_RATE.Name = "ANNUAL_INTEREST_RATE";
            // 
            // LOAN_AMOUNT
            // 
            this.LOAN_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.LOAN_AMOUNT.DataPropertyName = "LOAN_AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,##0.####";
            dataGridViewCellStyle5.NullValue = null;
            this.LOAN_AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.LOAN_AMOUNT, "LOAN_AMOUNT");
            this.LOAN_AMOUNT.Name = "LOAN_AMOUNT";
            // 
            // PAID_AMOUNT
            // 
            this.PAID_AMOUNT.DataPropertyName = "PAID_AMOUNT";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "#,##0.####";
            dataGridViewCellStyle6.NullValue = null;
            this.PAID_AMOUNT.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.PAID_AMOUNT, "PAID_AMOUNT");
            this.PAID_AMOUNT.Name = "PAID_AMOUNT";
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CURRENCY_SING_.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            // 
            // PageLoan
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageLoan";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private ExButton btnREMOVE;
        private DataGridView dgv;
        private ComboBox cboStatus;
        private ExTextbox txtQuickSearch;
        private ComboBox cboCurrency;
        private ComboBox cboLookup;
        private DataGridViewTextBoxColumn LOAN_ID;
        private DataGridViewTextBoxColumn LOAN_NO;
        private DataGridViewTextBoxColumn LOAN_DATE;
        private DataGridViewTextBoxColumn CLOSE_DATE_I;
        private DataGridViewTextBoxColumn BANK;
        private DataGridViewTextBoxColumn LOAN_ACCOUNT;
        private DataGridViewTextBoxColumn ANNUAL_INTEREST_RATE;
        private DataGridViewTextBoxColumn LOAN_AMOUNT;
        private DataGridViewTextBoxColumn PAID_AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
    }
}
