﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using HB01.Domain.Enums;
using HB01.Domain.ListModels;
using HB01.Helpers.DevExpressCustomize;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogAccountTransaction : ExDialog
    {
        #region PrivateData
        GeneralProcess _flag;
        private AccountType _tranType;

        TBL_ACCOUNT_TRAN _objNew = new TBL_ACCOUNT_TRAN();
        TBL_ACCOUNT_TRAN _objOld = new TBL_ACCOUNT_TRAN();

        #endregion

        #region Constructor
        public DialogAccountTransaction(GeneralProcess flag, TBL_ACCOUNT_TRAN objAccountTran, AccountType tranType)
        {
            InitializeComponent();
            _flag = flag;
            _tranType = tranType;
            objAccountTran._CopyTo(_objNew);
            objAccountTran._CopyTo(_objOld);
            _objNew.ACCOUNT_TYPE_ID = (int)tranType;



            //Account item

            var accounts = new List<AccountListModel>();
            if (tranType == AccountType.Income)
            {
                accounts = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Income } });
            }
            else
            {
                accounts = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Expense } });
            }
            this.cboTransAccount.SetDatasourceList(accounts, nameof(AccountListModel.DisplayAccountName));
            var pAcc = AccountChartHelper.GetAccounts(new AccountSearchParam { Natures = new List<AccountNatures> { AccountNatures.Asset } });
            this.cboPaymentAccount.SetDatasourceList(pAcc, nameof(AccountListModel.DisplayAccountName));
            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());

            //Initailize control 
            read();

            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
        }

        public TBL_ACCOUNT_TRAN AccountTran
        {
            get { return _objNew; }
        }

        #endregion

        #region Operation

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            //Set display text dialog.
            this.Text = DBDataContext.Db.TBL_ACCOUNT_TYPEs.OrderBy(x => x.INDEX).FirstOrDefault(x => x.TYPE_ID == (int)_tranType).TYPE_NAME;
            this.lblACCOUNT.Text += this.Text;
            this.Text = _flag.GetText(this.Text);
        }

        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboardKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeKeyboardEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //If record is duplicate.
            this.ClearAllValidation();
            if (DBDataContext.Db.TBL_ACCOUNT_TRANs.Where(x => x.TRAN_ACCOUNT_ID == _objNew.TRAN_ACCOUNT_ID
                && x.REF_NO.ToLower() == txtRefNo.Text.ToLower()
                && x.TRAN_BY.ToLower() == txtTranBy.Text.ToLower()
                && x.AMOUNT == _objNew.AMOUNT
                && x.TRAN_DATE.Date == _objNew.TRAN_DATE.Date
                && x.IS_ACTIVE
                && x.TRAN_ID != _objNew.TRAN_ID).Count() > 0)
            {
                if (MsgBox.ShowQuestion(string.Format(Resources.MSQ_ACCOUNT_TRANSACTION_IS_EXISTS, this._flag.GetText(string.Empty)), this.Text) == DialogResult.No)
                {
                    cboTransAccount.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblACCOUNT.Text));
                    txtRefNo.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblREF_NO.Text));
                    txtTranBy.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblCREATE_BY.Text));
                    dtpTranDate.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblDATE.Text));
                    txtAmount.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblAMOUNT.Text));
                    return;
                }
            }

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (cboTransAccount.EditValue == null)
            {
                cboTransAccount.SetValidation(string.Format(Resources.REQUIRED, lblACCOUNT.Text));
                val = true;

            }
            //if (cboTransAccount.EditValue != null &&
            //    cboTransAccount.SelectedNode.Nodes.Count > 0)
            //{
            //    cboTransAccount.SetValidation(Resources.MS_PLEASE_SELECT_SMALL_TYPE);
            //    val = true;
            //}


            if (cboPaymentAccount.EditValue == null)
            {
                cboPaymentAccount.SetValidation(string.Format(Resources.REQUIRED, lblPAYMENT_ACCOUNT.Text));
                val = true;
            }
            //if (cboPaymentAccount.SelectedNode != null &&
            //    cboPaymentAccount.SelectedNode.Nodes.Count > 0)
            //{
            //    cboTransAccount.SetValidation(Resources.MS_PLEASE_SELECT_SMALL_TYPE);
            //    val = true;
            //}
            if (txtTranBy.Text.Trim() == string.Empty)
            {
                txtTranBy.SetValidation(string.Format(Resources.REQUIRED, lblCREATE_BY.Text));
                val = true;
            }
            if (!DataHelper.IsNumber(txtAmount.Text))
            {
                txtAmount.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (DataHelper.ParseToDecimal(txtAmount.Text) == 0)
            {
                txtAmount.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (cboCurrency.SelectedValue == null)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, Resources.CURRENCY));
                val = true;
            }
            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            txtRefNo.Text = _objNew.REF_NO;
            txtTranBy.Text = _objNew.TRAN_BY;
            dtpTranDate.Value = _objNew.TRAN_DATE;
            txtAmount.Text = UIHelper.FormatCurrency(_objNew.AMOUNT, _objNew.CURRENCY_ID);
            txtNote.Text = _objNew.NOTE;
            cboCurrency.SelectedValue = _objNew.CURRENCY_ID;
            cboPaymentAccount.EditValue = _objNew.PAYMENT_ACCOUNT_ID;
            cboTransAccount.EditValue = _objNew.TRAN_ACCOUNT_ID;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.TRAN_ACCOUNT_ID = (int)cboTransAccount.EditValue;
            _objNew.PAYMENT_ACCOUNT_ID = (int)cboPaymentAccount.EditValue;
            _objNew.CURRENCY_ID = (int)cboCurrency.SelectedValue;
            _objNew.REF_NO = txtRefNo.Text;
            _objNew.TRAN_BY = txtTranBy.Text;
            _objNew.TRAN_DATE = dtpTranDate.Value;
            _objNew.AMOUNT = DataHelper.ParseToDecimal(UIHelper.FormatCurrency(decimal.Parse(txtAmount.Text), (int)cboCurrency.SelectedValue));
            _objNew.NOTE = txtNote.Text;

        }
        #endregion

    }
}