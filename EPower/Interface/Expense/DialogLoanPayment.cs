﻿using EPower.Base.Helper;
using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogLoanPayment : ExDialog
    {
        GeneralProcess _flag;

        TBL_LOAN_PAYMENT _objNew = new TBL_LOAN_PAYMENT();
        TBL_LOAN_PAYMENT _objOld = new TBL_LOAN_PAYMENT();
        public TBL_LOAN_PAYMENT LoanPayment
        {
            get { return _objNew; }
        }

        TBL_LOAN _objLoan = new TBL_LOAN();

        #region Constructor
        public DialogLoanPayment(GeneralProcess flag, TBL_LOAN_PAYMENT objLoanPayment, TBL_LOAN objLoan)
        {
            InitializeComponent();
            _flag = flag;
            objLoanPayment._CopyTo(_objNew);
            objLoanPayment._CopyTo(_objOld);
            objLoan._CopyTo(_objLoan);
            // Setup Account
            new AccountChartPopulator().PopluateTree(cboTransAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.EXPENSE_INTEREST));
            if (cboTransAccount.TreeView.Nodes.Count > 0)
            {
                cboTransAccount.TreeView.Nodes[0].Expand();
            }
            new AccountChartPopulator().PopluateTree(cboPaymentAccount.TreeView, AccountChartHelper.GetAccounts(AccountConfig.PAYMENT_ACCOUNTS));
            if (cboTransAccount.TreeView.Nodes.Count > 0)
            {
                cboTransAccount.TreeView.Nodes[0].Expand();
            }

            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());

            //Initailize control 
            read();

            this.Text = flag.GetText(this.Text);
            UIHelper.SetEnabled(this, !(flag == GeneralProcess.Delete));
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            cboCurrency.Enabled = false;
        }
        #endregion



        #region Operation
        /// <summary>
        /// Change Keyboard to Khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKeyboardKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void ChangeKeyboardEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        /// <summary>
        /// Data transaction.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //If record is duplicate.
            this.ClearAllValidation();

            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    DateTime date = DBDataContext.Db.GetSystemDate();
                    TBL_LOAN objLoan = DBDataContext.Db.TBL_LOANs.FirstOrDefault(x => x.LOAN_ID == _objLoan.LOAN_ID);
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objNew);
                        objLoan.PAID_AMOUNT += _objNew.PAY_AMOUNT;
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_objOld, _objNew);
                        objLoan.PAID_AMOUNT += _objNew.PAY_AMOUNT - _objOld.PAY_AMOUNT;
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objNew);
                        objLoan.PAID_AMOUNT += -_objOld.PAY_AMOUNT;
                    }
                    objLoan.ROW_DATE = date;
                    DBDataContext.Db.SubmitChanges();
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        #endregion

        #region Method
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();
            if (cboTransAccount.TreeView.SelectedNode == null)
            {
                cboTransAccount.SetValidation(string.Format(Resources.REQUIRED, lblEXPENSE_ACCOUNT.Text));
                val = true;

            }
            if (cboTransAccount.SelectedNode != null &&
                cboTransAccount.SelectedNode.Nodes.Count > 0)
            {
                cboTransAccount.SetValidation(Resources.MS_CHOOSE_CHILD_END_ACCOUNT);
                val = true;
            }

            if (cboPaymentAccount.TreeView.SelectedNode == null)
            {
                cboPaymentAccount.SetValidation(string.Format(Resources.REQUIRED, lblPAYMENT_ACCOUNT.Text));
                val = true;
            }
            if (cboPaymentAccount.SelectedNode != null &&
                cboPaymentAccount.SelectedNode.Nodes.Count > 0)
            {
                cboTransAccount.SetValidation(Resources.MS_CHOOSE_CHILD_END_ACCOUNT);
                val = true;
            }

            if (txtInterestAmount.Text == string.Empty)
            {
                txtInterestAmount.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                val = true;
            }
            if (cboCurrency.SelectedValue == null)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, Resources.CURRENCY));
                val = true;
            }
            return val;
        }

        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            // Account
            cboPaymentAccount.TreeView.SelectedNode = cboPaymentAccount.TreeView.GetNodesIncludeAncestors().FirstOrDefault(x => (int)x.Tag == _objNew.PAYMENT_ACCOUNT_ID);
            cboPaymentAccount.Text = cboPaymentAccount.TreeView.SelectedNode != null ? cboPaymentAccount.TreeView.SelectedNode.Text : "";
            cboTransAccount.TreeView.SelectedNode = cboTransAccount.TreeView.GetNodesIncludeAncestors().FirstOrDefault(x => (int)x.Tag == _objNew.EXPENSE_ACCOUNT_ID);
            cboTransAccount.Text = cboTransAccount.TreeView.SelectedNode != null ? cboTransAccount.TreeView.SelectedNode.Text : "";
            txtLoanNo.Text = _objLoan.LOAN_NO;
            dtpTranDate.Value = _objNew.PAY_DATE;
            txtInterestAmount.Text = _objNew.INTEREST_AMOUNT == 0 ? "0" : UIHelper.FormatCurrency(_objNew.INTEREST_AMOUNT, _objLoan.CURRENCY_ID);
            txtCapitalAmount.Text = _objNew.CAPITAL_AMOUNT == 0 ? "0" : UIHelper.FormatCurrency(_objNew.CAPITAL_AMOUNT, _objLoan.CURRENCY_ID);
            txtPaidAmount.Text = UIHelper.FormatCurrency(_objNew.PAY_AMOUNT, _objLoan.CURRENCY_ID);
            txtNote.Text = _objNew.NOTE;
            cboCurrency.SelectedValue = _objLoan.CURRENCY_ID;

        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objNew.EXPENSE_ACCOUNT_ID = (int)cboTransAccount.TreeView.SelectedNode.Tag;
            _objNew.PAYMENT_ACCOUNT_ID = (int)cboPaymentAccount.TreeView.SelectedNode.Tag;
            _objNew.PAY_DATE = new DateTime(dtpTranDate.Value.Year, dtpTranDate.Value.Month, dtpTranDate.Value.Day);
            _objNew.INTEREST_AMOUNT = DataHelper.ParseToDecimal(UIHelper.FormatCurrency(DataHelper.ParseToDecimal(txtInterestAmount.Text), _objLoan.CURRENCY_ID));
            _objNew.CAPITAL_AMOUNT = DataHelper.ParseToDecimal(UIHelper.FormatCurrency(DataHelper.ParseToDecimal(txtCapitalAmount.Text), _objLoan.CURRENCY_ID));
            _objNew.PAY_AMOUNT = DataHelper.ParseToDecimal(UIHelper.FormatCurrency(DataHelper.ParseToDecimal(txtPaidAmount.Text), _objLoan.CURRENCY_ID));
            _objNew.NOTE = txtNote.Text;
            _objNew.LOAN_ID = _objLoan.LOAN_ID;
        }
        #endregion

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void txtInterestAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void txtInterestAmount_TextChanged(object sender, EventArgs e)
        {
            txtPaidAmount.Text = UIHelper.FormatCurrency((DataHelper.ParseToDecimal(txtInterestAmount.Text) + DataHelper.ParseToDecimal(txtCapitalAmount.Text)), _objLoan.CURRENCY_ID);
        }
    }
}