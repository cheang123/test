﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogAccountTransactionCancel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogAccountTransactionCancel));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label1 = new System.Windows.Forms.Label();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.cboAccountItem = new System.Windows.Forms.ComboBox();
            this.lblPAYMENT_ACCOUNT = new System.Windows.Forms.Label();
            this.lblCREATE_BY = new System.Windows.Forms.Label();
            this.lblDATE = new System.Windows.Forms.Label();
            this.dtpTranDate = new System.Windows.Forms.DateTimePicker();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.txtRefNo = new System.Windows.Forms.TextBox();
            this.lblREF_NO = new System.Windows.Forms.Label();
            this.txtTranBy = new System.Windows.Forms.TextBox();
            this.txtNoteCancel = new System.Windows.Forms.TextBox();
            this.lblCANCEL_NOTE = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.label10);
            this.content.Controls.Add(this.txtNoteCancel);
            this.content.Controls.Add(this.lblCANCEL_NOTE);
            this.content.Controls.Add(this.txtTranBy);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label15);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.txtRefNo);
            this.content.Controls.Add(this.lblREF_NO);
            this.content.Controls.Add(this.txtAmount);
            this.content.Controls.Add(this.lblAMOUNT);
            this.content.Controls.Add(this.dtpTranDate);
            this.content.Controls.Add(this.lblDATE);
            this.content.Controls.Add(this.lblCREATE_BY);
            this.content.Controls.Add(this.lblPAYMENT_ACCOUNT);
            this.content.Controls.Add(this.cboAccountItem);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.cboAccountItem, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_ACCOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblCREATE_BY, 0);
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.dtpTranDate, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtAmount, 0);
            this.content.Controls.SetChildIndex(this.lblREF_NO, 0);
            this.content.Controls.SetChildIndex(this.txtRefNo, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label15, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.txtTranBy, 0);
            this.content.Controls.SetChildIndex(this.lblCANCEL_NOTE, 0);
            this.content.Controls.SetChildIndex(this.txtNoteCancel, 0);
            this.content.Controls.SetChildIndex(this.label10, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            this.txtNote.ReadOnly = true;
            this.txtNote.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // cboAccountItem
            // 
            this.cboAccountItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboAccountItem, "cboAccountItem");
            this.cboAccountItem.FormattingEnabled = true;
            this.cboAccountItem.Name = "cboAccountItem";
            // 
            // lblPAYMENT_ACCOUNT
            // 
            resources.ApplyResources(this.lblPAYMENT_ACCOUNT, "lblPAYMENT_ACCOUNT");
            this.lblPAYMENT_ACCOUNT.Name = "lblPAYMENT_ACCOUNT";
            // 
            // lblCREATE_BY
            // 
            resources.ApplyResources(this.lblCREATE_BY, "lblCREATE_BY");
            this.lblCREATE_BY.Name = "lblCREATE_BY";
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // dtpTranDate
            // 
            resources.ApplyResources(this.dtpTranDate, "dtpTranDate");
            this.dtpTranDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTranDate.Name = "dtpTranDate";
            // 
            // txtAmount
            // 
            resources.ApplyResources(this.txtAmount, "txtAmount");
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.ReadOnly = true;
            this.txtAmount.Enter += new System.EventHandler(this.ChangeKeyboardEnglish);
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // txtRefNo
            // 
            resources.ApplyResources(this.txtRefNo, "txtRefNo");
            this.txtRefNo.Name = "txtRefNo";
            this.txtRefNo.ReadOnly = true;
            this.txtRefNo.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // lblREF_NO
            // 
            resources.ApplyResources(this.lblREF_NO, "lblREF_NO");
            this.lblREF_NO.Name = "lblREF_NO";
            // 
            // txtTranBy
            // 
            this.txtTranBy.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTranBy.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            resources.ApplyResources(this.txtTranBy, "txtTranBy");
            this.txtTranBy.Name = "txtTranBy";
            this.txtTranBy.ReadOnly = true;
            this.txtTranBy.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // txtNoteCancel
            // 
            resources.ApplyResources(this.txtNoteCancel, "txtNoteCancel");
            this.txtNoteCancel.Name = "txtNoteCancel";
            this.txtNoteCancel.Enter += new System.EventHandler(this.ChangeKeyboardKhmer);
            // 
            // lblCANCEL_NOTE
            // 
            resources.ApplyResources(this.lblCANCEL_NOTE, "lblCANCEL_NOTE");
            this.lblCANCEL_NOTE.Name = "lblCANCEL_NOTE";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Name = "label10";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Name = "label15";
            // 
            // DialogAccountTransactionCancel
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogAccountTransactionCancel";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label1;
        private TextBox txtNote;
        private Label lblNOTE;
        private Label lblPAYMENT_ACCOUNT;
        public ComboBox cboAccountItem;
        private TextBox txtRefNo;
        private Label lblREF_NO;
        private TextBox txtAmount;
        private Label lblAMOUNT;
        private DateTimePicker dtpTranDate;
        private Label lblDATE;
        private Label lblCREATE_BY;
        private TextBox txtTranBy;
        private Label label10;
        private TextBox txtNoteCancel;
        private Label lblCANCEL_NOTE;
        private Label label15;
        private Label label14;
        private Label label13;
    }
}