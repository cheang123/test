﻿using EPower.Base.Helper;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogAccountConfig : ExDialog
	{
		bool _isSuccess = false;
		GeneralProcess _flag;
		TBL_ACCOUNT_CONFIG _objNew = new TBL_ACCOUNT_CONFIG();
		TBL_ACCOUNT_CONFIG _objOld = new TBL_ACCOUNT_CONFIG();
		List<TBL_ACCOUNT_CHART> accounts = new List<TBL_ACCOUNT_CHART>();
		List<string> _configId = new List<string>();

		#region Contructor
		public DialogAccountConfig(GeneralProcess flag, TBL_ACCOUNT_CONFIG objConfig)
		{
			InitializeComponent();
			this._flag = flag;
			objConfig._CopyTo(this._objNew);
			objConfig._CopyTo(this._objOld);
			if (_flag == GeneralProcess.Update)
			{
				foreach (var item in AccountChartHelper.GetAccounts(DataHelper.ParseToInt(_objNew.CONFIG_ID.ToString())))
				{
					_configId.Add(item.ACCOUNT_ID.ToString());
				}
			}
			read();
		}
		#endregion Contructor

		#region Method
		private void read()
		{
			try
			{
				this.tvw.Nodes.Clear();
				this.accounts = DBDataContext.Db.TBL_ACCOUNT_CHARTs.Where(x => x.IS_ACTIVE).OrderBy(x => x.ACCOUNT_CODE + " " + x.ACCOUNT_NAME).ToList();
				var nodeRoot = new TreeNode();
				nodeRoot.Text = string.Format(Resources.ALL_ACCOUNT_ITEM); // parent category
				nodeRoot.Tag = 0;
				this.tvw.Nodes.Add(nodeRoot);
				addChildNode(nodeRoot);
				tvw.Nodes[0].Expand();
			}
			catch (Exception ex)
			{
				MsgBox.ShowError(ex);
			} 
		}
		private void addChildNode(TreeNode node)
		{
			var id = (int)node.Tag;
			foreach (var account in accounts.Where(x => x.PARENT_ID == id && x.IS_ACTIVE).OrderBy(x => x.ACCOUNT_CODE + " " + x.ACCOUNT_NAME))
			{
				var childNode = new TreeNode();
				childNode.Text = account.ACCOUNT_CODE + " " + account.ACCOUNT_NAME;
				childNode.Tag = account.ACCOUNT_ID;
				node.Nodes.Add(childNode);
				addChildNode(childNode);
				if (_flag == GeneralProcess.Update)
				{
					foreach (var item in _configId)
					{
						if (childNode.Tag.ToString() == item.ToString())
						{
							childNode.Checked = true;
							//node.Expand();
						}
						else
							continue;
					}
				}
			}
		}
		private void save()
		{
			try
			{
				List<string> listCheckedNode = new List<string>();
				using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
				{
					var list = new List<TreeNode>();
                    if (chkINCLUDEROOT.Checked == true)
                    {
                        LookupCheckedIncludedRoot(tvw.Nodes, list);
                    }
                    else if (chkINCLUDEROOT.Checked == false)
                    {
                        lookupCheckedNotIncludeRoot(tvw.Nodes, list);
                    }
					
					foreach (var checkedNodes in list)
					{
						listCheckedNode.Add(checkedNodes.Tag.ToString());
					}
                    if (AccountChartHelper.ConcatAccountId(listCheckedNode).ToString() == string.Empty)
                    {
                        return;
                    }
                    if (_flag == GeneralProcess.Update)
                    {
                        TBL_ACCOUNT_CONFIG _objConfig = new TBL_ACCOUNT_CONFIG();
                        _objConfig.CONFIG_ID = _objNew.CONFIG_ID;
                        _objConfig.CONFIG_NAME = _objNew.CONFIG_NAME;
                        _objConfig.CONFIG_VALUE = AccountChartHelper.ConcatAccountId(listCheckedNode).ToString();
                        DBDataContext.Db.Update(_objOld, _objConfig);
                    }
					tran.Complete();
					_isSuccess = true;
				};
			}
			catch (Exception ex)
			{
				MsgBox.ShowError(ex);                
			}
		}

        private void lookupCheckedNotIncludeRoot(TreeNodeCollection nodes, List<TreeNode> list)
        {
            foreach (TreeNode node in nodes)
            {
                if (node.Checked && DataHelper.ParseToInt(node.Tag.ToString()) != 0)
                {
                    foreach (var item in from c in DBDataContext.Db.TBL_ACCOUNT_CHARTs
                                         where !DBDataContext.Db.TBL_ACCOUNT_CHARTs.Any(acc => acc.PARENT_ID == c.ACCOUNT_ID)
                                         select c)
                    {
                        if (item.ACCOUNT_ID == (int)node.Tag)
                        {
                            list.Add(node);
                        }
                    }
                }
                lookupCheckedNotIncludeRoot(node.Nodes, list);
            }
        }
        private void LookupCheckedIncludedRoot(TreeNodeCollection nodes, List<TreeNode> list)
        {
            foreach (TreeNode node in nodes)
            {
                if (node.Checked && DataHelper.ParseToInt(node.Tag.ToString()) != 0)
                {
                    list.Add(node);
                }
                LookupCheckedIncludedRoot(node.Nodes, list);
            }
        }
		
		#endregion Method

		#region Event
		private void DialogAccountConfig_Load(object sender, EventArgs e)
		{
			Runner.Run(read, Resources.SHOW_INFORMATION);
		}

		private void btnCLOSE_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			Runner.RunNewThread(save, Resources.SAVING_DATA);
			if (_isSuccess)
			{
				this.DialogResult = DialogResult.OK;
			} 
		}
		#endregion Event

	   
	}
}