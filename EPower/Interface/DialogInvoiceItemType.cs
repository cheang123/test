﻿using System;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;

namespace EPower.Interface
{
    public partial class DialogInvoiceItemType : ExDialog
    {        
        GeneralProcess _flag;
        TBL_INVOICE_ITEM_TYPE _objItemType = new TBL_INVOICE_ITEM_TYPE();
        public TBL_INVOICE_ITEM_TYPE InvioceItemType
        {
            get { return _objItemType; }
        }
        TBL_INVOICE_ITEM_TYPE _oldObjItemType = new TBL_INVOICE_ITEM_TYPE();

        #region Constructor
        public DialogInvoiceItemType(GeneralProcess flag, TBL_INVOICE_ITEM_TYPE objItemType)
        {
            InitializeComponent();
            _flag = flag;
            objItemType._CopyTo(_objItemType);
            objItemType._CopyTo(_oldObjItemType);

            if (flag == GeneralProcess.Insert)
            {
                _objItemType.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;            
            read();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Data transaction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            //if duplicate record.
            txtInvoiceItemType.ClearValidation();
            if (DBDataContext.Db.IsExits(_objItemType, "INVOICE_ITEM_TYPE_NAME"))
            {
                txtInvoiceItemType.SetValidation(string.Format( Resources.MS_IS_EXISTS,lblSERVICE_TYPE.Text));                
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objItemType);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjItemType, _objItemType);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objItemType);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }

        /// <summary>
        /// User read only.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        
        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        #endregion

        #region Method
        /// <summary>
        /// Load data from object.
        /// </summary>
        private void read()
        {            
            txtInvoiceItemType.Text = _objItemType.INVOICE_ITEM_TYPE_NAME;
        }

        /// <summary>
        /// Write data to object.
        /// </summary>
        private void write()
        {
            _objItemType.INVOICE_ITEM_TYPE_NAME = txtInvoiceItemType.Text.Trim();
        }

        private bool inValid()
        {
            bool val = false;
            txtInvoiceItemType.ClearValidation();
            if (txtInvoiceItemType.Text.Trim().Length <= 0)
            {
                txtInvoiceItemType.SetValidation(string.Format(Resources.REQUIRED, lblSERVICE_TYPE.Text));
                val = true;
            }
            return val;
        } 
        #endregion   

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objItemType);
        }
    }
}
