﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface.PrePaid
{
    public partial class DialogRenewCard : ExDialog
    {
        TBL_CUSTOMER _objCusReadCard;
        TBL_CUSTOMER _objCustomer;
        TBL_CUSTOMER_METER _objCustomerMeter;
        TBL_METER _objMeter;

        TBL_PREPAID_CUSTOMER _objPrePaidCustomer;
        TBL_CUSTOMER_BUY _objCusBuy;

        TBL_INVOICE_ITEM _objInvoiceItem;
        TBL_INVOICE _objInvoice;
        TBL_INVOICE_DETAIL _objInvoiceDetail;
        TBL_PAYMENT _objPayment;
        TBL_PAYMENT_DETAIL _objPaymentDetail;

        TestCutCard _objTestCutCard;

        public DialogRenewCard()
        {
            InitializeComponent();
            newRenewCard();
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME }));
        }

        private void bind()
        {
            txtCashDrawerName.Text = DBDataContext.Db.TBL_CASH_DRAWERs.FirstOrDefault(x => x.CASH_DRAWER_ID == Login.CurrentCashDrawer.CASH_DRAWER_ID).CASH_DRAWER_NAME;
            txtUserCashDrawerName.Text = Login.CurrentLogin.LOGIN_NAME;

            //Todo:Add customer enum
            var invoiceItem = from invItem in DBDataContext.Db.TBL_INVOICE_ITEMs
                              where invItem.INVOICE_ITEM_ID == (int)InvoiceItem.PrepaidPower
                              select invItem;
            _objInvoiceItem = invoiceItem.FirstOrDefault();
            UIHelper.SetDataSourceToComboBox(cboInvoiceItem, invoiceItem, "INVOICE_ITEM_ID", "INVOICE_ITEM_NAME");
            cboInvoiceItem.SelectedIndex = 0;
            cboCurrency.SelectedValue = _objInvoiceItem.CURRENCY_ID;
            txtPrice.Text = UIHelper.FormatCurrency(_objInvoiceItem.PRICE, _objInvoiceItem.CURRENCY_ID);
        }

        private void write()
        {
            TBL_CHANGE_LOG objlog = null;

            DateTime dtNow = DBDataContext.Db.GetSystemDate();
            decimal decAmount = 0;
            Sequence sequence = Method.GetSequenceId(SequenceType.InvoiceService);
            var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(DBDataContext.Db.GetSystemDate(), (int)cboCurrency.SelectedValue);
            int intDueDay = DataHelper.ParseToInt(DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == (int)Utility.INVOICE_DUE_DATE).UTILITY_VALUE);
            decAmount = DataHelper.ParseToDecimal(txtPrice.Text);
            //No invoice
            if (decAmount <= 0)
            {
                return;
            }
            _objInvoice = new TBL_INVOICE()
            {
                INVOICE_NO = Method.GetNextSequence(sequence, true),
                CUSTOMER_ID = _objCustomer.CUSTOMER_ID,
                CURRENCY_ID = (int)cboCurrency.SelectedValue,
                CYCLE_ID = 0,
                PRINT_COUNT = 0,
                RUN_ID = 0,
                DISCOUNT_AMOUNT = 0,
                DISCOUNT_AMOUNT_NAME = string.Empty,
                DISCOUNT_USAGE = 0,
                DISCOUNT_USAGE_NAME = string.Empty,
                START_DATE = UIHelper._DefaultDate,
                START_USAGE = 0,
                END_DATE = UIHelper._DefaultDate,
                END_USAGE = 0,
                TOTAL_USAGE = 0,
                INVOICE_DATE = dtNow,
                DUE_DATE = dtNow.AddDays(intDueDay),
                INVOICE_MONTH = new DateTime(dtNow.Year, dtNow.Month, 1).AddMonths(1).AddDays(-1),
                INVOICE_TITLE = _objInvoiceItem.INVOICE_ITEM_NAME,
                IS_SERVICE_BILL = true,
                METER_CODE = _objMeter.METER_CODE,
                TOTAL_AMOUNT = decAmount,
                SETTLE_AMOUNT = UIHelper.RoundUp(decAmount, (int)cboCurrency.SelectedValue),
                PAID_AMOUNT = getTotalPay(),
                FORWARD_AMOUNT = 0,
                INVOICE_STATUS = (int)InvoiceStatus.Open,
                START_PAY_DATE = dtNow,
                //EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                //EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                BASED_PRICE = 0,
                CUSTOMER_CONNECTION_TYPE_ID = _objCustomer.CUSTOMER_CONNECTION_TYPE_ID,
                PRICE = 0,
                PRICE_ID = 0,
                ROW_DATE = dtNow
            };

            objlog = DBDataContext.Db.Insert(_objInvoice);

            _objInvoiceDetail = new TBL_INVOICE_DETAIL()
            {
                INVOICE_ID = _objInvoice.INVOICE_ID,
                INVOICE_ITEM_ID = _objInvoiceItem.INVOICE_ITEM_ID,
                PRICE = _objInvoiceItem.PRICE,
                START_USAGE = 0,
                END_USAGE = 0,
                USAGE = 0,
                AMOUNT = _objInvoice.TOTAL_AMOUNT,
                CHARGE_DESCRIPTION = _objInvoiceItem.INVOICE_ITEM_NAME,
                REF_NO = _objInvoice.INVOICE_NO,
                TRAN_DATE = _objInvoice.INVOICE_DATE,
                EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                TAX_AMOUNT = 0
            };


            DBDataContext.Db.Insert(_objInvoiceDetail);


            //No Payment
            if (getTotalPay() <= 0)
            {
                return;
            }
            _objPayment = new TBL_PAYMENT()
            {
                PAYMENT_NO = Method.GetNextSequence(Sequence.Receipt, true),
                CURRENCY_ID = _objInvoice.CURRENCY_ID,
                CUSTOMER_ID = _objCustomer.CUSTOMER_ID,
                CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                CREATE_ON = dtNow,
                IS_ACTIVE = true,
                PAY_AMOUNT = getTotalPay(),
                PAY_DATE = dtNow,
                USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID,
                DUE_AMOUNT = _objInvoice.SETTLE_AMOUNT,
                IS_VOID = false,
                PARENT_ID = 0,
                PAYMENT_ACCOUNT_ID = 72,
                BANK_PAYMENT_DETAIL_ID = 0,
                EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                PAYMENT_METHOD_ID = 0
            };

            DBDataContext.Db.InsertChild(_objPayment, _objInvoice, ref objlog);

            _objPaymentDetail = new TBL_PAYMENT_DETAIL()
            {
                INVOICE_ID = _objInvoice.INVOICE_ID,
                PAY_AMOUNT = getTotalPay(),
                PAYMENT_ID = _objPayment.PAYMENT_ID,
                DUE_AMOUNT = getTotalPay()
            };

            DBDataContext.Db.InsertChild(_objPaymentDetail, _objInvoice, ref objlog);

            //Close invoice
            if (getTotalPay() == _objInvoice.SETTLE_AMOUNT)
            {
                TBL_INVOICE objOld = new TBL_INVOICE();
                _objInvoice._CopyTo(objOld);
                _objInvoice.INVOICE_STATUS = (int)InvoiceStatus.Close;
                DBDataContext.Db.Update(objOld, _objInvoice);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {

        }

        private void txtCustomerCode_AdvanceSearch(object sender, EventArgs e)
        {
            if (txtCustomerCode.Text == string.Empty)
            {
                txtCustomerCode.CancelSearch(false);
                return;
            }
            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => !row.IS_POST_PAID && row.CUSTOMER_CODE == Method.FormatCustomerCode(txtCustomerCode.Text.Trim()));
            if (_objCustomer == null)
            {
                txtCustomerCode.CancelSearch(false);
                MsgBox.ShowInformation(Resources.MS_CUSTOMER_NOT_EXISTS);
                return;
            }

            _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.CUSTOMER_ID == _objCustomer.CUSTOMER_ID && row.IS_ACTIVE);
            if (_objCustomerMeter == null)
            {
                txtCustomerCode.CancelSearch(false);
            }
            else
            {
                _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(row => row.METER_ID == _objCustomerMeter.METER_ID);
                if (_objMeter == null)
                {
                    txtCustomerCode.CancelSearch(false);
                }
            }
            read();
        }

        private void txtCustomerName_AdvanceSearch(object sender, EventArgs e)
        {
            DialogCustomerSearch diag = new DialogCustomerSearch(txtCustomerName.Text, DialogCustomerSearch.PowerType.Prepaid);
            if (diag.ShowDialog() != DialogResult.OK)
            {
                txtCustomerName.CancelSearch(false);
                return;
            }
            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == diag.CustomerID);
            if (_objCustomer == null)
            {
                txtCustomerName.CancelSearch(false);
                return;
            }
            _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.CUSTOMER_ID == _objCustomer.CUSTOMER_ID && row.IS_ACTIVE);
            if (_objCustomerMeter == null)
            {
                txtCustomerName.CancelSearch(false);
            }
            else
            {
                _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(row => row.METER_ID == _objCustomerMeter.METER_ID);
                if (_objMeter == null)
                {
                    txtCustomerName.CancelSearch(false);
                }
            }
            read();
        }

        private void txtMeterCode_AdvanceSearch(object sender, EventArgs e)
        {
            // if not entry.
            if (txtMeterCode.Text.Trim() == string.Empty)
            {
                txtMeterCode.CancelSearch(false);
                return;
            }

            string strMeterCode = Method.FormatMeterCode(txtMeterCode.Text);
            _objMeter = DBDataContext.Db.TBL_METERs.FirstOrDefault(m => m.METER_CODE == strMeterCode);

            // if not contain in database.
            if (_objMeter == null)
            {
                MsgBox.ShowInformation(Resources.MS_METER_CODE_NOT_FOUND);
                txtMeterCode.CancelSearch(false);
                return;
            }

            // if meter is in use.
            if (_objMeter.STATUS_ID != (int)MeterStatus.Used)
            {
                MsgBox.ShowInformation(Resources.MS_METER_STATUS_IN_USE);
                txtMeterCode.CancelSearch(false);
                return;
            }

            _objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(row => row.IS_ACTIVE && row.METER_ID == _objMeter.METER_ID);

            _objCustomer = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == _objCustomerMeter.CUSTOMER_ID);

            read();

        }

        private void read()
        {
            // accpet entry.
            txtCustomerCode.AcceptSearch(false);
            txtCustomerName.AcceptSearch(false);
            txtMeterCode.AcceptSearch(false);

            // show customer information.
            txtCustomerName.Text = _objCustomer.LAST_NAME_KH + " " + _objCustomer.FIRST_NAME_KH;
            txtCustomerCode.Text = _objCustomer.CUSTOMER_CODE;

            if (_objMeter != null)
            {
                var cusInfo = (from c in DBDataContext.Db.TBL_CUSTOMERs
                               join pc in DBDataContext.Db.TBL_PREPAID_CUSTOMERs on c.CUSTOMER_ID equals pc.CUSTOMER_ID
                               join cb in DBDataContext.Db.TBL_CUSTOMER_BUYs on pc.PREPAID_CUS_ID equals cb.PREPAID_CUS_ID
                               where pc.CUSTOMER_ID == _objCustomer.CUSTOMER_ID
                                    //&& cb.IS_VOID == false
                               orderby cb.CREATE_ON descending
                               select new
                               {
                                   CUSTOMER = c,
                                   PREPAID_CUSTOMER = pc,
                                   CUSTOMER_LAST_BUY = cb ?? new TBL_CUSTOMER_BUY() { CREATE_ON = UIHelper._DefaultDate, IS_VOID = false }
                               }).FirstOrDefault();

                _objPrePaidCustomer = cusInfo.PREPAID_CUSTOMER;
                _objCusBuy = cusInfo.CUSTOMER_LAST_BUY.IS_VOID ? new TBL_CUSTOMER_BUY() { CREATE_ON = UIHelper._DefaultDate, IS_VOID = false } : cusInfo.CUSTOMER_LAST_BUY;
                if (_objCusBuy != null)
                {
                    txtLastPurchasePower.Text = _objCusBuy.BUY_QTY.ToString("N0");
                    dtpLastPurchaseDate.SetValue(_objCusBuy.CREATE_ON);
                }
                txtPriceType.Text = DBDataContext.Db.TBL_PRICEs.FirstOrDefault(x => x.PRICE_ID == _objCustomer.PRICE_ID && x.IS_ACTIVE).PRICE_NAME;
                txtCompensate.Text = _objPrePaidCustomer.COMPENSATED.ToString("N0");
                txtTotalPurchasePower.Text = _objPrePaidCustomer.TOTAL_BUY_POWER.ToString("N0");
                txtBuyTimes.Text = _objPrePaidCustomer.BUY_TIMES.ToString();
                txtMeterCode.Text = _objMeter.METER_CODE;
                txtAreaCode.Text = DBDataContext.Db.TBL_AREAs.FirstOrDefault(row => row.AREA_ID == _objCustomer.AREA_ID).AREA_CODE;
                txtPoleCode.Text = DBDataContext.Db.TBL_POLEs.FirstOrDefault(row => row.POLE_ID == _objCustomerMeter.POLE_ID).POLE_CODE;
                txtBoxCode.Text = DBDataContext.Db.TBL_BOXes.FirstOrDefault(row => row.BOX_ID == _objCustomerMeter.BOX_ID).BOX_CODE;
            }
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void txtCustomerCode_CancelAdvanceSearch(object sender, EventArgs e)
        {
            newRenewCard();
        }

        private void newRenewCard()
        {
            _objCusReadCard = null;
            _objCustomer = null;
            _objCustomerMeter = null;
            _objMeter = null;

            _objCusBuy = null;
            _objPrePaidCustomer = null;
            _objInvoice = null;
            _objInvoiceDetail = null;
            _objPayment = null;
            _objPaymentDetail = null;

            txtCustomerCode.CancelSearch(false);
            txtCustomerName.CancelSearch(false);
            txtMeterCode.CancelSearch(false);

            txtCustomerCode.Text =
            txtCustomerName.Text =
            txtMeterCode.Text =
            txtAreaCode.Text =
            txtPoleCode.Text =
            txtBoxCode.Text =
            txtPriceType.Text =
            txtCompensate.Text =
            txtTotalPurchasePower.Text =
            txtLastPurchasePower.Text =
            txtBuyTimes.Text = string.Empty;

            dtpLastPurchaseDate.ClearValue();

            bind();
            Refresh();
        }

        private void txtTextChangeCalcPayment(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedIndex == -1)
            {
                return;
            }

            decimal decPrice = 0,
                decPayment = 0,
                decDueAmount = 0;
            if (txtPrice.Text.Trim() == string.Empty)
            {
                txtPrice.Text = "0";
                txtPrice.SelectAll();
            }
            if (txtPayment.Text.Trim() == string.Empty)
            {
                txtPayment.Text = "0";
                txtPayment.SelectAll();
            }
            decPrice = DataHelper.ParseToDecimal(txtPrice.Text);
            decPayment = DataHelper.ParseToDecimal(UIHelper.FormatCurrency(DataHelper.ParseToDecimal(txtPayment.Text), (int)cboCurrency.SelectedValue));
            decDueAmount = decPrice - decPayment;
            txtDueAmount.Text = UIHelper.FormatCurrency(decDueAmount, (int)cboCurrency.SelectedValue);

            if (((TextBox)sender).Name.Contains(txtPayment.Name))
            {
                lblREAD_AMOUNT_.Text = decPayment <= 0 ? string.Empty : string.Format("{0} ({1})", DataHelper.NumberToWord(DataHelper.ParseToDecimal(txtPayment.Text)), Resources.RIEL);
            }
            else
            {
                lblREAD_AMOUNT_.Text = decDueAmount <= 0 ? string.Empty : string.Format("{0} ({1})", DataHelper.NumberToWord(DataHelper.ParseToDecimal(txtPrice.Text)), Resources.RIEL);
            }
        }

        private bool readvalidCard()
        {
            ICCard objReadCard = ReadCardInfo();
            if ((CardType)objReadCard.ICCardType != CardType.FormattedCard && (CardType)objReadCard.ICCardType != CardType.BlankCard)
            {
                if (MsgBox.ShowQuestion(Resources.MS_PLEASE_INSERT_NEW_CARD, "") != DialogResult.Yes)
                {
                    return false;
                }
                objReadCard = ReadCardInfo();
                if ((CardType)objReadCard.ICCardType != CardType.FormattedCard && (CardType)objReadCard.ICCardType != CardType.BlankCard)
                {
                    return readvalidCard();
                }
            }
            return true;
        }
        string _message = "";
        private void btnOK_Click(object sender, EventArgs e)
        {

            if (inValid())
            {
                return;
            }

            if (chkSERVICE_CHARGE_NEW_CARD.Checked)
            {
                write();
            }

            if (!readvalidCard())
            {
                return;
            }

            bool blnPrint = false;
            bool blnRemakeSuccess = false;


            Runner.RunNewThread(() =>
            {
                try
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        if (remakCard())
                        {
                            tran.Complete();
                            blnPrint = chkSERVICE_CHARGE_NEW_CARD.Checked && chkPRINT_RECEIPT.Checked;
                            blnRemakeSuccess = true;
                        }
                    }
                }
                catch (Exception exp)
                {
                    throw exp;
                }
            });



            //print
            if (blnPrint)
            {
                try
                {
                    SupportTaker objSt = new SupportTaker(SupportTaker.Process.PrintInvoice);
                    if (objSt.Check())
                    {
                        return;
                    }
                    DBDataContext.Db.TBL_INVOICE_TO_PRINTs.DeleteAllOnSubmit(DBDataContext.Db.TBL_INVOICE_TO_PRINTs);
                    DBDataContext.Db.TBL_INVOICE_TO_PRINTs.InsertOnSubmit(new TBL_INVOICE_TO_PRINT
                    {
                        INVOICE_ID = _objInvoice.INVOICE_ID,
                        PRINT_ORDER = 1
                    });
                    DBDataContext.Db.SubmitChanges();
                    CrystalReportHelper cr = Method.GetInvoiceReport();
                    cr.PrintReport(Settings.Default.PRINTER_INVOICE);
                    cr.Dispose();
                }
                catch (Exception exp)
                {
                    MsgBox.ShowError(exp);
                }
            }

            if (blnRemakeSuccess)
            {
                if (!string.IsNullOrEmpty(_message))
                {
                    MsgBox.ShowInformation(_message, Text);
                }
                DialogResult = DialogResult.OK;
            }
        }

        private bool remakCard()
        {
            _message = "";
            string strReturn = string.Empty;
            //format card
            int intReturn = new ICCard().FormatCard(out strReturn, Prepaid.GetAreaCode());

            if (intReturn != 0)
            {
                return false;
            }

            intReturn = 1;

            //renew card
            intReturn = new PowerCard().WriteNewCustomer(Prepaid.GetAreaCode(), _objCustomer.CUSTOMER_ID.ToString()
                        , Prepaid.GetMeterType(_objMeter), Prepaid.GetAlarmQTY()
                        , _objCustomer.ACTIVATE_DATE, out strReturn);
            if (intReturn != 0)
            {
                return false;
            }

            if (chkLAST_POWER_CARD.Checked)
            {
                if (_objTestCutCard.BuyTimes == _objPrePaidCustomer.BUY_TIMES)
                {
                    _message = Resources.MS_CUSTOMER_ALREADY_INSERT_POWER_CARD_TO_METER;
                    return true;
                    //MsgBox.ShowInformation();                    
                }
                else if (_objTestCutCard.BuyTimes == _objPrePaidCustomer.BUY_TIMES - 1)
                {
                    intReturn = new PowerCard().WritePurchaseCard(_objPrePaidCustomer.CARD_CODE
                              , _objCusBuy.BUY_TIMES, _objCusBuy.CREATE_ON
                              , _objCusBuy.BUY_QTY, out strReturn);
                    if (intReturn == 0)
                    {
                        _message = string.Format(Resources.MS_NEW_CUSTOMER_CARD_HAVE_POWER, _objCusBuy.BUY_QTY);
                        //MsgBox.ShowInformation();
                    }
                }
                else
                {
                    //intReturn = new PowerCard().WritePurchaseCard(_objPrePaidCustomer.CARD_CODE
                    //          , _objCusBuy.BUY_TIMES, _objCusBuy.CREATE_ON
                    //          , _objCusBuy.BUY_QTY, out strReturn);
                    //if (intReturn==0)
                    //{
                    _message = Resources.MS_INVALID_BUY_TIME;
                    return true;
                    //MsgBox.ShowInformation();
                    //}                    
                }

                //fail to write last purchase power
                if (intReturn != 0)
                {
                    _message = Resources.IC_CARD_MAKE_FAIL;
                    //MsgBox.ShowInformation();
                    return false;
                }
                PowerCard objValPower = (PowerCard)ICCard.Read();
                if (objValPower.PurchasePower != _objCusBuy.BUY_QTY)
                {
                    _message = Resources.MS_WRITE_POWER_CARD_FAIL;
                    //MsgBox.ShowInformation(,Text);
                    return false;
                }
            }
            ICHelper.BeepSound(out strReturn);
            return true;
        }

        private ICCard ReadCardInfo()
        {
            ICCard objReadCard = ICCard.Read();
            _objCusReadCard = null;
            switch ((CardType)objReadCard.ICCardType)
            {
                case CardType.NoCard:
                    lblCARD_TYPE_.Text = Resources.MS_PLEASE_INSERT_NEW_CARD;
                    break;
                case CardType.BlankCard:
                case CardType.FormattedCard:
                    lblCARD_TYPE_.Text = Resources.NEW_CARD;
                    break;
                case CardType.HavePowerCard:
                case CardType.NoPowerCard:
                    lblCARD_TYPE_.Text = Resources.MS_CARD_INUSE;
                    string strCusCode = ((PowerCard)objReadCard).CustomerCode;
                    _objCusReadCard = (from c in DBDataContext.Db.TBL_CUSTOMERs
                                       join pc in DBDataContext.Db.TBL_PREPAID_CUSTOMERs on c.CUSTOMER_ID equals pc.CUSTOMER_ID
                                       where pc.CARD_CODE.ToLower() == strCusCode.ToLower()
                                       select c).FirstOrDefault();
                    break;
                case CardType.ResetCard:
                case CardType.PresetCard:
                case CardType.TestCard:
                case CardType.CutCard:
                    lblCARD_TYPE_.Text = Resources.MS_CARD_CANNOT_USE;
                    break;
                default:
                    break;
            }

            return objReadCard;
        }


        private bool inValid()
        {
            bool blnVal = false;
            this.ClearAllValidation();
            if (_objCustomer == null)
            {
                txtCustomerCode.SetValidation(string.Format(Resources.REQUIRED, lblCUSTOMER_CODE.Text));
                return true;
            }

            if (string.IsNullOrEmpty(txtMeterCode.Text.Trim()))
            {
                txtMeterCode.SetValidation(string.Format(Resources.REQUIRED, lblMETER_CODE.Text));
                return true;
            }

            if (chkSERVICE_CHARGE_NEW_CARD.Checked)
            {
                if (!DataHelper.IsNumber(txtPrice.Text))
                {
                    txtPrice.SetValidation(string.Format(Resources.REQUIRED, lblUNIT_PRICE.Text));
                    blnVal = true;
                }

                if (!DataHelper.IsNumber(txtPayment.Text))
                {
                    txtPayment.SetValidation(string.Format(Resources.REQUIRED, lblSETTLE_AMOUNT.Text));
                    blnVal = true;
                }

                if (getTotalBalance() < 0)
                {
                    txtPayment.SetValidation(Resources.MS_PAYMENT_OVER_AMOUNT);
                    blnVal = true;
                }
                if (getTotalBalance() > 0)
                {
                    txtPayment.SetValidation(Resources.MS_PAID_FULL_AMOUNT);
                    blnVal = true;
                }
                if (cboCurrency.SelectedIndex == -1)
                {
                    cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                    blnVal = true;
                }
            }

            return blnVal;
        }

        private decimal getTotalBalance()
        {

            decimal decPrice = 0, decPayment = 0;

            decPrice = UIHelper.RoundUp(DataHelper.ParseToDecimal(txtPrice.Text), (int)cboCurrency.SelectedValue);
            decPayment = UIHelper.RoundUp(DataHelper.ParseToDecimal(txtPayment.Text), (int)cboCurrency.SelectedValue);

            return decPrice - decPayment;
        }

        public decimal getTotalPay()
        {
            return UIHelper.RoundUp(DataHelper.ParseToDecimal(txtPayment.Text), (int)cboCurrency.SelectedValue);
        }

        private void checkAddCharge_CheckedChanged(object sender, EventArgs e)
        {
            dtpRenewDate.Enabled =
                cboCurrency.Enabled =
            txtCashDrawerName.Enabled =
            txtUserCashDrawerName.Enabled =
            txtPayment.Enabled =
            txtPrice.Enabled =
            txtDueAmount.Enabled =
            chkPRINT_RECEIPT.Enabled = chkSERVICE_CHARGE_NEW_CARD.Checked;
        }

        private void btnReadCard_Click(object sender, EventArgs e)
        {
            if (chkLAST_POWER_CARD.Checked)
            {
                ReadTestCard();
            }
            else
            {
                ReadCardInfo();
            }
        }

        private ICCard ReadTestCard()
        {
            ICCard objReadCard = ICCard.Read();
            _objCusReadCard = null;
            _objTestCutCard = null;
            switch ((CardType)objReadCard.ICCardType)
            {
                case CardType.NoCard:
                    lblCARD_TYPE_.Text = Resources.MS_PLEASE_INSERT_NEW_CARD;
                    break;
                case CardType.TestCard:
                    _objTestCutCard = (TestCutCard)objReadCard;
                    txtCustomerCode.Text = Prepaid.GetCustomerInfo(_objTestCutCard.CustomerCode).CUSTOMER_CODE;
                    txtCustomerCode_AdvanceSearch(null, null);
                    break;
                case CardType.BlankCard:
                case CardType.FormattedCard:
                case CardType.HavePowerCard:
                case CardType.NoPowerCard:
                case CardType.ResetCard:
                case CardType.PresetCard:
                case CardType.CutCard:
                    lblCARD_TYPE_.Text = Resources.MS_PLEASE_INSERT_QUERY_CARD;
                    break;
                default:
                    break;
            }

            return objReadCard;
        }

        private void lblCardDetailInfo_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (_objCusReadCard == null)
            {
                return;
            }

            DialogCustomer dig = new DialogCustomer(GeneralProcess.Update, _objCusReadCard);
            dig.ShowDialog();
        }

        private void chkNewHavePower_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLAST_POWER_CARD.Checked)
            {
                lblCARD_TYPE_.Text = Resources.MS_PLEASE_INSERT_QUERY_CARD;
                txtCustomerCode.Enabled =
                txtMeterCode.Enabled =
                txtCustomerName.Enabled = false;
            }
            else
            {
                lblCARD_TYPE_.Text = string.Empty;
                txtCustomerCode.Enabled =
                txtMeterCode.Enabled =
                txtCustomerName.Enabled = true;
                txtCustomerCode_CancelAdvanceSearch(null, null);
            }
        }
    }
}