﻿namespace EPower.Interface.PrePaid
{
    partial class DialogChangeConnectionType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.btnClose = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.cboNewConnectionType = new System.Windows.Forms.ComboBox();
            this.cboNewCustomerGroup = new System.Windows.Forms.ComboBox();
            this.txtOldCusConn = new System.Windows.Forms.TextBox();
            this.txtOldCusGroup = new System.Windows.Forms.TextBox();
            this.txtCusName = new System.Windows.Forms.TextBox();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.lblNEW_CUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.Label();
            this.lblNEW_CUSTOMER_GROUP_TYPE = new System.Windows.Forms.Label();
            this.lblCHANGE_DATE = new System.Windows.Forms.Label();
            this.lblOLD_CONNECTION_TYPE = new System.Windows.Forms.Label();
            this.lblOLD_CUSTOMER_GROUP_TYPE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label28);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.btnClose);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.txtNote);
            this.content.Controls.Add(this.txtOldCusGroup);
            this.content.Controls.Add(this.lblNOTE);
            this.content.Controls.Add(this.cboNewConnectionType);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.cboNewCustomerGroup);
            this.content.Controls.Add(this.lblOLD_CUSTOMER_GROUP_TYPE);
            this.content.Controls.Add(this.txtOldCusConn);
            this.content.Controls.Add(this.lblOLD_CONNECTION_TYPE);
            this.content.Controls.Add(this.lblCHANGE_DATE);
            this.content.Controls.Add(this.txtCusName);
            this.content.Controls.Add(this.lblNEW_CUSTOMER_GROUP_TYPE);
            this.content.Controls.Add(this.lblNEW_CUSTOMER_CONNECTION_TYPE);
            this.content.Controls.Add(this.dateTimePicker);
            this.content.Size = new System.Drawing.Size(457, 417);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.dateTimePicker, 0);
            this.content.Controls.SetChildIndex(this.lblNEW_CUSTOMER_CONNECTION_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblNEW_CUSTOMER_GROUP_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtCusName, 0);
            this.content.Controls.SetChildIndex(this.lblCHANGE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblOLD_CONNECTION_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtOldCusConn, 0);
            this.content.Controls.SetChildIndex(this.lblOLD_CUSTOMER_GROUP_TYPE, 0);
            this.content.Controls.SetChildIndex(this.cboNewCustomerGroup, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.cboNewConnectionType, 0);
            this.content.Controls.SetChildIndex(this.lblNOTE, 0);
            this.content.Controls.SetChildIndex(this.txtOldCusGroup, 0);
            this.content.Controls.SetChildIndex(this.txtNote, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnClose, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.label28, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            // 
            // btnCHANGE_LOG
            // 
            this.btnCHANGE_LOG.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCHANGE_LOG.Location = new System.Drawing.Point(16, 379);
            this.btnCHANGE_LOG.Margin = new System.Windows.Forms.Padding(3, 28, 3, 28);
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.Size = new System.Drawing.Size(78, 23);
            this.btnCHANGE_LOG.TabIndex = 385;
            this.btnCHANGE_LOG.Text = "ប្រវត្តិកែប្រែ";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnCHANGE_LOG_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnClose.Location = new System.Drawing.Point(365, 379);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 384;
            this.btnClose.Text = "បិទ";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnOK.Location = new System.Drawing.Point(269, 379);
            this.btnOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(85, 23);
            this.btnOK.TabIndex = 383;
            this.btnOK.Text = "យល់ព្រម";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(137, 223);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNote.Size = new System.Drawing.Size(305, 120);
            this.txtNote.TabIndex = 382;
            // 
            // lblNOTE
            // 
            this.lblNOTE.AutoSize = true;
            this.lblNOTE.Location = new System.Drawing.Point(18, 226);
            this.lblNOTE.Name = "lblNOTE";
            this.lblNOTE.Size = new System.Drawing.Size(45, 19);
            this.lblNOTE.TabIndex = 381;
            this.lblNOTE.Text = "សំគាល់";
            // 
            // cboNewConnectionType
            // 
            this.cboNewConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNewConnectionType.FormattingEnabled = true;
            this.cboNewConnectionType.Location = new System.Drawing.Point(137, 189);
            this.cboNewConnectionType.Name = "cboNewConnectionType";
            this.cboNewConnectionType.Size = new System.Drawing.Size(305, 27);
            this.cboNewConnectionType.TabIndex = 380;
            // 
            // cboNewCustomerGroup
            // 
            this.cboNewCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNewCustomerGroup.FormattingEnabled = true;
            this.cboNewCustomerGroup.Location = new System.Drawing.Point(137, 154);
            this.cboNewCustomerGroup.Name = "cboNewCustomerGroup";
            this.cboNewCustomerGroup.Size = new System.Drawing.Size(305, 27);
            this.cboNewCustomerGroup.TabIndex = 379;
            this.cboNewCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboNewCustomerGroup_SelectedIndexChanged);
            // 
            // txtOldCusConn
            // 
            this.txtOldCusConn.BackColor = System.Drawing.SystemColors.Control;
            this.txtOldCusConn.Enabled = false;
            this.txtOldCusConn.Location = new System.Drawing.Point(137, 87);
            this.txtOldCusConn.Name = "txtOldCusConn";
            this.txtOldCusConn.Size = new System.Drawing.Size(305, 27);
            this.txtOldCusConn.TabIndex = 378;
            // 
            // txtOldCusGroup
            // 
            this.txtOldCusGroup.BackColor = System.Drawing.SystemColors.Control;
            this.txtOldCusGroup.Enabled = false;
            this.txtOldCusGroup.Location = new System.Drawing.Point(137, 54);
            this.txtOldCusGroup.Name = "txtOldCusGroup";
            this.txtOldCusGroup.Size = new System.Drawing.Size(305, 27);
            this.txtOldCusGroup.TabIndex = 377;
            // 
            // txtCusName
            // 
            this.txtCusName.BackColor = System.Drawing.SystemColors.Control;
            this.txtCusName.Enabled = false;
            this.txtCusName.Location = new System.Drawing.Point(137, 22);
            this.txtCusName.Name = "txtCusName";
            this.txtCusName.Size = new System.Drawing.Size(305, 27);
            this.txtCusName.TabIndex = 376;
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.dateTimePicker.Enabled = false;
            this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker.Location = new System.Drawing.Point(137, 121);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(305, 27);
            this.dateTimePicker.TabIndex = 374;
            // 
            // lblNEW_CUSTOMER_CONNECTION_TYPE
            // 
            this.lblNEW_CUSTOMER_CONNECTION_TYPE.AutoSize = true;
            this.lblNEW_CUSTOMER_CONNECTION_TYPE.Location = new System.Drawing.Point(18, 192);
            this.lblNEW_CUSTOMER_CONNECTION_TYPE.Name = "lblNEW_CUSTOMER_CONNECTION_TYPE";
            this.lblNEW_CUSTOMER_CONNECTION_TYPE.Size = new System.Drawing.Size(73, 19);
            this.lblNEW_CUSTOMER_CONNECTION_TYPE.TabIndex = 373;
            this.lblNEW_CUSTOMER_CONNECTION_TYPE.Text = "ការភ្ជាប់ចរន្តថ្មី";
            // 
            // lblNEW_CUSTOMER_GROUP_TYPE
            // 
            this.lblNEW_CUSTOMER_GROUP_TYPE.AutoSize = true;
            this.lblNEW_CUSTOMER_GROUP_TYPE.Location = new System.Drawing.Point(18, 157);
            this.lblNEW_CUSTOMER_GROUP_TYPE.Name = "lblNEW_CUSTOMER_GROUP_TYPE";
            this.lblNEW_CUSTOMER_GROUP_TYPE.Size = new System.Drawing.Size(89, 19);
            this.lblNEW_CUSTOMER_GROUP_TYPE.TabIndex = 372;
            this.lblNEW_CUSTOMER_GROUP_TYPE.Text = "ប្រភេទអតិថិជនថ្មី";
            // 
            // lblCHANGE_DATE
            // 
            this.lblCHANGE_DATE.AutoSize = true;
            this.lblCHANGE_DATE.Location = new System.Drawing.Point(18, 127);
            this.lblCHANGE_DATE.Name = "lblCHANGE_DATE";
            this.lblCHANGE_DATE.Size = new System.Drawing.Size(45, 19);
            this.lblCHANGE_DATE.TabIndex = 371;
            this.lblCHANGE_DATE.Text = "ថ្ងៃខែប្តូរ";
            // 
            // lblOLD_CONNECTION_TYPE
            // 
            this.lblOLD_CONNECTION_TYPE.AutoSize = true;
            this.lblOLD_CONNECTION_TYPE.Location = new System.Drawing.Point(18, 90);
            this.lblOLD_CONNECTION_TYPE.Name = "lblOLD_CONNECTION_TYPE";
            this.lblOLD_CONNECTION_TYPE.Size = new System.Drawing.Size(89, 19);
            this.lblOLD_CONNECTION_TYPE.TabIndex = 370;
            this.lblOLD_CONNECTION_TYPE.Text = "ការភ្ជាប់ចរន្តចាស់";
            // 
            // lblOLD_CUSTOMER_GROUP_TYPE
            // 
            this.lblOLD_CUSTOMER_GROUP_TYPE.AutoSize = true;
            this.lblOLD_CUSTOMER_GROUP_TYPE.Location = new System.Drawing.Point(18, 57);
            this.lblOLD_CUSTOMER_GROUP_TYPE.Name = "lblOLD_CUSTOMER_GROUP_TYPE";
            this.lblOLD_CUSTOMER_GROUP_TYPE.Size = new System.Drawing.Size(105, 19);
            this.lblOLD_CUSTOMER_GROUP_TYPE.TabIndex = 369;
            this.lblOLD_CUSTOMER_GROUP_TYPE.Text = "ប្រភេទអតិថិជនចាស់";
            // 
            // lblCUSTOMER_NAME
            // 
            this.lblCUSTOMER_NAME.AutoSize = true;
            this.lblCUSTOMER_NAME.Location = new System.Drawing.Point(18, 25);
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            this.lblCUSTOMER_NAME.Size = new System.Drawing.Size(80, 19);
            this.lblCUSTOMER_NAME.TabIndex = 368;
            this.lblCUSTOMER_NAME.Text = "ឈ្មោះអតិថិជន";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label28.Location = new System.Drawing.Point(115, 155);
            this.label28.Margin = new System.Windows.Forms.Padding(2);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(15, 20);
            this.label28.TabIndex = 387;
            this.label28.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(115, 192);
            this.label1.Margin = new System.Windows.Forms.Padding(2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 20);
            this.label1.TabIndex = 388;
            this.label1.Text = "*";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Location = new System.Drawing.Point(1, 368);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(456, 1);
            this.panel1.TabIndex = 389;
            // 
            // DialogChangeConnectionType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 440);
            this.Name = "DialogChangeConnectionType";
            this.Text = "ប្តូរប្រភេទភ្ជាប់ចរន្ត";
            this.Load += new System.EventHandler(this.DialogChangeConnectionType_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private SoftTech.Component.ExButton btnCHANGE_LOG;
        private SoftTech.Component.ExButton btnClose;
        private SoftTech.Component.ExButton btnOK;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label lblNOTE;
        private System.Windows.Forms.ComboBox cboNewConnectionType;
        private System.Windows.Forms.ComboBox cboNewCustomerGroup;
        private System.Windows.Forms.TextBox txtOldCusConn;
        private System.Windows.Forms.TextBox txtOldCusGroup;
        private System.Windows.Forms.TextBox txtCusName;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.Label lblNEW_CUSTOMER_CONNECTION_TYPE;
        private System.Windows.Forms.Label lblNEW_CUSTOMER_GROUP_TYPE;
        private System.Windows.Forms.Label lblCHANGE_DATE;
        private System.Windows.Forms.Label lblOLD_CONNECTION_TYPE;
        private System.Windows.Forms.Label lblOLD_CUSTOMER_GROUP_TYPE;
        private System.Windows.Forms.Label lblCUSTOMER_NAME;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
    }
}