﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class DialogCustomerActivatePrepaid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerActivatePrepaid));
            this.lblPRICE = new System.Windows.Forms.Label();
            this.lblGENERAL_INFORMATION = new System.Windows.Forms.Label();
            this.lblCUSTOMER_TYPE = new System.Windows.Forms.Label();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.lblPRICE_TYPE = new System.Windows.Forms.Label();
            this.lblMETER_INSTALLATION = new System.Windows.Forms.Label();
            this.lblMETER_TYPE = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblBREAKER_TYPE = new System.Windows.Forms.Label();
            this.lblBREAKER_CODE = new System.Windows.Forms.Label();
            this.lblSHIELD = new System.Windows.Forms.Label();
            this.lblCABLE_SHIELD = new System.Windows.Forms.Label();
            this.txtMeter = new SoftTech.Component.ExTextbox();
            this.txtBreaker = new SoftTech.Component.ExTextbox();
            this.cboMeterShield = new System.Windows.Forms.ComboBox();
            this.cboCableShield = new System.Windows.Forms.ComboBox();
            this.lblLAST_NAME_KH = new System.Windows.Forms.Label();
            this.txtLastNamekh = new System.Windows.Forms.TextBox();
            this.lblBOX = new System.Windows.Forms.Label();
            this.txtBox = new SoftTech.Component.ExTextbox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtMeterType = new System.Windows.Forms.TextBox();
            this.txtBreakerType = new System.Windows.Forms.TextBox();
            this.txtPoleCode = new System.Windows.Forms.TextBox();
            this.txtCustomerType = new System.Windows.Forms.TextBox();
            this.lblSTARTUP = new System.Windows.Forms.Label();
            this.lblREAD_POWER_ = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.chkPRINT_RECEIPT = new System.Windows.Forms.CheckBox();
            this.txtCashDrawerName = new System.Windows.Forms.TextBox();
            this.txtUserCashDrawerName = new System.Windows.Forms.TextBox();
            this.lblCASHIER = new System.Windows.Forms.Label();
            this.lblCASH_DRAWER = new System.Windows.Forms.Label();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.lblSETTLE_AMOUNT = new System.Windows.Forms.Label();
            this.lblDISCOUNT = new System.Windows.Forms.Label();
            this.txtUnitPrice = new System.Windows.Forms.TextBox();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.txtBuyPower = new System.Windows.Forms.TextBox();
            this.lblPRICE_1 = new System.Windows.Forms.Label();
            this.lblBUY_POWER = new System.Windows.Forms.Label();
            this.lblDATE = new System.Windows.Forms.Label();
            this.dtpActivateDate = new System.Windows.Forms.DateTimePicker();
            this.lblREAD_AMOUNT_ = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblSHOW_DETAIL = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblCARD_TYPE_ = new System.Windows.Forms.Label();
            this.btnREAD_CARD = new SoftTech.Component.ExButton();
            this.txtPriceName = new System.Windows.Forms.TextBox();
            this.txtSignUnitPrice = new System.Windows.Forms.TextBox();
            this.txtSignTotalAmount = new System.Windows.Forms.TextBox();
            this.txtSignPrice = new System.Windows.Forms.TextBox();
            this.txtSTART_USAGE = new System.Windows.Forms.TextBox();
            this.lblSTART_USAGE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtSTART_USAGE);
            this.content.Controls.Add(this.lblSTART_USAGE);
            this.content.Controls.Add(this.txtSignTotalAmount);
            this.content.Controls.Add(this.txtSignPrice);
            this.content.Controls.Add(this.txtSignUnitPrice);
            this.content.Controls.Add(this.txtPriceName);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.lblREAD_AMOUNT_);
            this.content.Controls.Add(this.dtpActivateDate);
            this.content.Controls.Add(this.chkPRINT_RECEIPT);
            this.content.Controls.Add(this.txtCashDrawerName);
            this.content.Controls.Add(this.txtUserCashDrawerName);
            this.content.Controls.Add(this.lblCASHIER);
            this.content.Controls.Add(this.lblCASH_DRAWER);
            this.content.Controls.Add(this.txtTotalAmount);
            this.content.Controls.Add(this.lblSETTLE_AMOUNT);
            this.content.Controls.Add(this.lblDISCOUNT);
            this.content.Controls.Add(this.txtUnitPrice);
            this.content.Controls.Add(this.txtDiscount);
            this.content.Controls.Add(this.txtBuyPower);
            this.content.Controls.Add(this.lblPRICE_1);
            this.content.Controls.Add(this.lblBUY_POWER);
            this.content.Controls.Add(this.lblDATE);
            this.content.Controls.Add(this.txtPrice);
            this.content.Controls.Add(this.lblREAD_POWER_);
            this.content.Controls.Add(this.lblSTARTUP);
            this.content.Controls.Add(this.txtCustomerType);
            this.content.Controls.Add(this.txtPoleCode);
            this.content.Controls.Add(this.txtBreakerType);
            this.content.Controls.Add(this.txtMeterType);
            this.content.Controls.Add(this.txtBox);
            this.content.Controls.Add(this.lblBOX);
            this.content.Controls.Add(this.txtLastNamekh);
            this.content.Controls.Add(this.lblLAST_NAME_KH);
            this.content.Controls.Add(this.lblCABLE_SHIELD);
            this.content.Controls.Add(this.lblSHIELD);
            this.content.Controls.Add(this.lblBREAKER_CODE);
            this.content.Controls.Add(this.txtBreaker);
            this.content.Controls.Add(this.lblBREAKER_TYPE);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.lblPRICE);
            this.content.Controls.Add(this.lblGENERAL_INFORMATION);
            this.content.Controls.Add(this.lblCUSTOMER_TYPE);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.lblPOLE);
            this.content.Controls.Add(this.lblPRICE_TYPE);
            this.content.Controls.Add(this.lblMETER_INSTALLATION);
            this.content.Controls.Add(this.lblMETER_TYPE);
            this.content.Controls.Add(this.cboCableShield);
            this.content.Controls.Add(this.cboMeterShield);
            this.content.Controls.Add(this.txtMeter);
            this.content.Controls.Add(this.label29);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.label14);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.label29, 0);
            this.content.Controls.SetChildIndex(this.txtMeter, 0);
            this.content.Controls.SetChildIndex(this.cboMeterShield, 0);
            this.content.Controls.SetChildIndex(this.cboCableShield, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_INSTALLATION, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblGENERAL_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtBreaker, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblSHIELD, 0);
            this.content.Controls.SetChildIndex(this.lblCABLE_SHIELD, 0);
            this.content.Controls.SetChildIndex(this.lblLAST_NAME_KH, 0);
            this.content.Controls.SetChildIndex(this.txtLastNamekh, 0);
            this.content.Controls.SetChildIndex(this.lblBOX, 0);
            this.content.Controls.SetChildIndex(this.txtBox, 0);
            this.content.Controls.SetChildIndex(this.txtMeterType, 0);
            this.content.Controls.SetChildIndex(this.txtBreakerType, 0);
            this.content.Controls.SetChildIndex(this.txtPoleCode, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerType, 0);
            this.content.Controls.SetChildIndex(this.lblSTARTUP, 0);
            this.content.Controls.SetChildIndex(this.lblREAD_POWER_, 0);
            this.content.Controls.SetChildIndex(this.txtPrice, 0);
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.lblBUY_POWER, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE_1, 0);
            this.content.Controls.SetChildIndex(this.txtBuyPower, 0);
            this.content.Controls.SetChildIndex(this.txtDiscount, 0);
            this.content.Controls.SetChildIndex(this.txtUnitPrice, 0);
            this.content.Controls.SetChildIndex(this.lblDISCOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblSETTLE_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtTotalAmount, 0);
            this.content.Controls.SetChildIndex(this.lblCASH_DRAWER, 0);
            this.content.Controls.SetChildIndex(this.lblCASHIER, 0);
            this.content.Controls.SetChildIndex(this.txtUserCashDrawerName, 0);
            this.content.Controls.SetChildIndex(this.txtCashDrawerName, 0);
            this.content.Controls.SetChildIndex(this.chkPRINT_RECEIPT, 0);
            this.content.Controls.SetChildIndex(this.dtpActivateDate, 0);
            this.content.Controls.SetChildIndex(this.lblREAD_AMOUNT_, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.txtPriceName, 0);
            this.content.Controls.SetChildIndex(this.txtSignUnitPrice, 0);
            this.content.Controls.SetChildIndex(this.txtSignPrice, 0);
            this.content.Controls.SetChildIndex(this.txtSignTotalAmount, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtSTART_USAGE, 0);
            // 
            // lblPRICE
            // 
            resources.ApplyResources(this.lblPRICE, "lblPRICE");
            this.lblPRICE.Name = "lblPRICE";
            // 
            // lblGENERAL_INFORMATION
            // 
            resources.ApplyResources(this.lblGENERAL_INFORMATION, "lblGENERAL_INFORMATION");
            this.lblGENERAL_INFORMATION.Name = "lblGENERAL_INFORMATION";
            // 
            // lblCUSTOMER_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_TYPE, "lblCUSTOMER_TYPE");
            this.lblCUSTOMER_TYPE.Name = "lblCUSTOMER_TYPE";
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // lblPOLE
            // 
            resources.ApplyResources(this.lblPOLE, "lblPOLE");
            this.lblPOLE.Name = "lblPOLE";
            // 
            // lblPRICE_TYPE
            // 
            resources.ApplyResources(this.lblPRICE_TYPE, "lblPRICE_TYPE");
            this.lblPRICE_TYPE.Name = "lblPRICE_TYPE";
            // 
            // lblMETER_INSTALLATION
            // 
            resources.ApplyResources(this.lblMETER_INSTALLATION, "lblMETER_INSTALLATION");
            this.lblMETER_INSTALLATION.Name = "lblMETER_INSTALLATION";
            // 
            // lblMETER_TYPE
            // 
            resources.ApplyResources(this.lblMETER_TYPE, "lblMETER_TYPE");
            this.lblMETER_TYPE.Name = "lblMETER_TYPE";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblBREAKER_TYPE
            // 
            resources.ApplyResources(this.lblBREAKER_TYPE, "lblBREAKER_TYPE");
            this.lblBREAKER_TYPE.Name = "lblBREAKER_TYPE";
            // 
            // lblBREAKER_CODE
            // 
            resources.ApplyResources(this.lblBREAKER_CODE, "lblBREAKER_CODE");
            this.lblBREAKER_CODE.Name = "lblBREAKER_CODE";
            // 
            // lblSHIELD
            // 
            resources.ApplyResources(this.lblSHIELD, "lblSHIELD");
            this.lblSHIELD.Name = "lblSHIELD";
            // 
            // lblCABLE_SHIELD
            // 
            resources.ApplyResources(this.lblCABLE_SHIELD, "lblCABLE_SHIELD");
            this.lblCABLE_SHIELD.Name = "lblCABLE_SHIELD";
            // 
            // txtMeter
            // 
            this.txtMeter.BackColor = System.Drawing.Color.White;
            this.txtMeter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtMeter, "txtMeter");
            this.txtMeter.Name = "txtMeter";
            this.txtMeter.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtMeter.AdvanceSearch += new System.EventHandler(this.txtMeter_AdvanceSearch);
            this.txtMeter.CancelAdvanceSearch += new System.EventHandler(this.txtMeter_CancelAdvanceSearch);
            this.txtMeter.Enter += new System.EventHandler(this.txtMeter_Enter);
            // 
            // txtBreaker
            // 
            this.txtBreaker.BackColor = System.Drawing.Color.White;
            this.txtBreaker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtBreaker, "txtBreaker");
            this.txtBreaker.Name = "txtBreaker";
            this.txtBreaker.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtBreaker.AdvanceSearch += new System.EventHandler(this.txtBreaker_AdvanceSearch);
            this.txtBreaker.CancelAdvanceSearch += new System.EventHandler(this.txtBreaker_CancelAdvanceSearch);
            this.txtBreaker.Enter += new System.EventHandler(this.txtMeter_Enter);
            // 
            // cboMeterShield
            // 
            this.cboMeterShield.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMeterShield.FormattingEnabled = true;
            resources.ApplyResources(this.cboMeterShield, "cboMeterShield");
            this.cboMeterShield.Name = "cboMeterShield";
            // 
            // cboCableShield
            // 
            this.cboCableShield.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCableShield.FormattingEnabled = true;
            resources.ApplyResources(this.cboCableShield, "cboCableShield");
            this.cboCableShield.Name = "cboCableShield";
            // 
            // lblLAST_NAME_KH
            // 
            resources.ApplyResources(this.lblLAST_NAME_KH, "lblLAST_NAME_KH");
            this.lblLAST_NAME_KH.Name = "lblLAST_NAME_KH";
            // 
            // txtLastNamekh
            // 
            resources.ApplyResources(this.txtLastNamekh, "txtLastNamekh");
            this.txtLastNamekh.Name = "txtLastNamekh";
            this.txtLastNamekh.ReadOnly = true;
            this.txtLastNamekh.TabStop = false;
            // 
            // lblBOX
            // 
            resources.ApplyResources(this.lblBOX, "lblBOX");
            this.lblBOX.Name = "lblBOX";
            // 
            // txtBox
            // 
            this.txtBox.BackColor = System.Drawing.Color.White;
            this.txtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtBox, "txtBox");
            this.txtBox.Name = "txtBox";
            this.txtBox.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtBox.AdvanceSearch += new System.EventHandler(this.txtBox_AdvanceSearch);
            this.txtBox.CancelAdvanceSearch += new System.EventHandler(this.txtBox_CancelAdvanceSearch);
            this.txtBox.Enter += new System.EventHandler(this.txtMeter_Enter);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // label29
            // 
            resources.ApplyResources(this.label29, "label29");
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Name = "label29";
            // 
            // txtMeterType
            // 
            resources.ApplyResources(this.txtMeterType, "txtMeterType");
            this.txtMeterType.Name = "txtMeterType";
            this.txtMeterType.ReadOnly = true;
            this.txtMeterType.TabStop = false;
            // 
            // txtBreakerType
            // 
            resources.ApplyResources(this.txtBreakerType, "txtBreakerType");
            this.txtBreakerType.Name = "txtBreakerType";
            this.txtBreakerType.ReadOnly = true;
            this.txtBreakerType.TabStop = false;
            // 
            // txtPoleCode
            // 
            resources.ApplyResources(this.txtPoleCode, "txtPoleCode");
            this.txtPoleCode.Name = "txtPoleCode";
            this.txtPoleCode.ReadOnly = true;
            this.txtPoleCode.TabStop = false;
            // 
            // txtCustomerType
            // 
            resources.ApplyResources(this.txtCustomerType, "txtCustomerType");
            this.txtCustomerType.Name = "txtCustomerType";
            this.txtCustomerType.ReadOnly = true;
            this.txtCustomerType.TabStop = false;
            // 
            // lblSTARTUP
            // 
            resources.ApplyResources(this.lblSTARTUP, "lblSTARTUP");
            this.lblSTARTUP.Name = "lblSTARTUP";
            // 
            // lblREAD_POWER_
            // 
            resources.ApplyResources(this.lblREAD_POWER_, "lblREAD_POWER_");
            this.lblREAD_POWER_.Name = "lblREAD_POWER_";
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.ReadOnly = true;
            this.txtPrice.TabStop = false;
            // 
            // chkPRINT_RECEIPT
            // 
            resources.ApplyResources(this.chkPRINT_RECEIPT, "chkPRINT_RECEIPT");
            this.chkPRINT_RECEIPT.Checked = true;
            this.chkPRINT_RECEIPT.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPRINT_RECEIPT.Name = "chkPRINT_RECEIPT";
            this.chkPRINT_RECEIPT.UseVisualStyleBackColor = true;
            // 
            // txtCashDrawerName
            // 
            resources.ApplyResources(this.txtCashDrawerName, "txtCashDrawerName");
            this.txtCashDrawerName.Name = "txtCashDrawerName";
            this.txtCashDrawerName.ReadOnly = true;
            // 
            // txtUserCashDrawerName
            // 
            resources.ApplyResources(this.txtUserCashDrawerName, "txtUserCashDrawerName");
            this.txtUserCashDrawerName.Name = "txtUserCashDrawerName";
            this.txtUserCashDrawerName.ReadOnly = true;
            // 
            // lblCASHIER
            // 
            resources.ApplyResources(this.lblCASHIER, "lblCASHIER");
            this.lblCASHIER.Name = "lblCASHIER";
            // 
            // lblCASH_DRAWER
            // 
            resources.ApplyResources(this.lblCASH_DRAWER, "lblCASH_DRAWER");
            this.lblCASH_DRAWER.Name = "lblCASH_DRAWER";
            // 
            // txtTotalAmount
            // 
            resources.ApplyResources(this.txtTotalAmount, "txtTotalAmount");
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.ReadOnly = true;
            // 
            // lblSETTLE_AMOUNT
            // 
            resources.ApplyResources(this.lblSETTLE_AMOUNT, "lblSETTLE_AMOUNT");
            this.lblSETTLE_AMOUNT.Name = "lblSETTLE_AMOUNT";
            // 
            // lblDISCOUNT
            // 
            resources.ApplyResources(this.lblDISCOUNT, "lblDISCOUNT");
            this.lblDISCOUNT.Name = "lblDISCOUNT";
            // 
            // txtUnitPrice
            // 
            resources.ApplyResources(this.txtUnitPrice, "txtUnitPrice");
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.TextChanged += new System.EventHandler(this.txtCalAmount);
            this.txtUnitPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeyNumber);
            // 
            // txtDiscount
            // 
            resources.ApplyResources(this.txtDiscount, "txtDiscount");
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.TextChanged += new System.EventHandler(this.txtCalAmount);
            this.txtDiscount.Enter += new System.EventHandler(this.txtEnterEnglish);
            this.txtDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeyNumber);
            // 
            // txtBuyPower
            // 
            resources.ApplyResources(this.txtBuyPower, "txtBuyPower");
            this.txtBuyPower.Name = "txtBuyPower";
            this.txtBuyPower.TextChanged += new System.EventHandler(this.txtCalAmount);
            this.txtBuyPower.Enter += new System.EventHandler(this.txtEnterEnglish);
            this.txtBuyPower.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeyNumber);
            // 
            // lblPRICE_1
            // 
            resources.ApplyResources(this.lblPRICE_1, "lblPRICE_1");
            this.lblPRICE_1.Name = "lblPRICE_1";
            // 
            // lblBUY_POWER
            // 
            resources.ApplyResources(this.lblBUY_POWER, "lblBUY_POWER");
            this.lblBUY_POWER.Name = "lblBUY_POWER";
            // 
            // lblDATE
            // 
            resources.ApplyResources(this.lblDATE, "lblDATE");
            this.lblDATE.Name = "lblDATE";
            // 
            // dtpActivateDate
            // 
            resources.ApplyResources(this.dtpActivateDate, "dtpActivateDate");
            this.dtpActivateDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpActivateDate.Name = "dtpActivateDate";
            // 
            // lblREAD_AMOUNT_
            // 
            resources.ApplyResources(this.lblREAD_AMOUNT_, "lblREAD_AMOUNT_");
            this.lblREAD_AMOUNT_.Name = "lblREAD_AMOUNT_";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel2.Controls.Add(this.lblSHOW_DETAIL);
            this.panel2.Controls.Add(this.lblCARD_TYPE_);
            this.panel2.Controls.Add(this.btnREAD_CARD);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // lblSHOW_DETAIL
            // 
            resources.ApplyResources(this.lblSHOW_DETAIL, "lblSHOW_DETAIL");
            this.lblSHOW_DETAIL.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblSHOW_DETAIL.Name = "lblSHOW_DETAIL";
            this.lblSHOW_DETAIL.TabStop = true;
            this.lblSHOW_DETAIL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblCardDetailInfo_LinkClicked);
            // 
            // lblCARD_TYPE_
            // 
            this.lblCARD_TYPE_.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblCARD_TYPE_, "lblCARD_TYPE_");
            this.lblCARD_TYPE_.Name = "lblCARD_TYPE_";
            // 
            // btnREAD_CARD
            // 
            resources.ApplyResources(this.btnREAD_CARD, "btnREAD_CARD");
            this.btnREAD_CARD.Name = "btnREAD_CARD";
            this.btnREAD_CARD.UseVisualStyleBackColor = true;
            this.btnREAD_CARD.Click += new System.EventHandler(this.btnReadCard_Click);
            // 
            // txtPriceName
            // 
            resources.ApplyResources(this.txtPriceName, "txtPriceName");
            this.txtPriceName.Name = "txtPriceName";
            this.txtPriceName.ReadOnly = true;
            this.txtPriceName.TabStop = false;
            // 
            // txtSignUnitPrice
            // 
            resources.ApplyResources(this.txtSignUnitPrice, "txtSignUnitPrice");
            this.txtSignUnitPrice.Name = "txtSignUnitPrice";
            this.txtSignUnitPrice.ReadOnly = true;
            this.txtSignUnitPrice.TabStop = false;
            // 
            // txtSignTotalAmount
            // 
            resources.ApplyResources(this.txtSignTotalAmount, "txtSignTotalAmount");
            this.txtSignTotalAmount.Name = "txtSignTotalAmount";
            this.txtSignTotalAmount.ReadOnly = true;
            this.txtSignTotalAmount.TabStop = false;
            // 
            // txtSignPrice
            // 
            resources.ApplyResources(this.txtSignPrice, "txtSignPrice");
            this.txtSignPrice.Name = "txtSignPrice";
            this.txtSignPrice.ReadOnly = true;
            this.txtSignPrice.TabStop = false;
            // 
            // txtSTART_USAGE
            // 
            resources.ApplyResources(this.txtSTART_USAGE, "txtSTART_USAGE");
            this.txtSTART_USAGE.Name = "txtSTART_USAGE";
            this.txtSTART_USAGE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSTART_USAGE_KeyDown);
            // 
            // lblSTART_USAGE
            // 
            resources.ApplyResources(this.lblSTART_USAGE, "lblSTART_USAGE");
            this.lblSTART_USAGE.Name = "lblSTART_USAGE";
            // 
            // DialogCustomerActivatePrepaid
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerActivatePrepaid";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblPRICE;
        private Label lblGENERAL_INFORMATION;
        private Label lblCUSTOMER_TYPE;
        private Label lblMETER_CODE;
        private Label lblPOLE;
        private Label lblPRICE_TYPE;
        private Label lblMETER_INSTALLATION;
        private Label lblMETER_TYPE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblBREAKER_TYPE;
        private Label lblSHIELD;
        private Label lblBREAKER_CODE;
        private Label lblCABLE_SHIELD;
        private ExTextbox txtMeter;
        private ExTextbox txtBreaker;
        private ComboBox cboCableShield;
        private ComboBox cboMeterShield;
        private Label lblLAST_NAME_KH;
        private TextBox txtLastNamekh;
        private Label lblBOX;
        private ExTextbox txtBox;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label29;
        private TextBox txtPoleCode;
        private TextBox txtBreakerType;
        private TextBox txtMeterType;
        private TextBox txtCustomerType;
        private Label lblSTARTUP;
        private Label lblREAD_POWER_;
        private TextBox txtPrice;
        private CheckBox chkPRINT_RECEIPT;
        private TextBox txtCashDrawerName;
        private TextBox txtUserCashDrawerName;
        private Label lblCASHIER;
        private Label lblCASH_DRAWER;
        private TextBox txtTotalAmount;
        private Label lblSETTLE_AMOUNT;
        private Label lblDISCOUNT;
        private TextBox txtUnitPrice;
        private TextBox txtDiscount;
        private TextBox txtBuyPower;
        private Label lblPRICE_1;
        private Label lblBUY_POWER;
        private Label lblDATE;
        private DateTimePicker dtpActivateDate;
        private Label lblREAD_AMOUNT_;
        private Panel panel2;
        private ExLinkLabel lblSHOW_DETAIL;
        private Label lblCARD_TYPE_;
        private ExButton btnREAD_CARD;
        private TextBox txtPriceName;
        private TextBox txtSignTotalAmount;
        private TextBox txtSignUnitPrice;
        private TextBox txtSignPrice;
        private TextBox txtSTART_USAGE;
        private Label lblSTART_USAGE;
    }
}
