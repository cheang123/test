﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class PageRunPrepaidCredit
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageRunPrepaidCredit));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pHeader = new System.Windows.Forms.Panel();
            this.btnCHECK_USAGE = new SoftTech.Component.ExButton();
            this.btnMETER_UNREGISTER = new SoftTech.Component.ExButton();
            this.btnCUSTOMER_NO_USAGE = new SoftTech.Component.ExButton();
            this.btnRUN_SUBSIDY = new SoftTech.Component.ExButton();
            this.dgvBillingCycle = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.calendarColumn1 = new SoftTech.Component.CalendarColumn();
            this.calendarColumn2 = new SoftTech.Component.CalendarColumn();
            this.dataGridViewTimeColumn1 = new SoftTech.Component.DataGridViewTimeColumn();
            this.CYCLE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CYCLE_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.START_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.END_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COLLECT_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_CUSTOMER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BILLING_CUSTOMER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NO_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBillingCycle)).BeginInit();
            this.SuspendLayout();
            // 
            // pHeader
            // 
            this.pHeader.BackColor = System.Drawing.Color.Transparent;
            this.pHeader.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.pHeader.Controls.Add(this.btnCHECK_USAGE);
            this.pHeader.Controls.Add(this.btnMETER_UNREGISTER);
            this.pHeader.Controls.Add(this.btnCUSTOMER_NO_USAGE);
            this.pHeader.Controls.Add(this.btnRUN_SUBSIDY);
            resources.ApplyResources(this.pHeader, "pHeader");
            this.pHeader.Name = "pHeader";
            // 
            // btnCHECK_USAGE
            // 
            resources.ApplyResources(this.btnCHECK_USAGE, "btnCHECK_USAGE");
            this.btnCHECK_USAGE.Name = "btnCHECK_USAGE";
            this.btnCHECK_USAGE.UseVisualStyleBackColor = true;
            this.btnCHECK_USAGE.Click += new System.EventHandler(this.btnSpecialUsage_Click);
            // 
            // btnMETER_UNREGISTER
            // 
            resources.ApplyResources(this.btnMETER_UNREGISTER, "btnMETER_UNREGISTER");
            this.btnMETER_UNREGISTER.Name = "btnMETER_UNREGISTER";
            this.btnMETER_UNREGISTER.UseVisualStyleBackColor = true;
            this.btnMETER_UNREGISTER.Click += new System.EventHandler(this.btnViewUnregister_Click);
            // 
            // btnCUSTOMER_NO_USAGE
            // 
            resources.ApplyResources(this.btnCUSTOMER_NO_USAGE, "btnCUSTOMER_NO_USAGE");
            this.btnCUSTOMER_NO_USAGE.Name = "btnCUSTOMER_NO_USAGE";
            this.btnCUSTOMER_NO_USAGE.UseVisualStyleBackColor = true;
            this.btnCUSTOMER_NO_USAGE.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // btnRUN_SUBSIDY
            // 
            resources.ApplyResources(this.btnRUN_SUBSIDY, "btnRUN_SUBSIDY");
            this.btnRUN_SUBSIDY.Name = "btnRUN_SUBSIDY";
            this.btnRUN_SUBSIDY.UseVisualStyleBackColor = true;
            this.btnRUN_SUBSIDY.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // dgvBillingCycle
            // 
            this.dgvBillingCycle.AllowUserToAddRows = false;
            this.dgvBillingCycle.AllowUserToDeleteRows = false;
            this.dgvBillingCycle.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvBillingCycle.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBillingCycle.BackgroundColor = System.Drawing.Color.White;
            this.dgvBillingCycle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvBillingCycle.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvBillingCycle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBillingCycle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CYCLE_ID,
            this.CYCLE_NAME,
            this.MONTH,
            this.START_DATE,
            this.END_DATE,
            this.COLLECT_DATE,
            this.TOTAL_CUSTOMER,
            this.BILLING_CUSTOMER,
            this.NO_USAGE});
            resources.ApplyResources(this.dgvBillingCycle, "dgvBillingCycle");
            this.dgvBillingCycle.EnableHeadersVisualStyles = false;
            this.dgvBillingCycle.Name = "dgvBillingCycle";
            this.dgvBillingCycle.ReadOnly = true;
            this.dgvBillingCycle.RowHeadersVisible = false;
            this.dgvBillingCycle.RowTemplate.Height = 25;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CYCLE_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CYCLE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "BILLING_MONTH";
            dataGridViewCellStyle9.Format = "ថ្ងៃទី dd - ខែ MM - ឆ្នាំ yyyy";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle9;
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "END_DATE";
            dataGridViewCellStyle10.Format = "ថ្ងៃទី 0";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle10;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "COLLECT_DAY";
            dataGridViewCellStyle11.Format = "0 នាក់";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle11;
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "TOTAL_CUSTOMER";
            dataGridViewCellStyle12.Format = "0 នាក់";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle12;
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "NO_USAGE";
            dataGridViewCellStyle13.Format = "0 នាក់";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle13;
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "NO_USAGE";
            dataGridViewCellStyle14.Format = "0 នាក់";
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle14;
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // calendarColumn1
            // 
            this.calendarColumn1.DataPropertyName = "BILLING_MONTH";
            dataGridViewCellStyle15.Format = "ខែ MM - ឆ្នាំ yyyy";
            this.calendarColumn1.DefaultCellStyle = dataGridViewCellStyle15;
            resources.ApplyResources(this.calendarColumn1, "calendarColumn1");
            this.calendarColumn1.Name = "calendarColumn1";
            this.calendarColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // calendarColumn2
            // 
            dataGridViewCellStyle16.Format = "ថ្ងៃទី dd - ខែ MM - ឆ្នាំ yyyy";
            this.calendarColumn2.DefaultCellStyle = dataGridViewCellStyle16;
            resources.ApplyResources(this.calendarColumn2, "calendarColumn2");
            this.calendarColumn2.Name = "calendarColumn2";
            this.calendarColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.calendarColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewTimeColumn1
            // 
            resources.ApplyResources(this.dataGridViewTimeColumn1, "dataGridViewTimeColumn1");
            this.dataGridViewTimeColumn1.Name = "dataGridViewTimeColumn1";
            this.dataGridViewTimeColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTimeColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // CYCLE_ID
            // 
            this.CYCLE_ID.DataPropertyName = "CYCLE_ID";
            resources.ApplyResources(this.CYCLE_ID, "CYCLE_ID");
            this.CYCLE_ID.Name = "CYCLE_ID";
            this.CYCLE_ID.ReadOnly = true;
            // 
            // CYCLE_NAME
            // 
            this.CYCLE_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CYCLE_NAME.DataPropertyName = "CYCLE_NAME";
            resources.ApplyResources(this.CYCLE_NAME, "CYCLE_NAME");
            this.CYCLE_NAME.Name = "CYCLE_NAME";
            this.CYCLE_NAME.ReadOnly = true;
            // 
            // MONTH
            // 
            this.MONTH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.MONTH.DataPropertyName = "BILLING_MONTH";
            dataGridViewCellStyle2.Format = "MM - yyyy";
            this.MONTH.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.MONTH, "MONTH");
            this.MONTH.Name = "MONTH";
            this.MONTH.ReadOnly = true;
            this.MONTH.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // START_DATE
            // 
            this.START_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.START_DATE.DataPropertyName = "START_DATE";
            dataGridViewCellStyle3.Format = "dd - MM - yyyy";
            this.START_DATE.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.START_DATE, "START_DATE");
            this.START_DATE.Name = "START_DATE";
            this.START_DATE.ReadOnly = true;
            this.START_DATE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // END_DATE
            // 
            this.END_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.END_DATE.DataPropertyName = "END_DATE";
            dataGridViewCellStyle4.Format = "dd - MM - yyyy";
            this.END_DATE.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.END_DATE, "END_DATE");
            this.END_DATE.Name = "END_DATE";
            this.END_DATE.ReadOnly = true;
            // 
            // COLLECT_DATE
            // 
            this.COLLECT_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.COLLECT_DATE.DataPropertyName = "COLLECT_DAY";
            dataGridViewCellStyle5.Format = "ថ្ងៃទី 0";
            this.COLLECT_DATE.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.COLLECT_DATE, "COLLECT_DATE");
            this.COLLECT_DATE.Name = "COLLECT_DATE";
            this.COLLECT_DATE.ReadOnly = true;
            // 
            // TOTAL_CUSTOMER
            // 
            this.TOTAL_CUSTOMER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TOTAL_CUSTOMER.DataPropertyName = "TOTAL_CUSTOMER";
            dataGridViewCellStyle6.Format = "0 នាក់";
            this.TOTAL_CUSTOMER.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.TOTAL_CUSTOMER, "TOTAL_CUSTOMER");
            this.TOTAL_CUSTOMER.Name = "TOTAL_CUSTOMER";
            this.TOTAL_CUSTOMER.ReadOnly = true;
            // 
            // BILLING_CUSTOMER
            // 
            this.BILLING_CUSTOMER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.BILLING_CUSTOMER.DataPropertyName = "BILLING_CUSTOMER";
            dataGridViewCellStyle7.Format = "0 នាក់";
            this.BILLING_CUSTOMER.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.BILLING_CUSTOMER, "BILLING_CUSTOMER");
            this.BILLING_CUSTOMER.Name = "BILLING_CUSTOMER";
            this.BILLING_CUSTOMER.ReadOnly = true;
            // 
            // NO_USAGE
            // 
            this.NO_USAGE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.NO_USAGE.DataPropertyName = "NO_USAGE";
            dataGridViewCellStyle8.Format = "0 នាក់";
            this.NO_USAGE.DefaultCellStyle = dataGridViewCellStyle8;
            resources.ApplyResources(this.NO_USAGE, "NO_USAGE");
            this.NO_USAGE.Name = "NO_USAGE";
            this.NO_USAGE.ReadOnly = true;
            // 
            // PageRunPrepaidCredit
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgvBillingCycle);
            this.Controls.Add(this.pHeader);
            this.Name = "PageRunPrepaidCredit";
            this.pHeader.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBillingCycle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel pHeader;
        private ExButton btnRUN_SUBSIDY;
        private ExButton btnCUSTOMER_NO_USAGE;
        private DataGridView dgvBillingCycle;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTimeColumn dataGridViewTimeColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private CalendarColumn calendarColumn1;
        private CalendarColumn calendarColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private ExButton btnMETER_UNREGISTER;
        private ExButton btnCHECK_USAGE;
        private DataGridViewTextBoxColumn CYCLE_ID;
        private DataGridViewTextBoxColumn CYCLE_NAME;
        private DataGridViewTextBoxColumn MONTH;
        private DataGridViewTextBoxColumn START_DATE;
        private DataGridViewTextBoxColumn END_DATE;
        private DataGridViewTextBoxColumn COLLECT_DATE;
        private DataGridViewTextBoxColumn TOTAL_CUSTOMER;
        private DataGridViewTextBoxColumn BILLING_CUSTOMER;
        private DataGridViewTextBoxColumn NO_USAGE;
    }
}
