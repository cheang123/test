﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class DialogRenewCard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogRenewCard));
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timeConnection = new System.Windows.Forms.Timer(this.components);
            this.lblGENERAL_INFORMATION = new System.Windows.Forms.Label();
            this.txtPoleCode = new System.Windows.Forms.TextBox();
            this.lblBOX = new System.Windows.Forms.Label();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.txtBoxCode = new System.Windows.Forms.TextBox();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.txtAreaCode = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblUSAGE_INFORMATION = new System.Windows.Forms.Label();
            this.lblINVOICE_DATE = new System.Windows.Forms.Label();
            this.lblCASH_DRAWER = new System.Windows.Forms.Label();
            this.lblCASHIER = new System.Windows.Forms.Label();
            this.txtCashDrawerName = new System.Windows.Forms.TextBox();
            this.txtUserCashDrawerName = new System.Windows.Forms.TextBox();
            this.chkPRINT_RECEIPT = new System.Windows.Forms.CheckBox();
            this.txtCustomerCode = new SoftTech.Component.ExTextbox();
            this.txtCustomerName = new SoftTech.Component.ExTextbox();
            this.txtMeterCode = new SoftTech.Component.ExTextbox();
            this.txtBuyTimes = new System.Windows.Forms.TextBox();
            this.txtLastPurchasePower = new System.Windows.Forms.TextBox();
            this.lblBUY_TIMES = new System.Windows.Forms.Label();
            this.lblLAST_BUY_DATE = new System.Windows.Forms.Label();
            this.lblLAST_BUY_POWER = new System.Windows.Forms.Label();
            this.txtTotalPurchasePower = new System.Windows.Forms.TextBox();
            this.lblTOTAL_BUY_POWER = new System.Windows.Forms.Label();
            this.lblPOWER_COMPENSATED = new System.Windows.Forms.Label();
            this.txtCompensate = new System.Windows.Forms.TextBox();
            this.txtPriceType = new System.Windows.Forms.TextBox();
            this.dtpLastPurchaseDate = new System.Windows.Forms.DateTimePicker();
            this.cboInvoiceItem = new System.Windows.Forms.ComboBox();
            this.lblSERVICE = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.lblUNIT_PRICE = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtPayment = new System.Windows.Forms.TextBox();
            this.lblPRICE_TYPE = new System.Windows.Forms.Label();
            this.dtpRenewDate = new System.Windows.Forms.DateTimePicker();
            this.lblREAD_AMOUNT_ = new System.Windows.Forms.Label();
            this.txtDueAmount = new System.Windows.Forms.TextBox();
            this.lblDUE_AMOUNT = new System.Windows.Forms.Label();
            this.lblSETTLE_AMOUNT = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkLAST_POWER_CARD = new System.Windows.Forms.CheckBox();
            this.lblSHOW_DETAIL = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblCARD_TYPE_ = new System.Windows.Forms.Label();
            this.btnREAD_CARD = new SoftTech.Component.ExButton();
            this.chkSERVICE_CHARGE_NEW_CARD = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.chkSERVICE_CHARGE_NEW_CARD);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.lblDUE_AMOUNT);
            this.content.Controls.Add(this.lblSETTLE_AMOUNT);
            this.content.Controls.Add(this.txtDueAmount);
            this.content.Controls.Add(this.lblREAD_AMOUNT_);
            this.content.Controls.Add(this.dtpRenewDate);
            this.content.Controls.Add(this.lblPRICE_TYPE);
            this.content.Controls.Add(this.txtPayment);
            this.content.Controls.Add(this.lblUNIT_PRICE);
            this.content.Controls.Add(this.txtPrice);
            this.content.Controls.Add(this.cboInvoiceItem);
            this.content.Controls.Add(this.lblSERVICE);
            this.content.Controls.Add(this.dtpLastPurchaseDate);
            this.content.Controls.Add(this.txtBuyTimes);
            this.content.Controls.Add(this.txtLastPurchasePower);
            this.content.Controls.Add(this.lblBUY_TIMES);
            this.content.Controls.Add(this.lblLAST_BUY_DATE);
            this.content.Controls.Add(this.lblLAST_BUY_POWER);
            this.content.Controls.Add(this.txtTotalPurchasePower);
            this.content.Controls.Add(this.lblTOTAL_BUY_POWER);
            this.content.Controls.Add(this.lblPOWER_COMPENSATED);
            this.content.Controls.Add(this.txtCompensate);
            this.content.Controls.Add(this.txtPriceType);
            this.content.Controls.Add(this.txtMeterCode);
            this.content.Controls.Add(this.txtCustomerName);
            this.content.Controls.Add(this.txtCustomerCode);
            this.content.Controls.Add(this.chkPRINT_RECEIPT);
            this.content.Controls.Add(this.txtCashDrawerName);
            this.content.Controls.Add(this.txtUserCashDrawerName);
            this.content.Controls.Add(this.lblCASHIER);
            this.content.Controls.Add(this.lblCASH_DRAWER);
            this.content.Controls.Add(this.lblINVOICE_DATE);
            this.content.Controls.Add(this.lblUSAGE_INFORMATION);
            this.content.Controls.Add(this.txtPoleCode);
            this.content.Controls.Add(this.lblBOX);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.txtBoxCode);
            this.content.Controls.Add(this.lblPOLE);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.txtAreaCode);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            this.content.Controls.Add(this.lblGENERAL_INFORMATION);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.label32);
            this.content.Controls.Add(this.label30);
            this.content.Controls.Add(this.label2);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.label30, 0);
            this.content.Controls.SetChildIndex(this.label32, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblGENERAL_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtAreaCode, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE, 0);
            this.content.Controls.SetChildIndex(this.txtBoxCode, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblBOX, 0);
            this.content.Controls.SetChildIndex(this.txtPoleCode, 0);
            this.content.Controls.SetChildIndex(this.lblUSAGE_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblINVOICE_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblCASH_DRAWER, 0);
            this.content.Controls.SetChildIndex(this.lblCASHIER, 0);
            this.content.Controls.SetChildIndex(this.txtUserCashDrawerName, 0);
            this.content.Controls.SetChildIndex(this.txtCashDrawerName, 0);
            this.content.Controls.SetChildIndex(this.chkPRINT_RECEIPT, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCode, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerName, 0);
            this.content.Controls.SetChildIndex(this.txtMeterCode, 0);
            this.content.Controls.SetChildIndex(this.txtPriceType, 0);
            this.content.Controls.SetChildIndex(this.txtCompensate, 0);
            this.content.Controls.SetChildIndex(this.lblPOWER_COMPENSATED, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_BUY_POWER, 0);
            this.content.Controls.SetChildIndex(this.txtTotalPurchasePower, 0);
            this.content.Controls.SetChildIndex(this.lblLAST_BUY_POWER, 0);
            this.content.Controls.SetChildIndex(this.lblLAST_BUY_DATE, 0);
            this.content.Controls.SetChildIndex(this.lblBUY_TIMES, 0);
            this.content.Controls.SetChildIndex(this.txtLastPurchasePower, 0);
            this.content.Controls.SetChildIndex(this.txtBuyTimes, 0);
            this.content.Controls.SetChildIndex(this.dtpLastPurchaseDate, 0);
            this.content.Controls.SetChildIndex(this.lblSERVICE, 0);
            this.content.Controls.SetChildIndex(this.cboInvoiceItem, 0);
            this.content.Controls.SetChildIndex(this.txtPrice, 0);
            this.content.Controls.SetChildIndex(this.lblUNIT_PRICE, 0);
            this.content.Controls.SetChildIndex(this.txtPayment, 0);
            this.content.Controls.SetChildIndex(this.lblPRICE_TYPE, 0);
            this.content.Controls.SetChildIndex(this.dtpRenewDate, 0);
            this.content.Controls.SetChildIndex(this.lblREAD_AMOUNT_, 0);
            this.content.Controls.SetChildIndex(this.txtDueAmount, 0);
            this.content.Controls.SetChildIndex(this.lblSETTLE_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblDUE_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.chkSERVICE_CHARGE_NEW_CARD, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // timeConnection
            // 
            this.timeConnection.Interval = 3000;
            // 
            // lblGENERAL_INFORMATION
            // 
            resources.ApplyResources(this.lblGENERAL_INFORMATION, "lblGENERAL_INFORMATION");
            this.lblGENERAL_INFORMATION.Name = "lblGENERAL_INFORMATION";
            // 
            // txtPoleCode
            // 
            resources.ApplyResources(this.txtPoleCode, "txtPoleCode");
            this.txtPoleCode.Name = "txtPoleCode";
            this.txtPoleCode.ReadOnly = true;
            // 
            // lblBOX
            // 
            resources.ApplyResources(this.lblBOX, "lblBOX");
            this.lblBOX.Name = "lblBOX";
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // txtBoxCode
            // 
            resources.ApplyResources(this.txtBoxCode, "txtBoxCode");
            this.txtBoxCode.Name = "txtBoxCode";
            this.txtBoxCode.ReadOnly = true;
            // 
            // lblPOLE
            // 
            resources.ApplyResources(this.lblPOLE, "lblPOLE");
            this.lblPOLE.Name = "lblPOLE";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // txtAreaCode
            // 
            resources.ApplyResources(this.txtAreaCode, "txtAreaCode");
            this.txtAreaCode.Name = "txtAreaCode";
            this.txtAreaCode.ReadOnly = true;
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblUSAGE_INFORMATION
            // 
            resources.ApplyResources(this.lblUSAGE_INFORMATION, "lblUSAGE_INFORMATION");
            this.lblUSAGE_INFORMATION.Name = "lblUSAGE_INFORMATION";
            // 
            // lblINVOICE_DATE
            // 
            resources.ApplyResources(this.lblINVOICE_DATE, "lblINVOICE_DATE");
            this.lblINVOICE_DATE.Name = "lblINVOICE_DATE";
            // 
            // lblCASH_DRAWER
            // 
            resources.ApplyResources(this.lblCASH_DRAWER, "lblCASH_DRAWER");
            this.lblCASH_DRAWER.Name = "lblCASH_DRAWER";
            // 
            // lblCASHIER
            // 
            resources.ApplyResources(this.lblCASHIER, "lblCASHIER");
            this.lblCASHIER.Name = "lblCASHIER";
            // 
            // txtCashDrawerName
            // 
            resources.ApplyResources(this.txtCashDrawerName, "txtCashDrawerName");
            this.txtCashDrawerName.Name = "txtCashDrawerName";
            this.txtCashDrawerName.ReadOnly = true;
            // 
            // txtUserCashDrawerName
            // 
            resources.ApplyResources(this.txtUserCashDrawerName, "txtUserCashDrawerName");
            this.txtUserCashDrawerName.Name = "txtUserCashDrawerName";
            this.txtUserCashDrawerName.ReadOnly = true;
            // 
            // chkPRINT_RECEIPT
            // 
            resources.ApplyResources(this.chkPRINT_RECEIPT, "chkPRINT_RECEIPT");
            this.chkPRINT_RECEIPT.Checked = true;
            this.chkPRINT_RECEIPT.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPRINT_RECEIPT.Name = "chkPRINT_RECEIPT";
            this.chkPRINT_RECEIPT.UseVisualStyleBackColor = true;
            // 
            // txtCustomerCode
            // 
            this.txtCustomerCode.BackColor = System.Drawing.Color.White;
            this.txtCustomerCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerCode.AdvanceSearch += new System.EventHandler(this.txtCustomerCode_AdvanceSearch);
            this.txtCustomerCode.CancelAdvanceSearch += new System.EventHandler(this.txtCustomerCode_CancelAdvanceSearch);
            this.txtCustomerCode.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.BackColor = System.Drawing.Color.White;
            this.txtCustomerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerName, "txtCustomerName");
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerName.AdvanceSearch += new System.EventHandler(this.txtCustomerName_AdvanceSearch);
            this.txtCustomerName.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // txtMeterCode
            // 
            this.txtMeterCode.BackColor = System.Drawing.Color.White;
            this.txtMeterCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtMeterCode, "txtMeterCode");
            this.txtMeterCode.Name = "txtMeterCode";
            this.txtMeterCode.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtMeterCode.AdvanceSearch += new System.EventHandler(this.txtMeterCode_AdvanceSearch);
            this.txtMeterCode.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtBuyTimes
            // 
            resources.ApplyResources(this.txtBuyTimes, "txtBuyTimes");
            this.txtBuyTimes.Name = "txtBuyTimes";
            this.txtBuyTimes.ReadOnly = true;
            // 
            // txtLastPurchasePower
            // 
            resources.ApplyResources(this.txtLastPurchasePower, "txtLastPurchasePower");
            this.txtLastPurchasePower.Name = "txtLastPurchasePower";
            this.txtLastPurchasePower.ReadOnly = true;
            // 
            // lblBUY_TIMES
            // 
            resources.ApplyResources(this.lblBUY_TIMES, "lblBUY_TIMES");
            this.lblBUY_TIMES.Name = "lblBUY_TIMES";
            // 
            // lblLAST_BUY_DATE
            // 
            resources.ApplyResources(this.lblLAST_BUY_DATE, "lblLAST_BUY_DATE");
            this.lblLAST_BUY_DATE.Name = "lblLAST_BUY_DATE";
            // 
            // lblLAST_BUY_POWER
            // 
            resources.ApplyResources(this.lblLAST_BUY_POWER, "lblLAST_BUY_POWER");
            this.lblLAST_BUY_POWER.Name = "lblLAST_BUY_POWER";
            // 
            // txtTotalPurchasePower
            // 
            resources.ApplyResources(this.txtTotalPurchasePower, "txtTotalPurchasePower");
            this.txtTotalPurchasePower.Name = "txtTotalPurchasePower";
            this.txtTotalPurchasePower.ReadOnly = true;
            // 
            // lblTOTAL_BUY_POWER
            // 
            resources.ApplyResources(this.lblTOTAL_BUY_POWER, "lblTOTAL_BUY_POWER");
            this.lblTOTAL_BUY_POWER.Name = "lblTOTAL_BUY_POWER";
            // 
            // lblPOWER_COMPENSATED
            // 
            resources.ApplyResources(this.lblPOWER_COMPENSATED, "lblPOWER_COMPENSATED");
            this.lblPOWER_COMPENSATED.Name = "lblPOWER_COMPENSATED";
            // 
            // txtCompensate
            // 
            resources.ApplyResources(this.txtCompensate, "txtCompensate");
            this.txtCompensate.Name = "txtCompensate";
            this.txtCompensate.ReadOnly = true;
            // 
            // txtPriceType
            // 
            resources.ApplyResources(this.txtPriceType, "txtPriceType");
            this.txtPriceType.Name = "txtPriceType";
            this.txtPriceType.ReadOnly = true;
            // 
            // dtpLastPurchaseDate
            // 
            resources.ApplyResources(this.dtpLastPurchaseDate, "dtpLastPurchaseDate");
            this.dtpLastPurchaseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLastPurchaseDate.Name = "dtpLastPurchaseDate";
            // 
            // cboInvoiceItem
            // 
            this.cboInvoiceItem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboInvoiceItem, "cboInvoiceItem");
            this.cboInvoiceItem.FormattingEnabled = true;
            this.cboInvoiceItem.Name = "cboInvoiceItem";
            // 
            // lblSERVICE
            // 
            resources.ApplyResources(this.lblSERVICE, "lblSERVICE");
            this.lblSERVICE.Name = "lblSERVICE";
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Name = "label30";
            // 
            // lblUNIT_PRICE
            // 
            resources.ApplyResources(this.lblUNIT_PRICE, "lblUNIT_PRICE");
            this.lblUNIT_PRICE.Name = "lblUNIT_PRICE";
            // 
            // txtPrice
            // 
            resources.ApplyResources(this.txtPrice, "txtPrice");
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.TextChanged += new System.EventHandler(this.txtTextChangeCalcPayment);
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Name = "label32";
            // 
            // txtPayment
            // 
            resources.ApplyResources(this.txtPayment, "txtPayment");
            this.txtPayment.Name = "txtPayment";
            this.txtPayment.TextChanged += new System.EventHandler(this.txtTextChangeCalcPayment);
            // 
            // lblPRICE_TYPE
            // 
            resources.ApplyResources(this.lblPRICE_TYPE, "lblPRICE_TYPE");
            this.lblPRICE_TYPE.Name = "lblPRICE_TYPE";
            // 
            // dtpRenewDate
            // 
            resources.ApplyResources(this.dtpRenewDate, "dtpRenewDate");
            this.dtpRenewDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRenewDate.Name = "dtpRenewDate";
            // 
            // lblREAD_AMOUNT_
            // 
            resources.ApplyResources(this.lblREAD_AMOUNT_, "lblREAD_AMOUNT_");
            this.lblREAD_AMOUNT_.Name = "lblREAD_AMOUNT_";
            // 
            // txtDueAmount
            // 
            resources.ApplyResources(this.txtDueAmount, "txtDueAmount");
            this.txtDueAmount.Name = "txtDueAmount";
            this.txtDueAmount.ReadOnly = true;
            // 
            // lblDUE_AMOUNT
            // 
            resources.ApplyResources(this.lblDUE_AMOUNT, "lblDUE_AMOUNT");
            this.lblDUE_AMOUNT.Name = "lblDUE_AMOUNT";
            // 
            // lblSETTLE_AMOUNT
            // 
            resources.ApplyResources(this.lblSETTLE_AMOUNT, "lblSETTLE_AMOUNT");
            this.lblSETTLE_AMOUNT.Name = "lblSETTLE_AMOUNT";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel2.Controls.Add(this.chkLAST_POWER_CARD);
            this.panel2.Controls.Add(this.lblSHOW_DETAIL);
            this.panel2.Controls.Add(this.lblCARD_TYPE_);
            this.panel2.Controls.Add(this.btnREAD_CARD);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // chkLAST_POWER_CARD
            // 
            resources.ApplyResources(this.chkLAST_POWER_CARD, "chkLAST_POWER_CARD");
            this.chkLAST_POWER_CARD.Name = "chkLAST_POWER_CARD";
            this.chkLAST_POWER_CARD.UseVisualStyleBackColor = true;
            this.chkLAST_POWER_CARD.CheckedChanged += new System.EventHandler(this.chkNewHavePower_CheckedChanged);
            // 
            // lblSHOW_DETAIL
            // 
            resources.ApplyResources(this.lblSHOW_DETAIL, "lblSHOW_DETAIL");
            this.lblSHOW_DETAIL.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblSHOW_DETAIL.Name = "lblSHOW_DETAIL";
            this.lblSHOW_DETAIL.TabStop = true;
            this.lblSHOW_DETAIL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblCardDetailInfo_LinkClicked);
            // 
            // lblCARD_TYPE_
            // 
            this.lblCARD_TYPE_.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblCARD_TYPE_, "lblCARD_TYPE_");
            this.lblCARD_TYPE_.Name = "lblCARD_TYPE_";
            // 
            // btnREAD_CARD
            // 
            resources.ApplyResources(this.btnREAD_CARD, "btnREAD_CARD");
            this.btnREAD_CARD.Name = "btnREAD_CARD";
            this.btnREAD_CARD.UseVisualStyleBackColor = true;
            this.btnREAD_CARD.Click += new System.EventHandler(this.btnReadCard_Click);
            // 
            // chkSERVICE_CHARGE_NEW_CARD
            // 
            resources.ApplyResources(this.chkSERVICE_CHARGE_NEW_CARD, "chkSERVICE_CHARGE_NEW_CARD");
            this.chkSERVICE_CHARGE_NEW_CARD.Checked = true;
            this.chkSERVICE_CHARGE_NEW_CARD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSERVICE_CHARGE_NEW_CARD.Name = "chkSERVICE_CHARGE_NEW_CARD";
            this.chkSERVICE_CHARGE_NEW_CARD.UseVisualStyleBackColor = true;
            this.chkSERVICE_CHARGE_NEW_CARD.CheckedChanged += new System.EventHandler(this.checkAddCharge_CheckedChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // DialogRenewCard
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogRenewCard";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel1;
        private Timer timeConnection;
        private Label lblGENERAL_INFORMATION;
        private TextBox txtPoleCode;
        private Label lblBOX;
        private Label lblMETER_CODE;
        private TextBox txtBoxCode;
        private Label lblPOLE;
        private Label lblAREA;
        private Label lblCUSTOMER_NAME;
        private TextBox txtAreaCode;
        private Label lblCUSTOMER_CODE;
        private Label lblUSAGE_INFORMATION;
        private Label lblINVOICE_DATE;
        private CheckBox chkPRINT_RECEIPT;
        private TextBox txtCashDrawerName;
        private TextBox txtUserCashDrawerName;
        private Label lblCASHIER;
        private Label lblCASH_DRAWER;
        private ExTextbox txtMeterCode;
        private ExTextbox txtCustomerName;
        private ExTextbox txtCustomerCode;
        private TextBox txtBuyTimes;
        private TextBox txtLastPurchasePower;
        private Label lblBUY_TIMES;
        private Label lblLAST_BUY_DATE;
        private Label lblLAST_BUY_POWER;
        private TextBox txtTotalPurchasePower;
        private Label lblTOTAL_BUY_POWER;
        private Label lblPOWER_COMPENSATED;
        private TextBox txtCompensate;
        private TextBox txtPriceType;
        private DateTimePicker dtpLastPurchaseDate;
        private ComboBox cboInvoiceItem;
        private Label lblSERVICE;
        private Label label30;
        private Label lblUNIT_PRICE;
        private TextBox txtPrice;
        private Label label32;
        private TextBox txtPayment;
        private DateTimePicker dtpRenewDate;
        private Label lblPRICE_TYPE;
        private TextBox txtDueAmount;
        private Label lblREAD_AMOUNT_;
        private Label lblDUE_AMOUNT;
        private Label lblSETTLE_AMOUNT;
        private Panel panel2;
        private Label lblCARD_TYPE_;
        private ExButton btnREAD_CARD;
        private CheckBox chkSERVICE_CHARGE_NEW_CARD;
        private ExLinkLabel lblSHOW_DETAIL;
        private CheckBox chkLAST_POWER_CARD;
        private Label label2;
        private ComboBox cboCurrency;
        private Label lblCURRENCY;
    }
}