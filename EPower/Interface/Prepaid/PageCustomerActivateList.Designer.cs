﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class PageCustomerActivateList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageCustomerActivateList));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnREPORT_CONNECTION = new SoftTech.Component.ExButton();
            this.btnREPORT_AGREEMENT = new SoftTech.Component.ExButton();
            this.btnADD_SERVICE = new SoftTech.Component.ExButton();
            this.btnDEPOSIT = new SoftTech.Component.ExButton();
            this.btnACTIVATE = new SoftTech.Component.ExButton();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.txtSearchCus = new SoftTech.Component.ExTextbox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMPARE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PHASE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEPOSIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONNECTION_FEE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnREPORT_CONNECTION);
            this.panel1.Controls.Add(this.btnREPORT_AGREEMENT);
            this.panel1.Controls.Add(this.btnADD_SERVICE);
            this.panel1.Controls.Add(this.btnDEPOSIT);
            this.panel1.Controls.Add(this.btnACTIVATE);
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.txtSearchCus);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnREPORT_CONNECTION
            // 
            resources.ApplyResources(this.btnREPORT_CONNECTION, "btnREPORT_CONNECTION");
            this.btnREPORT_CONNECTION.Name = "btnREPORT_CONNECTION";
            this.btnREPORT_CONNECTION.UseVisualStyleBackColor = true;
            this.btnREPORT_CONNECTION.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // btnREPORT_AGREEMENT
            // 
            resources.ApplyResources(this.btnREPORT_AGREEMENT, "btnREPORT_AGREEMENT");
            this.btnREPORT_AGREEMENT.Name = "btnREPORT_AGREEMENT";
            this.btnREPORT_AGREEMENT.UseVisualStyleBackColor = true;
            this.btnREPORT_AGREEMENT.Click += new System.EventHandler(this.btnAgreement_Click);
            // 
            // btnADD_SERVICE
            // 
            resources.ApplyResources(this.btnADD_SERVICE, "btnADD_SERVICE");
            this.btnADD_SERVICE.Name = "btnADD_SERVICE";
            this.btnADD_SERVICE.UseVisualStyleBackColor = true;
            this.btnADD_SERVICE.Click += new System.EventHandler(this.btnAddCharge_Click);
            // 
            // btnDEPOSIT
            // 
            resources.ApplyResources(this.btnDEPOSIT, "btnDEPOSIT");
            this.btnDEPOSIT.Name = "btnDEPOSIT";
            this.btnDEPOSIT.UseVisualStyleBackColor = true;
            this.btnDEPOSIT.Click += new System.EventHandler(this.btnDeposit_Click);
            // 
            // btnACTIVATE
            // 
            resources.ApplyResources(this.btnACTIVATE, "btnACTIVATE");
            this.btnACTIVATE.Name = "btnACTIVATE";
            this.btnACTIVATE.UseVisualStyleBackColor = true;
            this.btnACTIVATE.Click += new System.EventHandler(this.btnActive_Click);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnBlock_Click);
            // 
            // txtSearchCus
            // 
            this.txtSearchCus.BackColor = System.Drawing.Color.White;
            this.txtSearchCus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearchCus, "txtSearchCus");
            this.txtSearchCus.Name = "txtSearchCus";
            this.txtSearchCus.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearchCus.QuickSearch += new System.EventHandler(this.txtSearchCus_QuickSearch);
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.AREA,
            this.CUSTOMER_TYPE,
            this.AMPARE,
            this.PHASE,
            this.DEPOSIT,
            this.CONNECTION_FEE,
            this.CURRENCY_SING_});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            this.CUSTOMER_ID.FillWeight = 65F;
            resources.ApplyResources(this.CUSTOMER_ID, "CUSTOMER_ID");
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            // 
            // AREA
            // 
            this.AREA.DataPropertyName = "AREA_NAME";
            resources.ApplyResources(this.AREA, "AREA");
            this.AREA.Name = "AREA";
            // 
            // CUSTOMER_TYPE
            // 
            this.CUSTOMER_TYPE.DataPropertyName = "CUSTOMER_TYPE_NAME";
            resources.ApplyResources(this.CUSTOMER_TYPE, "CUSTOMER_TYPE");
            this.CUSTOMER_TYPE.Name = "CUSTOMER_TYPE";
            // 
            // AMPARE
            // 
            this.AMPARE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.AMPARE.DataPropertyName = "AMPARE_NAME";
            resources.ApplyResources(this.AMPARE, "AMPARE");
            this.AMPARE.Name = "AMPARE";
            // 
            // PHASE
            // 
            this.PHASE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.PHASE.DataPropertyName = "PHASE_NAME";
            resources.ApplyResources(this.PHASE, "PHASE");
            this.PHASE.Name = "PHASE";
            // 
            // DEPOSIT
            // 
            this.DEPOSIT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DEPOSIT.DataPropertyName = "DEPOSIT";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##0.####";
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DEPOSIT.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.DEPOSIT, "DEPOSIT");
            this.DEPOSIT.Name = "DEPOSIT";
            // 
            // CONNECTION_FEE
            // 
            this.CONNECTION_FEE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CONNECTION_FEE.DataPropertyName = "CONNECTION_FEE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "#,##0.####";
            this.CONNECTION_FEE.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CONNECTION_FEE, "CONNECTION_FEE");
            this.CONNECTION_FEE.Name = "CONNECTION_FEE";
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            this.CURRENCY_SING_.FillWeight = 65F;
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            // 
            // PageCustomerActivateList
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageCustomerActivateList";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtSearchCus;
        private ExButton btnACTIVATE;
        private ExButton btnREMOVE;
        private ExButton btnDEPOSIT;
        private ExButton btnADD_SERVICE;
        private ExButton btnREPORT_CONNECTION;
        private ExButton btnREPORT_AGREEMENT;
        private DataGridView dgv;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn CUSTOMER_TYPE;
        private DataGridViewTextBoxColumn AMPARE;
        private DataGridViewTextBoxColumn PHASE;
        private DataGridViewTextBoxColumn DEPOSIT;
        private DataGridViewTextBoxColumn CONNECTION_FEE;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
    }
}
