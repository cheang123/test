﻿using System;
using System.IO;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.PrePaid
{
    public partial class PageReportTotalSaleSummary : UserControl
    {
        private CrystalReportHelper ch;
        DateTime dtStart, dtEnd;
        public PageReportTotalSaleSummary()
        {
            InitializeComponent();            
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            Runner.Run(viewReport);
        }

        private void btnMail_Click(object sender, EventArgs e)
        {
            sendMail();
        }

        private void viewReport()
        {
            ch = new CrystalReportHelper("ReportPrepaidTotalCustomerSale.rpt");
            dtStart = UIHelper._DefaultDate;
            dtEnd = DBDataContext.Db.GetSystemDate();             
            ch.SetParameter("@START_DATE", dtStart);
            ch.SetParameter("@END_DATE",dtEnd);            
            viewer.ReportSource = ch.Report;
            viewer.ViewReport();
        }

        private void sendMail()
        {
            try
            {
                viewReport();
                string exportPath = new FileInfo(Settings.Default.PATH_TEMP + ch.ReportName + ".xls").FullName;
                ch.ExportToExcel(exportPath);
                DialogSendMail.Instance.Add(ch.ReportName, exportPath);
                DialogSendMail.Instance.ShowDialog();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }
    }
}
