﻿using System.ComponentModel;
using System.Windows.Forms;
using AxCrystalActiveXReportViewerLib105;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class PageReportMonthToDateSaleSummary
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportMonthToDateSaleSummary));
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboCustomerType = new System.Windows.Forms.ComboBox();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btnMail = new SoftTech.Component.ExButton();
            this.btnViewReport = new SoftTech.Component.ExButton();
            this.viewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboCustomerType);
            this.panel1.Controls.Add(this.cboArea);
            this.panel1.Controls.Add(this.dtpEnd);
            this.panel1.Controls.Add(this.dtpDate);
            this.panel1.Controls.Add(this.btnMail);
            this.panel1.Controls.Add(this.btnViewReport);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboCustomerType
            // 
            this.cboCustomerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerType.FormattingEnabled = true;
            this.cboCustomerType.Items.AddRange(new object[] {
            resources.GetString("cboCustomerType.Items"),
            resources.GetString("cboCustomerType.Items1"),
            resources.GetString("cboCustomerType.Items2"),
            resources.GetString("cboCustomerType.Items3"),
            resources.GetString("cboCustomerType.Items4")});
            resources.ApplyResources(this.cboCustomerType, "cboCustomerType");
            this.cboCustomerType.Name = "cboCustomerType";
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.FormattingEnabled = true;
            this.cboArea.Items.AddRange(new object[] {
            resources.GetString("cboArea.Items"),
            resources.GetString("cboArea.Items1"),
            resources.GetString("cboArea.Items2"),
            resources.GetString("cboArea.Items3"),
            resources.GetString("cboArea.Items4")});
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            // 
            // dtpEnd
            // 
            resources.ApplyResources(this.dtpEnd, "dtpEnd");
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Name = "dtpEnd";
            // 
            // dtpDate
            // 
            resources.ApplyResources(this.dtpDate, "dtpDate");
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.ShowCheckBox = true;
            // 
            // btnMail
            // 
            resources.ApplyResources(this.btnMail, "btnMail");
            this.btnMail.Name = "btnMail";
            this.btnMail.UseVisualStyleBackColor = true;
            this.btnMail.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // btnViewReport
            // 
            resources.ApplyResources(this.btnViewReport, "btnViewReport");
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.UseVisualStyleBackColor = true;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // viewer
            // 
            this.viewer.ActiveViewIndex = -1;
            this.viewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.viewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.viewer.DisplayStatusBar = false;
            resources.ApplyResources(this.viewer, "viewer");
            this.viewer.Name = "viewer";
            // 
            // PageReportMonthToDateSaleSummary
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.viewer);
            this.Controls.Add(this.panel1);
            this.Name = "PageReportMonthToDateSaleSummary";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnViewReport;
        private ExButton btnMail;
        private DateTimePicker dtpDate;
        private DateTimePicker dtpEnd;
        public ComboBox cboCustomerType;
        public ComboBox cboArea;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer viewer;
    }
}
