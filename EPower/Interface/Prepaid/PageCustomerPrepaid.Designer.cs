﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class PageCustomerPrepaid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageCustomerPrepaid));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnREAD_CARD = new SoftTech.Component.ExButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCLOSE_CUSTOMER = new SoftTech.Component.ExButton();
            this.btnADD_SERVICE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.pnlGeneral = new System.Windows.Forms.Panel();
            this.txtStatus = new System.Windows.Forms.ComboBox();
            this.txtSearch = new SoftTech.Component.ExTextbox();
            this.lblTYPE_TO_SEARCH = new System.Windows.Forms.Label();
            this.lblROW_NOT_FOUND = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboCustomerGroup = new System.Windows.Forms.ComboBox();
            this.cboConnectionType = new System.Windows.Forms.ComboBox();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PHONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlGeneral.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.CUSTOMER_CONNECTION_TYPE,
            this.AREA,
            this.POLE,
            this.BOX,
            this.METER,
            this.PHONE,
            this.CUSTOMER_STATUS});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnREAD_CARD);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnCLOSE_CUSTOMER);
            this.panel1.Controls.Add(this.btnADD_SERVICE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.pnlGeneral);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnREAD_CARD
            // 
            resources.ApplyResources(this.btnREAD_CARD, "btnREAD_CARD");
            this.btnREAD_CARD.Name = "btnREAD_CARD";
            this.btnREAD_CARD.UseVisualStyleBackColor = true;
            this.btnREAD_CARD.Click += new System.EventHandler(this.btnReadCard_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGray;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // btnCLOSE_CUSTOMER
            // 
            resources.ApplyResources(this.btnCLOSE_CUSTOMER, "btnCLOSE_CUSTOMER");
            this.btnCLOSE_CUSTOMER.Name = "btnCLOSE_CUSTOMER";
            this.btnCLOSE_CUSTOMER.UseVisualStyleBackColor = true;
            this.btnCLOSE_CUSTOMER.Click += new System.EventHandler(this.btnCloseCustomer_Click);
            // 
            // btnADD_SERVICE
            // 
            resources.ApplyResources(this.btnADD_SERVICE, "btnADD_SERVICE");
            this.btnADD_SERVICE.Name = "btnADD_SERVICE";
            this.btnADD_SERVICE.UseVisualStyleBackColor = true;
            this.btnADD_SERVICE.Click += new System.EventHandler(this.btnAddCharge_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // pnlGeneral
            // 
            this.pnlGeneral.BackColor = System.Drawing.Color.Transparent;
            this.pnlGeneral.Controls.Add(this.cboConnectionType);
            this.pnlGeneral.Controls.Add(this.cboCustomerGroup);
            this.pnlGeneral.Controls.Add(this.txtStatus);
            this.pnlGeneral.Controls.Add(this.txtSearch);
            resources.ApplyResources(this.pnlGeneral, "pnlGeneral");
            this.pnlGeneral.Name = "pnlGeneral";
            // 
            // txtStatus
            // 
            this.txtStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtStatus.FormattingEnabled = true;
            resources.ApplyResources(this.txtStatus, "txtStatus");
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.SelectedIndexChanged += new System.EventHandler(this.txtStatus_SelectedIndexChanged);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearch, "txtSearch");
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearch.QuickSearch += new System.EventHandler(this.exTextbox1_QuickSearch);
            this.txtSearch.Enter += new System.EventHandler(this.txtSearch_Enter);
            // 
            // lblTYPE_TO_SEARCH
            // 
            resources.ApplyResources(this.lblTYPE_TO_SEARCH, "lblTYPE_TO_SEARCH");
            this.lblTYPE_TO_SEARCH.ForeColor = System.Drawing.Color.DimGray;
            this.lblTYPE_TO_SEARCH.Name = "lblTYPE_TO_SEARCH";
            // 
            // lblROW_NOT_FOUND
            // 
            resources.ApplyResources(this.lblROW_NOT_FOUND, "lblROW_NOT_FOUND");
            this.lblROW_NOT_FOUND.ForeColor = System.Drawing.Color.Black;
            this.lblROW_NOT_FOUND.Name = "lblROW_NOT_FOUND";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CUSTOMER_TYPE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "AREA";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "PHONE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // cboCustomerGroup
            // 
            this.cboCustomerGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerGroup.DropDownWidth = 180;
            this.cboCustomerGroup.FormattingEnabled = true;
            resources.ApplyResources(this.cboCustomerGroup, "cboCustomerGroup");
            this.cboCustomerGroup.Name = "cboCustomerGroup";
            this.cboCustomerGroup.SelectedIndexChanged += new System.EventHandler(this.cboCustomerGroup_SelectedIndexChanged);
            // 
            // cboConnectionType
            // 
            this.cboConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConnectionType.DropDownWidth = 180;
            this.cboConnectionType.FormattingEnabled = true;
            resources.ApplyResources(this.cboConnectionType, "cboConnectionType");
            this.cboConnectionType.Name = "cboConnectionType";
            this.cboConnectionType.SelectedIndexChanged += new System.EventHandler(this.cboConnectionType_SelectedIndexChanged);
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.CUSTOMER_ID, "CUSTOMER_ID");
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            // 
            // CUSTOMER_CONNECTION_TYPE
            // 
            this.CUSTOMER_CONNECTION_TYPE.DataPropertyName = "CUSTOMER_TYPE";
            resources.ApplyResources(this.CUSTOMER_CONNECTION_TYPE, "CUSTOMER_CONNECTION_TYPE");
            this.CUSTOMER_CONNECTION_TYPE.Name = "CUSTOMER_CONNECTION_TYPE";
            // 
            // AREA
            // 
            this.AREA.DataPropertyName = "AREA";
            resources.ApplyResources(this.AREA, "AREA");
            this.AREA.Name = "AREA";
            // 
            // POLE
            // 
            this.POLE.DataPropertyName = "POLE";
            resources.ApplyResources(this.POLE, "POLE");
            this.POLE.Name = "POLE";
            // 
            // BOX
            // 
            this.BOX.DataPropertyName = "BOX";
            resources.ApplyResources(this.BOX, "BOX");
            this.BOX.Name = "BOX";
            // 
            // METER
            // 
            this.METER.DataPropertyName = "METER";
            resources.ApplyResources(this.METER, "METER");
            this.METER.Name = "METER";
            // 
            // PHONE
            // 
            this.PHONE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PHONE.DataPropertyName = "PHONE";
            resources.ApplyResources(this.PHONE, "PHONE");
            this.PHONE.Name = "PHONE";
            // 
            // CUSTOMER_STATUS
            // 
            this.CUSTOMER_STATUS.DataPropertyName = "CUSTOMER_STATUS";
            resources.ApplyResources(this.CUSTOMER_STATUS, "CUSTOMER_STATUS");
            this.CUSTOMER_STATUS.Name = "CUSTOMER_STATUS";
            // 
            // PageCustomerPrepaid
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lblROW_NOT_FOUND);
            this.Controls.Add(this.lblTYPE_TO_SEARCH);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageCustomerPrepaid";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.pnlGeneral.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtSearch;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        private ExButton btnADD_SERVICE;
        private ComboBox txtStatus;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private ExButton btnCLOSE_CUSTOMER;
        private Label lblTYPE_TO_SEARCH;
        private Label lblROW_NOT_FOUND;
        private ExButton btnREAD_CARD;
        private Label label1;
        private Panel pnlGeneral;
        private ComboBox cboCustomerGroup;
        private ComboBox cboConnectionType;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn CUSTOMER_CONNECTION_TYPE;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn POLE;
        private DataGridViewTextBoxColumn BOX;
        private DataGridViewTextBoxColumn METER;
        private DataGridViewTextBoxColumn PHONE;
        private DataGridViewTextBoxColumn CUSTOMER_STATUS;
    }
}
