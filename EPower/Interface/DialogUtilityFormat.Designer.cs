﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogUtilityFormat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogUtilityFormat));
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblHEADER = new System.Windows.Forms.Label();
            this.lblSTART = new System.Windows.Forms.Label();
            this.lblDIGIT = new System.Windows.Forms.Label();
            this.txtHEADER = new System.Windows.Forms.TextBox();
            this.txtDIGIIT = new System.Windows.Forms.TextBox();
            this.txtSTART_NUMBER = new System.Windows.Forms.TextBox();
            this.chkDISPLAY_YEAR = new System.Windows.Forms.CheckBox();
            this.lblSAMPLE = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSAMPLE = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            resources.ApplyResources(this.content, "content");
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.txtSAMPLE);
            this.content.Controls.Add(this.label8);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.lblSAMPLE);
            this.content.Controls.Add(this.chkDISPLAY_YEAR);
            this.content.Controls.Add(this.txtSTART_NUMBER);
            this.content.Controls.Add(this.txtDIGIIT);
            this.content.Controls.Add(this.txtHEADER);
            this.content.Controls.Add(this.lblDIGIT);
            this.content.Controls.Add(this.lblSTART);
            this.content.Controls.Add(this.lblHEADER);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblHEADER, 0);
            this.content.Controls.SetChildIndex(this.lblSTART, 0);
            this.content.Controls.SetChildIndex(this.lblDIGIT, 0);
            this.content.Controls.SetChildIndex(this.txtHEADER, 0);
            this.content.Controls.SetChildIndex(this.txtDIGIIT, 0);
            this.content.Controls.SetChildIndex(this.txtSTART_NUMBER, 0);
            this.content.Controls.SetChildIndex(this.chkDISPLAY_YEAR, 0);
            this.content.Controls.SetChildIndex(this.lblSAMPLE, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.label8, 0);
            this.content.Controls.SetChildIndex(this.txtSAMPLE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblHEADER
            // 
            resources.ApplyResources(this.lblHEADER, "lblHEADER");
            this.lblHEADER.Name = "lblHEADER";
            // 
            // lblSTART
            // 
            resources.ApplyResources(this.lblSTART, "lblSTART");
            this.lblSTART.Name = "lblSTART";
            // 
            // lblDIGIT
            // 
            resources.ApplyResources(this.lblDIGIT, "lblDIGIT");
            this.lblDIGIT.Name = "lblDIGIT";
            // 
            // txtHEADER
            // 
            resources.ApplyResources(this.txtHEADER, "txtHEADER");
            this.txtHEADER.Name = "txtHEADER";
            this.txtHEADER.TextChanged += new System.EventHandler(this.txtHEADER_TextChanged);
            this.txtHEADER.Enter += new System.EventHandler(this.txtSAMPLE_Enter);
            // 
            // txtDIGIIT
            // 
            resources.ApplyResources(this.txtDIGIIT, "txtDIGIIT");
            this.txtDIGIIT.Name = "txtDIGIIT";
            this.txtDIGIIT.TextChanged += new System.EventHandler(this.txtDIGIIT_TextChanged);
            this.txtDIGIIT.Enter += new System.EventHandler(this.txtSAMPLE_Enter);
            // 
            // txtSTART_NUMBER
            // 
            resources.ApplyResources(this.txtSTART_NUMBER, "txtSTART_NUMBER");
            this.txtSTART_NUMBER.Name = "txtSTART_NUMBER";
            this.txtSTART_NUMBER.TextChanged += new System.EventHandler(this.txtSTART_TextChanged);
            this.txtSTART_NUMBER.Enter += new System.EventHandler(this.txtSAMPLE_Enter);
            // 
            // chkDISPLAY_YEAR
            // 
            resources.ApplyResources(this.chkDISPLAY_YEAR, "chkDISPLAY_YEAR");
            this.chkDISPLAY_YEAR.Name = "chkDISPLAY_YEAR";
            this.chkDISPLAY_YEAR.UseVisualStyleBackColor = true;
            this.chkDISPLAY_YEAR.CheckedChanged += new System.EventHandler(this.chkSHOWYEAR_CheckedChanged);
            this.chkDISPLAY_YEAR.Enter += new System.EventHandler(this.txtSAMPLE_Enter);
            // 
            // lblSAMPLE
            // 
            resources.ApplyResources(this.lblSAMPLE, "lblSAMPLE");
            this.lblSAMPLE.Name = "lblSAMPLE";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // txtSAMPLE
            // 
            resources.ApplyResources(this.txtSAMPLE, "txtSAMPLE");
            this.txtSAMPLE.BackColor = System.Drawing.Color.LightYellow;
            this.txtSAMPLE.Name = "txtSAMPLE";
            this.txtSAMPLE.ReadOnly = true;
            this.txtSAMPLE.Enter += new System.EventHandler(this.txtSAMPLE_Enter);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Name = "panel1";
            // 
            // DialogUtilityFormat
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogUtilityFormat";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnOK;
        private TextBox txtSTART_NUMBER;
        private TextBox txtDIGIIT;
        private TextBox txtHEADER;
        private Label lblDIGIT;
        private Label lblSTART;
        private Label lblHEADER;
        private ExButton btnCLOSE;
        private TextBox txtSAMPLE;
        private Label label8;
        private Label label7;
        private Label label6;
        private Label label5;
        private Label lblSAMPLE;
        private CheckBox chkDISPLAY_YEAR;
        private Panel panel1;
    }
}