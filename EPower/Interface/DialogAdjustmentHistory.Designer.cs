﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogAdjustmentHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogAdjustmentHistory));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.ADJUST_INVOICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BEFORE_ADJUST_USAGE_I = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BEFORE_ADJUST_AMOUNT_I = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADJUST_USAGE_III = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADJUST_AMOUNT_I = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADJUST_REASON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FORMAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToOrderColumns = true;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ADJUST_INVOICE_ID,
            this.INVOICE_ID,
            this.CREATE_ON,
            this.METER_CODE,
            this.BEFORE_ADJUST_USAGE_I,
            this.BEFORE_ADJUST_AMOUNT_I,
            this.ADJUST_USAGE_III,
            this.ADJUST_AMOUNT_I,
            this.CURRENCY_SING_,
            this.ADJUST_REASON,
            this.CREATE_BY,
            this.FORMAT});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_CellFormatting);
            // 
            // ADJUST_INVOICE_ID
            // 
            this.ADJUST_INVOICE_ID.DataPropertyName = "ADJUST_INVOICE_ID";
            resources.ApplyResources(this.ADJUST_INVOICE_ID, "ADJUST_INVOICE_ID");
            this.ADJUST_INVOICE_ID.Name = "ADJUST_INVOICE_ID";
            this.ADJUST_INVOICE_ID.ReadOnly = true;
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.DataPropertyName = "INVOICE_ID";
            resources.ApplyResources(this.INVOICE_ID, "INVOICE_ID");
            this.INVOICE_ID.Name = "INVOICE_ID";
            this.INVOICE_ID.ReadOnly = true;
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "dd-MM-yyyy hh:mm tt";
            this.CREATE_ON.DefaultCellStyle = dataGridViewCellStyle2;
            this.CREATE_ON.FillWeight = 137.0558F;
            resources.ApplyResources(this.CREATE_ON, "CREATE_ON");
            this.CREATE_ON.Name = "CREATE_ON";
            this.CREATE_ON.ReadOnly = true;
            // 
            // METER_CODE
            // 
            this.METER_CODE.DataPropertyName = "METER_CODE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.METER_CODE.DefaultCellStyle = dataGridViewCellStyle3;
            this.METER_CODE.FillWeight = 103.3359F;
            resources.ApplyResources(this.METER_CODE, "METER_CODE");
            this.METER_CODE.Name = "METER_CODE";
            this.METER_CODE.ReadOnly = true;
            // 
            // BEFORE_ADJUST_USAGE_I
            // 
            this.BEFORE_ADJUST_USAGE_I.DataPropertyName = "BEFORE_ADJUST_USAGE";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "N0";
            dataGridViewCellStyle4.NullValue = null;
            this.BEFORE_ADJUST_USAGE_I.DefaultCellStyle = dataGridViewCellStyle4;
            this.BEFORE_ADJUST_USAGE_I.FillWeight = 96.07079F;
            resources.ApplyResources(this.BEFORE_ADJUST_USAGE_I, "BEFORE_ADJUST_USAGE_I");
            this.BEFORE_ADJUST_USAGE_I.Name = "BEFORE_ADJUST_USAGE_I";
            this.BEFORE_ADJUST_USAGE_I.ReadOnly = true;
            // 
            // BEFORE_ADJUST_AMOUNT_I
            // 
            this.BEFORE_ADJUST_AMOUNT_I.DataPropertyName = "BEFORE_ADJUST_AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BEFORE_ADJUST_AMOUNT_I.DefaultCellStyle = dataGridViewCellStyle5;
            this.BEFORE_ADJUST_AMOUNT_I.FillWeight = 103.6179F;
            resources.ApplyResources(this.BEFORE_ADJUST_AMOUNT_I, "BEFORE_ADJUST_AMOUNT_I");
            this.BEFORE_ADJUST_AMOUNT_I.Name = "BEFORE_ADJUST_AMOUNT_I";
            this.BEFORE_ADJUST_AMOUNT_I.ReadOnly = true;
            // 
            // ADJUST_USAGE_III
            // 
            this.ADJUST_USAGE_III.DataPropertyName = "ADJUST_USAGE";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.Format = "N0";
            this.ADJUST_USAGE_III.DefaultCellStyle = dataGridViewCellStyle6;
            this.ADJUST_USAGE_III.FillWeight = 77.81057F;
            resources.ApplyResources(this.ADJUST_USAGE_III, "ADJUST_USAGE_III");
            this.ADJUST_USAGE_III.Name = "ADJUST_USAGE_III";
            this.ADJUST_USAGE_III.ReadOnly = true;
            // 
            // ADJUST_AMOUNT_I
            // 
            this.ADJUST_AMOUNT_I.DataPropertyName = "ADJUST_AMOUNT";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ADJUST_AMOUNT_I.DefaultCellStyle = dataGridViewCellStyle7;
            this.ADJUST_AMOUNT_I.FillWeight = 104.2302F;
            resources.ApplyResources(this.ADJUST_AMOUNT_I, "ADJUST_AMOUNT_I");
            this.ADJUST_AMOUNT_I.Name = "ADJUST_AMOUNT_I";
            this.ADJUST_AMOUNT_I.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CURRENCY_SING_.DefaultCellStyle = dataGridViewCellStyle8;
            this.CURRENCY_SING_.FillWeight = 22.17691F;
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // ADJUST_REASON
            // 
            this.ADJUST_REASON.DataPropertyName = "ADJUST_REASON";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.ADJUST_REASON.DefaultCellStyle = dataGridViewCellStyle9;
            this.ADJUST_REASON.FillWeight = 166.4769F;
            resources.ApplyResources(this.ADJUST_REASON, "ADJUST_REASON");
            this.ADJUST_REASON.Name = "ADJUST_REASON";
            this.ADJUST_REASON.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.CREATE_BY.DefaultCellStyle = dataGridViewCellStyle10;
            this.CREATE_BY.FillWeight = 89.22507F;
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // FORMAT
            // 
            this.FORMAT.DataPropertyName = "FORMAT";
            resources.ApplyResources(this.FORMAT, "FORMAT");
            this.FORMAT.Name = "FORMAT";
            this.FORMAT.ReadOnly = true;
            // 
            // DialogAdjustmentHistory
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogAdjustmentHistory";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel1;
        private DataGridView dgv;
        private DataGridViewTextBoxColumn ADJUST_INVOICE_ID;
        private DataGridViewTextBoxColumn INVOICE_ID;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn METER_CODE;
        private DataGridViewTextBoxColumn BEFORE_ADJUST_USAGE_I;
        private DataGridViewTextBoxColumn BEFORE_ADJUST_AMOUNT_I;
        private DataGridViewTextBoxColumn ADJUST_USAGE_III;
        private DataGridViewTextBoxColumn ADJUST_AMOUNT_I;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private DataGridViewTextBoxColumn ADJUST_REASON;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn FORMAT;
    }
}