﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogDownloadUsage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogDownloadUsage));
            this.btnRECEIVE = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblCUSTOMER_USAGE = new System.Windows.Forms.Label();
            this.txtHaveUsageCustomer = new System.Windows.Forms.TextBox();
            this.lblMETER_UNKNOWN = new System.Windows.Forms.Label();
            this.btnCHECK = new SoftTech.Component.ExButton();
            this.txtUnknownMeter = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCHECK_1 = new SoftTech.Component.ExButton();
            this.txtUnRegister = new System.Windows.Forms.TextBox();
            this.lblMETER_UNREGISTER = new System.Windows.Forms.Label();
            this.btnCUSTOMER_NO_USAGE = new SoftTech.Component.ExLinkLabel(this.components);
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnCUSTOMER_NO_USAGE);
            this.content.Controls.Add(this.btnCHECK_1);
            this.content.Controls.Add(this.txtUnRegister);
            this.content.Controls.Add(this.lblMETER_UNREGISTER);
            this.content.Controls.Add(this.panel2);
            this.content.Controls.Add(this.btnCHECK);
            this.content.Controls.Add(this.txtUnknownMeter);
            this.content.Controls.Add(this.lblMETER_UNKNOWN);
            this.content.Controls.Add(this.txtHaveUsageCustomer);
            this.content.Controls.Add(this.lblCUSTOMER_USAGE);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnRECEIVE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnRECEIVE, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtHaveUsageCustomer, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_UNKNOWN, 0);
            this.content.Controls.SetChildIndex(this.txtUnknownMeter, 0);
            this.content.Controls.SetChildIndex(this.btnCHECK, 0);
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_UNREGISTER, 0);
            this.content.Controls.SetChildIndex(this.txtUnRegister, 0);
            this.content.Controls.SetChildIndex(this.btnCHECK_1, 0);
            this.content.Controls.SetChildIndex(this.btnCUSTOMER_NO_USAGE, 0);
            // 
            // btnRECEIVE
            // 
            resources.ApplyResources(this.btnRECEIVE, "btnRECEIVE");
            this.btnRECEIVE.Name = "btnRECEIVE";
            this.btnRECEIVE.UseVisualStyleBackColor = true;
            this.btnRECEIVE.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblCUSTOMER_USAGE
            // 
            resources.ApplyResources(this.lblCUSTOMER_USAGE, "lblCUSTOMER_USAGE");
            this.lblCUSTOMER_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.lblCUSTOMER_USAGE.Name = "lblCUSTOMER_USAGE";
            // 
            // txtHaveUsageCustomer
            // 
            resources.ApplyResources(this.txtHaveUsageCustomer, "txtHaveUsageCustomer");
            this.txtHaveUsageCustomer.Name = "txtHaveUsageCustomer";
            this.txtHaveUsageCustomer.ReadOnly = true;
            // 
            // lblMETER_UNKNOWN
            // 
            resources.ApplyResources(this.lblMETER_UNKNOWN, "lblMETER_UNKNOWN");
            this.lblMETER_UNKNOWN.BackColor = System.Drawing.Color.Transparent;
            this.lblMETER_UNKNOWN.Name = "lblMETER_UNKNOWN";
            // 
            // btnCHECK
            // 
            resources.ApplyResources(this.btnCHECK, "btnCHECK");
            this.btnCHECK.Name = "btnCHECK";
            this.btnCHECK.UseVisualStyleBackColor = true;
            this.btnCHECK.Click += new System.EventHandler(this.btnCheckUnknown_Click);
            // 
            // txtUnknownMeter
            // 
            resources.ApplyResources(this.txtUnknownMeter, "txtUnknownMeter");
            this.txtUnknownMeter.Name = "txtUnknownMeter";
            this.txtUnknownMeter.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnCHECK_1
            // 
            resources.ApplyResources(this.btnCHECK_1, "btnCHECK_1");
            this.btnCHECK_1.Name = "btnCHECK_1";
            this.btnCHECK_1.UseVisualStyleBackColor = true;
            this.btnCHECK_1.Click += new System.EventHandler(this.btnCheckUnregister_Click);
            // 
            // txtUnRegister
            // 
            resources.ApplyResources(this.txtUnRegister, "txtUnRegister");
            this.txtUnRegister.Name = "txtUnRegister";
            this.txtUnRegister.ReadOnly = true;
            // 
            // lblMETER_UNREGISTER
            // 
            resources.ApplyResources(this.lblMETER_UNREGISTER, "lblMETER_UNREGISTER");
            this.lblMETER_UNREGISTER.BackColor = System.Drawing.Color.Transparent;
            this.lblMETER_UNREGISTER.Name = "lblMETER_UNREGISTER";
            // 
            // btnCUSTOMER_NO_USAGE
            // 
            resources.ApplyResources(this.btnCUSTOMER_NO_USAGE, "btnCUSTOMER_NO_USAGE");
            this.btnCUSTOMER_NO_USAGE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCUSTOMER_NO_USAGE.Name = "btnCUSTOMER_NO_USAGE";
            this.btnCUSTOMER_NO_USAGE.TabStop = true;
            this.btnCUSTOMER_NO_USAGE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnViewNotCollectCustomer_LinkClicked);
            // 
            // DialogDownloadUsage
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogDownloadUsage";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnRECEIVE;
        private ExButton btnCLOSE;
        private Label lblCUSTOMER_USAGE;
        private TextBox txtHaveUsageCustomer;
        private ExButton btnCHECK;
        private TextBox txtUnknownMeter;
        private Label lblMETER_UNKNOWN;
        private Panel panel2;
        private ExButton btnCHECK_1;
        private TextBox txtUnRegister;
        private Label lblMETER_UNREGISTER;
        private ExLinkLabel btnCUSTOMER_NO_USAGE;
    }
}
