﻿namespace EPower
{
    partial class DialogChangeConnectionTypeHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvHistory = new System.Windows.Forms.DataGridView();
            this.OLD_CONNECTION_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NEW_CUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CHANGE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblCUSTOMER_CHANGE_AMPARE_HISTORY = new System.Windows.Forms.Label();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.txtCustomerGroup = new System.Windows.Forms.TextBox();
            this.txtCustomerConnection = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_CONNECTION_TYPE = new System.Windows.Forms.Label();
            this.lblCHANGE_CONNECTION_TYPE_HISTORY = new System.Windows.Forms.Label();
            this.lblCUSTOMER_INFORMATION = new System.Windows.Forms.Label();
            this.txtCustomerCode = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.lblCUSTOMER_TYPE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtCustomerGroup);
            this.content.Controls.Add(this.txtCustomerConnection);
            this.content.Controls.Add(this.lblCUSTOMER_CONNECTION_TYPE);
            this.content.Controls.Add(this.lblCHANGE_CONNECTION_TYPE_HISTORY);
            this.content.Controls.Add(this.lblCUSTOMER_INFORMATION);
            this.content.Controls.Add(this.txtCustomerCode);
            this.content.Controls.Add(this.txtCustomerName);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_TYPE);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.dgvHistory);
            this.content.Controls.Add(this.lblCUSTOMER_CHANGE_AMPARE_HISTORY);
            this.content.Size = new System.Drawing.Size(780, 577);
            this.content.Text = "CONTENT";
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CHANGE_AMPARE_HISTORY, 0);
            this.content.Controls.SetChildIndex(this.dgvHistory, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerName, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCode, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblCHANGE_CONNECTION_TYPE_HISTORY, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CONNECTION_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerConnection, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerGroup, 0);
            // 
            // dgvHistory
            // 
            this.dgvHistory.AllowUserToAddRows = false;
            this.dgvHistory.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.dgvHistory.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvHistory.BackgroundColor = System.Drawing.Color.White;
            this.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvHistory.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OLD_CONNECTION_TYPE,
            this.NEW_CUSTOMER_CONNECTION_TYPE,
            this.CREATE_BY,
            this.CHANGE_DATE,
            this.NOTE});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHistory.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvHistory.EnableHeadersVisualStyles = false;
            this.dgvHistory.Location = new System.Drawing.Point(1, 138);
            this.dgvHistory.Name = "dgvHistory";
            this.dgvHistory.ReadOnly = true;
            this.dgvHistory.RowHeadersVisible = false;
            this.dgvHistory.RowTemplate.Height = 30;
            this.dgvHistory.RowTemplate.ReadOnly = true;
            this.dgvHistory.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvHistory.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvHistory.Size = new System.Drawing.Size(779, 401);
            this.dgvHistory.TabIndex = 342;
            // 
            // OLD_CONNECTION_TYPE
            // 
            this.OLD_CONNECTION_TYPE.DataPropertyName = "OLD_CONNECTION_TYPE";
            this.OLD_CONNECTION_TYPE.HeaderText = "ការភ្ជាប់ចរន្តចាស់";
            this.OLD_CONNECTION_TYPE.Name = "OLD_CONNECTION_TYPE";
            this.OLD_CONNECTION_TYPE.ReadOnly = true;
            this.OLD_CONNECTION_TYPE.Width = 200;
            // 
            // NEW_CUSTOMER_CONNECTION_TYPE
            // 
            this.NEW_CUSTOMER_CONNECTION_TYPE.DataPropertyName = "NEW_CUSTOMER_CONNECTION_TYPE";
            this.NEW_CUSTOMER_CONNECTION_TYPE.HeaderText = "ការភ្ជាប់ចរន្តថ្មី";
            this.NEW_CUSTOMER_CONNECTION_TYPE.Name = "NEW_CUSTOMER_CONNECTION_TYPE";
            this.NEW_CUSTOMER_CONNECTION_TYPE.ReadOnly = true;
            this.NEW_CUSTOMER_CONNECTION_TYPE.Width = 200;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            this.CREATE_BY.HeaderText = "ដោយ";
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // CHANGE_DATE
            // 
            this.CHANGE_DATE.DataPropertyName = "CHANGE_DATE";
            dataGridViewCellStyle2.Format = "dd-MM-yyyy hh:mm:tt";
            dataGridViewCellStyle2.NullValue = null;
            this.CHANGE_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            this.CHANGE_DATE.HeaderText = "ថ្ងៃកែប្រែ";
            this.CHANGE_DATE.Name = "CHANGE_DATE";
            this.CHANGE_DATE.ReadOnly = true;
            this.CHANGE_DATE.Width = 150;
            // 
            // NOTE
            // 
            this.NOTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NOTE.DataPropertyName = "NOTE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.NOTE.DefaultCellStyle = dataGridViewCellStyle3;
            this.NOTE.HeaderText = "សម្គាល់";
            this.NOTE.Name = "NOTE";
            this.NOTE.ReadOnly = true;
            // 
            // lblCUSTOMER_CHANGE_AMPARE_HISTORY
            // 
            this.lblCUSTOMER_CHANGE_AMPARE_HISTORY.AutoSize = true;
            this.lblCUSTOMER_CHANGE_AMPARE_HISTORY.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCUSTOMER_CHANGE_AMPARE_HISTORY.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCUSTOMER_CHANGE_AMPARE_HISTORY.Location = new System.Drawing.Point(9, 165);
            this.lblCUSTOMER_CHANGE_AMPARE_HISTORY.Margin = new System.Windows.Forms.Padding(2);
            this.lblCUSTOMER_CHANGE_AMPARE_HISTORY.Name = "lblCUSTOMER_CHANGE_AMPARE_HISTORY";
            this.lblCUSTOMER_CHANGE_AMPARE_HISTORY.Size = new System.Drawing.Size(262, 25);
            this.lblCUSTOMER_CHANGE_AMPARE_HISTORY.TabIndex = 341;
            this.lblCUSTOMER_CHANGE_AMPARE_HISTORY.Text = "ប្រវត្តិកែប្រែប្រភេទភ្ជាប់របស់អតិថិជន";
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(694, 546);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(74, 23);
            this.btnCLOSE.TabIndex = 352;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnCLOSE_Click);
            // 
            // txtCustomerGroup
            // 
            this.txtCustomerGroup.Enabled = false;
            this.txtCustomerGroup.Location = new System.Drawing.Point(123, 60);
            this.txtCustomerGroup.Margin = new System.Windows.Forms.Padding(2);
            this.txtCustomerGroup.Name = "txtCustomerGroup";
            this.txtCustomerGroup.ReadOnly = true;
            this.txtCustomerGroup.Size = new System.Drawing.Size(213, 32);
            this.txtCustomerGroup.TabIndex = 355;
            // 
            // txtCustomerConnection
            // 
            this.txtCustomerConnection.Enabled = false;
            this.txtCustomerConnection.Location = new System.Drawing.Point(496, 60);
            this.txtCustomerConnection.Margin = new System.Windows.Forms.Padding(2);
            this.txtCustomerConnection.Name = "txtCustomerConnection";
            this.txtCustomerConnection.ReadOnly = true;
            this.txtCustomerConnection.Size = new System.Drawing.Size(213, 32);
            this.txtCustomerConnection.TabIndex = 356;
            // 
            // lblCUSTOMER_CONNECTION_TYPE
            // 
            this.lblCUSTOMER_CONNECTION_TYPE.AutoSize = true;
            this.lblCUSTOMER_CONNECTION_TYPE.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCUSTOMER_CONNECTION_TYPE.Location = new System.Drawing.Point(386, 64);
            this.lblCUSTOMER_CONNECTION_TYPE.Margin = new System.Windows.Forms.Padding(2);
            this.lblCUSTOMER_CONNECTION_TYPE.Name = "lblCUSTOMER_CONNECTION_TYPE";
            this.lblCUSTOMER_CONNECTION_TYPE.Size = new System.Drawing.Size(87, 25);
            this.lblCUSTOMER_CONNECTION_TYPE.TabIndex = 362;
            this.lblCUSTOMER_CONNECTION_TYPE.Text = "ការភ្ជាប់ចរន្ត";
            // 
            // lblCHANGE_CONNECTION_TYPE_HISTORY
            // 
            this.lblCHANGE_CONNECTION_TYPE_HISTORY.AutoSize = true;
            this.lblCHANGE_CONNECTION_TYPE_HISTORY.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCHANGE_CONNECTION_TYPE_HISTORY.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCHANGE_CONNECTION_TYPE_HISTORY.Location = new System.Drawing.Point(15, 110);
            this.lblCHANGE_CONNECTION_TYPE_HISTORY.Margin = new System.Windows.Forms.Padding(2);
            this.lblCHANGE_CONNECTION_TYPE_HISTORY.Name = "lblCHANGE_CONNECTION_TYPE_HISTORY";
            this.lblCHANGE_CONNECTION_TYPE_HISTORY.Size = new System.Drawing.Size(267, 25);
            this.lblCHANGE_CONNECTION_TYPE_HISTORY.TabIndex = 361;
            this.lblCHANGE_CONNECTION_TYPE_HISTORY.Text = "ប្រវត្តិកែប្រែការភ្ជាប់ចរន្តរបស់អតិថិជន";
            // 
            // lblCUSTOMER_INFORMATION
            // 
            this.lblCUSTOMER_INFORMATION.AutoSize = true;
            this.lblCUSTOMER_INFORMATION.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCUSTOMER_INFORMATION.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCUSTOMER_INFORMATION.Location = new System.Drawing.Point(15, 5);
            this.lblCUSTOMER_INFORMATION.Margin = new System.Windows.Forms.Padding(2);
            this.lblCUSTOMER_INFORMATION.Name = "lblCUSTOMER_INFORMATION";
            this.lblCUSTOMER_INFORMATION.Size = new System.Drawing.Size(125, 25);
            this.lblCUSTOMER_INFORMATION.TabIndex = 360;
            this.lblCUSTOMER_INFORMATION.Text = "ព័ត៌មានអតិថិជន";
            // 
            // txtCustomerCode
            // 
            this.txtCustomerCode.Enabled = false;
            this.txtCustomerCode.Location = new System.Drawing.Point(123, 29);
            this.txtCustomerCode.Margin = new System.Windows.Forms.Padding(2);
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.ReadOnly = true;
            this.txtCustomerCode.Size = new System.Drawing.Size(213, 32);
            this.txtCustomerCode.TabIndex = 353;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Enabled = false;
            this.txtCustomerName.Location = new System.Drawing.Point(496, 29);
            this.txtCustomerName.Margin = new System.Windows.Forms.Padding(2);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.ReadOnly = true;
            this.txtCustomerName.Size = new System.Drawing.Size(213, 32);
            this.txtCustomerName.TabIndex = 354;
            // 
            // lblCUSTOMER_NAME
            // 
            this.lblCUSTOMER_NAME.AutoSize = true;
            this.lblCUSTOMER_NAME.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCUSTOMER_NAME.Location = new System.Drawing.Point(386, 33);
            this.lblCUSTOMER_NAME.Margin = new System.Windows.Forms.Padding(2);
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            this.lblCUSTOMER_NAME.Size = new System.Drawing.Size(105, 25);
            this.lblCUSTOMER_NAME.TabIndex = 359;
            this.lblCUSTOMER_NAME.Text = "ឈ្មោះអតិថិជន";
            // 
            // lblCUSTOMER_TYPE
            // 
            this.lblCUSTOMER_TYPE.AutoSize = true;
            this.lblCUSTOMER_TYPE.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCUSTOMER_TYPE.Location = new System.Drawing.Point(15, 64);
            this.lblCUSTOMER_TYPE.Margin = new System.Windows.Forms.Padding(2);
            this.lblCUSTOMER_TYPE.Name = "lblCUSTOMER_TYPE";
            this.lblCUSTOMER_TYPE.Size = new System.Drawing.Size(110, 25);
            this.lblCUSTOMER_TYPE.TabIndex = 358;
            this.lblCUSTOMER_TYPE.Text = "ប្រភេទអតិថិជន";
            // 
            // lblCUSTOMER_CODE
            // 
            this.lblCUSTOMER_CODE.AutoSize = true;
            this.lblCUSTOMER_CODE.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCUSTOMER_CODE.Location = new System.Drawing.Point(15, 33);
            this.lblCUSTOMER_CODE.Margin = new System.Windows.Forms.Padding(2);
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            this.lblCUSTOMER_CODE.Size = new System.Drawing.Size(121, 25);
            this.lblCUSTOMER_CODE.TabIndex = 357;
            this.lblCUSTOMER_CODE.Text = "លេខកូដអតិថជន";
            // 
            // DialogChangeConnectionTypeHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 600);
            this.Name = "DialogChangeConnectionTypeHistory";
            this.Text = "ប្រវត្តិកែប្រែការភ្ជាប់ចរន្តរបស់អតិថិជន";
            this.Load += new System.EventHandler(this.DialogChangeConnectionTypeHistory_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHistory)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvHistory;
        private System.Windows.Forms.Label lblCUSTOMER_CHANGE_AMPARE_HISTORY;
        private SoftTech.Component.ExButton btnCLOSE;
        private System.Windows.Forms.TextBox txtCustomerGroup;
        private System.Windows.Forms.TextBox txtCustomerConnection;
        private System.Windows.Forms.Label lblCUSTOMER_CONNECTION_TYPE;
        private System.Windows.Forms.Label lblCHANGE_CONNECTION_TYPE_HISTORY;
        private System.Windows.Forms.Label lblCUSTOMER_INFORMATION;
        private System.Windows.Forms.TextBox txtCustomerCode;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.Label lblCUSTOMER_NAME;
        private System.Windows.Forms.Label lblCUSTOMER_TYPE;
        private System.Windows.Forms.Label lblCUSTOMER_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn OLD_CONNECTION_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn NEW_CUSTOMER_CONNECTION_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CREATE_BY;
        private System.Windows.Forms.DataGridViewTextBoxColumn CHANGE_DATE;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOTE;
    }
}