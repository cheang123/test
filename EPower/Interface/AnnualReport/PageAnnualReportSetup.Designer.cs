﻿using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface.AnnualReport
{
    partial class PageAnnualReportSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblANNUAL_REPORT = new System.Windows.Forms.Label();
            this.lblYEAR = new System.Windows.Forms.Label();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblANNUAL_REPORT
            // 
            this.lblANNUAL_REPORT.AutoSize = true;
            this.lblANNUAL_REPORT.Font = new System.Drawing.Font("Khmer OS Muol Light", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblANNUAL_REPORT.Location = new System.Drawing.Point(126, 110);
            this.lblANNUAL_REPORT.Name = "lblANNUAL_REPORT";
            this.lblANNUAL_REPORT.Size = new System.Drawing.Size(297, 49);
            this.lblANNUAL_REPORT.TabIndex = 0;
            this.lblANNUAL_REPORT.Text = "របាយការណ៍ប្រចាំឆ្នាំ";
            // 
            // lblYEAR
            // 
            this.lblYEAR.AutoSize = true;
            this.lblYEAR.Location = new System.Drawing.Point(131, 167);
            this.lblYEAR.Margin = new System.Windows.Forms.Padding(2);
            this.lblYEAR.Name = "lblYEAR";
            this.lblYEAR.Size = new System.Drawing.Size(45, 19);
            this.lblYEAR.TabIndex = 1;
            this.lblYEAR.Text = "ប្រចាំឆ្នាំ";
            // 
            // cboYear
            // 
            this.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Location = new System.Drawing.Point(259, 163);
            this.cboYear.Margin = new System.Windows.Forms.Padding(2);
            this.cboYear.Name = "cboYear";
            this.cboYear.Size = new System.Drawing.Size(153, 27);
            this.cboYear.TabIndex = 2;
            this.cboYear.SelectedIndexChanged += new System.EventHandler(this.cboYear_SelectedIndexChanged);
            // 
            // lblCURRENCY
            // 
            this.lblCURRENCY.AutoSize = true;
            this.lblCURRENCY.Location = new System.Drawing.Point(131, 198);
            this.lblCURRENCY.Margin = new System.Windows.Forms.Padding(2);
            this.lblCURRENCY.Name = "lblCURRENCY";
            this.lblCURRENCY.Size = new System.Drawing.Size(102, 19);
            this.lblCURRENCY.TabIndex = 3;
            this.lblCURRENCY.Text = "បង្ហាញជារូបិយប័ណ្ណ";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.Location = new System.Drawing.Point(259, 194);
            this.cboCurrency.Margin = new System.Windows.Forms.Padding(2);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(153, 27);
            this.cboCurrency.TabIndex = 6;
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // PageAnnualReportSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 423);
            this.Controls.Add(this.cboCurrency);
            this.Controls.Add(this.lblCURRENCY);
            this.Controls.Add(this.cboYear);
            this.Controls.Add(this.lblYEAR);
            this.Controls.Add(this.lblANNUAL_REPORT);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PageAnnualReportSetup";
            this.Text = "PageReportSetup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label lblANNUAL_REPORT;
        private Label lblYEAR;
        private ComboBox cboYear;
        private Label lblCURRENCY;
        private ComboBox cboCurrency;
    }
}