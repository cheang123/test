﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;

namespace EPower.Interface.AnnualReport
{
    public partial class PageAnnualReportRun_ : Form
    {
        private string tableCode = "AS99";
        public PageAnnualReportRun_()
        {
            InitializeComponent();
        }  
        public void Update(string tableCode ){
            this.tableCode = tableCode;
            this.lblTable_.Text = (tableCode=="AS99"? "គ្រប់តារាង" : tableCode);
            this.lblCurrency_.Text = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == PageAnnualReportGroup.CURRENCY_ID).CURRENCY_NAME;
            this.lblYear.Text = DBDataContext.Db.TBL_YEARs.FirstOrDefault(x => x.YEAR_ID == PageAnnualReportGroup.YEAR_ID).YEAR_NAME;
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            var tables = DBDataContext.Db.TBL_TABLEs.Where(t => t.IS_ACTIVE && (tableCode=="AS99" || t.TABLE_CODE==tableCode) );
            var result = false;

            Runner.RunNewThread(delegate(){

                foreach (var table in tables)
                {
                    Runner.Instance.Text = string.Format("កំពង់ដំណើរការ {0}...", table.TABLE_CODE);
                    Application.DoEvents();

                    DBDataContext.Db.ExecuteCommand("EXEC ANR.RUN_" + table.TABLE_CODE.Replace("AS", "AS_") + " @YEAR_ID=@p0,@CURRENCY_ID=@p1", PageAnnualReportGroup.YEAR_ID, PageAnnualReportGroup.CURRENCY_ID);
                }

                // summary result
                DBDataContext.Db.ExecuteCommand("EXEC ANR.RUN_AS_09 @YEAR_ID=@p0,@CURRENCY_ID=@p1", PageAnnualReportGroup.YEAR_ID, PageAnnualReportGroup.CURRENCY_ID);
                DBDataContext.Db.ExecuteCommand("EXEC ANR.RUN_AS_10 @YEAR_ID=@p0,@CURRENCY_ID=@p1", PageAnnualReportGroup.YEAR_ID, PageAnnualReportGroup.CURRENCY_ID);
                DBDataContext.Db.ExecuteCommand("EXEC ANR.RUN_AS_00B @YEAR_ID=@p0,@CURRENCY_ID=@p1", PageAnnualReportGroup.YEAR_ID, PageAnnualReportGroup.CURRENCY_ID);

                result = true;
            });

            if (result == true)
            {
                MsgBox.ShowInformation("ដំណើរការបញ្ចប់ដោយជោគជ័យ!");
            }
        }
         
 
    }
}
