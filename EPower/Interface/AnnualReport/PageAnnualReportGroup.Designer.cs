﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.AnnualReport
{
    partial class PageAnnualReportGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.lblANNUAL_REPORT = new System.Windows.Forms.Label();
            this.btnRUN_ALL_BEFORE_VIEW = new SoftTech.Component.ExButton();
            this.btnPRINT_ALL = new SoftTech.Component.ExButton();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.lblSHOW = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.btnEXPORT_TO_EXCEL = new SoftTech.Component.ExButton();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.ItemSize = new System.Drawing.Size(40, 24);
            this.tabControl1.Location = new System.Drawing.Point(0, 37);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1047, 430);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage11
            // 
            this.tabPage11.Location = new System.Drawing.Point(4, 28);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(1039, 398);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "tabPage11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // tabPage12
            // 
            this.tabPage12.Location = new System.Drawing.Point(4, 28);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(1039, 398);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "X";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // lblANNUAL_REPORT
            // 
            this.lblANNUAL_REPORT.AutoSize = true;
            this.lblANNUAL_REPORT.Font = new System.Drawing.Font("Khmer OS Muol Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblANNUAL_REPORT.Location = new System.Drawing.Point(-2, 0);
            this.lblANNUAL_REPORT.Name = "lblANNUAL_REPORT";
            this.lblANNUAL_REPORT.Size = new System.Drawing.Size(210, 34);
            this.lblANNUAL_REPORT.TabIndex = 1;
            this.lblANNUAL_REPORT.Text = "របាយការណ៍ប្រចាំឆ្នាំ";
            // 
            // btnRUN_ALL_BEFORE_VIEW
            // 
            this.btnRUN_ALL_BEFORE_VIEW.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRUN_ALL_BEFORE_VIEW.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRUN_ALL_BEFORE_VIEW.Location = new System.Drawing.Point(804, 7);
            this.btnRUN_ALL_BEFORE_VIEW.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRUN_ALL_BEFORE_VIEW.Name = "btnRUN_ALL_BEFORE_VIEW";
            this.btnRUN_ALL_BEFORE_VIEW.Size = new System.Drawing.Size(123, 23);
            this.btnRUN_ALL_BEFORE_VIEW.TabIndex = 2;
            this.btnRUN_ALL_BEFORE_VIEW.Text = "តំណើរការទាំងអស់";
            this.btnRUN_ALL_BEFORE_VIEW.UseVisualStyleBackColor = true;
            this.btnRUN_ALL_BEFORE_VIEW.Click += new System.EventHandler(this.btnRunAll_Click);
            // 
            // btnPRINT_ALL
            // 
            this.btnPRINT_ALL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPRINT_ALL.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPRINT_ALL.Location = new System.Drawing.Point(932, 7);
            this.btnPRINT_ALL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPRINT_ALL.Name = "btnPRINT_ALL";
            this.btnPRINT_ALL.Size = new System.Drawing.Size(108, 23);
            this.btnPRINT_ALL.TabIndex = 3;
            this.btnPRINT_ALL.Text = "បោះពុម្ភទាំងអស់";
            this.btnPRINT_ALL.UseVisualStyleBackColor = true;
            this.btnPRINT_ALL.Click += new System.EventHandler(this.btnPrintAll_Click);
            // 
            // cboYear
            // 
            this.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Location = new System.Drawing.Point(214, 4);
            this.cboYear.Name = "cboYear";
            this.cboYear.Size = new System.Drawing.Size(94, 27);
            this.cboYear.TabIndex = 4;
            this.cboYear.SelectedIndexChanged += new System.EventHandler(this.cboYear_SelectedIndexChanged);
            // 
            // lblSHOW
            // 
            this.lblSHOW.AutoSize = true;
            this.lblSHOW.Location = new System.Drawing.Point(317, 8);
            this.lblSHOW.Name = "lblSHOW";
            this.lblSHOW.Size = new System.Drawing.Size(53, 19);
            this.lblSHOW.TabIndex = 5;
            this.lblSHOW.Text = "បង្ហាញជា";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            this.cboCurrency.Location = new System.Drawing.Point(374, 4);
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.Size = new System.Drawing.Size(79, 27);
            this.cboCurrency.TabIndex = 7;
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.cboCurrency_SelectedIndexChanged);
            // 
            // btnEXPORT_TO_EXCEL
            // 
            this.btnEXPORT_TO_EXCEL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEXPORT_TO_EXCEL.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEXPORT_TO_EXCEL.Location = new System.Drawing.Point(667, 8);
            this.btnEXPORT_TO_EXCEL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEXPORT_TO_EXCEL.Name = "btnEXPORT_TO_EXCEL";
            this.btnEXPORT_TO_EXCEL.Size = new System.Drawing.Size(131, 23);
            this.btnEXPORT_TO_EXCEL.TabIndex = 2;
            this.btnEXPORT_TO_EXCEL.Text = "ចំលង់ជាឯកសារ Excel";
            this.btnEXPORT_TO_EXCEL.UseVisualStyleBackColor = true;
            this.btnEXPORT_TO_EXCEL.Click += new System.EventHandler(this.btnExportToExcel_Click);
            // 
            // PageAnnualReportGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 467);
            this.Controls.Add(this.cboCurrency);
            this.Controls.Add(this.lblSHOW);
            this.Controls.Add(this.cboYear);
            this.Controls.Add(this.btnPRINT_ALL);
            this.Controls.Add(this.btnEXPORT_TO_EXCEL);
            this.Controls.Add(this.btnRUN_ALL_BEFORE_VIEW);
            this.Controls.Add(this.lblANNUAL_REPORT);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PageAnnualReportGroup";
            this.Text = "PageRunReport";
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage11;
        private TabPage tabPage12;
        private Label lblANNUAL_REPORT;
        private ExButton btnRUN_ALL_BEFORE_VIEW;
        private ExButton btnPRINT_ALL;
        private ComboBox cboYear;
        private Label lblSHOW;
        private ComboBox cboCurrency;
        private ExButton btnEXPORT_TO_EXCEL;

    }
}