﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.AnnualReport
{
    partial class PageBussinessPlan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblYEAR = new System.Windows.Forms.Label();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.lblANNUAL_PLANNING_IN_YEAR = new System.Windows.Forms.Label();
            this.lblTOTAL_INCOME = new System.Windows.Forms.Label();
            this.lblTOTAL_EXPENSE = new System.Windows.Forms.Label();
            this.lblPROFIT = new System.Windows.Forms.Label();
            this.lblANNUAL_PLANNING_NEXT_YEAR = new System.Windows.Forms.Label();
            this.lblTOTAL_INCOME_1 = new System.Windows.Forms.Label();
            this.lblTOTAL_EXPENSE_1 = new System.Windows.Forms.Label();
            this.lblREASON = new System.Windows.Forms.Label();
            this.lblANNUAL_ADJUSTMENT = new System.Windows.Forms.Label();
            this.txtThisYearReason = new System.Windows.Forms.TextBox();
            this.txtThisYearImprovement = new System.Windows.Forms.TextBox();
            this.txtThisYearIncome = new System.Windows.Forms.TextBox();
            this.txtThisYearExpense = new System.Windows.Forms.TextBox();
            this.txtThisYearProfit = new System.Windows.Forms.TextBox();
            this.txtNextYearIncome = new System.Windows.Forms.TextBox();
            this.txtNextYearExpense = new System.Windows.Forms.TextBox();
            this.btnSAVE = new SoftTech.Component.ExButton();
            this.lblNextYear_ = new System.Windows.Forms.Label();
            this.lblThisYear_ = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblYEAR
            // 
            this.lblYEAR.AutoSize = true;
            this.lblYEAR.Location = new System.Drawing.Point(16, 31);
            this.lblYEAR.Margin = new System.Windows.Forms.Padding(2);
            this.lblYEAR.Name = "lblYEAR";
            this.lblYEAR.Size = new System.Drawing.Size(45, 19);
            this.lblYEAR.TabIndex = 0;
            this.lblYEAR.Text = "ប្រចាំឆ្នាំ";
            // 
            // cboYear
            // 
            this.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Location = new System.Drawing.Point(108, 27);
            this.cboYear.Margin = new System.Windows.Forms.Padding(2);
            this.cboYear.Name = "cboYear";
            this.cboYear.Size = new System.Drawing.Size(162, 27);
            this.cboYear.TabIndex = 0;
            this.cboYear.SelectedIndexChanged += new System.EventHandler(this.cboYear_SelectedIndexChanged);
            // 
            // lblANNUAL_PLANNING_IN_YEAR
            // 
            this.lblANNUAL_PLANNING_IN_YEAR.AutoSize = true;
            this.lblANNUAL_PLANNING_IN_YEAR.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblANNUAL_PLANNING_IN_YEAR.Location = new System.Drawing.Point(16, 69);
            this.lblANNUAL_PLANNING_IN_YEAR.Margin = new System.Windows.Forms.Padding(2);
            this.lblANNUAL_PLANNING_IN_YEAR.Name = "lblANNUAL_PLANNING_IN_YEAR";
            this.lblANNUAL_PLANNING_IN_YEAR.Size = new System.Drawing.Size(135, 19);
            this.lblANNUAL_PLANNING_IN_YEAR.TabIndex = 2;
            this.lblANNUAL_PLANNING_IN_YEAR.Text = "គំរោងសម្រាប់ឆ្នាំបច្ចប្បន";
            // 
            // lblTOTAL_INCOME
            // 
            this.lblTOTAL_INCOME.AutoSize = true;
            this.lblTOTAL_INCOME.Location = new System.Drawing.Point(16, 96);
            this.lblTOTAL_INCOME.Margin = new System.Windows.Forms.Padding(2);
            this.lblTOTAL_INCOME.Name = "lblTOTAL_INCOME";
            this.lblTOTAL_INCOME.Size = new System.Drawing.Size(70, 19);
            this.lblTOTAL_INCOME.TabIndex = 3;
            this.lblTOTAL_INCOME.Text = "ចំណូលសរុប";
            // 
            // lblTOTAL_EXPENSE
            // 
            this.lblTOTAL_EXPENSE.AutoSize = true;
            this.lblTOTAL_EXPENSE.Location = new System.Drawing.Point(16, 127);
            this.lblTOTAL_EXPENSE.Margin = new System.Windows.Forms.Padding(2);
            this.lblTOTAL_EXPENSE.Name = "lblTOTAL_EXPENSE";
            this.lblTOTAL_EXPENSE.Size = new System.Drawing.Size(74, 19);
            this.lblTOTAL_EXPENSE.TabIndex = 4;
            this.lblTOTAL_EXPENSE.Text = "ចំណាយសរុប";
            // 
            // lblPROFIT
            // 
            this.lblPROFIT.AutoSize = true;
            this.lblPROFIT.Location = new System.Drawing.Point(16, 158);
            this.lblPROFIT.Margin = new System.Windows.Forms.Padding(2);
            this.lblPROFIT.Name = "lblPROFIT";
            this.lblPROFIT.Size = new System.Drawing.Size(74, 19);
            this.lblPROFIT.TabIndex = 5;
            this.lblPROFIT.Text = "ប្រាក់ចំណេញ";
            // 
            // lblANNUAL_PLANNING_NEXT_YEAR
            // 
            this.lblANNUAL_PLANNING_NEXT_YEAR.AutoSize = true;
            this.lblANNUAL_PLANNING_NEXT_YEAR.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblANNUAL_PLANNING_NEXT_YEAR.Location = new System.Drawing.Point(16, 337);
            this.lblANNUAL_PLANNING_NEXT_YEAR.Margin = new System.Windows.Forms.Padding(2);
            this.lblANNUAL_PLANNING_NEXT_YEAR.Name = "lblANNUAL_PLANNING_NEXT_YEAR";
            this.lblANNUAL_PLANNING_NEXT_YEAR.Size = new System.Drawing.Size(116, 19);
            this.lblANNUAL_PLANNING_NEXT_YEAR.TabIndex = 6;
            this.lblANNUAL_PLANNING_NEXT_YEAR.Text = "គំរោងសំរាប់ឆ្នាំបន្ទាប់";
            // 
            // lblTOTAL_INCOME_1
            // 
            this.lblTOTAL_INCOME_1.AutoSize = true;
            this.lblTOTAL_INCOME_1.Location = new System.Drawing.Point(16, 364);
            this.lblTOTAL_INCOME_1.Margin = new System.Windows.Forms.Padding(2);
            this.lblTOTAL_INCOME_1.Name = "lblTOTAL_INCOME_1";
            this.lblTOTAL_INCOME_1.Size = new System.Drawing.Size(70, 19);
            this.lblTOTAL_INCOME_1.TabIndex = 7;
            this.lblTOTAL_INCOME_1.Text = "ចំណូលសរុប";
            // 
            // lblTOTAL_EXPENSE_1
            // 
            this.lblTOTAL_EXPENSE_1.AutoSize = true;
            this.lblTOTAL_EXPENSE_1.Location = new System.Drawing.Point(16, 395);
            this.lblTOTAL_EXPENSE_1.Margin = new System.Windows.Forms.Padding(2);
            this.lblTOTAL_EXPENSE_1.Name = "lblTOTAL_EXPENSE_1";
            this.lblTOTAL_EXPENSE_1.Size = new System.Drawing.Size(74, 19);
            this.lblTOTAL_EXPENSE_1.TabIndex = 8;
            this.lblTOTAL_EXPENSE_1.Text = "ចំណាយសរុប";
            // 
            // lblREASON
            // 
            this.lblREASON.AutoSize = true;
            this.lblREASON.Location = new System.Drawing.Point(16, 188);
            this.lblREASON.Margin = new System.Windows.Forms.Padding(2);
            this.lblREASON.Name = "lblREASON";
            this.lblREASON.Size = new System.Drawing.Size(53, 19);
            this.lblREASON.TabIndex = 9;
            this.lblREASON.Text = "មូលហេតុ";
            // 
            // lblANNUAL_ADJUSTMENT
            // 
            this.lblANNUAL_ADJUSTMENT.AutoSize = true;
            this.lblANNUAL_ADJUSTMENT.Location = new System.Drawing.Point(16, 257);
            this.lblANNUAL_ADJUSTMENT.Margin = new System.Windows.Forms.Padding(2);
            this.lblANNUAL_ADJUSTMENT.Name = "lblANNUAL_ADJUSTMENT";
            this.lblANNUAL_ADJUSTMENT.Size = new System.Drawing.Size(65, 19);
            this.lblANNUAL_ADJUSTMENT.TabIndex = 10;
            this.lblANNUAL_ADJUSTMENT.Text = "ការកែសំរួល";
            // 
            // txtThisYearReason
            // 
            this.txtThisYearReason.Location = new System.Drawing.Point(108, 185);
            this.txtThisYearReason.Margin = new System.Windows.Forms.Padding(2);
            this.txtThisYearReason.Multiline = true;
            this.txtThisYearReason.Name = "txtThisYearReason";
            this.txtThisYearReason.Size = new System.Drawing.Size(531, 65);
            this.txtThisYearReason.TabIndex = 4;
            // 
            // txtThisYearImprovement
            // 
            this.txtThisYearImprovement.Location = new System.Drawing.Point(108, 254);
            this.txtThisYearImprovement.Margin = new System.Windows.Forms.Padding(2);
            this.txtThisYearImprovement.Multiline = true;
            this.txtThisYearImprovement.Name = "txtThisYearImprovement";
            this.txtThisYearImprovement.Size = new System.Drawing.Size(531, 65);
            this.txtThisYearImprovement.TabIndex = 5;
            // 
            // txtThisYearIncome
            // 
            this.txtThisYearIncome.Location = new System.Drawing.Point(108, 92);
            this.txtThisYearIncome.Margin = new System.Windows.Forms.Padding(2);
            this.txtThisYearIncome.Name = "txtThisYearIncome";
            this.txtThisYearIncome.Size = new System.Drawing.Size(162, 27);
            this.txtThisYearIncome.TabIndex = 1;
            this.txtThisYearIncome.TextChanged += new System.EventHandler(this.txtThisYearIncome_TextChanged);
            // 
            // txtThisYearExpense
            // 
            this.txtThisYearExpense.Location = new System.Drawing.Point(108, 123);
            this.txtThisYearExpense.Margin = new System.Windows.Forms.Padding(2);
            this.txtThisYearExpense.Name = "txtThisYearExpense";
            this.txtThisYearExpense.Size = new System.Drawing.Size(162, 27);
            this.txtThisYearExpense.TabIndex = 2;
            this.txtThisYearExpense.TextChanged += new System.EventHandler(this.txtThisYearExpense_TextChanged);
            // 
            // txtThisYearProfit
            // 
            this.txtThisYearProfit.Location = new System.Drawing.Point(108, 154);
            this.txtThisYearProfit.Margin = new System.Windows.Forms.Padding(2);
            this.txtThisYearProfit.Name = "txtThisYearProfit";
            this.txtThisYearProfit.ReadOnly = true;
            this.txtThisYearProfit.Size = new System.Drawing.Size(162, 27);
            this.txtThisYearProfit.TabIndex = 3;
            // 
            // txtNextYearIncome
            // 
            this.txtNextYearIncome.Location = new System.Drawing.Point(108, 360);
            this.txtNextYearIncome.Margin = new System.Windows.Forms.Padding(2);
            this.txtNextYearIncome.Name = "txtNextYearIncome";
            this.txtNextYearIncome.Size = new System.Drawing.Size(162, 27);
            this.txtNextYearIncome.TabIndex = 6;
            // 
            // txtNextYearExpense
            // 
            this.txtNextYearExpense.Location = new System.Drawing.Point(108, 391);
            this.txtNextYearExpense.Margin = new System.Windows.Forms.Padding(2);
            this.txtNextYearExpense.Name = "txtNextYearExpense";
            this.txtNextYearExpense.Size = new System.Drawing.Size(162, 27);
            this.txtNextYearExpense.TabIndex = 7;
            // 
            // btnSAVE
            // 
            this.btnSAVE.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSAVE.Location = new System.Drawing.Point(108, 435);
            this.btnSAVE.Margin = new System.Windows.Forms.Padding(2);
            this.btnSAVE.Name = "btnSAVE";
            this.btnSAVE.Size = new System.Drawing.Size(80, 23);
            this.btnSAVE.TabIndex = 8;
            this.btnSAVE.Text = "រក្សាទុក";
            this.btnSAVE.UseVisualStyleBackColor = true;
            this.btnSAVE.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblNextYear_
            // 
            this.lblNextYear_.AutoSize = true;
            this.lblNextYear_.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNextYear_.Location = new System.Drawing.Point(133, 337);
            this.lblNextYear_.Margin = new System.Windows.Forms.Padding(2);
            this.lblNextYear_.Name = "lblNextYear_";
            this.lblNextYear_.Size = new System.Drawing.Size(46, 19);
            this.lblNextYear_.TabIndex = 19;
            this.lblNextYear_.Text = "២០១៦";
            // 
            // lblThisYear_
            // 
            this.lblThisYear_.AutoSize = true;
            this.lblThisYear_.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThisYear_.Location = new System.Drawing.Point(152, 69);
            this.lblThisYear_.Margin = new System.Windows.Forms.Padding(2);
            this.lblThisYear_.Name = "lblThisYear_";
            this.lblThisYear_.Size = new System.Drawing.Size(46, 19);
            this.lblThisYear_.TabIndex = 20;
            this.lblThisYear_.Text = "២០១៥";
            // 
            // PageBussinessPlan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 514);
            this.Controls.Add(this.lblThisYear_);
            this.Controls.Add(this.lblNextYear_);
            this.Controls.Add(this.btnSAVE);
            this.Controls.Add(this.txtNextYearExpense);
            this.Controls.Add(this.txtNextYearIncome);
            this.Controls.Add(this.txtThisYearProfit);
            this.Controls.Add(this.txtThisYearExpense);
            this.Controls.Add(this.txtThisYearIncome);
            this.Controls.Add(this.txtThisYearImprovement);
            this.Controls.Add(this.txtThisYearReason);
            this.Controls.Add(this.lblANNUAL_ADJUSTMENT);
            this.Controls.Add(this.lblREASON);
            this.Controls.Add(this.lblTOTAL_EXPENSE_1);
            this.Controls.Add(this.lblTOTAL_INCOME_1);
            this.Controls.Add(this.lblANNUAL_PLANNING_NEXT_YEAR);
            this.Controls.Add(this.lblPROFIT);
            this.Controls.Add(this.lblTOTAL_EXPENSE);
            this.Controls.Add(this.lblTOTAL_INCOME);
            this.Controls.Add(this.lblANNUAL_PLANNING_IN_YEAR);
            this.Controls.Add(this.cboYear);
            this.Controls.Add(this.lblYEAR);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PageBussinessPlan";
            this.Text = "PageReportSetup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label lblYEAR;
        private ComboBox cboYear;
        private Label lblANNUAL_PLANNING_IN_YEAR;
        private Label lblTOTAL_INCOME;
        private Label lblTOTAL_EXPENSE;
        private Label lblPROFIT;
        private Label lblANNUAL_PLANNING_NEXT_YEAR;
        private Label lblTOTAL_INCOME_1;
        private Label lblTOTAL_EXPENSE_1;
        private Label lblREASON;
        private Label lblANNUAL_ADJUSTMENT;
        private TextBox txtThisYearReason;
        private TextBox txtThisYearImprovement;
        private TextBox txtThisYearIncome;
        private TextBox txtThisYearExpense;
        private TextBox txtThisYearProfit;
        private TextBox txtNextYearIncome;
        private TextBox txtNextYearExpense;
        private ExButton btnSAVE;
        private Label lblNextYear_;
        private Label lblThisYear_;

    }
}