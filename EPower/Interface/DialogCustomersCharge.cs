﻿using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Logic;
using EPower.Base.Properties;
using EPower.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomersCharge : ExDialog
    {
        List<Base.Logic.Currency> currencies = SettingLogic.Currencies;
        List<TBL_CUSTOMER> lstCus = new List<TBL_CUSTOMER>();
        List<TBL_INVOICE_ITEM> lstInvItem = new List<TBL_INVOICE_ITEM>();
        List<TBL_TAX> taxes = new List<TBL_TAX>();

        //Base.Logic.Currency currency = new Base.Logic.Currency();
        TBL_INVOICE_ITEM invItem = new TBL_INVOICE_ITEM();
        TBL_TAX tax = new TBL_TAX();

        decimal amount = 0;
        decimal taxAmount = 0;
        decimal totalAmount = 0;

        #region Constructor
        public DialogCustomersCharge(List<TBL_CUSTOMER> customers, int currency)
        {
            InitializeComponent();
            lblCustomerSelected_.Text = customers.Count.ToString();
            lstCus = customers;

            this.btnOK.Click += btnOk_Click;
            this.btnCLOSE.Click += btnClose_Click;
            this.cboTax.EditValueChanged += cboTax_EditValueChanged;
            this.cboInvoiceItem.EditValueChanged += cboInvoiceItem_EditValueChanged;

            lookup();

            this.txtAmount.TextChanged += txtAmount_TextChanged;
            this.cboCurrencyId.SelectedValueChanged += cboCurrencyId_SelectedValueChanged;
        }

        #endregion Constructor

        private void lookup()
        {
            UIHelper.SetDataSourceToComboBox(cboCurrencyId, Lookup.GetCurrencies());
            dtpInvoiceDate.Value = dtpDueDate.Value = DBDataContext.Db.GetSystemDate();

            //TAX
            taxes = new List<TBL_TAX>();
            taxes.Add(new TBL_TAX() { TAX_NAME = Resources.NO_TAX, TAX_ID = 0 });
            taxes.AddRange(DBDataContext.Db.TBL_TAXes.Where(x => x.IS_ACTIVE).ToList());
            cboTax.SetDataSourceToComboBox(taxes, nameof(TBL_TAX.TAX_ID), nameof(TBL_TAX.TAX_NAME));
            //cboTax.EditValue = 0;

            //SERVICE
            lstInvItem = DBDataContext.Db.TBL_INVOICE_ITEMs.Where(x => x.IS_ACTIVE && x.INVOICE_ITEM_TYPE_ID != 0).ToList();
            cboInvoiceItem.SetDataSourceToComboBox(lstInvItem, nameof(TBL_INVOICE_ITEM.INVOICE_ITEM_ID), nameof(TBL_INVOICE_ITEM.INVOICE_ITEM_NAME));
            cboInvoiceItem.EditValue = -4;

        }
        #region Event

        private void txtAmount_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.ClearAllValidation();
            if (string.IsNullOrEmpty(txtTotalAmount.Text))
            {
                this.txtAmount.SetValidation(string.Format(Resources.REQUIRED, lblAMOUNT_PENALTY.Text));
                return;
            }

            int currencyId = (int)cboCurrencyId.SelectedValue;
            DateTime now = DBDataContext.Db.GetSystemDate();
            DateTime dueDate = dtpDueDate.Value;
            DateTime invDate = dtpInvoiceDate.Value;
            DateTime dtInvoiceMonth = new DateTime(invDate.Year, invDate.Month, 1);
            var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(invDate, currencyId);
            Runner.RunNewThread(delegate ()
            {
                foreach (var cus in lstCus)
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        TBL_INVOICE objInv = new TBL_INVOICE();
                        objInv = Method.NewInvoice(true, invItem.INVOICE_ITEM_NAME, cus.CUSTOMER_ID);
                        objInv.CURRENCY_ID = currencyId;
                        objInv.CUSTOMER_ID = cus.CUSTOMER_ID;
                        objInv.DUE_DATE = dueDate;
                        objInv.INVOICE_DATE = invDate;
                        objInv.INVOICE_MONTH = dtInvoiceMonth;
                        objInv.METER_CODE = "";
                        objInv.START_DATE = now;
                        objInv.END_DATE = now;
                        objInv.START_PAY_DATE = now;
                        //insert invoice to db in order to get primary key
                        DBDataContext.Db.TBL_INVOICEs.InsertOnSubmit(objInv);
                        DBDataContext.Db.SubmitChanges();

                        TBL_INVOICE_DETAIL obj = new TBL_INVOICE_DETAIL()
                        {
                            AMOUNT = totalAmount,
                            END_USAGE = 0,
                            INVOICE_ID = objInv.INVOICE_ID,
                            INVOICE_ITEM_ID = invItem.INVOICE_ITEM_ID,
                            PRICE = amount,
                            START_USAGE = 0,
                            USAGE = 1,
                            CHARGE_DESCRIPTION = invItem.INVOICE_ITEM_NAME,
                            REF_NO = objInv.INVOICE_NO,
                            TRAN_DATE = objInv.INVOICE_DATE,
                            EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                            EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                            TAX_AMOUNT = taxAmount,
                            TAX_ID = tax?.TAX_ID??0
                        };
                        DBDataContext.Db.TBL_INVOICE_DETAILs.InsertOnSubmit(obj);

                        //Update Total Amount to Invoice;
                        objInv.TOTAL_AMOUNT += obj.AMOUNT;
                        objInv.SETTLE_AMOUNT = UIHelper.Round(objInv.TOTAL_AMOUNT, objInv.CURRENCY_ID);
                        DBDataContext.Db.SubmitChanges();

                        if (obj.TAX_ID != 0)
                        {
                            TBL_INVOICE_TAX objInvTax = new TBL_INVOICE_TAX
                            {
                                INVOICE_DETAIL_ID = obj.INVOICE_DETAIL_ID,
                                TAX_ID = obj.TAX_ID,
                                TAX_AMOUNT = obj.TAX_AMOUNT,
                                TAX_TITLE = taxes.FirstOrDefault(x => x.TAX_ID == obj.TAX_ID).TAX_NAME,
                                ROW_DATE = obj.TRAN_DATE,
                                IS_ACTIVE = true
                            };
                            DBDataContext.Db.TBL_INVOICE_TAXes.InsertOnSubmit(objInvTax);
                            DBDataContext.Db.SubmitChanges();
                        }

                        var balance = PrepaymentLogic.Instance.GetCustomerPrepayment(cus.CUSTOMER_ID, objInv.CURRENCY_ID);
                        if (balance > 0)
                        {
                            var invoices = new List<TBL_INVOICE>() { objInv };
                            PrepaymentLogic.Instance.ClearPaymentWithPrepay(invoices);
                        }
                        tran.Complete();
                    }
                }
            });
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void cboTax_EditValueChanged(object sender, EventArgs e)
        {
            tax = taxes.FirstOrDefault(x => x.TAX_ID == ((int?)cboTax.EditValue??0));
            taxAmount = 0;
            amount = DataHelper.ParseToDecimal(UIHelper.FormatCurrency(DataHelper.ParseToDecimal(txtAmount.Text), (int)cboCurrencyId.SelectedValue));
            Base.Logic.Currency currency = currencies.FirstOrDefault(x => x.CURRENCY_ID == (int)cboCurrencyId.SelectedValue);
            if (tax != null)
            {
                taxAmount = Method.CalculateTaxAmount(tax, amount, currency);
            }
            totalAmount = amount;
            if(tax.COMPUTATION != (int)TaxComputations.PERCENTAGE_INCLUDED_TAX)
            {
                totalAmount = amount + taxAmount;
            }
            this.txtTotalAmount.Text = UIHelper.FormatCurrency(totalAmount, currency.CURRENCY_ID);
        }
        private void cboInvoiceItem_EditValueChanged(object sender, EventArgs e)
        {
            invItem = lstInvItem.FirstOrDefault(x => x.INVOICE_ITEM_ID == (int)cboInvoiceItem.EditValue);
            if (invItem == null)
            {
                return;
            }
            txtAmount.Text = UIHelper.FormatCurrency(invItem.PRICE, invItem.CURRENCY_ID);
            cboCurrencyId.SelectedValue = invItem.CURRENCY_ID;
            cboTax.EditValue = invItem.TAX_ID;
            cboCurrencyId.Enabled = invItem.IS_EDITABLE;
            txtAmount.ReadOnly = !invItem.IS_EDITABLE;
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            cboTax_EditValueChanged(null, null);
        }

        private void cboCurrencyId_SelectedValueChanged(object sender, EventArgs e)
        {
            cboTax_EditValueChanged(null, null);
        }

        #endregion
    }
}
