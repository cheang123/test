﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogBankTran : ExDialog
    {
        #region Data
        GeneralProcess _flag = GeneralProcess.Insert;
        TBL_BANK_TRAN _objOld = new TBL_BANK_TRAN();
        TBL_BANK_TRAN _objNew = new TBL_BANK_TRAN();
        TBL_EXCHANGE_RATE objExchangeRate = null;
        public TBL_BANK_TRAN Voltage
        {
            get
            {
                return _objNew;
            }
        }

        #endregion Data

        #region Constructor

        public DialogBankTran(TBL_BANK_TRAN obj, GeneralProcess flag)
        {
            InitializeComponent();
            UIHelper.SetDataSourceToComboBox(cboCurrency, DBDataContext.Db.TLKP_CURRENCies);
            UIHelper.SetDataSourceToComboBox(cboBalanceCurrency, DBDataContext.Db.TLKP_CURRENCies);
            this._flag = flag;
            obj._CopyTo(this._objNew);
            obj._CopyTo(this._objOld);
            this.read();
            this.txtTransDate.Value = DBDataContext.Db.GetSystemDate();
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            if (flag == GeneralProcess.Delete)
            {
                UIHelper.SetEnabled(this, false);
            }
        }

        #endregion Constructor

        #region Method

        public bool invalid()
        {
            bool result = false;

            this.ClearAllValidation();
            if (this.txtRefNo.Text.Trim() == "")
            {
                this.txtRefNo.SetValidation(string.Format(Resources.REQUIRED, this.lblREF_NO.Text));
                this.txtRefNo.Focus();
                result = true;
            }
            if (this.cboCurrency.SelectedIndex == -1)
            {
                this.cboCurrency.SetValidation(string.Format(Resources.REQUIRED, this.lblCURRENCY.Text));
                this.cboCurrency.Focus();
                result = true;
            }
            if (!DataHelper.IsNumber(this.txtAmount.Text))
            {
                this.txtAmount.SetValidation(string.Format(Resources.REQUIRED, this.lblAMOUNT.Text));
                this.txtAmount.Focus();
                result = true;
            }
            if (this.cboBalanceCurrency.SelectedIndex == -1)
            {
                this.cboBalanceCurrency.SetValidation(string.Format(Resources.REQUIRED, this.cboBalanceCurrency.Text));
                this.cboBalanceCurrency.Focus();
                result = true;
            }
            if (this.txtTransDate.Value.Date == UIHelper._DefaultDate.Date)
            {
                this.txtTransDate.SetValidation(string.Format(Resources.REQUIRED, this.lblDATE.Text));
                this.txtTransDate.Focus();
                result = true;
            }
            if (result == false && this.objExchangeRate == null && (int)cboCurrency.SelectedValue != (int)cboBalanceCurrency.SelectedValue)
            {
                MsgBox.ShowWarning(Resources.MS_NO_DATA_ABOUT_EXCHANGE_RATE, this.Text);
                result = true;
            }
            return result;
        }

        public void read()
        {
            var currency = SettingLogic.Currencies.FirstOrDefault(x => x.CURRENCY_ID == _objNew.CURRENCY_ID);
            if (currency == null)
            {
                currency = SettingLogic.Currencies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY);
            }

            this.txtRefNo.Text = this._objNew.REF_NO;
            this.txtAmount.Text = this._objNew.AMOUNT == 0 ? "" : this._objNew.AMOUNT.ToString(currency.FORMAT);
            this.cboCurrency.SelectedValue = this._objNew.CURRENCY_ID;
            this.txtExchangeRate.Text = this._objNew.EXCHANGE_RATE.ToString(currency.FORMAT);
            this.cboBalanceCurrency.SelectedValue = this._objNew.BALANCE_CURRENCY_ID;
            this.rMultiplier.Checked = this._objNew.MULTIPLIER_METHOD;
            this.txtNote.Text = this._objNew.NOTE;
            if (this._objNew.TRAN_DATE == UIHelper._DefaultDate)
            {
                this.txtTransDate.ClearValue();
            }
            else
            {
                //this.txtTransDate.SetValue(this._objNew.TRAN_DATE);
            }
        }

        public void write()
        {
            this._objNew.REF_NO = this.txtRefNo.Text;
            this._objNew.AMOUNT = DataHelper.ParseToDecimal(this.txtAmount.Text);
            this._objNew.EXCHANGE_RATE = DataHelper.ParseToDecimal(this.txtExchangeRate.Text);
            this._objNew.CURRENCY_ID = (int)this.cboCurrency.SelectedValue;
            this._objNew.BALANCE_CURRENCY_ID = (int)this.cboBalanceCurrency.SelectedValue;
            this._objNew.MULTIPLIER_METHOD = rMultiplier.Checked;
            this._objNew.NOTE = this.txtNote.Text;
            this._objNew.TRAN_DATE = this.txtTransDate.Value;
            if (this._flag == GeneralProcess.Insert)
            {
                this._objNew.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                this._objNew.CREATE_ON = DBDataContext.Db.GetSystemDate();
            }
        }

        #endregion Method

        #region Event

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.invalid())
            {
                return;
            }
            this.write();
            using (TransactionScope tran = new TransactionScope())
            {
                if (DataHelper.ParseToDecimal(this.txtExchangeRate.Text) != exchangeRate)
                {
                    TBL_EXCHANGE_RATE objExchange = new TBL_EXCHANGE_RATE();
                    objExchange.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                    objExchange.CREATE_ON = DBDataContext.Db.GetSystemDate();
                    objExchange.CURRENCY_ID = (int)this.cboCurrency.SelectedValue;
                    objExchange.EXCHANGE_RATE = DataHelper.ParseToDecimal(this.txtExchangeRate.Text);
                    objExchange.EXCHANGE_CURRENCY_ID = (int)this.cboBalanceCurrency.SelectedValue;
                    objExchange.MULTIPLIER_METHOD = rMultiplier.Checked;
                    DBDataContext.Db.TBL_EXCHANGE_RATEs.InsertOnSubmit(objExchange);
                    DBDataContext.Db.SubmitChanges();
                    this._objNew.EXCHANGE_RATE = objExchange.EXCHANGE_RATE;
                }

                if (this._flag == GeneralProcess.Insert)
                {
                    DBDataContext.Db.Insert(this._objNew);
                }
                else if (this._flag == GeneralProcess.Update)
                {
                    DBDataContext.Db.Update(this._objOld, this._objNew);
                }
                else if (this._flag == GeneralProcess.Delete)
                {
                    DBDataContext.Db.Delete(this._objNew);
                }
                tran.Complete();
                this.DialogResult = DialogResult.OK;
            }
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }

        private void txtVOLTAGE_NAME_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        #endregion Event
        decimal exchangeRate = 0.0M;
        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedIndex == -1 || cboBalanceCurrency.SelectedIndex == -1)
            {
                return;
            }

            objExchangeRate = DBDataContext.Db.TBL_EXCHANGE_RATEs.OrderByDescending(row => row.CREATE_ON).FirstOrDefault(x => x.CURRENCY_ID == (int)cboCurrency.SelectedValue && x.EXCHANGE_CURRENCY_ID == (int)cboBalanceCurrency.SelectedValue);
            if (objExchangeRate != null)
            {
                var cDeposit = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == (int)cboCurrency.SelectedValue);
                var cBalance = DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x => x.CURRENCY_ID == (int)cboBalanceCurrency.SelectedValue);
                rMultiplier.Checked = objExchangeRate.MULTIPLIER_METHOD;
                exchangeRate = objExchangeRate.EXCHANGE_RATE;
                this.txtExchangeRate.Text = objExchangeRate.EXCHANGE_RATE.ToString("#,###.########");
                lblEXCHANGE_RATE_1.Text = string.Format(Resources.EXCHANGE_RATE_VALUE, objExchangeRate.MULTIPLIER_METHOD ? cDeposit.CURRENCY_SING : cBalance.CURRENCY_SING); ;
            }
            else if (objExchangeRate == null && (int)this.cboCurrency.SelectedValue != (int)this.cboBalanceCurrency.SelectedValue)
            {
                MsgBox.ShowWarning(Resources.MS_NO_DATA_ABOUT_EXCHANGE_RATE, this.Text);
                return;
            }

            if ((int)this.cboCurrency.SelectedValue == (int)this.cboBalanceCurrency.SelectedValue)
            {
                this.exchangeRate = 1;
                this.txtExchangeRate.Enabled = false;
                this.txtExchangeRate.Text = "1";
                lblEXCHANGE_RATE_1.Text = Resources.EXCHANGE_RATE;
            }
            else
            {
                this.txtExchangeRate.Enabled = true;
            }
            this.txtAmount.Focus();
        }

        private void InputEnglish(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void InputKhmer(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void txtTransDate_ValueChanged(object sender, EventArgs e)
        {
            if (txtTransDate.Value.Date == UIHelper._DefaultDate.Date)
            {
                txtTransDate.ClearValue();
            }
            else
            {
                //txtTransDate.SetValue(txtTransDate.Value);
            }
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            //var intCurrencyId = (int)cboCurrency.SelectedValue;
            //decimal amount = DataHelper.ParseToDecimal(this.txtAmount.Text);
            //this.txtAmount.Text=UIHelper.FormatCurrency(amount, intCurrencyId);
        }
    }
}
