﻿using EPower.Base.Helper.DevExpressCustomize;
using EPower.Base.Logic;
using EPower.Base.Properties;
using EPower.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerCharge : ExDialog
    {
        #region Data
        private TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();
        private List<TBL_INVOICE> _listInvoice = new List<TBL_INVOICE>();
        private List<TBL_PAYMENT> _listPayment = new List<TBL_PAYMENT>();

        Currencies mCurrency = null;
        int? _accountId = 0;
        TBL_PAYMENT_METHOD payMethod = new TBL_PAYMENT_METHOD();
        private bool _loading = false;
        #endregion Data

        #region Constructor
        public DialogCustomerCharge(TBL_CUSTOMER objCus)
        {
            this._loading = true;
            InitializeComponent();
            // SET PERMISSION
            lblINVOICE_DATE.Visible =
            dtpINVOICE_DATE.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_INVOICE_SERVICE_DATE]);
            dtpINVOICE_DATE.Enabled = SoftTech.Security.Logic.Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_INVOICE_SERVICE_DATE);

            // Payment method 
            var paymentMethods = DBDataContext.Db.TBL_PAYMENT_METHODs.Where(x => x.IS_ACTIVE).ToList();
            this.cboPaymentMethod.SetDataSourceToComboBox(paymentMethods, nameof(TBL_PAYMENT_METHOD.PAYMENT_METHOD_ID), nameof(TBL_PAYMENT_METHOD.PAYMENT_METHOD_NAME));
            this.cboPaymentMethod.EditValue = paymentMethods.FirstOrDefault().PAYMENT_METHOD_ID;

            // Tax
            this.cboTax.SetDataSourceToComboBox(DBDataContext.Db.TBL_TAXes.Where(x => x.IS_ACTIVE).ToList(), nameof(TBL_TAX.TAX_ID), nameof(TBL_TAX.TAX_NAME));

            //Currency
            bindCurrency();

            this.chkPRINT.Checked = Properties.Settings.Default.PRINT_INVOICE;
            this.chkIS_PAID.Checked = Properties.Settings.Default.IS_PAY_CHARGE;
            this.lblPAYMENT_METHOD.Enabled = this.cboPaymentMethod.Enabled = this.chkIS_PAID.Checked;
            objCus._CopyTo(_objCustomer);
            UIHelper.DataGridViewProperties(dgvInvoiceItem);
            bind();
            dtpINVOICE_DATE.Value = DBDataContext.Db.GetSystemDate();
            this._loading = false;

            this.cboTax.EditValueChanged += cboTax_EditValueChanged;
        }

        #endregion Constructor

        #region Method

        private void bindCurrency()
        {
            var currencies = SettingLogic.Currencies.ToList();
            UIHelper.SetDataSourceToComboBox(cboCurrency, currencies.Select(x => new { x.CURRENCY_ID, x.CURRENCY_NAME }));
            cboCurrency.SelectedValue = currencies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY)?.CURRENCY_ID ?? 0;
            mCurrency = SettingLogic.Currencies.FirstOrDefault(x => x.CURRENCY_ID == (int)cboCurrency.SelectedValue);
        }

        private void bind()
        {
            UIHelper.SetDataSourceToComboBox(cboInvoiceItem, from i in DBDataContext.Db.TBL_INVOICE_ITEMs
                                                             where i.INVOICE_ITEM_TYPE_ID != 0 && i.IS_ACTIVE
                                                             select new { i.INVOICE_ITEM_ID, i.INVOICE_ITEM_NAME }
                                                               , this.INVOICE_ITEM_ID.Name, "INVOICE_ITEM_NAME");
            this.cboInvoiceItem.SelectedIndex = -1;
        }

        private bool invalid()
        {
            bool blnReturn = false;
            this.ClearAllValidation();
            if (cboInvoiceItem.SelectedIndex == -1)
            {
                cboInvoiceItem.SetValidation(string.Format(Resources.REQUIRED, lblSERVICE.Text));
                blnReturn = true;
            }
            if (cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                blnReturn = true;
            }
            if (!DataHelper.IsNumber(txtPrice.Text))
            {
                txtPrice.SetValidation(Resources.REQUIRED_INPUT_NUMBER);
                blnReturn = true;
            }
            else
            {
                if (DataHelper.ParseToDecimal(txtPrice.Text) < 0)
                {
                    txtPrice.SetValidation(Resources.REQUEST_GRATER_THAN_ZERO);
                    blnReturn = true;
                }
            }

            if (txtDisplay.Text.Trim() == "")
            {
                txtDisplay.SetValidation(string.Format(Resources.REQUIRED, lblSHOW_ON_INVOICE.Text));
                blnReturn = true;
            }
            return blnReturn;
        }

        private bool invalidPayment()
        {
            bool blnReturn = false;
            payMethod = (TBL_PAYMENT_METHOD)cboPaymentMethod.GetSelectedDataRow();

            if (chkIS_PAID.Checked && payMethod == null)
            {
                this.cboPaymentMethod.SetValidation(string.Format(Resources.REQUIRED, lblPAYMENT_METHOD.Text));
                blnReturn = true;
            }

            _accountId = mCurrency.CURRENCY_ID == (int)Currency.KHR ? payMethod?.ACCOUNT_ID_KHR : mCurrency.CURRENCY_ID == (int)Currency.USD ? payMethod?.ACCOUNT_ID_USD :
                            mCurrency.CURRENCY_ID == (int)Currency.THB ? payMethod?.ACCOUNT_ID_THB : mCurrency.CURRENCY_ID == (int)Currency.VND ? payMethod?.ACCOUNT_ID_VND : 0;
            if (_accountId <= 0)
            {
                if (MsgBox.ShowQuestion(String.Format(Resources.MSG_PAYMENT_METHOD_NOT_SET_ACCOUNT, cboCurrency.Text), Resources.INFORMATION) == DialogResult.Yes)
                {
                    new DialogPaymentMethod(payMethod, GeneralProcess.Update).ShowDialog();
                }
                blnReturn = true;
            }
            return blnReturn;
        }

        bool isexist(int intInvItemId)
        {
            bool blnReturn = false;
            foreach (DataGridViewRow dgvr in dgvInvoiceItem.Rows)
            {
                int InvoiceItemId = int.Parse(dgvr.Cells[this.INVOICE_ITEM_ID.Name].Value.ToString());
                if (intInvItemId == InvoiceItemId)
                {
                    blnReturn = true;
                    break;
                }
            }
            return blnReturn;
        }

        void CreateNewInvoice()
        {
            DateTime datNow = DBDataContext.Db.GetSystemDate();
            DateTime dtInvoiceMonth = new DateTime();
            DateTime dtInvocieDate = new DateTime(dtpINVOICE_DATE.Value.Year, dtpINVOICE_DATE.Value.Month, 1);
            Decimal totalAmount = 0;   // Check if Total amount =0 will paid although no check is_paid
            //select all customer that in this cycle 
            var CusInfo = DBDataContext.Db.TBL_CUSTOMERs.FirstOrDefault(row => row.CUSTOMER_ID == _objCustomer.CUSTOMER_ID);
            var currencyId = (int)dgvInvoiceItem.Rows[0].Cells[CURRENCY_ID.Name].Value;
            TBL_TAX tax = (TBL_TAX)cboTax.GetSelectedDataRow();
            //get lastest service invoice
            var lstInvoice = DBDataContext.Db.TBL_INVOICEs.OrderByDescending(x => x.INVOICE_ID).FirstOrDefault(x => x.CUSTOMER_ID == _objCustomer.CUSTOMER_ID && x.CURRENCY_ID == currencyId && x.IS_SERVICE_BILL);
            if (!CusInfo.IS_POST_PAID)
            {
                dtInvoiceMonth = DBDataContext.Db.GetSystemDate();
                dtInvoiceMonth = new DateTime(dtInvoiceMonth.Year, dtInvoiceMonth.Month, 1);
            }
            else
            {
                dtInvoiceMonth = Method.GetNextBillingMonth(_objCustomer.BILLING_CYCLE_ID);
            }
            var exchangeRate = new ExchangeRateLogic().findLastExchangeRate(DBDataContext.Db.GetSystemDate(), currencyId);
            _listInvoice = new List<TBL_INVOICE>();
            foreach (DataGridViewRow dgvr in dgvInvoiceItem.Rows)
            {
                TBL_INVOICE objInv = _listInvoice.FirstOrDefault(x => x.CURRENCY_ID == (int)dgvr.Cells[CURRENCY_ID.Name].Value);
                if (objInv == null)
                {
                    objInv = Method.NewInvoice(true, dgvr.Cells[this.SERVICE.Name].Value.ToString(), _objCustomer.CUSTOMER_ID);
                    objInv.CURRENCY_ID = currencyId;
                    objInv.CUSTOMER_ID = _objCustomer.CUSTOMER_ID;
                    objInv.DUE_DATE = datNow.AddDays(double.Parse(DBDataContext.Db.TBL_UTILITies.FirstOrDefault(u => u.UTILITY_ID == (int)Utility.INVOICE_DUE_DATE).UTILITY_VALUE));
                    objInv.INVOICE_DATE = !dtpINVOICE_DATE.Visible || !dtpINVOICE_DATE.Enabled ? datNow : dtpINVOICE_DATE.Value;
                    objInv.INVOICE_MONTH = !dtpINVOICE_DATE.Visible || !dtpINVOICE_DATE.Enabled ? dtInvocieDate : new DateTime(dtpINVOICE_DATE.Value.Year, dtpINVOICE_DATE.Value.Month, 1);
                    objInv.METER_CODE = "";
                    objInv.START_DATE = datNow;
                    objInv.END_DATE = datNow;
                    objInv.START_PAY_DATE = datNow;
                    objInv.PRICE_ID = 0;
                    objInv.BASED_PRICE = 0;
                    objInv.PRICE = 0;
                    objInv.FORWARD_AMOUNT = lstInvoice == null ? 0 : lstInvoice.TOTAL_AMOUNT - lstInvoice.SETTLE_AMOUNT;
                    objInv.TOTAL_AMOUNT = DataHelper.ParseToDecimal(txtGrandTotal.Text) + objInv.FORWARD_AMOUNT;
                    objInv.SETTLE_AMOUNT = UIHelper.Round(objInv.TOTAL_AMOUNT, currencyId);
                    //insert invoice to db in order to get primary key
                    DBDataContext.Db.TBL_INVOICEs.InsertOnSubmit(objInv);
                    DBDataContext.Db.SubmitChanges();
                    _listInvoice.Add(objInv);
                }
                
                var price = DataHelper.ParseToDecimal(dgvr.Cells[this.UNIT_PRICE.Name].Value.ToString());
                decimal taxAmount = DataHelper.ParseToDecimal(Method.CalculateTaxAmount(tax, price).ToString("N5"));
                TBL_INVOICE_DETAIL obj = new TBL_INVOICE_DETAIL()
                {
                    AMOUNT = tax == null ? price : tax.COMPUTATION == (int)TaxComputations.PERCENTAGE_INCLUDED_TAX ? price : price + taxAmount,
                    END_USAGE = 0,
                    INVOICE_ID = objInv.INVOICE_ID,
                    INVOICE_ITEM_ID = int.Parse(dgvr.Cells[this.INVOICE_ITEM_ID.Name].Value.ToString()),
                    PRICE = price,
                    START_USAGE = 0,
                    USAGE = 1,
                    CHARGE_DESCRIPTION = dgvr.Cells[this.SERVICE.Name].Value.ToString(),
                    REF_NO = objInv.INVOICE_NO,
                    TRAN_DATE = objInv.INVOICE_DATE,
                    EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                    EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON,
                    TAX_AMOUNT = taxAmount,
                    TAX_ID = tax?.TAX_ID??0 //tax == null ? 0 : tax.TAX_ID
                };
                DBDataContext.Db.TBL_INVOICE_DETAILs.InsertOnSubmit(obj);

                //Update Total Amount to Invoice;
                //objInv.TOTAL_AMOUNT += obj.AMOUNT;
                //objInv.SETTLE_AMOUNT = UIHelper.Round(objInv.TOTAL_AMOUNT, objInv.CURRENCY_ID);
                DBDataContext.Db.SubmitChanges();
                totalAmount += DataHelper.ParseToDecimal(dgvr.Cells[this.UNIT_PRICE.Name].Value.ToString());
            }


            // checking if totalAmount ==0 invoice will paid
            if (chkIS_PAID.Checked || totalAmount == 0)
            {
                _listPayment = new List<TBL_PAYMENT>();
                foreach (var objInvoice in _listInvoice)
                {
                    TBL_PAYMENT objPayment = new TBL_PAYMENT()
                    {
                        CUSTOMER_ID = _objCustomer.CUSTOMER_ID,
                        CURRENCY_ID = objInvoice.CURRENCY_ID,
                        CREATE_ON = DBDataContext.Db.GetSystemDate(),
                        CREATE_BY = Login.CurrentLogin.LOGIN_NAME,
                        PAY_DATE = DBDataContext.Db.GetSystemDate(),
                        PAYMENT_NO = Method.GetNextSequence(Sequence.Receipt, true),
                        USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID,
                        PAY_AMOUNT = objInvoice.SETTLE_AMOUNT,
                        DUE_AMOUNT = objInvoice.SETTLE_AMOUNT,
                        IS_ACTIVE = true,
                        PAYMENT_ACCOUNT_ID = _accountId ?? 0,
                        EXCHANGE_RATE = exchangeRate.EXCHANGE_RATE,
                        EXCHANGE_RATE_DATE = exchangeRate.CREATE_ON
                    };
                    //update paid amount in invoice
                    objInvoice.PAID_AMOUNT = objPayment.PAY_AMOUNT;
                    objInvoice.INVOICE_STATUS = (int)InvoiceStatus.Close;
                    DBDataContext.Db.TBL_PAYMENTs.InsertOnSubmit(objPayment);
                    DBDataContext.Db.SubmitChanges();

                    //add payment detail
                    TBL_PAYMENT_DETAIL objPaymentDetail = new TBL_PAYMENT_DETAIL();
                    objPaymentDetail.PAYMENT_ID = objPayment.PAYMENT_ID;
                    objPaymentDetail.INVOICE_ID = objInvoice.INVOICE_ID;
                    objPaymentDetail.PAY_AMOUNT = objInvoice.PAID_AMOUNT;
                    objPaymentDetail.DUE_AMOUNT = objInvoice.SETTLE_AMOUNT;
                    DBDataContext.Db.TBL_PAYMENT_DETAILs.InsertOnSubmit(objPaymentDetail);
                    DBDataContext.Db.SubmitChanges();
                    _listPayment.Add(objPayment);
                }
            }
            else
            {
                var balance = PrepaymentLogic.Instance.GetCustomerPrepayment(_objCustomer.CUSTOMER_ID, currencyId);
                if (balance <= 0)
                {
                    return;
                }
                PrepaymentLogic.Instance.ClearPaymentWithPrepay(_listInvoice);
            }
        }

        private void addNew()
        {
            //_loading = true;
            if (invalid())
            {
                return;
            }
            int intInvoiceItemId = int.Parse(cboInvoiceItem.SelectedValue.ToString());

            if (isexist(intInvoiceItemId))
            {
                MsgBox.ShowInformation(string.Format(Resources.MS_IS_EXISTS, lblSERVICE.Text));
                return;
            }

            if (intInvoiceItemId == (int)InvoiceItem.NewConnection)
            {
                //if customer have connection fee already.
                int confee = (from i in DBDataContext.Db.TBL_INVOICEs
                              join id in DBDataContext.Db.TBL_INVOICE_DETAILs on i.INVOICE_ID equals id.INVOICE_ID
                              where id.INVOICE_ITEM_ID == (int)InvoiceItem.NewConnection
                              && i.CUSTOMER_ID == _objCustomer.CUSTOMER_ID
                              select i).Count();
                if (confee > 0)
                {
                    if (MsgBox.ShowQuestion(Resources.MS_CUSTOMER_ALREADY_HAVE_CONNECTION_FEE, this.Text) != DialogResult.Yes)
                    {
                        return;
                    }
                }
            }
            var amount = UIHelper.Round(DataHelper.ParseToDecimal(txtPrice.Text), mCurrency.CURRENCY_ID);
            var total = DataHelper.ParseToDecimal(txtTotal.Text) + amount;
            this.txtTotal.Text = UIHelper.Round(total, mCurrency.CURRENCY_ID).ToString();

            dgvInvoiceItem.Rows.Add(intInvoiceItemId, mCurrency.CURRENCY_ID, txtDisplay.Text, amount, mCurrency.CURRENCY_SING);
            this.txtPrice.Text = this.txtDisplay.Text = string.Empty;
            this.txtPrice.Enabled = this.txtDisplay.Enabled = false;
            this.txtPrice.Focus();
            bind();
            CalculateTax();
        }

        private void CalculateTax()
        {
            this.txtTax.Text = "0";
            this.txtGrandTotal.Text = this.txtTotal.Text;
            var tax = (TBL_TAX)cboTax.GetSelectedDataRow();

            decimal grandTotal = 0;
            decimal amount = DataHelper.ParseToDecimal(txtTotal.Text);
            decimal taxAmount = Method.CalculateTaxAmount(tax, amount);
            this.txtTax.Text = taxAmount.ToString("#,##0.##");
            grandTotal = amount + taxAmount;
            if (tax != null && tax.COMPUTATION == (int)TaxComputations.PERCENTAGE_INCLUDED_TAX)
            {
                grandTotal = amount;
            }
            this.txtGrandTotal.Text = grandTotal.ToString(UIHelper._DefaultPriceFormat);
        }

        #endregion Method

        #region Event

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkAdd_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            addNew();
            this.cboCurrency.Enabled = false;
        }

        private void linkRemove_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvInvoiceItem.SelectedRows.Count > 0)
            {
                var total = DataHelper.ParseToDecimal(this.txtTotal.Text) - DataHelper.ParseToDecimal(dgvInvoiceItem.SelectedRows[0].Cells[UNIT_PRICE.Name].Value.ToString());
                this.txtTotal.Text = UIHelper.Round(total, mCurrency.CURRENCY_ID).ToString();
                dgvInvoiceItem.Rows.Remove(dgvInvoiceItem.SelectedRows[0]);
                if (dgvInvoiceItem.Rows.Count == 0)
                {
                    this.cboCurrency.Enabled = true;
                    this.cboCurrency.Text = "";
                    this.cboCurrency.SelectedIndex = -1;
                }
                CalculateTax();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (invalidPayment())
            {
                return;
            }
            try
            {
                //check if now item added to grid
                //then return function
                if (dgvInvoiceItem.Rows.Count == 0)
                {
                    MsgBox.ShowInformation(Resources.MS_ADD_SERVICE_CHARGE);
                    return;
                }
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    CreateNewInvoice();
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
                if (this.chkPRINT.Checked)
                {
                    if (this.chkIS_PAID.Checked)
                    {
                        foreach (var item in _listPayment)
                        {
                            Printing.ReceiptPayment(item.PAYMENT_ID);
                        }
                    }
                    else
                    {
                        foreach (var item in _listInvoice)
                        {
                            printInvoice(item.INVOICE_ID);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void printInvoice(long invoiceID)
        {
            var PintInvoieID = 0;
            TBL_PRINT_INVOICE objPrintInvoice = new TBL_PRINT_INVOICE()
            {
                PRINT_DATE = DBDataContext.Db.GetSystemDate(),
                LOGIN_ID = Login.CurrentLogin.LOGIN_ID
            };
            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
            {
                DBDataContext.Db.TBL_PRINT_INVOICEs.InsertOnSubmit(objPrintInvoice);
                DBDataContext.Db.SubmitChanges();
                int i = 1;
                DBDataContext.Db.TBL_PRINT_INVOICE_DETAILs.InsertOnSubmit(new TBL_PRINT_INVOICE_DETAIL()
                {
                    PRINT_INVOICE_ID = objPrintInvoice.PRINT_INVOICE_ID,
                    INVOICE_ID = DataHelper.ParseToInt(invoiceID.ToString()),
                    PRINT_ORDER = i++
                });
                DBDataContext.Db.SubmitChanges();
                tran.Complete();
            }
            PintInvoieID = objPrintInvoice.PRINT_INVOICE_ID;
            //Check Print New and Old Invocie
            var InvoiceName = Properties.Settings.Default.REPORT_INVOICE;
            var PinterInvoiceName = Properties.Settings.Default.PRINTER_INVOICE;
            var TemplateString = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == 40).UTILITY_VALUE.ToString();
            var Templates = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BillPrintingListModel>>(TemplateString);
            var template = Templates.FirstOrDefault(x => x.TemplateName == InvoiceName);

            if (!template.IsOld)
            {
                ReportLogic.Instance.GetPrintNewInvoice(InvoiceName, PinterInvoiceName, PintInvoieID);
            }
            else
            {
                CrystalReportHelper cr = Method.GetInvoiceReport(InvoiceName);
                cr.SetParameter("@PRINT_INVOICE_ID", PintInvoieID);
                cr.PrintReport(PinterInvoiceName);
                cr.Dispose();
            }
        }

        private void txtPrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                if (DataHelper.ParseToDouble(txtPrice.Text) > 0)
                {
                    this.addNew();
                }
            }
        }

        private void txtPrice_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void cboInvoiceItem_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void chkPrintInvoice_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.PRINT_INVOICE = this.chkPRINT.Checked;
            Properties.Settings.Default.Save();
        }

        private void chkPayment_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.IS_PAY_CHARGE = chkIS_PAID.Checked;
            Properties.Settings.Default.Save();
            this.lblPAYMENT_METHOD.Enabled = this.cboPaymentMethod.Enabled = this.chkIS_PAID.Checked;
        }

        private void cboCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCurrency.SelectedIndex == -1)
            {
                return;
            }
            if (cboInvoiceItem.SelectedIndex == -1)
            {
                return;
            }
            txtPrice.Text = txtDisplay.Text = string.Empty;
            decimal decPrice = 0;
            TBL_INVOICE_ITEM objInvoiceItem = DBDataContext.Db.TBL_INVOICE_ITEMs.FirstOrDefault(i => i.INVOICE_ITEM_ID == (int)cboInvoiceItem.SelectedValue);
            mCurrency = SettingLogic.Currencies.FirstOrDefault(x => x.CURRENCY_ID == (int)cboCurrency.SelectedValue);
            if (objInvoiceItem.INVOICE_ITEM_ID == (int)InvoiceItem.NewConnection)
            {
                decPrice = Method.GetNewConnectionAmount(_objCustomer.CUSTOMER_ID, mCurrency.CURRENCY_ID);
            }
            else
            {
                decPrice = objInvoiceItem.PRICE;
            }

            this.txtPrice.Text = UIHelper.FormatCurrency(decPrice, mCurrency.CURRENCY_ID);
            this.txtPrice.Enabled = objInvoiceItem.IS_EDITABLE;
            this.txtPrice.ReadOnly = !objInvoiceItem.IS_EDITABLE;
            this.txtDisplay.Text = objInvoiceItem.INVOICE_ITEM_NAME;
            this.txtDisplay.Enabled = objInvoiceItem.IS_EDITABLE;
            this.txtDisplay.ReadOnly = !objInvoiceItem.IS_EDITABLE;
            if (objInvoiceItem.INVOICE_ITEM_ID == (int)InvoiceItem.NewConnection && !string.IsNullOrEmpty(txtPrice.Text.Trim()))
            {
                addNew();
            }
        }

        private void cboInvoiceItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboInvoiceItem.SelectedIndex != -1)
            {
                cboCurrency_SelectedIndexChanged(null, null);
            }
        }

        private void dgvInvoiceItem_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            if (e.ColumnIndex != dgvInvoiceItem.Columns[UNIT_PRICE.Name].Index)
            {
                return;
            }
            var currencyId = DataHelper.ParseToDecimal(dgvInvoiceItem.Rows[e.RowIndex].Cells[CURRENCY_ID.Name].Value?.ToString() ?? "0");
            var defaultCurrency = SettingLogic.Currencies.FirstOrDefault(x => x.IS_DEFAULT_CURRENCY);
            var currency = SettingLogic.Currencies.FirstOrDefault(x => x.CURRENCY_ID == currencyId);
            var format = defaultCurrency.FORMAT;
            if (currency != null)
            {
                format = currency.FORMAT;
            }
            e.CellStyle.Format = format;
        }

        private void dgvInvoiceItem_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            cboCurrency.Enabled = (dgvInvoiceItem.Rows.Count <= 0);
        }

        private void cboTax_EditValueChanged(object sender, EventArgs e)
        {
            CalculateTax();
        }

        #endregion Event 
    }
}
