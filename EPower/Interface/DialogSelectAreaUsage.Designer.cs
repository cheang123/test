﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogSelectAreaUsage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogSelectAreaUsage));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.AREA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SELECT_ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_CUSTOMER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.txtSearch = new SoftTech.Component.ExTextbox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtSearch);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.lblCount);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.dgv);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblCount, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.txtSearch, 0);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AREA_ID,
            this.SELECT_,
            this.AREA,
            this.TOTAL_CUSTOMER});
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.DataSourceChanged += new System.EventHandler(this.dgv_DataSourceChanged);
            this.dgv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellClick);
            this.dgv.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEndEdit);
            // 
            // AREA_ID
            // 
            this.AREA_ID.DataPropertyName = "AREA_ID";
            resources.ApplyResources(this.AREA_ID, "AREA_ID");
            this.AREA_ID.Name = "AREA_ID";
            // 
            // SELECT_
            // 
            this.SELECT_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.SELECT_.DataPropertyName = "SELECT_";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.NullValue = false;
            this.SELECT_.DefaultCellStyle = dataGridViewCellStyle2;
            this.SELECT_.FillWeight = 90F;
            resources.ApplyResources(this.SELECT_, "SELECT_");
            this.SELECT_.Name = "SELECT_";
            // 
            // AREA
            // 
            this.AREA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AREA.DataPropertyName = "AREA_CODE";
            resources.ApplyResources(this.AREA, "AREA");
            this.AREA.Name = "AREA";
            // 
            // TOTAL_CUSTOMER
            // 
            this.TOTAL_CUSTOMER.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.TOTAL_CUSTOMER.DataPropertyName = "TOTAL_CUSTOMER";
            resources.ApplyResources(this.TOTAL_CUSTOMER, "TOTAL_CUSTOMER");
            this.TOTAL_CUSTOMER.Name = "TOTAL_CUSTOMER";
            this.TOTAL_CUSTOMER.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // lblCount
            // 
            resources.ApplyResources(this.lblCount, "lblCount");
            this.lblCount.Name = "lblCount";
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.White;
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtSearch, "txtSearch");
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearch.QuickSearch += new System.EventHandler(this.TxtSearch_QuickSearch);
            // 
            // DialogSelectAreaUsage
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogSelectAreaUsage";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DialogSelectAreaUsage_FormClosing);
            this.Load += new System.EventHandler(this.DialogSelectAreaUsage_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dgv;
        private ExButton btnCLOSE;
        private Label label4;
        private Label label3;
        private Label lblCount;
        private DataGridViewTextBoxColumn AREA_ID;
        private DataGridViewCheckBoxColumn SELECT_;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn TOTAL_CUSTOMER;
        private ExTextbox txtSearch;
    }
}
