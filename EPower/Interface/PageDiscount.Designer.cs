﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageDiscount
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.DISCOUNT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISCOUNT_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISCOUNT_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISCOUNT_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DISCOUNT_UNIT_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboDiscountType = new System.Windows.Forms.ComboBox();
            this.btnCUSTOMER = new SoftTech.Component.ExButton();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DISCOUNT_ID,
            this.DISCOUNT_NAME,
            this.DISCOUNT_TYPE,
            this.DISCOUNT_,
            this.DISCOUNT_UNIT_,
            this.NOTE});
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Location = new System.Drawing.Point(0, 34);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(664, 317);
            this.dgv.TabIndex = 0;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // DISCOUNT_ID
            // 
            this.DISCOUNT_ID.DataPropertyName = "DISCOUNT_ID";
            this.DISCOUNT_ID.HeaderText = "DISCOUNT_ID";
            this.DISCOUNT_ID.Name = "DISCOUNT_ID";
            this.DISCOUNT_ID.ReadOnly = true;
            this.DISCOUNT_ID.Visible = false;
            // 
            // DISCOUNT_NAME
            // 
            this.DISCOUNT_NAME.DataPropertyName = "DISCOUNT_NAME";
            this.DISCOUNT_NAME.FillWeight = 5.076141F;
            this.DISCOUNT_NAME.HeaderText = "ឈ្មោះ";
            this.DISCOUNT_NAME.Name = "DISCOUNT_NAME";
            this.DISCOUNT_NAME.ReadOnly = true;
            this.DISCOUNT_NAME.Width = 300;
            // 
            // DISCOUNT_TYPE
            // 
            this.DISCOUNT_TYPE.DataPropertyName = "DISCOUNT_TYPE";
            this.DISCOUNT_TYPE.HeaderText = "ប្រភេទ";
            this.DISCOUNT_TYPE.Name = "DISCOUNT_TYPE";
            this.DISCOUNT_TYPE.ReadOnly = true;
            this.DISCOUNT_TYPE.Width = 150;
            // 
            // DISCOUNT_
            // 
            this.DISCOUNT_.DataPropertyName = "DISCOUNT";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.DISCOUNT_.DefaultCellStyle = dataGridViewCellStyle2;
            this.DISCOUNT_.FillWeight = 194.9239F;
            this.DISCOUNT_.HeaderText = "បរិមាណ";
            this.DISCOUNT_.Name = "DISCOUNT_";
            this.DISCOUNT_.ReadOnly = true;
            // 
            // DISCOUNT_UNIT_
            // 
            this.DISCOUNT_UNIT_.DataPropertyName = "DISCOUNT_UNIT";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.DISCOUNT_UNIT_.DefaultCellStyle = dataGridViewCellStyle3;
            this.DISCOUNT_UNIT_.HeaderText = "";
            this.DISCOUNT_UNIT_.Name = "DISCOUNT_UNIT_";
            this.DISCOUNT_UNIT_.ReadOnly = true;
            this.DISCOUNT_UNIT_.Width = 40;
            // 
            // NOTE
            // 
            this.NOTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NOTE.DataPropertyName = "DESCRIPTION";
            this.NOTE.HeaderText = "ពណ៌នា";
            this.NOTE.Name = "NOTE";
            this.NOTE.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboDiscountType);
            this.panel1.Controls.Add(this.btnCUSTOMER);
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.txtQuickSearch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(664, 34);
            this.panel1.TabIndex = 0;
            // 
            // cboDiscountType
            // 
            this.cboDiscountType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscountType.FormattingEnabled = true;
            this.cboDiscountType.Location = new System.Drawing.Point(175, 4);
            this.cboDiscountType.Name = "cboDiscountType";
            this.cboDiscountType.Size = new System.Drawing.Size(146, 27);
            this.cboDiscountType.TabIndex = 5;
            this.cboDiscountType.SelectedIndexChanged += new System.EventHandler(this.cboDiscountType_SelectedIndexChanged);
            // 
            // btnCUSTOMER
            // 
            this.btnCUSTOMER.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCUSTOMER.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCUSTOMER.Location = new System.Drawing.Point(365, 5);
            this.btnCUSTOMER.Margin = new System.Windows.Forms.Padding(2);
            this.btnCUSTOMER.Name = "btnCUSTOMER";
            this.btnCUSTOMER.Size = new System.Drawing.Size(71, 23);
            this.btnCUSTOMER.TabIndex = 4;
            this.btnCUSTOMER.Text = "អតិថិជន";
            this.btnCUSTOMER.UseVisualStyleBackColor = true;
            this.btnCUSTOMER.Click += new System.EventHandler(this.btnCutomer_Click);
            // 
            // btnREMOVE
            // 
            this.btnREMOVE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnREMOVE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnREMOVE.Location = new System.Drawing.Point(588, 5);
            this.btnREMOVE.Margin = new System.Windows.Forms.Padding(2);
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.Size = new System.Drawing.Size(71, 23);
            this.btnREMOVE.TabIndex = 3;
            this.btnREMOVE.Text = "លុប";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnADD
            // 
            this.btnADD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnADD.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnADD.Location = new System.Drawing.Point(440, 5);
            this.btnADD.Margin = new System.Windows.Forms.Padding(2);
            this.btnADD.Name = "btnADD";
            this.btnADD.Size = new System.Drawing.Size(71, 23);
            this.btnADD.TabIndex = 1;
            this.btnADD.Text = "បន្ថែមថ្មី";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT
            // 
            this.btnEDIT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEDIT.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnEDIT.Location = new System.Drawing.Point(514, 5);
            this.btnEDIT.Margin = new System.Windows.Forms.Padding(2);
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.Size = new System.Drawing.Size(71, 23);
            this.btnEDIT.TabIndex = 2;
            this.btnEDIT.Text = "កែប្រែ";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtQuickSearch.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.txtQuickSearch.Location = new System.Drawing.Point(5, 4);
            this.txtQuickSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.Size = new System.Drawing.Size(167, 27);
            this.txtQuickSearch.TabIndex = 0;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "DISCOUNT_ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "DISCOUNT_ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DISCOUNT_NAME";
            this.dataGridViewTextBoxColumn2.FillWeight = 5.076141F;
            this.dataGridViewTextBoxColumn2.HeaderText = "ឈ្មោះ";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 300;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "DISCOUNT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn3.FillWeight = 194.9239F;
            this.dataGridViewTextBoxColumn3.HeaderText = "បរិមាណ";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "DISCOUNT_TYPE";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 40;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "DESCRIPTION";
            this.dataGridViewTextBoxColumn5.HeaderText = "ពណ៌នា";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // PageDiscount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(664, 351);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PageDiscount";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        private ExButton btnREMOVE;
        private ExButton btnCUSTOMER;
        private ComboBox cboDiscountType;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn DISCOUNT_ID;
        private DataGridViewTextBoxColumn DISCOUNT_NAME;
        private DataGridViewTextBoxColumn DISCOUNT_TYPE;
        private DataGridViewTextBoxColumn DISCOUNT_;
        private DataGridViewTextBoxColumn DISCOUNT_UNIT_;
        private DataGridViewTextBoxColumn NOTE;
    }
}
