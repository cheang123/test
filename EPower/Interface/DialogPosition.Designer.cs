﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPosition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPosition));
            this.lblPOSITION = new System.Windows.Forms.Label();
            this.txtPositionName = new System.Windows.Forms.TextBox();
            this.lblJOB_DEPARTMENT = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.lblSALARY = new System.Windows.Forms.Label();
            this.chkIS_PAYROLL = new System.Windows.Forms.CheckBox();
            this.cboValue = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboValue);
            this.content.Controls.Add(this.chkIS_PAYROLL);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblSALARY);
            this.content.Controls.Add(this.lblJOB_DEPARTMENT);
            this.content.Controls.Add(this.txtPositionName);
            this.content.Controls.Add(this.lblPOSITION);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblPOSITION, 0);
            this.content.Controls.SetChildIndex(this.txtPositionName, 0);
            this.content.Controls.SetChildIndex(this.lblJOB_DEPARTMENT, 0);
            this.content.Controls.SetChildIndex(this.lblSALARY, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.chkIS_PAYROLL, 0);
            this.content.Controls.SetChildIndex(this.cboValue, 0);
            // 
            // lblPOSITION
            // 
            resources.ApplyResources(this.lblPOSITION, "lblPOSITION");
            this.lblPOSITION.Name = "lblPOSITION";
            // 
            // txtPositionName
            // 
            resources.ApplyResources(this.txtPositionName, "txtPositionName");
            this.txtPositionName.Name = "txtPositionName";
            this.txtPositionName.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblJOB_DEPARTMENT
            // 
            resources.ApplyResources(this.lblJOB_DEPARTMENT, "lblJOB_DEPARTMENT");
            this.lblJOB_DEPARTMENT.Name = "lblJOB_DEPARTMENT";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // lblSALARY
            // 
            resources.ApplyResources(this.lblSALARY, "lblSALARY");
            this.lblSALARY.Name = "lblSALARY";
            // 
            // chkIS_PAYROLL
            // 
            resources.ApplyResources(this.chkIS_PAYROLL, "chkIS_PAYROLL");
            this.chkIS_PAYROLL.Name = "chkIS_PAYROLL";
            this.chkIS_PAYROLL.UseVisualStyleBackColor = true;
            // 
            // cboValue
            // 
            this.cboValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboValue.FormattingEnabled = true;
            resources.ApplyResources(this.cboValue, "cboValue");
            this.cboValue.Name = "cboValue";
            // 
            // DialogPosition
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogPosition";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblJOB_DEPARTMENT;
        private TextBox txtPositionName;
        private Label lblPOSITION;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label label1;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private ComboBox cboValue;
        private CheckBox chkIS_PAYROLL;
        private Label lblSALARY;
    }
}