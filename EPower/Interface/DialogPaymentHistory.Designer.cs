﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogPaymentHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogPaymentHistory));
            this.dgvPayment = new System.Windows.Forms.DataGridView();
            this.PAYMENT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RECEIPT_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENT_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CASH_DRAWER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPEN_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLOSED_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_INVOICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAID_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_VOID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BANK_PAYMENT_DETAIL_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPaidAmount = new System.Windows.Forms.TextBox();
            this.lblPAID_AMOUNT = new System.Windows.Forms.Label();
            this.lblPAYMENT_HISTORY = new System.Windows.Forms.Label();
            this.lblINVOICE_INFORMATION = new System.Windows.Forms.Label();
            this.lblINVOICE_TITLE = new System.Windows.Forms.Label();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.lblINVOICE_NO = new System.Windows.Forms.Label();
            this.btnREMOVE = new SoftTech.Component.ExLinkLabel(this.components);
            this.txtInvoiceNo = new System.Windows.Forms.TextBox();
            this.txtInvoiceTittle = new System.Windows.Forms.TextBox();
            this.txtSettleAmount = new System.Windows.Forms.TextBox();
            this.btnPRINT = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblPAYMENT = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblDUE_AMOUNT = new System.Windows.Forms.Label();
            this.txtDueAmount = new System.Windows.Forms.TextBox();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.exLinkLabel1 = new SoftTech.Component.ExLinkLabel(this.components);
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPayment)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboStatus);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.dgvPayment);
            this.content.Controls.Add(this.txtDueAmount);
            this.content.Controls.Add(this.txtSettleAmount);
            this.content.Controls.Add(this.txtPaidAmount);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.lblPAID_AMOUNT);
            this.content.Controls.Add(this.lblPAYMENT_HISTORY);
            this.content.Controls.Add(this.lblINVOICE_INFORMATION);
            this.content.Controls.Add(this.txtInvoiceNo);
            this.content.Controls.Add(this.txtInvoiceTittle);
            this.content.Controls.Add(this.lblDUE_AMOUNT);
            this.content.Controls.Add(this.lblINVOICE_TITLE);
            this.content.Controls.Add(this.lblAMOUNT);
            this.content.Controls.Add(this.lblINVOICE_NO);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblINVOICE_NO, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblINVOICE_TITLE, 0);
            this.content.Controls.SetChildIndex(this.lblDUE_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtInvoiceTittle, 0);
            this.content.Controls.SetChildIndex(this.txtInvoiceNo, 0);
            this.content.Controls.SetChildIndex(this.lblINVOICE_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_HISTORY, 0);
            this.content.Controls.SetChildIndex(this.lblPAID_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.txtPaidAmount, 0);
            this.content.Controls.SetChildIndex(this.txtSettleAmount, 0);
            this.content.Controls.SetChildIndex(this.txtDueAmount, 0);
            this.content.Controls.SetChildIndex(this.dgvPayment, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.cboStatus, 0);
            // 
            // dgvPayment
            // 
            this.dgvPayment.AllowUserToAddRows = false;
            this.dgvPayment.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPayment.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPayment.BackgroundColor = System.Drawing.Color.White;
            this.dgvPayment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPayment.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPayment.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PAYMENT_ID,
            this.RECEIPT_NO,
            this.DATE,
            this.CREATE_BY,
            this.PAYMENT_ACCOUNT,
            this.CASH_DRAWER_NAME,
            this.OPEN_DATE,
            this.CLOSED_DATE,
            this.TOTAL_INVOICE,
            this.PAID_AMOUNT,
            this.CURRENCY_SING_,
            this.IS_VOID,
            this.BANK_PAYMENT_DETAIL_ID});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPayment.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvPayment.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvPayment, "dgvPayment");
            this.dgvPayment.Name = "dgvPayment";
            this.dgvPayment.ReadOnly = true;
            this.dgvPayment.RowHeadersVisible = false;
            this.dgvPayment.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPayment_CellContentClick);
            // 
            // PAYMENT_ID
            // 
            this.PAYMENT_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PAYMENT_ID.DataPropertyName = "PAYMENT_ID";
            resources.ApplyResources(this.PAYMENT_ID, "PAYMENT_ID");
            this.PAYMENT_ID.Name = "PAYMENT_ID";
            this.PAYMENT_ID.ReadOnly = true;
            // 
            // RECEIPT_NO
            // 
            this.RECEIPT_NO.DataPropertyName = "PAYMENT_NO";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS System", 8.25F, System.Drawing.FontStyle.Underline);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Blue;
            this.RECEIPT_NO.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.RECEIPT_NO, "RECEIPT_NO");
            this.RECEIPT_NO.Name = "RECEIPT_NO";
            this.RECEIPT_NO.ReadOnly = true;
            // 
            // DATE
            // 
            this.DATE.DataPropertyName = "PAY_DATE";
            dataGridViewCellStyle3.Format = "dd-MM-yyyy";
            this.DATE.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.DATE, "DATE");
            this.DATE.Name = "DATE";
            this.DATE.ReadOnly = true;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            // 
            // PAYMENT_ACCOUNT
            // 
            this.PAYMENT_ACCOUNT.DataPropertyName = "ACCOUNT_NAME";
            resources.ApplyResources(this.PAYMENT_ACCOUNT, "PAYMENT_ACCOUNT");
            this.PAYMENT_ACCOUNT.Name = "PAYMENT_ACCOUNT";
            this.PAYMENT_ACCOUNT.ReadOnly = true;
            // 
            // CASH_DRAWER_NAME
            // 
            this.CASH_DRAWER_NAME.DataPropertyName = "CASH_DRAWER_NAME";
            resources.ApplyResources(this.CASH_DRAWER_NAME, "CASH_DRAWER_NAME");
            this.CASH_DRAWER_NAME.Name = "CASH_DRAWER_NAME";
            this.CASH_DRAWER_NAME.ReadOnly = true;
            // 
            // OPEN_DATE
            // 
            this.OPEN_DATE.DataPropertyName = "OPEN_DATE";
            dataGridViewCellStyle4.Format = "dd-MM-yyyy";
            dataGridViewCellStyle4.NullValue = null;
            this.OPEN_DATE.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.OPEN_DATE, "OPEN_DATE");
            this.OPEN_DATE.Name = "OPEN_DATE";
            this.OPEN_DATE.ReadOnly = true;
            // 
            // CLOSED_DATE
            // 
            this.CLOSED_DATE.DataPropertyName = "CLOSED_DATE";
            dataGridViewCellStyle5.Format = "dd-MM-yyyy";
            dataGridViewCellStyle5.NullValue = null;
            this.CLOSED_DATE.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.CLOSED_DATE, "CLOSED_DATE");
            this.CLOSED_DATE.Name = "CLOSED_DATE";
            this.CLOSED_DATE.ReadOnly = true;
            // 
            // TOTAL_INVOICE
            // 
            this.TOTAL_INVOICE.DataPropertyName = "NO_INVOICE";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TOTAL_INVOICE.DefaultCellStyle = dataGridViewCellStyle6;
            resources.ApplyResources(this.TOTAL_INVOICE, "TOTAL_INVOICE");
            this.TOTAL_INVOICE.Name = "TOTAL_INVOICE";
            this.TOTAL_INVOICE.ReadOnly = true;
            // 
            // PAID_AMOUNT
            // 
            this.PAID_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PAID_AMOUNT.DataPropertyName = "PAY_AMOUNT";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "#,##0.####";
            this.PAID_AMOUNT.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this.PAID_AMOUNT, "PAID_AMOUNT");
            this.PAID_AMOUNT.Name = "PAID_AMOUNT";
            this.PAID_AMOUNT.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SIGN";
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // IS_VOID
            // 
            this.IS_VOID.DataPropertyName = "IS_VOID";
            resources.ApplyResources(this.IS_VOID, "IS_VOID");
            this.IS_VOID.Name = "IS_VOID";
            this.IS_VOID.ReadOnly = true;
            // 
            // BANK_PAYMENT_DETAIL_ID
            // 
            this.BANK_PAYMENT_DETAIL_ID.DataPropertyName = "BANK_PAYMENT_DETAIL_ID";
            resources.ApplyResources(this.BANK_PAYMENT_DETAIL_ID, "BANK_PAYMENT_DETAIL_ID");
            this.BANK_PAYMENT_DETAIL_ID.Name = "BANK_PAYMENT_DETAIL_ID";
            this.BANK_PAYMENT_DETAIL_ID.ReadOnly = true;
            // 
            // txtPaidAmount
            // 
            resources.ApplyResources(this.txtPaidAmount, "txtPaidAmount");
            this.txtPaidAmount.Name = "txtPaidAmount";
            this.txtPaidAmount.ReadOnly = true;
            // 
            // lblPAID_AMOUNT
            // 
            resources.ApplyResources(this.lblPAID_AMOUNT, "lblPAID_AMOUNT");
            this.lblPAID_AMOUNT.Name = "lblPAID_AMOUNT";
            // 
            // lblPAYMENT_HISTORY
            // 
            resources.ApplyResources(this.lblPAYMENT_HISTORY, "lblPAYMENT_HISTORY");
            this.lblPAYMENT_HISTORY.Name = "lblPAYMENT_HISTORY";
            // 
            // lblINVOICE_INFORMATION
            // 
            resources.ApplyResources(this.lblINVOICE_INFORMATION, "lblINVOICE_INFORMATION");
            this.lblINVOICE_INFORMATION.Name = "lblINVOICE_INFORMATION";
            // 
            // lblINVOICE_TITLE
            // 
            resources.ApplyResources(this.lblINVOICE_TITLE, "lblINVOICE_TITLE");
            this.lblINVOICE_TITLE.Name = "lblINVOICE_TITLE";
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // lblINVOICE_NO
            // 
            resources.ApplyResources(this.lblINVOICE_NO, "lblINVOICE_NO");
            this.lblINVOICE_NO.Name = "lblINVOICE_NO";
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.TabStop = true;
            this.btnREMOVE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblDeletePayment_LinkClicked);
            // 
            // txtInvoiceNo
            // 
            resources.ApplyResources(this.txtInvoiceNo, "txtInvoiceNo");
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.ReadOnly = true;
            // 
            // txtInvoiceTittle
            // 
            resources.ApplyResources(this.txtInvoiceTittle, "txtInvoiceTittle");
            this.txtInvoiceTittle.Name = "txtInvoiceTittle";
            this.txtInvoiceTittle.ReadOnly = true;
            // 
            // txtSettleAmount
            // 
            resources.ApplyResources(this.txtSettleAmount, "txtSettleAmount");
            this.txtSettleAmount.Name = "txtSettleAmount";
            this.txtSettleAmount.ReadOnly = true;
            // 
            // btnPRINT
            // 
            resources.ApplyResources(this.btnPRINT, "btnPRINT");
            this.btnPRINT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPRINT.Name = "btnPRINT";
            this.btnPRINT.TabStop = true;
            this.btnPRINT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblPrint_LinkClicked);
            // 
            // lblPAYMENT
            // 
            resources.ApplyResources(this.lblPAYMENT, "lblPAYMENT");
            this.lblPAYMENT.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblPAYMENT.Name = "lblPAYMENT";
            this.lblPAYMENT.TabStop = true;
            this.lblPAYMENT.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblPayment_LinkClicked);
            // 
            // lblDUE_AMOUNT
            // 
            resources.ApplyResources(this.lblDUE_AMOUNT, "lblDUE_AMOUNT");
            this.lblDUE_AMOUNT.Name = "lblDUE_AMOUNT";
            // 
            // txtDueAmount
            // 
            resources.ApplyResources(this.txtDueAmount, "txtDueAmount");
            this.txtDueAmount.Name = "txtDueAmount";
            this.txtDueAmount.ReadOnly = true;
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.exLinkLabel1);
            this.panel1.Controls.Add(this.lblPAYMENT);
            this.panel1.Controls.Add(this.btnPRINT);
            this.panel1.Controls.Add(this.btnREMOVE);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // exLinkLabel1
            // 
            resources.ApplyResources(this.exLinkLabel1, "exLinkLabel1");
            this.exLinkLabel1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.exLinkLabel1.Name = "exLinkLabel1";
            this.exLinkLabel1.TabStop = true;
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // cboStatus
            // 
            this.cboStatus.AllowDrop = true;
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.cboStatus, "cboStatus");
            this.cboStatus.FormattingEnabled = true;
            this.cboStatus.Name = "cboStatus";
            this.cboStatus.SelectedIndexChanged += new System.EventHandler(this.cboStatus_SelectedIndexChanged);
            // 
            // DialogPaymentHistory
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogPaymentHistory";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPayment)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dgvPayment;
        private TextBox txtPaidAmount;
        private Label lblPAID_AMOUNT;
        private Label lblPAYMENT_HISTORY;
        private Label lblINVOICE_INFORMATION;
        private Label lblINVOICE_TITLE;
        private Label lblAMOUNT;
        private Label lblINVOICE_NO;
        private ExLinkLabel btnREMOVE;
        private TextBox txtSettleAmount;
        private TextBox txtInvoiceNo;
        private TextBox txtInvoiceTittle;
        private ExLinkLabel btnPRINT;
        private ExLinkLabel lblPAYMENT;
        private TextBox txtDueAmount;
        private Label lblDUE_AMOUNT;
        private ExButton btnCLOSE;
        private Panel panel1;
        private ExLinkLabel exLinkLabel1;
        private Label lblSTATUS;
        private ComboBox cboStatus;
        private DataGridViewTextBoxColumn PAYMENT_ID;
        private DataGridViewTextBoxColumn RECEIPT_NO;
        private DataGridViewTextBoxColumn DATE;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn PAYMENT_ACCOUNT;
        private DataGridViewTextBoxColumn CASH_DRAWER_NAME;
        private DataGridViewTextBoxColumn OPEN_DATE;
        private DataGridViewTextBoxColumn CLOSED_DATE;
        private DataGridViewTextBoxColumn TOTAL_INVOICE;
        private DataGridViewTextBoxColumn PAID_AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private DataGridViewTextBoxColumn IS_VOID;
        private DataGridViewTextBoxColumn BANK_PAYMENT_DETAIL_ID;
    }
}
