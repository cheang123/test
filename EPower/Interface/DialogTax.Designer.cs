﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogTax
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogTax));
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cboComputation = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.cboAccount = new HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTAX_EXPENSE_ACCOUNT = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.lblTAX_AMOUNT = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCOMPUTATION = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTaxName = new System.Windows.Forms.TextBox();
            this.lblTAX_NAME = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboComputation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboAccount.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.panel3);
            this.content.Controls.Add(this.panel2);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.panel2, 0);
            this.content.Controls.SetChildIndex(this.panel3, 0);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCHANGE_LOG);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.btnCLOSE);
            this.panel2.Controls.Add(this.btnOK);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cboComputation);
            this.panel3.Controls.Add(this.cboAccount);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.lblTAX_EXPENSE_ACCOUNT);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.txtAmount);
            this.panel3.Controls.Add(this.lblTAX_AMOUNT);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.lblCOMPUTATION);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.txtTaxName);
            this.panel3.Controls.Add(this.lblTAX_NAME);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // cboComputation
            // 
            resources.ApplyResources(this.cboComputation, "cboComputation");
            this.cboComputation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboComputation.Name = "cboComputation";
            this.cboComputation.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboComputation.Properties.Appearance.Font")));
            this.cboComputation.Properties.Appearance.Options.UseFont = true;
            this.cboComputation.Properties.AppearanceDisabled.Font = ((System.Drawing.Font)(resources.GetObject("cboComputation.Properties.AppearanceDisabled.Font")));
            this.cboComputation.Properties.AppearanceDisabled.Options.UseFont = true;
            this.cboComputation.Properties.AppearanceDropDown.Font = ((System.Drawing.Font)(resources.GetObject("cboComputation.Properties.AppearanceDropDown.Font")));
            this.cboComputation.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboComputation.Properties.AppearanceDropDownHeader.Font = ((System.Drawing.Font)(resources.GetObject("cboComputation.Properties.AppearanceDropDownHeader.Font")));
            this.cboComputation.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.cboComputation.Properties.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("cboComputation.Properties.AppearanceFocused.Font")));
            this.cboComputation.Properties.AppearanceFocused.Options.UseFont = true;
            this.cboComputation.Properties.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("cboComputation.Properties.AppearanceReadOnly.Font")));
            this.cboComputation.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.cboComputation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboComputation.Properties.Buttons"))))});
            this.cboComputation.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboComputation.Properties.Columns"), resources.GetString("cboComputation.Properties.Columns1"), ((int)(resources.GetObject("cboComputation.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboComputation.Properties.Columns3"))), resources.GetString("cboComputation.Properties.Columns4"), ((bool)(resources.GetObject("cboComputation.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboComputation.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboComputation.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboComputation.Properties.Columns8"))))});
            this.cboComputation.Properties.DropDownRows = 4;
            this.cboComputation.Properties.NullText = resources.GetString("cboComputation.Properties.NullText");
            this.cboComputation.Properties.PopupWidth = 200;
            this.cboComputation.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboComputation.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboComputation.Required = false;
            // 
            // cboAccount
            // 
            resources.ApplyResources(this.cboAccount, "cboAccount");
            this.cboAccount.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboAccount.Name = "cboAccount";
            this.cboAccount.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cboAccount.Properties.Appearance.Font")));
            this.cboAccount.Properties.Appearance.Options.UseFont = true;
            this.cboAccount.Properties.AppearanceDisabled.Font = ((System.Drawing.Font)(resources.GetObject("cboAccount.Properties.AppearanceDisabled.Font")));
            this.cboAccount.Properties.AppearanceDisabled.Options.UseFont = true;
            this.cboAccount.Properties.AppearanceDropDown.Font = ((System.Drawing.Font)(resources.GetObject("cboAccount.Properties.AppearanceDropDown.Font")));
            this.cboAccount.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cboAccount.Properties.AppearanceDropDownHeader.Font = ((System.Drawing.Font)(resources.GetObject("cboAccount.Properties.AppearanceDropDownHeader.Font")));
            this.cboAccount.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.cboAccount.Properties.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("cboAccount.Properties.AppearanceFocused.Font")));
            this.cboAccount.Properties.AppearanceFocused.Options.UseFont = true;
            this.cboAccount.Properties.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("cboAccount.Properties.AppearanceReadOnly.Font")));
            this.cboAccount.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.cboAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cboAccount.Properties.Buttons"))))});
            this.cboAccount.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboAccount.Properties.Columns"), resources.GetString("cboAccount.Properties.Columns1"), ((int)(resources.GetObject("cboAccount.Properties.Columns2"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboAccount.Properties.Columns3"))), resources.GetString("cboAccount.Properties.Columns4"), ((bool)(resources.GetObject("cboAccount.Properties.Columns5"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboAccount.Properties.Columns6"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboAccount.Properties.Columns7"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboAccount.Properties.Columns8")))),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo(resources.GetString("cboAccount.Properties.Columns9"), resources.GetString("cboAccount.Properties.Columns10"), ((int)(resources.GetObject("cboAccount.Properties.Columns11"))), ((DevExpress.Utils.FormatType)(resources.GetObject("cboAccount.Properties.Columns12"))), resources.GetString("cboAccount.Properties.Columns13"), ((bool)(resources.GetObject("cboAccount.Properties.Columns14"))), ((DevExpress.Utils.HorzAlignment)(resources.GetObject("cboAccount.Properties.Columns15"))), ((DevExpress.Data.ColumnSortOrder)(resources.GetObject("cboAccount.Properties.Columns16"))), ((DevExpress.Utils.DefaultBoolean)(resources.GetObject("cboAccount.Properties.Columns17"))))});
            this.cboAccount.Properties.NullText = resources.GetString("cboAccount.Properties.NullText");
            this.cboAccount.Properties.PopupWidth = 500;
            this.cboAccount.Properties.PopupWidthMode = DevExpress.XtraEditors.PopupWidthMode.ContentWidth;
            this.cboAccount.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoSearch;
            this.cboAccount.Required = false;
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // lblTAX_EXPENSE_ACCOUNT
            // 
            resources.ApplyResources(this.lblTAX_EXPENSE_ACCOUNT, "lblTAX_EXPENSE_ACCOUNT");
            this.lblTAX_EXPENSE_ACCOUNT.Name = "lblTAX_EXPENSE_ACCOUNT";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // txtAmount
            // 
            resources.ApplyResources(this.txtAmount, "txtAmount");
            this.txtAmount.Name = "txtAmount";
            // 
            // lblTAX_AMOUNT
            // 
            resources.ApplyResources(this.lblTAX_AMOUNT, "lblTAX_AMOUNT");
            this.lblTAX_AMOUNT.Name = "lblTAX_AMOUNT";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Name = "label2";
            // 
            // lblCOMPUTATION
            // 
            resources.ApplyResources(this.lblCOMPUTATION, "lblCOMPUTATION");
            this.lblCOMPUTATION.Name = "lblCOMPUTATION";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // txtTaxName
            // 
            resources.ApplyResources(this.txtTaxName, "txtTaxName");
            this.txtTaxName.Name = "txtTaxName";
            // 
            // lblTAX_NAME
            // 
            resources.ApplyResources(this.lblTAX_NAME, "lblTAX_NAME");
            this.lblTAX_NAME.Name = "lblTAX_NAME";
            // 
            // DialogTax
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogTax";
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboComputation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboAccount.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Panel panel2;
        private ExButton btnCHANGE_LOG;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Panel panel3;
        private Panel panel1;
        private Label label1;
        private TextBox txtTaxName;
        private Label lblTAX_NAME;
        private Label label4;
        private Label lblTAX_EXPENSE_ACCOUNT;
        private Label label3;
        private TextBox txtAmount;
        private Label lblTAX_AMOUNT;
        private Label label2;
        private Label lblCOMPUTATION;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboAccount;
        private HB01.Helpers.DevExpressCustomize.CustmizeComponent.DLookUpEdit cboComputation;
    }
}