﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageCustomerType
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageCustomerType));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.CUSTOMER_TYPE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_ACTIVE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.INCOME_ACCOUNT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEPOSIT_USAGE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DAILY_SUPPLY_HOUR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DAILY_SUPPLY_SCHEDULE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INCOME_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_TYPE_ID,
            this.IS_ACTIVE,
            this.INCOME_ACCOUNT_ID,
            this.CUSTOMER_TYPE,
            this.DEPOSIT_USAGE,
            this.DAILY_SUPPLY_HOUR,
            this.DAILY_SUPPLY_SCHEDULE,
            this.INCOME_ACCOUNT,
            this.NOTE});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // CUSTOMER_TYPE_ID
            // 
            this.CUSTOMER_TYPE_ID.DataPropertyName = "CUSTOMER_TYPE_ID";
            resources.ApplyResources(this.CUSTOMER_TYPE_ID, "CUSTOMER_TYPE_ID");
            this.CUSTOMER_TYPE_ID.Name = "CUSTOMER_TYPE_ID";
            // 
            // IS_ACTIVE
            // 
            this.IS_ACTIVE.DataPropertyName = "IS_ACTIVE";
            resources.ApplyResources(this.IS_ACTIVE, "IS_ACTIVE");
            this.IS_ACTIVE.Name = "IS_ACTIVE";
            // 
            // INCOME_ACCOUNT_ID
            // 
            this.INCOME_ACCOUNT_ID.DataPropertyName = "INCOME_ACCOUNT_ID";
            resources.ApplyResources(this.INCOME_ACCOUNT_ID, "INCOME_ACCOUNT_ID");
            this.INCOME_ACCOUNT_ID.Name = "INCOME_ACCOUNT_ID";
            // 
            // CUSTOMER_TYPE
            // 
            this.CUSTOMER_TYPE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_TYPE.DataPropertyName = "CUSTOMER_TYPE_NAME";
            resources.ApplyResources(this.CUSTOMER_TYPE, "CUSTOMER_TYPE");
            this.CUSTOMER_TYPE.Name = "CUSTOMER_TYPE";
            // 
            // DEPOSIT_USAGE
            // 
            this.DEPOSIT_USAGE.DataPropertyName = "DEPOSIT_USAGE";
            dataGridViewCellStyle2.Format = "0";
            this.DEPOSIT_USAGE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.DEPOSIT_USAGE, "DEPOSIT_USAGE");
            this.DEPOSIT_USAGE.Name = "DEPOSIT_USAGE";
            // 
            // DAILY_SUPPLY_HOUR
            // 
            this.DAILY_SUPPLY_HOUR.DataPropertyName = "DAILY_SUPPLY_HOUR";
            resources.ApplyResources(this.DAILY_SUPPLY_HOUR, "DAILY_SUPPLY_HOUR");
            this.DAILY_SUPPLY_HOUR.Name = "DAILY_SUPPLY_HOUR";
            // 
            // DAILY_SUPPLY_SCHEDULE
            // 
            this.DAILY_SUPPLY_SCHEDULE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.DAILY_SUPPLY_SCHEDULE.DataPropertyName = "DAILY_SUPPLY_SCHEDULE";
            resources.ApplyResources(this.DAILY_SUPPLY_SCHEDULE, "DAILY_SUPPLY_SCHEDULE");
            this.DAILY_SUPPLY_SCHEDULE.Name = "DAILY_SUPPLY_SCHEDULE";
            // 
            // INCOME_ACCOUNT
            // 
            this.INCOME_ACCOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.INCOME_ACCOUNT.DataPropertyName = "INCOME_ACCOUNT_NAME";
            resources.ApplyResources(this.INCOME_ACCOUNT, "INCOME_ACCOUNT");
            this.INCOME_ACCOUNT.Name = "INCOME_ACCOUNT";
            // 
            // NOTE
            // 
            this.NOTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NOTE.DataPropertyName = "NOTE";
            resources.ApplyResources(this.NOTE, "NOTE");
            this.NOTE.Name = "NOTE";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // flowLayoutPanel1
            // 
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Controls.Add(this.btnREMOVE);
            this.flowLayoutPanel1.Controls.Add(this.btnEDIT);
            this.flowLayoutPanel1.Controls.Add(this.btnADD);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txtQuickSearch_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // PageCustomerType
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageCustomerType";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        private ExButton btnREMOVE;
        private FlowLayoutPanel flowLayoutPanel1;
        private DataGridViewTextBoxColumn CUSTOMER_TYPE_ID;
        private DataGridViewCheckBoxColumn IS_ACTIVE;
        private DataGridViewTextBoxColumn INCOME_ACCOUNT_ID;
        private DataGridViewTextBoxColumn CUSTOMER_TYPE;
        private DataGridViewTextBoxColumn DEPOSIT_USAGE;
        private DataGridViewTextBoxColumn DAILY_SUPPLY_HOUR;
        private DataGridViewTextBoxColumn DAILY_SUPPLY_SCHEDULE;
        private DataGridViewTextBoxColumn INCOME_ACCOUNT;
        private DataGridViewTextBoxColumn NOTE;
    }
}
