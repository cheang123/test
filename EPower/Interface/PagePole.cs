﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class PagePole : Form
    {
        
        int areaID = 0;
        public int AreaID
        {
            get { return areaID; }
            private set { areaID = value; }
        }
        public TBL_POLE Pole
        {
            get
            {
                TBL_POLE objPole = null;
                if (dgvPole.SelectedRows.Count > 0)
                {
                    try
                    {
                        int intPoleID = (int)dgvPole.SelectedRows[0].Cells["POLE_ID"].Value;
                        objPole = DBDataContext.Db.TBL_POLEs.FirstOrDefault(x => x.POLE_ID == intPoleID && x.IS_ACTIVE);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objPole;
            }
        }

        #region Constructor
        public PagePole()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgvPole);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                if (cboArea.SelectedIndex != -1)
                {
                    areaID = (int)cboArea.SelectedValue;
                } 

                dgvPole.DataSource = from p in DBDataContext.Db.TBL_POLEs
                                     join a in DBDataContext.Db.TBL_AREAs on p.AREA_ID equals a.AREA_ID
                                     join t in DBDataContext.Db.TBL_TRANSFORMERs on p.TRANSFORMER_ID equals t.TRANSFORMER_ID 
                                     where  (p.IS_ACTIVE && a.IS_ACTIVE && (areaID == 0 || a.AREA_ID == areaID)) &&
                                            (p.POLE_CODE.ToLower()+a.AREA_NAME.ToLower()).Contains(txtQuickSearch.Text.ToLower().Trim())
                                     orderby p.POLE_CODE
                                     select new
                                     {
                                         p.POLE_ID,
                                         p.POLE_CODE,
                                         a.AREA_NAME,
                                         t.TRANSFORMER_CODE
                                     };
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        /// <summary>
        /// Add new pole.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        { 
            TBL_POLE objPole = new TBL_POLE()
            {
                POLE_CODE = "",
                STARTED_DATE=DBDataContext.Db.GetSystemDate(),
                END_DATE = UIHelper._DefaultDate,
                IS_PROPERTY = true,
                VILLAGE_CODE=DBDataContext.Db.TBL_COMPANies.FirstOrDefault().VILLAGE_CODE
            };
            if (cboArea.SelectedIndex!=-1)
            {
                objPole.AREA_ID = (int)cboArea.SelectedValue;
            }

            DialogPole dig = new DialogPole(GeneralProcess.Insert, objPole);
            if (dig.ShowDialog() == DialogResult.OK)
            {                
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgvPole, dig.Pole.POLE_ID);
            }
        }

        /// <summary>
        /// Edit pole.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvPole.SelectedRows.Count > 0)
            {
                DialogPole dig = new DialogPole(GeneralProcess.Update, Pole);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvPole, dig.Pole.POLE_ID);
                }
            }
        }

        /// <summary>
        /// Remove pole.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (IsPoleDeletable())
            {
                DialogPole dig = new DialogPole(GeneralProcess.Delete, Pole);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvPole, dig.Pole.POLE_ID - 1);
                }    
            }                
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void dgvPole_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvPole.SelectedRows.Count > 0)
            {
                int intPoleId = int.Parse(dgvPole.SelectedRows[0].Cells["POLE_ID"].Value.ToString());
                var box = from b in DBDataContext.Db.TBL_BOXes
                                    where b.POLE_ID == intPoleId
                                    orderby b.BOX_CODE
                                    select  new { b.BOX_ID, b.BOX_CODE,b.STATUS_ID};
                dgvBox.DataSource = box;

                if (box.Where(x=>x.STATUS_ID!=(int)BoxStatus.Used).Count()>0)
                {
                    foreach (DataGridViewRow r in dgvBox.Rows)
                    {
                        int sBox = (int)r.Cells[STATUS_ID.Name].Value;
                        if (sBox==(int)BoxStatus.Unavailable) 
                            r.DefaultCellStyle.ForeColor=Color.Red;
                        else if (sBox==(int)BoxStatus.Stock)
                            r.DefaultCellStyle.ForeColor = Color.Blue; 
                    }
                } 
            }
        }

        private void btnNewBox_Click(object sender, EventArgs e)
        {
            if (dgvPole.SelectedRows.Count > 0)
            {
                int intPoleId = (int)dgvPole.SelectedRows[0].Cells[POLE_ID.Name].Value;
                DialogBox dig = new DialogBox(GeneralProcess.Insert, new TBL_BOX()
                {
                    BOX_CODE = "",
                    POLE_ID = intPoleId,
                    STATUS_ID = (int)BoxStatus.Stock,
                    STARTED_DATE = DBDataContext.Db.GetSystemDate(),
                    END_DATE = UIHelper._DefaultDate,
                });
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgvPole, dig.Box.POLE_ID);
                    UIHelper.SelectRow(dgvBox, dig.Box.BOX_ID);
                }
            } 
        }

        private void btnEditBox_Click(object sender, EventArgs e)
        {
            if (dgvBox.SelectedRows.Count > 0)
            {
                int intBoxId = (int)dgvBox.SelectedRows[0].Cells["BOX_ID"].Value;
                TBL_BOX objBox = DBDataContext.Db.TBL_BOXes.Single(b => b.BOX_ID == intBoxId);
                DialogBox dig = new DialogBox(GeneralProcess.Update, objBox);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    dgvPole_SelectionChanged(null, null);
                    UIHelper.SelectRow(dgvPole, dig.Box.POLE_ID);
                    UIHelper.SelectRow(dgvBox, dig.Box.BOX_ID);
                }
            }
        }

        private void btnRemoveBox_Click(object sender, EventArgs e)
        {
            if (IsBoxDeletable())
            {
                int intBoxId = (int)dgvBox.SelectedRows[0].Cells["BOX_ID"].Value;
                TBL_BOX objBox = DBDataContext.Db.TBL_BOXes.Single(b => b.BOX_ID == intBoxId);
                DialogBox dig = new DialogBox(GeneralProcess.Delete, objBox);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    dgvPole_SelectionChanged(null, null);
                    UIHelper.SelectRow(dgvPole, dig.Box.POLE_ID);
                    UIHelper.SelectRow(dgvBox, dig.Box.BOX_ID - 1);
                }
            }
        }
        #endregion

        #region Method
        public bool IsBoxDeletable()
        {
            bool val = false;
            //TODO: check database;
            if (dgvBox.SelectedRows.Count > 0)
            {
                val = true;
                if (DBDataContext.Db.TBL_CUSTOMER_METERs.Where(x=>x.BOX_ID==(int)dgvBox.SelectedRows[0].Cells["BOX_ID"].Value && x.IS_ACTIVE).Count()>0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    val = false;
                }
                if (DBDataContext.Db.TBL_BOXes.Where(x => x.BOX_ID == (int)dgvBox.SelectedRows[0].Cells["BOX_ID"].Value && x.STATUS_ID==(int)BoxStatus.Unavailable).Count()>0){
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE_CUS_RECORD_ALREADY_REMOVE);
                    val = false;
                }
            }
            
            return val;
        }

        public bool IsPoleDeletable()
        {
            bool val = false;
            //TODO: check database;
            if (dgvPole.SelectedRows.Count>0)
            {
                val = true;
                if (DBDataContext.Db.TBL_BOXes.Where(x=>x.POLE_ID==Pole.POLE_ID && x.STATUS_ID==(int)BoxStatus.Used).Count()>0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    val = false;
                }
            }
            if(DBDataContext.Db.TBL_TRANSFORMERs.Where(x=>x.IS_ACTIVE && x.POLE_ID == Pole.POLE_ID).Count() > 0)
            {
                MsgBox.ShowInformation(string.Format(Resources.MSG_POLE_USING_ON_TRANSFO, DBDataContext.Db.TBL_TRANSFORMERs.FirstOrDefault(x => x.POLE_ID == Pole.POLE_ID).TRANSFORMER_CODE));
                val = false;
            }
            return val;
        }

        #endregion

        public void LoadArea()
        {
            int tempArea =  AreaID;
            DataTable dt = (from a in DBDataContext.Db.TBL_AREAs
                            where a.IS_ACTIVE
                            orderby a.AREA_NAME 
                            select a)._ToDataTable();
            //Create Row Templet
            DataRow dr = dt.NewRow();
            dr["AREA_ID"] = 0;
            dr["AREA_NAME"] =Resources.ALL_AREA;
            dt.Rows.InsertAt(dr, 0);

            UIHelper.SetDataSourceToComboBox(cboArea, dt);
             cboArea.SelectedValue = tempArea;
        }

        private void btnCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvBox.SelectedRows.Count > 0)
                {
                    int intBoxId = (int)dgvBox.SelectedRows[0].Cells[BOX_ID.Name].Value;
                    new DialogCustomerInBox(intBoxId).ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void dgvPole_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }

        private void dgvBox_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEditBox_Click(null, null);
        }
    }
}
