﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogReceivePayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogReceivePayment));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.txtTotalBalance = new System.Windows.Forms.TextBox();
            this.lblDUE_AMOUNT = new System.Windows.Forms.Label();
            this.txtTotalDue = new System.Windows.Forms.TextBox();
            this.txtTotalPay = new System.Windows.Forms.TextBox();
            this.lblPAY_AMOUNT = new System.Windows.Forms.Label();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.lblArea = new System.Windows.Forms.Label();
            this.txtAreaName = new System.Windows.Forms.TextBox();
            this.txtCustomerCode = new SoftTech.Component.ExTextbox();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.lblCUSTOMER_INFO = new System.Windows.Forms.Label();
            this.lblINVOICE_TO_PAY = new System.Windows.Forms.Label();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblPAY_DATE = new System.Windows.Forms.Label();
            this.lblTOTAL_AMOUNT = new System.Windows.Forms.Label();
            this.txtCustomerName = new SoftTech.Component.ExTextbox();
            this.txtMeterCode = new SoftTech.Component.ExTextbox();
            this.txtPole = new System.Windows.Forms.TextBox();
            this.lblPole = new System.Windows.Forms.Label();
            this.txtBox = new System.Windows.Forms.TextBox();
            this.lblBOX = new System.Windows.Forms.Label();
            this.lblCASHIER = new System.Windows.Forms.Label();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.chkPARTIAL_PAYMENT = new System.Windows.Forms.CheckBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.INVOICE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CHK_ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.INVOICE_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_DATE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INVOICE_TITLE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SETTLE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAID_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DUE_AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_INVOICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FORMAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkPRINT_RECEIPT = new System.Windows.Forms.CheckBox();
            this.lblREADING_TEXT_ = new System.Windows.Forms.Label();
            this.chkCHECK_ALL_ = new System.Windows.Forms.CheckBox();
            this.chkAUTO_FILL_DUE_AMOUNT = new System.Windows.Forms.CheckBox();
            this.txtSignTotalDue = new System.Windows.Forms.TextBox();
            this.txtSignTotalBalance = new System.Windows.Forms.TextBox();
            this.txtSignTotalPay = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.lblCURRENCY = new System.Windows.Forms.Label();
            this.lblPAYMENT_METHOD = new System.Windows.Forms.Label();
            this.txtSignDepositBalance = new System.Windows.Forms.TextBox();
            this.txtPrepayBalance = new System.Windows.Forms.TextBox();
            this.lblPREPAYMENT_AMOUNT = new System.Windows.Forms.Label();
            this.cboPaymentMethod = new System.Windows.Forms.ComboBox();
            this.txtPayDate = new DevExpress.XtraEditors.DateEdit();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.txtPayDate);
            this.content.Controls.Add(this.cboPaymentMethod);
            this.content.Controls.Add(this.txtSignDepositBalance);
            this.content.Controls.Add(this.txtPrepayBalance);
            this.content.Controls.Add(this.lblPREPAYMENT_AMOUNT);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.cboCurrency);
            this.content.Controls.Add(this.lblPAYMENT_METHOD);
            this.content.Controls.Add(this.lblCURRENCY);
            this.content.Controls.Add(this.txtSignTotalPay);
            this.content.Controls.Add(this.txtSignTotalBalance);
            this.content.Controls.Add(this.txtSignTotalDue);
            this.content.Controls.Add(this.chkAUTO_FILL_DUE_AMOUNT);
            this.content.Controls.Add(this.chkCHECK_ALL_);
            this.content.Controls.Add(this.chkPRINT_RECEIPT);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.lblREADING_TEXT_);
            this.content.Controls.Add(this.chkPARTIAL_PAYMENT);
            this.content.Controls.Add(this.txtLogin);
            this.content.Controls.Add(this.lblCASHIER);
            this.content.Controls.Add(this.txtBox);
            this.content.Controls.Add(this.lblBOX);
            this.content.Controls.Add(this.txtPole);
            this.content.Controls.Add(this.lblPole);
            this.content.Controls.Add(this.txtMeterCode);
            this.content.Controls.Add(this.txtCustomerName);
            this.content.Controls.Add(this.lblPAY_DATE);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblINVOICE_TO_PAY);
            this.content.Controls.Add(this.lblCUSTOMER_INFO);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.txtCustomerCode);
            this.content.Controls.Add(this.txtAreaName);
            this.content.Controls.Add(this.lblArea);
            this.content.Controls.Add(this.txtTotalBalance);
            this.content.Controls.Add(this.lblDUE_AMOUNT);
            this.content.Controls.Add(this.txtTotalDue);
            this.content.Controls.Add(this.txtTotalPay);
            this.content.Controls.Add(this.lblPAY_AMOUNT);
            this.content.Controls.Add(this.lblTOTAL_AMOUNT);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblPAY_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtTotalPay, 0);
            this.content.Controls.SetChildIndex(this.txtTotalDue, 0);
            this.content.Controls.SetChildIndex(this.lblDUE_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtTotalBalance, 0);
            this.content.Controls.SetChildIndex(this.lblArea, 0);
            this.content.Controls.SetChildIndex(this.txtAreaName, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCode, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_INFO, 0);
            this.content.Controls.SetChildIndex(this.lblINVOICE_TO_PAY, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblPAY_DATE, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerName, 0);
            this.content.Controls.SetChildIndex(this.txtMeterCode, 0);
            this.content.Controls.SetChildIndex(this.lblPole, 0);
            this.content.Controls.SetChildIndex(this.txtPole, 0);
            this.content.Controls.SetChildIndex(this.lblBOX, 0);
            this.content.Controls.SetChildIndex(this.txtBox, 0);
            this.content.Controls.SetChildIndex(this.lblCASHIER, 0);
            this.content.Controls.SetChildIndex(this.txtLogin, 0);
            this.content.Controls.SetChildIndex(this.chkPARTIAL_PAYMENT, 0);
            this.content.Controls.SetChildIndex(this.lblREADING_TEXT_, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.chkPRINT_RECEIPT, 0);
            this.content.Controls.SetChildIndex(this.chkCHECK_ALL_, 0);
            this.content.Controls.SetChildIndex(this.chkAUTO_FILL_DUE_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtSignTotalDue, 0);
            this.content.Controls.SetChildIndex(this.txtSignTotalBalance, 0);
            this.content.Controls.SetChildIndex(this.txtSignTotalPay, 0);
            this.content.Controls.SetChildIndex(this.lblCURRENCY, 0);
            this.content.Controls.SetChildIndex(this.lblPAYMENT_METHOD, 0);
            this.content.Controls.SetChildIndex(this.cboCurrency, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.lblPREPAYMENT_AMOUNT, 0);
            this.content.Controls.SetChildIndex(this.txtPrepayBalance, 0);
            this.content.Controls.SetChildIndex(this.txtSignDepositBalance, 0);
            this.content.Controls.SetChildIndex(this.cboPaymentMethod, 0);
            this.content.Controls.SetChildIndex(this.txtPayDate, 0);
            // 
            // txtTotalBalance
            // 
            resources.ApplyResources(this.txtTotalBalance, "txtTotalBalance");
            this.txtTotalBalance.Name = "txtTotalBalance";
            this.txtTotalBalance.ReadOnly = true;
            this.txtTotalBalance.TabStop = false;
            // 
            // lblDUE_AMOUNT
            // 
            resources.ApplyResources(this.lblDUE_AMOUNT, "lblDUE_AMOUNT");
            this.lblDUE_AMOUNT.Name = "lblDUE_AMOUNT";
            // 
            // txtTotalDue
            // 
            resources.ApplyResources(this.txtTotalDue, "txtTotalDue");
            this.txtTotalDue.Name = "txtTotalDue";
            this.txtTotalDue.ReadOnly = true;
            this.txtTotalDue.TabStop = false;
            // 
            // txtTotalPay
            // 
            resources.ApplyResources(this.txtTotalPay, "txtTotalPay");
            this.txtTotalPay.Name = "txtTotalPay";
            this.txtTotalPay.TextChanged += new System.EventHandler(this.txtPayAmount_TextChanged);
            this.txtTotalPay.Enter += new System.EventHandler(this.InputEnglish);
            this.txtTotalPay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPayAmount_KeyDown);
            // 
            // lblPAY_AMOUNT
            // 
            resources.ApplyResources(this.lblPAY_AMOUNT, "lblPAY_AMOUNT");
            this.lblPAY_AMOUNT.Name = "lblPAY_AMOUNT";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // lblArea
            // 
            resources.ApplyResources(this.lblArea, "lblArea");
            this.lblArea.Name = "lblArea";
            // 
            // txtAreaName
            // 
            resources.ApplyResources(this.txtAreaName, "txtAreaName");
            this.txtAreaName.Name = "txtAreaName";
            // 
            // txtCustomerCode
            // 
            this.txtCustomerCode.BackColor = System.Drawing.Color.White;
            this.txtCustomerCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerCode.AdvanceSearch += new System.EventHandler(this.txtCustomerCode_AdvanceSearch);
            this.txtCustomerCode.CancelAdvanceSearch += new System.EventHandler(this.txtMeterCode_CancelAdvanceSearch);
            this.txtCustomerCode.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // lblCUSTOMER_INFO
            // 
            resources.ApplyResources(this.lblCUSTOMER_INFO, "lblCUSTOMER_INFO");
            this.lblCUSTOMER_INFO.Name = "lblCUSTOMER_INFO";
            // 
            // lblINVOICE_TO_PAY
            // 
            resources.ApplyResources(this.lblINVOICE_TO_PAY, "lblINVOICE_TO_PAY");
            this.lblINVOICE_TO_PAY.Name = "lblINVOICE_TO_PAY";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblPAY_DATE
            // 
            resources.ApplyResources(this.lblPAY_DATE, "lblPAY_DATE");
            this.lblPAY_DATE.Name = "lblPAY_DATE";
            // 
            // lblTOTAL_AMOUNT
            // 
            resources.ApplyResources(this.lblTOTAL_AMOUNT, "lblTOTAL_AMOUNT");
            this.lblTOTAL_AMOUNT.Name = "lblTOTAL_AMOUNT";
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.BackColor = System.Drawing.Color.White;
            this.txtCustomerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerName, "txtCustomerName");
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerName.AdvanceSearch += new System.EventHandler(this.txtCustomerName_AdvanceSearch);
            this.txtCustomerName.CancelAdvanceSearch += new System.EventHandler(this.txtMeterCode_CancelAdvanceSearch);
            this.txtCustomerName.Enter += new System.EventHandler(this.InputKhmer);
            // 
            // txtMeterCode
            // 
            this.txtMeterCode.BackColor = System.Drawing.Color.White;
            this.txtMeterCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtMeterCode, "txtMeterCode");
            this.txtMeterCode.Name = "txtMeterCode";
            this.txtMeterCode.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtMeterCode.AdvanceSearch += new System.EventHandler(this.txtMeterCode_AdvanceSearch);
            this.txtMeterCode.CancelAdvanceSearch += new System.EventHandler(this.txtMeterCode_CancelAdvanceSearch);
            this.txtMeterCode.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtPole
            // 
            resources.ApplyResources(this.txtPole, "txtPole");
            this.txtPole.Name = "txtPole";
            // 
            // lblPole
            // 
            resources.ApplyResources(this.lblPole, "lblPole");
            this.lblPole.Name = "lblPole";
            // 
            // txtBox
            // 
            resources.ApplyResources(this.txtBox, "txtBox");
            this.txtBox.Name = "txtBox";
            // 
            // lblBOX
            // 
            resources.ApplyResources(this.lblBOX, "lblBOX");
            this.lblBOX.Name = "lblBOX";
            // 
            // lblCASHIER
            // 
            resources.ApplyResources(this.lblCASHIER, "lblCASHIER");
            this.lblCASHIER.Name = "lblCASHIER";
            // 
            // txtLogin
            // 
            resources.ApplyResources(this.txtLogin, "txtLogin");
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.ReadOnly = true;
            this.txtLogin.TabStop = false;
            // 
            // chkPARTIAL_PAYMENT
            // 
            resources.ApplyResources(this.chkPARTIAL_PAYMENT, "chkPARTIAL_PAYMENT");
            this.chkPARTIAL_PAYMENT.Checked = true;
            this.chkPARTIAL_PAYMENT.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPARTIAL_PAYMENT.Name = "chkPARTIAL_PAYMENT";
            this.chkPARTIAL_PAYMENT.UseVisualStyleBackColor = true;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.INVOICE_ID,
            this.CHK_,
            this.INVOICE_NO,
            this.INVOICE_DATE,
            this.INVOICE_TITLE,
            this.SETTLE_AMOUNT,
            this.PAID_AMOUNT,
            this.DUE_AMOUNT,
            this.CURRENCY_SING_,
            this.IS_INVOICE,
            this.CURRENCY_ID,
            this.FORMAT});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            this.dgv.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgv_CellFormatting);
            // 
            // INVOICE_ID
            // 
            this.INVOICE_ID.DataPropertyName = "INVOICE_ID";
            resources.ApplyResources(this.INVOICE_ID, "INVOICE_ID");
            this.INVOICE_ID.Name = "INVOICE_ID";
            // 
            // CHK_
            // 
            this.CHK_.DataPropertyName = "IS_PAY";
            resources.ApplyResources(this.CHK_, "CHK_");
            this.CHK_.Name = "CHK_";
            // 
            // INVOICE_NO
            // 
            this.INVOICE_NO.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.INVOICE_NO.DataPropertyName = "INVOICE_NO";
            resources.ApplyResources(this.INVOICE_NO, "INVOICE_NO");
            this.INVOICE_NO.Name = "INVOICE_NO";
            this.INVOICE_NO.ReadOnly = true;
            // 
            // INVOICE_DATE
            // 
            this.INVOICE_DATE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.INVOICE_DATE.DataPropertyName = "INVOICE_DATE";
            dataGridViewCellStyle2.Format = "dd-MM-yyyy";
            this.INVOICE_DATE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.INVOICE_DATE, "INVOICE_DATE");
            this.INVOICE_DATE.Name = "INVOICE_DATE";
            this.INVOICE_DATE.ReadOnly = true;
            // 
            // INVOICE_TITLE
            // 
            this.INVOICE_TITLE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.INVOICE_TITLE.DataPropertyName = "INVOICE_TITLE";
            resources.ApplyResources(this.INVOICE_TITLE, "INVOICE_TITLE");
            this.INVOICE_TITLE.Name = "INVOICE_TITLE";
            this.INVOICE_TITLE.ReadOnly = true;
            // 
            // SETTLE_AMOUNT
            // 
            this.SETTLE_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.SETTLE_AMOUNT.DataPropertyName = "SETTLE_AMOUNT";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.NullValue = null;
            this.SETTLE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.SETTLE_AMOUNT, "SETTLE_AMOUNT");
            this.SETTLE_AMOUNT.Name = "SETTLE_AMOUNT";
            this.SETTLE_AMOUNT.ReadOnly = true;
            // 
            // PAID_AMOUNT
            // 
            this.PAID_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.PAID_AMOUNT.DataPropertyName = "PAID_AMOUNT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.NullValue = null;
            this.PAID_AMOUNT.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.PAID_AMOUNT, "PAID_AMOUNT");
            this.PAID_AMOUNT.Name = "PAID_AMOUNT";
            this.PAID_AMOUNT.ReadOnly = true;
            // 
            // DUE_AMOUNT
            // 
            this.DUE_AMOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.DUE_AMOUNT.DataPropertyName = "DUE_AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.DUE_AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.DUE_AMOUNT, "DUE_AMOUNT");
            this.DUE_AMOUNT.Name = "DUE_AMOUNT";
            this.DUE_AMOUNT.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            // 
            // IS_INVOICE
            // 
            this.IS_INVOICE.DataPropertyName = "IS_INVOICE";
            resources.ApplyResources(this.IS_INVOICE, "IS_INVOICE");
            this.IS_INVOICE.Name = "IS_INVOICE";
            this.IS_INVOICE.ReadOnly = true;
            this.IS_INVOICE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IS_INVOICE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CURRENCY_ID
            // 
            this.CURRENCY_ID.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.CURRENCY_ID, "CURRENCY_ID");
            this.CURRENCY_ID.Name = "CURRENCY_ID";
            // 
            // FORMAT
            // 
            this.FORMAT.DataPropertyName = "FORMAT";
            resources.ApplyResources(this.FORMAT, "FORMAT");
            this.FORMAT.Name = "FORMAT";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // chkPRINT_RECEIPT
            // 
            resources.ApplyResources(this.chkPRINT_RECEIPT, "chkPRINT_RECEIPT");
            this.chkPRINT_RECEIPT.Name = "chkPRINT_RECEIPT";
            this.chkPRINT_RECEIPT.UseVisualStyleBackColor = true;
            this.chkPRINT_RECEIPT.CheckedChanged += new System.EventHandler(this.chkPrintReceipt_CheckedChanged);
            // 
            // lblREADING_TEXT_
            // 
            resources.ApplyResources(this.lblREADING_TEXT_, "lblREADING_TEXT_");
            this.lblREADING_TEXT_.Name = "lblREADING_TEXT_";
            // 
            // chkCHECK_ALL_
            // 
            resources.ApplyResources(this.chkCHECK_ALL_, "chkCHECK_ALL_");
            this.chkCHECK_ALL_.Checked = true;
            this.chkCHECK_ALL_.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCHECK_ALL_.Name = "chkCHECK_ALL_";
            this.chkCHECK_ALL_.UseVisualStyleBackColor = true;
            this.chkCHECK_ALL_.CheckedChanged += new System.EventHandler(this.chkCheckAll_CheckedChanged);
            // 
            // chkAUTO_FILL_DUE_AMOUNT
            // 
            resources.ApplyResources(this.chkAUTO_FILL_DUE_AMOUNT, "chkAUTO_FILL_DUE_AMOUNT");
            this.chkAUTO_FILL_DUE_AMOUNT.Checked = true;
            this.chkAUTO_FILL_DUE_AMOUNT.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAUTO_FILL_DUE_AMOUNT.Name = "chkAUTO_FILL_DUE_AMOUNT";
            this.chkAUTO_FILL_DUE_AMOUNT.UseVisualStyleBackColor = true;
            this.chkAUTO_FILL_DUE_AMOUNT.CheckedChanged += new System.EventHandler(this.chkAUTO_FILL_DUE_AMOUNT_CheckedChanged);
            // 
            // txtSignTotalDue
            // 
            resources.ApplyResources(this.txtSignTotalDue, "txtSignTotalDue");
            this.txtSignTotalDue.Name = "txtSignTotalDue";
            this.txtSignTotalDue.ReadOnly = true;
            this.txtSignTotalDue.TabStop = false;
            // 
            // txtSignTotalBalance
            // 
            resources.ApplyResources(this.txtSignTotalBalance, "txtSignTotalBalance");
            this.txtSignTotalBalance.Name = "txtSignTotalBalance";
            this.txtSignTotalBalance.ReadOnly = true;
            this.txtSignTotalBalance.TabStop = false;
            // 
            // txtSignTotalPay
            // 
            resources.ApplyResources(this.txtSignTotalPay, "txtSignTotalPay");
            this.txtSignTotalPay.Name = "txtSignTotalPay";
            this.txtSignTotalPay.ReadOnly = true;
            this.txtSignTotalPay.TabStop = false;
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedValueChanged += new System.EventHandler(this.cboCurrency_SelectedValueChanged);
            // 
            // lblCURRENCY
            // 
            resources.ApplyResources(this.lblCURRENCY, "lblCURRENCY");
            this.lblCURRENCY.Name = "lblCURRENCY";
            // 
            // lblPAYMENT_METHOD
            // 
            resources.ApplyResources(this.lblPAYMENT_METHOD, "lblPAYMENT_METHOD");
            this.lblPAYMENT_METHOD.Name = "lblPAYMENT_METHOD";
            // 
            // txtSignDepositBalance
            // 
            resources.ApplyResources(this.txtSignDepositBalance, "txtSignDepositBalance");
            this.txtSignDepositBalance.Name = "txtSignDepositBalance";
            this.txtSignDepositBalance.ReadOnly = true;
            this.txtSignDepositBalance.TabStop = false;
            // 
            // txtPrepayBalance
            // 
            resources.ApplyResources(this.txtPrepayBalance, "txtPrepayBalance");
            this.txtPrepayBalance.Name = "txtPrepayBalance";
            this.txtPrepayBalance.ReadOnly = true;
            this.txtPrepayBalance.TabStop = false;
            // 
            // lblPREPAYMENT_AMOUNT
            // 
            resources.ApplyResources(this.lblPREPAYMENT_AMOUNT, "lblPREPAYMENT_AMOUNT");
            this.lblPREPAYMENT_AMOUNT.Name = "lblPREPAYMENT_AMOUNT";
            // 
            // cboPaymentMethod
            // 
            this.cboPaymentMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPaymentMethod.FormattingEnabled = true;
            resources.ApplyResources(this.cboPaymentMethod, "cboPaymentMethod");
            this.cboPaymentMethod.Name = "cboPaymentMethod";
            // 
            // txtPayDate
            // 
            resources.ApplyResources(this.txtPayDate, "txtPayDate");
            this.txtPayDate.Name = "txtPayDate";
            this.txtPayDate.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.Appearance.Font")));
            this.txtPayDate.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtPayDate.Properties.Appearance.Options.UseFont = true;
            this.txtPayDate.Properties.Appearance.Options.UseForeColor = true;
            this.txtPayDate.Properties.AppearanceCalendar.Button.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.Button.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.Button.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.ButtonHighlighted.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.ButtonHighlighted.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.ButtonHighlighted.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.ButtonPressed.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.ButtonPressed.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.ButtonPressed.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.CalendarHeader.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.CalendarHeader.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.CalendarHeader.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.DayCell.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.DayCell.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.DayCell.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.DayCellDisabled.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.DayCellDisabled.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.DayCellDisabled.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.DayCellHighlighted.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.DayCellHighlighted.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.DayCellHighlighted.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.DayCellHoliday.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.DayCellHoliday.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.DayCellHoliday.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.DayCellInactive.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.DayCellInactive.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.DayCellInactive.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.DayCellPressed.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.DayCellPressed.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.DayCellPressed.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.DayCellSelected.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.DayCellSelected.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.DayCellSelected.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.DayCellSpecial.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.DayCellSpecial.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.DayCellSpecial.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.DayCellSpecialHighlighted.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.DayCellSpecialHighlighted.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.DayCellSpecialHighlighted.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.DayCellSpecialPressed.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.DayCellSpecialPressed.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.DayCellSpecialPressed.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.DayCellSpecialSelected.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.DayCellSpecialSelected.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.DayCellSpecialSelected.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.DayCellToday.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.DayCellToday.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.DayCellToday.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.Header.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.Header.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.Header.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.HeaderHighlighted.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.HeaderHighlighted.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.HeaderHighlighted.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.HeaderPressed.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.HeaderPressed.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.HeaderPressed.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.WeekDay.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.WeekDay.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.WeekDay.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceCalendar.WeekNumber.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceCalendar.WeekNumber.Font")));
            this.txtPayDate.Properties.AppearanceCalendar.WeekNumber.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceDisabled.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceDisabled.Font")));
            this.txtPayDate.Properties.AppearanceDisabled.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceDropDown.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceDropDown.Font")));
            this.txtPayDate.Properties.AppearanceDropDown.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceFocused.Font")));
            this.txtPayDate.Properties.AppearanceFocused.Options.UseFont = true;
            this.txtPayDate.Properties.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.AppearanceReadOnly.Font")));
            this.txtPayDate.Properties.AppearanceReadOnly.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject1, "serializableAppearanceObject1");
            serializableAppearanceObject1.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject2, "serializableAppearanceObject2");
            serializableAppearanceObject2.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject3, "serializableAppearanceObject3");
            serializableAppearanceObject3.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject4, "serializableAppearanceObject4");
            serializableAppearanceObject4.Options.UseFont = true;
            this.txtPayDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("txtPayDate.Properties.Buttons"))), resources.GetString("txtPayDate.Properties.Buttons1"), ((int)(resources.GetObject("txtPayDate.Properties.Buttons2"))), ((bool)(resources.GetObject("txtPayDate.Properties.Buttons3"))), ((bool)(resources.GetObject("txtPayDate.Properties.Buttons4"))), ((bool)(resources.GetObject("txtPayDate.Properties.Buttons5"))), editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, resources.GetString("txtPayDate.Properties.Buttons6"), ((object)(resources.GetObject("txtPayDate.Properties.Buttons7"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("txtPayDate.Properties.Buttons8"))), ((DevExpress.Utils.ToolTipAnchor)(resources.GetObject("txtPayDate.Properties.Buttons9"))))});
            this.txtPayDate.Properties.CalendarTimeProperties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.CalendarTimeProperties.Appearance.Font")));
            this.txtPayDate.Properties.CalendarTimeProperties.Appearance.Options.UseFont = true;
            this.txtPayDate.Properties.CalendarTimeProperties.AppearanceDisabled.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.CalendarTimeProperties.AppearanceDisabled.Font")));
            this.txtPayDate.Properties.CalendarTimeProperties.AppearanceDisabled.Options.UseFont = true;
            this.txtPayDate.Properties.CalendarTimeProperties.AppearanceFocused.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.CalendarTimeProperties.AppearanceFocused.Font")));
            this.txtPayDate.Properties.CalendarTimeProperties.AppearanceFocused.Options.UseFont = true;
            this.txtPayDate.Properties.CalendarTimeProperties.AppearanceReadOnly.Font = ((System.Drawing.Font)(resources.GetObject("txtPayDate.Properties.CalendarTimeProperties.AppearanceReadOnly.Font")));
            this.txtPayDate.Properties.CalendarTimeProperties.AppearanceReadOnly.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject5, "serializableAppearanceObject5");
            serializableAppearanceObject5.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject6, "serializableAppearanceObject6");
            serializableAppearanceObject6.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject7, "serializableAppearanceObject7");
            serializableAppearanceObject7.Options.UseFont = true;
            resources.ApplyResources(serializableAppearanceObject8, "serializableAppearanceObject8");
            serializableAppearanceObject8.Options.UseFont = true;
            this.txtPayDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("txtPayDate.Properties.CalendarTimeProperties.Buttons"))), resources.GetString("txtPayDate.Properties.CalendarTimeProperties.Buttons1"), ((int)(resources.GetObject("txtPayDate.Properties.CalendarTimeProperties.Buttons2"))), ((bool)(resources.GetObject("txtPayDate.Properties.CalendarTimeProperties.Buttons3"))), ((bool)(resources.GetObject("txtPayDate.Properties.CalendarTimeProperties.Buttons4"))), ((bool)(resources.GetObject("txtPayDate.Properties.CalendarTimeProperties.Buttons5"))), editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, resources.GetString("txtPayDate.Properties.CalendarTimeProperties.Buttons6"), ((object)(resources.GetObject("txtPayDate.Properties.CalendarTimeProperties.Buttons7"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("txtPayDate.Properties.CalendarTimeProperties.Buttons8"))), ((DevExpress.Utils.ToolTipAnchor)(resources.GetObject("txtPayDate.Properties.CalendarTimeProperties.Buttons9"))))});
            this.txtPayDate.Properties.DisplayFormat.FormatString = "dd-MM​-yyyy HH:mm:ss";
            this.txtPayDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtPayDate.Properties.EditFormat.FormatString = "dd-MM​-yyyy HH:mm:ss";
            this.txtPayDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtPayDate.Properties.Mask.EditMask = resources.GetString("txtPayDate.Properties.Mask.EditMask");
            this.txtPayDate.EditValueChanged += new System.EventHandler(this.txtPayDate_EditValueChanged);
            this.txtPayDate.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // DialogReceivePayment
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Image = global::EPower.Properties.Resources.icon_payment;
            this.Name = "DialogReceivePayment";
            this.Activated += new System.EventHandler(this.DialogReceivePayment_Activated_1);
            this.Load += new System.EventHandler(this.DialogReceivePayment_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayDate.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox txtTotalBalance;
        private Label lblDUE_AMOUNT;
        private TextBox txtTotalDue;
        private TextBox txtTotalPay;
        private Label lblPAY_AMOUNT;
        private Label lblCUSTOMER_NAME;
        private Label lblCUSTOMER_CODE;
        private Label lblArea;
        private TextBox txtAreaName;
        private ExTextbox txtCustomerCode;
        private Label lblINVOICE_TO_PAY;
        private Label lblCUSTOMER_INFO;
        private Label lblMETER_CODE;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblPAY_DATE;
        private Label lblTOTAL_AMOUNT;
        private TextBox txtBox;
        private Label lblBOX;
        private TextBox txtPole;
        private Label lblPole;
        private ExTextbox txtMeterCode;
        private ExTextbox txtCustomerName;
        private TextBox txtLogin;
        private Label lblCASHIER;
        private CheckBox chkPARTIAL_PAYMENT;
        private DataGridView dgv;
        private Panel panel1;
        private CheckBox chkPRINT_RECEIPT;
        private Label lblREADING_TEXT_;
        private CheckBox chkCHECK_ALL_;
        private CheckBox chkAUTO_FILL_DUE_AMOUNT;
        private TextBox txtSignTotalPay;
        private TextBox txtSignTotalBalance;
        private TextBox txtSignTotalDue;
        private Label label12;
        private ComboBox cboCurrency;
        private Label lblCURRENCY;
        private Label lblPAYMENT_METHOD;
        private TextBox txtSignDepositBalance;
        private TextBox txtPrepayBalance;
        private Label lblPREPAYMENT_AMOUNT;
        private DataGridViewTextBoxColumn INVOICE_ID;
        private DataGridViewCheckBoxColumn CHK_;
        private DataGridViewTextBoxColumn INVOICE_NO;
        private DataGridViewTextBoxColumn INVOICE_DATE;
        private DataGridViewTextBoxColumn INVOICE_TITLE;
        private DataGridViewTextBoxColumn SETTLE_AMOUNT;
        private DataGridViewTextBoxColumn PAID_AMOUNT;
        private DataGridViewTextBoxColumn DUE_AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private DataGridViewTextBoxColumn IS_INVOICE;
        private DataGridViewTextBoxColumn CURRENCY_ID;
        private DataGridViewTextBoxColumn FORMAT;
        private ComboBox cboPaymentMethod;
        private DevExpress.XtraEditors.DateEdit txtPayDate;
    }
}