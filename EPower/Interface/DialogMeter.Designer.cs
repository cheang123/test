﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogMeter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogMeter));
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.txtMeterCode = new System.Windows.Forms.TextBox();
            this.lblMETER_TYPE = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.cboMeterType = new System.Windows.Forms.ComboBox();
            this.lblSTATUS = new System.Windows.Forms.Label();
            this.cboStatus = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.btnAddMeterType = new SoftTech.Component.ExAddItem();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMULTIPLIER = new System.Windows.Forms.TextBox();
            this.lblMULTIPLIER = new System.Windows.Forms.Label();
            this.lblREASON = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboReason = new System.Windows.Forms.ComboBox();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.cboReason);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.lblREASON);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.txtMULTIPLIER);
            this.content.Controls.Add(this.lblMULTIPLIER);
            this.content.Controls.Add(this.btnAddMeterType);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.cboStatus);
            this.content.Controls.Add(this.lblSTATUS);
            this.content.Controls.Add(this.cboMeterType);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.lblMETER_TYPE);
            this.content.Controls.Add(this.txtMeterCode);
            this.content.Controls.Add(this.lblMETER_CODE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtMeterCode, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.cboMeterType, 0);
            this.content.Controls.SetChildIndex(this.lblSTATUS, 0);
            this.content.Controls.SetChildIndex(this.cboStatus, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.btnAddMeterType, 0);
            this.content.Controls.SetChildIndex(this.lblMULTIPLIER, 0);
            this.content.Controls.SetChildIndex(this.txtMULTIPLIER, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.lblREASON, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.cboReason, 0);
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // txtMeterCode
            // 
            resources.ApplyResources(this.txtMeterCode, "txtMeterCode");
            this.txtMeterCode.Name = "txtMeterCode";
            this.txtMeterCode.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblMETER_TYPE
            // 
            resources.ApplyResources(this.lblMETER_TYPE, "lblMETER_TYPE");
            this.lblMETER_TYPE.Name = "lblMETER_TYPE";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cboMeterType
            // 
            this.cboMeterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMeterType.FormattingEnabled = true;
            resources.ApplyResources(this.cboMeterType, "cboMeterType");
            this.cboMeterType.Name = "cboMeterType";
            // 
            // lblSTATUS
            // 
            resources.ApplyResources(this.lblSTATUS, "lblSTATUS");
            this.lblSTATUS.Name = "lblSTATUS";
            // 
            // cboStatus
            // 
            this.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStatus.FormattingEnabled = true;
            resources.ApplyResources(this.cboStatus, "cboStatus");
            this.cboStatus.Name = "cboStatus";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Name = "label4";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // btnAddMeterType
            // 
            this.btnAddMeterType.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddMeterType, "btnAddMeterType");
            this.btnAddMeterType.Name = "btnAddMeterType";
            this.btnAddMeterType.AddItem += new System.EventHandler(this.btnAddMeterType_AddItem);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Name = "label5";
            // 
            // txtMULTIPLIER
            // 
            resources.ApplyResources(this.txtMULTIPLIER, "txtMULTIPLIER");
            this.txtMULTIPLIER.Name = "txtMULTIPLIER";
            this.txtMULTIPLIER.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblMULTIPLIER
            // 
            resources.ApplyResources(this.lblMULTIPLIER, "lblMULTIPLIER");
            this.lblMULTIPLIER.Name = "lblMULTIPLIER";
            // 
            // lblREASON
            // 
            resources.ApplyResources(this.lblREASON, "lblREASON");
            this.lblREASON.Name = "lblREASON";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Name = "label6";
            // 
            // cboReason
            // 
            this.cboReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReason.FormattingEnabled = true;
            resources.ApplyResources(this.cboReason, "cboReason");
            this.cboReason.Name = "cboReason";
            // 
            // DialogMeter
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogMeter";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblMETER_TYPE;
        private TextBox txtMeterCode;
        private Label lblMETER_CODE;
        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ComboBox cboMeterType;
        private Label lblSTATUS;
        private ComboBox cboStatus;
        private Label label4;
        private Label label3;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private ExAddItem btnAddMeterType;
        private Label label5;
        private TextBox txtMULTIPLIER;
        private Label lblMULTIPLIER;
        private Label lblREASON;
        private Label label6;
        private ComboBox cboReason;
    }
}