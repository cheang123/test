﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogMeterUnregister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogMeterUnregister));
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnPRINT = new SoftTech.Component.ExButton();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_DIGITAL = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.METER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblTOTAL = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblCOUNT_ = new System.Windows.Forms.Label();
            this.btnREGISTER = new SoftTech.Component.ExButton();
            this.btnDOWNLOAD_METER = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnDOWNLOAD_METER);
            this.content.Controls.Add(this.btnREGISTER);
            this.content.Controls.Add(this.lblTOTAL);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.lblCOUNT_);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnPRINT);
            this.content.Controls.Add(this.dgv);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.btnPRINT, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblCOUNT_, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.lblTOTAL, 0);
            this.content.Controls.SetChildIndex(this.btnREGISTER, 0);
            this.content.Controls.SetChildIndex(this.btnDOWNLOAD_METER, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPRINT
            // 
            resources.ApplyResources(this.btnPRINT, "btnPRINT");
            this.btnPRINT.Name = "btnPRINT";
            this.btnPRINT.UseVisualStyleBackColor = true;
            this.btnPRINT.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_CODE,
            this.METER_ID,
            this.CUSTOMER_NAME,
            this.METER_CODE,
            this.AREA,
            this.BOX,
            this.IS_DIGITAL,
            this.METER});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            // 
            // METER_ID
            // 
            this.METER_ID.DataPropertyName = "METER_ID";
            resources.ApplyResources(this.METER_ID, "METER_ID");
            this.METER_ID.Name = "METER_ID";
            this.METER_ID.ReadOnly = true;
            this.METER_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "CUS_NAME";
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            this.CUSTOMER_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // METER_CODE
            // 
            this.METER_CODE.DataPropertyName = "METER_CODE";
            resources.ApplyResources(this.METER_CODE, "METER_CODE");
            this.METER_CODE.Name = "METER_CODE";
            this.METER_CODE.ReadOnly = true;
            this.METER_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AREA
            // 
            this.AREA.DataPropertyName = "AREA_CODE";
            resources.ApplyResources(this.AREA, "AREA");
            this.AREA.Name = "AREA";
            this.AREA.ReadOnly = true;
            this.AREA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // BOX
            // 
            this.BOX.DataPropertyName = "BOX_CODE";
            resources.ApplyResources(this.BOX, "BOX");
            this.BOX.Name = "BOX";
            this.BOX.ReadOnly = true;
            this.BOX.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // IS_DIGITAL
            // 
            this.IS_DIGITAL.DataPropertyName = "IS_DIGITAL";
            resources.ApplyResources(this.IS_DIGITAL, "IS_DIGITAL");
            this.IS_DIGITAL.Name = "IS_DIGITAL";
            // 
            // METER
            // 
            this.METER.DataPropertyName = "METER";
            resources.ApplyResources(this.METER, "METER");
            this.METER.Name = "METER";
            // 
            // lblTOTAL
            // 
            resources.ApplyResources(this.lblTOTAL, "lblTOTAL");
            this.lblTOTAL.Name = "lblTOTAL";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // lblCOUNT_
            // 
            resources.ApplyResources(this.lblCOUNT_, "lblCOUNT_");
            this.lblCOUNT_.Name = "lblCOUNT_";
            // 
            // btnREGISTER
            // 
            resources.ApplyResources(this.btnREGISTER, "btnREGISTER");
            this.btnREGISTER.Name = "btnREGISTER";
            this.btnREGISTER.UseVisualStyleBackColor = true;
            this.btnREGISTER.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // btnDOWNLOAD_METER
            // 
            resources.ApplyResources(this.btnDOWNLOAD_METER, "btnDOWNLOAD_METER");
            this.btnDOWNLOAD_METER.Name = "btnDOWNLOAD_METER";
            this.btnDOWNLOAD_METER.UseVisualStyleBackColor = true;
            this.btnDOWNLOAD_METER.Click += new System.EventHandler(this.btnNewMeter_Click);
            // 
            // DialogMeterUnregister
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DialogMeterUnregister";
            this.Load += new System.EventHandler(this.DialogMeterUnregister_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnPRINT;
        private DataGridView dgv;
        private Label lblTOTAL;
        private Label label3;
        private Label lblCOUNT_;
        private ExButton btnREGISTER;
        private ExButton btnDOWNLOAD_METER;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn METER_ID;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn METER_CODE;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn BOX;
        private DataGridViewCheckBoxColumn IS_DIGITAL;
        private DataGridViewTextBoxColumn METER;
    }
}
