﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerMergeAccounts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerMergeAccounts));
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.txtCustomerCode = new SoftTech.Component.ExTextbox();
            this.lblCUSTOMER_CODE = new System.Windows.Forms.Label();
            this.dgvArea = new System.Windows.Forms.DataGridView();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblMAIN_CUSTOMER = new System.Windows.Forms.Label();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.lblAREA = new System.Windows.Forms.Label();
            this.txtArea = new System.Windows.Forms.TextBox();
            this.txtMeter = new System.Windows.Forms.TextBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.lblCUSTOMER = new System.Windows.Forms.Label();
            this.btnREMOVE = new SoftTech.Component.ExLinkLabel(this.components);
            this.btnADD = new SoftTech.Component.ExLinkLabel(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.CUSTOMER_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CUSTOMER_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.METER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_REACTIVE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.REACTIVE_RULE = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArea)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.lblCUSTOMER);
            this.content.Controls.Add(this.txtCustomerName);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.txtMeter);
            this.content.Controls.Add(this.txtArea);
            this.content.Controls.Add(this.lblAREA);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.lblMAIN_CUSTOMER);
            this.content.Controls.Add(this.txtCustomerCode);
            this.content.Controls.Add(this.lblCUSTOMER_CODE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.dgvArea);
            this.content.Controls.Add(this.btnCLOSE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.dgvArea, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerCode, 0);
            this.content.Controls.SetChildIndex(this.lblMAIN_CUSTOMER, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblAREA, 0);
            this.content.Controls.SetChildIndex(this.txtArea, 0);
            this.content.Controls.SetChildIndex(this.txtMeter, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerName, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtCustomerCode
            // 
            this.txtCustomerCode.BackColor = System.Drawing.Color.White;
            this.txtCustomerCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtCustomerCode, "txtCustomerCode");
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtCustomerCode.QuickSearch += new System.EventHandler(this.txtMasterCustomer_QuickSearch);
            this.txtCustomerCode.AdvanceSearch += new System.EventHandler(this.txtMasterCustomer_AdvanceSearch);
            this.txtCustomerCode.CancelAdvanceSearch += new System.EventHandler(this.txtMasterCustomer_CancelAdvanceSearch);
            // 
            // lblCUSTOMER_CODE
            // 
            resources.ApplyResources(this.lblCUSTOMER_CODE, "lblCUSTOMER_CODE");
            this.lblCUSTOMER_CODE.Name = "lblCUSTOMER_CODE";
            // 
            // dgvArea
            // 
            this.dgvArea.AllowUserToAddRows = false;
            this.dgvArea.AllowUserToDeleteRows = false;
            this.dgvArea.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvArea.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvArea.BackgroundColor = System.Drawing.Color.White;
            this.dgvArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvArea.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvArea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvArea.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CUSTOMER_ID,
            this.STATUS_ID,
            this.CUSTOMER_CODE,
            this.CUSTOMER_NAME,
            this.AREA,
            this.METER_CODE,
            this.IS_REACTIVE,
            this.REACTIVE_RULE});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvArea.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvArea.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvArea.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvArea, "dgvArea");
            this.dgvArea.MultiSelect = false;
            this.dgvArea.Name = "dgvArea";
            this.dgvArea.RowHeadersVisible = false;
            this.dgvArea.RowTemplate.Height = 25;
            this.dgvArea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblMAIN_CUSTOMER
            // 
            resources.ApplyResources(this.lblMAIN_CUSTOMER, "lblMAIN_CUSTOMER");
            this.lblMAIN_CUSTOMER.Name = "lblMAIN_CUSTOMER";
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // lblAREA
            // 
            resources.ApplyResources(this.lblAREA, "lblAREA");
            this.lblAREA.Name = "lblAREA";
            // 
            // txtArea
            // 
            resources.ApplyResources(this.txtArea, "txtArea");
            this.txtArea.Name = "txtArea";
            // 
            // txtMeter
            // 
            resources.ApplyResources(this.txtMeter, "txtMeter");
            this.txtMeter.Name = "txtMeter";
            // 
            // txtCustomerName
            // 
            resources.ApplyResources(this.txtCustomerName, "txtCustomerName");
            this.txtCustomerName.Name = "txtCustomerName";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // lblCUSTOMER
            // 
            resources.ApplyResources(this.lblCUSTOMER, "lblCUSTOMER");
            this.lblCUSTOMER.Name = "lblCUSTOMER";
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.TabStop = true;
            this.btnREMOVE.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnRemove_LinkClicked);
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnADD.Name = "btnADD";
            this.btnADD.TabStop = true;
            this.btnADD.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnAdd_LinkClicked);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnREMOVE);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnCHANGE_LOG_Click);
            // 
            // CUSTOMER_ID
            // 
            this.CUSTOMER_ID.DataPropertyName = "CUSTOMER_ID";
            resources.ApplyResources(this.CUSTOMER_ID, "CUSTOMER_ID");
            this.CUSTOMER_ID.Name = "CUSTOMER_ID";
            this.CUSTOMER_ID.ReadOnly = true;
            // 
            // STATUS_ID
            // 
            this.STATUS_ID.DataPropertyName = "STATUS_ID";
            resources.ApplyResources(this.STATUS_ID, "STATUS_ID");
            this.STATUS_ID.Name = "STATUS_ID";
            // 
            // CUSTOMER_CODE
            // 
            this.CUSTOMER_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.CUSTOMER_CODE.DataPropertyName = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.FillWeight = 126.8655F;
            resources.ApplyResources(this.CUSTOMER_CODE, "CUSTOMER_CODE");
            this.CUSTOMER_CODE.Name = "CUSTOMER_CODE";
            this.CUSTOMER_CODE.ReadOnly = true;
            // 
            // CUSTOMER_NAME
            // 
            this.CUSTOMER_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CUSTOMER_NAME.DataPropertyName = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.FillWeight = 6.854217F;
            resources.ApplyResources(this.CUSTOMER_NAME, "CUSTOMER_NAME");
            this.CUSTOMER_NAME.Name = "CUSTOMER_NAME";
            this.CUSTOMER_NAME.ReadOnly = true;
            // 
            // AREA
            // 
            this.AREA.DataPropertyName = "AREA_NAME";
            this.AREA.FillWeight = 5.832329F;
            resources.ApplyResources(this.AREA, "AREA");
            this.AREA.Name = "AREA";
            this.AREA.ReadOnly = true;
            // 
            // METER_CODE
            // 
            this.METER_CODE.DataPropertyName = "METER_CODE";
            this.METER_CODE.FillWeight = 110.3729F;
            resources.ApplyResources(this.METER_CODE, "METER_CODE");
            this.METER_CODE.Name = "METER_CODE";
            this.METER_CODE.ReadOnly = true;
            // 
            // IS_REACTIVE
            // 
            this.IS_REACTIVE.DataPropertyName = "IS_REACTIVE";
            resources.ApplyResources(this.IS_REACTIVE, "IS_REACTIVE");
            this.IS_REACTIVE.Name = "IS_REACTIVE";
            // 
            // REACTIVE_RULE
            // 
            resources.ApplyResources(this.REACTIVE_RULE, "REACTIVE_RULE");
            this.REACTIVE_RULE.Name = "REACTIVE_RULE";
            this.REACTIVE_RULE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // DialogCustomerMergeAccounts
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerMergeAccounts";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArea)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private DataGridView dgvArea;
        private Label lblCUSTOMER_CODE;
        private ExTextbox txtCustomerCode;
        private ExButton btnOK;
        private Label lblMETER_CODE;
        private Label lblMAIN_CUSTOMER;
        private Label lblAREA;
        private TextBox txtCustomerName;
        private Label lblCUSTOMER_NAME;
        private TextBox txtMeter;
        private TextBox txtArea;
        private Label lblCUSTOMER;
        private ExLinkLabel btnREMOVE;
        private ExLinkLabel btnADD;
        private Panel panel1;
        private ExButton btnCHANGE_LOG;
        private DataGridViewTextBoxColumn CUSTOMER_ID;
        private DataGridViewTextBoxColumn STATUS_ID;
        private DataGridViewTextBoxColumn CUSTOMER_CODE;
        private DataGridViewTextBoxColumn CUSTOMER_NAME;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn METER_CODE;
        private DataGridViewCheckBoxColumn IS_REACTIVE;
        private DataGridViewComboBoxColumn REACTIVE_RULE;
    }
}