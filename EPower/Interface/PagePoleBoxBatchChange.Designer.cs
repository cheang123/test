﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PagePoleBoxBatchChange
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PagePoleBoxBatchChange));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvPole = new System.Windows.Forms.DataGridView();
            this.POLE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.POLE_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AREA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TRANSFORMER_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvBox = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnBOX_BATCH_CHANGE = new SoftTech.Component.ExButton();
            this.cboTransfo = new System.Windows.Forms.ComboBox();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.btnPOLE_BATCH_CHANGE = new SoftTech.Component.ExButton();
            this.txtSearchBox = new SoftTech.Component.ExTextbox();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STATUS_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBox)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvPole);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvBox);
            // 
            // dgvPole
            // 
            this.dgvPole.AllowUserToAddRows = false;
            this.dgvPole.AllowUserToDeleteRows = false;
            this.dgvPole.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPole.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPole.BackgroundColor = System.Drawing.Color.White;
            this.dgvPole.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPole.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvPole.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPole.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.POLE_ID,
            this.POLE_CODE,
            this.AREA,
            this.TRANSFORMER_CODE});
            resources.ApplyResources(this.dgvPole, "dgvPole");
            this.dgvPole.EnableHeadersVisualStyles = false;
            this.dgvPole.Name = "dgvPole";
            this.dgvPole.RowHeadersVisible = false;
            this.dgvPole.RowTemplate.Height = 25;
            this.dgvPole.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPole.SelectionChanged += new System.EventHandler(this.dgvPole_SelectionChanged);
            // 
            // POLE_ID
            // 
            this.POLE_ID.DataPropertyName = "POLE_ID";
            resources.ApplyResources(this.POLE_ID, "POLE_ID");
            this.POLE_ID.Name = "POLE_ID";
            // 
            // POLE_CODE
            // 
            this.POLE_CODE.DataPropertyName = "POLE_CODE";
            resources.ApplyResources(this.POLE_CODE, "POLE_CODE");
            this.POLE_CODE.Name = "POLE_CODE";
            // 
            // AREA
            // 
            this.AREA.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AREA.DataPropertyName = "AREA_NAME";
            resources.ApplyResources(this.AREA, "AREA");
            this.AREA.Name = "AREA";
            // 
            // TRANSFORMER_CODE
            // 
            this.TRANSFORMER_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TRANSFORMER_CODE.DataPropertyName = "TRANSFORMER_CODE";
            resources.ApplyResources(this.TRANSFORMER_CODE, "TRANSFORMER_CODE");
            this.TRANSFORMER_CODE.Name = "TRANSFORMER_CODE";
            // 
            // dgvBox
            // 
            this.dgvBox.AllowUserToAddRows = false;
            this.dgvBox.AllowUserToDeleteRows = false;
            this.dgvBox.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvBox.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvBox.BackgroundColor = System.Drawing.Color.White;
            this.dgvBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvBox.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvBox.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBox.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BOX_ID,
            this.STATUS_ID,
            this.BOX_CODE});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBox.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.dgvBox, "dgvBox");
            this.dgvBox.EnableHeadersVisualStyles = false;
            this.dgvBox.Name = "dgvBox";
            this.dgvBox.ReadOnly = true;
            this.dgvBox.RowHeadersVisible = false;
            this.dgvBox.RowTemplate.Height = 25;
            this.dgvBox.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.btnBOX_BATCH_CHANGE);
            this.panel1.Controls.Add(this.cboTransfo);
            this.panel1.Controls.Add(this.cboArea);
            this.panel1.Controls.Add(this.btnPOLE_BATCH_CHANGE);
            this.panel1.Controls.Add(this.txtSearchBox);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnBOX_BATCH_CHANGE
            // 
            resources.ApplyResources(this.btnBOX_BATCH_CHANGE, "btnBOX_BATCH_CHANGE");
            this.btnBOX_BATCH_CHANGE.Name = "btnBOX_BATCH_CHANGE";
            this.btnBOX_BATCH_CHANGE.UseVisualStyleBackColor = true;
            this.btnBOX_BATCH_CHANGE.Click += new System.EventHandler(this.btnBOX_BATCH_CHANGE_Click);
            // 
            // cboTransfo
            // 
            this.cboTransfo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransfo.FormattingEnabled = true;
            this.cboTransfo.Items.AddRange(new object[] {
            resources.GetString("cboTransfo.Items"),
            resources.GetString("cboTransfo.Items1"),
            resources.GetString("cboTransfo.Items2"),
            resources.GetString("cboTransfo.Items3"),
            resources.GetString("cboTransfo.Items4")});
            resources.ApplyResources(this.cboTransfo, "cboTransfo");
            this.cboTransfo.Name = "cboTransfo";
            this.cboTransfo.SelectedIndexChanged += new System.EventHandler(this.cboArea_SelectedIndexChanged);
            this.cboTransfo.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // cboArea
            // 
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.FormattingEnabled = true;
            this.cboArea.Items.AddRange(new object[] {
            resources.GetString("cboArea.Items"),
            resources.GetString("cboArea.Items1"),
            resources.GetString("cboArea.Items2"),
            resources.GetString("cboArea.Items3"),
            resources.GetString("cboArea.Items4")});
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            this.cboArea.SelectedIndexChanged += new System.EventHandler(this.cboArea_SelectedIndexChanged);
            this.cboArea.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // btnPOLE_BATCH_CHANGE
            // 
            resources.ApplyResources(this.btnPOLE_BATCH_CHANGE, "btnPOLE_BATCH_CHANGE");
            this.btnPOLE_BATCH_CHANGE.Name = "btnPOLE_BATCH_CHANGE";
            this.btnPOLE_BATCH_CHANGE.UseVisualStyleBackColor = true;
            this.btnPOLE_BATCH_CHANGE.Click += new System.EventHandler(this.btnPOLE_BATCH_CHANGE_Click);
            // 
            // txtSearchBox
            // 
            resources.ApplyResources(this.txtSearchBox, "txtSearchBox");
            this.txtSearchBox.BackColor = System.Drawing.Color.White;
            this.txtSearchBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearchBox.Name = "txtSearchBox";
            this.txtSearchBox.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtSearchBox.QuickSearch += new System.EventHandler(this.txtSearchBox_QuickSearch);
            this.txtSearchBox.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            this.txtQuickSearch.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "POLE_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "POLE_CODE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "AREA_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "TRANSFORMER_CODE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "BOX_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "STATUS_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "BOX_CODE";
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // BOX_ID
            // 
            this.BOX_ID.DataPropertyName = "BOX_ID";
            resources.ApplyResources(this.BOX_ID, "BOX_ID");
            this.BOX_ID.Name = "BOX_ID";
            this.BOX_ID.ReadOnly = true;
            // 
            // STATUS_ID
            // 
            this.STATUS_ID.DataPropertyName = "STATUS_ID";
            resources.ApplyResources(this.STATUS_ID, "STATUS_ID");
            this.STATUS_ID.Name = "STATUS_ID";
            this.STATUS_ID.ReadOnly = true;
            // 
            // BOX_CODE
            // 
            this.BOX_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BOX_CODE.DataPropertyName = "BOX_CODE";
            resources.ApplyResources(this.BOX_CODE, "BOX_CODE");
            this.BOX_CODE.Name = "BOX_CODE";
            this.BOX_CODE.ReadOnly = true;
            // 
            // PagePoleBoxBatchChange
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Name = "PagePoleBoxBatchChange";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnPOLE_BATCH_CHANGE;
        public ComboBox cboArea;
        private SplitContainer splitContainer1;
        private DataGridView dgvPole;
        private DataGridView dgvBox;
        private ExButton btnBOX_BATCH_CHANGE;
        private DataGridViewTextBoxColumn POLE_ID;
        private DataGridViewTextBoxColumn POLE_CODE;
        private DataGridViewTextBoxColumn AREA;
        private DataGridViewTextBoxColumn TRANSFORMER_CODE;
        public ComboBox cboTransfo;
        private ExTextbox txtSearchBox;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private DataGridViewTextBoxColumn BOX_ID;
        private DataGridViewTextBoxColumn STATUS_ID;
        private DataGridViewTextBoxColumn BOX_CODE;
    }
}
