﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCustomerBatchChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerBatchChange));
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.cboArea = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cboPole = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cboPrice = new System.Windows.Forms.ComboBox();
            this.cboCycle = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtVillage = new System.Windows.Forms.TextBox();
            this.btnChooseVillage = new SoftTech.Component.ExButton();
            this.label8 = new System.Windows.Forms.Label();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblCustomer);
            this.content.Controls.Add(this.label8);
            this.content.Controls.Add(this.btnChooseVillage);
            this.content.Controls.Add(this.txtVillage);
            this.content.Controls.Add(this.label11);
            this.content.Controls.Add(this.cboCycle);
            this.content.Controls.Add(this.cboPrice);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.label6);
            this.content.Controls.Add(this.label5);
            this.content.Controls.Add(this.label4);
            this.content.Controls.Add(this.cboBox);
            this.content.Controls.Add(this.label3);
            this.content.Controls.Add(this.cboPole);
            this.content.Controls.Add(this.label2);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.cboArea);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.btnCLOSE);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.cboArea, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.label2, 0);
            this.content.Controls.SetChildIndex(this.cboPole, 0);
            this.content.Controls.SetChildIndex(this.label3, 0);
            this.content.Controls.SetChildIndex(this.cboBox, 0);
            this.content.Controls.SetChildIndex(this.label4, 0);
            this.content.Controls.SetChildIndex(this.label5, 0);
            this.content.Controls.SetChildIndex(this.label6, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.cboPrice, 0);
            this.content.Controls.SetChildIndex(this.cboCycle, 0);
            this.content.Controls.SetChildIndex(this.label11, 0);
            this.content.Controls.SetChildIndex(this.txtVillage, 0);
            this.content.Controls.SetChildIndex(this.btnChooseVillage, 0);
            this.content.Controls.SetChildIndex(this.label8, 0);
            this.content.Controls.SetChildIndex(this.lblCustomer, 0);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // cboArea
            // 
            this.cboArea.DropDownHeight = 300;
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.DropDownWidth = 100;
            this.cboArea.FormattingEnabled = true;
            resources.ApplyResources(this.cboArea, "cboArea");
            this.cboArea.Name = "cboArea";
            this.cboArea.SelectedIndexChanged += new System.EventHandler(this.cboArea_SelectedIndexChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // cboPole
            // 
            this.cboPole.DropDownHeight = 300;
            this.cboPole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPole.DropDownWidth = 100;
            this.cboPole.FormattingEnabled = true;
            resources.ApplyResources(this.cboPole, "cboPole");
            this.cboPole.Name = "cboPole";
            this.cboPole.SelectedIndexChanged += new System.EventHandler(this.cboPole_SelectedIndexChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // cboBox
            // 
            this.cboBox.DropDownHeight = 300;
            this.cboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBox.DropDownWidth = 100;
            this.cboBox.FormattingEnabled = true;
            resources.ApplyResources(this.cboBox, "cboBox");
            this.cboBox.Name = "cboBox";
            this.cboBox.SelectedIndexChanged += new System.EventHandler(this.cboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // cboPrice
            // 
            this.cboPrice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrice.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrice, "cboPrice");
            this.cboPrice.Name = "cboPrice";
            // 
            // cboCycle
            // 
            this.cboCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboCycle, "cboCycle");
            this.cboCycle.Name = "cboCycle";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // txtVillage
            // 
            this.txtVillage.BackColor = System.Drawing.Color.LightYellow;
            resources.ApplyResources(this.txtVillage, "txtVillage");
            this.txtVillage.Name = "txtVillage";
            this.txtVillage.ReadOnly = true;
            // 
            // btnChooseVillage
            // 
            resources.ApplyResources(this.btnChooseVillage, "btnChooseVillage");
            this.btnChooseVillage.Name = "btnChooseVillage";
            this.btnChooseVillage.UseVisualStyleBackColor = true;
            this.btnChooseVillage.Click += new System.EventHandler(this.btnChooseVillage_Click);
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // lblCustomer
            // 
            resources.ApplyResources(this.lblCustomer, "lblCustomer");
            this.lblCustomer.Name = "lblCustomer";
            // 
            // DialogCustomerBatchChange
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerBatchChange";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private ComboBox cboBox;
        private Label label3;
        private ComboBox cboPole;
        private Label label2;
        private Label label1;
        private ComboBox cboArea;
        private Label label7;
        private Label label6;
        private Label label5;
        private Label label4;
        private ComboBox cboCycle;
        private ComboBox cboPrice;
        private Label label11;
        private TextBox txtVillage;
        private ExButton btnChooseVillage;
        private Label lblCustomer;
        private Label label8;
    }
}