﻿namespace EPower.Interface
{
    partial class DialogCustomerActivate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCustomerActivate));
            this.lblACTIVATE_DATE = new System.Windows.Forms.Label();
            this.nudCutoffDays = new System.Windows.Forms.NumericUpDown();
            this.lblCUT_DAY = new System.Windows.Forms.Label();
            this.lblCUSTOMER_INFORMATION = new System.Windows.Forms.Label();
            this.txtMeterAmp = new System.Windows.Forms.TextBox();
            this.txtMeterVol = new System.Windows.Forms.TextBox();
            this.txtMeterPhase = new System.Windows.Forms.TextBox();
            this.lblCUSTOMER_TYPE = new System.Windows.Forms.Label();
            this.lblMETER_CODE = new System.Windows.Forms.Label();
            this.lblPOLE = new System.Windows.Forms.Label();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.lblMETER_INSTALLATION = new System.Windows.Forms.Label();
            this.lblMETER_TYPE = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.lblBREAKER_TYPE = new System.Windows.Forms.Label();
            this.lblBREAKER_CODE = new System.Windows.Forms.Label();
            this.txtBreakerPhase = new System.Windows.Forms.TextBox();
            this.txtBreakerVol = new System.Windows.Forms.TextBox();
            this.txtBreakerAmp = new System.Windows.Forms.TextBox();
            this.lblSHIELD = new System.Windows.Forms.Label();
            this.lblCABLE_SHIELD = new System.Windows.Forms.Label();
            this.lblAMPARE_1 = new System.Windows.Forms.Label();
            this.lblVOLTAGE_1 = new System.Windows.Forms.Label();
            this.lblPHASE_1 = new System.Windows.Forms.Label();
            this.lblAMPARE = new System.Windows.Forms.Label();
            this.lblVOLTAGE = new System.Windows.Forms.Label();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.lblBREAKER_INSTALLATION = new System.Windows.Forms.Label();
            this.txtMeter = new SoftTech.Component.ExTextbox();
            this.txtBreaker = new SoftTech.Component.ExTextbox();
            this.cboMeterShield = new System.Windows.Forms.ComboBox();
            this.cboCableShield = new System.Windows.Forms.ComboBox();
            this.lblCUSTOMER_NAME = new System.Windows.Forms.Label();
            this.txtLastNamekh = new System.Windows.Forms.TextBox();
            this.lblBOX = new System.Windows.Forms.Label();
            this.txtBox = new SoftTech.Component.ExTextbox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblCUTTER = new System.Windows.Forms.Label();
            this.lblBILLER = new System.Windows.Forms.Label();
            this.lblCOLLECTOR = new System.Windows.Forms.Label();
            this.txtCollector = new System.Windows.Forms.TextBox();
            this.txtBiller = new System.Windows.Forms.TextBox();
            this.txtCutter = new System.Windows.Forms.TextBox();
            this.lblPOLE_AND_BOX_INSTALLATION = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtMeterType = new System.Windows.Forms.TextBox();
            this.txtBreakerType = new System.Windows.Forms.TextBox();
            this.txtPoleCode = new System.Windows.Forms.TextBox();
            this.txtCustomerType = new System.Windows.Forms.TextBox();
            this.dtpActivateDate = new System.Windows.Forms.DateTimePicker();
            this.lblCONSTANT = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblCONSTANT_1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lblSTART_USAGE = new System.Windows.Forms.Label();
            this.txtSTART_USAGE = new System.Windows.Forms.TextBox();
            this.lblPOSITION_IN_BOX = new System.Windows.Forms.Label();
            this.txtPositionInBox = new System.Windows.Forms.TextBox();
            this.lblVIEW_BOX_CUSTOMER = new SoftTech.Component.ExLinkLabel(this.components);
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCutoffDays)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.lblVIEW_BOX_CUSTOMER);
            this.content.Controls.Add(this.txtSTART_USAGE);
            this.content.Controls.Add(this.lblSTART_USAGE);
            this.content.Controls.Add(this.lblCONSTANT_1);
            this.content.Controls.Add(this.textBox2);
            this.content.Controls.Add(this.lblCONSTANT);
            this.content.Controls.Add(this.textBox1);
            this.content.Controls.Add(this.dtpActivateDate);
            this.content.Controls.Add(this.txtCustomerType);
            this.content.Controls.Add(this.txtPoleCode);
            this.content.Controls.Add(this.txtBreakerType);
            this.content.Controls.Add(this.txtMeterType);
            this.content.Controls.Add(this.label30);
            this.content.Controls.Add(this.label28);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.label14);
            this.content.Controls.Add(this.lblPOLE_AND_BOX_INSTALLATION);
            this.content.Controls.Add(this.txtPositionInBox);
            this.content.Controls.Add(this.txtCutter);
            this.content.Controls.Add(this.txtBiller);
            this.content.Controls.Add(this.txtCollector);
            this.content.Controls.Add(this.label13);
            this.content.Controls.Add(this.lblPOSITION_IN_BOX);
            this.content.Controls.Add(this.lblCUTTER);
            this.content.Controls.Add(this.lblBILLER);
            this.content.Controls.Add(this.lblCOLLECTOR);
            this.content.Controls.Add(this.txtBox);
            this.content.Controls.Add(this.lblBOX);
            this.content.Controls.Add(this.txtLastNamekh);
            this.content.Controls.Add(this.lblCUSTOMER_NAME);
            this.content.Controls.Add(this.lblBREAKER_INSTALLATION);
            this.content.Controls.Add(this.lblAMPARE);
            this.content.Controls.Add(this.lblVOLTAGE);
            this.content.Controls.Add(this.lblPHASE);
            this.content.Controls.Add(this.lblAMPARE_1);
            this.content.Controls.Add(this.lblVOLTAGE_1);
            this.content.Controls.Add(this.lblPHASE_1);
            this.content.Controls.Add(this.lblCABLE_SHIELD);
            this.content.Controls.Add(this.lblSHIELD);
            this.content.Controls.Add(this.txtBreakerAmp);
            this.content.Controls.Add(this.txtBreakerVol);
            this.content.Controls.Add(this.txtBreakerPhase);
            this.content.Controls.Add(this.lblBREAKER_CODE);
            this.content.Controls.Add(this.txtBreaker);
            this.content.Controls.Add(this.lblBREAKER_TYPE);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.lblACTIVATE_DATE);
            this.content.Controls.Add(this.nudCutoffDays);
            this.content.Controls.Add(this.lblCUT_DAY);
            this.content.Controls.Add(this.lblCUSTOMER_INFORMATION);
            this.content.Controls.Add(this.txtMeterAmp);
            this.content.Controls.Add(this.txtMeterVol);
            this.content.Controls.Add(this.txtMeterPhase);
            this.content.Controls.Add(this.lblCUSTOMER_TYPE);
            this.content.Controls.Add(this.lblMETER_CODE);
            this.content.Controls.Add(this.lblPOLE);
            this.content.Controls.Add(this.cboBillingCycle);
            this.content.Controls.Add(this.lblCYCLE_NAME);
            this.content.Controls.Add(this.lblMETER_INSTALLATION);
            this.content.Controls.Add(this.lblMETER_TYPE);
            this.content.Controls.Add(this.cboCableShield);
            this.content.Controls.Add(this.cboMeterShield);
            this.content.Controls.Add(this.txtMeter);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.txtMeter, 0);
            this.content.Controls.SetChildIndex(this.cboMeterShield, 0);
            this.content.Controls.SetChildIndex(this.cboCableShield, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_INSTALLATION, 0);
            this.content.Controls.SetChildIndex(this.lblCYCLE_NAME, 0);
            this.content.Controls.SetChildIndex(this.cboBillingCycle, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE, 0);
            this.content.Controls.SetChildIndex(this.lblMETER_CODE, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtMeterPhase, 0);
            this.content.Controls.SetChildIndex(this.txtMeterVol, 0);
            this.content.Controls.SetChildIndex(this.txtMeterAmp, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_INFORMATION, 0);
            this.content.Controls.SetChildIndex(this.lblCUT_DAY, 0);
            this.content.Controls.SetChildIndex(this.nudCutoffDays, 0);
            this.content.Controls.SetChildIndex(this.lblACTIVATE_DATE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtBreaker, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtBreakerPhase, 0);
            this.content.Controls.SetChildIndex(this.txtBreakerVol, 0);
            this.content.Controls.SetChildIndex(this.txtBreakerAmp, 0);
            this.content.Controls.SetChildIndex(this.lblSHIELD, 0);
            this.content.Controls.SetChildIndex(this.lblCABLE_SHIELD, 0);
            this.content.Controls.SetChildIndex(this.lblPHASE_1, 0);
            this.content.Controls.SetChildIndex(this.lblVOLTAGE_1, 0);
            this.content.Controls.SetChildIndex(this.lblAMPARE_1, 0);
            this.content.Controls.SetChildIndex(this.lblPHASE, 0);
            this.content.Controls.SetChildIndex(this.lblVOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.lblAMPARE, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_INSTALLATION, 0);
            this.content.Controls.SetChildIndex(this.lblCUSTOMER_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtLastNamekh, 0);
            this.content.Controls.SetChildIndex(this.lblBOX, 0);
            this.content.Controls.SetChildIndex(this.txtBox, 0);
            this.content.Controls.SetChildIndex(this.lblCOLLECTOR, 0);
            this.content.Controls.SetChildIndex(this.lblBILLER, 0);
            this.content.Controls.SetChildIndex(this.lblCUTTER, 0);
            this.content.Controls.SetChildIndex(this.lblPOSITION_IN_BOX, 0);
            this.content.Controls.SetChildIndex(this.label13, 0);
            this.content.Controls.SetChildIndex(this.txtCollector, 0);
            this.content.Controls.SetChildIndex(this.txtBiller, 0);
            this.content.Controls.SetChildIndex(this.txtCutter, 0);
            this.content.Controls.SetChildIndex(this.txtPositionInBox, 0);
            this.content.Controls.SetChildIndex(this.lblPOLE_AND_BOX_INSTALLATION, 0);
            this.content.Controls.SetChildIndex(this.label14, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.label28, 0);
            this.content.Controls.SetChildIndex(this.label30, 0);
            this.content.Controls.SetChildIndex(this.txtMeterType, 0);
            this.content.Controls.SetChildIndex(this.txtBreakerType, 0);
            this.content.Controls.SetChildIndex(this.txtPoleCode, 0);
            this.content.Controls.SetChildIndex(this.txtCustomerType, 0);
            this.content.Controls.SetChildIndex(this.dtpActivateDate, 0);
            this.content.Controls.SetChildIndex(this.textBox1, 0);
            this.content.Controls.SetChildIndex(this.lblCONSTANT, 0);
            this.content.Controls.SetChildIndex(this.textBox2, 0);
            this.content.Controls.SetChildIndex(this.lblCONSTANT_1, 0);
            this.content.Controls.SetChildIndex(this.lblSTART_USAGE, 0);
            this.content.Controls.SetChildIndex(this.txtSTART_USAGE, 0);
            this.content.Controls.SetChildIndex(this.lblVIEW_BOX_CUSTOMER, 0);
            // 
            // lblACTIVATE_DATE
            // 
            resources.ApplyResources(this.lblACTIVATE_DATE, "lblACTIVATE_DATE");
            this.lblACTIVATE_DATE.Name = "lblACTIVATE_DATE";
            // 
            // nudCutoffDays
            // 
            resources.ApplyResources(this.nudCutoffDays, "nudCutoffDays");
            this.nudCutoffDays.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudCutoffDays.Name = "nudCutoffDays";
            this.nudCutoffDays.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblCUT_DAY
            // 
            resources.ApplyResources(this.lblCUT_DAY, "lblCUT_DAY");
            this.lblCUT_DAY.Name = "lblCUT_DAY";
            // 
            // lblCUSTOMER_INFORMATION
            // 
            resources.ApplyResources(this.lblCUSTOMER_INFORMATION, "lblCUSTOMER_INFORMATION");
            this.lblCUSTOMER_INFORMATION.Name = "lblCUSTOMER_INFORMATION";
            // 
            // txtMeterAmp
            // 
            resources.ApplyResources(this.txtMeterAmp, "txtMeterAmp");
            this.txtMeterAmp.Name = "txtMeterAmp";
            this.txtMeterAmp.ReadOnly = true;
            this.txtMeterAmp.TabStop = false;
            // 
            // txtMeterVol
            // 
            resources.ApplyResources(this.txtMeterVol, "txtMeterVol");
            this.txtMeterVol.Name = "txtMeterVol";
            this.txtMeterVol.ReadOnly = true;
            this.txtMeterVol.TabStop = false;
            // 
            // txtMeterPhase
            // 
            resources.ApplyResources(this.txtMeterPhase, "txtMeterPhase");
            this.txtMeterPhase.Name = "txtMeterPhase";
            this.txtMeterPhase.ReadOnly = true;
            this.txtMeterPhase.TabStop = false;
            // 
            // lblCUSTOMER_TYPE
            // 
            resources.ApplyResources(this.lblCUSTOMER_TYPE, "lblCUSTOMER_TYPE");
            this.lblCUSTOMER_TYPE.Name = "lblCUSTOMER_TYPE";
            // 
            // lblMETER_CODE
            // 
            resources.ApplyResources(this.lblMETER_CODE, "lblMETER_CODE");
            this.lblMETER_CODE.Name = "lblMETER_CODE";
            // 
            // lblPOLE
            // 
            resources.ApplyResources(this.lblPOLE, "lblPOLE");
            this.lblPOLE.Name = "lblPOLE";
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            resources.ApplyResources(this.cboBillingCycle, "cboBillingCycle");
            this.cboBillingCycle.Name = "cboBillingCycle";
            // 
            // lblCYCLE_NAME
            // 
            resources.ApplyResources(this.lblCYCLE_NAME, "lblCYCLE_NAME");
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            // 
            // lblMETER_INSTALLATION
            // 
            resources.ApplyResources(this.lblMETER_INSTALLATION, "lblMETER_INSTALLATION");
            this.lblMETER_INSTALLATION.Name = "lblMETER_INSTALLATION";
            // 
            // lblMETER_TYPE
            // 
            resources.ApplyResources(this.lblMETER_TYPE, "lblMETER_TYPE");
            this.lblMETER_TYPE.Name = "lblMETER_TYPE";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblBREAKER_TYPE
            // 
            resources.ApplyResources(this.lblBREAKER_TYPE, "lblBREAKER_TYPE");
            this.lblBREAKER_TYPE.Name = "lblBREAKER_TYPE";
            // 
            // lblBREAKER_CODE
            // 
            resources.ApplyResources(this.lblBREAKER_CODE, "lblBREAKER_CODE");
            this.lblBREAKER_CODE.Name = "lblBREAKER_CODE";
            // 
            // txtBreakerPhase
            // 
            resources.ApplyResources(this.txtBreakerPhase, "txtBreakerPhase");
            this.txtBreakerPhase.Name = "txtBreakerPhase";
            this.txtBreakerPhase.ReadOnly = true;
            this.txtBreakerPhase.TabStop = false;
            // 
            // txtBreakerVol
            // 
            resources.ApplyResources(this.txtBreakerVol, "txtBreakerVol");
            this.txtBreakerVol.Name = "txtBreakerVol";
            this.txtBreakerVol.ReadOnly = true;
            this.txtBreakerVol.TabStop = false;
            // 
            // txtBreakerAmp
            // 
            resources.ApplyResources(this.txtBreakerAmp, "txtBreakerAmp");
            this.txtBreakerAmp.Name = "txtBreakerAmp";
            this.txtBreakerAmp.ReadOnly = true;
            this.txtBreakerAmp.TabStop = false;
            // 
            // lblSHIELD
            // 
            resources.ApplyResources(this.lblSHIELD, "lblSHIELD");
            this.lblSHIELD.Name = "lblSHIELD";
            // 
            // lblCABLE_SHIELD
            // 
            resources.ApplyResources(this.lblCABLE_SHIELD, "lblCABLE_SHIELD");
            this.lblCABLE_SHIELD.Name = "lblCABLE_SHIELD";
            // 
            // lblAMPARE_1
            // 
            resources.ApplyResources(this.lblAMPARE_1, "lblAMPARE_1");
            this.lblAMPARE_1.Name = "lblAMPARE_1";
            // 
            // lblVOLTAGE_1
            // 
            resources.ApplyResources(this.lblVOLTAGE_1, "lblVOLTAGE_1");
            this.lblVOLTAGE_1.Name = "lblVOLTAGE_1";
            // 
            // lblPHASE_1
            // 
            resources.ApplyResources(this.lblPHASE_1, "lblPHASE_1");
            this.lblPHASE_1.Name = "lblPHASE_1";
            // 
            // lblAMPARE
            // 
            resources.ApplyResources(this.lblAMPARE, "lblAMPARE");
            this.lblAMPARE.Name = "lblAMPARE";
            // 
            // lblVOLTAGE
            // 
            resources.ApplyResources(this.lblVOLTAGE, "lblVOLTAGE");
            this.lblVOLTAGE.Name = "lblVOLTAGE";
            // 
            // lblPHASE
            // 
            resources.ApplyResources(this.lblPHASE, "lblPHASE");
            this.lblPHASE.Name = "lblPHASE";
            // 
            // lblBREAKER_INSTALLATION
            // 
            resources.ApplyResources(this.lblBREAKER_INSTALLATION, "lblBREAKER_INSTALLATION");
            this.lblBREAKER_INSTALLATION.Name = "lblBREAKER_INSTALLATION";
            // 
            // txtMeter
            // 
            this.txtMeter.BackColor = System.Drawing.Color.White;
            this.txtMeter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtMeter, "txtMeter");
            this.txtMeter.Name = "txtMeter";
            this.txtMeter.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtMeter.AdvanceSearch += new System.EventHandler(this.txtMeter_AdvanceSearch);
            this.txtMeter.CancelAdvanceSearch += new System.EventHandler(this.txtMeter_CancelAdvanceSearch);
            this.txtMeter.Enter += new System.EventHandler(this.txtMeter_Enter);
            // 
            // txtBreaker
            // 
            this.txtBreaker.BackColor = System.Drawing.Color.White;
            this.txtBreaker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtBreaker, "txtBreaker");
            this.txtBreaker.Name = "txtBreaker";
            this.txtBreaker.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtBreaker.AdvanceSearch += new System.EventHandler(this.txtBreaker_AdvanceSearch);
            this.txtBreaker.CancelAdvanceSearch += new System.EventHandler(this.txtBreaker_CancelAdvanceSearch);
            this.txtBreaker.Enter += new System.EventHandler(this.txtMeter_Enter);
            // 
            // cboMeterShield
            // 
            this.cboMeterShield.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMeterShield.FormattingEnabled = true;
            resources.ApplyResources(this.cboMeterShield, "cboMeterShield");
            this.cboMeterShield.Name = "cboMeterShield";
            // 
            // cboCableShield
            // 
            this.cboCableShield.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCableShield.FormattingEnabled = true;
            resources.ApplyResources(this.cboCableShield, "cboCableShield");
            this.cboCableShield.Name = "cboCableShield";
            // 
            // lblCUSTOMER_NAME
            // 
            resources.ApplyResources(this.lblCUSTOMER_NAME, "lblCUSTOMER_NAME");
            this.lblCUSTOMER_NAME.Name = "lblCUSTOMER_NAME";
            // 
            // txtLastNamekh
            // 
            resources.ApplyResources(this.txtLastNamekh, "txtLastNamekh");
            this.txtLastNamekh.Name = "txtLastNamekh";
            this.txtLastNamekh.ReadOnly = true;
            this.txtLastNamekh.TabStop = false;
            // 
            // lblBOX
            // 
            resources.ApplyResources(this.lblBOX, "lblBOX");
            this.lblBOX.Name = "lblBOX";
            // 
            // txtBox
            // 
            this.txtBox.BackColor = System.Drawing.Color.White;
            this.txtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtBox, "txtBox");
            this.txtBox.Name = "txtBox";
            this.txtBox.SearchMode = SoftTech.Component.ExTextbox.SearchModes.AdvanceSearch;
            this.txtBox.AdvanceSearch += new System.EventHandler(this.txtBox_AdvanceSearch);
            this.txtBox.CancelAdvanceSearch += new System.EventHandler(this.txtBox_CancelAdvanceSearch);
            this.txtBox.Enter += new System.EventHandler(this.txtMeter_Enter);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Name = "label13";
            // 
            // lblCUTTER
            // 
            resources.ApplyResources(this.lblCUTTER, "lblCUTTER");
            this.lblCUTTER.Name = "lblCUTTER";
            // 
            // lblBILLER
            // 
            resources.ApplyResources(this.lblBILLER, "lblBILLER");
            this.lblBILLER.Name = "lblBILLER";
            // 
            // lblCOLLECTOR
            // 
            resources.ApplyResources(this.lblCOLLECTOR, "lblCOLLECTOR");
            this.lblCOLLECTOR.Name = "lblCOLLECTOR";
            // 
            // txtCollector
            // 
            resources.ApplyResources(this.txtCollector, "txtCollector");
            this.txtCollector.Name = "txtCollector";
            this.txtCollector.ReadOnly = true;
            this.txtCollector.TabStop = false;
            // 
            // txtBiller
            // 
            resources.ApplyResources(this.txtBiller, "txtBiller");
            this.txtBiller.Name = "txtBiller";
            this.txtBiller.ReadOnly = true;
            this.txtBiller.TabStop = false;
            // 
            // txtCutter
            // 
            resources.ApplyResources(this.txtCutter, "txtCutter");
            this.txtCutter.Name = "txtCutter";
            this.txtCutter.ReadOnly = true;
            this.txtCutter.TabStop = false;
            // 
            // lblPOLE_AND_BOX_INSTALLATION
            // 
            resources.ApplyResources(this.lblPOLE_AND_BOX_INSTALLATION, "lblPOLE_AND_BOX_INSTALLATION");
            this.lblPOLE_AND_BOX_INSTALLATION.Name = "lblPOLE_AND_BOX_INSTALLATION";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Name = "label14";
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Name = "label28";
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Name = "label30";
            // 
            // txtMeterType
            // 
            resources.ApplyResources(this.txtMeterType, "txtMeterType");
            this.txtMeterType.Name = "txtMeterType";
            this.txtMeterType.ReadOnly = true;
            this.txtMeterType.TabStop = false;
            // 
            // txtBreakerType
            // 
            resources.ApplyResources(this.txtBreakerType, "txtBreakerType");
            this.txtBreakerType.Name = "txtBreakerType";
            this.txtBreakerType.ReadOnly = true;
            this.txtBreakerType.TabStop = false;
            // 
            // txtPoleCode
            // 
            resources.ApplyResources(this.txtPoleCode, "txtPoleCode");
            this.txtPoleCode.Name = "txtPoleCode";
            this.txtPoleCode.ReadOnly = true;
            this.txtPoleCode.TabStop = false;
            // 
            // txtCustomerType
            // 
            resources.ApplyResources(this.txtCustomerType, "txtCustomerType");
            this.txtCustomerType.Name = "txtCustomerType";
            this.txtCustomerType.ReadOnly = true;
            this.txtCustomerType.TabStop = false;
            // 
            // dtpActivateDate
            // 
            resources.ApplyResources(this.dtpActivateDate, "dtpActivateDate");
            this.dtpActivateDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpActivateDate.Name = "dtpActivateDate";
            // 
            // lblCONSTANT
            // 
            resources.ApplyResources(this.lblCONSTANT, "lblCONSTANT");
            this.lblCONSTANT.Name = "lblCONSTANT";
            // 
            // textBox1
            // 
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.TabStop = false;
            // 
            // lblCONSTANT_1
            // 
            resources.ApplyResources(this.lblCONSTANT_1, "lblCONSTANT_1");
            this.lblCONSTANT_1.Name = "lblCONSTANT_1";
            // 
            // textBox2
            // 
            resources.ApplyResources(this.textBox2, "textBox2");
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.TabStop = false;
            // 
            // lblSTART_USAGE
            // 
            resources.ApplyResources(this.lblSTART_USAGE, "lblSTART_USAGE");
            this.lblSTART_USAGE.Name = "lblSTART_USAGE";
            // 
            // txtSTART_USAGE
            // 
            resources.ApplyResources(this.txtSTART_USAGE, "txtSTART_USAGE");
            this.txtSTART_USAGE.Name = "txtSTART_USAGE";
            this.txtSTART_USAGE.Enter += new System.EventHandler(this.txtMeter_Enter);
            this.txtSTART_USAGE.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSTART_USAGE_KeyDown);
            // 
            // lblPOSITION_IN_BOX
            // 
            resources.ApplyResources(this.lblPOSITION_IN_BOX, "lblPOSITION_IN_BOX");
            this.lblPOSITION_IN_BOX.Name = "lblPOSITION_IN_BOX";
            // 
            // txtPositionInBox
            // 
            resources.ApplyResources(this.txtPositionInBox, "txtPositionInBox");
            this.txtPositionInBox.Name = "txtPositionInBox";
            this.txtPositionInBox.ReadOnly = true;
            this.txtPositionInBox.TabStop = false;
            this.txtPositionInBox.Enter += new System.EventHandler(this.txtMeter_Enter);
            this.txtPositionInBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPositionInBox_KeyPress);
            // 
            // lblVIEW_BOX_CUSTOMER
            // 
            resources.ApplyResources(this.lblVIEW_BOX_CUSTOMER, "lblVIEW_BOX_CUSTOMER");
            this.lblVIEW_BOX_CUSTOMER.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblVIEW_BOX_CUSTOMER.Name = "lblVIEW_BOX_CUSTOMER";
            this.lblVIEW_BOX_CUSTOMER.TabStop = true;
            this.lblVIEW_BOX_CUSTOMER.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblViewBoxCustomers_LinkClicked);
            // 
            // DialogCustomerActivate
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCustomerActivate";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCutoffDays)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblACTIVATE_DATE;
        private System.Windows.Forms.NumericUpDown nudCutoffDays;
        private System.Windows.Forms.Label lblCUT_DAY;
        private System.Windows.Forms.Label lblCUSTOMER_INFORMATION;
        private System.Windows.Forms.TextBox txtMeterAmp;
        private System.Windows.Forms.TextBox txtMeterVol;
        private System.Windows.Forms.TextBox txtMeterPhase;
        private System.Windows.Forms.Label lblCUSTOMER_TYPE;
        private System.Windows.Forms.Label lblMETER_CODE;
        private System.Windows.Forms.Label lblPOLE;
        private System.Windows.Forms.ComboBox cboBillingCycle;
        private System.Windows.Forms.Label lblCYCLE_NAME;
        private System.Windows.Forms.Label lblMETER_INSTALLATION;
        private System.Windows.Forms.Label lblMETER_TYPE;
        private System.Windows.Forms.Panel panel1;
        private SoftTech.Component.ExButton btnCLOSE;
        private SoftTech.Component.ExButton btnOK;
        private System.Windows.Forms.Label lblBREAKER_TYPE;
        private System.Windows.Forms.Label lblSHIELD;
        private System.Windows.Forms.TextBox txtBreakerAmp;
        private System.Windows.Forms.TextBox txtBreakerVol;
        private System.Windows.Forms.TextBox txtBreakerPhase;
        private System.Windows.Forms.Label lblBREAKER_CODE;
        private System.Windows.Forms.Label lblCABLE_SHIELD;
        private System.Windows.Forms.Label lblAMPARE_1;
        private System.Windows.Forms.Label lblVOLTAGE_1;
        private System.Windows.Forms.Label lblPHASE_1;
        private System.Windows.Forms.Label lblAMPARE;
        private System.Windows.Forms.Label lblVOLTAGE;
        private System.Windows.Forms.Label lblPHASE;
        private System.Windows.Forms.Label lblBREAKER_INSTALLATION;
        private SoftTech.Component.ExTextbox txtMeter;
        private SoftTech.Component.ExTextbox txtBreaker;
        private System.Windows.Forms.ComboBox cboCableShield;
        private System.Windows.Forms.ComboBox cboMeterShield;
        private System.Windows.Forms.Label lblCUSTOMER_NAME;
        private System.Windows.Forms.TextBox txtLastNamekh;
        private System.Windows.Forms.Label lblBOX;
        private SoftTech.Component.ExTextbox txtBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblCUTTER;
        private System.Windows.Forms.Label lblBILLER;
        private System.Windows.Forms.Label lblCOLLECTOR;
        private System.Windows.Forms.TextBox txtCutter;
        private System.Windows.Forms.TextBox txtBiller;
        private System.Windows.Forms.TextBox txtCollector;
        private System.Windows.Forms.Label lblPOLE_AND_BOX_INSTALLATION;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtPoleCode;
        private System.Windows.Forms.TextBox txtBreakerType;
        private System.Windows.Forms.TextBox txtMeterType;
        private System.Windows.Forms.TextBox txtCustomerType;
        private System.Windows.Forms.DateTimePicker dtpActivateDate;
        private System.Windows.Forms.Label lblCONSTANT_1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label lblCONSTANT;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblSTART_USAGE;
        private System.Windows.Forms.TextBox txtSTART_USAGE;
        private System.Windows.Forms.TextBox txtPositionInBox;
        private System.Windows.Forms.Label lblPOSITION_IN_BOX;
        private SoftTech.Component.ExLinkLabel lblVIEW_BOX_CUSTOMER;
    }
}
