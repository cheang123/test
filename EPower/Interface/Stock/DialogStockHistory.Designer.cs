﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.Stock
{
    partial class DialogStockHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvInvoiceItem = new System.Windows.Forms.DataGridView();
            this.lblPRODUCT_NAME = new System.Windows.Forms.Label();
            this.lblTRANS = new System.Windows.Forms.Label();
            this.cboTRAN_TYPE = new System.Windows.Forms.ComboBox();
            this.txtITEM = new System.Windows.Forms.TextBox();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnDETAIL = new SoftTech.Component.ExButton();
            this.lblDATE = new System.Windows.Forms.Label();
            this.dtp1 = new System.Windows.Forms.DateTimePicker();
            this.dtp2 = new System.Windows.Forms.DateTimePicker();
            this.btnREPORT = new SoftTech.Component.ExButton();
            this.STOCK_TRAN_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TRANS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceItem)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.btnREPORT);
            this.content.Controls.Add(this.dtp2);
            this.content.Controls.Add(this.dtp1);
            this.content.Controls.Add(this.lblDATE);
            this.content.Controls.Add(this.btnDETAIL);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.txtITEM);
            this.content.Controls.Add(this.cboTRAN_TYPE);
            this.content.Controls.Add(this.lblTRANS);
            this.content.Controls.Add(this.lblPRODUCT_NAME);
            this.content.Controls.Add(this.dgvInvoiceItem);
            this.content.Size = new System.Drawing.Size(577, 377);
            this.content.Text = "\'CONTENT\'";
            this.content.Controls.SetChildIndex(this.dgvInvoiceItem, 0);
            this.content.Controls.SetChildIndex(this.lblPRODUCT_NAME, 0);
            this.content.Controls.SetChildIndex(this.lblTRANS, 0);
            this.content.Controls.SetChildIndex(this.cboTRAN_TYPE, 0);
            this.content.Controls.SetChildIndex(this.txtITEM, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.btnDETAIL, 0);
            this.content.Controls.SetChildIndex(this.lblDATE, 0);
            this.content.Controls.SetChildIndex(this.dtp1, 0);
            this.content.Controls.SetChildIndex(this.dtp2, 0);
            this.content.Controls.SetChildIndex(this.btnREPORT, 0);
            // 
            // dgvInvoiceItem
            // 
            this.dgvInvoiceItem.AllowUserToAddRows = false;
            this.dgvInvoiceItem.AllowUserToDeleteRows = false;
            this.dgvInvoiceItem.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvInvoiceItem.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvInvoiceItem.BackgroundColor = System.Drawing.Color.White;
            this.dgvInvoiceItem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvInvoiceItem.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvInvoiceItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInvoiceItem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.STOCK_TRAN_ID,
            this.TRANS,
            this.QTY,
            this.CREATE_BY,
            this.CREATE_ON,
            this.NOTE});
            this.dgvInvoiceItem.Location = new System.Drawing.Point(7, 82);
            this.dgvInvoiceItem.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvInvoiceItem.Name = "dgvInvoiceItem";
            this.dgvInvoiceItem.ReadOnly = true;
            this.dgvInvoiceItem.RowHeadersVisible = false;
            this.dgvInvoiceItem.RowTemplate.Height = 25;
            this.dgvInvoiceItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvInvoiceItem.Size = new System.Drawing.Size(561, 259);
            this.dgvInvoiceItem.TabIndex = 24;
            // 
            // lblPRODUCT_NAME
            // 
            this.lblPRODUCT_NAME.AutoSize = true;
            this.lblPRODUCT_NAME.Location = new System.Drawing.Point(7, 12);
            this.lblPRODUCT_NAME.Margin = new System.Windows.Forms.Padding(2);
            this.lblPRODUCT_NAME.Name = "lblPRODUCT_NAME";
            this.lblPRODUCT_NAME.Size = new System.Drawing.Size(37, 19);
            this.lblPRODUCT_NAME.TabIndex = 25;
            this.lblPRODUCT_NAME.Text = "ទំនិញ";
            // 
            // lblTRANS
            // 
            this.lblTRANS.AutoSize = true;
            this.lblTRANS.Location = new System.Drawing.Point(330, 43);
            this.lblTRANS.Margin = new System.Windows.Forms.Padding(2);
            this.lblTRANS.Name = "lblTRANS";
            this.lblTRANS.Size = new System.Drawing.Size(41, 19);
            this.lblTRANS.TabIndex = 28;
            this.lblTRANS.Text = "ប្រភេទ";
            // 
            // cboTRAN_TYPE
            // 
            this.cboTRAN_TYPE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTRAN_TYPE.FormattingEnabled = true;
            this.cboTRAN_TYPE.Location = new System.Drawing.Point(397, 39);
            this.cboTRAN_TYPE.Margin = new System.Windows.Forms.Padding(2);
            this.cboTRAN_TYPE.Name = "cboTRAN_TYPE";
            this.cboTRAN_TYPE.Size = new System.Drawing.Size(171, 27);
            this.cboTRAN_TYPE.TabIndex = 30;
            this.cboTRAN_TYPE.SelectedIndexChanged += new System.EventHandler(this.cboTRAN_TYPE_SelectedIndexChanged);
            // 
            // txtITEM
            // 
            this.txtITEM.Enabled = false;
            this.txtITEM.Location = new System.Drawing.Point(89, 8);
            this.txtITEM.Margin = new System.Windows.Forms.Padding(2);
            this.txtITEM.Name = "txtITEM";
            this.txtITEM.Size = new System.Drawing.Size(226, 27);
            this.txtITEM.TabIndex = 31;
            // 
            // btnCLOSE
            // 
            this.btnCLOSE.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCLOSE.Location = new System.Drawing.Point(492, 347);
            this.btnCLOSE.Margin = new System.Windows.Forms.Padding(2);
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.Size = new System.Drawing.Size(77, 23);
            this.btnCLOSE.TabIndex = 32;
            this.btnCLOSE.Text = "បិទ";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnTrans_Click);
            // 
            // btnDETAIL
            // 
            this.btnDETAIL.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnDETAIL.Location = new System.Drawing.Point(304, 347);
            this.btnDETAIL.Margin = new System.Windows.Forms.Padding(2);
            this.btnDETAIL.Name = "btnDETAIL";
            this.btnDETAIL.Size = new System.Drawing.Size(90, 23);
            this.btnDETAIL.TabIndex = 33;
            this.btnDETAIL.Text = "លំអិត";
            this.btnDETAIL.UseVisualStyleBackColor = true;
            this.btnDETAIL.Click += new System.EventHandler(this.btnView_Click);
            // 
            // lblDATE
            // 
            this.lblDATE.AutoSize = true;
            this.lblDATE.Location = new System.Drawing.Point(7, 43);
            this.lblDATE.Margin = new System.Windows.Forms.Padding(2);
            this.lblDATE.Name = "lblDATE";
            this.lblDATE.Size = new System.Drawing.Size(65, 19);
            this.lblDATE.TabIndex = 34;
            this.lblDATE.Text = "កាលបរិច្ឆេទ";
            // 
            // dtp1
            // 
            this.dtp1.CustomFormat = "dd-MMM-yyyy";
            this.dtp1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp1.Location = new System.Drawing.Point(89, 39);
            this.dtp1.Margin = new System.Windows.Forms.Padding(2);
            this.dtp1.Name = "dtp1";
            this.dtp1.Size = new System.Drawing.Size(110, 27);
            this.dtp1.TabIndex = 36;
            this.dtp1.ValueChanged += new System.EventHandler(this.dtp1_ValueChanged);
            // 
            // dtp2
            // 
            this.dtp2.CustomFormat = "dd-MMM-yyyy";
            this.dtp2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp2.Location = new System.Drawing.Point(205, 39);
            this.dtp2.Margin = new System.Windows.Forms.Padding(2);
            this.dtp2.Name = "dtp2";
            this.dtp2.Size = new System.Drawing.Size(110, 27);
            this.dtp2.TabIndex = 37;
            this.dtp2.ValueChanged += new System.EventHandler(this.dtp2_ValueChanged);
            // 
            // btnREPORT
            // 
            this.btnREPORT.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnREPORT.Location = new System.Drawing.Point(398, 347);
            this.btnREPORT.Margin = new System.Windows.Forms.Padding(2);
            this.btnREPORT.Name = "btnREPORT";
            this.btnREPORT.Size = new System.Drawing.Size(90, 23);
            this.btnREPORT.TabIndex = 38;
            this.btnREPORT.Text = "របាយការណ៍";
            this.btnREPORT.UseVisualStyleBackColor = true;
            this.btnREPORT.Click += new System.EventHandler(this.btnReport_Click);
            // 
            // STOCK_TRAN_ID
            // 
            this.STOCK_TRAN_ID.DataPropertyName = "STOCK_TRAN_ID";
            this.STOCK_TRAN_ID.HeaderText = "STOCK_TRAN_ID";
            this.STOCK_TRAN_ID.Name = "STOCK_TRAN_ID";
            this.STOCK_TRAN_ID.ReadOnly = true;
            this.STOCK_TRAN_ID.Visible = false;
            // 
            // TRANS
            // 
            this.TRANS.DataPropertyName = "STOCK_TRAN_TYPE";
            this.TRANS.HeaderText = "ប្រតិបត្តិការ";
            this.TRANS.Name = "TRANS";
            this.TRANS.ReadOnly = true;
            this.TRANS.Width = 80;
            // 
            // QTY
            // 
            this.QTY.DataPropertyName = "QTY";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            this.QTY.DefaultCellStyle = dataGridViewCellStyle8;
            this.QTY.HeaderText = "បរិមាណ";
            this.QTY.Name = "QTY";
            this.QTY.ReadOnly = true;
            this.QTY.Width = 50;
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            this.CREATE_BY.HeaderText = "ធ្វើដោយ";
            this.CREATE_BY.Name = "CREATE_BY";
            this.CREATE_BY.ReadOnly = true;
            this.CREATE_BY.Width = 80;
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle9.Format = "dd-MM-yyyy";
            this.CREATE_ON.DefaultCellStyle = dataGridViewCellStyle9;
            this.CREATE_ON.HeaderText = "កាលបរិច្ខេទ";
            this.CREATE_ON.Name = "CREATE_ON";
            this.CREATE_ON.ReadOnly = true;
            this.CREATE_ON.Width = 80;
            // 
            // NOTE
            // 
            this.NOTE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NOTE.DataPropertyName = "REMARK";
            this.NOTE.HeaderText = "ចំណាំ";
            this.NOTE.Name = "NOTE";
            this.NOTE.ReadOnly = true;
            // 
            // DialogStockHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 400);
            this.Name = "DialogStockHistory";
            this.Text = "ប្រវត្តិ  ប្រតិបត្តិការណ៍";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dgvInvoiceItem;
        private TextBox txtITEM;
        private ComboBox cboTRAN_TYPE;
        private Label lblTRANS;
        private Label lblPRODUCT_NAME;
        private ExButton btnDETAIL;
        private ExButton btnCLOSE;
        private Label lblDATE;
        private DateTimePicker dtp2;
        private DateTimePicker dtp1;
        private ExButton btnREPORT;
        private DataGridViewTextBoxColumn STOCK_TRAN_ID;
        private DataGridViewTextBoxColumn TRANS;
        private DataGridViewTextBoxColumn QTY;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn NOTE;
    }
}