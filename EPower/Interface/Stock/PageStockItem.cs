﻿using System;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.Stock
{
    public partial class PageStockItem : Form
    {
        public TBL_STOCK_ITEM Item
        {
            get
            {
                TBL_STOCK_ITEM objAmpare = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int ampareID = (int)dgv.SelectedRows[0].Cells["ITEM_ID"].Value;
                        objAmpare = DBDataContext.Db.TBL_STOCK_ITEMs.FirstOrDefault(x => x.ITEM_ID == ampareID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objAmpare;
            }
        }

        #region Constructor
        public PageStockItem()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            txt_QuickSearch(null, null);
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new ampare.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogStockItem dig = new DialogStockItem(GeneralProcess.Insert, new TBL_STOCK_ITEM() { ITEM_NAME = "" });
            if (dig.ShowDialog() == DialogResult.OK)
            {
                txt_QuickSearch(null, null);
                UIHelper.SelectRow(dgv, dig.Ampare.ITEM_ID);
            }
        }

        /// <summary>
        /// Edit ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                DialogStockItem dig = new DialogStockItem(GeneralProcess.Update, Item);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Ampare.ITEM_ID);
                }
            }
        }

        /// <summary>
        /// Remove ampare
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exButton1_Click(object sender, EventArgs e)
        {
            if (IsDeletable())
            {
                DialogStockItem dig = new DialogStockItem(GeneralProcess.Delete, Item);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    txt_QuickSearch(null, null);
                    UIHelper.SelectRow(dgv, dig.Ampare.ITEM_ID - 1);
                }
            }
        }

        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            try
            {
                dgv.DataSource = from ap in DBDataContext.Db.TBL_STOCK_ITEMs
                                 where ap.IS_ACTIVE &&
                                 ap.ITEM_NAME.ToLower().Contains(txtQuickSearch.Text.ToLower().Trim())
                                 select ap;
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            
        }
        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }
            btnEdit_Click(null, null);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            //TODO: can delete record.
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
                if (DBDataContext.Db.TBL_STOCK_TRANs.Where(x=>x.ITEM_ID==Item.ITEM_ID).Count()>0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    val = false;
                }
            }
            return val;
        }
        #endregion

      
    }
}