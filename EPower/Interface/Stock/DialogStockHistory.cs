﻿using System;
using System.Linq;
using EPower.Properties;
using SoftTech.Helper;
using SoftTech;
using SoftTech.Component;

namespace EPower.Interface.Stock
{
    public partial class DialogStockHistory : ExDialog
    {
        int ItemID = 0;
        StockItemType itemType;
        bool _loading = false;
        public DialogStockHistory(int itemId,StockItemType type,string itemName)
        {
            InitializeComponent();

            this._loading = true;

            UIHelper.SetDataSourceToComboBox(this.cboTRAN_TYPE,DBDataContext.Db.TLKP_STOCK_TRAN_TYPEs,Resources.ALL_STOCK_TRANSACTION_TYPE);

            this.ItemID = itemId; 
            this.txtITEM.Text = itemName;
            this.itemType = type;

            this.dtp1.Value = DBDataContext.Db.GetSystemDate().Date.AddDays(-7);
            this.dtp2.Value = DBDataContext.Db.GetSystemDate().Date;
            
            this._loading = false;
            
            this.bind();
        }
        private void bind()
        {
            if (this._loading) return;
             
            int tranTypeId = (int)this.cboTRAN_TYPE.SelectedValue; 
            DateTime date1 = dtp1.Value.Date;
            DateTime date2 = dtp2.Value.Date.AddDays(1); 
            this.dgvInvoiceItem.DataSource = from s in DBDataContext.Db.TBL_STOCK_TRANs
                                             join t in DBDataContext.Db.TLKP_STOCK_TRAN_TYPEs on s.STOCK_TRAN_TYPE_ID equals t.STOCK_TRAN_TYPE_ID
                                             join stf in DBDataContext.Db.TLKP_STOCK_TYPEs on s.FROM_STOCK_TYPE_ID equals stf.STOCK_TYPE_ID
                                             join stt in DBDataContext.Db.TLKP_STOCK_TYPEs on s.TO_STOCK_TYPE_ID equals stt.STOCK_TYPE_ID
                                             where (tranTypeId == 0 || s.STOCK_TRAN_TYPE_ID == tranTypeId)
                                                    && s.CREATE_ON.Date >= date1 && s.CREATE_ON.Date <= date2
                                                    && s.ITEM_ID == this.ItemID
                                                    && s.ITEM_TYPE_ID ==(int)this.itemType
                                             orderby s.CREATE_ON descending
                                             select new
                                             {
                                                 s.STOCK_TRAN_ID,
                                                 s.CREATE_BY,
                                                 s.CREATE_ON,
                                                 s.QTY,
                                                 s.REMARK,
                                                 // FROM_TYPE = stf.STOCK_TYPE_NAME,
                                                 // TO_TYPE = stt.STOCK_TYPE_NAME,
                                                 t.STOCK_TRAN_TYPE
                                             };
        } 

        private void cboTRAN_TYPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.bind();
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dtp2_ValueChanged(object sender, EventArgs e)
        {
            this.bind();
        }

        private void dtp1_ValueChanged(object sender, EventArgs e)
        {
            this.bind();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            if (this.dgvInvoiceItem.SelectedRows.Count ==0 ) return;
            long tranId = (long)this.dgvInvoiceItem.SelectedRows[0].Cells[this.STOCK_TRAN_ID.Name].Value;
            TBL_STOCK_TRAN obj = DBDataContext.Db.TBL_STOCK_TRANs.FirstOrDefault(row => row.STOCK_TRAN_ID == tranId);
            DialogStockTran diag = new DialogStockTran(GeneralProcess.View,(StockItemType )obj.ITEM_TYPE_ID, obj);
            diag.Show();
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            CrystalReportHelper ch = new CrystalReportHelper("ReportStockHistory.rpt");
            ch.SetParameter("@ITEM_ID", this.ItemID);
            ch.SetParameter("@ITEM_TYPE_ID", (int)this.itemType);
            ch.SetParameter("@D1", this.dtp1.Value.Date);
            ch.SetParameter("@D2", this.dtp2.Value.Date);
            ch.SetParameter("@STOCK_TRAN_TYPE_ID", (int)this.cboTRAN_TYPE.SelectedValue);
            ch.ViewReport("");
        }
    }
}
