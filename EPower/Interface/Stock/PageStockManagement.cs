﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface.Stock
{
    public partial class PageStockManagement : Form
    {
        public TBL_STOCK_ITEM Item
        {
            get
            {
                TBL_STOCK_ITEM objAmpare = null;
                if (dgv.SelectedRows.Count > 0)
                {
                    try
                    {
                        int ampareID = (int)dgv.SelectedRows[0].Cells["ITEM_ID"].Value;
                        objAmpare = DBDataContext.Db.TBL_STOCK_ITEMs.FirstOrDefault(x => x.ITEM_ID == ampareID);
                    }
                    catch (Exception ex)
                    {
                        MsgBox.ShowError(ex);
                    }
                }
                return objAmpare;
            }
        }

        #region Constructor
        public PageStockManagement()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            this.bind();
            BindReport();
        }
        #endregion

        #region Operation
        /// <summary>
        /// Add new ampare.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0)
            {
                return;
            }

            if ((int)this.dgv.SelectedRows[0].Cells["TYPE"].Value == (int)StockItemType.Meter)
            {
                MsgBox.ShowInformation(Resources.STOCK_ITEM_COULD_NOT_ADD_TRANSACTION);
                return;
            }
            int itemId = (int)this.dgv.SelectedRows[0].Cells[this.ITEM_ID.Name].Value;

            DialogStockTran dialog = new DialogStockTran(GeneralProcess.Insert,(StockItemType)(int)this.dgv.SelectedRows[0].Cells["TYPE"].Value, new TBL_STOCK_TRAN()
            {
                STOCK_TRAN_TYPE_ID = (int)StockTranType.StockIn,
                ITEM_ID = itemId
            });

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                this.bind();
            } 
        }

        

        /// <summary>
        /// Load data from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_QuickSearch(object sender, EventArgs e)
        {
            bind();
        }
        private class StockResult
        { 
            public int ITEM_ID { get; set; }
            public string ITEM_NAME { get; set; }
            public string UNIT { get; set; }
            public decimal QTY_STOCK { get; set; }
            public decimal QTY_USED { get; set; }
            public decimal QTY_DAMAGE { get; set; }
            public StockItemType TYPE { get; set; }
        }
        private void bind()
        {
            var val = this.txtQuickSearch.Text.ToLower();

            var sqlStocks =   from i in DBDataContext.Db.TBL_STOCK_ITEMs.Where(row=>row.IS_ACTIVE)
                              join t in ( from t in DBDataContext.Db.TBL_STOCK_TRANs
                                          where t.ITEM_TYPE_ID ==(int)StockItemType.StockItem
                                          group t by t.ITEM_ID into g
                                          select new
                                          {
                                              ITEM_ID    = g.Key,
                                              QTY_STOCK  = g.Sum(row => (row.TO_STOCK_TYPE_ID == (int)StockType.Stock       ? row.QTY : 0) - (row.FROM_STOCK_TYPE_ID == (int)StockType.Stock ? row.QTY : 0)),
                                              QTY_USED   = g.Sum(row => (row.TO_STOCK_TYPE_ID == (int)StockType.Used        ? row.QTY : 0) - (row.FROM_STOCK_TYPE_ID == (int)StockType.Used ? row.QTY : 0)),
                                              QTY_DAMAGE = g.Sum(row => (row.TO_STOCK_TYPE_ID == (int)StockType.Unavailable ? row.QTY : 0) - (row.FROM_STOCK_TYPE_ID == (int)StockType.Unavailable ? row.QTY : 0))
                                          }) 
                              on i.ITEM_ID equals t.ITEM_ID  into joinItemStock
                              from it in joinItemStock.DefaultIfEmpty()
                              where i.ITEM_NAME.ToLower().Contains(val)
                              select new StockResult(){
                                    ITEM_ID = i.ITEM_ID,
                                    ITEM_NAME = i.ITEM_NAME,
                                    UNIT = i.UNIT,
                                    QTY_STOCK = it == null ? 0 : it.QTY_STOCK,
                                    QTY_USED = it == null ? 0 : it.QTY_USED,
                                    QTY_DAMAGE = it == null ? 0 : it.QTY_DAMAGE,
                                    TYPE = StockItemType.StockItem
                              };

            var sqlMeters = from m in DBDataContext.Db.TBL_METER_TYPEs
                            where m.IS_ACTIVE && m.METER_TYPE_NAME.ToLower().Contains(val)
                            select new StockResult()
                            {
                                ITEM_ID = m.METER_TYPE_ID,
                                ITEM_NAME = m.METER_TYPE_NAME,
                                UNIT = "",
                                QTY_STOCK = DBDataContext.Db.TBL_METERs.Count(row => row.STATUS_ID == (int)MeterStatus.Stock && row.METER_TYPE_ID == m.METER_TYPE_ID),
                                QTY_USED = DBDataContext.Db.TBL_METERs.Count(row => row.STATUS_ID == (int)MeterStatus.Used && row.METER_TYPE_ID == m.METER_TYPE_ID),
                                QTY_DAMAGE = DBDataContext.Db.TBL_METERs.Count(row => row.STATUS_ID == (int)MeterStatus.Unavailable && row.METER_TYPE_ID == m.METER_TYPE_ID),
                                TYPE = StockItemType.Meter
                            };

            var s = from t in DBDataContext.Db.TBL_STOCK_TRANs
                    where t.ITEM_TYPE_ID == (int)StockItemType.Breaker
                    group t by t.ITEM_ID into g
                    select new
                    {
                        ITEM_ID = g.Key,
                        QTY_STOCK = g.Sum(row => (row.TO_STOCK_TYPE_ID == (int)StockType.Stock ? row.QTY : 0) - (row.FROM_STOCK_TYPE_ID == (int)StockType.Stock ? row.QTY : 0)),
                        QTY_USED = g.Sum(row => (row.TO_STOCK_TYPE_ID == (int)StockType.Used ? row.QTY : 0) - (row.FROM_STOCK_TYPE_ID == (int)StockType.Used ? row.QTY : 0)),
                        QTY_DAMAGE = g.Sum(row => (row.TO_STOCK_TYPE_ID == (int)StockType.Unavailable ? row.QTY : 0) - (row.FROM_STOCK_TYPE_ID == (int)StockType.Unavailable ? row.QTY : 0))
                    }; 
            var sqlBreakers = from m in DBDataContext.Db.TBL_CIRCUIT_BREAKER_TYPEs
                              join t in
                                  (from t in DBDataContext.Db.TBL_STOCK_TRANs
                                   where t.ITEM_TYPE_ID == (int)StockItemType.Breaker
                                   group t by t.ITEM_ID into g
                                   select new
                                   {
                                       BREAKER_TYPE_ID = g.Key,
                                       QTY_STOCK = g.Sum(row => (row.TO_STOCK_TYPE_ID == (int)StockType.Stock ? row.QTY : 0) - (row.FROM_STOCK_TYPE_ID == (int)StockType.Stock ? row.QTY : 0)),
                                       QTY_USED = g.Sum(row => (row.TO_STOCK_TYPE_ID == (int)StockType.Used ? row.QTY : 0) - (row.FROM_STOCK_TYPE_ID == (int)StockType.Used ? row.QTY : 0)),
                                       QTY_DAMAGE = g.Sum(row => (row.TO_STOCK_TYPE_ID == (int)StockType.Unavailable ? row.QTY : 0) - (row.FROM_STOCK_TYPE_ID == (int)StockType.Unavailable ? row.QTY : 0))
                                   })
                              on m.BREAKER_TYPE_ID equals t.BREAKER_TYPE_ID into joinItemStock
                              from it in joinItemStock.DefaultIfEmpty()
                              where m.IS_ACTIVE && m.BREAKER_TYPE_NAME.ToLower().Contains(val)
                               

                              select new StockResult()
                              {                                  
                                  ITEM_ID = m.BREAKER_TYPE_ID,
                                  ITEM_NAME = m.BREAKER_TYPE_NAME,
                                  UNIT = "",
                                  QTY_STOCK = it == null ? 0 : it.QTY_STOCK,
                                  QTY_USED = it == null ? 0 : it.QTY_USED,
                                  QTY_DAMAGE = it == null ? 0 : it.QTY_DAMAGE,                                  
                                  TYPE = StockItemType.Breaker                                 
                              };
            
            this.dgv.DataSource = sqlStocks .Union(sqlMeters)
                                            .Union(sqlBreakers)
                                            .OrderBy(row => row.TYPE)
                                            .ThenBy(row  => row.ITEM_NAME);
        }
        #endregion

        #region Method
        private bool IsDeletable()
        {
            //TODO: can delete record.
            bool val = false;
            if (dgv.SelectedRows.Count > 0)
            {
                val = true;
                if (DBDataContext.Db.TBL_STOCK_TRANs.Where(x=>x.ITEM_ID==Item.ITEM_ID).Count()>0)
                {
                    MsgBox.ShowInformation(Resources.MS_CANNOT_REMOVE);
                    val = false;
                }
            }
            return val;
        }
        #endregion 
 
        private void btnHistory_Click(object sender, EventArgs e)
        {
            if (this.dgv.SelectedRows.Count == 0)
            {
                return;
            }
            int itemId = (int)this.dgv.SelectedRows[0].Cells[this.ITEM_ID.Name].Value;
            string itemName = this.dgv.SelectedRows[0].Cells[this.PRODUCT_NAME.Name].Value.ToString();
            StockItemType type = (StockItemType)this.dgv.SelectedRows[0].Cells[this.TYPE.Name].Value;
            DialogStockHistory diag = new DialogStockHistory(itemId,type,itemName);
            diag.ShowDialog();

        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            ViewReportStock().ViewReport(string.Empty);
        }

        public CrystalReportHelper ViewReportStock()
        {
            return new CrystalReportHelper("ReportStock.rpt");             
        }

        private void BindReport()
        {
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("REPORT_ID", typeof(int));
            dtReport.Columns.Add("REPORT_NAME", typeof(string));
            dtReport.Rows.Add(0, Resources.REPORT + Resources.STOCK);
            UIHelper.SetDataSourceToComboBox(cboReport, dtReport, "REPORT_ID", "REPORT_NAME");
            cboReport.SelectedIndex = 0;
        }

        void ViewReport()
        {
            if (dgv.SelectedRows.Count == 0)
            {
                return;
            }

            if (cboReport.SelectedIndex == 0)
            {
                Runner.Run(delegate ()
                {
                    CrystalReportHelper cr = ViewReportStock();
                    cr.ViewReport(Resources.REPORT + Resources.STOCK);
                    cr.Dispose();
                });
            }
        }

        private void cboReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ViewReport();
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            ViewReport();
        }
    }
}