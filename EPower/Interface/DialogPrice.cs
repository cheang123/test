﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    enum ChangeStatus
    {
        New = 0,
        Edit = 1,
        Delete = 2
    }

    public partial class DialogPrice : ExDialog
    {
        #region Private 

        GeneralProcess _flag = GeneralProcess.Insert;
        TBL_PRICE _objNew = new TBL_PRICE();

        public TBL_PRICE Price
        {
            get { return _objNew; }
            set { _objNew = value; }
        }
        TBL_PRICE _objOld = new TBL_PRICE();
        DataTable _dtPriceDetail = new DataTable();
        IEnumerable<TBL_PRICE_DETAIL> _lstOldPriceDetail = null;
        bool _blnIsStart = true;
        object _objBeforeEdit = null;

        #endregion Private

        #region Constructor
        public DialogPrice(TBL_PRICE obj, GeneralProcess flag)
        {
            InitializeComponent();
            this._flag = flag;

            UIHelper.SetDataSourceToComboBox(cboCurrency, Lookup.GetCurrencies());


            if (_flag != GeneralProcess.Insert)
            {
                //initialize price detail to temp table
                if (_flag == GeneralProcess.Update)
                {
                    this.Text = Resources.UPDATE + " " + this.Text;
                }
                obj._CopyTo(this._objNew);
                obj._CopyTo(this._objOld);

                InitialDataPriceDetail();

                //read object to control
                read();

                if (_flag == GeneralProcess.Delete)
                {
                    this.Text = Resources.DELETE + " " + this.Text;
                    UIHelper.SetEnabled(this, false);
                }
            }
            else
            {
                this.Text = Resources.INSERT + " " + this.Text;
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;

            //set columns type to datagrid view
            dgvPriceDetail.Columns[this.FROM_USAGE.Name].ValueType = typeof(int);
            dgvPriceDetail.Columns[this.TO_USAGE.Name].ValueType = typeof(int);
            dgvPriceDetail.Columns[this.PRICE.Name].ValueType = typeof(decimal);
            dgvPriceDetail.Columns[this.ALLOW_FIXED_AMOUNT.Name].ValueType = typeof(bool);
            dgvPriceDetail.Columns[this.FIXED_AMOUNT.Name].ValueType = typeof(decimal);

            //dgvPriceDetail.Columns[this.PRICE.Name].DefaultCellStyle.Format="N5";
            txtPriceName.Focus();
            _blnIsStart = false;
        }
        #endregion Constructor

        #region Method

        private void InitialDataPriceDetail()
        {
            _dtPriceDetail = (from pd in DBDataContext.Db.TBL_PRICE_DETAILs
                              where pd.PRICE_ID == _objOld.PRICE_ID
                              select new
                              {
                                  pd.PRICE_DETAIL_ID,
                                  pd.START_USAGE,
                                  pd.END_USAGE,
                                  pd.PRICE,
                                  STATUS = 1
                              })._ToDataTable();

            _lstOldPriceDetail = from pd in DBDataContext.Db.TBL_PRICE_DETAILs
                                 where pd.PRICE_ID == _objOld.PRICE_ID
                                 select pd;
        }

        private void LoadPriceDetail()
        {
            foreach (TBL_PRICE_DETAIL pd in _lstOldPriceDetail)
            {
                dgvPriceDetail.Rows.Add(pd.PRICE_DETAIL_ID, (int)pd.START_USAGE, (int)pd.END_USAGE, pd.PRICE, pd.IS_ALLOW_FIXED_AMOUNT, pd.FIXED_AMOUNT, (int)ChangeStatus.Edit);
                dgvPriceDetail[this.FIXED_AMOUNT.Name, this.dgvPriceDetail.Rows.Count - 1].ReadOnly = !pd.IS_ALLOW_FIXED_AMOUNT;
            }
        }

        private void SavePriceDetail(TBL_CHANGE_LOG objChangeLog)
        {
            List<int> lstInsertedID = new List<int>();//store all inserted PriceDetailId
            //update or add new pricedetail records to database
            foreach (DataGridViewRow dgvr in dgvPriceDetail.Rows)
            {
                int intStatus = int.Parse(dgvr.Cells[STATUS.Name].Value.ToString());
                //Create new object from datagrid
                TBL_PRICE_DETAIL objPriceDetailNew = new TBL_PRICE_DETAIL();
                objPriceDetailNew.PRICE_DETAIL_ID = int.Parse(dgvr.Cells[PRICE_DETAIL_ID.Name].Value.ToString());
                objPriceDetailNew.PRICE_ID = _objNew.PRICE_ID;
                objPriceDetailNew.START_USAGE = DataHelper.ParseToDecimal(dgvr.Cells[FROM_USAGE.Name].Value.ToString());
                objPriceDetailNew.END_USAGE = DataHelper.ParseToDecimal(dgvr.Cells[TO_USAGE.Name].Value.ToString());
                objPriceDetailNew.PRICE = DataHelper.ParseToDecimal(dgvr.Cells[PRICE.Name].Value.ToString());
                objPriceDetailNew.IS_ALLOW_FIXED_AMOUNT = (bool)(dgvr.Cells[this.ALLOW_FIXED_AMOUNT.Name].Value ?? false);
                objPriceDetailNew.FIXED_AMOUNT = DataHelper.ParseToDecimal(dgvr.Cells[this.FIXED_AMOUNT.Name].Value.ToString());

                //if user add new price detail
                if (intStatus == (int)ChangeStatus.New)
                {
                    DBDataContext.Db.InsertChild(objPriceDetailNew, this._objNew, ref objChangeLog);

                    //add to lstInsertedID inorder to check when delete
                    lstInsertedID.Add(objPriceDetailNew.PRICE_DETAIL_ID);
                }

                //if user edit on existing price detail
                else if (intStatus == (int)ChangeStatus.Edit)
                {
                    TBL_PRICE_DETAIL objPriceDetailOld = new TBL_PRICE_DETAIL();
                    _lstOldPriceDetail.FirstOrDefault(pd => pd.PRICE_DETAIL_ID == objPriceDetailNew.PRICE_DETAIL_ID)._CopyTo(objPriceDetailOld);
                    DBDataContext.Db.UpdateChild(objPriceDetailOld, objPriceDetailNew, _objOld, ref objChangeLog);
                }
            }

            if (_lstOldPriceDetail != null)
            {
                //delete PriceDetail records to database
                foreach (TBL_PRICE_DETAIL pd in _lstOldPriceDetail)
                {
                    //if the recored is not newly inserted
                    //then delete it from db
                    if (!lstInsertedID.Contains(pd.PRICE_DETAIL_ID))
                    {
                        if (IsDeleted(pd.PRICE_DETAIL_ID))
                        {
                            DBDataContext.Db.DeleteChild(pd, this._objNew, ref objChangeLog);
                        }
                    }
                }
            }

        }

        private bool invalid()
        {
            bool blnReturn = false;
            this.ClearAllValidation();

            if (this.txtPriceName.Text.Trim() == "")
            {
                txtPriceName.SetValidation(string.Format(Resources.REQUIRED, lblPRICE_NAME.Text));
                txtPriceName.Focus();
                blnReturn = true;
            }
            if (this.cboCurrency.SelectedIndex == -1)
            {
                cboCurrency.SetValidation(string.Format(Resources.REQUIRED, lblCURRENCY.Text));
                blnReturn = true;
            }
            if (this._flag != GeneralProcess.Delete && !DataHelper.IsPositiveNumber(this.txtCAPACITY_PRICE.Text))
            {
                txtCAPACITY_PRICE.SetValidation(Resources.REQUIRED_POSITIVE_NUMBER);
                blnReturn = true;
            }
            if (this._flag != GeneralProcess.Delete && this.dgvPriceDetail.Rows.Count == 0)
            {
                MsgBox.ShowInformation(Resources.MS_INSERT_ONE_RECORD_OF_PRICE);
                return true;
            }

            if (dgvPriceDetail.Rows.Count > 0)
            {
                //check whether end with -1 or not
                DataGridViewRow dgvr = dgvPriceDetail.Rows[dgvPriceDetail.Rows.Count - 1];
                int intEndUsage = DataHelper.ParseToInt(dgvr.Cells[TO_USAGE.Name].Value.ToString());
                if (intEndUsage != (int)Unlimitted.INT)
                {
                    MsgBox.ShowInformation(lblTERM_OF_PRICE.Text);
                    return true;
                }

                //check price must be > 0
                foreach (DataGridViewRow dgvRow in dgvPriceDetail.Rows)
                {
                    double dblPrice = double.Parse(dgvRow.Cells[PRICE.Name].Value.ToString());
                    if (dblPrice <= 0)
                    {
                        string strPriceNot0 = Resources.REQUEST_GRATER_THAN_ZERO;
                        MsgBox.ShowInformation(strPriceNot0);
                        return true;
                    }
                }
            }
            return blnReturn;
        }

        private void read()
        {
            txtPriceName.Text = _objNew.PRICE_NAME;
            cboCurrency.SelectedValue = _objNew.CURRENCY_ID;
            this.chkSTANDARD_PRICING.Checked = this._objNew.IS_STANDARD_RATING;
            this.txtCAPACITY_PRICE.Text = _objNew.CAPACITY_PRICE.ToString(UIHelper._DefaultPriceFormat);
            //Load price to detail to datagridview
            LoadPriceDetail();
        }

        private void write()
        {
            _objNew.PRICE_NAME = txtPriceName.Text.Trim();
            _objNew.CURRENCY_ID = int.Parse(cboCurrency.SelectedValue.ToString());
            _objNew.CAPACITY_PRICE = DataHelper.ParseToDecimal(txtCAPACITY_PRICE.Text);
            if (_flag == GeneralProcess.Insert)
            {
                _objNew.CREATE_BY = Login.CurrentLogin.LOGIN_NAME;
                _objNew.CREATE_ON = DBDataContext.Db.GetSystemDate();
                _objNew.IS_ACTIVE = true;
            }
            _objNew.IS_STANDARD_RATING = this.chkSTANDARD_PRICING.Checked;
        }

        private void AdjustStartUsage(int intRowIndex, int intValue)
        {
            dgvPriceDetail.Rows[intRowIndex].Cells[FROM_USAGE.Name].Value = intValue;
            //if the row is not last row
            if (intRowIndex < dgvPriceDetail.Rows.Count - 1)
            {
                int intEndUsage = int.Parse(dgvPriceDetail.Rows[intRowIndex].Cells[TO_USAGE.Name].Value.ToString());
                //if start usage > end usage
                if (intValue >= intEndUsage)
                {
                    dgvPriceDetail.Rows[intRowIndex].Cells[TO_USAGE.Name].Value = intValue + 50;
                    //update to start usage of next row
                    AdjustStartUsage(intRowIndex + 1, intValue + 50 + 1);
                }
            }
        }

        //if the row already deledted from datagridview
        bool IsDeleted(int intPriceDetailID)
        {
            bool blnReturn = true;
            foreach (DataGridViewRow dgvr in dgvPriceDetail.Rows)
            {
                int PriceDetailId = int.Parse(dgvr.Cells[PRICE_DETAIL_ID.Name].Value.ToString());
                if (intPriceDetailID == PriceDetailId)
                {
                    blnReturn = false;
                    break;
                }
            }
            return blnReturn;
        }
        void toolTip_Draw(object sender, DrawToolTipEventArgs e)
        {
            using (e.Graphics)
            {
                Font f = new Font("Khmer OS System", 8.5f);
                e.DrawBackground();
                e.DrawBorder();
                e.Graphics.DrawString(e.ToolTipText, f, Brushes.Black, new PointF(2, 2));
            }
        }
        void toolTip_Popup(object sender, PopupEventArgs e)
        {
            e.ToolTipSize = TextRenderer.MeasureText(Resources.MS_FOR_CAPACITY_CHARGE, new Font("Khmer OS System", 8.5f));
        }
        #endregion Method

        #region Event

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkAdd_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvPriceDetail.Rows.Count > 0)
            {
                bool blnIsUnlimitted = false;
                foreach (DataGridViewRow dgvr in dgvPriceDetail.Rows)
                {
                    if ((int)dgvr.Cells[TO_USAGE.Name].Value == -1)
                    {
                        MsgBox.ShowInformation(Resources.MSG_EDIT_POWER_BEFORE_NEW_LINE);
                        blnIsUnlimitted = true;
                        break;
                    }
                }
                if (!blnIsUnlimitted)
                {
                    DataGridViewRow dgvr = dgvPriceDetail.Rows[dgvPriceDetail.Rows.Count - 1];
                    int intEndUsage = (int)dgvr.Cells[TO_USAGE.Name].Value;
                    dgvPriceDetail.Rows.Add(0, intEndUsage + 1, (int)Unlimitted.INT, 0, false, 0, 0);
                    dgvPriceDetail[this.FIXED_AMOUNT.Name, this.dgvPriceDetail.Rows.Count - 1].ReadOnly = true;
                }
            }
            else
            {
                dgvPriceDetail.Rows.Add(0, 0, (int)Unlimitted.INT, 0, false, 0, 0);
                dgvPriceDetail[this.FIXED_AMOUNT.Name, this.dgvPriceDetail.Rows.Count - 1].ReadOnly = true;
            }
        }

        private void linkRemove_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvPriceDetail.Rows.Count > 0)
            {
                dgvPriceDetail.Rows.RemoveAt(dgvPriceDetail.Rows.Count - 1);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            btnOK.Focus();

            this.ClearAllValidation();

            if (invalid())
            {
                return;
            }

            write();

            if (DBDataContext.Db.IsExits(_objNew, "PRICE_NAME"))
            {
                txtPriceName.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblPRICE_NAME.Text));
                return;
            }
            try
            {
                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    TBL_CHANGE_LOG objChangeLog = null;
                    if (this._flag == GeneralProcess.Insert)
                    {
                        objChangeLog = DBDataContext.Db.Insert(this._objNew);
                    }
                    else if (this._flag == GeneralProcess.Update)
                    {
                        objChangeLog = DBDataContext.Db.Update(this._objOld, this._objNew);
                    }
                    else if (this._flag == GeneralProcess.Delete)
                    {
                        objChangeLog = DBDataContext.Db.Delete(this._objNew);
                    }
                    SavePriceDetail(objChangeLog);
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void txtPriceName_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void txtCAPACITY_PRICE_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        private void dgvPriceDetail_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void dgvPriceDetail_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            dgvPriceDetail.CancelEdit();
        }

        private void dgvPriceDetail_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (_blnIsStart)
            {
                return;
            }
            if (e.RowIndex == -1 || e.ColumnIndex == -1)
            {
                return;
            }

            //edit on end usage columns
            if (e.ColumnIndex == dgvPriceDetail.Columns[TO_USAGE.Name].Index)
            {
                int intEndUsage = DataHelper.ParseToInt(dgvPriceDetail.Rows[e.RowIndex].Cells[TO_USAGE.Name].Value.ToString());
                int intStartUsage = DataHelper.ParseToInt(dgvPriceDetail.Rows[e.RowIndex].Cells[FROM_USAGE.Name].Value.ToString());
                //if not edit on last row
                if (e.RowIndex != dgvPriceDetail.Rows.Count - 1)
                {
                    //not allow to type unlimitted value
                    if (intEndUsage == (int)Unlimitted.INT)
                    {
                        MsgBox.ShowInformation(lblTERM_OF_PRICE.Text);
                        dgvPriceDetail.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = _objBeforeEdit;
                    }
                    else
                    {
                        //end usage must bigger then start usage
                        if (intStartUsage > intEndUsage)
                        {
                            MsgBox.ShowInformation(Resources.MS_START_MUST_SMALLER_THAN_END_VALUE);
                            dgvPriceDetail.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = _objBeforeEdit;
                        }
                        else
                        {
                            //adjust start usage of the next row
                            AdjustStartUsage(e.RowIndex + 1, intEndUsage + 1);
                        }
                    }
                }
                else
                {
                    if (intEndUsage != (int)Unlimitted.INT)
                    {
                        //end usage must bigger then start usage
                        if (intStartUsage > intEndUsage)
                        {
                            MsgBox.ShowInformation(Resources.MS_START_MUST_SMALLER_THAN_END_VALUE);
                            dgvPriceDetail.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = _objBeforeEdit;
                        }
                    }
                }
            }
            //if user edit on PRICE column
            else if (e.ColumnIndex == dgvPriceDetail.Columns[PRICE.Name].Index)
            {
                decimal dblPrice = DataHelper.ParseToDecimal(dgvPriceDetail.Rows[e.RowIndex].Cells[PRICE.Name].Value.ToString());
                if (dblPrice <= 0)
                {
                    string strPriceNot0 = Resources.REQUEST_GRATER_THAN_ZERO;
                    MsgBox.ShowInformation(strPriceNot0);
                    dgvPriceDetail.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = _objBeforeEdit;
                }
            }
        }

        private void dgvPriceDetail_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            _objBeforeEdit = dgvPriceDetail.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
        }

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objNew);
        }
        private void dgvPriceDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1)
            {
                if (this.dgvPriceDetail.Columns[e.ColumnIndex].Name == this.ALLOW_FIXED_AMOUNT.Name)
                {
                    this.dgvPriceDetail[this.FIXED_AMOUNT.Name, e.RowIndex].ReadOnly = (bool)(this.dgvPriceDetail[this.ALLOW_FIXED_AMOUNT.Name, e.RowIndex].Value ?? false);
                    if (this.dgvPriceDetail[this.FIXED_AMOUNT.Name, e.RowIndex].ReadOnly)
                    {
                        this.dgvPriceDetail[this.FIXED_AMOUNT.Name, e.RowIndex].Value = 0;
                    }
                }
            }
        }

        private void txtDecimalOnly(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void picHelp_MouseEnter(object sender, EventArgs e)
        {
            ToolTip tp = new ToolTip
            {
                AutoPopDelay = 15000,
                ShowAlways = true,
                OwnerDraw = true,
            };
            tp.SetToolTip(picHelp, Resources.MS_FOR_CAPACITY_CHARGE);
            tp.Draw += new DrawToolTipEventHandler(toolTip_Draw);
            tp.Popup += new PopupEventHandler(toolTip_Popup);
        }
        #endregion Event
    }
}
