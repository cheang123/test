﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogReportSechdule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogReportSechdule));
            this.btnOK = new SoftTech.Component.ExButton();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.tabSchedule = new System.Windows.Forms.TabControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.nudRecurs = new System.Windows.Forms.NumericUpDown();
            this.grbMonthly = new System.Windows.Forms.GroupBox();
            this.nudDay = new System.Windows.Forms.NumericUpDown();
            this.chkEndofMonth = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblEveryday = new System.Windows.Forms.Label();
            this.chkResend = new System.Windows.Forms.CheckBox();
            this.lblRecurs = new System.Windows.Forms.Label();
            this.grbWeekly = new System.Windows.Forms.GroupBox();
            this.chkMonday = new System.Windows.Forms.CheckBox();
            this.chkSunday = new System.Windows.Forms.CheckBox();
            this.chkTuesday = new System.Windows.Forms.CheckBox();
            this.chkSatursday = new System.Windows.Forms.CheckBox();
            this.chkWednesday = new System.Windows.Forms.CheckBox();
            this.chkFriday = new System.Windows.Forms.CheckBox();
            this.chkThursday = new System.Windows.Forms.CheckBox();
            this.cboPeriod = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPeriod = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSechuleName = new System.Windows.Forms.TextBox();
            this.lblSechuleName = new System.Windows.Forms.Label();
            this.grbDaily = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nudEveryHours = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.radIsUnlimited = new System.Windows.Forms.RadioButton();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.radEndDate = new System.Windows.Forms.RadioButton();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpStartTime = new System.Windows.Forms.DateTimePicker();
            this.lblEveryHours = new System.Windows.Forms.Label();
            this.radEveryHours = new System.Windows.Forms.RadioButton();
            this.radDailyRecurs = new System.Windows.Forms.RadioButton();
            this.tabSentTo = new System.Windows.Forms.TabPage();
            this.dgvRole = new System.Windows.Forms.DataGridView();
            this.SELECTED = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ROLE_SCHEDULE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_ACTIVE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabReport = new System.Windows.Forms.TabPage();
            this.dgvReport = new System.Windows.Forms.DataGridView();
            this.SELECTED_REPORT = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.REPORT_SEND_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REPORT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REPORT_LOCAL_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SEND_YESTERDAY = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SEND_IS_RUNBILL = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.IS_ENGLISH = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnChangelog = new SoftTech.Component.ExButton();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabSchedule.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRecurs)).BeginInit();
            this.grbMonthly.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDay)).BeginInit();
            this.grbWeekly.SuspendLayout();
            this.grbDaily.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEveryHours)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabSentTo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRole)).BeginInit();
            this.tabReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            resources.ApplyResources(this.content, "content");
            this.content.Controls.Add(this.tabSchedule);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.Add(this.btnChangelog);
            this.content.Controls.SetChildIndex(this.btnChangelog, 0);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.tabSchedule, 0);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabSchedule
            // 
            resources.ApplyResources(this.tabSchedule, "tabSchedule");
            this.tabSchedule.Controls.Add(this.tabGeneral);
            this.tabSchedule.Controls.Add(this.tabSentTo);
            this.tabSchedule.Controls.Add(this.tabReport);
            this.tabSchedule.Name = "tabSchedule";
            this.tabSchedule.SelectedIndex = 0;
            // 
            // tabGeneral
            // 
            resources.ApplyResources(this.tabGeneral, "tabGeneral");
            this.tabGeneral.Controls.Add(this.nudRecurs);
            this.tabGeneral.Controls.Add(this.grbMonthly);
            this.tabGeneral.Controls.Add(this.chkResend);
            this.tabGeneral.Controls.Add(this.lblRecurs);
            this.tabGeneral.Controls.Add(this.grbWeekly);
            this.tabGeneral.Controls.Add(this.cboPeriod);
            this.tabGeneral.Controls.Add(this.label3);
            this.tabGeneral.Controls.Add(this.lblPeriod);
            this.tabGeneral.Controls.Add(this.label2);
            this.tabGeneral.Controls.Add(this.txtSechuleName);
            this.tabGeneral.Controls.Add(this.lblSechuleName);
            this.tabGeneral.Controls.Add(this.grbDaily);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.UseVisualStyleBackColor = true;
            // 
            // nudRecurs
            // 
            resources.ApplyResources(this.nudRecurs, "nudRecurs");
            this.nudRecurs.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudRecurs.Name = "nudRecurs";
            this.nudRecurs.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudRecurs.Enter += new System.EventHandler(this.English_Enter);
            // 
            // grbMonthly
            // 
            resources.ApplyResources(this.grbMonthly, "grbMonthly");
            this.grbMonthly.Controls.Add(this.nudDay);
            this.grbMonthly.Controls.Add(this.chkEndofMonth);
            this.grbMonthly.Controls.Add(this.label6);
            this.grbMonthly.Controls.Add(this.lblEveryday);
            this.grbMonthly.Name = "grbMonthly";
            this.grbMonthly.TabStop = false;
            // 
            // nudDay
            // 
            resources.ApplyResources(this.nudDay, "nudDay");
            this.nudDay.Maximum = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.nudDay.Name = "nudDay";
            this.nudDay.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDay.Enter += new System.EventHandler(this.English_Enter);
            // 
            // chkEndofMonth
            // 
            resources.ApplyResources(this.chkEndofMonth, "chkEndofMonth");
            this.chkEndofMonth.Name = "chkEndofMonth";
            this.chkEndofMonth.UseVisualStyleBackColor = true;
            this.chkEndofMonth.CheckedChanged += new System.EventHandler(this.chkEndofMonth_CheckedChanged);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // lblEveryday
            // 
            resources.ApplyResources(this.lblEveryday, "lblEveryday");
            this.lblEveryday.Name = "lblEveryday";
            // 
            // chkResend
            // 
            resources.ApplyResources(this.chkResend, "chkResend");
            this.chkResend.Name = "chkResend";
            this.chkResend.UseVisualStyleBackColor = true;
            // 
            // lblRecurs
            // 
            resources.ApplyResources(this.lblRecurs, "lblRecurs");
            this.lblRecurs.Name = "lblRecurs";
            // 
            // grbWeekly
            // 
            resources.ApplyResources(this.grbWeekly, "grbWeekly");
            this.grbWeekly.Controls.Add(this.chkMonday);
            this.grbWeekly.Controls.Add(this.chkSunday);
            this.grbWeekly.Controls.Add(this.chkTuesday);
            this.grbWeekly.Controls.Add(this.chkSatursday);
            this.grbWeekly.Controls.Add(this.chkWednesday);
            this.grbWeekly.Controls.Add(this.chkFriday);
            this.grbWeekly.Controls.Add(this.chkThursday);
            this.grbWeekly.Name = "grbWeekly";
            this.grbWeekly.TabStop = false;
            // 
            // chkMonday
            // 
            resources.ApplyResources(this.chkMonday, "chkMonday");
            this.chkMonday.Name = "chkMonday";
            this.chkMonday.UseVisualStyleBackColor = true;
            // 
            // chkSunday
            // 
            resources.ApplyResources(this.chkSunday, "chkSunday");
            this.chkSunday.Name = "chkSunday";
            this.chkSunday.UseVisualStyleBackColor = true;
            // 
            // chkTuesday
            // 
            resources.ApplyResources(this.chkTuesday, "chkTuesday");
            this.chkTuesday.Name = "chkTuesday";
            this.chkTuesday.UseVisualStyleBackColor = true;
            // 
            // chkSatursday
            // 
            resources.ApplyResources(this.chkSatursday, "chkSatursday");
            this.chkSatursday.Name = "chkSatursday";
            this.chkSatursday.UseVisualStyleBackColor = true;
            // 
            // chkWednesday
            // 
            resources.ApplyResources(this.chkWednesday, "chkWednesday");
            this.chkWednesday.Name = "chkWednesday";
            this.chkWednesday.UseVisualStyleBackColor = true;
            // 
            // chkFriday
            // 
            resources.ApplyResources(this.chkFriday, "chkFriday");
            this.chkFriday.Name = "chkFriday";
            this.chkFriday.UseVisualStyleBackColor = true;
            // 
            // chkThursday
            // 
            resources.ApplyResources(this.chkThursday, "chkThursday");
            this.chkThursday.Name = "chkThursday";
            this.chkThursday.UseVisualStyleBackColor = true;
            // 
            // cboPeriod
            // 
            resources.ApplyResources(this.cboPeriod, "cboPeriod");
            this.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPeriod.FormattingEnabled = true;
            this.cboPeriod.Name = "cboPeriod";
            this.cboPeriod.SelectedIndexChanged += new System.EventHandler(this.cboPeriod_SelectedIndexChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // lblPeriod
            // 
            resources.ApplyResources(this.lblPeriod, "lblPeriod");
            this.lblPeriod.Name = "lblPeriod";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // txtSechuleName
            // 
            resources.ApplyResources(this.txtSechuleName, "txtSechuleName");
            this.txtSechuleName.Name = "txtSechuleName";
            this.txtSechuleName.Enter += new System.EventHandler(this.Khmer_Enter);
            // 
            // lblSechuleName
            // 
            resources.ApplyResources(this.lblSechuleName, "lblSechuleName");
            this.lblSechuleName.Name = "lblSechuleName";
            // 
            // grbDaily
            // 
            resources.ApplyResources(this.grbDaily, "grbDaily");
            this.grbDaily.Controls.Add(this.label4);
            this.grbDaily.Controls.Add(this.label1);
            this.grbDaily.Controls.Add(this.nudEveryHours);
            this.grbDaily.Controls.Add(this.label13);
            this.grbDaily.Controls.Add(this.panel3);
            this.grbDaily.Controls.Add(this.lblStartDate);
            this.grbDaily.Controls.Add(this.dtpStartDate);
            this.grbDaily.Controls.Add(this.label10);
            this.grbDaily.Controls.Add(this.dtpStartTime);
            this.grbDaily.Controls.Add(this.lblEveryHours);
            this.grbDaily.Controls.Add(this.radEveryHours);
            this.grbDaily.Controls.Add(this.radDailyRecurs);
            this.grbDaily.Name = "grbDaily";
            this.grbDaily.TabStop = false;
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // nudEveryHours
            // 
            resources.ApplyResources(this.nudEveryHours, "nudEveryHours");
            this.nudEveryHours.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudEveryHours.Name = "nudEveryHours";
            this.nudEveryHours.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // panel3
            // 
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.radIsUnlimited);
            this.panel3.Controls.Add(this.dtpEndDate);
            this.panel3.Controls.Add(this.radEndDate);
            this.panel3.Name = "panel3";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // radIsUnlimited
            // 
            resources.ApplyResources(this.radIsUnlimited, "radIsUnlimited");
            this.radIsUnlimited.Checked = true;
            this.radIsUnlimited.Name = "radIsUnlimited";
            this.radIsUnlimited.TabStop = true;
            this.radIsUnlimited.UseVisualStyleBackColor = true;
            // 
            // dtpEndDate
            // 
            resources.ApplyResources(this.dtpEndDate, "dtpEndDate");
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Name = "dtpEndDate";
            // 
            // radEndDate
            // 
            resources.ApplyResources(this.radEndDate, "radEndDate");
            this.radEndDate.Name = "radEndDate";
            this.radEndDate.UseVisualStyleBackColor = true;
            this.radEndDate.CheckedChanged += new System.EventHandler(this.radEndDate_CheckedChanged);
            // 
            // lblStartDate
            // 
            resources.ApplyResources(this.lblStartDate, "lblStartDate");
            this.lblStartDate.Name = "lblStartDate";
            // 
            // dtpStartDate
            // 
            resources.ApplyResources(this.dtpStartDate, "dtpStartDate");
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Name = "dtpStartDate";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // dtpStartTime
            // 
            resources.ApplyResources(this.dtpStartTime, "dtpStartTime");
            this.dtpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartTime.Name = "dtpStartTime";
            // 
            // lblEveryHours
            // 
            resources.ApplyResources(this.lblEveryHours, "lblEveryHours");
            this.lblEveryHours.Name = "lblEveryHours";
            // 
            // radEveryHours
            // 
            resources.ApplyResources(this.radEveryHours, "radEveryHours");
            this.radEveryHours.Name = "radEveryHours";
            this.radEveryHours.UseVisualStyleBackColor = true;
            this.radEveryHours.CheckedChanged += new System.EventHandler(this.radEveryHours_CheckedChanged);
            // 
            // radDailyRecurs
            // 
            resources.ApplyResources(this.radDailyRecurs, "radDailyRecurs");
            this.radDailyRecurs.Checked = true;
            this.radDailyRecurs.Name = "radDailyRecurs";
            this.radDailyRecurs.TabStop = true;
            this.radDailyRecurs.UseVisualStyleBackColor = true;
            // 
            // tabSentTo
            // 
            resources.ApplyResources(this.tabSentTo, "tabSentTo");
            this.tabSentTo.Controls.Add(this.dgvRole);
            this.tabSentTo.Name = "tabSentTo";
            this.tabSentTo.UseVisualStyleBackColor = true;
            // 
            // dgvRole
            // 
            resources.ApplyResources(this.dgvRole, "dgvRole");
            this.dgvRole.AllowUserToAddRows = false;
            this.dgvRole.AllowUserToDeleteRows = false;
            this.dgvRole.AllowUserToResizeRows = false;
            this.dgvRole.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRole.BackgroundColor = System.Drawing.Color.White;
            this.dgvRole.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvRole.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvRole.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRole.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SELECTED,
            this.ROLE_SCHEDULE_ID,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewCheckBoxColumn1,
            this.IS_ACTIVE});
            this.dgvRole.EnableHeadersVisualStyles = false;
            this.dgvRole.MultiSelect = false;
            this.dgvRole.Name = "dgvRole";
            this.dgvRole.ReadOnly = true;
            this.dgvRole.RowHeadersVisible = false;
            this.dgvRole.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRole.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRole_CellClick);
            // 
            // SELECTED
            // 
            this.SELECTED.DataPropertyName = "SELECTED";
            this.SELECTED.FillWeight = 20F;
            resources.ApplyResources(this.SELECTED, "SELECTED");
            this.SELECTED.Name = "SELECTED";
            this.SELECTED.ReadOnly = true;
            // 
            // ROLE_SCHEDULE_ID
            // 
            this.ROLE_SCHEDULE_ID.DataPropertyName = "ROLE_SCHEDULE_ID";
            resources.ApplyResources(this.ROLE_SCHEDULE_ID, "ROLE_SCHEDULE_ID");
            this.ROLE_SCHEDULE_ID.Name = "ROLE_SCHEDULE_ID";
            this.ROLE_SCHEDULE_ID.ReadOnly = true;
            this.ROLE_SCHEDULE_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ROLE_ID";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ROLE_NAME";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "DESCRIPTION";
            this.dataGridViewCheckBoxColumn1.FillWeight = 30F;
            resources.ApplyResources(this.dataGridViewCheckBoxColumn1, "dataGridViewCheckBoxColumn1");
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ReadOnly = true;
            this.dataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // IS_ACTIVE
            // 
            this.IS_ACTIVE.DataPropertyName = "IS_ACTIVE";
            resources.ApplyResources(this.IS_ACTIVE, "IS_ACTIVE");
            this.IS_ACTIVE.Name = "IS_ACTIVE";
            this.IS_ACTIVE.ReadOnly = true;
            this.IS_ACTIVE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tabReport
            // 
            resources.ApplyResources(this.tabReport, "tabReport");
            this.tabReport.Controls.Add(this.dgvReport);
            this.tabReport.Name = "tabReport";
            this.tabReport.UseVisualStyleBackColor = true;
            // 
            // dgvReport
            // 
            resources.ApplyResources(this.dgvReport, "dgvReport");
            this.dgvReport.AllowUserToAddRows = false;
            this.dgvReport.AllowUserToDeleteRows = false;
            this.dgvReport.AllowUserToResizeRows = false;
            this.dgvReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReport.BackgroundColor = System.Drawing.Color.White;
            this.dgvReport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SELECTED_REPORT,
            this.REPORT_SEND_ID,
            this.REPORT_ID,
            this.REPORT_LOCAL_NAME,
            this.SEND_YESTERDAY,
            this.SEND_IS_RUNBILL,
            this.IS_ENGLISH});
            this.dgvReport.EnableHeadersVisualStyles = false;
            this.dgvReport.MultiSelect = false;
            this.dgvReport.Name = "dgvReport";
            this.dgvReport.ReadOnly = true;
            this.dgvReport.RowHeadersVisible = false;
            this.dgvReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReport.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReport_CellClick);
            // 
            // SELECTED_REPORT
            // 
            this.SELECTED_REPORT.DataPropertyName = "SELECTED_REPORT";
            this.SELECTED_REPORT.FillWeight = 20F;
            resources.ApplyResources(this.SELECTED_REPORT, "SELECTED_REPORT");
            this.SELECTED_REPORT.Name = "SELECTED_REPORT";
            this.SELECTED_REPORT.ReadOnly = true;
            this.SELECTED_REPORT.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SELECTED_REPORT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // REPORT_SEND_ID
            // 
            this.REPORT_SEND_ID.DataPropertyName = "REPORT_SEND_ID";
            resources.ApplyResources(this.REPORT_SEND_ID, "REPORT_SEND_ID");
            this.REPORT_SEND_ID.Name = "REPORT_SEND_ID";
            this.REPORT_SEND_ID.ReadOnly = true;
            this.REPORT_SEND_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // REPORT_ID
            // 
            this.REPORT_ID.DataPropertyName = "REPORT_ID";
            resources.ApplyResources(this.REPORT_ID, "REPORT_ID");
            this.REPORT_ID.Name = "REPORT_ID";
            this.REPORT_ID.ReadOnly = true;
            this.REPORT_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // REPORT_LOCAL_NAME
            // 
            this.REPORT_LOCAL_NAME.DataPropertyName = "REPORT_LOCAL_NAME";
            resources.ApplyResources(this.REPORT_LOCAL_NAME, "REPORT_LOCAL_NAME");
            this.REPORT_LOCAL_NAME.Name = "REPORT_LOCAL_NAME";
            this.REPORT_LOCAL_NAME.ReadOnly = true;
            this.REPORT_LOCAL_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // SEND_YESTERDAY
            // 
            this.SEND_YESTERDAY.DataPropertyName = "SEND_YESTERDAY";
            this.SEND_YESTERDAY.FillWeight = 38F;
            resources.ApplyResources(this.SEND_YESTERDAY, "SEND_YESTERDAY");
            this.SEND_YESTERDAY.Name = "SEND_YESTERDAY";
            this.SEND_YESTERDAY.ReadOnly = true;
            // 
            // SEND_IS_RUNBILL
            // 
            this.SEND_IS_RUNBILL.DataPropertyName = "SEND_IS_RUNBILL";
            this.SEND_IS_RUNBILL.FillWeight = 38F;
            resources.ApplyResources(this.SEND_IS_RUNBILL, "SEND_IS_RUNBILL");
            this.SEND_IS_RUNBILL.Name = "SEND_IS_RUNBILL";
            this.SEND_IS_RUNBILL.ReadOnly = true;
            // 
            // IS_ENGLISH
            // 
            this.IS_ENGLISH.DataPropertyName = "IS_ENGLISH";
            this.IS_ENGLISH.FillWeight = 35F;
            resources.ApplyResources(this.IS_ENGLISH, "IS_ENGLISH");
            this.IS_ENGLISH.Name = "IS_ENGLISH";
            this.IS_ENGLISH.ReadOnly = true;
            this.IS_ENGLISH.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IS_ENGLISH.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // btnChangelog
            // 
            resources.ApplyResources(this.btnChangelog, "btnChangelog");
            this.btnChangelog.Name = "btnChangelog";
            this.btnChangelog.UseVisualStyleBackColor = true;
            this.btnChangelog.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // DialogReportSechdule
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogReportSechdule";
            this.Load += new System.EventHandler(this.DialogReportSechdule_Load);
            this.content.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabSchedule.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.tabGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRecurs)).EndInit();
            this.grbMonthly.ResumeLayout(false);
            this.grbMonthly.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDay)).EndInit();
            this.grbWeekly.ResumeLayout(false);
            this.grbWeekly.PerformLayout();
            this.grbDaily.ResumeLayout(false);
            this.grbDaily.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEveryHours)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabSentTo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRole)).EndInit();
            this.tabReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ExButton btnCLOSE;
        private ExButton btnOK;
        private TabControl tabSchedule;
        private TabPage tabGeneral;
        private Label lblSechuleName;
        private TabPage tabSentTo;
        private ComboBox cboPeriod;
        private Label label3;
        private Label lblPeriod;
        private Label label2;
        private TextBox txtSechuleName;
        private Label lblRecurs;
        private Label label6;
        private Label lblEveryday;
        private CheckBox chkSunday;
        private CheckBox chkSatursday;
        private CheckBox chkFriday;
        private CheckBox chkThursday;
        private CheckBox chkWednesday;
        private CheckBox chkTuesday;
        private CheckBox chkMonday;
        private GroupBox grbWeekly;
        private CheckBox chkEndofMonth;
        private NumericUpDown nudDay;
        private NumericUpDown nudRecurs;
        private TabPage tabReport;
        private DataGridView dgvReport;
        private DataGridView dgvRole;
        private ExButton btnChangelog;
        private CheckBox chkResend;
        private GroupBox grbMonthly;
        private GroupBox grbDaily;
        private Label label4;
        private Label label1;
        private NumericUpDown nudEveryHours;
        private Label label13;
        private Panel panel3;
        private Label label14;
        private RadioButton radIsUnlimited;
        private DateTimePicker dtpEndDate;
        private RadioButton radEndDate;
        private Label lblStartDate;
        private DateTimePicker dtpStartDate;
        private Label label10;
        private DateTimePicker dtpStartTime;
        private Label lblEveryHours;
        private RadioButton radEveryHours;
        private RadioButton radDailyRecurs;
        private DataGridViewCheckBoxColumn SELECTED;
        private DataGridViewTextBoxColumn ROLE_SCHEDULE_ID;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewCheckBoxColumn1;
        private DataGridViewTextBoxColumn IS_ACTIVE;
        private DataGridViewCheckBoxColumn SELECTED_REPORT;
        private DataGridViewTextBoxColumn REPORT_SEND_ID;
        private DataGridViewTextBoxColumn REPORT_ID;
        private DataGridViewTextBoxColumn REPORT_LOCAL_NAME;
        private DataGridViewCheckBoxColumn SEND_YESTERDAY;
        private DataGridViewCheckBoxColumn SEND_IS_RUNBILL;
        private DataGridViewCheckBoxColumn IS_ENGLISH;
    }
}
