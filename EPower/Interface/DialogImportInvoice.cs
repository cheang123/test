﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;

namespace EPower.Interface
{
    public partial class DialogImportInvoice : ExDialog
    {
        string newFile;
        public DialogImportInvoice()
        {
            InitializeComponent();
        }

        private void _btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "rpt files (*.rpt)|*.rpt";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtFILE_NAME.Text = ofd.FileName;
                newFile = ofd.SafeFileName;
            }
        }

        private void btnCLOSE_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool inValid()
        {
            bool val = false;
            if (String.IsNullOrEmpty(txtFILE_NAME.Text))
            {
                _btnBrowse.SetValidation(string.Format(Resources.REQUIRED, lblFILE_LOCATION.Text));
                val = true; ;
            }
            return val;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DateTime date = DateTime.Now;
            string backupFile = Application.StartupPath + "\\Report\\CrystalReport\\BackupInvoice\\" + Settings.Default.REPORT_INVOICE;
            string backupFileNew = Application.StartupPath + "\\Report\\CrystalReport\\BackupInvoice\\" + newFile;
            string pathBackup = Application.StartupPath + "\\Report\\CrystalReport\\BackupInvoice\\";
            string defaultCustomized = Application.StartupPath + "\\Report\\CrystalReport\\Customized\\";
            string PathCustomizeds = Application.StartupPath + "\\Report\\CrystalReport\\Customized\\" + newFile;
  
            string file = backupFile.Replace(".rpt", date.ToString("yyyyMMddHHmmss") + ".rpt");
            string fileNew = backupFileNew.Replace(".rpt", date.ToString("yyyyMMddHHmmss") + ".rpt");

            if (inValid())
            {
                return;
            }

            //!Exists path customized
            if (!Directory.Exists(defaultCustomized))
            {
                Directory.CreateDirectory(defaultCustomized);
            }


            //!Exists path backup
            if (!Directory.Exists(pathBackup))
            {
                Directory.CreateDirectory(pathBackup);
            }
            
            //if not check add new invoice and Exists file
            if(chkADD_NEW_INVOICE.Checked && File.Exists(PathCustomizeds))
            {
                MsgBox.ShowWarning(Resources.MS_INVOICE_IS_ALREADY, Resources.WARNING);
                return;
            }

            //if add new invoice but not check 
            if(!chkADD_NEW_INVOICE.Checked && !File.Exists(PathCustomizeds))
            {
                MsgBox.ShowWarning(Resources.MS_INVOICE_NOT_ALREADY, Resources.WARNING);
                return;
            }    

            //if not check or File Exists
            if(chkADD_NEW_INVOICE.Checked ||File.Exists(PathCustomizeds))
            {
                if (!File.Exists(PathCustomizeds))
                {
                    File.Copy(txtFILE_NAME.Text, PathCustomizeds, true);
                    var uTILITY = DBDataContext.Db.TBL_UTILITies.FirstOrDefault(x => x.UTILITY_ID == 40);
                    TBL_UTILITY UTILITY = uTILITY;
                    UTILITY.UTILITY_VALUE = UTILITY.UTILITY_VALUE + ";" + newFile;
                    DBDataContext.Db.SubmitChanges();
                }
                else
                {
                    File.Copy(PathCustomizeds, fileNew, true);
                    File.Copy(txtFILE_NAME.Text, PathCustomizeds, true);
                }
            }
            this.DialogResult = DialogResult.OK;
        }
    }
}
