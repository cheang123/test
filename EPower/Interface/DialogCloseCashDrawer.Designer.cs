﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCloseCashDrawer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCloseCashDrawer));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblCASH_DRAWER = new System.Windows.Forms.Label();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.lblOPEN_CASH_DRAWER_DATE = new System.Windows.Forms.Label();
            this.txtOpenDate = new System.Windows.Forms.DateTimePicker();
            this.lblPRINTER = new System.Windows.Forms.Label();
            this.txtCloseDate = new System.Windows.Forms.DateTimePicker();
            this.txtCashDrawer = new System.Windows.Forms.TextBox();
            this.btnVIEW = new SoftTech.Component.ExButton();
            this.cboPrinterCashDrawer = new System.Windows.Forms.ComboBox();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.CURRENCY_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CASH_FLOAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOTAL_CASH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACTUAL_CASH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SURPLUS_CASH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblAMOUNT = new System.Windows.Forms.Label();
            this.lblNOTE = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblREPORT = new System.Windows.Forms.Label();
            this.cboReport = new System.Windows.Forms.ComboBox();
            this.lblCLOSE_CASH_DRAWER_DATE = new System.Windows.Forms.Label();
            this.pnlCountMoney = new System.Windows.Forms.Panel();
            this.chkCOUNT_MONEY = new System.Windows.Forms.CheckBox();
            this.dgvCOUNT_MONEY = new System.Windows.Forms.DataGridView();
            this.COUNT_CASH_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_ID_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CASH_VALUE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUANTITY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING0 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONTROL_ = new System.Windows.Forms.DataGridViewImageColumn();
            this.IS_HEADER_ = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.pnlCountMoney.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCOUNT_MONEY)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.Controls.Add(this.pnlCountMoney);
            this.content.Controls.Add(this.lblCLOSE_CASH_DRAWER_DATE);
            this.content.Controls.Add(this.lblAMOUNT);
            this.content.Controls.Add(this.dgv);
            this.content.Controls.Add(this.cboReport);
            this.content.Controls.Add(this.cboPrinterCashDrawer);
            this.content.Controls.Add(this.btnVIEW);
            this.content.Controls.Add(this.txtCashDrawer);
            this.content.Controls.Add(this.lblREPORT);
            this.content.Controls.Add(this.txtCloseDate);
            this.content.Controls.Add(this.lblPRINTER);
            this.content.Controls.Add(this.txtOpenDate);
            this.content.Controls.Add(this.lblOPEN_CASH_DRAWER_DATE);
            this.content.Controls.Add(this.lblCASH_DRAWER);
            resources.ApplyResources(this.content, "content");
            this.content.Controls.SetChildIndex(this.lblCASH_DRAWER, 0);
            this.content.Controls.SetChildIndex(this.lblOPEN_CASH_DRAWER_DATE, 0);
            this.content.Controls.SetChildIndex(this.txtOpenDate, 0);
            this.content.Controls.SetChildIndex(this.lblPRINTER, 0);
            this.content.Controls.SetChildIndex(this.txtCloseDate, 0);
            this.content.Controls.SetChildIndex(this.lblREPORT, 0);
            this.content.Controls.SetChildIndex(this.txtCashDrawer, 0);
            this.content.Controls.SetChildIndex(this.btnVIEW, 0);
            this.content.Controls.SetChildIndex(this.cboPrinterCashDrawer, 0);
            this.content.Controls.SetChildIndex(this.cboReport, 0);
            this.content.Controls.SetChildIndex(this.dgv, 0);
            this.content.Controls.SetChildIndex(this.lblAMOUNT, 0);
            this.content.Controls.SetChildIndex(this.lblCLOSE_CASH_DRAWER_DATE, 0);
            this.content.Controls.SetChildIndex(this.pnlCountMoney, 0);
            // 
            // lblCASH_DRAWER
            // 
            resources.ApplyResources(this.lblCASH_DRAWER, "lblCASH_DRAWER");
            this.lblCASH_DRAWER.Name = "lblCASH_DRAWER";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblOPEN_CASH_DRAWER_DATE
            // 
            resources.ApplyResources(this.lblOPEN_CASH_DRAWER_DATE, "lblOPEN_CASH_DRAWER_DATE");
            this.lblOPEN_CASH_DRAWER_DATE.Name = "lblOPEN_CASH_DRAWER_DATE";
            // 
            // txtOpenDate
            // 
            resources.ApplyResources(this.txtOpenDate, "txtOpenDate");
            this.txtOpenDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtOpenDate.Name = "txtOpenDate";
            this.txtOpenDate.ShowUpDown = true;
            this.txtOpenDate.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // lblPRINTER
            // 
            resources.ApplyResources(this.lblPRINTER, "lblPRINTER");
            this.lblPRINTER.Name = "lblPRINTER";
            // 
            // txtCloseDate
            // 
            resources.ApplyResources(this.txtCloseDate, "txtCloseDate");
            this.txtCloseDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtCloseDate.Name = "txtCloseDate";
            this.txtCloseDate.ShowUpDown = true;
            this.txtCloseDate.Enter += new System.EventHandler(this.InputEnglish);
            // 
            // txtCashDrawer
            // 
            resources.ApplyResources(this.txtCashDrawer, "txtCashDrawer");
            this.txtCashDrawer.Name = "txtCashDrawer";
            // 
            // btnVIEW
            // 
            resources.ApplyResources(this.btnVIEW, "btnVIEW");
            this.btnVIEW.Name = "btnVIEW";
            this.btnVIEW.UseVisualStyleBackColor = true;
            this.btnVIEW.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // cboPrinterCashDrawer
            // 
            this.cboPrinterCashDrawer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrinterCashDrawer.FormattingEnabled = true;
            resources.ApplyResources(this.cboPrinterCashDrawer, "cboPrinterCashDrawer");
            this.cboPrinterCashDrawer.Name = "cboPrinterCashDrawer";
            this.cboPrinterCashDrawer.SelectedIndexChanged += new System.EventHandler(this.cboPrinterCashDrawer_SelectedIndexChanged);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CURRENCY_ID,
            this.CASH_FLOAT,
            this.TOTAL_CASH,
            this.ACTUAL_CASH,
            this.SURPLUS_CASH,
            this.CURRENCY_SING_});
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle15;
            this.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.ShowCellErrors = false;
            this.dgv.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellValidated);
            this.dgv.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgv_DataError);
            this.dgv.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_EditingControlShowing);
            // 
            // CURRENCY_ID
            // 
            this.CURRENCY_ID.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.CURRENCY_ID, "CURRENCY_ID");
            this.CURRENCY_ID.Name = "CURRENCY_ID";
            this.CURRENCY_ID.ReadOnly = true;
            // 
            // CASH_FLOAT
            // 
            this.CASH_FLOAT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CASH_FLOAT.DataPropertyName = "CASH_FLOAT";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle10.Format = "#,###.##";
            this.CASH_FLOAT.DefaultCellStyle = dataGridViewCellStyle10;
            resources.ApplyResources(this.CASH_FLOAT, "CASH_FLOAT");
            this.CASH_FLOAT.Name = "CASH_FLOAT";
            this.CASH_FLOAT.ReadOnly = true;
            // 
            // TOTAL_CASH
            // 
            this.TOTAL_CASH.DataPropertyName = "TOTAL_CASH";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.Format = "#,###.##";
            this.TOTAL_CASH.DefaultCellStyle = dataGridViewCellStyle11;
            resources.ApplyResources(this.TOTAL_CASH, "TOTAL_CASH");
            this.TOTAL_CASH.Name = "TOTAL_CASH";
            this.TOTAL_CASH.ReadOnly = true;
            // 
            // ACTUAL_CASH
            // 
            this.ACTUAL_CASH.DataPropertyName = "ACTUAL_CASH";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "#,###.##";
            this.ACTUAL_CASH.DefaultCellStyle = dataGridViewCellStyle12;
            resources.ApplyResources(this.ACTUAL_CASH, "ACTUAL_CASH");
            this.ACTUAL_CASH.Name = "ACTUAL_CASH";
            this.ACTUAL_CASH.ReadOnly = true;
            // 
            // SURPLUS_CASH
            // 
            this.SURPLUS_CASH.DataPropertyName = "SURPLUS";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle13.Format = "#,###.##";
            this.SURPLUS_CASH.DefaultCellStyle = dataGridViewCellStyle13;
            resources.ApplyResources(this.SURPLUS_CASH, "SURPLUS_CASH");
            this.SURPLUS_CASH.Name = "SURPLUS_CASH";
            this.SURPLUS_CASH.ReadOnly = true;
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CURRENCY_SING_.DefaultCellStyle = dataGridViewCellStyle14;
            this.CURRENCY_SING_.FillWeight = 35F;
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            this.CURRENCY_SING_.ReadOnly = true;
            // 
            // lblAMOUNT
            // 
            resources.ApplyResources(this.lblAMOUNT, "lblAMOUNT");
            this.lblAMOUNT.Name = "lblAMOUNT";
            // 
            // lblNOTE
            // 
            resources.ApplyResources(this.lblNOTE, "lblNOTE");
            this.lblNOTE.Name = "lblNOTE";
            // 
            // txtNote
            // 
            resources.ApplyResources(this.txtNote, "txtNote");
            this.txtNote.Name = "txtNote";
            // 
            // lblREPORT
            // 
            resources.ApplyResources(this.lblREPORT, "lblREPORT");
            this.lblREPORT.Name = "lblREPORT";
            // 
            // cboReport
            // 
            this.cboReport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboReport.FormattingEnabled = true;
            this.cboReport.Items.AddRange(new object[] {
            resources.GetString("cboReport.Items"),
            resources.GetString("cboReport.Items1"),
            resources.GetString("cboReport.Items2")});
            resources.ApplyResources(this.cboReport, "cboReport");
            this.cboReport.Name = "cboReport";
            this.cboReport.SelectedIndexChanged += new System.EventHandler(this.cboPrinterCashDrawer_SelectedIndexChanged);
            // 
            // lblCLOSE_CASH_DRAWER_DATE
            // 
            resources.ApplyResources(this.lblCLOSE_CASH_DRAWER_DATE, "lblCLOSE_CASH_DRAWER_DATE");
            this.lblCLOSE_CASH_DRAWER_DATE.Name = "lblCLOSE_CASH_DRAWER_DATE";
            // 
            // pnlCountMoney
            // 
            this.pnlCountMoney.BackColor = System.Drawing.Color.Transparent;
            this.pnlCountMoney.Controls.Add(this.chkCOUNT_MONEY);
            this.pnlCountMoney.Controls.Add(this.dgvCOUNT_MONEY);
            this.pnlCountMoney.Controls.Add(this.txtNote);
            this.pnlCountMoney.Controls.Add(this.lblNOTE);
            this.pnlCountMoney.Controls.Add(this.btnOK);
            this.pnlCountMoney.Controls.Add(this.btnCLOSE);
            resources.ApplyResources(this.pnlCountMoney, "pnlCountMoney");
            this.pnlCountMoney.Name = "pnlCountMoney";
            // 
            // chkCOUNT_MONEY
            // 
            resources.ApplyResources(this.chkCOUNT_MONEY, "chkCOUNT_MONEY");
            this.chkCOUNT_MONEY.Checked = true;
            this.chkCOUNT_MONEY.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCOUNT_MONEY.Name = "chkCOUNT_MONEY";
            this.chkCOUNT_MONEY.UseVisualStyleBackColor = true;
            this.chkCOUNT_MONEY.CheckedChanged += new System.EventHandler(this.chkCOUNT_MONEY_CheckedChanged);
            // 
            // dgvCOUNT_MONEY
            // 
            this.dgvCOUNT_MONEY.AllowUserToAddRows = false;
            this.dgvCOUNT_MONEY.AllowUserToDeleteRows = false;
            this.dgvCOUNT_MONEY.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvCOUNT_MONEY.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCOUNT_MONEY.BackgroundColor = System.Drawing.Color.White;
            this.dgvCOUNT_MONEY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvCOUNT_MONEY.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCOUNT_MONEY.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCOUNT_MONEY.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCOUNT_MONEY.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.COUNT_CASH_ID,
            this.CURRENCY_ID_,
            this.CASH_VALUE,
            this.QUANTITY,
            this.AMOUNT,
            this.CURRENCY_SING0,
            this.CONTROL_,
            this.IS_HEADER_});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCOUNT_MONEY.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvCOUNT_MONEY.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvCOUNT_MONEY.EnableHeadersVisualStyles = false;
            resources.ApplyResources(this.dgvCOUNT_MONEY, "dgvCOUNT_MONEY");
            this.dgvCOUNT_MONEY.MultiSelect = false;
            this.dgvCOUNT_MONEY.Name = "dgvCOUNT_MONEY";
            this.dgvCOUNT_MONEY.RowHeadersVisible = false;
            this.dgvCOUNT_MONEY.RowTemplate.Height = 25;
            this.dgvCOUNT_MONEY.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvCOUNT_MONEY.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCOUNT_MONEY_CellContentClick);
            this.dgvCOUNT_MONEY.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCOUNT_MONEY_CellEndEdit);
            this.dgvCOUNT_MONEY.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvCOUNT_MONEY_DataError);
            this.dgvCOUNT_MONEY.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvCOUNT_MONEY_EditingControlShowing);
            // 
            // COUNT_CASH_ID
            // 
            this.COUNT_CASH_ID.DataPropertyName = "COUNT_CASH_ID";
            resources.ApplyResources(this.COUNT_CASH_ID, "COUNT_CASH_ID");
            this.COUNT_CASH_ID.Name = "COUNT_CASH_ID";
            // 
            // CURRENCY_ID_
            // 
            this.CURRENCY_ID_.DataPropertyName = "CURRENCY_ID";
            resources.ApplyResources(this.CURRENCY_ID_, "CURRENCY_ID_");
            this.CURRENCY_ID_.Name = "CURRENCY_ID_";
            // 
            // CASH_VALUE
            // 
            this.CASH_VALUE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CASH_VALUE.DataPropertyName = "CASH_VALUE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.Format = "#,###.##";
            dataGridViewCellStyle3.NullValue = null;
            this.CASH_VALUE.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CASH_VALUE, "CASH_VALUE");
            this.CASH_VALUE.Name = "CASH_VALUE";
            this.CASH_VALUE.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CASH_VALUE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // QUANTITY
            // 
            this.QUANTITY.DataPropertyName = "QUANTITY";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.Format = "#,###.##";
            this.QUANTITY.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.QUANTITY, "QUANTITY");
            this.QUANTITY.Name = "QUANTITY";
            this.QUANTITY.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "AMOUNT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "#,###.##";
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.AMOUNT, "AMOUNT");
            this.AMOUNT.Name = "AMOUNT";
            this.AMOUNT.ReadOnly = true;
            this.AMOUNT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CURRENCY_SING0
            // 
            this.CURRENCY_SING0.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCellsExceptHeader;
            this.CURRENCY_SING0.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CURRENCY_SING0.DefaultCellStyle = dataGridViewCellStyle6;
            this.CURRENCY_SING0.FillWeight = 35F;
            resources.ApplyResources(this.CURRENCY_SING0, "CURRENCY_SING0");
            this.CURRENCY_SING0.Name = "CURRENCY_SING0";
            this.CURRENCY_SING0.ReadOnly = true;
            this.CURRENCY_SING0.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.CURRENCY_SING0.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CONTROL_
            // 
            this.CONTROL_.DataPropertyName = "CONTROL";
            resources.ApplyResources(this.CONTROL_, "CONTROL_");
            this.CONTROL_.Name = "CONTROL_";
            this.CONTROL_.ReadOnly = true;
            this.CONTROL_.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // IS_HEADER_
            // 
            this.IS_HEADER_.DataPropertyName = "IS_HEADER";
            resources.ApplyResources(this.IS_HEADER_, "IS_HEADER_");
            this.IS_HEADER_.Name = "IS_HEADER_";
            this.IS_HEADER_.ReadOnly = true;
            // 
            // DialogCloseCashDrawer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCloseCashDrawer";
            this.Load += new System.EventHandler(this.DialogCloseCashDrawer_Load);
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.pnlCountMoney.ResumeLayout(false);
            this.pnlCountMoney.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCOUNT_MONEY)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblCASH_DRAWER;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private Label lblOPEN_CASH_DRAWER_DATE;
        private DateTimePicker txtOpenDate;
        private TextBox txtCashDrawer;
        private DateTimePicker txtCloseDate;
        private Label lblPRINTER;
        private ExButton btnVIEW;
        private ComboBox cboPrinterCashDrawer;
        private Label lblAMOUNT;
        private DataGridView dgv;
        private TextBox txtNote;
        private Label lblNOTE;
        private ComboBox cboReport;
        private Label lblREPORT;
        private DataGridViewTextBoxColumn CURRENCY_ID;
        private DataGridViewTextBoxColumn CASH_FLOAT;
        private DataGridViewTextBoxColumn TOTAL_CASH;
        private DataGridViewTextBoxColumn ACTUAL_CASH;
        private DataGridViewTextBoxColumn SURPLUS_CASH;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private Panel pnlCountMoney;
        private CheckBox chkCOUNT_MONEY;
        private DataGridView dgvCOUNT_MONEY;
        private Label lblCLOSE_CASH_DRAWER_DATE;
        private DataGridViewTextBoxColumn COUNT_CASH_ID;
        private DataGridViewTextBoxColumn CURRENCY_ID_;
        private DataGridViewTextBoxColumn CASH_VALUE;
        private DataGridViewTextBoxColumn QUANTITY;
        private DataGridViewTextBoxColumn AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING0;
        private DataGridViewImageColumn CONTROL_;
        private DataGridViewCheckBoxColumn IS_HEADER_;
    }
}