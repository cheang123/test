﻿using EPower.Base.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogIRRegistration : ExDialog
    {
        DataTable dt = new DataTable();

        public DialogIRRegistration()
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgv);
            btnBrowse_Click(null, null);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();
            diag.Filter = "EPower File|*.epf";
            if (diag.ShowDialog() == DialogResult.OK)
            {
                this.txtFileName.Text = diag.FileName;
                loadData();
            }
        }

        private void loadData()
        {
            List<TBL_DEVICE> objDList = new List<TBL_DEVICE>();
            List<string> objDListPro = new List<string>();
            bool objType = true;
            string strDInfo = string.Empty;
            StreamReader sr = new StreamReader(txtFileName.Text);
            try
            {
                while (!sr.EndOfStream)
                {
                    //Check File Header
                    if (objType)
                    {
                        if (!sr.ReadLine().Trim().Contains(Method.GetFILE_HEADER(ItemType.IR_READER)))
                        {
                            MsgBox.ShowInformation(Resources.MS_FILE_IS_NOT_CORRECT_FORMAT);
                            return;
                        }
                        objType = false;
                        strDInfo = sr.ReadLine();
                        continue;
                    }
                    string strReadline = sr.ReadLine();
                    string[] irInfo = strReadline.Split('-');
                    //Invalid Register Code
                    if (!irInfo[1].Trim().Contains(Method.GetIR_REGCODE(irInfo[0])))
                    {
                        objDListPro.Add(string.Concat(strReadline, "-", Resources.MS_INVALID_REGISTER_CODE));
                        continue;
                    }
                    //Is Exist Device
                    if (DBDataContext.Db.TBL_DEVICEs
                        .Where(x => x.DEVICE_CODE.Contains(irInfo[0])
                            && x.DEVICE_TYPE_ID == (int)DeviceType.IR_READER).Count() > 0)
                    {
                        objDListPro.Add(string.Concat(strReadline, "-", string.Format(Resources.MS_IS_EXISTS, Resources.DEVICE)));
                        continue;
                    }
                    TBL_DEVICE objDevice = new TBL_DEVICE();
                    objDevice.DEVICE_CODE = irInfo[0];
                    objDevice.DEVICE_HARDWARE_ID = Method.GetIR_REGCODE(objDevice.DEVICE_CODE);
                    objDevice.DEVICE_OEM_INFO = strDInfo;
                    objDevice.DEVICE_TYPE = DeviceType.IR_READER.ToString();
                    objDevice.DEVICE_TYPE_ID = (int)DeviceType.IR_READER;
                    objDevice.STATUS_ID = (int)DeviceStatus.Stock;
                    objDevice.DRIVER = "";
                    objDList.Add(objDevice);
                }

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    foreach (TBL_DEVICE item in objDList)
                    {
                        DBDataContext.Db.Insert(item);
                    }
                    tran.Complete();
                }
                foreach (TBL_DEVICE item in objDList)
                {
                    objDListPro.Add(string.Format("{0}-{1}-{2}", item.DEVICE_CODE, item.DEVICE_HARDWARE_ID, Resources.REGISTER_SUCCESS));
                }
                dgv.Rows.Clear();
                foreach (string item in objDListPro)
                {
                    dgv.Rows.Add(item.Split('-'));
                }
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
            finally
            {
                sr.Close();
            }
        }
    }
}