﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Process = System.Diagnostics.Process;

namespace EPower
{
    public partial class DialogUpdate : ExDialog
    {
        string pathUpdate;
        string destination;
        string pathVersion;
        string url;
        string version = "";
        DateTime startDownload = DateTime.Now;
        WebClient webClient = null;

        private readonly Stopwatch _sw = new Stopwatch();
        static ManualResetEvent MyMA = new ManualResetEvent(false);
        private Thread runner = null;

        public DialogUpdate(string version)
        {
            InitializeComponent();
            this.version = version;
            pbAds.ImageLocation = "http://e-power.com.kh/ads-epower/download-ads.gif";
            runner = new Thread(download);
            runner.Start();
        }
        private void download()
        {
            webClient = new WebClient();
            url = Method.Utilities[Utility.UPDATE_URL];
            url += url.EndsWith("/") ? "" : "/";
            //no need later 
            //  url += "Versions/";
            url += string.Format("v{0}.zip", version);

            pathUpdate = new DirectoryInfo(Application.StartupPath).Parent.FullName + @"\Update";
            destination = pathUpdate + string.Format(@"\v{0}.zip", version);
            // if folder is not exists then create it
            if (!Directory.Exists(pathUpdate))
            {
                Directory.CreateDirectory(pathUpdate);
            }

            // if vX.X.X.X is exists then no need to download any more.
            if (Directory.Exists(pathVersion))
            {
                update(version);
                return;
            }

            _sw.Start();
            try
            {
                using (var client = new HttpClientDownloadWithProgress(url, destination))
                {
                    client.ProgressChanged += (totalFileSize, totalBytesDownloaded, progressPercentage) =>
                    {
                        var decimalFileSize = DataHelper.ParseToDouble(totalFileSize.ToString());
                        var decimalDownloaded = DataHelper.ParseToDouble(totalBytesDownloaded.ToString());

                        this.BeginInvoke(new Action(() =>
                        {
                            _lblrootname.Text = version;
                            var fileSize = totalFileSize / 1024d / 1024d;
                            var downloadCompleted = decimalDownloaded / 1024d / 1024d;
                            var transferRate = decimalDownloaded / 1024d / _sw.Elapsed.TotalSeconds;
                            var percentage = progressPercentage;
                            _lblFileSize.Text = $"{ fileSize:N2}MB";
                            _lblDownloaded.Text = $"{downloadCompleted:N2}MB ({percentage}%)";
                            _lblTransferRate.Text = $"{transferRate:N2}kb/s";
                            btnDownloading.Value = (int)progressPercentage;
                        }));
                    };
                    client.StartDownload().Wait();
                    _sw.Stop();
                }

            }
            catch(Exception ex)
            {
                MsgBox.ShowWarning(string.Format(Resources.YOU_CANNOT_PROCESS,"",Resources.UPDATE_VERSION), Resources.WARNING);
                MsgBox.LogError(ex);
                return;
            }

            destination = pathUpdate + string.Format(@"\v{0}.zip", version);
            pathVersion = destination.Replace(".zip", "");
            Zip.UnZipFiles(destination, pathVersion, "123", true);
            update(version);
        }

        private void update(string version)
        {
            string pathBackup = string.Format(@"{0}\Backup\{1} - {2:yyyyMMddHHmmss}", pathUpdate, new DirectoryInfo(Application.StartupPath).Name, DBDataContext.Db.GetSystemDate());

            if (!Directory.Exists(pathUpdate + @"\Backup"))
            {
                Directory.CreateDirectory(pathUpdate + @"\Backup");
            }
            if (!Directory.Exists(pathBackup))
            {
                Directory.CreateDirectory(pathBackup);
            }
            //Stop all Pointer services
            var services = ServiceController.GetServices().Where(x => x.ServiceName.Contains("Pointer"));
            foreach (var service in services)
            {
                if (service.Status == ServiceControllerStatus.Running)
                {
                    try
                    {
                        service.Stop();
                    }
                    catch
                    {

                    }
                }
            }

            // if bat file is not exists then create bat file.
            if (File.Exists(pathUpdate + @"\update.bat"))
            {
                File.Delete(pathUpdate + @"\update.bat");
            }
            // write bat file content
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("@ECHO OFF");
            sb.AppendLine(@"TASKKILL /F /FI ""pid gt 0"" /IM EPower.exe*");
            sb.AppendLine(@"TASKKILL /F /FI ""pid gt 0"" /IM EACAppClient.exe");
            sb.AppendLine(@"TASKKILL /F /FI ""pid gt 0"" /IM adb.exe");
            sb.AppendLine(string.Format("ATTRIB -R /S \"{0}\\*\"", Application.StartupPath));
            sb.AppendLine(string.Format("XCOPY \"{0}\\*\" \"{1}\" /E /Y", Application.StartupPath, pathBackup));
            sb.AppendLine(string.Format("XCOPY \"{0}\\v{1}\\*\" \"{2}\" /E /Y", pathUpdate, version, Application.StartupPath));
            sb.AppendLine(string.Format("START /d \"{0}\" EPower.exe", Application.StartupPath));
            File.WriteAllText(pathUpdate + "\\update.bat", sb.ToString());

            // start shell commant int bat file
            Process.Start(pathUpdate + "\\update.bat");

            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                webClient.CancelAsync();
            }
            catch (Exception)
            {
            }
            this.Close();
        }

        public bool UrlIsValid(string url)
        {
            try
            {
                HttpWebRequest request = HttpWebRequest.Create(url) as HttpWebRequest;
                request.Timeout = 5000; //set the timeout to 5 seconds to keep the user from waiting too long for the page to load
                request.Method = "HEAD"; //Get only the header information -- no need to download any content

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                int statusCode = (int)response.StatusCode;
                if (statusCode >= 100 && statusCode < 400) //Good requests
                {
                    return true;
                }
                else if (statusCode >= 500 && statusCode <= 510) //Server Errors
                {
                    // throw new Exception(String.Format("The remote server has thrown an internal error. Url is not valid: {0}", url)); 
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError) //400 errors
                {
                    return false;
                }
                else
                {
                    // throw new Exception(String.Format("Unhandled status [{0}] returned for url: {1}", ex.Status, url), ex);
                }
            }
            catch (Exception)
            {
                // throw new Exception(String.Format("Could not test url {0}.", url), ex);
            }
            return false;
        }

        private void pbAds_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                pbAds.ImageLocation = Application.StartupPath + @"\ads\download-ads.gif";
            }
        }
    }
    public class HttpClientDownloadWithProgress : IDisposable
    {
        private readonly string _downloadUrl;
        private readonly string _destinationFilePath;

        private HttpClient _httpClient;

        public delegate void ProgressChangedHandler(long? totalFileSize, long totalBytesDownloaded, double? progressPercentage);

        public event ProgressChangedHandler ProgressChanged;

        public HttpClientDownloadWithProgress(string downloadUrl, string destinationFilePath)
        {
            _downloadUrl = downloadUrl;
            _destinationFilePath = destinationFilePath;
        }

        public async Task StartDownload()
        {
            _httpClient = new HttpClient { Timeout = TimeSpan.FromDays(1) };

            using (var response = await _httpClient.GetAsync(_downloadUrl, HttpCompletionOption.ResponseHeadersRead))
                await DownloadFileFromHttpResponseMessage(response);
        }

        private async Task DownloadFileFromHttpResponseMessage(HttpResponseMessage response)
        {
            response.EnsureSuccessStatusCode();

            var totalBytes = response.Content.Headers.ContentLength;

            using (var contentStream = await response.Content.ReadAsStreamAsync())
                await ProcessContentStream(totalBytes, contentStream);
        }

        private async Task ProcessContentStream(long? totalDownloadSize, Stream contentStream)
        {
            var totalBytesRead = 0L;
            var readCount = 0L;
            var buffer = new byte[8192];
            var isMoreToRead = true;

            using (var fileStream = new FileStream(_destinationFilePath, FileMode.Create, FileAccess.Write, FileShare.None, 8192, true))
            {
                do
                {
                    var bytesRead = await contentStream.ReadAsync(buffer, 0, buffer.Length);
                    if (bytesRead == 0)
                    {
                        isMoreToRead = false;
                        TriggerProgressChanged(totalDownloadSize, totalBytesRead);
                        continue;
                    }

                    await fileStream.WriteAsync(buffer, 0, bytesRead);

                    totalBytesRead += bytesRead;
                    readCount += 1;

                    if (readCount % 100 == 0)
                        TriggerProgressChanged(totalDownloadSize, totalBytesRead);
                }
                while (isMoreToRead);
            }
        }

        private void TriggerProgressChanged(long? totalDownloadSize, long totalBytesRead)
        {
            if (ProgressChanged == null)
                return;

            double? progressPercentage = null;
            if (totalDownloadSize.HasValue)
                progressPercentage = Math.Round((double)totalBytesRead / totalDownloadSize.Value * 100, 2);

            ProgressChanged(totalDownloadSize, totalBytesRead, progressPercentage);
        }

        public void Dispose()
        {
            _httpClient?.Dispose();
        }
    }
}