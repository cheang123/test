﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageStaffExpense
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageStaffExpense));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtpYear = new System.Windows.Forms.DateTimePicker();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.dgvStaffExpense = new System.Windows.Forms.DataGridView();
            this.EXPENSE_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MONTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_BY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CREATE_ON = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAYMENT_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AMOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStaffExpense)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.dtpYear);
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // dtpYear
            // 
            resources.ApplyResources(this.dtpYear, "dtpYear");
            this.dtpYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpYear.Name = "dtpYear";
            this.dtpYear.ShowUpDown = true;
            this.dtpYear.ValueChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // dgvStaffExpense
            // 
            this.dgvStaffExpense.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvStaffExpense.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvStaffExpense.BackgroundColor = System.Drawing.Color.White;
            this.dgvStaffExpense.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvStaffExpense.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvStaffExpense.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStaffExpense.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EXPENSE_ID,
            this.MONTH,
            this.CREATE_BY,
            this.CREATE_ON,
            this.PAYMENT_ACCOUNT,
            this.AMOUNT,
            this.CURRENCY_SING_});
            resources.ApplyResources(this.dgvStaffExpense, "dgvStaffExpense");
            this.dgvStaffExpense.EnableHeadersVisualStyles = false;
            this.dgvStaffExpense.Name = "dgvStaffExpense";
            this.dgvStaffExpense.RowHeadersVisible = false;
            this.dgvStaffExpense.RowTemplate.Height = 25;
            this.dgvStaffExpense.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStaffExpense.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStaffExpense_CellDoubleClick);
            // 
            // EXPENSE_ID
            // 
            this.EXPENSE_ID.DataPropertyName = "EXPENSE_ID";
            resources.ApplyResources(this.EXPENSE_ID, "EXPENSE_ID");
            this.EXPENSE_ID.Name = "EXPENSE_ID";
            // 
            // MONTH
            // 
            this.MONTH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MONTH.DataPropertyName = "EXPENSE_DATE";
            dataGridViewCellStyle2.Format = "MM-yyyy";
            dataGridViewCellStyle2.NullValue = null;
            this.MONTH.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.MONTH, "MONTH");
            this.MONTH.Name = "MONTH";
            // 
            // CREATE_BY
            // 
            this.CREATE_BY.DataPropertyName = "CREATE_BY";
            resources.ApplyResources(this.CREATE_BY, "CREATE_BY");
            this.CREATE_BY.Name = "CREATE_BY";
            // 
            // CREATE_ON
            // 
            this.CREATE_ON.DataPropertyName = "CREATE_ON";
            dataGridViewCellStyle3.Format = "dd-MM-yyyy";
            dataGridViewCellStyle3.NullValue = null;
            this.CREATE_ON.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.CREATE_ON, "CREATE_ON");
            this.CREATE_ON.Name = "CREATE_ON";
            // 
            // PAYMENT_ACCOUNT
            // 
            this.PAYMENT_ACCOUNT.DataPropertyName = "ACCOUNT_NAME";
            resources.ApplyResources(this.PAYMENT_ACCOUNT, "PAYMENT_ACCOUNT");
            this.PAYMENT_ACCOUNT.Name = "PAYMENT_ACCOUNT";
            // 
            // AMOUNT
            // 
            this.AMOUNT.DataPropertyName = "TOTAL_AMOUNT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "#,##0.####";
            dataGridViewCellStyle4.NullValue = null;
            this.AMOUNT.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.AMOUNT, "AMOUNT");
            this.AMOUNT.Name = "AMOUNT";
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.CURRENCY_SING_.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            // 
            // PageStaffExpense
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgvStaffExpense);
            this.Controls.Add(this.panel1);
            this.Name = "PageStaffExpense";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStaffExpense)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private ExButton btnREMOVE;
        private DateTimePicker dtpYear;
        private DataGridView dgvStaffExpense;
        private DataGridViewTextBoxColumn EXPENSE_ID;
        private DataGridViewTextBoxColumn MONTH;
        private DataGridViewTextBoxColumn CREATE_BY;
        private DataGridViewTextBoxColumn CREATE_ON;
        private DataGridViewTextBoxColumn PAYMENT_ACCOUNT;
        private DataGridViewTextBoxColumn AMOUNT;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
    }
}
