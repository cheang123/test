﻿using System;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech.Component;

namespace EPower.Interface
{
    public partial class Notification : ExDialog
    {
        string mVersion;
        int timeOut = 0;
        double opacity = 100;

        #region Constructor
        public Notification(string version)
        {
            InitializeComponent();
            this.mVersion = version;
            
            this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
            this.Left = Screen.PrimaryScreen.WorkingArea.Width-this.Width; 
        }

        protected override void OnLoad(EventArgs e)
        {
            btnUpdate_.Text = Resources.MS_UPDATE_NEW_VERSION +" " + mVersion;
            timer1.Start();
        }

        #endregion 

        private void timer1_Tick(object sender, EventArgs e)
        {
            timeOut += timer1.Interval;
            if (timeOut==TimeSpan.FromMinutes(5).TotalMilliseconds)
            {
                this.Close();
            } 
            opacity -=5; 
            this.Opacity =opacity/100; 
        }

        private void Notification_MouseHover(object sender, EventArgs e)
        {
            timer1.Stop();
            timeOut = 0;
            opacity = 100;
            this.Opacity = 1.0;
        }

        private void Notification_MouseLeave(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            new DialogUpdate(mVersion).ShowDialog();
        }
    }
}