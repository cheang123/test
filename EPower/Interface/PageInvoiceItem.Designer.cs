﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PageInvoiceItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageInvoiceItem));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboCurrency = new System.Windows.Forms.ComboBox();
            this.cboItemType = new System.Windows.Forms.ComboBox();
            this.btnREMOVE = new SoftTech.Component.ExButton();
            this.btnADD = new SoftTech.Component.ExButton();
            this.btnEDIT = new SoftTech.Component.ExButton();
            this.txtQuickSearch = new SoftTech.Component.ExTextbox();
            this.INVOICE_ITEM_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERVICE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SERVICE_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INCOME_ACCOUNT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VALUE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CURRENCY_SING_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IS_RECURRING_SERVICE = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colItemTypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.INVOICE_ITEM_ID,
            this.SERVICE,
            this.SERVICE_TYPE,
            this.INCOME_ACCOUNT,
            this.VALUE,
            this.CURRENCY_SING_,
            this.IS_RECURRING_SERVICE,
            this.colItemTypeId});
            resources.ApplyResources(this.dgv, "dgv");
            this.dgv.EnableHeadersVisualStyles = false;
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 25;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellDoubleClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.cboCurrency);
            this.panel1.Controls.Add(this.cboItemType);
            this.panel1.Controls.Add(this.btnREMOVE);
            this.panel1.Controls.Add(this.btnADD);
            this.panel1.Controls.Add(this.btnEDIT);
            this.panel1.Controls.Add(this.txtQuickSearch);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboCurrency
            // 
            this.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCurrency.FormattingEnabled = true;
            resources.ApplyResources(this.cboCurrency, "cboCurrency");
            this.cboCurrency.Name = "cboCurrency";
            this.cboCurrency.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // cboItemType
            // 
            this.cboItemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboItemType.FormattingEnabled = true;
            resources.ApplyResources(this.cboItemType, "cboItemType");
            this.cboItemType.Name = "cboItemType";
            this.cboItemType.SelectedIndexChanged += new System.EventHandler(this.txt_QuickSearch);
            // 
            // btnREMOVE
            // 
            resources.ApplyResources(this.btnREMOVE, "btnREMOVE");
            this.btnREMOVE.Name = "btnREMOVE";
            this.btnREMOVE.UseVisualStyleBackColor = true;
            this.btnREMOVE.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnADD
            // 
            resources.ApplyResources(this.btnADD, "btnADD");
            this.btnADD.Name = "btnADD";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnEDIT
            // 
            resources.ApplyResources(this.btnEDIT, "btnEDIT");
            this.btnEDIT.Name = "btnEDIT";
            this.btnEDIT.UseVisualStyleBackColor = true;
            this.btnEDIT.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtQuickSearch
            // 
            this.txtQuickSearch.BackColor = System.Drawing.Color.White;
            this.txtQuickSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtQuickSearch, "txtQuickSearch");
            this.txtQuickSearch.Name = "txtQuickSearch";
            this.txtQuickSearch.SearchMode = SoftTech.Component.ExTextbox.SearchModes.QuickSearch;
            this.txtQuickSearch.QuickSearch += new System.EventHandler(this.txt_QuickSearch);
            // 
            // INVOICE_ITEM_ID
            // 
            this.INVOICE_ITEM_ID.DataPropertyName = "INVOICE_ITEM_ID";
            resources.ApplyResources(this.INVOICE_ITEM_ID, "INVOICE_ITEM_ID");
            this.INVOICE_ITEM_ID.Name = "INVOICE_ITEM_ID";
            // 
            // SERVICE
            // 
            this.SERVICE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SERVICE.DataPropertyName = "INVOICE_ITEM_NAME";
            resources.ApplyResources(this.SERVICE, "SERVICE");
            this.SERVICE.Name = "SERVICE";
            // 
            // SERVICE_TYPE
            // 
            this.SERVICE_TYPE.DataPropertyName = "INVOICE_ITEM_TYPE_NAME";
            resources.ApplyResources(this.SERVICE_TYPE, "SERVICE_TYPE");
            this.SERVICE_TYPE.Name = "SERVICE_TYPE";
            // 
            // INCOME_ACCOUNT
            // 
            this.INCOME_ACCOUNT.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.INCOME_ACCOUNT.DataPropertyName = "INCOME_ACCOUNT_NAME";
            resources.ApplyResources(this.INCOME_ACCOUNT, "INCOME_ACCOUNT");
            this.INCOME_ACCOUNT.Name = "INCOME_ACCOUNT";
            // 
            // VALUE
            // 
            this.VALUE.DataPropertyName = "PRICE";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "#,##0.####";
            dataGridViewCellStyle2.NullValue = null;
            this.VALUE.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.VALUE, "VALUE");
            this.VALUE.Name = "VALUE";
            // 
            // CURRENCY_SING_
            // 
            this.CURRENCY_SING_.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.CURRENCY_SING_.DataPropertyName = "CURRENCY_SING";
            resources.ApplyResources(this.CURRENCY_SING_, "CURRENCY_SING_");
            this.CURRENCY_SING_.Name = "CURRENCY_SING_";
            // 
            // IS_RECURRING_SERVICE
            // 
            this.IS_RECURRING_SERVICE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.IS_RECURRING_SERVICE.DataPropertyName = "IS_RECURRING_SERVICE";
            resources.ApplyResources(this.IS_RECURRING_SERVICE, "IS_RECURRING_SERVICE");
            this.IS_RECURRING_SERVICE.Name = "IS_RECURRING_SERVICE";
            // 
            // colItemTypeId
            // 
            this.colItemTypeId.DataPropertyName = "INVOICE_ITEM_TYPE_ID";
            resources.ApplyResources(this.colItemTypeId, "colItemTypeId");
            this.colItemTypeId.Name = "colItemTypeId";
            // 
            // PageInvoiceItem
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.panel1);
            this.Name = "PageInvoiceItem";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExTextbox txtQuickSearch;
        private ExButton btnADD;
        private ExButton btnEDIT;
        private DataGridView dgv;
        private ExButton btnREMOVE;
        public ComboBox cboItemType;
        public ComboBox cboCurrency;
        private DataGridViewTextBoxColumn INVOICE_ITEM_ID;
        private DataGridViewTextBoxColumn SERVICE;
        private DataGridViewTextBoxColumn SERVICE_TYPE;
        private DataGridViewTextBoxColumn INCOME_ACCOUNT;
        private DataGridViewTextBoxColumn VALUE;
        private DataGridViewTextBoxColumn CURRENCY_SING_;
        private DataGridViewCheckBoxColumn IS_RECURRING_SERVICE;
        private DataGridViewTextBoxColumn colItemTypeId;
    }
}
