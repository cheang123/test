﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class DialogCircuitBreakerType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogCircuitBreakerType));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCLOSE = new SoftTech.Component.ExButton();
            this.btnOK = new SoftTech.Component.ExButton();
            this.txtBrakerTypeCode = new System.Windows.Forms.TextBox();
            this.lblBREAKER_TYPE_CODE = new System.Windows.Forms.Label();
            this.txtBrakerTypeName = new System.Windows.Forms.TextBox();
            this.lblBREAKER_TYPE_NAME = new System.Windows.Forms.Label();
            this.cboAmpera = new System.Windows.Forms.ComboBox();
            this.cboVoltage = new System.Windows.Forms.ComboBox();
            this.cboPhase = new System.Windows.Forms.ComboBox();
            this.cboConstant = new System.Windows.Forms.ComboBox();
            this.lblAMPARE = new System.Windows.Forms.Label();
            this.lblVOLTAGE = new System.Windows.Forms.Label();
            this.lblPHASE = new System.Windows.Forms.Label();
            this.lblCONSTANT = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnCHANGE_LOG = new SoftTech.Component.ExButton();
            this.btnAddContant = new SoftTech.Component.ExAddItem();
            this.btnAddPhase = new SoftTech.Component.ExAddItem();
            this.btnAddAmpare = new SoftTech.Component.ExAddItem();
            this.btnAddVoltage = new SoftTech.Component.ExAddItem();
            this.content.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // content
            // 
            resources.ApplyResources(this.content, "content");
            this.content.Controls.Add(this.btnAddAmpare);
            this.content.Controls.Add(this.btnAddVoltage);
            this.content.Controls.Add(this.btnAddPhase);
            this.content.Controls.Add(this.btnAddContant);
            this.content.Controls.Add(this.btnCHANGE_LOG);
            this.content.Controls.Add(this.label12);
            this.content.Controls.Add(this.label11);
            this.content.Controls.Add(this.label10);
            this.content.Controls.Add(this.label7);
            this.content.Controls.Add(this.label1);
            this.content.Controls.Add(this.label9);
            this.content.Controls.Add(this.cboAmpera);
            this.content.Controls.Add(this.cboVoltage);
            this.content.Controls.Add(this.cboPhase);
            this.content.Controls.Add(this.cboConstant);
            this.content.Controls.Add(this.lblAMPARE);
            this.content.Controls.Add(this.lblVOLTAGE);
            this.content.Controls.Add(this.lblPHASE);
            this.content.Controls.Add(this.lblCONSTANT);
            this.content.Controls.Add(this.txtBrakerTypeCode);
            this.content.Controls.Add(this.lblBREAKER_TYPE_CODE);
            this.content.Controls.Add(this.txtBrakerTypeName);
            this.content.Controls.Add(this.lblBREAKER_TYPE_NAME);
            this.content.Controls.Add(this.panel1);
            this.content.Controls.Add(this.btnCLOSE);
            this.content.Controls.Add(this.btnOK);
            this.content.Controls.SetChildIndex(this.btnOK, 0);
            this.content.Controls.SetChildIndex(this.btnCLOSE, 0);
            this.content.Controls.SetChildIndex(this.panel1, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_TYPE_NAME, 0);
            this.content.Controls.SetChildIndex(this.txtBrakerTypeName, 0);
            this.content.Controls.SetChildIndex(this.lblBREAKER_TYPE_CODE, 0);
            this.content.Controls.SetChildIndex(this.txtBrakerTypeCode, 0);
            this.content.Controls.SetChildIndex(this.lblCONSTANT, 0);
            this.content.Controls.SetChildIndex(this.lblPHASE, 0);
            this.content.Controls.SetChildIndex(this.lblVOLTAGE, 0);
            this.content.Controls.SetChildIndex(this.lblAMPARE, 0);
            this.content.Controls.SetChildIndex(this.cboConstant, 0);
            this.content.Controls.SetChildIndex(this.cboPhase, 0);
            this.content.Controls.SetChildIndex(this.cboVoltage, 0);
            this.content.Controls.SetChildIndex(this.cboAmpera, 0);
            this.content.Controls.SetChildIndex(this.label9, 0);
            this.content.Controls.SetChildIndex(this.label1, 0);
            this.content.Controls.SetChildIndex(this.label7, 0);
            this.content.Controls.SetChildIndex(this.label10, 0);
            this.content.Controls.SetChildIndex(this.label11, 0);
            this.content.Controls.SetChildIndex(this.label12, 0);
            this.content.Controls.SetChildIndex(this.btnCHANGE_LOG, 0);
            this.content.Controls.SetChildIndex(this.btnAddContant, 0);
            this.content.Controls.SetChildIndex(this.btnAddPhase, 0);
            this.content.Controls.SetChildIndex(this.btnAddVoltage, 0);
            this.content.Controls.SetChildIndex(this.btnAddAmpare, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // btnCLOSE
            // 
            resources.ApplyResources(this.btnCLOSE, "btnCLOSE");
            this.btnCLOSE.Name = "btnCLOSE";
            this.btnCLOSE.UseVisualStyleBackColor = true;
            this.btnCLOSE.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtBrakerTypeCode
            // 
            resources.ApplyResources(this.txtBrakerTypeCode, "txtBrakerTypeCode");
            this.txtBrakerTypeCode.Name = "txtBrakerTypeCode";
            this.txtBrakerTypeCode.Enter += new System.EventHandler(this.ChangeEngilshKeyboard);
            // 
            // lblBREAKER_TYPE_CODE
            // 
            resources.ApplyResources(this.lblBREAKER_TYPE_CODE, "lblBREAKER_TYPE_CODE");
            this.lblBREAKER_TYPE_CODE.Name = "lblBREAKER_TYPE_CODE";
            // 
            // txtBrakerTypeName
            // 
            resources.ApplyResources(this.txtBrakerTypeName, "txtBrakerTypeName");
            this.txtBrakerTypeName.Name = "txtBrakerTypeName";
            this.txtBrakerTypeName.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblBREAKER_TYPE_NAME
            // 
            resources.ApplyResources(this.lblBREAKER_TYPE_NAME, "lblBREAKER_TYPE_NAME");
            this.lblBREAKER_TYPE_NAME.Name = "lblBREAKER_TYPE_NAME";
            // 
            // cboAmpera
            // 
            this.cboAmpera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAmpera.FormattingEnabled = true;
            resources.ApplyResources(this.cboAmpera, "cboAmpera");
            this.cboAmpera.Name = "cboAmpera";
            this.cboAmpera.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // cboVoltage
            // 
            this.cboVoltage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVoltage.FormattingEnabled = true;
            resources.ApplyResources(this.cboVoltage, "cboVoltage");
            this.cboVoltage.Name = "cboVoltage";
            this.cboVoltage.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // cboPhase
            // 
            this.cboPhase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPhase.FormattingEnabled = true;
            resources.ApplyResources(this.cboPhase, "cboPhase");
            this.cboPhase.Name = "cboPhase";
            this.cboPhase.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // cboConstant
            // 
            this.cboConstant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConstant.FormattingEnabled = true;
            resources.ApplyResources(this.cboConstant, "cboConstant");
            this.cboConstant.Name = "cboConstant";
            this.cboConstant.Enter += new System.EventHandler(this.ChangeKhmerKeyboard);
            // 
            // lblAMPARE
            // 
            resources.ApplyResources(this.lblAMPARE, "lblAMPARE");
            this.lblAMPARE.Name = "lblAMPARE";
            // 
            // lblVOLTAGE
            // 
            resources.ApplyResources(this.lblVOLTAGE, "lblVOLTAGE");
            this.lblVOLTAGE.Name = "lblVOLTAGE";
            // 
            // lblPHASE
            // 
            resources.ApplyResources(this.lblPHASE, "lblPHASE");
            this.lblPHASE.Name = "lblPHASE";
            // 
            // lblCONSTANT
            // 
            resources.ApplyResources(this.lblCONSTANT, "lblCONSTANT");
            this.lblCONSTANT.Name = "lblCONSTANT";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Name = "label9";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Name = "label1";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Name = "label7";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Name = "label10";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Name = "label11";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Name = "label12";
            // 
            // btnCHANGE_LOG
            // 
            resources.ApplyResources(this.btnCHANGE_LOG, "btnCHANGE_LOG");
            this.btnCHANGE_LOG.Name = "btnCHANGE_LOG";
            this.btnCHANGE_LOG.UseVisualStyleBackColor = true;
            this.btnCHANGE_LOG.Click += new System.EventHandler(this.btnChangelog_Click);
            // 
            // btnAddContant
            // 
            this.btnAddContant.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddContant, "btnAddContant");
            this.btnAddContant.Name = "btnAddContant";
            this.btnAddContant.AddItem += new System.EventHandler(this.btnAddContant_AddItem);
            // 
            // btnAddPhase
            // 
            this.btnAddPhase.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddPhase, "btnAddPhase");
            this.btnAddPhase.Name = "btnAddPhase";
            this.btnAddPhase.AddItem += new System.EventHandler(this.btnAddPhase_AddItem);
            // 
            // btnAddAmpare
            // 
            this.btnAddAmpare.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddAmpare, "btnAddAmpare");
            this.btnAddAmpare.Name = "btnAddAmpare";
            this.btnAddAmpare.AddItem += new System.EventHandler(this.btnAddAmpare_AddItem);
            // 
            // btnAddVoltage
            // 
            this.btnAddVoltage.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAddVoltage, "btnAddVoltage");
            this.btnAddVoltage.Name = "btnAddVoltage";
            this.btnAddVoltage.AddItem += new System.EventHandler(this.btnAddVoltage_AddItem);
            // 
            // DialogCircuitBreakerType
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DialogCircuitBreakerType";
            this.content.ResumeLayout(false);
            this.content.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private ExButton btnCLOSE;
        private ExButton btnOK;
        private TextBox txtBrakerTypeCode;
        private Label lblBREAKER_TYPE_CODE;
        private TextBox txtBrakerTypeName;
        private Label lblBREAKER_TYPE_NAME;
        private ComboBox cboAmpera;
        private ComboBox cboVoltage;
        private ComboBox cboPhase;
        private ComboBox cboConstant;
        private Label lblAMPARE;
        private Label lblVOLTAGE;
        private Label lblPHASE;
        private Label lblCONSTANT;
        private Label label12;
        private Label label11;
        private Label label10;
        private Label label7;
        private Label label1;
        private Label label9;
        private ExButton btnCHANGE_LOG;
        private ExAddItem btnAddAmpare;
        private ExAddItem btnAddVoltage;
        private ExAddItem btnAddPhase;
        private ExAddItem btnAddContant;
    }
}