﻿using System;
using System.Linq;
using System.Windows.Forms;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;

namespace EPower.Interface
{
    public partial class DialogPriceExtraChargeList : ExDialog
    {
        GeneralProcess _flag;
        TBL_PRICE _objPrice = new TBL_PRICE(); 
        #region Constructor
        public DialogPriceExtraChargeList(TBL_PRICE objPrice)
        {
            InitializeComponent();
            UIHelper.DataGridViewProperties(dgvPriceExtraCharge);
            objPrice._CopyTo(_objPrice);
            lblPriceName_.Text = _objPrice.PRICE_NAME; 
            read();
        }        
        #endregion

         

        #region Method
        
        /// <summary>
        /// Read data from object.
        /// </summary>
        private void read()
        {
            var db = from pe in DBDataContext.Db.TBL_PRICE_EXTRA_CHARGEs 
                     join iv in DBDataContext.Db.TBL_INVOICE_ITEMs on pe.INVOICE_ITEM_ID equals iv.INVOICE_ITEM_ID
                     where pe.IS_ACTIVE && pe.PRICE_ID==_objPrice.PRICE_ID
                     select new
                     {
                         pe.EXTRA_CHARGE_ID,
                         iv.INVOICE_ITEM_NAME,
                         pe.CHARGE_DESCRIPTION,
                         pe.CHARGE_VALUE,
                         CHARGE=pe.CHARGE_BY_PERCENTAGE?"%":DBDataContext.Db.TLKP_CURRENCies.FirstOrDefault(x=>x.CURRENCY_ID==_objPrice.CURRENCY_ID).CURRENCY_SING
                     };
            dgvPriceExtraCharge.DataSource = db;                 
        }

         
        #endregion

        private void linkAdd_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogPriceExtraCharge dig = new DialogPriceExtraCharge(GeneralProcess.Insert, _objPrice, new TBL_PRICE_EXTRA_CHARGE());
            if (dig.ShowDialog() == DialogResult.OK)
            {
                read();
                UIHelper.SelectRow(dgvPriceExtraCharge, dig.PriceExtraCharge.EXTRA_CHARGE_ID);
            }
        }

        private void linkRemove_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if(dgvPriceExtraCharge.Rows.Count>0)
            {
                int extraChargeId=(int)dgvPriceExtraCharge.SelectedRows[0].Cells[EXTRA_CHARGE_ID.Name].Value;
                TBL_PRICE_EXTRA_CHARGE objPriceExtraCharge=DBDataContext.Db.TBL_PRICE_EXTRA_CHARGEs.FirstOrDefault(x=>x.EXTRA_CHARGE_ID==extraChargeId);
                DialogPriceExtraCharge dig = new DialogPriceExtraCharge(GeneralProcess.Delete, _objPrice, objPriceExtraCharge);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    read();
                    UIHelper.SelectRow(dgvPriceExtraCharge, dig.PriceExtraCharge.EXTRA_CHARGE_ID - 1);
                }
            }
            
        }

        private void linkEdit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (dgvPriceExtraCharge.Rows.Count > 0)
            {
                int extraChargeId = (int)dgvPriceExtraCharge.SelectedRows[0].Cells[EXTRA_CHARGE_ID.Name].Value;
                TBL_PRICE_EXTRA_CHARGE objPriceExtraCharge = DBDataContext.Db.TBL_PRICE_EXTRA_CHARGEs.FirstOrDefault(x => x.EXTRA_CHARGE_ID == extraChargeId);
                DialogPriceExtraCharge dig = new DialogPriceExtraCharge(GeneralProcess.Update, _objPrice, objPriceExtraCharge);
                if (dig.ShowDialog() == DialogResult.OK)
                {
                    read();
                    UIHelper.SelectRow(dgvPriceExtraCharge, dig.PriceExtraCharge.EXTRA_CHARGE_ID);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

       
       
    }
}