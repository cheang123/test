﻿using EPower.Accounting.Interface;
using EPower.Base.Logic;
using EPower.Helper;
using EPower.Interface.BankPayment;
using EPower.Interface.PrePaid;
using EPower.Logic;
using EPower.Properties;
using HB01.Interfaces;
using Newtonsoft.Json.Linq;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;
using Process = System.Diagnostics.Process;
using Version = EPower.Update.Version;

namespace EPower.Interface
{
    public partial class FormMain : Form
    {
        Process proShedule = new Process();

        public FormMain()
        {
            InitializeComponent();


            ResourceHelper.ApplyResource(this);
            // resize screen
            this.WindowState = FormWindowState.Normal;
            this.StartPosition = FormStartPosition.Manual;
            this.Top = 0;
            this.Left = 0;
            this.Height = Screen.GetWorkingArea(this).Height;
            this.Width = Screen.GetWorkingArea(this).Width;


            // set up default parent to all dialog  
            ExDialog.DefaultOwner = this;

            // features avaiability
            this.btnPREPAID.Visible = Settings.Default.IS_USE_PREPAID;

            // apply permission. 
            btnPREPAID.Enabled = Login.IsAuthorized(Permission.PREPAID);
            btnPOSTPAID.Enabled = Login.IsAuthorized(Permission.POSTPAID);
            btnReports.Enabled = Login.IsAuthorized(Permission.REPORT);
            btnSYSTEM_SECURITY.Enabled = Login.IsAuthorized(Permission.SECURITY);
            btnADMIN_TASKS.Enabled = Login.IsAuthorized(Permission.ADMIN);
            btnCUSTOMER_BILLING.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING);
            btnPOWER_MANAGEMENT.Enabled = Login.IsAuthorized(Permission.POWER);

            // by default it's show page billing
            this.btnCUSTOMER_BILLING.Selected = true;
            this.showPageBilling();

            // show about us
            this.lblTITLE_.Text = DBDataContext.Db.TBL_COMPANies.FirstOrDefault().COMPANY_NAME;

            // demo version
            this.lblDEMO.Visible = DBDataContext.Db.Connection.Database.ToUpper().Contains("DEMO");

            btnLOG_OUT.Text = string.Format(Resources.LOG_OUT, Login.CurrentLogin.LOGIN_NAME);

            // advertisment
            // if have internet get from internet
            // else get from local folder
            if (TCPHelper.IsInternetConnected())
            {
                //pbAds.ImageLocation = "http://e-power.com.kh/ads-epower/e-power-ads.gif";
                pbAds.ImageLocation = Application.StartupPath + @"\ads\e-power-ads.gif";
            }
            else
            {
                pbAds.ImageLocation = Application.StartupPath + @"\ads\e-power-ads.gif";
            }


            PointerLogic.ActivateResource(Settings.Default.LANGUAGE);
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += delegate (object obj, DoWorkEventArgs e)
            {
                PointerLogic.checkServices(false);
                PointerLogic.Instance.startHB01();
            };
            worker.RunWorkerAsync();

            //this.btnACCOUNTING.Enabled = (PointerLogic.isConnectedPointer && Login.IsAuthorized(Permission.ACC));
            PointerLogic.PointerPath = Settings.Default.POINTER_PATH;
        }
        void NetworkChange_NetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {

            if (e.IsAvailable)
            {
                //Thread.Sleep(60*1000);
                PreProcessWithConnectivity();
            }
        }
        private void PreProcessWithConnectivity()
        {
            try
            {
                if (TCPHelper.IsInternetConnected())
                {
                    new ExchangeRateLogic().SyncExchangeRate();
                    checkVersionUpdate();
                    PaymentOperation.RunAutoSendDataInNewThread();
                    //CRMService.StartTrackMeterInNewThread();  

                    if (Method.GetUtilityValue(Utility.EBILL_SUPPLIER_AUTO_UPLOAD) == "1")
                    {
                        new EBillConnect().RunBackground();
                    }
                    BindAcitavtedBank();
                }
            }
            catch (Exception ex)
            {
                MsgBox.LogError(ex);
            }
        }
        public bool IsNewupdate;
        void checkVersionUpdate()
        {
            try
            {
                // checking for new avaliable update.
                BackgroundWorker worker = new BackgroundWorker();
                worker.DoWork += delegate (object s1, DoWorkEventArgs e1)
                {
                    e1.Result = CRMService.Intance.GetNextVersion(Version.ClientVersion);
                    if (!Settings.Default.ExchangeRateFirstLoad)
                    {
                        new ExchangeRateLogic().ExchangeRateFirstLoad();
                        Settings.Default.ExchangeRateFirstLoad = true;
                        Settings.Default.Save();
                    }
                };
                worker.RunWorkerCompleted += delegate (object s1, RunWorkerCompletedEventArgs e1)
                {
                    if (e1.Error != null || e1.Result == null)
                    {
                        return;
                    }
                    if (e1.Result.ToString().Length > 10)
                    {
                        //hide message connect to crm service . 
                        if (e1.Result.ToString() == Resources.CANNOT_CONECT_TO_SERVER)
                        {
                            return;
                        }
                        MsgBox.ShowError(e1.Result.ToString(), Resources.WARNING);
                        return;
                    }
                    var version = e1.Result.ToString();
                    if (string.IsNullOrEmpty(version))
                    {
                        return;
                    }
                    if (Version.ConvertToLong(version) <= Version.ConvertToLong(Version.ClientVersion))
                    {
                        return;
                    }
                    else
                    {
                        this.Invoke(new MethodInvoker(() => { new Notification(version).Show(); }));
                        IsNewupdate = true;
                    }
                };
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }

        }
        public class due
        {
            string cur_id { get; set; }
            decimal amount { get; set; }
        }
        void checkLicenseePendingPayment()
        {
            string url = "https://supplierapi.bill24.net/customer/org_code/" + Method.Utilities[Utility.B24_EAC_CUSTOMER_CODE].Replace("-", "");
            var client = new RestSharp.RestClient(url);
            var request = new RestSharp.RestRequest(RestSharp.Method.GET);
            request.AddHeader("token", "431832e44a36489997c1f2ad432e0de3");
            try
            {
                RestSharp.IRestResponse response = client.Execute(request);
                //var k = Newtonsoft.Json.JsonConvert.DeserializeObject(response.Content);
                var jo = JObject.Parse(response.Content);
                var data = jo["data"];
                if (data == null)
                {
                    return;
                }
                var dues = data["due"];
                if (dues.Any())
                {
                    new NotificationLicenseeFee(IsNewupdate).Show();
                }
            }
            catch (Exception)
            {

            }
        }

        void checkCashDrawer()
        {
            if (Base.Logic.Login.CurrentCashDrawer != null)
            {
                var days = DBDataContext.Db.TBL_CASH_DRAWERs.Where(x => x.CASH_DRAWER_ID == Base.Logic.Login.CurrentCashDrawer.CASH_DRAWER_ID).Select(x => x.DAY).FirstOrDefault();
                if (days != -1 && (DBDataContext.Db.GetSystemDate().Date - Base.Logic.Login.CurrentCashDrawer.OPEN_DATE.Date).TotalDays >= days)
                {
                    if (MsgBox.ShowQuestion(Resources.MS_THIS_CASH_DRAWER_NEED_TO_CLOSE, Resources.WARNING) != DialogResult.Yes)
                        return;
                    else
                        new DialogCloseCashDrawer().ShowDialog();
                }
            }
        }

        void checkPendingBankPayment()
        {
            if (!DataHelper.ParseToBoolean(Method.Utilities[Utility.BANK_PAYMENT_ENABLE]))
            {
                return;
            }
            decimal? bankRemainAmount = DBDataContext.Db.TBL_BANK_PAYMENT_DETAILs
                                        .Where(x => x.BANK != "NA" && !x.IS_VOID && x.RESULT == (int)BankPaymentImportResult.RecordSuccess)
                                        .Sum(s => (decimal?)s.PAY_AMOUNT - (decimal?)s.PAID_AMOUNT);
            if (bankRemainAmount != null && bankRemainAmount > 0)
            {
                if (MsgBox.ShowQuestion(Resources.MS_CUSTOMER_PENDING_PAYMENT, Resources.WARNING) == DialogResult.Yes)
                {
                    new BankPayment.DialogPendingPayment().ShowDialog();
                }
            }
        }


        private void lblExit_Click(object sender, EventArgs e)
        {
            try
            {
                Settings.Default.POINTER_PATH = PointerLogic.PointerPath;
                Settings.Default.Save();
                CRMService.MeterTrackingThread?.Abort();
            }
            catch (Exception) { }
            Application.Exit();
        }

        private void label5_MouseEnter(object sender, EventArgs e)
        {
            this.lblCLOSE_.ForeColor = Color.Red;
        }

        private void label5_MouseLeave(object sender, EventArgs e)
        {
            this.lblCLOSE_.ForeColor = Color.Black;
        }

        private void btnAdmin_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageAdmin), sender);
            this.ads.Show();
        }

        #region showPage
        private Control currentPage = null;
        private Dictionary<Type, Control> pages = new Dictionary<Type, Control>();
        private void showPage(Type type, object sender)
        {
            //Runner.RunNewThread(() =>
            //{
            if (!this.pages.ContainsKey(type))
            {
                this.pages[type] = (Control)Activator.CreateInstance(type);
                this.pages[type].Size = this.main.Size;
                if (this.pages[type] is Form)
                {
                    Form frm = (Form)this.pages[type];
                    frm.TopLevel = false;
                    frm.Visible = true;
                    frm.FormBorderStyle = FormBorderStyle.None;
                }
                this.main.Controls.Add(this.pages[type]);
                this.pages[type].Dock = DockStyle.Fill;

                ResourceHelper.ApplyResource(this.pages[type]);
            }
            if (this.currentPage != this.pages[type])
            {
                this.pages[type].Show();
                if (this.currentPage != null)
                {
                    this.currentPage.Hide();
                }
                this.currentPage = this.pages[type];
            }
            //});

            // this.title.Text = ((Control)sender).Text;
        }
        #endregion ShowPage()

        private void btnBilling_Click(object sender, EventArgs e)
        {
            showPageBilling();
            this.ads.Show();
        }
        private void showPageBilling()
        {
            this.showPage(typeof(PageBilling), this.btnCUSTOMER_BILLING);
            var cp = ((PageBilling)currentPage).currentPage;
            if (cp is PageCustomerList)
            {
                ((PageCustomerList)cp).BindData();
            }
        }

        private void btnPostpaidTasks_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePostPaid), sender);
            this.ads.Show();
        }

        private void btnSecurity_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageSecurity), sender);
            //this.ads.Show();
        }
        private void btnReports_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReport), sender);
            //this.ads.Show();
        }

        private void btnPrepaid_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePrePaid), sender);
            this.ads.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblMinimize_MouseHover(object sender, EventArgs e)
        {
            ((Label)sender).ForeColor = Color.Yellow;
        }

        private void lblMinimize_MouseLeave(object sender, EventArgs e)
        {
            ((Label)sender).ForeColor = Color.Black;
        }

        private void panel11_Click(object sender, EventArgs e)
        {
            new DialogAboutEPower().ShowDialog();
        }

        private void label1_MouseHover(object sender, EventArgs e)
        {
            ((Label)sender).ForeColor = Color.Black;
        }

        private void label1_MouseLeave(object sender, EventArgs e)
        {
            ((Label)sender).ForeColor = Color.DimGray;
        }

        private void label1_Click_1(object sender, EventArgs e)
        {
            Point p = this.lblGENERAL_TASK.Location;
            p.Y = this.lblGENERAL_TASK.Height + 3;
            this.cnmStrip.Show(p);
        }

        private void mnuAbout_Click(object sender, EventArgs e)
        {
            new DialogAboutEPower().ShowDialog();
        }

        private void mnuLogOut_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void mnuChangePassword_Click(object sender, EventArgs e)
        {
            new DialogChangePassword().ShowDialog();
        }

        private void mnuOpenCashDrawer_Click(object sender, EventArgs e)
        {
            new DialogOpenCashDrawer().ShowDialog();
        }

        private void mnuCloseCashDrawer_Click(object sender, EventArgs e)
        {
            new DialogCloseCashDrawer().ShowDialog();
        }

        private void mnuLanguageKH_Click(object sender, EventArgs e)
        {
            Settings.Default.LANGUAGE = "";
            Settings.Default.Save();
            Application.Restart();
        }

        private void mnuLanguageEN_Click(object sender, EventArgs e)
        {
            Settings.Default.LANGUAGE = "en-GB";
            Settings.Default.Save();
            Application.Restart();
        }

        private void cnmStrip_Opening(object sender, CancelEventArgs e)
        {
            this.btnENGLISH_LANGUAGE.Visible = Settings.Default.LANGUAGE.ToLower() != "en-gb";
            this.btnKHMER_LANGUAGE.Visible = Settings.Default.LANGUAGE.ToLower() == "en-gb";
        }

        private void btnPurchasePower_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePower), sender);
            this.ads.Show();
        }

        private void lblClose_Click(object sender, EventArgs e)
        {
            this.ads.Hide();
        }

        private void btnACCOUNTING_Click(object sender, EventArgs e)
        {
            if (!PointerLogic.isConnectedPointer)
            {
                if (PointerLogic.checkServices(true))
                {
                    this.btnACCOUNTING.Selected = false;
                    return;
                }

                if (new LoginDialog(true).DialogResult == DialogResult.OK)
                {
                    PointerLogic.Instance.startHB01();
                    this.showPage(typeof(NavAccounting), sender);
                    this.ads.Hide();
                }
                else
                {
                    if (new LoginDialog(true).ShowDialog() == DialogResult.OK)
                    {
                        PointerLogic.Instance.startHB01();
                        this.showPage(typeof(NavAccounting), sender);
                        this.ads.Hide();
                    }
                }
            }
            else
            {
                this.showPage(typeof(NavAccounting), sender);
                this.ads.Hide();
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            ResourceHelper.ApplyResource(this);
            btnOPEN_CASH_DRAWER.Text = Resources.OPEN_CASH_DRAWER;
            btnCLOSE_CASH_DRAWER.Text = Resources.CLOSE_CASH_DRAWER;
            btnCHANGE_PASSWORD.Text = Resources.CHANGE_PASSWORD;
            btnLOG_OUT.Text = string.Format(Resources.LOG_OUT, Login.CurrentLogin.LOGIN_NAME);
            btnCHANGE_LANGUAGE.Text = Resources.CHANGE_LANGUAGE;
            btnKHMER_LANGUAGE.Text = Resources.KHMER_LANGUAGE;
            btnENGLISH_LANGUAGE.Text = Resources.ENGLISH_LANGUAGE;
            btnABOUT_EPOWER.Text = Resources.ABOUT_E_POWER;

            checkPendingBankPayment();
            checkCashDrawer();
            checkLicenseePendingPayment();
            EBillConnect.CheckUploadRequired();
            PreProcessWithConnectivity();
            NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler(NetworkChange_NetworkAvailabilityChanged);
            //checkCashDrawer();

            this.ads.Show();
        }

        private void pbAds_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                pbAds.ImageLocation = Application.StartupPath + @"\ads\e-power-ads.gif";
            }
        }

        private void BindAcitavtedBank()
        {
            if (!DataHelper.ParseToBoolean(Method.Utilities[Utility.BANK_PAYMENT_ENABLE]))
            {
                return;
            }
            var paymentOperation = new PaymentOperation() { };
            var banks = paymentOperation.GetActivatedBank();
            foreach (var item in DBDataContext.Db.TBL_PAYMENT_CONFIGs)
            {
                var activeBank = banks.FirstOrDefault(x => x.BANK_CODE == item.PAYMENT_TYPE);
                item.IS_ACTIVE = activeBank?.ACTIVATED ?? false;
            }
            DBDataContext.Db.SubmitChanges();
        }
    }
}
