﻿using EPower.Base.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogCustomerInBox : ExDialog
    {
        private DataTable _dtSource = null;
        private int _boxId = 0;
        bool _load = true;
        public DialogCustomerInBox(int boxId)
        {
            InitializeComponent();

            // module "POSITION_IN_BOX_ENABLE" 
            POSITION_IN_BOX.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.POSITION_IN_BOX_ENABLE]);

            _load = true;
            _boxId = boxId;
            dgv.Columns[CUSTOMER_CODE.Name].ReadOnly = true;
            dgv.Columns[CUSTOMER_NAME.Name].ReadOnly = true;
            dgv.Columns[AREA.Name].ReadOnly = true;
            dgv.Columns[METER_CODE.Name].ReadOnly = true;
            dgv.Columns[POSITION_IN_BOX.Name].ReadOnly = false;
            bindData();
            _load = false;
        }

        private void bindData()
        {
            this._dtSource = (from cus in DBDataContext.Db.TBL_CUSTOMERs
                              join area in DBDataContext.Db.TBL_AREAs on cus.AREA_ID equals area.AREA_ID
                              join cm in
                                  (
                                      from m in DBDataContext.Db.TBL_METERs
                                      join cm in DBDataContext.Db.TBL_CUSTOMER_METERs on m.METER_ID equals cm.METER_ID
                                      join b in DBDataContext.Db.TBL_BOXes on cm.BOX_ID equals b.BOX_ID
                                      join p in DBDataContext.Db.TBL_POLEs on b.POLE_ID equals p.POLE_ID
                                      where cm.IS_ACTIVE
                                      select new { cm.CUSTOMER_ID, m.METER_CODE, p.POLE_CODE, b.BOX_CODE, cm.POSITION_IN_BOX, b.BOX_ID, cm.CUS_METER_ID }
                                      ) on cus.CUSTOMER_ID equals cm.CUSTOMER_ID into cmTmp
                              from cmt in cmTmp.DefaultIfEmpty()
                              where cmt.BOX_ID == _boxId
                              orderby cmt.POSITION_IN_BOX, cus.CUSTOMER_CODE
                              select new
                              {
                                  CUSTOMER_ID = cus.CUSTOMER_ID,
                                  CUSTOMER_CODE = cus.CUSTOMER_CODE,
                                  CUSTOMER_NAME = cus.LAST_NAME_KH + " " + cus.FIRST_NAME_KH,
                                  area.AREA_NAME,
                                  cmt.METER_CODE,
                                  cmt.POSITION_IN_BOX,
                                  cmt.CUS_METER_ID
                              })._ToDataTable();

            this.dgv.DataSource = this._dtSource;
        }

        private void dgv_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (_load)
            {
                return;
            }
            if (e.RowIndex == -1 || e.ColumnIndex == -1)
            {
                return;
            }
            if (e.ColumnIndex == dgv.Columns[POSITION_IN_BOX.Name].Index)
            {
                dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = DataHelper.ParseToInt(dgv.Rows[e.RowIndex].Cells[POSITION_IN_BOX.Name].Value.ToString());
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
            {
                foreach (DataGridViewRow item in dgv.Rows)
                {
                    TBL_CUSTOMER_METER objCustomerMeter = DBDataContext.Db.TBL_CUSTOMER_METERs.FirstOrDefault(x => x.CUS_METER_ID == (int)item.Cells[CUS_METER_ID.Name].Value);
                    objCustomerMeter.POSITION_IN_BOX = (int)item.Cells[POSITION_IN_BOX.Name].Value;
                    DBDataContext.Db.SubmitChanges();
                }
                tran.Complete();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            DataGridViewRow dr = dgv.Rows[e.RowIndex];
            if (dr == null)
            {
                return;
            }

            try
            {
                int intTotalFamily = DataHelper.ParseToInt(dr.Cells[POSITION_IN_BOX.Name].Value.ToString());

            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }
    }
}
