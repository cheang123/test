﻿using EPower.Base.Logic;
using EPower.Logic;
using EPower.Properties;
using SoftTech.Component;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogEAC : ExDialog
    {
        EBillConnect _connect = new EBillConnect();
        public DialogEAC()
        {
            InitializeComponent();

            lblSTATUS1.Text = "";
            chkAUTO_SENDING.Checked = Method.GetUtilityValue(Utility.EBILL_SUPPLIER_AUTO_UPLOAD) == "1";
            chkAUTO_SENDING.CheckedChanged += chkAUTO_SENDING_CheckedChanged;
            updateLastUploadDate();
        }

        private void updateLastUploadDate()
        {
            var lastUpload = EBillConnect.GetLastSuccessUpload();
            this.lblLastSuccessUpload_.Text = Resources.LAST_SUCCEED_UPLOAD + (lastUpload == null ? Resources.NOT_FOUND : lastUpload.Value.ToString("dd-MM-yyyy hh:mm tt"));

        }

        private void btnSEND_Click(object sender, EventArgs e)
        {
            using (BackgroundWorker bg = new BackgroundWorker())
            {
                bg.DoWork += delegate (object obj, DoWorkEventArgs arg)
               {
                   try
                   {
                       var pro = _connect.Process();
                       pro.StartInfo.UseShellExecute = false;
                       pro.StartInfo.RedirectStandardOutput = true;
                       pro.Start();
                       btnSEND.Invoke(new MethodInvoker(() =>
                       {
                           btnSEND.Enabled = false;
                       }));

                       while (!pro.StandardOutput.EndOfStream)
                       {
                           if (txtLOG.InvokeRequired)
                           {
                               txtLOG.Invoke(new MethodInvoker(() =>
                               {
                                   txtLOG.AppendText(pro.StandardOutput.ReadLine() + Environment.NewLine);
                               }));
                           }
                       }
                       btnSEND.Invoke(new MethodInvoker(() =>
                       {
                           btnSEND.Enabled = true;
                       }));

                   }
                   catch (Exception exp)
                   {
                       throw new Exception("EAC App error " + exp.Message, exp);
                   }
               };
                bg.RunWorkerCompleted += delegate (object obj, RunWorkerCompletedEventArgs arg)
                {
                    if (arg.Error != null)
                    {
                        MsgBox.ShowError(arg.Error);
                    }
                };
                bg.RunWorkerAsync();
            }

        }





        private void chkAUTO_SENDING_CheckedChanged(object sender, EventArgs e)
        {
            Method.SetUtilityValue(Utility.EBILL_SUPPLIER_AUTO_UPLOAD, chkAUTO_SENDING.Checked ? "1" : "0");
        }

        private void chkSHOW_PROCESS_CheckedChanged(object sender, EventArgs e)
        {
            Size size;
            size = chkSHOW_PROCESS.Checked ? new Size(Width, Height + txtLOG.Height) : new Size(Width, Height - txtLOG.Height);
            this.Size = size;
        }

        private void DialogEAC_Shown(object sender, EventArgs e)
        {
            /*
             * Test server connection.
             */
            Runner.RunNewThread(delegate ()
            {
                lblSTATUS1.Invoke(new MethodInvoker(() =>
                {
                    lblSTATUS1.Text = Resources.E_BILL_CANNOT_CONNECT;
                    lblSTATUS1.ForeColor = Color.Red;
                }));

                if (!_connect.Connect()) return;
                lblSTATUS1.Invoke(new MethodInvoker(() =>
                {
                    lblSTATUS1.Text = Resources.E_BILL_CONNECTED;
                    lblSTATUS1.ForeColor = Color.Blue;
                }));

            });
        }

        private void lblLastSuccessUpload__Click(object sender, EventArgs e)
        {

        }
    }
}