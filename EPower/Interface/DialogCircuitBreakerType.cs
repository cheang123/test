﻿using System;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using EPower.Properties;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using SoftTech.Security.Interface;
using SoftTech.Security.Logic;

namespace EPower.Interface
{
    public partial class DialogCircuitBreakerType : ExDialog
    {
        GeneralProcess _flag;        
        TBL_CIRCUIT_BREAKER_TYPE _objCircuitBrakerType = new TBL_CIRCUIT_BREAKER_TYPE();
        public TBL_CIRCUIT_BREAKER_TYPE CircuitBrakerType
        {
            get { return _objCircuitBrakerType; }
        }
        TBL_CIRCUIT_BREAKER_TYPE _oldObjCircuitBrakerType = new TBL_CIRCUIT_BREAKER_TYPE();

        #region Constructor
        public DialogCircuitBreakerType(GeneralProcess flag, TBL_CIRCUIT_BREAKER_TYPE objCircuitBrakerType)
        {
            InitializeComponent();
            btnAddAmpare.Enabled = Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_AMPARE);
            btnAddContant.Enabled = Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_CONTANT);
            btnAddPhase.Enabled = Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_PHASE);
            btnAddPhase.Enabled = false;
            btnAddVoltage.Enabled = Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_VOLTAGE);
            btnAddVoltage.Enabled = false;

            _flag = flag;

            dataLookUp();
            objCircuitBrakerType._CopyTo(_objCircuitBrakerType);
            objCircuitBrakerType._CopyTo(_oldObjCircuitBrakerType);

            if (flag == GeneralProcess.Insert)
            {
                _objCircuitBrakerType.IS_ACTIVE = true;
                this.Text = string.Concat(Resources.INSERT, this.Text);
            }
            else if (flag == GeneralProcess.Update)
            {
                this.Text = string.Concat(Resources.UPDATE, this.Text);
            }
            else if (flag == GeneralProcess.Delete)
            {
                this.Text = string.Concat(Resources.DELETE, this.Text);
                UIHelper.SetEnabled(this, false);
            }
            this.btnCHANGE_LOG.Visible = this._flag != GeneralProcess.Insert;
            read();
        }
        #endregion

        #region Method
        private void read()
        {
            
            txtBrakerTypeCode.Text = _objCircuitBrakerType.BREAKER_TYPE_CODE;
            txtBrakerTypeName.Text = _objCircuitBrakerType.BREAKER_TYPE_NAME;
            cboConstant.SelectedValue = _objCircuitBrakerType.BREAKER_CONST_ID;
            cboPhase.SelectedValue = _objCircuitBrakerType.BREAKER_PHASE_ID;
            cboVoltage.SelectedValue = _objCircuitBrakerType.BREAKER_VOL_ID;
            cboAmpera.SelectedValue = _objCircuitBrakerType.BREAKER_AMP_ID;            
        }

        private void write()
        {
            _objCircuitBrakerType.BREAKER_TYPE_CODE = txtBrakerTypeCode.Text.Trim();
            _objCircuitBrakerType.BREAKER_TYPE_NAME = txtBrakerTypeName.Text.Trim();
            _objCircuitBrakerType.BREAKER_CONST_ID = (int)cboConstant.SelectedValue;
            _objCircuitBrakerType.BREAKER_PHASE_ID = (int)cboPhase.SelectedValue;
            _objCircuitBrakerType.BREAKER_VOL_ID = (int)cboVoltage.SelectedValue;
            _objCircuitBrakerType.BREAKER_AMP_ID = (int)cboAmpera.SelectedValue;
            
        }

        private void dataLookUp()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                //Get constant.
                bindConstant();
                //Get phase.
                bindPhase();
                //Get voltage.
                bindVoltage();
                //Get ampare.
                bindAmpera();
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
            this.Cursor = Cursors.Default;
        }

        private void bindAmpera()
        {
            UIHelper.SetDataSourceToComboBox(cboAmpera, DBDataContext.Db.TBL_AMPAREs.ToList().OrderBy(x => Base.Logic.Lookup.ParseAmpare(x.AMPARE_NAME)));

        }

        private void bindVoltage()
        {
            UIHelper.SetDataSourceToComboBox(cboVoltage,
                from v in DBDataContext.Db.TBL_VOLTAGEs
                where v.IS_ACTIVE && v.VOLTAGE_ID>0
                orderby v.VOLTAGE_NAME
                select new
                {
                    v.VOLTAGE_ID,
                    v.VOLTAGE_NAME
                }, "VOLTAGE_ID", "VOLTAGE_NAME");
        }

        private void bindPhase()
        {
            UIHelper.SetDataSourceToComboBox(cboPhase,
                from p in DBDataContext.Db.TBL_PHASEs
                where p.IS_ACTIVE
                orderby p.PHASE_NAME
                select new
                {
                    p.PHASE_ID,
                    p.PHASE_NAME
                }, "PHASE_ID", "PHASE_NAME");
        }

        private void bindConstant()
        {
            UIHelper.SetDataSourceToComboBox(cboConstant,
                from c in DBDataContext.Db.TBL_CONSTANTs
                where c.IS_ACTIVE
                orderby c.CONSTANT_NAME
                select new
                {
                    c.CONSTANT_ID,
                    c.CONSTANT_NAME
                }, "CONSTANT_ID", "CONSTANT_NAME");
        }

        private bool inValid()
        {
            bool val = false;
            this.ClearAllValidation();

            if (txtBrakerTypeCode.Text.Trim() == string.Empty)
            {
                txtBrakerTypeCode.SetValidation(string.Format(Resources.REQUIRED, lblBREAKER_TYPE_CODE.Text));
                val = true;
            }
            if (txtBrakerTypeName.Text.Trim() == string.Empty)
            {
                txtBrakerTypeName.SetValidation(string.Format(Resources.REQUIRED, lblBREAKER_TYPE_NAME.Text));
                val = true;
            }            
            if (cboConstant.SelectedIndex == -1)
            {
                cboConstant.SetValidation(string.Format(Resources.REQUIRED, lblCONSTANT.Text));
                val = true;
            }
            if (cboPhase.SelectedIndex == -1)
            {
                cboPhase.SetValidation(string.Format(Resources.REQUIRED, lblPHASE.Text));
                val = true;
            }
            if (cboVoltage.SelectedIndex == -1)
            {
                cboVoltage.SetValidation(string.Format(Resources.REQUIRED, lblVOLTAGE.Text));
                val = true;
            }
            if (cboVoltage.SelectedIndex == -1)
            {
                cboVoltage.SetValidation(string.Format(Resources.REQUIRED, lblVOLTAGE.Text));
                val = true;
            }
            if (cboAmpera.SelectedIndex == -1)
            {
                cboAmpera.SetValidation(string.Format(Resources.REQUIRED, lblAMPARE.Text));
                val = true;
            }

            return val;
        } 
        #endregion

        #region Operation
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (inValid())
            {
                return;
            }
            write();
            txtBrakerTypeCode.ClearValidation();
            if (DBDataContext.Db.IsExits(_objCircuitBrakerType, "BREAKER_TYPE_CODE"))
            {
                txtBrakerTypeCode.SetValidation(string.Format(Resources.MS_IS_EXISTS, lblBREAKER_TYPE_CODE.Text));
                return;
            }
            try
            {

                using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                {
                    if (_flag == GeneralProcess.Insert)
                    {
                        DBDataContext.Db.Insert(_objCircuitBrakerType);
                    }
                    else if (_flag == GeneralProcess.Update)
                    {
                        DBDataContext.Db.Update(_oldObjCircuitBrakerType, _objCircuitBrakerType);
                    }
                    else if (_flag == GeneralProcess.Delete)
                    {
                        DBDataContext.Db.Delete(_objCircuitBrakerType);
                    }
                    tran.Complete();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                MsgBox.ShowError(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Change current keyboard layout to khmer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeKhmerKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }
        /// <summary>
        /// Change current keyboard layout to engilsh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeEngilshKeyboard(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }
        
        #endregion                

        private void btnChangelog_Click(object sender, EventArgs e)
        {
            DialogChangeLog.ShowChangeLog(this._objCircuitBrakerType);
        }

        private void btnAddContant_AddItem(object sender, EventArgs e)
        {
            DialogConstant dig = new DialogConstant(GeneralProcess.Insert, new TBL_CONSTANT());
            if (dig.ShowDialog()== DialogResult.OK)
            {
                bindConstant();
                cboConstant.SelectedValue = dig.Constant.CONSTANT_ID;
            }
        }

        private void btnAddPhase_AddItem(object sender, EventArgs e)
        {
            DialogPhase dig = new DialogPhase(GeneralProcess.Insert, new TBL_PHASE());
            if (dig.ShowDialog()== DialogResult.OK)
            {
                bindPhase();
                cboPhase.SelectedValue = dig.Phase.PHASE_ID;
            }
        }

        private void btnAddVoltage_AddItem(object sender, EventArgs e)
        {
            DialogVoltage dig = new DialogVoltage(new TBL_VOLTAGE(), GeneralProcess.Insert);
            if (dig.ShowDialog()== DialogResult.OK)
            {
                bindVoltage();
                cboVoltage.SelectedValue = dig.Voltage.VOLTAGE_ID;
            }

        }

        private void btnAddAmpare_AddItem(object sender, EventArgs e)
        {
            DialogAmpare dig = new DialogAmpare(GeneralProcess.Insert, new TBL_AMPARE());
            if (dig.ShowDialog()== DialogResult.OK)
            {
                bindAmpera();
                cboAmpera.SelectedValue = dig.Ampare.AMPARE_ID;
            }
        }
    }
}
