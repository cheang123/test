﻿using EPower.Base.Helper.Component;

namespace EPower.Interface
{
    partial class PageReportSoldByCustomerType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraTreeList.StyleFormatConditions.TreeListFormatRule treeListFormatRule1 = new DevExpress.XtraTreeList.StyleFormatConditions.TreeListFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleContains formatConditionRuleContains1 = new DevExpress.XtraEditors.FormatConditionRuleContains();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportSoldByCustomerType));
            DevExpress.XtraTreeList.StyleFormatConditions.TreeListFormatRule treeListFormatRule2 = new DevExpress.XtraTreeList.StyleFormatConditions.TreeListFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleContains formatConditionRuleContains2 = new DevExpress.XtraEditors.FormatConditionRuleContains();
            DevExpress.XtraTreeList.StyleFormatConditions.TreeListFormatRule treeListFormatRule3 = new DevExpress.XtraTreeList.StyleFormatConditions.TreeListFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleContains formatConditionRuleContains3 = new DevExpress.XtraEditors.FormatConditionRuleContains();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCYCLE_NAME = new System.Windows.Forms.Label();
            this.cboBillingCycle = new System.Windows.Forms.ComboBox();
            this.rdpDate = new EPower.Base.Helper.Component.ReportDatePicker();
            this.btnMail = new SoftTech.Component.ExButton();
            this.btnViewReport = new SoftTech.Component.ExButton();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.popupExport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.btnExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnCsv = new DevExpress.XtraBars.BarButtonItem();
            this.btnPdf = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.popActions = new DevExpress.XtraBars.PopupMenu(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this._lblSubTitle2 = new System.Windows.Forms.Label();
            this._lblSubTitle1 = new System.Windows.Forms.Label();
            this._lblTitle = new System.Windows.Forms.Label();
            this._lblAddress = new System.Windows.Forms.Label();
            this._lblCompany = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.dpExport = new DevExpress.XtraEditors.DropDownButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this._picLogo = new System.Windows.Forms.PictureBox();
            this.tgvCustomerType = new DevExpress.XtraTreeList.TreeList();
            this.colCUSTOMER_CONNECTION_TYPE_NAME = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTOTAL_CUSTOMER = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colKWH_POWER = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colKVAR_POWER = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colKWH_AMOUNT = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colKVAR_AMOUNT = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colCAPACITY_CHARGE_AMOUNT = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colTOTAL_AMOUNT = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colFORMAT = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._picLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tgvCustomerType)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = global::EPower.Properties.Resources.dock;
            this.panel1.Controls.Add(this.lblCYCLE_NAME);
            this.panel1.Controls.Add(this.cboBillingCycle);
            this.panel1.Controls.Add(this.rdpDate);
            this.panel1.Controls.Add(this.btnMail);
            this.panel1.Controls.Add(this.btnViewReport);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1205, 44);
            this.panel1.TabIndex = 1;
            // 
            // lblCYCLE_NAME
            // 
            this.lblCYCLE_NAME.AutoSize = true;
            this.lblCYCLE_NAME.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCYCLE_NAME.Location = new System.Drawing.Point(193, 11);
            this.lblCYCLE_NAME.Margin = new System.Windows.Forms.Padding(2);
            this.lblCYCLE_NAME.Name = "lblCYCLE_NAME";
            this.lblCYCLE_NAME.Size = new System.Drawing.Size(45, 19);
            this.lblCYCLE_NAME.TabIndex = 22;
            this.lblCYCLE_NAME.Text = "ជុំទូទាត់";
            // 
            // cboBillingCycle
            // 
            this.cboBillingCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBillingCycle.FormattingEnabled = true;
            this.cboBillingCycle.Location = new System.Drawing.Point(244, 7);
            this.cboBillingCycle.Margin = new System.Windows.Forms.Padding(2);
            this.cboBillingCycle.Name = "cboBillingCycle";
            this.cboBillingCycle.Size = new System.Drawing.Size(176, 27);
            this.cboBillingCycle.TabIndex = 21;
            this.cboBillingCycle.SelectedIndexChanged += new System.EventHandler(this.cboBillingCycle_SelectedIndexChanged);
            // 
            // rdpDate
            // 
            this.rdpDate.AutoSave = true;
            this.rdpDate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.rdpDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rdpDate.DateFilterTypes = EPower.Base.Helper.Component.ReportDatePicker.DateFilterType.Period;
            this.rdpDate.DefaultValue = EPower.Base.Helper.Component.ReportDatePicker.DateRanges.LastMonth;
            this.rdpDate.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.rdpDate.FromDate = new System.DateTime(2022, 5, 1, 0, 0, 0, 0);
            this.rdpDate.LastCustomDays = 30;
            this.rdpDate.LastFinancialYear = true;
            this.rdpDate.LastMonth = true;
            this.rdpDate.LastQuarter = true;
            this.rdpDate.LastWeek = true;
            this.rdpDate.Location = new System.Drawing.Point(4, 7);
            this.rdpDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rdpDate.Name = "rdpDate";
            this.rdpDate.Size = new System.Drawing.Size(173, 27);
            this.rdpDate.TabIndex = 20;
            this.rdpDate.ThisFinancialYear = true;
            this.rdpDate.ThisMonth = true;
            this.rdpDate.ThisQuarter = true;
            this.rdpDate.ThisWeek = true;
            this.rdpDate.ToDate = new System.DateTime(2022, 5, 31, 23, 59, 59, 0);
            this.rdpDate.Today = true;
            this.rdpDate.Value = EPower.Base.Helper.Component.ReportDatePicker.DateRanges.LastMonth;
            this.rdpDate.Yesterday = true;
            this.rdpDate.ValueChanged += new System.EventHandler(this.rdpDate_ValueChanged);
            // 
            // btnMail
            // 
            this.btnMail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMail.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnMail.Location = new System.Drawing.Point(1128, 9);
            this.btnMail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnMail.Name = "btnMail";
            this.btnMail.Size = new System.Drawing.Size(71, 23);
            this.btnMail.TabIndex = 13;
            this.btnMail.Text = "អ៊ីមែល";
            this.btnMail.UseVisualStyleBackColor = true;
            this.btnMail.Visible = false;
            this.btnMail.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // btnViewReport
            // 
            this.btnViewReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewReport.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnViewReport.Location = new System.Drawing.Point(1052, 9);
            this.btnViewReport.Margin = new System.Windows.Forms.Padding(2);
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.Size = new System.Drawing.Size(71, 23);
            this.btnViewReport.TabIndex = 5;
            this.btnViewReport.Text = "មើល";
            this.btnViewReport.UseVisualStyleBackColor = true;
            this.btnViewReport.Visible = false;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl1.Location = new System.Drawing.Point(1205, 0);
            this.barDockControl1.Manager = null;
            this.barDockControl1.Size = new System.Drawing.Size(0, 699);
            // 
            // popupExport
            // 
            this.popupExport.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCsv),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPdf),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPrint, true)});
            this.popupExport.Manager = this.barManager1;
            this.popupExport.Name = "popupExport";
            // 
            // btnExcel
            // 
            this.btnExcel.Caption = "Excel";
            this.btnExcel.Id = 3;
            this.btnExcel.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Normal.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnExcel.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnExcel.Name = "btnExcel";
            // 
            // btnCsv
            // 
            this.btnCsv.Caption = "Csv";
            this.btnCsv.Id = 5;
            this.btnCsv.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Normal.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnCsv.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnCsv.Name = "btnCsv";
            // 
            // btnPdf
            // 
            this.btnPdf.Caption = "Pdf";
            this.btnPdf.Id = 2;
            this.btnPdf.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPdf.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPdf.Name = "btnPdf";
            // 
            // btnPrint
            // 
            this.btnPrint.Caption = "​បោះពុម្ព";
            this.btnPrint.Id = 4;
            this.btnPrint.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPrint.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPrint.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPrint.Name = "btnPrint";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnExcel,
            this.btnCsv,
            this.btnPdf,
            this.btnPrint,
            this.barButtonItem1});
            this.barManager1.MaxItemId = 8;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1205, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 699);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1205, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 699);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl2.Location = new System.Drawing.Point(1205, 0);
            this.barDockControl2.Manager = this.barManager1;
            this.barDockControl2.Size = new System.Drawing.Size(0, 699);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Id = 6;
            this.barButtonItem1.ItemAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem1.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem1.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem1.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.barButtonItem1.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // popActions
            // 
            this.popActions.Manager = this.barManager1;
            this.popActions.Name = "popActions";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this._lblSubTitle2);
            this.panel2.Controls.Add(this._lblSubTitle1);
            this.panel2.Controls.Add(this._lblTitle);
            this.panel2.Controls.Add(this._lblAddress);
            this.panel2.Controls.Add(this._lblCompany);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 44);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1205, 146);
            this.panel2.TabIndex = 143;
            // 
            // _lblSubTitle2
            // 
            this._lblSubTitle2.BackColor = System.Drawing.Color.White;
            this._lblSubTitle2.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblSubTitle2.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblSubTitle2.Font = new System.Drawing.Font("Kh Siemreap", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblSubTitle2.Location = new System.Drawing.Point(129, 114);
            this._lblSubTitle2.Name = "_lblSubTitle2";
            this._lblSubTitle2.Size = new System.Drawing.Size(929, 27);
            this._lblSubTitle2.TabIndex = 156;
            this._lblSubTitle2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _lblSubTitle1
            // 
            this._lblSubTitle1.BackColor = System.Drawing.Color.White;
            this._lblSubTitle1.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblSubTitle1.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblSubTitle1.Font = new System.Drawing.Font("Kh Siemreap", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblSubTitle1.Location = new System.Drawing.Point(129, 87);
            this._lblSubTitle1.Name = "_lblSubTitle1";
            this._lblSubTitle1.Size = new System.Drawing.Size(929, 27);
            this._lblSubTitle1.TabIndex = 155;
            this._lblSubTitle1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _lblTitle
            // 
            this._lblTitle.BackColor = System.Drawing.Color.White;
            this._lblTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblTitle.Font = new System.Drawing.Font("Kh Muol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblTitle.Location = new System.Drawing.Point(129, 60);
            this._lblTitle.Name = "_lblTitle";
            this._lblTitle.Size = new System.Drawing.Size(929, 27);
            this._lblTitle.TabIndex = 154;
            this._lblTitle.Text = "របាយការណ៍លក់តាមប្រភេទអតិថិជន";
            this._lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _lblAddress
            // 
            this._lblAddress.BackColor = System.Drawing.Color.White;
            this._lblAddress.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblAddress.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblAddress.Font = new System.Drawing.Font("Kh Siemreap", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblAddress.Location = new System.Drawing.Point(129, 30);
            this._lblAddress.Name = "_lblAddress";
            this._lblAddress.Size = new System.Drawing.Size(929, 30);
            this._lblAddress.TabIndex = 153;
            this._lblAddress.Text = "អាស័យដ្ឋានក្រុមហ៊ុន";
            this._lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _lblCompany
            // 
            this._lblCompany.BackColor = System.Drawing.Color.White;
            this._lblCompany.Cursor = System.Windows.Forms.Cursors.Hand;
            this._lblCompany.Dock = System.Windows.Forms.DockStyle.Top;
            this._lblCompany.Font = new System.Drawing.Font("Kh Muol", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._lblCompany.Location = new System.Drawing.Point(129, 0);
            this._lblCompany.Name = "_lblCompany";
            this._lblCompany.Size = new System.Drawing.Size(929, 30);
            this._lblCompany.TabIndex = 152;
            this._lblCompany.Text = "ឈ្មោះក្រុមហ៊ុន";
            this._lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.flowLayoutPanel3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1058, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(147, 146);
            this.panel4.TabIndex = 143;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel3.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel3.Controls.Add(this.dpExport);
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 109);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(144, 32);
            this.flowLayoutPanel3.TabIndex = 151;
            // 
            // dpExport
            // 
            this.dpExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dpExport.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dpExport.Appearance.Options.UseBackColor = true;
            this.dpExport.Appearance.Options.UseBorderColor = true;
            this.dpExport.Appearance.Options.UseFont = true;
            this.dpExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dpExport.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dpExport.DropDownControl = this.popupExport;
            this.dpExport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dpExport.ImageOptions.Image")));
            this.dpExport.ImageOptions.ImageUri.Uri = "SendCSV;Size32x32;GrayScaled";
            this.dpExport.ImageOptions.SvgImageSize = new System.Drawing.Size(10, 10);
            this.dpExport.Location = new System.Drawing.Point(100, 1);
            this.dpExport.LookAndFeel.SkinMaskColor = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dpExport.Margin = new System.Windows.Forms.Padding(1);
            this.dpExport.Name = "dpExport";
            this.dpExport.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            this.dpExport.Size = new System.Drawing.Size(43, 26);
            this.dpExport.TabIndex = 19;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this._picLogo);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(129, 146);
            this.panel3.TabIndex = 143;
            // 
            // _picLogo
            // 
            this._picLogo.Location = new System.Drawing.Point(12, 7);
            this._picLogo.Name = "_picLogo";
            this._picLogo.Size = new System.Drawing.Size(111, 111);
            this._picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this._picLogo.TabIndex = 0;
            this._picLogo.TabStop = false;
            // 
            // tgvCustomerType
            // 
            this.tgvCustomerType.Appearance.BandPanel.Font = new System.Drawing.Font("Kh Siemreap", 9F);
            this.tgvCustomerType.Appearance.BandPanel.Options.UseFont = true;
            this.tgvCustomerType.Appearance.Caption.Font = new System.Drawing.Font("Kh Siemreap", 9F);
            this.tgvCustomerType.Appearance.Caption.Options.UseFont = true;
            this.tgvCustomerType.Appearance.Caption.Options.UseTextOptions = true;
            this.tgvCustomerType.Appearance.Caption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tgvCustomerType.Appearance.HeaderPanel.Font = new System.Drawing.Font("Kh Siemreap", 9F);
            this.tgvCustomerType.Appearance.HeaderPanel.Options.UseFont = true;
            this.tgvCustomerType.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.tgvCustomerType.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tgvCustomerType.Appearance.Row.Font = new System.Drawing.Font("Kh Siemreap", 9F);
            this.tgvCustomerType.Appearance.Row.Options.UseFont = true;
            this.tgvCustomerType.Appearance.SelectedRow.Font = new System.Drawing.Font("Kh Siemreap", 9F);
            this.tgvCustomerType.Appearance.SelectedRow.Options.UseFont = true;
            this.tgvCustomerType.AppearancePrint.Caption.Font = new System.Drawing.Font("Kh Siemreap", 9F);
            this.tgvCustomerType.AppearancePrint.Caption.Options.UseFont = true;
            this.tgvCustomerType.AppearancePrint.Row.Font = new System.Drawing.Font("Kh Siemreap", 9F);
            this.tgvCustomerType.AppearancePrint.Row.Options.UseFont = true;
            this.tgvCustomerType.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colCUSTOMER_CONNECTION_TYPE_NAME,
            this.colTOTAL_CUSTOMER,
            this.colKWH_POWER,
            this.colKVAR_POWER,
            this.colKWH_AMOUNT,
            this.colKVAR_AMOUNT,
            this.colCAPACITY_CHARGE_AMOUNT,
            this.colTOTAL_AMOUNT,
            this.colFORMAT});
            this.tgvCustomerType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tgvCustomerType.Font = new System.Drawing.Font("Kh Siemreap", 9F);
            treeListFormatRule1.ApplyToRow = true;
            treeListFormatRule1.Name = "StatusPending";
            formatConditionRuleContains1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleContains1.Appearance.Options.UseForeColor = true;
            formatConditionRuleContains1.Values = ((System.Collections.IList)(resources.GetObject("formatConditionRuleContains1.Values")));
            treeListFormatRule1.Rule = formatConditionRuleContains1;
            treeListFormatRule2.ApplyToRow = true;
            treeListFormatRule2.Name = "StatusDisconnect";
            formatConditionRuleContains2.Appearance.ForeColor = System.Drawing.Color.Orange;
            formatConditionRuleContains2.Appearance.Options.UseForeColor = true;
            formatConditionRuleContains2.Values = ((System.Collections.IList)(resources.GetObject("formatConditionRuleContains2.Values")));
            treeListFormatRule2.Rule = formatConditionRuleContains2;
            treeListFormatRule3.ApplyToRow = true;
            treeListFormatRule3.Name = "StatusClosed";
            formatConditionRuleContains3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleContains3.Appearance.Options.UseForeColor = true;
            formatConditionRuleContains3.Values = ((System.Collections.IList)(resources.GetObject("formatConditionRuleContains3.Values")));
            treeListFormatRule3.Rule = formatConditionRuleContains3;
            this.tgvCustomerType.FormatRules.Add(treeListFormatRule1);
            this.tgvCustomerType.FormatRules.Add(treeListFormatRule2);
            this.tgvCustomerType.FormatRules.Add(treeListFormatRule3);
            this.tgvCustomerType.KeyFieldName = "RenderId";
            this.tgvCustomerType.Location = new System.Drawing.Point(0, 190);
            this.tgvCustomerType.Name = "tgvCustomerType";
            this.tgvCustomerType.OptionsBehavior.AutoPopulateColumns = false;
            this.tgvCustomerType.OptionsBehavior.AutoScrollOnSorting = false;
            this.tgvCustomerType.OptionsBehavior.Editable = false;
            this.tgvCustomerType.OptionsBehavior.PopulateServiceColumns = true;
            this.tgvCustomerType.OptionsCustomization.AllowSort = false;
            this.tgvCustomerType.OptionsPrint.PrintReportFooter = false;
            this.tgvCustomerType.OptionsView.ShowIndicator = false;
            this.tgvCustomerType.OptionsView.ShowTreeLines = DevExpress.Utils.DefaultBoolean.False;
            this.tgvCustomerType.Padding = new System.Windows.Forms.Padding(1);
            this.tgvCustomerType.ParentFieldName = "ParentId";
            this.tgvCustomerType.Size = new System.Drawing.Size(1205, 509);
            this.tgvCustomerType.TabIndex = 150;
            // 
            // colCUSTOMER_CONNECTION_TYPE_NAME
            // 
            this.colCUSTOMER_CONNECTION_TYPE_NAME.AppearanceCell.Options.UseTextOptions = true;
            this.colCUSTOMER_CONNECTION_TYPE_NAME.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colCUSTOMER_CONNECTION_TYPE_NAME.Caption = "ប្រភេទនៃការលក់";
            this.colCUSTOMER_CONNECTION_TYPE_NAME.FieldName = "CUSTOMER_CONNECTION_TYPE_NAME";
            this.colCUSTOMER_CONNECTION_TYPE_NAME.Name = "colCUSTOMER_CONNECTION_TYPE_NAME";
            this.colCUSTOMER_CONNECTION_TYPE_NAME.Visible = true;
            this.colCUSTOMER_CONNECTION_TYPE_NAME.VisibleIndex = 0;
            this.colCUSTOMER_CONNECTION_TYPE_NAME.Width = 564;
            // 
            // colTOTAL_CUSTOMER
            // 
            this.colTOTAL_CUSTOMER.AppearanceCell.Options.UseTextOptions = true;
            this.colTOTAL_CUSTOMER.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOTAL_CUSTOMER.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTOTAL_CUSTOMER.AppearanceHeader.Options.UseTextOptions = true;
            this.colTOTAL_CUSTOMER.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTOTAL_CUSTOMER.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTOTAL_CUSTOMER.Caption = "ចំនួនអតិថិជន";
            this.colTOTAL_CUSTOMER.FieldName = "TOTAL_CUSTOMER";
            this.colTOTAL_CUSTOMER.Format.FormatString = "#,#0";
            this.colTOTAL_CUSTOMER.Format.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colTOTAL_CUSTOMER.Name = "colTOTAL_CUSTOMER";
            this.colTOTAL_CUSTOMER.Visible = true;
            this.colTOTAL_CUSTOMER.VisibleIndex = 1;
            this.colTOTAL_CUSTOMER.Width = 85;
            // 
            // colKWH_POWER
            // 
            this.colKWH_POWER.AppearanceCell.Options.UseTextOptions = true;
            this.colKWH_POWER.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKWH_POWER.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colKWH_POWER.AppearanceHeader.Options.UseTextOptions = true;
            this.colKWH_POWER.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKWH_POWER.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colKWH_POWER.Caption = "ថាមពលលក់ (kWh Sold)";
            this.colKWH_POWER.FieldName = "KWH_USAGE";
            this.colKWH_POWER.Format.FormatString = "#,##0";
            this.colKWH_POWER.Format.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colKWH_POWER.Name = "colKWH_POWER";
            this.colKWH_POWER.Visible = true;
            this.colKWH_POWER.VisibleIndex = 2;
            this.colKWH_POWER.Width = 86;
            // 
            // colKVAR_POWER
            // 
            this.colKVAR_POWER.AppearanceCell.Options.UseTextOptions = true;
            this.colKVAR_POWER.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKVAR_POWER.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colKVAR_POWER.AppearanceHeader.Options.UseTextOptions = true;
            this.colKVAR_POWER.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKVAR_POWER.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colKVAR_POWER.Caption = "ថាមពលលក់ (kVar Sold)";
            this.colKVAR_POWER.FieldName = "KVAR_USAGE";
            this.colKVAR_POWER.Format.FormatString = "#,##0";
            this.colKVAR_POWER.Format.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colKVAR_POWER.Name = "colKVAR_POWER";
            this.colKVAR_POWER.Visible = true;
            this.colKVAR_POWER.VisibleIndex = 3;
            this.colKVAR_POWER.Width = 86;
            // 
            // colKWH_AMOUNT
            // 
            this.colKWH_AMOUNT.Caption = "ទឹកប្រាក់ថាមពលលក់ (kWh)";
            this.colKWH_AMOUNT.FieldName = "KWH_AMOUNT";
            this.colKWH_AMOUNT.Name = "colKWH_AMOUNT";
            this.colKWH_AMOUNT.Visible = true;
            this.colKWH_AMOUNT.VisibleIndex = 4;
            this.colKWH_AMOUNT.Width = 103;
            // 
            // colKVAR_AMOUNT
            // 
            this.colKVAR_AMOUNT.Caption = "ទឹកប្រាក់ថាមពលលក់ (kVar)";
            this.colKVAR_AMOUNT.FieldName = "KVAR_AMOUNT";
            this.colKVAR_AMOUNT.Name = "colKVAR_AMOUNT";
            this.colKVAR_AMOUNT.Visible = true;
            this.colKVAR_AMOUNT.VisibleIndex = 5;
            this.colKVAR_AMOUNT.Width = 129;
            // 
            // colCAPACITY_CHARGE_AMOUNT
            // 
            this.colCAPACITY_CHARGE_AMOUNT.Caption = "CAPACITY_CHARGE";
            this.colCAPACITY_CHARGE_AMOUNT.FieldName = "CAPACITY_CHARGE";
            this.colCAPACITY_CHARGE_AMOUNT.Name = "colCAPACITY_CHARGE_AMOUNT";
            this.colCAPACITY_CHARGE_AMOUNT.Visible = true;
            this.colCAPACITY_CHARGE_AMOUNT.VisibleIndex = 6;
            // 
            // colTOTAL_AMOUNT
            // 
            this.colTOTAL_AMOUNT.Caption = "ចំនួនទឹកប្រាក់";
            this.colTOTAL_AMOUNT.FieldName = "TOTAL_AMOUNT";
            this.colTOTAL_AMOUNT.Name = "colTOTAL_AMOUNT";
            this.colTOTAL_AMOUNT.Visible = true;
            this.colTOTAL_AMOUNT.VisibleIndex = 7;
            this.colTOTAL_AMOUNT.Width = 150;
            // 
            // colFORMAT
            // 
            this.colFORMAT.Caption = "FORMAT";
            this.colFORMAT.FieldName = "FORMAT";
            this.colFORMAT.Name = "colFORMAT";
            // 
            // PageReportSoldByCustomerType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1205, 699);
            this.Controls.Add(this.tgvCustomerType);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.barDockControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PageReportSoldByCustomerType";
            this.Text = "PageSellCustomerType";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._picLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tgvCustomerType)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private SoftTech.Component.ExButton btnMail;
        private SoftTech.Component.ExButton btnViewReport;
        private ReportDatePicker rdpDate;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarButtonItem btnExcel;
        private DevExpress.XtraBars.BarButtonItem btnCsv;
        private DevExpress.XtraBars.BarButtonItem btnPdf;
        private DevExpress.XtraBars.BarButtonItem btnPrint;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.PopupMenu popupExport;
        private DevExpress.XtraBars.PopupMenu popActions;
        private DevExpress.XtraTreeList.TreeList tgvCustomerType;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCUSTOMER_CONNECTION_TYPE_NAME;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTOTAL_CUSTOMER;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colKWH_POWER;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colKVAR_POWER;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colKWH_AMOUNT;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colKVAR_AMOUNT;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colCAPACITY_CHARGE_AMOUNT;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colTOTAL_AMOUNT;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colFORMAT;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label _lblSubTitle2;
        private System.Windows.Forms.Label _lblSubTitle1;
        private System.Windows.Forms.Label _lblTitle;
        private System.Windows.Forms.Label _lblAddress;
        private System.Windows.Forms.Label _lblCompany;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.DropDownButton dpExport;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox _picLogo;
        private System.Windows.Forms.ComboBox cboBillingCycle;
        private System.Windows.Forms.Label lblCYCLE_NAME;
    }
}