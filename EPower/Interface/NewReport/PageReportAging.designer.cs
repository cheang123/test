﻿using EPower.Base.Helper.Component;
using System.ComponentModel;
using System.Windows.Forms;

namespace EPower.Interface.NewReport
{
    partial class PageReportAging
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageReportAging));
            this.colCURRENCY = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pnContainer = new System.Windows.Forms.Panel();
            this.pgvAgingReport = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.colCUSTOMER_CODE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colCUSTOMER_NAME = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPHONE_NUMBER = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colAREA = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPOLE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colBOX = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colNOT_ON_DUE_DATE = new DevExpress.XtraPivotGrid.PivotGridField();
            this._colP1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this._colP2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this._colP3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this._colP4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this._colP5 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colAMOUNT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colTOTAL_DUE_AMOUNT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colPAID_AMOUNT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colINVOICE_NO = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colINVOICE_DATE_BILLING = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colINVOICE_TITLE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colMETER_CODE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colSTATUS = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colOPEN_DAYS = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colCUSTOMER_CONNECTION_TYPE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colCUSTOMER_GROUP = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colCYCLE_NAME = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colINVOICE_ITEM = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colINVOICE_ITEM_TYPE = new DevExpress.XtraPivotGrid.PivotGridField();
            this.colROW_NO = new DevExpress.XtraPivotGrid.PivotGridField();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.dpExport = new DevExpress.XtraEditors.DropDownButton();
            this.popupExport = new DevExpress.XtraBars.PopupMenu(this.components);
            this.btnExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btnCsv = new DevExpress.XtraBars.BarButtonItem();
            this.btnPdf = new DevExpress.XtraBars.BarButtonItem();
            this.bntRaw_Data = new DevExpress.XtraBars.BarButtonItem();
            this.btnPRINT = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.btnResetToDefault = new DevExpress.XtraBars.BarButtonItem();
            this.btnSAVE_TEMPLATE = new DevExpress.XtraBars.BarButtonItem();
            this.dpAction = new DevExpress.XtraEditors.DropDownButton();
            this.popActions = new DevExpress.XtraBars.PopupMenu(this.components);
            this._lblSubTitle1 = new System.Windows.Forms.Label();
            this._lblTitle = new System.Windows.Forms.Label();
            this._lblAddress = new System.Windows.Forms.Label();
            this._lblCompany = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblDAY = new System.Windows.Forms.Label();
            this.nudDays = new System.Windows.Forms.NumericUpDown();
            this.rdpDate = new EPower.Base.Helper.Component.ReportDatePicker();
            this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField3 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField4 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField5 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField6 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField7 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField8 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField9 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField10 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField12 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField13 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField14 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField15 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField16 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField17 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField18 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField19 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField20 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField22 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField23 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField24 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotGridField25 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pnContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pgvAgingReport)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDays)).BeginInit();
            this.SuspendLayout();
            // 
            // colCURRENCY
            // 
            this.colCURRENCY.Appearance.Cell.Options.UseTextOptions = true;
            this.colCURRENCY.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCURRENCY.Appearance.CellGrandTotal.BackColor2 = ((System.Drawing.Color)(resources.GetObject("colCURRENCY.Appearance.CellGrandTotal.BackColor2")));
            this.colCURRENCY.Appearance.CellGrandTotal.Font = ((System.Drawing.Font)(resources.GetObject("colCURRENCY.Appearance.CellGrandTotal.Font")));
            this.colCURRENCY.Appearance.CellGrandTotal.Options.UseFont = true;
            this.colCURRENCY.Appearance.CellTotal.Font = ((System.Drawing.Font)(resources.GetObject("colCURRENCY.Appearance.CellTotal.Font")));
            this.colCURRENCY.Appearance.CellTotal.Options.UseFont = true;
            this.colCURRENCY.Appearance.Value.Options.UseTextOptions = true;
            this.colCURRENCY.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCURRENCY.Appearance.ValueGrandTotal.Font = ((System.Drawing.Font)(resources.GetObject("colCURRENCY.Appearance.ValueGrandTotal.Font")));
            this.colCURRENCY.Appearance.ValueGrandTotal.Options.UseFont = true;
            this.colCURRENCY.Appearance.ValueTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colCURRENCY.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("colCURRENCY.Appearance.ValueTotal.Font")));
            this.colCURRENCY.Appearance.ValueTotal.Options.UseBackColor = true;
            this.colCURRENCY.Appearance.ValueTotal.Options.UseFont = true;
            this.colCURRENCY.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colCURRENCY.AreaIndex = 0;
            resources.ApplyResources(this.colCURRENCY, "colCURRENCY");
            this.colCURRENCY.FieldName = "CURRENCY_CODE";
            this.colCURRENCY.Name = "colCURRENCY";
            this.colCURRENCY.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.False;
            this.colCURRENCY.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCURRENCY.Options.AllowSortBySummary = DevExpress.Utils.DefaultBoolean.False;
            this.colCURRENCY.Options.ShowCustomTotals = false;
            this.colCURRENCY.Options.ShowGrandTotal = false;
            // 
            // pnContainer
            // 
            this.pnContainer.BackColor = System.Drawing.Color.Transparent;
            this.pnContainer.Controls.Add(this.pgvAgingReport);
            this.pnContainer.Controls.Add(this.flowLayoutPanel3);
            this.pnContainer.Controls.Add(this._lblSubTitle1);
            this.pnContainer.Controls.Add(this._lblTitle);
            this.pnContainer.Controls.Add(this._lblAddress);
            this.pnContainer.Controls.Add(this._lblCompany);
            this.pnContainer.Controls.Add(this.panel2);
            resources.ApplyResources(this.pnContainer, "pnContainer");
            this.pnContainer.Name = "pnContainer";
            // 
            // pgvAgingReport
            // 
            this.pgvAgingReport.Appearance.Cell.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.Cell.Font")));
            this.pgvAgingReport.Appearance.ColumnHeaderArea.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.ColumnHeaderArea.Font")));
            this.pgvAgingReport.Appearance.ColumnHeaderArea.Options.UseFont = true;
            this.pgvAgingReport.Appearance.CustomTotalCell.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.CustomTotalCell.Font")));
            this.pgvAgingReport.Appearance.CustomTotalCell.Options.UseFont = true;
            this.pgvAgingReport.Appearance.DataHeaderArea.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.DataHeaderArea.Font")));
            this.pgvAgingReport.Appearance.FieldHeader.BackColor = System.Drawing.Color.Silver;
            this.pgvAgingReport.Appearance.FieldHeader.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.FieldHeader.Font")));
            this.pgvAgingReport.Appearance.FieldHeader.Options.UseBackColor = true;
            this.pgvAgingReport.Appearance.FieldHeader.Options.UseFont = true;
            this.pgvAgingReport.Appearance.FieldValue.BackColor = System.Drawing.Color.White;
            this.pgvAgingReport.Appearance.FieldValue.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.FieldValue.Font")));
            this.pgvAgingReport.Appearance.FieldValue.Options.UseBackColor = true;
            this.pgvAgingReport.Appearance.FieldValueGrandTotal.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.FieldValueGrandTotal.Font")));
            this.pgvAgingReport.Appearance.FieldValueGrandTotal.Options.UseFont = true;
            this.pgvAgingReport.Appearance.FieldValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.FieldValueTotal.Font")));
            this.pgvAgingReport.Appearance.FieldValueTotal.Options.UseFont = true;
            this.pgvAgingReport.Appearance.FocusedCell.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.FocusedCell.Font")));
            this.pgvAgingReport.Appearance.FocusedCell.Options.UseFont = true;
            this.pgvAgingReport.Appearance.GrandTotalCell.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.GrandTotalCell.Font")));
            this.pgvAgingReport.Appearance.GrandTotalCell.Options.UseFont = true;
            this.pgvAgingReport.Appearance.GrandTotalCell.Options.UseTextOptions = true;
            this.pgvAgingReport.Appearance.GrandTotalCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.pgvAgingReport.Appearance.HeaderArea.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.HeaderArea.Font")));
            this.pgvAgingReport.Appearance.HeaderArea.Options.UseFont = true;
            this.pgvAgingReport.Appearance.HeaderGroupLine.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.HeaderGroupLine.Font")));
            this.pgvAgingReport.Appearance.HeaderGroupLine.Options.UseFont = true;
            this.pgvAgingReport.Appearance.Lines.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.Lines.Font")));
            this.pgvAgingReport.Appearance.Lines.Options.UseFont = true;
            this.pgvAgingReport.Appearance.RowHeaderArea.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.RowHeaderArea.Font")));
            this.pgvAgingReport.Appearance.RowHeaderArea.Options.UseFont = true;
            this.pgvAgingReport.Appearance.TotalCell.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.Appearance.TotalCell.Font")));
            this.pgvAgingReport.Appearance.TotalCell.Options.UseFont = true;
            this.pgvAgingReport.AppearancePrint.Cell.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.AppearancePrint.Cell.Font")));
            this.pgvAgingReport.AppearancePrint.Cell.Options.UseFont = true;
            this.pgvAgingReport.AppearancePrint.FieldHeader.BackColor = System.Drawing.Color.Silver;
            this.pgvAgingReport.AppearancePrint.FieldHeader.Font = ((System.Drawing.Font)(resources.GetObject("pgvAgingReport.AppearancePrint.FieldHeader.Font")));
            this.pgvAgingReport.AppearancePrint.FieldHeader.Options.UseBackColor = true;
            this.pgvAgingReport.AppearancePrint.FieldHeader.Options.UseFont = true;
            this.pgvAgingReport.AppearancePrint.FieldValue.BackColor = System.Drawing.Color.White;
            this.pgvAgingReport.AppearancePrint.FieldValue.Options.UseBackColor = true;
            this.pgvAgingReport.AppearancePrint.FieldValueGrandTotal.BackColor = System.Drawing.Color.Silver;
            this.pgvAgingReport.AppearancePrint.FieldValueGrandTotal.BackColor2 = ((System.Drawing.Color)(resources.GetObject("pgvAgingReport.AppearancePrint.FieldValueGrandTotal.BackColor2")));
            this.pgvAgingReport.AppearancePrint.FieldValueGrandTotal.BorderColor = System.Drawing.Color.Silver;
            this.pgvAgingReport.AppearancePrint.FieldValueGrandTotal.Options.UseBackColor = true;
            this.pgvAgingReport.AppearancePrint.FieldValueGrandTotal.Options.UseBorderColor = true;
            this.pgvAgingReport.AppearancePrint.FieldValueTotal.BackColor = System.Drawing.Color.Silver;
            this.pgvAgingReport.AppearancePrint.FieldValueTotal.BackColor2 = ((System.Drawing.Color)(resources.GetObject("pgvAgingReport.AppearancePrint.FieldValueTotal.BackColor2")));
            this.pgvAgingReport.AppearancePrint.FieldValueTotal.BorderColor = System.Drawing.Color.Silver;
            this.pgvAgingReport.AppearancePrint.FieldValueTotal.Options.UseBackColor = true;
            this.pgvAgingReport.AppearancePrint.FieldValueTotal.Options.UseBorderColor = true;
            resources.ApplyResources(this.pgvAgingReport, "pgvAgingReport");
            this.pgvAgingReport.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.colCUSTOMER_CODE,
            this.colCUSTOMER_NAME,
            this.colPHONE_NUMBER,
            this.colAREA,
            this.colPOLE,
            this.colBOX,
            this.colNOT_ON_DUE_DATE,
            this._colP1,
            this._colP2,
            this._colP3,
            this._colP4,
            this._colP5,
            this.colAMOUNT,
            this.colTOTAL_DUE_AMOUNT,
            this.colPAID_AMOUNT,
            this.colCURRENCY,
            this.colINVOICE_NO,
            this.colINVOICE_DATE_BILLING,
            this.colINVOICE_TITLE,
            this.colMETER_CODE,
            this.colSTATUS,
            this.colOPEN_DAYS,
            this.colCUSTOMER_CONNECTION_TYPE,
            this.colCUSTOMER_GROUP,
            this.colCYCLE_NAME,
            this.colINVOICE_ITEM,
            this.colINVOICE_ITEM_TYPE,
            this.colROW_NO});
            this.pgvAgingReport.Name = "pgvAgingReport";
            this.pgvAgingReport.OptionsBehavior.HorizontalScrolling = DevExpress.XtraPivotGrid.PivotGridScrolling.Control;
            this.pgvAgingReport.OptionsBehavior.SortBySummaryDefaultOrder = DevExpress.XtraPivotGrid.PivotSortBySummaryOrder.Ascending;
            this.pgvAgingReport.OptionsDataField.AreaIndex = 0;
            this.pgvAgingReport.OptionsDataField.Caption = resources.GetString("pgvAgingReport.OptionsDataField.Caption");
            this.pgvAgingReport.OptionsFilter.DefaultFilterEditorView = DevExpress.XtraEditors.FilterEditorViewMode.VisualAndText;
            this.pgvAgingReport.OptionsFilterPopup.AutoFilterType = DevExpress.Utils.DefaultBoolean.True;
            this.pgvAgingReport.OptionsHint.ShowHeaderHints = false;
            this.pgvAgingReport.OptionsOLAP.SortByCustomFieldValueDisplayText = true;
            this.pgvAgingReport.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pgvAgingReport.OptionsPrint.PrintFilterHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pgvAgingReport.OptionsPrint.PrintUnusedFilterFields = false;
            this.pgvAgingReport.OptionsPrint.PrintVertLines = DevExpress.Utils.DefaultBoolean.True;
            this.pgvAgingReport.OptionsPrint.UsePrintAppearance = true;
            this.pgvAgingReport.OptionsView.ShowColumnGrandTotalHeader = false;
            this.pgvAgingReport.OptionsView.ShowColumnGrandTotals = false;
            this.pgvAgingReport.OptionsView.ShowColumnTotals = false;
            this.pgvAgingReport.OptionsView.ShowRowGrandTotalHeader = false;
            this.pgvAgingReport.OptionsView.ShowRowGrandTotals = false;
            this.pgvAgingReport.OptionsView.ShowTotalsForSingleValues = true;
            this.pgvAgingReport.CustomUnboundFieldData += new DevExpress.XtraPivotGrid.CustomFieldDataEventHandler(this.pgvAgingReport_CustomUnboundFieldData);
            this.pgvAgingReport.Click += new System.EventHandler(this.pgvAgingReport_Click);
            this.pgvAgingReport.DragDrop += new System.Windows.Forms.DragEventHandler(this.pgvAgingReport_DragDrop);
            // 
            // colCUSTOMER_CODE
            // 
            this.colCUSTOMER_CODE.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colCUSTOMER_CODE.AreaIndex = 2;
            this.colCUSTOMER_CODE.FieldName = "CUSTOMER_CODE";
            resources.ApplyResources(this.colCUSTOMER_CODE, "colCUSTOMER_CODE");
            this.colCUSTOMER_CODE.Name = "colCUSTOMER_CODE";
            this.colCUSTOMER_CODE.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_CODE.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_CODE.Options.AllowSortBySummary = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_CODE.Options.ShowCustomTotals = false;
            this.colCUSTOMER_CODE.Options.ShowGrandTotal = false;
            this.colCUSTOMER_CODE.Options.ShowTotals = false;
            this.colCUSTOMER_CODE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            // 
            // colCUSTOMER_NAME
            // 
            this.colCUSTOMER_NAME.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colCUSTOMER_NAME.AreaIndex = 3;
            this.colCUSTOMER_NAME.FieldName = "CUSTOMER_NAME";
            this.colCUSTOMER_NAME.Name = "colCUSTOMER_NAME";
            this.colCUSTOMER_NAME.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_NAME.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_NAME.Options.AllowSortBySummary = DevExpress.Utils.DefaultBoolean.False;
            this.colCUSTOMER_NAME.Options.ShowCustomTotals = false;
            this.colCUSTOMER_NAME.Options.ShowGrandTotal = false;
            this.colCUSTOMER_NAME.Options.ShowTotals = false;
            this.colCUSTOMER_NAME.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colCUSTOMER_NAME.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            resources.ApplyResources(this.colCUSTOMER_NAME, "colCUSTOMER_NAME");
            // 
            // colPHONE_NUMBER
            // 
            this.colPHONE_NUMBER.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colPHONE_NUMBER.AreaIndex = 4;
            resources.ApplyResources(this.colPHONE_NUMBER, "colPHONE_NUMBER");
            this.colPHONE_NUMBER.FieldName = "PHONE";
            this.colPHONE_NUMBER.Name = "colPHONE_NUMBER";
            this.colPHONE_NUMBER.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colPHONE_NUMBER.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPHONE_NUMBER.Options.AllowSortBySummary = DevExpress.Utils.DefaultBoolean.False;
            this.colPHONE_NUMBER.Options.ShowCustomTotals = false;
            this.colPHONE_NUMBER.Options.ShowGrandTotal = false;
            this.colPHONE_NUMBER.Options.ShowTotals = false;
            this.colPHONE_NUMBER.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this.colPHONE_NUMBER.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colAREA
            // 
            this.colAREA.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colAREA.AreaIndex = 5;
            this.colAREA.FieldName = "AREA_NAME";
            this.colAREA.Name = "colAREA";
            this.colAREA.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colAREA.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colAREA.Options.AllowSortBySummary = DevExpress.Utils.DefaultBoolean.False;
            this.colAREA.Options.ShowCustomTotals = false;
            this.colAREA.Options.ShowGrandTotal = false;
            this.colAREA.Options.ShowTotals = false;
            this.colAREA.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            resources.ApplyResources(this.colAREA, "colAREA");
            // 
            // colPOLE
            // 
            this.colPOLE.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colPOLE.AreaIndex = 6;
            this.colPOLE.FieldName = "POLE_CODE";
            this.colPOLE.Name = "colPOLE";
            this.colPOLE.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colPOLE.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colPOLE.Options.AllowSortBySummary = DevExpress.Utils.DefaultBoolean.False;
            this.colPOLE.Options.ShowCustomTotals = false;
            this.colPOLE.Options.ShowGrandTotal = false;
            this.colPOLE.Options.ShowTotals = false;
            this.colPOLE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            resources.ApplyResources(this.colPOLE, "colPOLE");
            // 
            // colBOX
            // 
            this.colBOX.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colBOX.AreaIndex = 7;
            resources.ApplyResources(this.colBOX, "colBOX");
            this.colBOX.FieldName = "BOX_CODE";
            this.colBOX.Name = "colBOX";
            this.colBOX.Options.ShowCustomTotals = false;
            this.colBOX.Options.ShowGrandTotal = false;
            this.colBOX.Options.ShowTotals = false;
            // 
            // colNOT_ON_DUE_DATE
            // 
            this.colNOT_ON_DUE_DATE.Appearance.Cell.Options.UseTextOptions = true;
            this.colNOT_ON_DUE_DATE.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNOT_ON_DUE_DATE.Appearance.CellGrandTotal.Options.UseTextOptions = true;
            this.colNOT_ON_DUE_DATE.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNOT_ON_DUE_DATE.Appearance.CellTotal.Options.UseTextOptions = true;
            this.colNOT_ON_DUE_DATE.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNOT_ON_DUE_DATE.Appearance.Header.Options.UseTextOptions = true;
            this.colNOT_ON_DUE_DATE.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNOT_ON_DUE_DATE.Appearance.Value.Options.UseTextOptions = true;
            this.colNOT_ON_DUE_DATE.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNOT_ON_DUE_DATE.Appearance.ValueGrandTotal.Options.UseTextOptions = true;
            this.colNOT_ON_DUE_DATE.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNOT_ON_DUE_DATE.Appearance.ValueTotal.Options.UseTextOptions = true;
            this.colNOT_ON_DUE_DATE.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colNOT_ON_DUE_DATE.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.colNOT_ON_DUE_DATE.AreaIndex = 0;
            resources.ApplyResources(this.colNOT_ON_DUE_DATE, "colNOT_ON_DUE_DATE");
            this.colNOT_ON_DUE_DATE.FieldName = "P0";
            this.colNOT_ON_DUE_DATE.Name = "colNOT_ON_DUE_DATE";
            this.colNOT_ON_DUE_DATE.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.True;
            this.colNOT_ON_DUE_DATE.Options.ShowCustomTotals = false;
            this.colNOT_ON_DUE_DATE.Options.ShowGrandTotal = false;
            this.colNOT_ON_DUE_DATE.Options.ShowTotals = false;
            this.colNOT_ON_DUE_DATE.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            // 
            // _colP1
            // 
            this._colP1.Appearance.Cell.Options.UseTextOptions = true;
            this._colP1.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP1.Appearance.CellGrandTotal.Options.UseTextOptions = true;
            this._colP1.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP1.Appearance.CellTotal.Options.UseTextOptions = true;
            this._colP1.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP1.Appearance.Header.Options.UseTextOptions = true;
            this._colP1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP1.Appearance.Value.Options.UseTextOptions = true;
            this._colP1.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP1.Appearance.ValueGrandTotal.Options.UseTextOptions = true;
            this._colP1.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP1.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("_colP1.Appearance.ValueTotal.Font")));
            this._colP1.Appearance.ValueTotal.Options.UseFont = true;
            this._colP1.Appearance.ValueTotal.Options.UseTextOptions = true;
            this._colP1.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this._colP1.AreaIndex = 1;
            resources.ApplyResources(this._colP1, "_colP1");
            this._colP1.FieldName = "P1";
            this._colP1.Name = "_colP1";
            this._colP1.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this._colP1.Options.ShowCustomTotals = false;
            this._colP1.Options.ShowGrandTotal = false;
            this._colP1.Options.ShowTotals = false;
            this._colP1.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            // 
            // _colP2
            // 
            this._colP2.Appearance.Cell.Options.UseTextOptions = true;
            this._colP2.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP2.Appearance.CellGrandTotal.Options.UseTextOptions = true;
            this._colP2.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP2.Appearance.CellTotal.Options.UseTextOptions = true;
            this._colP2.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP2.Appearance.Header.Options.UseTextOptions = true;
            this._colP2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP2.Appearance.Value.Options.UseTextOptions = true;
            this._colP2.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP2.Appearance.ValueGrandTotal.Options.UseTextOptions = true;
            this._colP2.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP2.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("_colP2.Appearance.ValueTotal.Font")));
            this._colP2.Appearance.ValueTotal.Options.UseFont = true;
            this._colP2.Appearance.ValueTotal.Options.UseTextOptions = true;
            this._colP2.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP2.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this._colP2.AreaIndex = 2;
            resources.ApplyResources(this._colP2, "_colP2");
            this._colP2.FieldName = "P2";
            this._colP2.Name = "_colP2";
            this._colP2.Options.ShowCustomTotals = false;
            this._colP2.Options.ShowGrandTotal = false;
            this._colP2.Options.ShowTotals = false;
            this._colP2.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            this._colP2.ToolTips.ValueFormat.FormatString = "d";
            this._colP2.ToolTips.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // _colP3
            // 
            this._colP3.Appearance.Cell.Options.UseTextOptions = true;
            this._colP3.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP3.Appearance.CellGrandTotal.Options.UseTextOptions = true;
            this._colP3.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP3.Appearance.CellTotal.Options.UseTextOptions = true;
            this._colP3.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP3.Appearance.Header.Options.UseTextOptions = true;
            this._colP3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP3.Appearance.Value.Options.UseTextOptions = true;
            this._colP3.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP3.Appearance.ValueGrandTotal.Options.UseTextOptions = true;
            this._colP3.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP3.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("_colP3.Appearance.ValueTotal.Font")));
            this._colP3.Appearance.ValueTotal.Options.UseFont = true;
            this._colP3.Appearance.ValueTotal.Options.UseTextOptions = true;
            this._colP3.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP3.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this._colP3.AreaIndex = 3;
            resources.ApplyResources(this._colP3, "_colP3");
            this._colP3.FieldName = "P3";
            this._colP3.Name = "_colP3";
            this._colP3.Options.ShowCustomTotals = false;
            this._colP3.Options.ShowGrandTotal = false;
            this._colP3.Options.ShowTotals = false;
            this._colP3.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            // 
            // _colP4
            // 
            this._colP4.Appearance.Cell.Options.UseTextOptions = true;
            this._colP4.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP4.Appearance.CellGrandTotal.Options.UseTextOptions = true;
            this._colP4.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP4.Appearance.CellTotal.Options.UseTextOptions = true;
            this._colP4.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP4.Appearance.Header.Options.UseTextOptions = true;
            this._colP4.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP4.Appearance.Value.Options.UseTextOptions = true;
            this._colP4.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP4.Appearance.ValueGrandTotal.Options.UseTextOptions = true;
            this._colP4.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP4.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("_colP4.Appearance.ValueTotal.Font")));
            this._colP4.Appearance.ValueTotal.Options.UseFont = true;
            this._colP4.Appearance.ValueTotal.Options.UseTextOptions = true;
            this._colP4.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP4.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this._colP4.AreaIndex = 4;
            resources.ApplyResources(this._colP4, "_colP4");
            this._colP4.FieldName = "P4";
            this._colP4.Name = "_colP4";
            this._colP4.Options.ShowCustomTotals = false;
            this._colP4.Options.ShowGrandTotal = false;
            this._colP4.Options.ShowTotals = false;
            this._colP4.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            // 
            // _colP5
            // 
            this._colP5.Appearance.Cell.Options.UseTextOptions = true;
            this._colP5.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP5.Appearance.CellGrandTotal.Options.UseTextOptions = true;
            this._colP5.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP5.Appearance.CellTotal.Options.UseTextOptions = true;
            this._colP5.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP5.Appearance.Header.Options.UseTextOptions = true;
            this._colP5.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP5.Appearance.Value.Options.UseTextOptions = true;
            this._colP5.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP5.Appearance.ValueGrandTotal.Options.UseTextOptions = true;
            this._colP5.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP5.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("_colP5.Appearance.ValueTotal.Font")));
            this._colP5.Appearance.ValueTotal.Options.UseFont = true;
            this._colP5.Appearance.ValueTotal.Options.UseTextOptions = true;
            this._colP5.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._colP5.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this._colP5.AreaIndex = 5;
            resources.ApplyResources(this._colP5, "_colP5");
            this._colP5.FieldName = "P5";
            this._colP5.Name = "_colP5";
            this._colP5.Options.ShowCustomTotals = false;
            this._colP5.Options.ShowGrandTotal = false;
            this._colP5.Options.ShowTotals = false;
            this._colP5.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            // 
            // colAMOUNT
            // 
            this.colAMOUNT.Appearance.Cell.Options.UseTextOptions = true;
            this.colAMOUNT.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colAMOUNT.Appearance.CellGrandTotal.Options.UseTextOptions = true;
            this.colAMOUNT.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colAMOUNT.Appearance.CellTotal.Options.UseTextOptions = true;
            this.colAMOUNT.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colAMOUNT.Appearance.Header.Options.UseTextOptions = true;
            this.colAMOUNT.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colAMOUNT.Appearance.Value.Options.UseTextOptions = true;
            this.colAMOUNT.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colAMOUNT.Appearance.ValueGrandTotal.Options.UseTextOptions = true;
            this.colAMOUNT.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colAMOUNT.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("colAMOUNT.Appearance.ValueTotal.Font")));
            this.colAMOUNT.Appearance.ValueTotal.Options.UseFont = true;
            this.colAMOUNT.Appearance.ValueTotal.Options.UseTextOptions = true;
            this.colAMOUNT.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colAMOUNT.AreaIndex = 6;
            this.colAMOUNT.FieldName = "SETTLE_AMOUNT";
            this.colAMOUNT.Name = "colAMOUNT";
            this.colAMOUNT.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.True;
            this.colAMOUNT.Options.ShowCustomTotals = false;
            this.colAMOUNT.Options.ShowGrandTotal = false;
            this.colAMOUNT.Options.ShowTotals = false;
            resources.ApplyResources(this.colAMOUNT, "colAMOUNT");
            // 
            // colTOTAL_DUE_AMOUNT
            // 
            this.colTOTAL_DUE_AMOUNT.Appearance.CellGrandTotal.Options.UseTextOptions = true;
            this.colTOTAL_DUE_AMOUNT.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTOTAL_DUE_AMOUNT.Appearance.CellTotal.Options.UseTextOptions = true;
            this.colTOTAL_DUE_AMOUNT.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTOTAL_DUE_AMOUNT.Appearance.Header.Options.UseTextOptions = true;
            this.colTOTAL_DUE_AMOUNT.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTOTAL_DUE_AMOUNT.Appearance.Value.Options.UseTextOptions = true;
            this.colTOTAL_DUE_AMOUNT.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTOTAL_DUE_AMOUNT.Appearance.ValueGrandTotal.Options.UseTextOptions = true;
            this.colTOTAL_DUE_AMOUNT.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTOTAL_DUE_AMOUNT.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("colTOTAL_DUE_AMOUNT.Appearance.ValueTotal.Font")));
            this.colTOTAL_DUE_AMOUNT.Appearance.ValueTotal.Options.UseFont = true;
            this.colTOTAL_DUE_AMOUNT.Appearance.ValueTotal.Options.UseTextOptions = true;
            this.colTOTAL_DUE_AMOUNT.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTOTAL_DUE_AMOUNT.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.colTOTAL_DUE_AMOUNT.AreaIndex = 6;
            resources.ApplyResources(this.colTOTAL_DUE_AMOUNT, "colTOTAL_DUE_AMOUNT");
            this.colTOTAL_DUE_AMOUNT.FieldName = "TOTAL";
            this.colTOTAL_DUE_AMOUNT.Name = "colTOTAL_DUE_AMOUNT";
            this.colTOTAL_DUE_AMOUNT.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.True;
            this.colTOTAL_DUE_AMOUNT.Options.ShowCustomTotals = false;
            this.colTOTAL_DUE_AMOUNT.Options.ShowGrandTotal = false;
            this.colTOTAL_DUE_AMOUNT.Options.ShowTotals = false;
            this.colTOTAL_DUE_AMOUNT.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            // 
            // colPAID_AMOUNT
            // 
            this.colPAID_AMOUNT.Appearance.Cell.Options.UseTextOptions = true;
            this.colPAID_AMOUNT.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colPAID_AMOUNT.Appearance.CellGrandTotal.Options.UseTextOptions = true;
            this.colPAID_AMOUNT.Appearance.CellGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colPAID_AMOUNT.Appearance.CellTotal.Options.UseTextOptions = true;
            this.colPAID_AMOUNT.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colPAID_AMOUNT.Appearance.Header.Options.UseTextOptions = true;
            this.colPAID_AMOUNT.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colPAID_AMOUNT.Appearance.Value.Options.UseTextOptions = true;
            this.colPAID_AMOUNT.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colPAID_AMOUNT.Appearance.ValueGrandTotal.Options.UseTextOptions = true;
            this.colPAID_AMOUNT.Appearance.ValueGrandTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colPAID_AMOUNT.Appearance.ValueTotal.Font = ((System.Drawing.Font)(resources.GetObject("colPAID_AMOUNT.Appearance.ValueTotal.Font")));
            this.colPAID_AMOUNT.Appearance.ValueTotal.Options.UseFont = true;
            this.colPAID_AMOUNT.Appearance.ValueTotal.Options.UseTextOptions = true;
            this.colPAID_AMOUNT.Appearance.ValueTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colPAID_AMOUNT.AreaIndex = 12;
            resources.ApplyResources(this.colPAID_AMOUNT, "colPAID_AMOUNT");
            this.colPAID_AMOUNT.FieldName = "PAID_AMOUNT";
            this.colPAID_AMOUNT.Name = "colPAID_AMOUNT";
            this.colPAID_AMOUNT.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colPAID_AMOUNT.Options.ShowCustomTotals = false;
            this.colPAID_AMOUNT.Options.ShowGrandTotal = false;
            this.colPAID_AMOUNT.Options.ShowTotals = false;
            // 
            // colINVOICE_NO
            // 
            this.colINVOICE_NO.AreaIndex = 10;
            resources.ApplyResources(this.colINVOICE_NO, "colINVOICE_NO");
            this.colINVOICE_NO.FieldName = "INVOICE_NO";
            this.colINVOICE_NO.Name = "colINVOICE_NO";
            this.colINVOICE_NO.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colINVOICE_NO.Options.ShowCustomTotals = false;
            this.colINVOICE_NO.Options.ShowGrandTotal = false;
            this.colINVOICE_NO.Options.ShowTotals = false;
            // 
            // colINVOICE_DATE_BILLING
            // 
            this.colINVOICE_DATE_BILLING.AreaIndex = 11;
            resources.ApplyResources(this.colINVOICE_DATE_BILLING, "colINVOICE_DATE_BILLING");
            this.colINVOICE_DATE_BILLING.FieldName = "INVOICE_DATE";
            this.colINVOICE_DATE_BILLING.Name = "colINVOICE_DATE_BILLING";
            this.colINVOICE_DATE_BILLING.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colINVOICE_DATE_BILLING.Options.ShowCustomTotals = false;
            this.colINVOICE_DATE_BILLING.Options.ShowGrandTotal = false;
            this.colINVOICE_DATE_BILLING.Options.ShowTotals = false;
            // 
            // colINVOICE_TITLE
            // 
            this.colINVOICE_TITLE.AreaIndex = 7;
            resources.ApplyResources(this.colINVOICE_TITLE, "colINVOICE_TITLE");
            this.colINVOICE_TITLE.FieldName = "INVOICE_TITLE";
            this.colINVOICE_TITLE.Name = "colINVOICE_TITLE";
            this.colINVOICE_TITLE.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colINVOICE_TITLE.Options.ShowCustomTotals = false;
            this.colINVOICE_TITLE.Options.ShowGrandTotal = false;
            this.colINVOICE_TITLE.Options.ShowTotals = false;
            this.colINVOICE_TITLE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colMETER_CODE
            // 
            this.colMETER_CODE.AreaIndex = 3;
            resources.ApplyResources(this.colMETER_CODE, "colMETER_CODE");
            this.colMETER_CODE.FieldName = "METER_CODE";
            this.colMETER_CODE.Name = "colMETER_CODE";
            this.colMETER_CODE.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colMETER_CODE.Options.ShowCustomTotals = false;
            this.colMETER_CODE.Options.ShowGrandTotal = false;
            this.colMETER_CODE.Options.ShowTotals = false;
            // 
            // colSTATUS
            // 
            this.colSTATUS.AreaIndex = 9;
            resources.ApplyResources(this.colSTATUS, "colSTATUS");
            this.colSTATUS.FieldName = "STATUS";
            this.colSTATUS.Name = "colSTATUS";
            this.colSTATUS.Options.ShowCustomTotals = false;
            this.colSTATUS.Options.ShowGrandTotal = false;
            this.colSTATUS.Options.ShowTotals = false;
            // 
            // colOPEN_DAYS
            // 
            this.colOPEN_DAYS.Appearance.Cell.Options.UseTextOptions = true;
            this.colOPEN_DAYS.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOPEN_DAYS.Appearance.Header.Options.UseTextOptions = true;
            this.colOPEN_DAYS.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOPEN_DAYS.Appearance.Value.Options.UseTextOptions = true;
            this.colOPEN_DAYS.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colOPEN_DAYS.AreaIndex = 0;
            resources.ApplyResources(this.colOPEN_DAYS, "colOPEN_DAYS");
            this.colOPEN_DAYS.FieldName = "OPEN_DAYS";
            this.colOPEN_DAYS.Name = "colOPEN_DAYS";
            this.colOPEN_DAYS.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.True;
            this.colOPEN_DAYS.Options.ShowCustomTotals = false;
            this.colOPEN_DAYS.Options.ShowGrandTotal = false;
            this.colOPEN_DAYS.Options.ShowTotals = false;
            // 
            // colCUSTOMER_CONNECTION_TYPE
            // 
            this.colCUSTOMER_CONNECTION_TYPE.AreaIndex = 5;
            resources.ApplyResources(this.colCUSTOMER_CONNECTION_TYPE, "colCUSTOMER_CONNECTION_TYPE");
            this.colCUSTOMER_CONNECTION_TYPE.FieldName = "CUSTOMER_CONNECTION_TYPE";
            this.colCUSTOMER_CONNECTION_TYPE.Name = "colCUSTOMER_CONNECTION_TYPE";
            this.colCUSTOMER_CONNECTION_TYPE.Options.ShowCustomTotals = false;
            this.colCUSTOMER_CONNECTION_TYPE.Options.ShowGrandTotal = false;
            this.colCUSTOMER_CONNECTION_TYPE.Options.ShowTotals = false;
            this.colCUSTOMER_CONNECTION_TYPE.TotalsVisibility = DevExpress.XtraPivotGrid.PivotTotalsVisibility.None;
            // 
            // colCUSTOMER_GROUP
            // 
            this.colCUSTOMER_GROUP.AreaIndex = 4;
            resources.ApplyResources(this.colCUSTOMER_GROUP, "colCUSTOMER_GROUP");
            this.colCUSTOMER_GROUP.FieldName = "CUSTOMER_GROUP";
            this.colCUSTOMER_GROUP.Name = "colCUSTOMER_GROUP";
            this.colCUSTOMER_GROUP.Options.ShowCustomTotals = false;
            this.colCUSTOMER_GROUP.Options.ShowGrandTotal = false;
            this.colCUSTOMER_GROUP.Options.ShowTotals = false;
            // 
            // colCYCLE_NAME
            // 
            this.colCYCLE_NAME.AreaIndex = 8;
            resources.ApplyResources(this.colCYCLE_NAME, "colCYCLE_NAME");
            this.colCYCLE_NAME.FieldName = "CYCLE_NAME";
            this.colCYCLE_NAME.Name = "colCYCLE_NAME";
            this.colCYCLE_NAME.Options.ShowCustomTotals = false;
            this.colCYCLE_NAME.Options.ShowGrandTotal = false;
            this.colCYCLE_NAME.Options.ShowTotals = false;
            // 
            // colINVOICE_ITEM
            // 
            this.colINVOICE_ITEM.AreaIndex = 2;
            resources.ApplyResources(this.colINVOICE_ITEM, "colINVOICE_ITEM");
            this.colINVOICE_ITEM.FieldName = "INVOICE_ITEM";
            this.colINVOICE_ITEM.Name = "colINVOICE_ITEM";
            this.colINVOICE_ITEM.Options.ShowCustomTotals = false;
            this.colINVOICE_ITEM.Options.ShowGrandTotal = false;
            this.colINVOICE_ITEM.Options.ShowTotals = false;
            // 
            // colINVOICE_ITEM_TYPE
            // 
            this.colINVOICE_ITEM_TYPE.AreaIndex = 1;
            resources.ApplyResources(this.colINVOICE_ITEM_TYPE, "colINVOICE_ITEM_TYPE");
            this.colINVOICE_ITEM_TYPE.FieldName = "INVOICE_ITEM_TYPE";
            this.colINVOICE_ITEM_TYPE.Name = "colINVOICE_ITEM_TYPE";
            this.colINVOICE_ITEM_TYPE.Options.ShowCustomTotals = false;
            this.colINVOICE_ITEM_TYPE.Options.ShowGrandTotal = false;
            this.colINVOICE_ITEM_TYPE.Options.ShowTotals = false;
            // 
            // colROW_NO
            // 
            this.colROW_NO.Appearance.Cell.Options.UseTextOptions = true;
            this.colROW_NO.Appearance.Cell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colROW_NO.Appearance.Cell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colROW_NO.Appearance.CellTotal.Options.UseTextOptions = true;
            this.colROW_NO.Appearance.CellTotal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colROW_NO.Appearance.CellTotal.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colROW_NO.Appearance.Header.Options.UseTextOptions = true;
            this.colROW_NO.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colROW_NO.Appearance.Header.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colROW_NO.Appearance.Value.Options.UseTextOptions = true;
            this.colROW_NO.Appearance.Value.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colROW_NO.Appearance.Value.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colROW_NO.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.colROW_NO.AreaIndex = 1;
            this.colROW_NO.FieldName = "ROW_NO";
            this.colROW_NO.Name = "colROW_NO";
            this.colROW_NO.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.colROW_NO.Options.AllowDragInCustomizationForm = DevExpress.Utils.DefaultBoolean.False;
            this.colROW_NO.Options.AllowExpand = DevExpress.Utils.DefaultBoolean.False;
            this.colROW_NO.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.False;
            this.colROW_NO.Options.AllowHide = DevExpress.Utils.DefaultBoolean.False;
            this.colROW_NO.Options.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colROW_NO.Options.ShowCustomTotals = false;
            this.colROW_NO.Options.ShowGrandTotal = false;
            this.colROW_NO.Options.ShowTotals = false;
            this.colROW_NO.SortMode = DevExpress.XtraPivotGrid.PivotSortMode.Custom;
            resources.ApplyResources(this.colROW_NO, "colROW_NO");
            // 
            // flowLayoutPanel3
            // 
            resources.ApplyResources(this.flowLayoutPanel3, "flowLayoutPanel3");
            this.flowLayoutPanel3.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel3.Controls.Add(this.dpExport);
            this.flowLayoutPanel3.Controls.Add(this.dpAction);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            // 
            // dpExport
            // 
            resources.ApplyResources(this.dpExport, "dpExport");
            this.dpExport.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.dpExport.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dpExport.Appearance.Font")));
            this.dpExport.Appearance.Options.UseBackColor = true;
            this.dpExport.Appearance.Options.UseBorderColor = true;
            this.dpExport.Appearance.Options.UseFont = true;
            this.dpExport.AppearanceDisabled.Font = ((System.Drawing.Font)(resources.GetObject("dpExport.AppearanceDisabled.Font")));
            this.dpExport.AppearanceDisabled.Options.UseFont = true;
            this.dpExport.AppearanceDropDown.Font = ((System.Drawing.Font)(resources.GetObject("dpExport.AppearanceDropDown.Font")));
            this.dpExport.AppearanceDropDown.Options.UseFont = true;
            this.dpExport.AppearanceDropDownDisabled.Font = ((System.Drawing.Font)(resources.GetObject("dpExport.AppearanceDropDownDisabled.Font")));
            this.dpExport.AppearanceDropDownDisabled.Options.UseFont = true;
            this.dpExport.AppearanceDropDownHovered.Font = ((System.Drawing.Font)(resources.GetObject("dpExport.AppearanceDropDownHovered.Font")));
            this.dpExport.AppearanceDropDownHovered.Options.UseFont = true;
            this.dpExport.AppearanceDropDownPressed.Font = ((System.Drawing.Font)(resources.GetObject("dpExport.AppearanceDropDownPressed.Font")));
            this.dpExport.AppearanceDropDownPressed.Options.UseFont = true;
            this.dpExport.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("dpExport.AppearanceHovered.Font")));
            this.dpExport.AppearanceHovered.Options.UseFont = true;
            this.dpExport.AppearancePressed.Font = ((System.Drawing.Font)(resources.GetObject("dpExport.AppearancePressed.Font")));
            this.dpExport.AppearancePressed.Options.UseFont = true;
            this.dpExport.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dpExport.DropDownControl = this.popupExport;
            this.dpExport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dpExport.ImageOptions.Image")));
            this.dpExport.ImageOptions.ImageUri.Uri = "SendCSV;Size32x32;GrayScaled";
            this.dpExport.ImageOptions.SvgImageSize = new System.Drawing.Size(10, 10);
            this.dpExport.LookAndFeel.SkinMaskColor = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Transparent;
            this.dpExport.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dpExport.Name = "dpExport";
            this.dpExport.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            // 
            // popupExport
            // 
            this.popupExport.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCsv),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPdf),
            new DevExpress.XtraBars.LinkPersistInfo(this.bntRaw_Data, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnPRINT)});
            this.popupExport.Manager = this.barManager1;
            this.popupExport.Name = "popupExport";
            // 
            // btnExcel
            // 
            resources.ApplyResources(this.btnExcel, "btnExcel");
            this.btnExcel.Id = 0;
            this.btnExcel.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnExcel.ItemAppearance.Disabled.Font")));
            this.btnExcel.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnExcel.ItemAppearance.Hovered.Font")));
            this.btnExcel.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnExcel.ItemAppearance.Normal.Font")));
            this.btnExcel.ItemAppearance.Normal.Options.UseFont = true;
            this.btnExcel.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnExcel.ItemAppearance.Pressed.Font")));
            this.btnExcel.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExcel_ItemClick);
            // 
            // btnCsv
            // 
            resources.ApplyResources(this.btnCsv, "btnCsv");
            this.btnCsv.Id = 1;
            this.btnCsv.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnCsv.ItemAppearance.Disabled.Font")));
            this.btnCsv.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnCsv.ItemAppearance.Hovered.Font")));
            this.btnCsv.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnCsv.ItemAppearance.Normal.Font")));
            this.btnCsv.ItemAppearance.Normal.Options.UseFont = true;
            this.btnCsv.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnCsv.ItemAppearance.Pressed.Font")));
            this.btnCsv.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnCsv.Name = "btnCsv";
            this.btnCsv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCsv_ItemClick);
            // 
            // btnPdf
            // 
            resources.ApplyResources(this.btnPdf, "btnPdf");
            this.btnPdf.Id = 2;
            this.btnPdf.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnPdf.ItemAppearance.Disabled.Font")));
            this.btnPdf.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnPdf.ItemAppearance.Hovered.Font")));
            this.btnPdf.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnPdf.ItemAppearance.Normal.Font")));
            this.btnPdf.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPdf.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnPdf.ItemAppearance.Pressed.Font")));
            this.btnPdf.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPdf.Name = "btnPdf";
            this.btnPdf.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPdf_ItemClick);
            // 
            // bntRaw_Data
            // 
            resources.ApplyResources(this.bntRaw_Data, "bntRaw_Data");
            this.bntRaw_Data.Id = 6;
            this.bntRaw_Data.Name = "bntRaw_Data";
            this.bntRaw_Data.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bntRaw_Data_ItemClick);
            // 
            // btnPRINT
            // 
            resources.ApplyResources(this.btnPRINT, "btnPRINT");
            this.btnPRINT.Id = 3;
            this.btnPRINT.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnPRINT.ItemAppearance.Disabled.Font")));
            this.btnPRINT.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPRINT.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnPRINT.ItemAppearance.Hovered.Font")));
            this.btnPRINT.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPRINT.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnPRINT.ItemAppearance.Normal.Font")));
            this.btnPRINT.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPRINT.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnPRINT.ItemAppearance.Pressed.Font")));
            this.btnPRINT.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPRINT.ItemInMenuAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnPRINT.ItemInMenuAppearance.Disabled.Font")));
            this.btnPRINT.ItemInMenuAppearance.Disabled.Options.UseFont = true;
            this.btnPRINT.ItemInMenuAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnPRINT.ItemInMenuAppearance.Hovered.Font")));
            this.btnPRINT.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.btnPRINT.ItemInMenuAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnPRINT.ItemInMenuAppearance.Normal.Font")));
            this.btnPRINT.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.btnPRINT.ItemInMenuAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnPRINT.ItemInMenuAppearance.Pressed.Font")));
            this.btnPRINT.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.btnPRINT.Name = "btnPRINT";
            this.btnPRINT.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPRINT_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnExcel,
            this.btnCsv,
            this.btnPdf,
            this.btnPRINT,
            this.btnResetToDefault,
            this.bntRaw_Data,
            this.btnSAVE_TEMPLATE});
            this.barManager1.MaxItemId = 10;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            resources.ApplyResources(this.barDockControlTop, "barDockControlTop");
            this.barDockControlTop.Manager = this.barManager1;
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            resources.ApplyResources(this.barDockControlBottom, "barDockControlBottom");
            this.barDockControlBottom.Manager = this.barManager1;
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            resources.ApplyResources(this.barDockControlLeft, "barDockControlLeft");
            this.barDockControlLeft.Manager = this.barManager1;
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            resources.ApplyResources(this.barDockControlRight, "barDockControlRight");
            this.barDockControlRight.Manager = this.barManager1;
            // 
            // btnResetToDefault
            // 
            resources.ApplyResources(this.btnResetToDefault, "btnResetToDefault");
            this.btnResetToDefault.Id = 5;
            this.btnResetToDefault.ItemAppearance.Disabled.Font = ((System.Drawing.Font)(resources.GetObject("btnResetToDefault.ItemAppearance.Disabled.Font")));
            this.btnResetToDefault.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnResetToDefault.ItemAppearance.Hovered.Font = ((System.Drawing.Font)(resources.GetObject("btnResetToDefault.ItemAppearance.Hovered.Font")));
            this.btnResetToDefault.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnResetToDefault.ItemAppearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("btnResetToDefault.ItemAppearance.Normal.Font")));
            this.btnResetToDefault.ItemAppearance.Normal.Options.UseFont = true;
            this.btnResetToDefault.ItemAppearance.Pressed.Font = ((System.Drawing.Font)(resources.GetObject("btnResetToDefault.ItemAppearance.Pressed.Font")));
            this.btnResetToDefault.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnResetToDefault.Name = "btnResetToDefault";
            this.btnResetToDefault.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnResetToDefault_ItemClick);
            // 
            // btnSAVE_TEMPLATE
            // 
            resources.ApplyResources(this.btnSAVE_TEMPLATE, "btnSAVE_TEMPLATE");
            this.btnSAVE_TEMPLATE.Id = 7;
            this.btnSAVE_TEMPLATE.Name = "btnSAVE_TEMPLATE";
            // 
            // dpAction
            // 
            resources.ApplyResources(this.dpAction, "dpAction");
            this.dpAction.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.dpAction.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.dpAction.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("dpAction.Appearance.Font")));
            this.dpAction.Appearance.Options.UseBackColor = true;
            this.dpAction.Appearance.Options.UseBorderColor = true;
            this.dpAction.Appearance.Options.UseFont = true;
            this.dpAction.AppearanceDisabled.Font = ((System.Drawing.Font)(resources.GetObject("dpAction.AppearanceDisabled.Font")));
            this.dpAction.AppearanceDisabled.Options.UseFont = true;
            this.dpAction.AppearanceDropDown.Font = ((System.Drawing.Font)(resources.GetObject("dpAction.AppearanceDropDown.Font")));
            this.dpAction.AppearanceDropDown.Options.UseFont = true;
            this.dpAction.AppearanceDropDownDisabled.Font = ((System.Drawing.Font)(resources.GetObject("dpAction.AppearanceDropDownDisabled.Font")));
            this.dpAction.AppearanceDropDownDisabled.Options.UseFont = true;
            this.dpAction.AppearanceDropDownHovered.Font = ((System.Drawing.Font)(resources.GetObject("dpAction.AppearanceDropDownHovered.Font")));
            this.dpAction.AppearanceDropDownHovered.Options.UseFont = true;
            this.dpAction.AppearanceDropDownPressed.Font = ((System.Drawing.Font)(resources.GetObject("dpAction.AppearanceDropDownPressed.Font")));
            this.dpAction.AppearanceDropDownPressed.Options.UseFont = true;
            this.dpAction.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("dpAction.AppearanceHovered.Font")));
            this.dpAction.AppearanceHovered.Options.UseFont = true;
            this.dpAction.AppearancePressed.Font = ((System.Drawing.Font)(resources.GetObject("dpAction.AppearancePressed.Font")));
            this.dpAction.AppearancePressed.Options.UseFont = true;
            this.dpAction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dpAction.DropDownArrowStyle = DevExpress.XtraEditors.DropDownArrowStyle.Show;
            this.dpAction.DropDownControl = this.popActions;
            this.dpAction.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("dpAction.ImageOptions.Image")));
            this.dpAction.ImageOptions.ImageUri.Uri = "WrapText;Size32x32;GrayScaled";
            this.dpAction.ImageOptions.SvgImageColorizationMode = DevExpress.Utils.SvgImageColorizationMode.Full;
            this.dpAction.LookAndFeel.SkinMaskColor = System.Drawing.Color.Transparent;
            this.dpAction.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Transparent;
            this.dpAction.LookAndFeel.UseDefaultLookAndFeel = false;
            this.dpAction.Name = "dpAction";
            this.dpAction.PaintStyle = DevExpress.XtraEditors.Controls.PaintStyles.Light;
            // 
            // popActions
            // 
            this.popActions.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSAVE_TEMPLATE)});
            this.popActions.Manager = this.barManager1;
            this.popActions.Name = "popActions";
            // 
            // _lblSubTitle1
            // 
            this._lblSubTitle1.BackColor = System.Drawing.Color.White;
            this._lblSubTitle1.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this._lblSubTitle1, "_lblSubTitle1");
            this._lblSubTitle1.Name = "_lblSubTitle1";
            // 
            // _lblTitle
            // 
            this._lblTitle.BackColor = System.Drawing.Color.White;
            this._lblTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this._lblTitle, "_lblTitle");
            this._lblTitle.Name = "_lblTitle";
            // 
            // _lblAddress
            // 
            this._lblAddress.BackColor = System.Drawing.Color.White;
            this._lblAddress.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this._lblAddress, "_lblAddress");
            this._lblAddress.Name = "_lblAddress";
            // 
            // _lblCompany
            // 
            this._lblCompany.BackColor = System.Drawing.Color.White;
            this._lblCompany.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this._lblCompany, "_lblCompany");
            this._lblCompany.Name = "_lblCompany";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.lblDAY);
            this.panel2.Controls.Add(this.nudDays);
            this.panel2.Controls.Add(this.rdpDate);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // lblDAY
            // 
            resources.ApplyResources(this.lblDAY, "lblDAY");
            this.lblDAY.Name = "lblDAY";
            // 
            // nudDays
            // 
            resources.ApplyResources(this.nudDays, "nudDays");
            this.nudDays.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDays.Name = "nudDays";
            this.nudDays.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // rdpDate
            // 
            this.rdpDate.AutoSave = true;
            resources.ApplyResources(this.rdpDate, "rdpDate");
            this.rdpDate.DateFilterTypes = EPower.Base.Helper.Component.ReportDatePicker.DateFilterType.Period;
            this.rdpDate.DefaultValue = EPower.Base.Helper.Component.ReportDatePicker.DateRanges.Today;
            this.rdpDate.FromDate = new System.DateTime(2022, 7, 29, 0, 0, 0, 0);
            this.rdpDate.LastCustomDays = 30;
            this.rdpDate.LastFinancialYear = true;
            this.rdpDate.LastMonth = true;
            this.rdpDate.LastQuarter = true;
            this.rdpDate.LastWeek = true;
            this.rdpDate.Name = "rdpDate";
            this.rdpDate.ThisFinancialYear = true;
            this.rdpDate.ThisMonth = true;
            this.rdpDate.ThisQuarter = true;
            this.rdpDate.ThisWeek = true;
            this.rdpDate.ToDate = new System.DateTime(2022, 7, 29, 23, 59, 59, 0);
            this.rdpDate.Today = true;
            this.rdpDate.Value = EPower.Base.Helper.Component.ReportDatePicker.DateRanges.Today;
            this.rdpDate.Yesterday = true;
            // 
            // pivotGridField1
            // 
            this.pivotGridField1.AreaIndex = 13;
            this.pivotGridField1.FieldName = "CUSTOMER_CODE";
            this.pivotGridField1.Name = "pivotGridField1";
            // 
            // pivotGridField2
            // 
            this.pivotGridField2.AreaIndex = 14;
            this.pivotGridField2.FieldName = "CUSTOMER_NAME";
            this.pivotGridField2.Name = "pivotGridField2";
            // 
            // pivotGridField3
            // 
            this.pivotGridField3.AreaIndex = 15;
            this.pivotGridField3.FieldName = "PHONE_NUMBER";
            this.pivotGridField3.Name = "pivotGridField3";
            // 
            // pivotGridField4
            // 
            this.pivotGridField4.AreaIndex = 16;
            this.pivotGridField4.FieldName = "CUSTOMER_TYPE";
            this.pivotGridField4.Name = "pivotGridField4";
            // 
            // pivotGridField5
            // 
            this.pivotGridField5.AreaIndex = 17;
            this.pivotGridField5.FieldName = "ADDRESS_CUSTOMER";
            this.pivotGridField5.Name = "pivotGridField5";
            // 
            // pivotGridField6
            // 
            this.pivotGridField6.AreaIndex = 18;
            this.pivotGridField6.FieldName = "METER_CODE";
            this.pivotGridField6.Name = "pivotGridField6";
            // 
            // pivotGridField7
            // 
            this.pivotGridField7.AreaIndex = 0;
            this.pivotGridField7.FieldName = "AMPARE_NAME";
            this.pivotGridField7.Name = "pivotGridField7";
            // 
            // pivotGridField8
            // 
            this.pivotGridField8.AreaIndex = 1;
            this.pivotGridField8.FieldName = "AREA_NAME";
            this.pivotGridField8.Name = "pivotGridField8";
            // 
            // pivotGridField9
            // 
            this.pivotGridField9.AreaIndex = 2;
            this.pivotGridField9.FieldName = "POLE_NAME";
            this.pivotGridField9.Name = "pivotGridField9";
            // 
            // pivotGridField10
            // 
            this.pivotGridField10.AreaIndex = 3;
            this.pivotGridField10.FieldName = "BOX_CODE";
            this.pivotGridField10.Name = "pivotGridField10";
            // 
            // pivotGridField11
            // 
            this.pivotGridField11.AreaIndex = 4;
            this.pivotGridField11.FieldName = "CREATE_ON";
            this.pivotGridField11.Name = "pivotGridField11";
            // 
            // pivotGridField12
            // 
            this.pivotGridField12.AreaIndex = 5;
            this.pivotGridField12.FieldName = "INVOICE_DATE";
            this.pivotGridField12.Name = "pivotGridField12";
            // 
            // pivotGridField13
            // 
            this.pivotGridField13.AreaIndex = 6;
            this.pivotGridField13.FieldName = "BANK_CUT_OFF_DATE";
            this.pivotGridField13.Name = "pivotGridField13";
            // 
            // pivotGridField14
            // 
            this.pivotGridField14.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField14.AreaIndex = 0;
            this.pivotGridField14.FieldName = "PAID_DATE";
            this.pivotGridField14.Name = "pivotGridField14";
            // 
            // pivotGridField15
            // 
            this.pivotGridField15.AreaIndex = 7;
            this.pivotGridField15.FieldName = "DUE_DATE";
            this.pivotGridField15.Name = "pivotGridField15";
            // 
            // pivotGridField16
            // 
            this.pivotGridField16.AreaIndex = 8;
            this.pivotGridField16.FieldName = "REFERENCE";
            this.pivotGridField16.Name = "pivotGridField16";
            // 
            // pivotGridField17
            // 
            this.pivotGridField17.AreaIndex = 9;
            this.pivotGridField17.FieldName = "DESCRIPTION";
            this.pivotGridField17.Name = "pivotGridField17";
            // 
            // pivotGridField18
            // 
            this.pivotGridField18.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotGridField18.AreaIndex = 0;
            this.pivotGridField18.FieldName = "PAYMENT_METHOD";
            this.pivotGridField18.Name = "pivotGridField18";
            resources.ApplyResources(this.pivotGridField18, "pivotGridField18");
            // 
            // pivotGridField19
            // 
            this.pivotGridField19.AreaIndex = 19;
            this.pivotGridField19.FieldName = "PAY_AT_BANK";
            this.pivotGridField19.Name = "pivotGridField19";
            // 
            // pivotGridField20
            // 
            this.pivotGridField20.AreaIndex = 10;
            this.pivotGridField20.FieldName = "RECEIVE_BY";
            this.pivotGridField20.Name = "pivotGridField20";
            // 
            // pivotGridField21
            // 
            this.pivotGridField21.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField21.AreaIndex = 1;
            this.pivotGridField21.FieldName = "TOTAL_PAY";
            this.pivotGridField21.Name = "pivotGridField21";
            // 
            // pivotGridField22
            // 
            this.pivotGridField22.AreaIndex = 20;
            this.pivotGridField22.FieldName = "TOTAL_VOID";
            this.pivotGridField22.Name = "pivotGridField22";
            // 
            // pivotGridField23
            // 
            this.pivotGridField23.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.pivotGridField23.AreaIndex = 2;
            this.pivotGridField23.FieldName = "TOTAL_PAID";
            this.pivotGridField23.Name = "pivotGridField23";
            // 
            // pivotGridField24
            // 
            this.pivotGridField24.AreaIndex = 11;
            this.pivotGridField24.FieldName = "STATUS";
            this.pivotGridField24.Name = "pivotGridField24";
            // 
            // pivotGridField25
            // 
            this.pivotGridField25.AreaIndex = 12;
            this.pivotGridField25.FieldName = "NOTE";
            this.pivotGridField25.Name = "pivotGridField25";
            // 
            // PageReportAging
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pnContainer);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "PageReportAging";
            this.pnContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pgvAgingReport)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popActions)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudDays)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Panel pnContainer;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField2;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField3;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField4;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField5;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField6;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField7;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField8;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField9;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField10;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField11;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField12;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField13;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField14;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField15;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField16;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField17;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField18;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField19;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField20;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField21;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField22;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField23;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField24;
        private DevExpress.XtraPivotGrid.PivotGridField pivotGridField25;
        private DevExpress.XtraBars.PopupMenu popupExport;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btnExcel;
        private DevExpress.XtraBars.BarButtonItem btnCsv;
        private DevExpress.XtraBars.BarButtonItem btnPdf;
        private DevExpress.XtraBars.BarButtonItem btnPRINT;
        private Panel panel2;
        private Label _lblSubTitle1;
        private Label _lblTitle;
        private Label _lblAddress;
        private Label _lblCompany;
        private ReportDatePicker rdpDate;
        private Label lblDAY;
        private NumericUpDown nudDays;
        private FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.DropDownButton dpExport;
        private DevExpress.XtraEditors.DropDownButton dpAction;
        private DevExpress.XtraBars.PopupMenu popActions;
        private DevExpress.XtraBars.BarButtonItem btnResetToDefault;
        private DevExpress.XtraPivotGrid.PivotGridControl pgvAgingReport;
        private DevExpress.XtraPivotGrid.PivotGridField colCUSTOMER_CODE;
        private DevExpress.XtraPivotGrid.PivotGridField colCUSTOMER_NAME;
        private DevExpress.XtraPivotGrid.PivotGridField colPHONE_NUMBER;
        private DevExpress.XtraPivotGrid.PivotGridField colAREA;
        private DevExpress.XtraPivotGrid.PivotGridField colPOLE;
        private DevExpress.XtraPivotGrid.PivotGridField colBOX;
        private DevExpress.XtraPivotGrid.PivotGridField colNOT_ON_DUE_DATE;
        private DevExpress.XtraPivotGrid.PivotGridField _colP1;
        private DevExpress.XtraPivotGrid.PivotGridField _colP2;
        private DevExpress.XtraPivotGrid.PivotGridField _colP3;
        private DevExpress.XtraPivotGrid.PivotGridField _colP4;
        private DevExpress.XtraPivotGrid.PivotGridField _colP5;
        private DevExpress.XtraPivotGrid.PivotGridField colAMOUNT;
        private DevExpress.XtraPivotGrid.PivotGridField colTOTAL_DUE_AMOUNT;
        private DevExpress.XtraPivotGrid.PivotGridField colPAID_AMOUNT;
        private DevExpress.XtraPivotGrid.PivotGridField colCURRENCY;
        private DevExpress.XtraPivotGrid.PivotGridField colINVOICE_NO;
        private DevExpress.XtraPivotGrid.PivotGridField colINVOICE_DATE_BILLING;
        private DevExpress.XtraPivotGrid.PivotGridField colINVOICE_TITLE;
        private DevExpress.XtraPivotGrid.PivotGridField colSTATUS;
        private DevExpress.XtraPivotGrid.PivotGridField colOPEN_DAYS;
        private DevExpress.XtraPivotGrid.PivotGridField colCUSTOMER_CONNECTION_TYPE;
        private DevExpress.XtraPivotGrid.PivotGridField colCUSTOMER_GROUP;
        private DevExpress.XtraPivotGrid.PivotGridField colCYCLE_NAME;
        private DevExpress.XtraPivotGrid.PivotGridField colINVOICE_ITEM;
        private DevExpress.XtraPivotGrid.PivotGridField colINVOICE_ITEM_TYPE;
        private DevExpress.XtraBars.BarButtonItem bntRaw_Data;
        private DevExpress.XtraPivotGrid.PivotGridField colMETER_CODE;
        private DevExpress.XtraPivotGrid.PivotGridField colROW_NO;
        private DevExpress.XtraBars.BarButtonItem btnSAVE_TEMPLATE;
    }
}
