﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface.PrePaid
{
    partial class PagePrePaid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PagePrePaid));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblTrans = new System.Windows.Forms.Label();
            this.btnPURCHASE_POWER = new SoftTech.Component.ExMenuItem();
            this.btnVOID_CARD = new SoftTech.Component.ExMenuItem();
            this.btnRENEW_POWER_CARC = new SoftTech.Component.ExMenuItem();
            this.btnCREATE_SPECIAL_CARD = new SoftTech.Component.ExMenuItem();
            this.btnREAD_DATA_FROM_METER = new SoftTech.Component.ExMenuItem();
            this.lblCUSTOMER_PREPAID = new System.Windows.Forms.Label();
            this.btnNEW_CUSTOMER = new SoftTech.Component.ExMenuItem();
            this.btnACTIVATE_CUSTOMER = new SoftTech.Component.ExMenuItem();
            this.btnMANAGE_CUSTOMER = new SoftTech.Component.ExMenuItem();
            this.lblSUBSIDY = new System.Windows.Forms.Label();
            this.btnRUN_CREDIT = new SoftTech.Component.ExMenuItem();
            this.btnRUN_CREDIT_HISTORY = new SoftTech.Component.ExMenuItem();
            this.lblREPORTS = new System.Windows.Forms.Label();
            this.btnREPORT_DAILY_SALE_DETAIL = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_DAILY_SALE_SUMMARY = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_PREPAID_BUY_STATS = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_NOT_BUY_CUSTOMER = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_VOID_POWER_BUY = new SoftTech.Component.ExMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.tabPREPAID = new SoftTech.Component.ExTabItem();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.main = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btnPREPAID = new SoftTech.Component.ExTabItem();
            this.btnREPORT_PREPAID_PAYMENT_HISTORY = new SoftTech.Component.ExMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel12);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel16);
            this.splitContainer1.Size = new System.Drawing.Size(674, 516);
            this.splitContainer1.SplitterDistance = 204;
            this.splitContainer1.TabIndex = 27;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel2);
            this.panel12.Controls.Add(this.tabPREPAID);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(204, 516);
            this.panel12.TabIndex = 25;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.panel13);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 23);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(204, 493);
            this.panel2.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.lblTrans);
            this.panel1.Controls.Add(this.btnPURCHASE_POWER);
            this.panel1.Controls.Add(this.btnVOID_CARD);
            this.panel1.Controls.Add(this.btnRENEW_POWER_CARC);
            this.panel1.Controls.Add(this.btnCREATE_SPECIAL_CARD);
            this.panel1.Controls.Add(this.btnREAD_DATA_FROM_METER);
            this.panel1.Controls.Add(this.lblCUSTOMER_PREPAID);
            this.panel1.Controls.Add(this.btnNEW_CUSTOMER);
            this.panel1.Controls.Add(this.btnACTIVATE_CUSTOMER);
            this.panel1.Controls.Add(this.btnMANAGE_CUSTOMER);
            this.panel1.Controls.Add(this.lblSUBSIDY);
            this.panel1.Controls.Add(this.btnRUN_CREDIT);
            this.panel1.Controls.Add(this.btnRUN_CREDIT_HISTORY);
            this.panel1.Controls.Add(this.lblREPORTS);
            this.panel1.Controls.Add(this.btnREPORT_DAILY_SALE_DETAIL);
            this.panel1.Controls.Add(this.btnREPORT_DAILY_SALE_SUMMARY);
            this.panel1.Controls.Add(this.btnREPORT_PREPAID_BUY_STATS);
            this.panel1.Controls.Add(this.btnREPORT_NOT_BUY_CUSTOMER);
            this.panel1.Controls.Add(this.btnREPORT_VOID_POWER_BUY);
            this.panel1.Controls.Add(this.btnREPORT_PREPAID_PAYMENT_HISTORY);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(202, 492);
            this.panel1.TabIndex = 6;
            // 
            // lblTrans
            // 
            this.lblTrans.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTrans.BackColor = System.Drawing.Color.Silver;
            this.lblTrans.Location = new System.Drawing.Point(0, 0);
            this.lblTrans.Margin = new System.Windows.Forms.Padding(0);
            this.lblTrans.Name = "lblTrans";
            this.lblTrans.Size = new System.Drawing.Size(320, 24);
            this.lblTrans.TabIndex = 0;
            this.lblTrans.Text = "ប្រតិបត្តិការណ៍";
            this.lblTrans.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnPURCHASE_POWER
            // 
            this.btnPURCHASE_POWER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPURCHASE_POWER.BackColor = System.Drawing.Color.Transparent;
            this.btnPURCHASE_POWER.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnPURCHASE_POWER.Image = ((System.Drawing.Image)(resources.GetObject("btnPURCHASE_POWER.Image")));
            this.btnPURCHASE_POWER.Location = new System.Drawing.Point(0, 24);
            this.btnPURCHASE_POWER.Margin = new System.Windows.Forms.Padding(0);
            this.btnPURCHASE_POWER.Name = "btnPURCHASE_POWER";
            this.btnPURCHASE_POWER.Selected = false;
            this.btnPURCHASE_POWER.Size = new System.Drawing.Size(320, 24);
            this.btnPURCHASE_POWER.TabIndex = 2;
            this.btnPURCHASE_POWER.Text = "លក់អគ្គិសនី";
            this.btnPURCHASE_POWER.Click += new System.EventHandler(this.btnPurchasePower_Click);
            // 
            // btnVOID_CARD
            // 
            this.btnVOID_CARD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVOID_CARD.BackColor = System.Drawing.Color.Transparent;
            this.btnVOID_CARD.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnVOID_CARD.Image = ((System.Drawing.Image)(resources.GetObject("btnVOID_CARD.Image")));
            this.btnVOID_CARD.Location = new System.Drawing.Point(0, 48);
            this.btnVOID_CARD.Margin = new System.Windows.Forms.Padding(0);
            this.btnVOID_CARD.Name = "btnVOID_CARD";
            this.btnVOID_CARD.Selected = false;
            this.btnVOID_CARD.Size = new System.Drawing.Size(320, 24);
            this.btnVOID_CARD.TabIndex = 3;
            this.btnVOID_CARD.Text = "លុបការទិញអគ្គិសនី";
            this.btnVOID_CARD.Click += new System.EventHandler(this.btnVoidCard_Click);
            // 
            // btnRENEW_POWER_CARC
            // 
            this.btnRENEW_POWER_CARC.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRENEW_POWER_CARC.BackColor = System.Drawing.Color.Transparent;
            this.btnRENEW_POWER_CARC.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnRENEW_POWER_CARC.Image = ((System.Drawing.Image)(resources.GetObject("btnRENEW_POWER_CARC.Image")));
            this.btnRENEW_POWER_CARC.Location = new System.Drawing.Point(0, 72);
            this.btnRENEW_POWER_CARC.Margin = new System.Windows.Forms.Padding(0);
            this.btnRENEW_POWER_CARC.Name = "btnRENEW_POWER_CARC";
            this.btnRENEW_POWER_CARC.Selected = false;
            this.btnRENEW_POWER_CARC.Size = new System.Drawing.Size(320, 24);
            this.btnRENEW_POWER_CARC.TabIndex = 4;
            this.btnRENEW_POWER_CARC.Text = "បង្កើតកាតទិញអគ្គិសនីថ្មី";
            this.btnRENEW_POWER_CARC.Click += new System.EventHandler(this.btnRenewCard_Click);
            // 
            // btnCREATE_SPECIAL_CARD
            // 
            this.btnCREATE_SPECIAL_CARD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCREATE_SPECIAL_CARD.BackColor = System.Drawing.Color.Transparent;
            this.btnCREATE_SPECIAL_CARD.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnCREATE_SPECIAL_CARD.Image = ((System.Drawing.Image)(resources.GetObject("btnCREATE_SPECIAL_CARD.Image")));
            this.btnCREATE_SPECIAL_CARD.Location = new System.Drawing.Point(0, 96);
            this.btnCREATE_SPECIAL_CARD.Margin = new System.Windows.Forms.Padding(0);
            this.btnCREATE_SPECIAL_CARD.Name = "btnCREATE_SPECIAL_CARD";
            this.btnCREATE_SPECIAL_CARD.Selected = false;
            this.btnCREATE_SPECIAL_CARD.Size = new System.Drawing.Size(320, 24);
            this.btnCREATE_SPECIAL_CARD.TabIndex = 5;
            this.btnCREATE_SPECIAL_CARD.Text = "បង្កើតកាតពិសេស";
            this.btnCREATE_SPECIAL_CARD.Click += new System.EventHandler(this.btnCreateCard_Click);
            // 
            // btnREAD_DATA_FROM_METER
            // 
            this.btnREAD_DATA_FROM_METER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnREAD_DATA_FROM_METER.BackColor = System.Drawing.Color.Transparent;
            this.btnREAD_DATA_FROM_METER.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnREAD_DATA_FROM_METER.Image = ((System.Drawing.Image)(resources.GetObject("btnREAD_DATA_FROM_METER.Image")));
            this.btnREAD_DATA_FROM_METER.Location = new System.Drawing.Point(0, 120);
            this.btnREAD_DATA_FROM_METER.Margin = new System.Windows.Forms.Padding(0);
            this.btnREAD_DATA_FROM_METER.Name = "btnREAD_DATA_FROM_METER";
            this.btnREAD_DATA_FROM_METER.Selected = false;
            this.btnREAD_DATA_FROM_METER.Size = new System.Drawing.Size(320, 24);
            this.btnREAD_DATA_FROM_METER.TabIndex = 25;
            this.btnREAD_DATA_FROM_METER.Text = "ពិនិត្យព័ត៌មានលើកុងទ័រ";
            this.btnREAD_DATA_FROM_METER.Click += new System.EventHandler(this.btnReadDataFromMeter_Click);
            // 
            // lblCUSTOMER_PREPAID
            // 
            this.lblCUSTOMER_PREPAID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCUSTOMER_PREPAID.BackColor = System.Drawing.Color.Silver;
            this.lblCUSTOMER_PREPAID.Location = new System.Drawing.Point(0, 144);
            this.lblCUSTOMER_PREPAID.Margin = new System.Windows.Forms.Padding(0);
            this.lblCUSTOMER_PREPAID.Name = "lblCUSTOMER_PREPAID";
            this.lblCUSTOMER_PREPAID.Size = new System.Drawing.Size(320, 24);
            this.lblCUSTOMER_PREPAID.TabIndex = 6;
            this.lblCUSTOMER_PREPAID.Text = "អតិថិជនជនបង់ប្រាក់មុន";
            this.lblCUSTOMER_PREPAID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnNEW_CUSTOMER
            // 
            this.btnNEW_CUSTOMER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNEW_CUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.btnNEW_CUSTOMER.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnNEW_CUSTOMER.Image = ((System.Drawing.Image)(resources.GetObject("btnNEW_CUSTOMER.Image")));
            this.btnNEW_CUSTOMER.Location = new System.Drawing.Point(0, 168);
            this.btnNEW_CUSTOMER.Margin = new System.Windows.Forms.Padding(0);
            this.btnNEW_CUSTOMER.Name = "btnNEW_CUSTOMER";
            this.btnNEW_CUSTOMER.Selected = false;
            this.btnNEW_CUSTOMER.Size = new System.Drawing.Size(320, 24);
            this.btnNEW_CUSTOMER.TabIndex = 1;
            this.btnNEW_CUSTOMER.Text = "អតិថិជនថ្មី";
            this.btnNEW_CUSTOMER.Click += new System.EventHandler(this.btnNewCustomer_Click);
            // 
            // btnACTIVATE_CUSTOMER
            // 
            this.btnACTIVATE_CUSTOMER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnACTIVATE_CUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.btnACTIVATE_CUSTOMER.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnACTIVATE_CUSTOMER.Image = ((System.Drawing.Image)(resources.GetObject("btnACTIVATE_CUSTOMER.Image")));
            this.btnACTIVATE_CUSTOMER.Location = new System.Drawing.Point(0, 192);
            this.btnACTIVATE_CUSTOMER.Margin = new System.Windows.Forms.Padding(0);
            this.btnACTIVATE_CUSTOMER.Name = "btnACTIVATE_CUSTOMER";
            this.btnACTIVATE_CUSTOMER.Selected = false;
            this.btnACTIVATE_CUSTOMER.Size = new System.Drawing.Size(320, 24);
            this.btnACTIVATE_CUSTOMER.TabIndex = 8;
            this.btnACTIVATE_CUSTOMER.Text = "ចាប់ផ្តើមប្រើ";
            this.btnACTIVATE_CUSTOMER.Click += new System.EventHandler(this.btnCustomerActivate_Click);
            // 
            // btnMANAGE_CUSTOMER
            // 
            this.btnMANAGE_CUSTOMER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMANAGE_CUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.btnMANAGE_CUSTOMER.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnMANAGE_CUSTOMER.Image = ((System.Drawing.Image)(resources.GetObject("btnMANAGE_CUSTOMER.Image")));
            this.btnMANAGE_CUSTOMER.Location = new System.Drawing.Point(0, 216);
            this.btnMANAGE_CUSTOMER.Margin = new System.Windows.Forms.Padding(0);
            this.btnMANAGE_CUSTOMER.Name = "btnMANAGE_CUSTOMER";
            this.btnMANAGE_CUSTOMER.Selected = false;
            this.btnMANAGE_CUSTOMER.Size = new System.Drawing.Size(320, 24);
            this.btnMANAGE_CUSTOMER.TabIndex = 7;
            this.btnMANAGE_CUSTOMER.Text = "គ្រប់គ្រងអតិថិជន";
            this.btnMANAGE_CUSTOMER.Click += new System.EventHandler(this.btnCustomer_Click);
            // 
            // lblSUBSIDY
            // 
            this.lblSUBSIDY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSUBSIDY.BackColor = System.Drawing.Color.Silver;
            this.lblSUBSIDY.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblSUBSIDY.Location = new System.Drawing.Point(0, 240);
            this.lblSUBSIDY.Margin = new System.Windows.Forms.Padding(0);
            this.lblSUBSIDY.Name = "lblSUBSIDY";
            this.lblSUBSIDY.Size = new System.Drawing.Size(320, 24);
            this.lblSUBSIDY.TabIndex = 27;
            this.lblSUBSIDY.Text = "ការឧបត្ថម្ភធន";
            this.lblSUBSIDY.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRUN_CREDIT
            // 
            this.btnRUN_CREDIT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRUN_CREDIT.BackColor = System.Drawing.Color.Transparent;
            this.btnRUN_CREDIT.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnRUN_CREDIT.Image = ((System.Drawing.Image)(resources.GetObject("btnRUN_CREDIT.Image")));
            this.btnRUN_CREDIT.Location = new System.Drawing.Point(0, 264);
            this.btnRUN_CREDIT.Margin = new System.Windows.Forms.Padding(0);
            this.btnRUN_CREDIT.Name = "btnRUN_CREDIT";
            this.btnRUN_CREDIT.Selected = false;
            this.btnRUN_CREDIT.Size = new System.Drawing.Size(320, 24);
            this.btnRUN_CREDIT.TabIndex = 28;
            this.btnRUN_CREDIT.Text = "ការទូទាត់ប្រចាំខែ";
            this.btnRUN_CREDIT.Click += new System.EventHandler(this.btnPrepaidRunCredit_Click);
            // 
            // btnRUN_CREDIT_HISTORY
            // 
            this.btnRUN_CREDIT_HISTORY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRUN_CREDIT_HISTORY.BackColor = System.Drawing.Color.Transparent;
            this.btnRUN_CREDIT_HISTORY.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnRUN_CREDIT_HISTORY.Image = ((System.Drawing.Image)(resources.GetObject("btnRUN_CREDIT_HISTORY.Image")));
            this.btnRUN_CREDIT_HISTORY.Location = new System.Drawing.Point(0, 288);
            this.btnRUN_CREDIT_HISTORY.Margin = new System.Windows.Forms.Padding(0);
            this.btnRUN_CREDIT_HISTORY.Name = "btnRUN_CREDIT_HISTORY";
            this.btnRUN_CREDIT_HISTORY.Selected = false;
            this.btnRUN_CREDIT_HISTORY.Size = new System.Drawing.Size(320, 24);
            this.btnRUN_CREDIT_HISTORY.TabIndex = 29;
            this.btnRUN_CREDIT_HISTORY.Text = "ប្រវត្តិទូទាត់";
            this.btnRUN_CREDIT_HISTORY.Click += new System.EventHandler(this.exMenuItem1_Click);
            // 
            // lblREPORTS
            // 
            this.lblREPORTS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblREPORTS.BackColor = System.Drawing.Color.Silver;
            this.lblREPORTS.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblREPORTS.Location = new System.Drawing.Point(0, 312);
            this.lblREPORTS.Margin = new System.Windows.Forms.Padding(0);
            this.lblREPORTS.Name = "lblREPORTS";
            this.lblREPORTS.Size = new System.Drawing.Size(320, 24);
            this.lblREPORTS.TabIndex = 20;
            this.lblREPORTS.Text = "របាយការណ៌";
            this.lblREPORTS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnREPORT_DAILY_SALE_DETAIL
            // 
            this.btnREPORT_DAILY_SALE_DETAIL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnREPORT_DAILY_SALE_DETAIL.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_DAILY_SALE_DETAIL.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnREPORT_DAILY_SALE_DETAIL.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_DAILY_SALE_DETAIL.Image")));
            this.btnREPORT_DAILY_SALE_DETAIL.Location = new System.Drawing.Point(0, 336);
            this.btnREPORT_DAILY_SALE_DETAIL.Margin = new System.Windows.Forms.Padding(0);
            this.btnREPORT_DAILY_SALE_DETAIL.Name = "btnREPORT_DAILY_SALE_DETAIL";
            this.btnREPORT_DAILY_SALE_DETAIL.Selected = false;
            this.btnREPORT_DAILY_SALE_DETAIL.Size = new System.Drawing.Size(320, 24);
            this.btnREPORT_DAILY_SALE_DETAIL.TabIndex = 21;
            this.btnREPORT_DAILY_SALE_DETAIL.Text = "ចំណូលពីការលក់អគ្គិសនី";
            this.btnREPORT_DAILY_SALE_DETAIL.Click += new System.EventHandler(this.btnReportDailySaleDetail_Click);
            // 
            // btnREPORT_DAILY_SALE_SUMMARY
            // 
            this.btnREPORT_DAILY_SALE_SUMMARY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnREPORT_DAILY_SALE_SUMMARY.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_DAILY_SALE_SUMMARY.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnREPORT_DAILY_SALE_SUMMARY.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_DAILY_SALE_SUMMARY.Image")));
            this.btnREPORT_DAILY_SALE_SUMMARY.Location = new System.Drawing.Point(0, 360);
            this.btnREPORT_DAILY_SALE_SUMMARY.Margin = new System.Windows.Forms.Padding(0);
            this.btnREPORT_DAILY_SALE_SUMMARY.Name = "btnREPORT_DAILY_SALE_SUMMARY";
            this.btnREPORT_DAILY_SALE_SUMMARY.Selected = false;
            this.btnREPORT_DAILY_SALE_SUMMARY.Size = new System.Drawing.Size(320, 24);
            this.btnREPORT_DAILY_SALE_SUMMARY.TabIndex = 22;
            this.btnREPORT_DAILY_SALE_SUMMARY.Text = "ចំណូលពីការលក់អគ្គិសនីសង្ខេប";
            this.btnREPORT_DAILY_SALE_SUMMARY.Click += new System.EventHandler(this.btnReportPrepaidMonthToDateSaleSummary_Click);
            // 
            // btnREPORT_PREPAID_BUY_STATS
            // 
            this.btnREPORT_PREPAID_BUY_STATS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnREPORT_PREPAID_BUY_STATS.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_PREPAID_BUY_STATS.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnREPORT_PREPAID_BUY_STATS.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_PREPAID_BUY_STATS.Image")));
            this.btnREPORT_PREPAID_BUY_STATS.Location = new System.Drawing.Point(0, 384);
            this.btnREPORT_PREPAID_BUY_STATS.Margin = new System.Windows.Forms.Padding(0);
            this.btnREPORT_PREPAID_BUY_STATS.Name = "btnREPORT_PREPAID_BUY_STATS";
            this.btnREPORT_PREPAID_BUY_STATS.Selected = false;
            this.btnREPORT_PREPAID_BUY_STATS.Size = new System.Drawing.Size(320, 24);
            this.btnREPORT_PREPAID_BUY_STATS.TabIndex = 23;
            this.btnREPORT_PREPAID_BUY_STATS.Text = "ស្ថិតិការទិញថាមពលអគ្គិសនី";
            this.btnREPORT_PREPAID_BUY_STATS.Click += new System.EventHandler(this.btnReportPrepaidBuyingFrequency_Click);
            // 
            // btnREPORT_NOT_BUY_CUSTOMER
            // 
            this.btnREPORT_NOT_BUY_CUSTOMER.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnREPORT_NOT_BUY_CUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_NOT_BUY_CUSTOMER.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnREPORT_NOT_BUY_CUSTOMER.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_NOT_BUY_CUSTOMER.Image")));
            this.btnREPORT_NOT_BUY_CUSTOMER.Location = new System.Drawing.Point(0, 408);
            this.btnREPORT_NOT_BUY_CUSTOMER.Margin = new System.Windows.Forms.Padding(0);
            this.btnREPORT_NOT_BUY_CUSTOMER.Name = "btnREPORT_NOT_BUY_CUSTOMER";
            this.btnREPORT_NOT_BUY_CUSTOMER.Selected = false;
            this.btnREPORT_NOT_BUY_CUSTOMER.Size = new System.Drawing.Size(320, 24);
            this.btnREPORT_NOT_BUY_CUSTOMER.TabIndex = 24;
            this.btnREPORT_NOT_BUY_CUSTOMER.Text = "អតិថិជនមិនបានទិញថាមពល";
            this.btnREPORT_NOT_BUY_CUSTOMER.Click += new System.EventHandler(this.btnNotBuyPower_Click);
            // 
            // btnREPORT_VOID_POWER_BUY
            // 
            this.btnREPORT_VOID_POWER_BUY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnREPORT_VOID_POWER_BUY.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_VOID_POWER_BUY.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnREPORT_VOID_POWER_BUY.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_VOID_POWER_BUY.Image")));
            this.btnREPORT_VOID_POWER_BUY.Location = new System.Drawing.Point(0, 432);
            this.btnREPORT_VOID_POWER_BUY.Margin = new System.Windows.Forms.Padding(0);
            this.btnREPORT_VOID_POWER_BUY.Name = "btnREPORT_VOID_POWER_BUY";
            this.btnREPORT_VOID_POWER_BUY.Selected = false;
            this.btnREPORT_VOID_POWER_BUY.Size = new System.Drawing.Size(320, 24);
            this.btnREPORT_VOID_POWER_BUY.TabIndex = 26;
            this.btnREPORT_VOID_POWER_BUY.Text = "លុបការទិញថាមពលអគិ្គសនី";
            this.btnREPORT_VOID_POWER_BUY.Click += new System.EventHandler(this.btnReportVoidPower_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(203, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1, 492);
            this.panel3.TabIndex = 5;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Silver;
            this.panel15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel15.Location = new System.Drawing.Point(1, 492);
            this.panel15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(203, 1);
            this.panel15.TabIndex = 4;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Silver;
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1, 493);
            this.panel13.TabIndex = 2;
            // 
            // tabPREPAID
            // 
            this.tabPREPAID.BackColor = System.Drawing.Color.Transparent;
            this.tabPREPAID.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabPREPAID.Image = ((System.Drawing.Image)(resources.GetObject("tabPREPAID.Image")));
            this.tabPREPAID.IsTitle = true;
            this.tabPREPAID.Location = new System.Drawing.Point(0, 0);
            this.tabPREPAID.Margin = new System.Windows.Forms.Padding(0);
            this.tabPREPAID.Name = "tabPREPAID";
            this.tabPREPAID.Selected = false;
            this.tabPREPAID.Size = new System.Drawing.Size(204, 23);
            this.tabPREPAID.TabIndex = 0;
            this.tabPREPAID.Text = "បង់ប្រាក់មុន";
            this.tabPREPAID.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.panel20);
            this.panel16.Controls.Add(this.btnPREPAID);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(466, 516);
            this.panel16.TabIndex = 26;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.main);
            this.panel20.Controls.Add(this.panel17);
            this.panel20.Controls.Add(this.panel18);
            this.panel20.Controls.Add(this.panel19);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(0, 23);
            this.panel20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(466, 493);
            this.panel20.TabIndex = 5;
            // 
            // main
            // 
            this.main.BackColor = System.Drawing.Color.White;
            this.main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main.Location = new System.Drawing.Point(1, 0);
            this.main.Name = "main";
            this.main.Size = new System.Drawing.Size(464, 492);
            this.main.TabIndex = 6;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Silver;
            this.panel17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel17.Location = new System.Drawing.Point(1, 492);
            this.panel17.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(464, 1);
            this.panel17.TabIndex = 4;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Silver;
            this.panel18.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel18.Location = new System.Drawing.Point(465, 0);
            this.panel18.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(1, 493);
            this.panel18.TabIndex = 3;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Silver;
            this.panel19.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1, 493);
            this.panel19.TabIndex = 2;
            // 
            // btnPREPAID
            // 
            this.btnPREPAID.BackColor = System.Drawing.Color.Transparent;
            this.btnPREPAID.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPREPAID.Image = null;
            this.btnPREPAID.IsTitle = true;
            this.btnPREPAID.Location = new System.Drawing.Point(0, 0);
            this.btnPREPAID.Margin = new System.Windows.Forms.Padding(0);
            this.btnPREPAID.Name = "btnPREPAID";
            this.btnPREPAID.Selected = false;
            this.btnPREPAID.Size = new System.Drawing.Size(466, 23);
            this.btnPREPAID.TabIndex = 0;
            this.btnPREPAID.Text = "អតិថិជន";
            this.btnPREPAID.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnREPORT_PREPAID_PAYMENT_HISTORY
            // 
            this.btnREPORT_PREPAID_PAYMENT_HISTORY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnREPORT_PREPAID_PAYMENT_HISTORY.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_PREPAID_PAYMENT_HISTORY.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.btnREPORT_PREPAID_PAYMENT_HISTORY.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_PREPAID_PAYMENT_HISTORY.Image")));
            this.btnREPORT_PREPAID_PAYMENT_HISTORY.Location = new System.Drawing.Point(0, 456);
            this.btnREPORT_PREPAID_PAYMENT_HISTORY.Margin = new System.Windows.Forms.Padding(0);
            this.btnREPORT_PREPAID_PAYMENT_HISTORY.Name = "btnREPORT_PREPAID_PAYMENT_HISTORY";
            this.btnREPORT_PREPAID_PAYMENT_HISTORY.Selected = false;
            this.btnREPORT_PREPAID_PAYMENT_HISTORY.Size = new System.Drawing.Size(320, 24);
            this.btnREPORT_PREPAID_PAYMENT_HISTORY.TabIndex = 30;
            this.btnREPORT_PREPAID_PAYMENT_HISTORY.Text = "ប្រវត្តិនៃទូទាត់";
            this.btnREPORT_PREPAID_PAYMENT_HISTORY.Click += new System.EventHandler(this.btnREPORT_PREPAID_PAYMENT_HISTORY_Click);
            // 
            // PagePrePaid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Khmer OS System", 8.25F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PagePrePaid";
            this.Size = new System.Drawing.Size(674, 516);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel16;
        private Panel panel20;
        private ExTabItem btnPREPAID;
        private Panel panel12;
        private ExTabItem tabPREPAID;
        private SplitContainer splitContainer1;
        private Panel main;
        private Panel panel17;
        private Panel panel18;
        private Panel panel19;
        private Panel panel2;
        private Panel panel15;
        private Panel panel13;
        private Panel panel3;
        private FlowLayoutPanel panel1;
        private ExMenuItem btnMANAGE_CUSTOMER;
        private ExMenuItem btnRENEW_POWER_CARC;
        private ExMenuItem btnCREATE_SPECIAL_CARD;
        private Label lblCUSTOMER_PREPAID;
        private ExMenuItem btnPURCHASE_POWER;
        private Label lblTrans;
        private ExMenuItem btnNEW_CUSTOMER;
        private ExMenuItem btnACTIVATE_CUSTOMER;
        private ExMenuItem btnVOID_CARD;
        private Label lblREPORTS;
        private ExMenuItem btnREPORT_DAILY_SALE_DETAIL;
        private ExMenuItem btnREPORT_DAILY_SALE_SUMMARY;
        private ExMenuItem btnREPORT_PREPAID_BUY_STATS;
        private ExMenuItem btnREPORT_NOT_BUY_CUSTOMER;
        private ExMenuItem btnREPORT_VOID_POWER_BUY;
        private Label lblSUBSIDY;
        private ExMenuItem btnRUN_CREDIT;
        private ExMenuItem btnRUN_CREDIT_HISTORY;
        private ExMenuItem btnREAD_DATA_FROM_METER;
        private ExMenuItem btnREPORT_PREPAID_PAYMENT_HISTORY;
    }
}
