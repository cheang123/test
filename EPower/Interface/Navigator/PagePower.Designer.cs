﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PagePower
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PagePower));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblEAC_REPORT = new System.Windows.Forms.Label();
            this.btnREPORT_QUARTERLY = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_ANNUAL = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_SUBSIDY = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_CUSTOMER_USAGE_STATISTIC = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_OPS = new SoftTech.Component.ExMenuItem();
            this.btnE_A_C = new SoftTech.Component.ExMenuItem();
            this.lblTRANSACTION = new System.Windows.Forms.Label();
            this.btnINCOME = new SoftTech.Component.ExMenuItem();
            this.btnEXCHANGE_RATE = new SoftTech.Component.ExMenuItem();
            this.lblSETUP_ACCOUNT_CHART = new System.Windows.Forms.Label();
            this.btnACCOUNT_CHART = new SoftTech.Component.ExMenuItem();
            this.lblPOWER_DISTRIBUTION = new System.Windows.Forms.Label();
            this.btnLICENSEE = new SoftTech.Component.ExMenuItem();
            this.btnDISTRIBUTION_FACILITY = new SoftTech.Component.ExMenuItem();
            this.btnLICENSE_VILLAGE = new SoftTech.Component.ExMenuItem();
            this.lblPURCHASE_POWER = new System.Windows.Forms.Label();
            this.btnPOWER_SOURCE = new SoftTech.Component.ExMenuItem();
            this.btnPOWER_PURCHASE = new SoftTech.Component.ExMenuItem();
            this.lblGENERATION = new System.Windows.Forms.Label();
            this.btnPOWER_GENERATION = new SoftTech.Component.ExMenuItem();
            this.btnFUEL_PURCHASE = new SoftTech.Component.ExMenuItem();
            this.btnFUEL_SOURCE = new SoftTech.Component.ExMenuItem();
            this.btnGENERATOR = new SoftTech.Component.ExMenuItem();
            this.lblPOWER_USAGE = new System.Windows.Forms.Label();
            this.btnPOWER_ADJUSTMENT = new SoftTech.Component.ExMenuItem();
            this.btnPOWER_USAGE_BY_TRANSFORMER = new SoftTech.Component.ExMenuItem();
            this.lblMESUREMENT = new System.Windows.Forms.Label();
            this.btnAMPARE = new SoftTech.Component.ExMenuItem();
            this.btnVOLTAGE = new SoftTech.Component.ExMenuItem();
            this.btnPHASE = new SoftTech.Component.ExMenuItem();
            this.btnCAPACITY = new SoftTech.Component.ExMenuItem();
            this.btnSHIELD = new SoftTech.Component.ExMenuItem();
            this.btnCONSTANT = new SoftTech.Component.ExMenuItem();
            this.lblREPORTS = new System.Windows.Forms.Label();
            this.btnCUSTOMER_STATISTICS = new SoftTech.Component.ExMenuItem();
            this.btnSUMMARY_METER_BY_TRANSFORMER = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_INCOME = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_EXPENSE = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_PROFIT_LOSS = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_FIX_ASSET = new SoftTech.Component.ExMenuItem();
            this.btnREPORT_FIX_ASSET_DEPRECIATION = new SoftTech.Component.ExMenuItem();
            this.lblFIX_ASSET = new System.Windows.Forms.Label();
            this.btnFIX_ASSET_TYPE = new SoftTech.Component.ExMenuItem();
            this.btnFIX_ASSET_ITEM = new SoftTech.Component.ExMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.lblPOWER_MANAGEMENT_1 = new SoftTech.Component.ExTabItem();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.main = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.POWER_MANAGEMENT_2 = new SoftTech.Component.ExTabItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel12);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel16);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel2);
            this.panel12.Controls.Add(this.lblPOWER_MANAGEMENT_1);
            resources.ApplyResources(this.panel12, "panel12");
            this.panel12.Name = "panel12";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.panel13);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.lblEAC_REPORT);
            this.panel1.Controls.Add(this.btnREPORT_QUARTERLY);
            this.panel1.Controls.Add(this.btnREPORT_ANNUAL);
            this.panel1.Controls.Add(this.btnREPORT_SUBSIDY);
            this.panel1.Controls.Add(this.btnREPORT_CUSTOMER_USAGE_STATISTIC);
            this.panel1.Controls.Add(this.btnREPORT_OPS);
            this.panel1.Controls.Add(this.btnE_A_C);
            this.panel1.Controls.Add(this.lblTRANSACTION);
            this.panel1.Controls.Add(this.btnINCOME);
            this.panel1.Controls.Add(this.btnEXCHANGE_RATE);
            this.panel1.Controls.Add(this.lblSETUP_ACCOUNT_CHART);
            this.panel1.Controls.Add(this.btnACCOUNT_CHART);
            this.panel1.Controls.Add(this.lblPOWER_DISTRIBUTION);
            this.panel1.Controls.Add(this.btnLICENSEE);
            this.panel1.Controls.Add(this.btnDISTRIBUTION_FACILITY);
            this.panel1.Controls.Add(this.btnLICENSE_VILLAGE);
            this.panel1.Controls.Add(this.lblPURCHASE_POWER);
            this.panel1.Controls.Add(this.btnPOWER_SOURCE);
            this.panel1.Controls.Add(this.btnPOWER_PURCHASE);
            this.panel1.Controls.Add(this.lblGENERATION);
            this.panel1.Controls.Add(this.btnPOWER_GENERATION);
            this.panel1.Controls.Add(this.btnFUEL_PURCHASE);
            this.panel1.Controls.Add(this.btnFUEL_SOURCE);
            this.panel1.Controls.Add(this.btnGENERATOR);
            this.panel1.Controls.Add(this.lblPOWER_USAGE);
            this.panel1.Controls.Add(this.btnPOWER_ADJUSTMENT);
            this.panel1.Controls.Add(this.btnPOWER_USAGE_BY_TRANSFORMER);
            this.panel1.Controls.Add(this.lblMESUREMENT);
            this.panel1.Controls.Add(this.btnAMPARE);
            this.panel1.Controls.Add(this.btnVOLTAGE);
            this.panel1.Controls.Add(this.btnPHASE);
            this.panel1.Controls.Add(this.btnCAPACITY);
            this.panel1.Controls.Add(this.btnSHIELD);
            this.panel1.Controls.Add(this.btnCONSTANT);
            this.panel1.Controls.Add(this.lblREPORTS);
            this.panel1.Controls.Add(this.btnCUSTOMER_STATISTICS);
            this.panel1.Controls.Add(this.btnSUMMARY_METER_BY_TRANSFORMER);
            this.panel1.Controls.Add(this.btnREPORT_INCOME);
            this.panel1.Controls.Add(this.btnREPORT_EXPENSE);
            this.panel1.Controls.Add(this.btnREPORT_PROFIT_LOSS);
            this.panel1.Controls.Add(this.btnREPORT_FIX_ASSET);
            this.panel1.Controls.Add(this.btnREPORT_FIX_ASSET_DEPRECIATION);
            this.panel1.Controls.Add(this.lblFIX_ASSET);
            this.panel1.Controls.Add(this.btnFIX_ASSET_TYPE);
            this.panel1.Controls.Add(this.btnFIX_ASSET_ITEM);
            this.panel1.Name = "panel1";
            // 
            // lblEAC_REPORT
            // 
            resources.ApplyResources(this.lblEAC_REPORT, "lblEAC_REPORT");
            this.lblEAC_REPORT.BackColor = System.Drawing.Color.Silver;
            this.lblEAC_REPORT.Name = "lblEAC_REPORT";
            // 
            // btnREPORT_QUARTERLY
            // 
            resources.ApplyResources(this.btnREPORT_QUARTERLY, "btnREPORT_QUARTERLY");
            this.btnREPORT_QUARTERLY.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_QUARTERLY.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_QUARTERLY.Image")));
            this.btnREPORT_QUARTERLY.Name = "btnREPORT_QUARTERLY";
            this.btnREPORT_QUARTERLY.Selected = false;
            this.btnREPORT_QUARTERLY.Click += new System.EventHandler(this.btnREPORT_QUARTERLY_Click);
            // 
            // btnREPORT_ANNUAL
            // 
            resources.ApplyResources(this.btnREPORT_ANNUAL, "btnREPORT_ANNUAL");
            this.btnREPORT_ANNUAL.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_ANNUAL.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_ANNUAL.Image")));
            this.btnREPORT_ANNUAL.Name = "btnREPORT_ANNUAL";
            this.btnREPORT_ANNUAL.Selected = false;
            this.btnREPORT_ANNUAL.Click += new System.EventHandler(this.btnREPORT_ANNUAL_Click);
            // 
            // btnREPORT_SUBSIDY
            // 
            resources.ApplyResources(this.btnREPORT_SUBSIDY, "btnREPORT_SUBSIDY");
            this.btnREPORT_SUBSIDY.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_SUBSIDY.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_SUBSIDY.Image")));
            this.btnREPORT_SUBSIDY.Name = "btnREPORT_SUBSIDY";
            this.btnREPORT_SUBSIDY.Selected = false;
            this.btnREPORT_SUBSIDY.Click += new System.EventHandler(this.btnREPORT_SUBSIDY_Click);
            // 
            // btnREPORT_CUSTOMER_USAGE_STATISTIC
            // 
            resources.ApplyResources(this.btnREPORT_CUSTOMER_USAGE_STATISTIC, "btnREPORT_CUSTOMER_USAGE_STATISTIC");
            this.btnREPORT_CUSTOMER_USAGE_STATISTIC.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_CUSTOMER_USAGE_STATISTIC.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_CUSTOMER_USAGE_STATISTIC.Image")));
            this.btnREPORT_CUSTOMER_USAGE_STATISTIC.Name = "btnREPORT_CUSTOMER_USAGE_STATISTIC";
            this.btnREPORT_CUSTOMER_USAGE_STATISTIC.Selected = false;
            this.btnREPORT_CUSTOMER_USAGE_STATISTIC.Click += new System.EventHandler(this.btnREPORT_CUSTOMER_USAGE_STATISTIC_Click);
            // 
            // btnREPORT_OPS
            // 
            resources.ApplyResources(this.btnREPORT_OPS, "btnREPORT_OPS");
            this.btnREPORT_OPS.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_OPS.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_OPS.Image")));
            this.btnREPORT_OPS.Name = "btnREPORT_OPS";
            this.btnREPORT_OPS.Selected = false;
            this.btnREPORT_OPS.Click += new System.EventHandler(this.btnREPORT_OPS_Click);
            // 
            // btnE_A_C
            // 
            resources.ApplyResources(this.btnE_A_C, "btnE_A_C");
            this.btnE_A_C.BackColor = System.Drawing.Color.Transparent;
            this.btnE_A_C.Image = ((System.Drawing.Image)(resources.GetObject("btnE_A_C.Image")));
            this.btnE_A_C.Name = "btnE_A_C";
            this.btnE_A_C.Selected = false;
            this.btnE_A_C.Click += new System.EventHandler(this.btnEAC_Click);
            // 
            // lblTRANSACTION
            // 
            resources.ApplyResources(this.lblTRANSACTION, "lblTRANSACTION");
            this.lblTRANSACTION.BackColor = System.Drawing.Color.Silver;
            this.lblTRANSACTION.Name = "lblTRANSACTION";
            // 
            // btnINCOME
            // 
            resources.ApplyResources(this.btnINCOME, "btnINCOME");
            this.btnINCOME.BackColor = System.Drawing.Color.Transparent;
            this.btnINCOME.Image = ((System.Drawing.Image)(resources.GetObject("btnINCOME.Image")));
            this.btnINCOME.Name = "btnINCOME";
            this.btnINCOME.Selected = false;
            this.btnINCOME.Click += new System.EventHandler(this.btnINCOME_Click);
            // 
            // btnEXCHANGE_RATE
            // 
            resources.ApplyResources(this.btnEXCHANGE_RATE, "btnEXCHANGE_RATE");
            this.btnEXCHANGE_RATE.BackColor = System.Drawing.Color.Transparent;
            this.btnEXCHANGE_RATE.Image = ((System.Drawing.Image)(resources.GetObject("btnEXCHANGE_RATE.Image")));
            this.btnEXCHANGE_RATE.Name = "btnEXCHANGE_RATE";
            this.btnEXCHANGE_RATE.Selected = false;
            this.btnEXCHANGE_RATE.Click += new System.EventHandler(this.btnEXCHANGE_RATE_Click);
            // 
            // lblSETUP_ACCOUNT_CHART
            // 
            resources.ApplyResources(this.lblSETUP_ACCOUNT_CHART, "lblSETUP_ACCOUNT_CHART");
            this.lblSETUP_ACCOUNT_CHART.BackColor = System.Drawing.Color.Silver;
            this.lblSETUP_ACCOUNT_CHART.Name = "lblSETUP_ACCOUNT_CHART";
            // 
            // btnACCOUNT_CHART
            // 
            resources.ApplyResources(this.btnACCOUNT_CHART, "btnACCOUNT_CHART");
            this.btnACCOUNT_CHART.BackColor = System.Drawing.Color.Transparent;
            this.btnACCOUNT_CHART.Image = ((System.Drawing.Image)(resources.GetObject("btnACCOUNT_CHART.Image")));
            this.btnACCOUNT_CHART.Name = "btnACCOUNT_CHART";
            this.btnACCOUNT_CHART.Selected = false;
            this.btnACCOUNT_CHART.Click += new System.EventHandler(this.btnACCOUNT_CHART_Click);
            // 
            // lblPOWER_DISTRIBUTION
            // 
            resources.ApplyResources(this.lblPOWER_DISTRIBUTION, "lblPOWER_DISTRIBUTION");
            this.lblPOWER_DISTRIBUTION.BackColor = System.Drawing.Color.Silver;
            this.lblPOWER_DISTRIBUTION.Name = "lblPOWER_DISTRIBUTION";
            // 
            // btnLICENSEE
            // 
            resources.ApplyResources(this.btnLICENSEE, "btnLICENSEE");
            this.btnLICENSEE.BackColor = System.Drawing.Color.Transparent;
            this.btnLICENSEE.Image = ((System.Drawing.Image)(resources.GetObject("btnLICENSEE.Image")));
            this.btnLICENSEE.Name = "btnLICENSEE";
            this.btnLICENSEE.Selected = false;
            this.btnLICENSEE.Click += new System.EventHandler(this.lblLicensee_Click);
            // 
            // btnDISTRIBUTION_FACILITY
            // 
            resources.ApplyResources(this.btnDISTRIBUTION_FACILITY, "btnDISTRIBUTION_FACILITY");
            this.btnDISTRIBUTION_FACILITY.BackColor = System.Drawing.Color.Transparent;
            this.btnDISTRIBUTION_FACILITY.Image = ((System.Drawing.Image)(resources.GetObject("btnDISTRIBUTION_FACILITY.Image")));
            this.btnDISTRIBUTION_FACILITY.Name = "btnDISTRIBUTION_FACILITY";
            this.btnDISTRIBUTION_FACILITY.Selected = false;
            this.btnDISTRIBUTION_FACILITY.Click += new System.EventHandler(this.btnDistributionFacility_Click);
            // 
            // btnLICENSE_VILLAGE
            // 
            resources.ApplyResources(this.btnLICENSE_VILLAGE, "btnLICENSE_VILLAGE");
            this.btnLICENSE_VILLAGE.BackColor = System.Drawing.Color.Transparent;
            this.btnLICENSE_VILLAGE.Image = ((System.Drawing.Image)(resources.GetObject("btnLICENSE_VILLAGE.Image")));
            this.btnLICENSE_VILLAGE.Name = "btnLICENSE_VILLAGE";
            this.btnLICENSE_VILLAGE.Selected = false;
            this.btnLICENSE_VILLAGE.Click += new System.EventHandler(this.btnLicenseVillage_Click);
            // 
            // lblPURCHASE_POWER
            // 
            resources.ApplyResources(this.lblPURCHASE_POWER, "lblPURCHASE_POWER");
            this.lblPURCHASE_POWER.BackColor = System.Drawing.Color.Silver;
            this.lblPURCHASE_POWER.Name = "lblPURCHASE_POWER";
            // 
            // btnPOWER_SOURCE
            // 
            resources.ApplyResources(this.btnPOWER_SOURCE, "btnPOWER_SOURCE");
            this.btnPOWER_SOURCE.BackColor = System.Drawing.Color.Transparent;
            this.btnPOWER_SOURCE.Image = ((System.Drawing.Image)(resources.GetObject("btnPOWER_SOURCE.Image")));
            this.btnPOWER_SOURCE.Name = "btnPOWER_SOURCE";
            this.btnPOWER_SOURCE.Selected = false;
            this.btnPOWER_SOURCE.Click += new System.EventHandler(this.btnPurchaseAgreement_Click);
            // 
            // btnPOWER_PURCHASE
            // 
            resources.ApplyResources(this.btnPOWER_PURCHASE, "btnPOWER_PURCHASE");
            this.btnPOWER_PURCHASE.BackColor = System.Drawing.Color.Transparent;
            this.btnPOWER_PURCHASE.Image = ((System.Drawing.Image)(resources.GetObject("btnPOWER_PURCHASE.Image")));
            this.btnPOWER_PURCHASE.Name = "btnPOWER_PURCHASE";
            this.btnPOWER_PURCHASE.Selected = false;
            this.btnPOWER_PURCHASE.Click += new System.EventHandler(this.btnPowerPurchase_Click);
            // 
            // lblGENERATION
            // 
            resources.ApplyResources(this.lblGENERATION, "lblGENERATION");
            this.lblGENERATION.BackColor = System.Drawing.Color.Silver;
            this.lblGENERATION.Name = "lblGENERATION";
            // 
            // btnPOWER_GENERATION
            // 
            resources.ApplyResources(this.btnPOWER_GENERATION, "btnPOWER_GENERATION");
            this.btnPOWER_GENERATION.BackColor = System.Drawing.Color.Transparent;
            this.btnPOWER_GENERATION.Image = ((System.Drawing.Image)(resources.GetObject("btnPOWER_GENERATION.Image")));
            this.btnPOWER_GENERATION.Name = "btnPOWER_GENERATION";
            this.btnPOWER_GENERATION.Selected = false;
            this.btnPOWER_GENERATION.Click += new System.EventHandler(this.lblPowerGeneration_Click);
            // 
            // btnFUEL_PURCHASE
            // 
            resources.ApplyResources(this.btnFUEL_PURCHASE, "btnFUEL_PURCHASE");
            this.btnFUEL_PURCHASE.BackColor = System.Drawing.Color.Transparent;
            this.btnFUEL_PURCHASE.Image = ((System.Drawing.Image)(resources.GetObject("btnFUEL_PURCHASE.Image")));
            this.btnFUEL_PURCHASE.Name = "btnFUEL_PURCHASE";
            this.btnFUEL_PURCHASE.Selected = false;
            this.btnFUEL_PURCHASE.Click += new System.EventHandler(this.btnFuelPurchase_Click);
            // 
            // btnFUEL_SOURCE
            // 
            resources.ApplyResources(this.btnFUEL_SOURCE, "btnFUEL_SOURCE");
            this.btnFUEL_SOURCE.BackColor = System.Drawing.Color.Transparent;
            this.btnFUEL_SOURCE.Image = ((System.Drawing.Image)(resources.GetObject("btnFUEL_SOURCE.Image")));
            this.btnFUEL_SOURCE.Name = "btnFUEL_SOURCE";
            this.btnFUEL_SOURCE.Selected = false;
            this.btnFUEL_SOURCE.Click += new System.EventHandler(this.btnFuelSource_Click);
            // 
            // btnGENERATOR
            // 
            resources.ApplyResources(this.btnGENERATOR, "btnGENERATOR");
            this.btnGENERATOR.BackColor = System.Drawing.Color.Transparent;
            this.btnGENERATOR.Image = ((System.Drawing.Image)(resources.GetObject("btnGENERATOR.Image")));
            this.btnGENERATOR.Name = "btnGENERATOR";
            this.btnGENERATOR.Selected = false;
            this.btnGENERATOR.Click += new System.EventHandler(this.lblGenerator_Click);
            // 
            // lblPOWER_USAGE
            // 
            resources.ApplyResources(this.lblPOWER_USAGE, "lblPOWER_USAGE");
            this.lblPOWER_USAGE.BackColor = System.Drawing.Color.Silver;
            this.lblPOWER_USAGE.Name = "lblPOWER_USAGE";
            // 
            // btnPOWER_ADJUSTMENT
            // 
            resources.ApplyResources(this.btnPOWER_ADJUSTMENT, "btnPOWER_ADJUSTMENT");
            this.btnPOWER_ADJUSTMENT.BackColor = System.Drawing.Color.Transparent;
            this.btnPOWER_ADJUSTMENT.Image = ((System.Drawing.Image)(resources.GetObject("btnPOWER_ADJUSTMENT.Image")));
            this.btnPOWER_ADJUSTMENT.Name = "btnPOWER_ADJUSTMENT";
            this.btnPOWER_ADJUSTMENT.Selected = false;
            this.btnPOWER_ADJUSTMENT.Click += new System.EventHandler(this.btnPowerAdjust_Click);
            // 
            // btnPOWER_USAGE_BY_TRANSFORMER
            // 
            resources.ApplyResources(this.btnPOWER_USAGE_BY_TRANSFORMER, "btnPOWER_USAGE_BY_TRANSFORMER");
            this.btnPOWER_USAGE_BY_TRANSFORMER.BackColor = System.Drawing.Color.Transparent;
            this.btnPOWER_USAGE_BY_TRANSFORMER.Image = ((System.Drawing.Image)(resources.GetObject("btnPOWER_USAGE_BY_TRANSFORMER.Image")));
            this.btnPOWER_USAGE_BY_TRANSFORMER.Name = "btnPOWER_USAGE_BY_TRANSFORMER";
            this.btnPOWER_USAGE_BY_TRANSFORMER.Selected = false;
            this.btnPOWER_USAGE_BY_TRANSFORMER.Click += new System.EventHandler(this.btnPowerTransformer_Click);
            // 
            // lblMESUREMENT
            // 
            resources.ApplyResources(this.lblMESUREMENT, "lblMESUREMENT");
            this.lblMESUREMENT.BackColor = System.Drawing.Color.Silver;
            this.lblMESUREMENT.Name = "lblMESUREMENT";
            // 
            // btnAMPARE
            // 
            resources.ApplyResources(this.btnAMPARE, "btnAMPARE");
            this.btnAMPARE.BackColor = System.Drawing.Color.Transparent;
            this.btnAMPARE.Image = ((System.Drawing.Image)(resources.GetObject("btnAMPARE.Image")));
            this.btnAMPARE.Name = "btnAMPARE";
            this.btnAMPARE.Selected = false;
            this.btnAMPARE.Click += new System.EventHandler(this.btnAmpare_Click);
            // 
            // btnVOLTAGE
            // 
            resources.ApplyResources(this.btnVOLTAGE, "btnVOLTAGE");
            this.btnVOLTAGE.BackColor = System.Drawing.Color.Transparent;
            this.btnVOLTAGE.Image = ((System.Drawing.Image)(resources.GetObject("btnVOLTAGE.Image")));
            this.btnVOLTAGE.Name = "btnVOLTAGE";
            this.btnVOLTAGE.Selected = false;
            this.btnVOLTAGE.Click += new System.EventHandler(this.btnVoltage_Click);
            // 
            // btnPHASE
            // 
            resources.ApplyResources(this.btnPHASE, "btnPHASE");
            this.btnPHASE.BackColor = System.Drawing.Color.Transparent;
            this.btnPHASE.Image = ((System.Drawing.Image)(resources.GetObject("btnPHASE.Image")));
            this.btnPHASE.Name = "btnPHASE";
            this.btnPHASE.Selected = false;
            this.btnPHASE.Click += new System.EventHandler(this.btnPhase_Click);
            // 
            // btnCAPACITY
            // 
            resources.ApplyResources(this.btnCAPACITY, "btnCAPACITY");
            this.btnCAPACITY.BackColor = System.Drawing.Color.Transparent;
            this.btnCAPACITY.Image = ((System.Drawing.Image)(resources.GetObject("btnCAPACITY.Image")));
            this.btnCAPACITY.Name = "btnCAPACITY";
            this.btnCAPACITY.Selected = false;
            this.btnCAPACITY.Click += new System.EventHandler(this.btnPageCapacity_Click);
            // 
            // btnSHIELD
            // 
            resources.ApplyResources(this.btnSHIELD, "btnSHIELD");
            this.btnSHIELD.BackColor = System.Drawing.Color.Transparent;
            this.btnSHIELD.Image = ((System.Drawing.Image)(resources.GetObject("btnSHIELD.Image")));
            this.btnSHIELD.Name = "btnSHIELD";
            this.btnSHIELD.Selected = false;
            this.btnSHIELD.Click += new System.EventHandler(this.btnSeal_Click);
            // 
            // btnCONSTANT
            // 
            resources.ApplyResources(this.btnCONSTANT, "btnCONSTANT");
            this.btnCONSTANT.BackColor = System.Drawing.Color.Transparent;
            this.btnCONSTANT.Image = ((System.Drawing.Image)(resources.GetObject("btnCONSTANT.Image")));
            this.btnCONSTANT.Name = "btnCONSTANT";
            this.btnCONSTANT.Selected = false;
            this.btnCONSTANT.Click += new System.EventHandler(this.btnConstant_Click);
            // 
            // lblREPORTS
            // 
            resources.ApplyResources(this.lblREPORTS, "lblREPORTS");
            this.lblREPORTS.BackColor = System.Drawing.Color.Silver;
            this.lblREPORTS.Name = "lblREPORTS";
            // 
            // btnCUSTOMER_STATISTICS
            // 
            resources.ApplyResources(this.btnCUSTOMER_STATISTICS, "btnCUSTOMER_STATISTICS");
            this.btnCUSTOMER_STATISTICS.BackColor = System.Drawing.Color.Transparent;
            this.btnCUSTOMER_STATISTICS.Image = ((System.Drawing.Image)(resources.GetObject("btnCUSTOMER_STATISTICS.Image")));
            this.btnCUSTOMER_STATISTICS.Name = "btnCUSTOMER_STATISTICS";
            this.btnCUSTOMER_STATISTICS.Selected = false;
            this.btnCUSTOMER_STATISTICS.Click += new System.EventHandler(this.btnCustomerStatistics_Click);
            // 
            // btnSUMMARY_METER_BY_TRANSFORMER
            // 
            resources.ApplyResources(this.btnSUMMARY_METER_BY_TRANSFORMER, "btnSUMMARY_METER_BY_TRANSFORMER");
            this.btnSUMMARY_METER_BY_TRANSFORMER.BackColor = System.Drawing.Color.Transparent;
            this.btnSUMMARY_METER_BY_TRANSFORMER.Image = ((System.Drawing.Image)(resources.GetObject("btnSUMMARY_METER_BY_TRANSFORMER.Image")));
            this.btnSUMMARY_METER_BY_TRANSFORMER.Name = "btnSUMMARY_METER_BY_TRANSFORMER";
            this.btnSUMMARY_METER_BY_TRANSFORMER.Selected = false;
            this.btnSUMMARY_METER_BY_TRANSFORMER.Click += new System.EventHandler(this.btnSummaryMeterByTransformer_Click);
            // 
            // btnREPORT_INCOME
            // 
            resources.ApplyResources(this.btnREPORT_INCOME, "btnREPORT_INCOME");
            this.btnREPORT_INCOME.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_INCOME.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_INCOME.Image")));
            this.btnREPORT_INCOME.Name = "btnREPORT_INCOME";
            this.btnREPORT_INCOME.Selected = false;
            this.btnREPORT_INCOME.Click += new System.EventHandler(this.btnREPORT_INCOME_Click);
            // 
            // btnREPORT_EXPENSE
            // 
            resources.ApplyResources(this.btnREPORT_EXPENSE, "btnREPORT_EXPENSE");
            this.btnREPORT_EXPENSE.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_EXPENSE.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_EXPENSE.Image")));
            this.btnREPORT_EXPENSE.Name = "btnREPORT_EXPENSE";
            this.btnREPORT_EXPENSE.Selected = false;
            this.btnREPORT_EXPENSE.Click += new System.EventHandler(this.btnREPORT_EXPENSE_Click);
            // 
            // btnREPORT_PROFIT_LOSS
            // 
            resources.ApplyResources(this.btnREPORT_PROFIT_LOSS, "btnREPORT_PROFIT_LOSS");
            this.btnREPORT_PROFIT_LOSS.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_PROFIT_LOSS.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_PROFIT_LOSS.Image")));
            this.btnREPORT_PROFIT_LOSS.Name = "btnREPORT_PROFIT_LOSS";
            this.btnREPORT_PROFIT_LOSS.Selected = false;
            this.btnREPORT_PROFIT_LOSS.Click += new System.EventHandler(this.btnREPORT_PROFIT_LOSS_Click);
            // 
            // btnREPORT_FIX_ASSET
            // 
            resources.ApplyResources(this.btnREPORT_FIX_ASSET, "btnREPORT_FIX_ASSET");
            this.btnREPORT_FIX_ASSET.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_FIX_ASSET.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_FIX_ASSET.Image")));
            this.btnREPORT_FIX_ASSET.Name = "btnREPORT_FIX_ASSET";
            this.btnREPORT_FIX_ASSET.Selected = false;
            this.btnREPORT_FIX_ASSET.Click += new System.EventHandler(this.btnREPORT_FIX_ASSET_Click);
            // 
            // btnREPORT_FIX_ASSET_DEPRECIATION
            // 
            resources.ApplyResources(this.btnREPORT_FIX_ASSET_DEPRECIATION, "btnREPORT_FIX_ASSET_DEPRECIATION");
            this.btnREPORT_FIX_ASSET_DEPRECIATION.BackColor = System.Drawing.Color.Transparent;
            this.btnREPORT_FIX_ASSET_DEPRECIATION.Image = ((System.Drawing.Image)(resources.GetObject("btnREPORT_FIX_ASSET_DEPRECIATION.Image")));
            this.btnREPORT_FIX_ASSET_DEPRECIATION.Name = "btnREPORT_FIX_ASSET_DEPRECIATION";
            this.btnREPORT_FIX_ASSET_DEPRECIATION.Selected = false;
            this.btnREPORT_FIX_ASSET_DEPRECIATION.Click += new System.EventHandler(this.btnREPORT_FIX_ASSET_DEPRECIATION_Click);
            // 
            // lblFIX_ASSET
            // 
            resources.ApplyResources(this.lblFIX_ASSET, "lblFIX_ASSET");
            this.lblFIX_ASSET.BackColor = System.Drawing.Color.Silver;
            this.lblFIX_ASSET.Name = "lblFIX_ASSET";
            // 
            // btnFIX_ASSET_TYPE
            // 
            resources.ApplyResources(this.btnFIX_ASSET_TYPE, "btnFIX_ASSET_TYPE");
            this.btnFIX_ASSET_TYPE.BackColor = System.Drawing.Color.Transparent;
            this.btnFIX_ASSET_TYPE.Image = ((System.Drawing.Image)(resources.GetObject("btnFIX_ASSET_TYPE.Image")));
            this.btnFIX_ASSET_TYPE.Name = "btnFIX_ASSET_TYPE";
            this.btnFIX_ASSET_TYPE.Selected = false;
            this.btnFIX_ASSET_TYPE.Click += new System.EventHandler(this.btnFIX_ASSET_TYPE_Click);
            // 
            // btnFIX_ASSET_ITEM
            // 
            resources.ApplyResources(this.btnFIX_ASSET_ITEM, "btnFIX_ASSET_ITEM");
            this.btnFIX_ASSET_ITEM.BackColor = System.Drawing.Color.Transparent;
            this.btnFIX_ASSET_ITEM.Image = ((System.Drawing.Image)(resources.GetObject("btnFIX_ASSET_ITEM.Image")));
            this.btnFIX_ASSET_ITEM.Name = "btnFIX_ASSET_ITEM";
            this.btnFIX_ASSET_ITEM.Selected = false;
            this.btnFIX_ASSET_ITEM.Click += new System.EventHandler(this.btnFIX_ASSET_ITEM_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel15, "panel15");
            this.panel15.Name = "panel15";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel13, "panel13");
            this.panel13.Name = "panel13";
            // 
            // lblPOWER_MANAGEMENT_1
            // 
            this.lblPOWER_MANAGEMENT_1.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblPOWER_MANAGEMENT_1, "lblPOWER_MANAGEMENT_1");
            this.lblPOWER_MANAGEMENT_1.Image = global::EPower.Properties.Resources.img_power;
            this.lblPOWER_MANAGEMENT_1.IsTitle = true;
            this.lblPOWER_MANAGEMENT_1.Name = "lblPOWER_MANAGEMENT_1";
            this.lblPOWER_MANAGEMENT_1.Selected = false;
            this.lblPOWER_MANAGEMENT_1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.panel20);
            this.panel16.Controls.Add(this.POWER_MANAGEMENT_2);
            resources.ApplyResources(this.panel16, "panel16");
            this.panel16.Name = "panel16";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.main);
            this.panel20.Controls.Add(this.panel17);
            this.panel20.Controls.Add(this.panel18);
            this.panel20.Controls.Add(this.panel19);
            resources.ApplyResources(this.panel20, "panel20");
            this.panel20.Name = "panel20";
            // 
            // main
            // 
            this.main.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.main, "main");
            this.main.Name = "main";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel17, "panel17");
            this.panel17.Name = "panel17";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel18, "panel18");
            this.panel18.Name = "panel18";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel19, "panel19");
            this.panel19.Name = "panel19";
            // 
            // POWER_MANAGEMENT_2
            // 
            this.POWER_MANAGEMENT_2.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.POWER_MANAGEMENT_2, "POWER_MANAGEMENT_2");
            this.POWER_MANAGEMENT_2.Image = null;
            this.POWER_MANAGEMENT_2.IsTitle = true;
            this.POWER_MANAGEMENT_2.Name = "POWER_MANAGEMENT_2";
            this.POWER_MANAGEMENT_2.Selected = false;
            this.POWER_MANAGEMENT_2.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PagePower
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "PagePower";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel16;
        private Panel panel20;
        private ExTabItem POWER_MANAGEMENT_2;
        private Panel panel12;
        private ExTabItem lblPOWER_MANAGEMENT_1;
        private SplitContainer splitContainer1;
        private Panel main;
        private Panel panel17;
        private Panel panel18;
        private Panel panel19;
        private Panel panel2;
        private Panel panel15;
        private Panel panel13;
        private Panel panel3;
        private FlowLayoutPanel panel1;
        private ExMenuItem btnPOWER_ADJUSTMENT;
        private Label lblPOWER_USAGE;
        private ExMenuItem btnPOWER_USAGE_BY_TRANSFORMER;
        private ExMenuItem btnDISTRIBUTION_FACILITY;
        private ExMenuItem btnPOWER_SOURCE;
        private ExMenuItem btnPOWER_PURCHASE;
        private ExMenuItem btnCAPACITY;
        private ExMenuItem btnSHIELD;
        private ExMenuItem btnCONSTANT;
        private ExMenuItem btnPHASE;
        private ExMenuItem btnAMPARE;
        private ExMenuItem btnVOLTAGE;
        private Label lblMESUREMENT;
        private Label lblPOWER_DISTRIBUTION;
        private Label lblPURCHASE_POWER;
        private Label lblREPORTS;
        private ExMenuItem btnSUMMARY_METER_BY_TRANSFORMER;
        private ExMenuItem btnCUSTOMER_STATISTICS;
        private ExMenuItem btnLICENSE_VILLAGE;
        private Label lblGENERATION;
        private ExMenuItem btnGENERATOR;
        private ExMenuItem btnPOWER_GENERATION;
        private ExMenuItem btnLICENSEE;
        private ExMenuItem btnFUEL_SOURCE;
        private ExMenuItem btnFUEL_PURCHASE;
        private Label lblEAC_REPORT;
        private ExMenuItem btnREPORT_QUARTERLY;
        private ExMenuItem btnREPORT_OPS;
        private ExMenuItem btnREPORT_ANNUAL;
        private ExMenuItem btnREPORT_CUSTOMER_USAGE_STATISTIC;
        private ExMenuItem btnREPORT_SUBSIDY;
        private ExMenuItem btnE_A_C;
        private Label lblSETUP_ACCOUNT_CHART;
        private ExMenuItem btnACCOUNT_CHART;
        private Label lblTRANSACTION;
        private ExMenuItem btnINCOME;
        private ExMenuItem btnEXCHANGE_RATE;
        private ExMenuItem btnREPORT_INCOME;
        private ExMenuItem btnREPORT_EXPENSE;
        private ExMenuItem btnREPORT_PROFIT_LOSS;
        private ExMenuItem btnREPORT_FIX_ASSET;
        private ExMenuItem btnREPORT_FIX_ASSET_DEPRECIATION;
        private Label lblFIX_ASSET;
        private ExMenuItem btnFIX_ASSET_TYPE;
        private ExMenuItem btnFIX_ASSET_ITEM;
    }
}
