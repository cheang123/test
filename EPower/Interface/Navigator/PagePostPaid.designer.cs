﻿using System.ComponentModel;
using System.Windows.Forms;
using SoftTech.Component;

namespace EPower.Interface
{
    partial class PagePostPaid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PagePostPaid));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblTRANS = new System.Windows.Forms.Label();
            this.btnRECEIVE_PAYMENT = new SoftTech.Component.ExMenuItem();
            this.btnPRINT_PAYMENT_RECEIPT = new SoftTech.Component.ExMenuItem();
            this.btnVOID_PAYMENT = new SoftTech.Component.ExMenuItem();
            this.btnINPUT_USAGE = new SoftTech.Component.ExMenuItem();
            this.lblIR_READER = new System.Windows.Forms.Label();
            this.btnDOWNLOAD_UPLOAD_VIA_WIRE = new SoftTech.Component.ExMenuItem();
            this.btnDOWNLOAD_UPLOAD_VIA_INTERNET = new SoftTech.Component.ExMenuItem();
            this.btnIR_REGISTER_DIGITAL_METER = new SoftTech.Component.ExMenuItem();
            this.lblIR_RELAY_CONTROLLER = new System.Windows.Forms.Label();
            this.btnSEND_AND_RECEIVE_CUSTOMER = new SoftTech.Component.ExMenuItem();
            this.lblDELETE_STATUS_TAMPER = new System.Windows.Forms.Label();
            this.btnSEND_AND_RECEIVE_TAMPER = new SoftTech.Component.ExMenuItem();
            this.lblGPRS = new System.Windows.Forms.Label();
            this.btnGPRS_USAGE = new SoftTech.Component.ExMenuItem();
            this.lblAMR = new System.Windows.Forms.Label();
            this.btnAMR_USAGE = new SoftTech.Component.ExMenuItem();
            this.lblBARCODE_USAGE = new System.Windows.Forms.Label();
            this.btnPRINT_BARCODE_USAGE = new SoftTech.Component.ExMenuItem();
            this.btnBARCODE_COLLECT_USAGE = new SoftTech.Component.ExMenuItem();
            this.btnMOBILE_USAGE = new System.Windows.Forms.Label();
            this.btnMOBILE_SEND_USAGE = new SoftTech.Component.ExMenuItem();
            this.btnMOBILE_RECEIVE_USAGE = new SoftTech.Component.ExMenuItem();
            this.lblBANK_PAYMENT = new System.Windows.Forms.Label();
            this.btnBANK_PAYMENT_HISTORY = new SoftTech.Component.ExMenuItem();
            this.btnBANK_PAYMENT_SUMMARY = new SoftTech.Component.ExMenuItem();
            this.btnBANK_PAYMENT_SUMMARY_NEW = new SoftTech.Component.ExMenuItem();
            this.btnPAYMENT_EPOWER = new SoftTech.Component.ExMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.btnPOSTPAID = new SoftTech.Component.ExTabItem();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.main = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.lblPOSTPAID = new SoftTech.Component.ExTabItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel20.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel12);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel16);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel2);
            this.panel12.Controls.Add(this.btnPOSTPAID);
            resources.ApplyResources(this.panel12, "panel12");
            this.panel12.Name = "panel12";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.panel13);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.Controls.Add(this.lblTRANS);
            this.panel1.Controls.Add(this.btnRECEIVE_PAYMENT);
            this.panel1.Controls.Add(this.btnPRINT_PAYMENT_RECEIPT);
            this.panel1.Controls.Add(this.btnVOID_PAYMENT);
            this.panel1.Controls.Add(this.btnINPUT_USAGE);
            this.panel1.Controls.Add(this.lblIR_READER);
            this.panel1.Controls.Add(this.btnDOWNLOAD_UPLOAD_VIA_WIRE);
            this.panel1.Controls.Add(this.btnDOWNLOAD_UPLOAD_VIA_INTERNET);
            this.panel1.Controls.Add(this.btnIR_REGISTER_DIGITAL_METER);
            this.panel1.Controls.Add(this.lblIR_RELAY_CONTROLLER);
            this.panel1.Controls.Add(this.btnSEND_AND_RECEIVE_CUSTOMER);
            this.panel1.Controls.Add(this.lblDELETE_STATUS_TAMPER);
            this.panel1.Controls.Add(this.btnSEND_AND_RECEIVE_TAMPER);
            this.panel1.Controls.Add(this.lblGPRS);
            this.panel1.Controls.Add(this.btnGPRS_USAGE);
            this.panel1.Controls.Add(this.lblAMR);
            this.panel1.Controls.Add(this.btnAMR_USAGE);
            this.panel1.Controls.Add(this.lblBARCODE_USAGE);
            this.panel1.Controls.Add(this.btnPRINT_BARCODE_USAGE);
            this.panel1.Controls.Add(this.btnBARCODE_COLLECT_USAGE);
            this.panel1.Controls.Add(this.btnMOBILE_USAGE);
            this.panel1.Controls.Add(this.btnMOBILE_SEND_USAGE);
            this.panel1.Controls.Add(this.btnMOBILE_RECEIVE_USAGE);
            this.panel1.Controls.Add(this.lblBANK_PAYMENT);
            this.panel1.Controls.Add(this.btnBANK_PAYMENT_HISTORY);
            this.panel1.Controls.Add(this.btnBANK_PAYMENT_SUMMARY);
            this.panel1.Controls.Add(this.btnBANK_PAYMENT_SUMMARY_NEW);
            this.panel1.Controls.Add(this.btnPAYMENT_EPOWER);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblTRANS
            // 
            resources.ApplyResources(this.lblTRANS, "lblTRANS");
            this.lblTRANS.BackColor = System.Drawing.Color.Silver;
            this.lblTRANS.Name = "lblTRANS";
            // 
            // btnRECEIVE_PAYMENT
            // 
            resources.ApplyResources(this.btnRECEIVE_PAYMENT, "btnRECEIVE_PAYMENT");
            this.btnRECEIVE_PAYMENT.BackColor = System.Drawing.Color.Transparent;
            this.btnRECEIVE_PAYMENT.Image = ((System.Drawing.Image)(resources.GetObject("btnRECEIVE_PAYMENT.Image")));
            this.btnRECEIVE_PAYMENT.Name = "btnRECEIVE_PAYMENT";
            this.btnRECEIVE_PAYMENT.Selected = false;
            this.btnRECEIVE_PAYMENT.Click += new System.EventHandler(this.btnReceivePayment_Click);
            // 
            // btnPRINT_PAYMENT_RECEIPT
            // 
            resources.ApplyResources(this.btnPRINT_PAYMENT_RECEIPT, "btnPRINT_PAYMENT_RECEIPT");
            this.btnPRINT_PAYMENT_RECEIPT.BackColor = System.Drawing.Color.Transparent;
            this.btnPRINT_PAYMENT_RECEIPT.Image = ((System.Drawing.Image)(resources.GetObject("btnPRINT_PAYMENT_RECEIPT.Image")));
            this.btnPRINT_PAYMENT_RECEIPT.Name = "btnPRINT_PAYMENT_RECEIPT";
            this.btnPRINT_PAYMENT_RECEIPT.Selected = false;
            this.btnPRINT_PAYMENT_RECEIPT.Click += new System.EventHandler(this.btnPrintReceipt_Click);
            // 
            // btnVOID_PAYMENT
            // 
            resources.ApplyResources(this.btnVOID_PAYMENT, "btnVOID_PAYMENT");
            this.btnVOID_PAYMENT.BackColor = System.Drawing.Color.Transparent;
            this.btnVOID_PAYMENT.Image = ((System.Drawing.Image)(resources.GetObject("btnVOID_PAYMENT.Image")));
            this.btnVOID_PAYMENT.Name = "btnVOID_PAYMENT";
            this.btnVOID_PAYMENT.Selected = false;
            this.btnVOID_PAYMENT.Click += new System.EventHandler(this.btnPaymentCancel_Click);
            // 
            // btnINPUT_USAGE
            // 
            resources.ApplyResources(this.btnINPUT_USAGE, "btnINPUT_USAGE");
            this.btnINPUT_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.btnINPUT_USAGE.Image = ((System.Drawing.Image)(resources.GetObject("btnINPUT_USAGE.Image")));
            this.btnINPUT_USAGE.Name = "btnINPUT_USAGE";
            this.btnINPUT_USAGE.Selected = false;
            this.btnINPUT_USAGE.Click += new System.EventHandler(this.btnCollectUsage_Click);
            // 
            // lblIR_READER
            // 
            resources.ApplyResources(this.lblIR_READER, "lblIR_READER");
            this.lblIR_READER.BackColor = System.Drawing.Color.Silver;
            this.lblIR_READER.Name = "lblIR_READER";
            // 
            // btnDOWNLOAD_UPLOAD_VIA_WIRE
            // 
            resources.ApplyResources(this.btnDOWNLOAD_UPLOAD_VIA_WIRE, "btnDOWNLOAD_UPLOAD_VIA_WIRE");
            this.btnDOWNLOAD_UPLOAD_VIA_WIRE.BackColor = System.Drawing.Color.Transparent;
            this.btnDOWNLOAD_UPLOAD_VIA_WIRE.Image = ((System.Drawing.Image)(resources.GetObject("btnDOWNLOAD_UPLOAD_VIA_WIRE.Image")));
            this.btnDOWNLOAD_UPLOAD_VIA_WIRE.Name = "btnDOWNLOAD_UPLOAD_VIA_WIRE";
            this.btnDOWNLOAD_UPLOAD_VIA_WIRE.Selected = false;
            this.btnDOWNLOAD_UPLOAD_VIA_WIRE.Click += new System.EventHandler(this.btnDOWNLOAD_UPLOAD_VIA_WIRE_Click);
            // 
            // btnDOWNLOAD_UPLOAD_VIA_INTERNET
            // 
            resources.ApplyResources(this.btnDOWNLOAD_UPLOAD_VIA_INTERNET, "btnDOWNLOAD_UPLOAD_VIA_INTERNET");
            this.btnDOWNLOAD_UPLOAD_VIA_INTERNET.BackColor = System.Drawing.Color.Transparent;
            this.btnDOWNLOAD_UPLOAD_VIA_INTERNET.Image = ((System.Drawing.Image)(resources.GetObject("btnDOWNLOAD_UPLOAD_VIA_INTERNET.Image")));
            this.btnDOWNLOAD_UPLOAD_VIA_INTERNET.Name = "btnDOWNLOAD_UPLOAD_VIA_INTERNET";
            this.btnDOWNLOAD_UPLOAD_VIA_INTERNET.Selected = false;
            this.btnDOWNLOAD_UPLOAD_VIA_INTERNET.Click += new System.EventHandler(this.btnDOWNLOAD_UPLOAD_INTERNET_Click);
            // 
            // btnIR_REGISTER_DIGITAL_METER
            // 
            resources.ApplyResources(this.btnIR_REGISTER_DIGITAL_METER, "btnIR_REGISTER_DIGITAL_METER");
            this.btnIR_REGISTER_DIGITAL_METER.BackColor = System.Drawing.Color.Transparent;
            this.btnIR_REGISTER_DIGITAL_METER.Image = ((System.Drawing.Image)(resources.GetObject("btnIR_REGISTER_DIGITAL_METER.Image")));
            this.btnIR_REGISTER_DIGITAL_METER.Name = "btnIR_REGISTER_DIGITAL_METER";
            this.btnIR_REGISTER_DIGITAL_METER.Selected = false;
            this.btnIR_REGISTER_DIGITAL_METER.Click += new System.EventHandler(this.btnRegisterMeter_Click);
            // 
            // lblIR_RELAY_CONTROLLER
            // 
            resources.ApplyResources(this.lblIR_RELAY_CONTROLLER, "lblIR_RELAY_CONTROLLER");
            this.lblIR_RELAY_CONTROLLER.BackColor = System.Drawing.Color.Silver;
            this.lblIR_RELAY_CONTROLLER.Name = "lblIR_RELAY_CONTROLLER";
            // 
            // btnSEND_AND_RECEIVE_CUSTOMER
            // 
            resources.ApplyResources(this.btnSEND_AND_RECEIVE_CUSTOMER, "btnSEND_AND_RECEIVE_CUSTOMER");
            this.btnSEND_AND_RECEIVE_CUSTOMER.BackColor = System.Drawing.Color.Transparent;
            this.btnSEND_AND_RECEIVE_CUSTOMER.Image = ((System.Drawing.Image)(resources.GetObject("btnSEND_AND_RECEIVE_CUSTOMER.Image")));
            this.btnSEND_AND_RECEIVE_CUSTOMER.Name = "btnSEND_AND_RECEIVE_CUSTOMER";
            this.btnSEND_AND_RECEIVE_CUSTOMER.Selected = false;
            this.btnSEND_AND_RECEIVE_CUSTOMER.Click += new System.EventHandler(this.btnIrRelayController_Click);
            // 
            // lblDELETE_STATUS_TAMPER
            // 
            resources.ApplyResources(this.lblDELETE_STATUS_TAMPER, "lblDELETE_STATUS_TAMPER");
            this.lblDELETE_STATUS_TAMPER.BackColor = System.Drawing.Color.Silver;
            this.lblDELETE_STATUS_TAMPER.Name = "lblDELETE_STATUS_TAMPER";
            // 
            // btnSEND_AND_RECEIVE_TAMPER
            // 
            resources.ApplyResources(this.btnSEND_AND_RECEIVE_TAMPER, "btnSEND_AND_RECEIVE_TAMPER");
            this.btnSEND_AND_RECEIVE_TAMPER.BackColor = System.Drawing.Color.Transparent;
            this.btnSEND_AND_RECEIVE_TAMPER.Image = ((System.Drawing.Image)(resources.GetObject("btnSEND_AND_RECEIVE_TAMPER.Image")));
            this.btnSEND_AND_RECEIVE_TAMPER.Name = "btnSEND_AND_RECEIVE_TAMPER";
            this.btnSEND_AND_RECEIVE_TAMPER.Selected = false;
            this.btnSEND_AND_RECEIVE_TAMPER.Click += new System.EventHandler(this.btnSEND_AND_RECEIVE_TAMPER_Click);
            // 
            // lblGPRS
            // 
            resources.ApplyResources(this.lblGPRS, "lblGPRS");
            this.lblGPRS.BackColor = System.Drawing.Color.Silver;
            this.lblGPRS.Name = "lblGPRS";
            // 
            // btnGPRS_USAGE
            // 
            resources.ApplyResources(this.btnGPRS_USAGE, "btnGPRS_USAGE");
            this.btnGPRS_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.btnGPRS_USAGE.Image = ((System.Drawing.Image)(resources.GetObject("btnGPRS_USAGE.Image")));
            this.btnGPRS_USAGE.Name = "btnGPRS_USAGE";
            this.btnGPRS_USAGE.Selected = false;
            this.btnGPRS_USAGE.Click += new System.EventHandler(this.btnUsageGPRS_Click);
            // 
            // lblAMR
            // 
            resources.ApplyResources(this.lblAMR, "lblAMR");
            this.lblAMR.BackColor = System.Drawing.Color.Silver;
            this.lblAMR.Name = "lblAMR";
            // 
            // btnAMR_USAGE
            // 
            resources.ApplyResources(this.btnAMR_USAGE, "btnAMR_USAGE");
            this.btnAMR_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.btnAMR_USAGE.Image = ((System.Drawing.Image)(resources.GetObject("btnAMR_USAGE.Image")));
            this.btnAMR_USAGE.Name = "btnAMR_USAGE";
            this.btnAMR_USAGE.Selected = false;
            this.btnAMR_USAGE.Click += new System.EventHandler(this.btnAMRSystem_Click);
            // 
            // lblBARCODE_USAGE
            // 
            resources.ApplyResources(this.lblBARCODE_USAGE, "lblBARCODE_USAGE");
            this.lblBARCODE_USAGE.BackColor = System.Drawing.Color.Silver;
            this.lblBARCODE_USAGE.Name = "lblBARCODE_USAGE";
            // 
            // btnPRINT_BARCODE_USAGE
            // 
            resources.ApplyResources(this.btnPRINT_BARCODE_USAGE, "btnPRINT_BARCODE_USAGE");
            this.btnPRINT_BARCODE_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.btnPRINT_BARCODE_USAGE.Image = ((System.Drawing.Image)(resources.GetObject("btnPRINT_BARCODE_USAGE.Image")));
            this.btnPRINT_BARCODE_USAGE.Name = "btnPRINT_BARCODE_USAGE";
            this.btnPRINT_BARCODE_USAGE.Selected = false;
            this.btnPRINT_BARCODE_USAGE.Click += new System.EventHandler(this.btnPrintBarcode_Click);
            // 
            // btnBARCODE_COLLECT_USAGE
            // 
            resources.ApplyResources(this.btnBARCODE_COLLECT_USAGE, "btnBARCODE_COLLECT_USAGE");
            this.btnBARCODE_COLLECT_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.btnBARCODE_COLLECT_USAGE.Image = ((System.Drawing.Image)(resources.GetObject("btnBARCODE_COLLECT_USAGE.Image")));
            this.btnBARCODE_COLLECT_USAGE.Name = "btnBARCODE_COLLECT_USAGE";
            this.btnBARCODE_COLLECT_USAGE.Selected = false;
            this.btnBARCODE_COLLECT_USAGE.Click += new System.EventHandler(this.btnCollectByMetrologic_Click);
            // 
            // btnMOBILE_USAGE
            // 
            resources.ApplyResources(this.btnMOBILE_USAGE, "btnMOBILE_USAGE");
            this.btnMOBILE_USAGE.BackColor = System.Drawing.Color.Silver;
            this.btnMOBILE_USAGE.Name = "btnMOBILE_USAGE";
            // 
            // btnMOBILE_SEND_USAGE
            // 
            resources.ApplyResources(this.btnMOBILE_SEND_USAGE, "btnMOBILE_SEND_USAGE");
            this.btnMOBILE_SEND_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.btnMOBILE_SEND_USAGE.Image = ((System.Drawing.Image)(resources.GetObject("btnMOBILE_SEND_USAGE.Image")));
            this.btnMOBILE_SEND_USAGE.Name = "btnMOBILE_SEND_USAGE";
            this.btnMOBILE_SEND_USAGE.Selected = false;
            this.btnMOBILE_SEND_USAGE.Click += new System.EventHandler(this.exMenuItem1_Click);
            // 
            // btnMOBILE_RECEIVE_USAGE
            // 
            resources.ApplyResources(this.btnMOBILE_RECEIVE_USAGE, "btnMOBILE_RECEIVE_USAGE");
            this.btnMOBILE_RECEIVE_USAGE.BackColor = System.Drawing.Color.Transparent;
            this.btnMOBILE_RECEIVE_USAGE.Image = ((System.Drawing.Image)(resources.GetObject("btnMOBILE_RECEIVE_USAGE.Image")));
            this.btnMOBILE_RECEIVE_USAGE.Name = "btnMOBILE_RECEIVE_USAGE";
            this.btnMOBILE_RECEIVE_USAGE.Selected = false;
            this.btnMOBILE_RECEIVE_USAGE.Click += new System.EventHandler(this.btnCollectUsageByDevice_Click);
            // 
            // lblBANK_PAYMENT
            // 
            resources.ApplyResources(this.lblBANK_PAYMENT, "lblBANK_PAYMENT");
            this.lblBANK_PAYMENT.BackColor = System.Drawing.Color.Silver;
            this.lblBANK_PAYMENT.Name = "lblBANK_PAYMENT";
            // 
            // btnBANK_PAYMENT_HISTORY
            // 
            resources.ApplyResources(this.btnBANK_PAYMENT_HISTORY, "btnBANK_PAYMENT_HISTORY");
            this.btnBANK_PAYMENT_HISTORY.BackColor = System.Drawing.Color.Transparent;
            this.btnBANK_PAYMENT_HISTORY.Image = ((System.Drawing.Image)(resources.GetObject("btnBANK_PAYMENT_HISTORY.Image")));
            this.btnBANK_PAYMENT_HISTORY.Name = "btnBANK_PAYMENT_HISTORY";
            this.btnBANK_PAYMENT_HISTORY.Selected = false;
            this.btnBANK_PAYMENT_HISTORY.Click += new System.EventHandler(this.btnBankPaymentHistory_Click);
            // 
            // btnBANK_PAYMENT_SUMMARY
            // 
            resources.ApplyResources(this.btnBANK_PAYMENT_SUMMARY, "btnBANK_PAYMENT_SUMMARY");
            this.btnBANK_PAYMENT_SUMMARY.BackColor = System.Drawing.Color.Transparent;
            this.btnBANK_PAYMENT_SUMMARY.Image = ((System.Drawing.Image)(resources.GetObject("btnBANK_PAYMENT_SUMMARY.Image")));
            this.btnBANK_PAYMENT_SUMMARY.Name = "btnBANK_PAYMENT_SUMMARY";
            this.btnBANK_PAYMENT_SUMMARY.Selected = false;
            this.btnBANK_PAYMENT_SUMMARY.Click += new System.EventHandler(this.btnBankPaymentSummary_Click);
            // 
            // btnBANK_PAYMENT_SUMMARY_NEW
            // 
            resources.ApplyResources(this.btnBANK_PAYMENT_SUMMARY_NEW, "btnBANK_PAYMENT_SUMMARY_NEW");
            this.btnBANK_PAYMENT_SUMMARY_NEW.BackColor = System.Drawing.Color.Transparent;
            this.btnBANK_PAYMENT_SUMMARY_NEW.Image = ((System.Drawing.Image)(resources.GetObject("btnBANK_PAYMENT_SUMMARY_NEW.Image")));
            this.btnBANK_PAYMENT_SUMMARY_NEW.Name = "btnBANK_PAYMENT_SUMMARY_NEW";
            this.btnBANK_PAYMENT_SUMMARY_NEW.Selected = false;
            this.btnBANK_PAYMENT_SUMMARY_NEW.Click += new System.EventHandler(this.btnREPORT_CASH_COLLECTION_Click);
            // 
            // btnPAYMENT_EPOWER
            // 
            resources.ApplyResources(this.btnPAYMENT_EPOWER, "btnPAYMENT_EPOWER");
            this.btnPAYMENT_EPOWER.BackColor = System.Drawing.Color.Transparent;
            this.btnPAYMENT_EPOWER.Image = ((System.Drawing.Image)(resources.GetObject("btnPAYMENT_EPOWER.Image")));
            this.btnPAYMENT_EPOWER.Name = "btnPAYMENT_EPOWER";
            this.btnPAYMENT_EPOWER.Selected = false;
            this.btnPAYMENT_EPOWER.Click += new System.EventHandler(this.btnPAYMENT_EPOWER_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel15, "panel15");
            this.panel15.Name = "panel15";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel13, "panel13");
            this.panel13.Name = "panel13";
            // 
            // btnPOSTPAID
            // 
            this.btnPOSTPAID.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnPOSTPAID, "btnPOSTPAID");
            this.btnPOSTPAID.Image = global::EPower.Properties.Resources.home;
            this.btnPOSTPAID.IsTitle = true;
            this.btnPOSTPAID.Name = "btnPOSTPAID";
            this.btnPOSTPAID.Selected = false;
            this.btnPOSTPAID.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.panel20);
            this.panel16.Controls.Add(this.lblPOSTPAID);
            resources.ApplyResources(this.panel16, "panel16");
            this.panel16.Name = "panel16";
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.main);
            this.panel20.Controls.Add(this.panel17);
            this.panel20.Controls.Add(this.panel18);
            this.panel20.Controls.Add(this.panel19);
            resources.ApplyResources(this.panel20, "panel20");
            this.panel20.Name = "panel20";
            // 
            // main
            // 
            this.main.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.main, "main");
            this.main.Name = "main";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel17, "panel17");
            this.panel17.Name = "panel17";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel18, "panel18");
            this.panel18.Name = "panel18";
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Silver;
            resources.ApplyResources(this.panel19, "panel19");
            this.panel19.Name = "panel19";
            // 
            // lblPOSTPAID
            // 
            this.lblPOSTPAID.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblPOSTPAID, "lblPOSTPAID");
            this.lblPOSTPAID.Image = null;
            this.lblPOSTPAID.IsTitle = true;
            this.lblPOSTPAID.Name = "lblPOSTPAID";
            this.lblPOSTPAID.Selected = false;
            this.lblPOSTPAID.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PagePostPaid
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "PagePostPaid";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel16;
        private Panel panel20;
        private ExTabItem lblPOSTPAID;
        private Panel panel12;
        private ExMenuItem btnRECEIVE_PAYMENT;
        private ExTabItem btnPOSTPAID;
        private SplitContainer splitContainer1;
        private Panel main;
        private Panel panel17;
        private Panel panel18;
        private Panel panel19;
        private Panel panel2;
        private Panel panel15;
        private Panel panel13;
        private Panel panel3;
        private Label btnMOBILE_USAGE;
        private FlowLayoutPanel panel1;
        private ExMenuItem btnMOBILE_RECEIVE_USAGE;
        private ExMenuItem btnINPUT_USAGE;
        private ExMenuItem btnMOBILE_SEND_USAGE;
        private Label lblIR_READER;
        private ExMenuItem btnDOWNLOAD_UPLOAD_VIA_WIRE;
        private ExMenuItem btnIR_REGISTER_DIGITAL_METER;
        private ExMenuItem btnVOID_PAYMENT;
        private ExMenuItem btnPRINT_PAYMENT_RECEIPT;
        private Label lblTRANS;
        private Label lblBANK_PAYMENT;
        private ExMenuItem btnBARCODE_COLLECT_USAGE;
        private Label lblBARCODE_USAGE;
        private ExMenuItem btnPRINT_BARCODE_USAGE;
        private ExMenuItem btnBANK_PAYMENT_HISTORY;
        private ExMenuItem btnBANK_PAYMENT_SUMMARY;
        private Label lblGPRS;
        private ExMenuItem btnGPRS_USAGE;
        private Label lblIR_RELAY_CONTROLLER;
        private ExMenuItem btnSEND_AND_RECEIVE_CUSTOMER;
        private Label lblAMR;
        private ExMenuItem btnAMR_USAGE;
        private ExMenuItem btnPAYMENT_EPOWER;
        private ExMenuItem btnDOWNLOAD_UPLOAD_VIA_INTERNET;
        private Label lblDELETE_STATUS_TAMPER;
        private ExMenuItem btnSEND_AND_RECEIVE_TAMPER;
        private ExMenuItem btnBANK_PAYMENT_SUMMARY_NEW;
    }
}
