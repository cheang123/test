﻿using SoftTech;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class PageBilling : UserControl
    {
       
        public PageBilling()
        {
            InitializeComponent();

            //Assign permission
            btnMANAGE_CUSTOMER.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER);
            btnActivate.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_ACTIVATE);
            btnBLOCK_CUSTOMER.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_BLOCK);
            btnUNBLOCK_CUSTOMER.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_UNBLOCK);

            btnPRICE.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_SETUPRUNBILL_PRICE);
            btnBILLING_CYCLE.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_SETUPRUNBILL_BILLINGCYCLE);
            btnSERVICE_TYPE.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_SETUPRUNBILL_INVOICEITEMTYPE);
            btnSERVICE.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_SETUPRUNBILL_INVOICEITEM);
            btnDISCOUNT.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_SETUPRUNBILL_DISCOUNT);
            btnCONNECTION_FEE.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_SETUPRUNBILL_CONNECTFEE);
            btnRUN_BILL.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_BILLINGOPERATION_RUNBILL);
            btnPRINT_BILL.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_BILLINGOPERATION_PRINTINVOICE);
            btnBILLING_HISTORY.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_BILLINGOPERATION_BILLINGHISTORY);
            btnDEPOSIT.Enabled = Login.IsAuthorized(Permission.CUSTOMERANDBILLING_CUSTOMER_REGISTER_UPDATE_DEPOSIT_DATE);
 
            // by default show customer list
            this.showPageCustomerList();
        }


        #region Show Page
        public Control currentPage = null;
        private Dictionary<Type, Control> pages = new Dictionary<Type, Control>();
        private void showPage(Type type, object sender)
        {
            if (!this.pages.ContainsKey(type))
            {
                this.pages[type] = (Control)Activator.CreateInstance(type);
                this.pages[type].Size = this.main.Size;
                if (this.pages[type] is Form)
                {
                    Form frm = (Form)this.pages[type];
                    frm.TopLevel = false;
                    frm.Visible = true;
                    frm.FormBorderStyle = FormBorderStyle.None;
                }
                this.main.Controls.Add(this.pages[type]);
                this.pages[type].Dock = DockStyle.Fill;
                ResourceHelper.ApplyResource(this.pages[type]);
            }
            if (this.currentPage != this.pages[type])
            {
                this.pages[type].Show();
                if (this.currentPage != null)
                {
                    this.currentPage.Hide();
                }
                this.currentPage = this.pages[type];
            }
            this.btnBILLING_1.Text = ((Control)sender).Text;
        }
        #endregion ShowPage()


        private void menuPrice_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePrice), sender);
        }

        private void menuBillingCycle_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageBillingCycle), sender);
        }

        private void menuInvoiceItem_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageInvoiceItemType), sender);
        }

        private void btnPayrollItem_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageInvoiceItem), sender);
            ((PageInvoiceItem)this.currentPage).loadCurrency();
            ((PageInvoiceItem)this.currentPage).loadInvoiceItemType(); 
        } 
        private void btnRunBill_Click(object sender, EventArgs e)
        { 
            this.showPage(typeof(PageRunBill),sender);
            ((PageRunBill)this.currentPage).bind(); 
        } 
        private void btnPrintBill_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePrintInvoice), sender);
            ((PagePrintInvoice)this.currentPage).Bind();
        } 
        private void btnRunBillingHistory_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageRunBillHistory), sender);
            ((PageRunBillHistory)this.currentPage).bind();
        }

        private void menuFind_Click(object sender, EventArgs e)
        {
            this.showPageCustomerList();
        }

        private void menuActivate_Click(object sender, EventArgs e)
        {
            showCustomerActivateList();
        }

        private void showPageCustomerList()
        { 
            this.btnMANAGE_CUSTOMER.Selected = true;
            this.showPage(typeof(PageCustomerList),this.btnMANAGE_CUSTOMER);
            ((PageCustomerList)this.currentPage).BindCustomerType();
            ((PageCustomerList)this.currentPage).BindCustomerStatus();
            ((PageCustomerList)this.currentPage).BindData();
        }

        private void showCustomerActivateList()
        { 
            this.showPage(typeof(PageCustomerActivateList), this.btnActivate);
            ((PageCustomerActivateList)this.currentPage).BindData();
        }
 
        private void manuBlock_Click(object sender, EventArgs e)
        {  
            this.showPage(typeof(PageCustomerBlock), sender);
            ((PageCustomerBlock)this.currentPage).BindData();
        }

        private void menuConnection_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageCustomerUnBlock), sender);
            ((PageCustomerUnBlock)this.currentPage).BindData();
        } 

        private void btnDiscount_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageDiscount), sender);
        }

        private void btnDeposit_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageConnectionFee), sender);
        }

        private void btnDeposit_Click_1(object sender, EventArgs e)
        {
            this.showPage(typeof(PageDeposit), sender);
        }

        private void btnMergeCustomer_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageCustomerMergeAccount), sender);
            ((PageCustomerMergeAccount)this.currentPage).BindData();
        }

        private void PageBilling_Load(object sender, EventArgs e)
        {

        }
    }
}
