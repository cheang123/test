﻿using EPower.Accounting.Interface.Setting;
using EPower.Base.Logic;
using EPower.Interface.Stock;
using SoftTech;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class PageAdmin : UserControl
    {
        public PageAdmin()
        {
            InitializeComponent();

            btnArea.Enabled = Login.IsAuthorized(Permission.ADMIN_AREA);
            btnTransformer.Enabled = Login.IsAuthorized(Permission.ADMIN_TRANSFORMER);
            btnPOLE_AND_BOX.Enabled = Login.IsAuthorized(Permission.ADMIN_POLEANDBOX);
            btnMETER_TYPE.Enabled = Login.IsAuthorized(Permission.ADMIN_METERTYPE);
            btnMETER.Enabled = Login.IsAuthorized(Permission.ADMIN_METER);
            btnBREAKER_TYPE.Enabled = Login.IsAuthorized(Permission.ADMIN_CIRCUITBREAKERTYPE);
            btnBREAKER.Enabled = Login.IsAuthorized(Permission.ADMIN_CIRCUITBREAKER);
            btnEQUIPMENT.Enabled = Login.IsAuthorized(Permission.ADMIN_EQIPMENT);
            btnDEVICE.Enabled = Login.IsAuthorized(Permission.ADMIN_DEVICEREGISTER);
            btnEMPLOYEE.Enabled = Login.IsAuthorized(Permission.ADMIN_EMPLOYEE);
            btnCASH_DRAWER.Enabled = Login.IsAuthorized(Permission.ADMIN_CASTDRAWER);
            btnCUSTOMER_TYPE.Enabled = Login.IsAuthorized(Permission.ADMIN_CUSTOMERTYPE);
            btnCompany.Enabled = Login.IsAuthorized(Permission.ADMIN_COMPANY);

            btnBANK_DEPOSIT.Enabled = Login.IsAuthorized(Permission.ADMIN_BANK_DEPOSIT);

            btnSTOCK_ITEM.Enabled = Login.IsAuthorized(Permission.ADMIN_STOCK_ITEM);
            btnSTOCK.Enabled = Login.IsAuthorized(Permission.ADMIN_STOCK_MGT);

            btnUTILITY.Enabled = Login.IsAuthorized(Permission.ADMIN_OTHER_USERSETTING);
            btnCUSTOMER_BATCH_UPDATE.Enabled = Login.IsAuthorized(Permission.ADMIN_OTHER_CUSTOMERBATCHCHANGE);

            this.lblSTOCK.Visible =
            this.btnSTOCK_ITEM.Visible =
            this.btnSTOCK.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_STOCK_MANGEMENT]);

            this.lblBANK_TRANSACTION.Visible =
            this.btnBANK_DEPOSIT.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_BANK_DEPOSIT]);

        }

        #region showPage
        private Control currentPage = null;
        private Dictionary<Type, Control> pages = new Dictionary<Type, Control>();
        private void showPage(Type type, object sender)
        {
            if (!this.pages.ContainsKey(type))
            {
                this.pages[type] = (Control)Activator.CreateInstance(type);
                this.pages[type].Size = this.main.Size;
                if (this.pages[type] is Form)
                {
                    Form frm = (Form)this.pages[type];
                    frm.TopLevel = false;
                    frm.Visible = true;
                    frm.FormBorderStyle = FormBorderStyle.None;
                }
                this.main.Controls.Add(this.pages[type]);
                this.pages[type].Dock = DockStyle.Fill;
                ResourceHelper.ApplyResource(this.pages[type]);
            }
            if (this.currentPage != this.pages[type])
            {
                this.pages[type].Show();
                if (this.currentPage != null)
                {
                    this.currentPage.Hide();
                }
                this.currentPage = this.pages[type];
            }
            this.lblADMIN_TASKS.Text = ((Control)sender).Text;
        }
        #endregion ShowPage()

        private void btnArea_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageArea), sender);
        }
        private void btnPole_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePole), sender);
            ((PagePole)this.currentPage).LoadArea();
        }
        private void btnMeter_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageMeter), sender);
            ((PageMeter)this.currentPage).loadMeterStatus();
            ((PageMeter)this.currentPage).loadMeterType();
        }
        private void btnMeterType_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageMeterType), sender);
        }
        private void btnDevice_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageDevice), sender);
            ((PageDevice)this.currentPage).loadDeviceStatus();
        }
        private void btnCollector_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePosition), sender);
            ((PagePosition)this.currentPage).lookup();
        }
        private void btnCustomerType_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageCustomerType), sender);
        }
        private void btnCashDrawer_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageCashDrawer), sender);
            ((PageCashDrawer)this.currentPage).loadCurrency();
        }
        private void btnCircuitBreakerType_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageCircuitBreakerType), sender);
        }
        private void btnCircuitBreaker_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageCircuitBreaker), sender);
            ((PageCircuitBreaker)currentPage).loadBreakerType();
            ((PageCircuitBreaker)currentPage).loadBrakerStatus();
        }
        private void btnEquipment_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageEquipment), sender);
        }
        private void btnTransformer_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageTransformer), sender);
        }
        private void btnCompany_Click(object sender, EventArgs e)
        {
            this.btnCompany.Selected = false;
            (new DialogCompany()).ShowDialog();
        }
        private void btnReportSchedule_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportSchedule), sender);
        }
        private void btnUtility_Click(object sender, EventArgs e)
        {
            this.btnUTILITY.Selected = false;
            new DialogUtility().ShowDialog();
        }
        private void btnManageStock_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageStockManagement), sender);
        }
        private void btnStockItem_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageStockItem), sender);
        }
        private void btnBankDeposit_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageBankTran), sender);
        }

        private void btnCustomerBatchUpdate_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageCustomerBatchChange), sender);
            ((PageCustomerBatchChange)this.currentPage).BindData();
        }

        private void btnFormulaCalculation_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageFormulaCalculation), sender);
        }

        private void btnPOLE_BOX_BATCH_UPDATE_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePoleBoxBatchChange), sender);
            ((PagePoleBoxBatchChange)this.currentPage).LoadArea();
        }

        private void btnSEQUENCE_FORMAT_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageSequence), sender);
        }

        private void btnPAYMENT_SETTING_Click(object sender, EventArgs e)
        {
            new DialogPaymentSetting().ShowDialog();
        }

        private void btnPAYMENT_METHOD_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePaymentMethod), sender);
        }

        private void btnTAX_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageTax), sender);
        }
    }
}
