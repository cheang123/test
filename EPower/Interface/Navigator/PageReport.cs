﻿using EPower.Base.Logic;
using EPower.Interface.AnnualReport;
using EPower.Interface.NewReport;
using SoftTech;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class PageReport : UserControl
    { 
        public PageReport()
        {
            InitializeComponent(); 

            // assign permission
            btnREPORT_TRIALBALANCE_DAILY.Enabled = Login.IsAuthorized(Permission.REPORT_TRIALBALANCEDAILY);
            btnREPORT_TRIAL_BALANCE_CUSTOMER_DETAIL.Enabled = Login.IsAuthorized(Permission.REPORT_TRIALBALANCECUSTOMERDETAIL);
            btnREPORT_AR_DETAIL.Enabled = Login.IsAuthorized(Permission.REPORT_TRIALBALANCEARDETAIL);
            btnREPORT_DEPOSIT_PREPAYMENT.Enabled = Login.IsAuthorized(Permission.REPORT_TRIALBALANCEDEPOSITDETAIL);
            btnBLOCK_AND_RECONNECT.Enabled = Login.IsAuthorized(Permission.REPORT_BLOCKANDRECONNECT);
            btnREPORT_AGING.Enabled = Login.IsAuthorized(Permission.REPORT_AGING);
            btnREPORT_CASH_DAILY.Enabled = Login.IsAuthorized(Permission.REPORT_CASHINGDETAIL);
            btnREPORT_CASH_DAILY_SUMMARY.Enabled = Login.IsAuthorized(Permission.REPORT_CASHINGSUMARY);

            btnREPORT_CASH_RECEIVED.Enabled = Login.IsAuthorized(Permission.REPORT_MONTHLY_CASH_RECEIVE);
            btnREPORT_MONTHLY_POWER_INCOME.Enabled = Login.IsAuthorized(Permission.REPORT_MONTHLY_POWERINCOME);
            btnREPORT_MONTHLY_OTHER_INCOME.Enabled = Login.IsAuthorized(Permission.REPORT_MONTHLY_OTHERREVENUE);
            btnREPORT_MONTHLY_DISCOUNT.Enabled = Login.IsAuthorized(Permission.REPORT_MONTHLY_DISCOUNT);
            btnREPORT_YEARLY_POWER_USAGE.Enabled = Login.IsAuthorized(Permission.REPORT_MONTHLY_POWERUSAGE);
            btnREPORT_CUSTOMER.Enabled = Login.IsAuthorized(Permission.REPORT_MONTHLY_CUSTOMERS);
            btnREPORT_MONTHLY_POWER_USAGE.Enabled = Login.IsAuthorized(Permission.REPORT_MONTHLY_POWERUSAGE);
            btnREPORT_CUSTOMER_USAGE_SUMMARY.Enabled = Login.IsAuthorized(Permission.REPORT_MONTHLY_USAGESUMMARY);
            btnREPORT_RECURRING_SERVING.Enabled = Login.IsAuthorized(Permission.REPORT_MONTHLY_RECURRINGSERVICE);
            btnREPORT_BANK_TRANSACTION.Enabled = Login.IsAuthorized(Permission.REPORT_MONTHLY_BANKDEPOSIT);
            btnREPORT_QUARTERLY.Enabled = Login.IsAuthorized(Permission.REPORT_MONTHLY_QUATERREPORT);
            btnREPORT_ANNUAL.Enabled = Login.IsAuthorized(Permission.REPORT_MONTHLY_ANNUALREPORT);

            btnREPORT_RECURRING_SERVING.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_RECURRING_SERVICE]);
            btnREPORT_BANK_TRANSACTION.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_BANK_DEPOSIT]);
            //btnREPORT_QUARTERLY.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_QUARTERLY_REPORT]);
            //btnREPORT_ANNUAL.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_ANNAUL_REPORT]);


            //btnREPORT_QUARTERLY.Visible = 
            //    btnREPORT_ANNUAL.Visible = 
            //    btnREPORT_SUBSIDY.Visible =
            //    btnREPORT_CUSTOMER_USAGE_STATISTIC.Visible = Properties.Settings.Default.LANGUAGE != "en-GB";
        }

        #region Show Page
        private Control currentPage = null;
        private Dictionary<Type, Control> pages = new Dictionary<Type, Control>();
        private void showPage(Type type, object sender)
        {
            if (!this.pages.ContainsKey(type))
            {
                this.pages[type] = (Control)Activator.CreateInstance(type);
                this.pages[type].Size = this.main.Size;
                if (this.pages[type] is Form)
                {
                    Form frm = (Form)this.pages[type];
                    frm.TopLevel = false;
                    frm.Visible = true;
                    frm.FormBorderStyle = FormBorderStyle.None;
                }
                this.main.Controls.Add(this.pages[type]);
                this.pages[type].Dock = DockStyle.Fill;
                ResourceHelper.ApplyResource(this.pages[type]);
            }
            if (this.currentPage != this.pages[type])
            {
                this.pages[type].Show();
                if (this.currentPage != null)
                {
                    this.currentPage.Hide();
                }
                this.currentPage = this.pages[type];
            }
            this.lblREPORT_1.Text = ((Control)sender).Text;
        }
        #endregion ShowPage()

        private void btnOtherRevenueByMonth_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportOtherRevenueByMonth), sender);
        }

        private void btnYearlyPowerUsage_Click(object sender, EventArgs e)
        { 
            this.showPage(typeof(PageReportPower),sender);
        }

        private void btnReportTrialBalanceDaily_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportTrialBalanceDaily), sender);
        }

        private void btnReportTrialBalanceDetailDaily_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportTrialBalanceCustomerDetail), sender);
        }

        private void btnReportAging_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportAging), sender);
        }

        private void btnARDetail_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportTrialBalanceARDetail), sender);
        }

        private void btnDepositDetail_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportTrialBalanceDepositDetail), sender);
        } 
        private void btnCloseCashDrawer_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportCashDaily), sender);
            ((PageReportCashDaily)this.currentPage).BindData();
        }
        private void btnMonthlyPowerIncome_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportPowerIncome), sender);
        }

        private void btnCashDailySummaryReport_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportCashDailySummary), sender);
        }

        private void btnCustomer_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportCustomer), sender);
        }
       
        private void btnDiscountDetail_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportDiscountDetail), sender);
        }


        private void btnUsageMonthly_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportUsageMonthly), sender);
        }

        private void btnIncomeMonthly_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportCashReceive), sender);
        } 
        private void btnBlockAndReconnect_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportBlockAndReconnect), sender);
        }

        private void btnReportBankTran_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportBankTran), sender);
        }

        private void btnCustomerUsageSummary_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportCustomerUsageSummary), sender);
        }

        private void btnRecurringServing_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportRecurringService), sender);
        }

        private void exMenuItem1_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportQuaterly), sender);
        }

        private void btnAnnualReport_Click(object sender, EventArgs e)
        {
            new DialogAnnualReport().Show();
        }

        private void btnReportCustomerUsageStatistic_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportCustomerUsageStatistic), sender);
        }

        private void btnREPORT_METER_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportMeters), sender);
        }

        private void btnE_FILLING_SALE_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(EFillingSale), sender);
        }

        private void btnSOLD_BY_CUSTOMER_TYPE_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportSoldByCustomerType), sender);
        }

        private void btnREPORT_CASH_COLLECTION_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportPayment), sender);
        }

        private void btnREPORT_AR_DETAIL_NEW_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportPayment), sender);
        }

        private void btnGENERAL_CUSTOMER_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(GeneralCustomer), sender);
        }
    }
}
