﻿using EPower.Accounting.Interface;
using EPower.Base.Logic;
using EPower.Interface.AnnualReport;
using EPower.Interface.Power;
using SoftTech;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Login = SoftTech.Security.Logic.Login;

namespace EPower.Interface
{
    public partial class PagePower : UserControl
    {
        public PagePower()
        {
            InitializeComponent();

            btnLICENSE_VILLAGE.Enabled = Login.IsAuthorized(Permission.POWER_DISTRIBUTION_LICENSEVILLAGE);
            btnDISTRIBUTION_FACILITY.Enabled = Login.IsAuthorized(Permission.POWER_DISTRIBUTION_DISTRIBUTION_FACILITY);
            btnLICENSEE.Enabled = Login.IsAuthorized(Permission.POWER_DISTRIBUTION_LICENSEE);

            btnPOWER_SOURCE.Enabled = Login.IsAuthorized(Permission.POWER_PURCHASEPOWER_AGREEMENT);
            btnPOWER_PURCHASE.Enabled = Login.IsAuthorized(Permission.POWER_PURCHASEPOWER_POWERAGREEMENT);

            btnGENERATOR.Enabled = Login.IsAuthorized(Permission.POWER_PRODUCTION_GENERATOR);
            btnPOWER_GENERATION.Enabled = Login.IsAuthorized(Permission.POWER_PRODUCTION_GENERATION);
            btnFUEL_SOURCE.Enabled = Login.IsAuthorized(Permission.POWER_PRODUCTION_FUELSOURCE);
            btnFUEL_PURCHASE.Enabled = Login.IsAuthorized(Permission.POWER_PRODUCTION_FUELPURCHASE);

            btnPOWER_ADJUSTMENT.Enabled = Login.IsAuthorized(Permission.POWER_USEPOWER_POWER);
            btnPOWER_USAGE_BY_TRANSFORMER.Enabled = Login.IsAuthorized(Permission.POWER_USEPOWER_POWERBYTRANSOFRMAER);

            btnAMPARE.Enabled = Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_AMPARE);
            btnVOLTAGE.Enabled = Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_VOLTAGE);
            btnPHASE.Enabled = Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_PHASE);
            btnCONSTANT.Enabled = Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_CONTANT);
            btnSHIELD.Enabled = Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_SEAL);
            btnCAPACITY.Enabled = Login.IsAuthorized(Permission.POWER_UNITELECTRICITY_CAPACITY);

            btnCUSTOMER_STATISTICS.Enabled = Login.IsAuthorized(Permission.POWER_REPORT_CUSTOMERSTATISTIC);
            btnSUMMARY_METER_BY_TRANSFORMER.Enabled = Login.IsAuthorized(Permission.POWER_REPORT_METERBYTRANSFORMER);

            btnPOWER_ADJUSTMENT.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ADJUST_POWER_PRODUCTION]);

            btnREPORT_ANNUAL.Enabled = Login.IsAuthorized(Permission.POWER_EAC_REPORT_REPORT_YEARLY);
            btnREPORT_QUARTERLY.Enabled = Login.IsAuthorized(Permission.POWER_EAC_REPORT_REPORT_QUARTER);
            btnREPORT_SUBSIDY.Enabled = Login.IsAuthorized(Permission.POWER_EAC_REPORT_REPORT_SUBSIDY);
            btnREPORT_CUSTOMER_USAGE_STATISTIC.Enabled = Login.IsAuthorized(Permission.POWER_EAC_REPORT_REPORT_CUSTOMER_USAGE_STATIC);
            btnREPORT_OPS.Enabled = Login.IsAuthorized(Permission.POWER_EAC_REPORT_OPS_REPORT);

            btnE_A_C.Visible = DataHelper.ParseToBoolean(Method.GetUtilityValue(Utility.EBILL_ENABLE));
            btnCAPACITY.Visible = false;
            btnPHASE.Visible = false;
            btnVOLTAGE.Visible = false;

            btnE_A_C.Enabled = Login.IsAuthorized(Permission.EAC_App);

            // Old Accouting permssion
            this.btnREPORT_INCOME.Enabled = Login.IsAuthorized(Permission.ACC_REPORT_INCOME);
            this.btnREPORT_EXPENSE.Enabled = Login.IsAuthorized(Permission.ACC_REPORT_EXPENSE);
            this.btnREPORT_PROFIT_LOSS.Enabled = Login.IsAuthorized(Permission.ACC_REPORT_PL);
            this.btnREPORT_FIX_ASSET.Enabled = Login.IsAuthorized(Permission.ACC_REPORT_FIXASSET_SUMMARY);
            this.btnREPORT_FIX_ASSET_DEPRECIATION.Enabled = Login.IsAuthorized(Permission.ACC_REPORT_DEPRECIATION);

            this.btnFIX_ASSET_TYPE.Enabled = Login.IsAuthorized(Permission.ACC_FIXASSET_CATEGORY);
            this.btnFIX_ASSET_ITEM.Enabled = Login.IsAuthorized(Permission.ACC_FIXASSET_ITEM);

            this.btnACCOUNT_CHART.Enabled = Login.IsAuthorized(Permission.ACC_CHART_ACCOUNTCHART);

            this.btnINCOME.Enabled = Login.IsAuthorized(Permission.ACC_TRANS_INCOME);
            this.btnEXCHANGE_RATE.Enabled = Login.IsAuthorized(Permission.ACC_TRANS_EXCHANGERATE);
        }

        #region Show Page
        private Control currentPage = null;
        private Dictionary<Type, Control> pages = new Dictionary<Type, Control>();
        private void showPage(Type type, object sender)
        {
            if (!this.pages.ContainsKey(type))
            {
                this.pages[type] = (Control)Activator.CreateInstance(type);
                this.pages[type].Size = this.main.Size;
                if (this.pages[type] is Form)
                {
                    Form frm = (Form)this.pages[type];
                    frm.TopLevel = false;
                    frm.Visible = true;
                    frm.FormBorderStyle = FormBorderStyle.None;
                }
                this.main.Controls.Add(this.pages[type]);
                this.pages[type].Dock = DockStyle.Fill;
                ResourceHelper.ApplyResource(this.pages[type]);
            }
            if (this.currentPage != this.pages[type])
            {
                this.pages[type].Show();
                if (this.currentPage != null)
                {
                    this.currentPage.Hide();
                }
                this.currentPage = this.pages[type];
            }
            this.POWER_MANAGEMENT_2.Text = ((Control)sender).Text;
        }
        #endregion ShowPage() 

        private void btnPowerTransformer_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePowerUsageByTransformer), sender);
        }
        private void btnDistribution_Click(object sender, EventArgs e)
        {
            //this.showPage(typeof(PageDistributionType), sender);
        }
        private void btnDistributionFacility_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageDistributionFacility), sender);
        }
        private void btnPurchaseAgreement_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePowerSource), sender);
        }
        private void btnPowerPurchase_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePowerPurchase), sender);
            ((PagePowerPurchase)this.currentPage).BindPowerSource();
        }
        private void btnVoltage_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageVoltage), sender);
        }
        private void btnSeal_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageSeal), sender);
        }
        private void btnAmpare_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageAmpere), sender);
        }
        private void btnPhase_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePhase), sender);
        }
        private void btnConstant_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageConstant), sender);
        }
        private void btnPageCapacity_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageCapacity), sender);
        }
        //private void btnReportPurchasePowerAgreement_Click(object sender, EventArgs e)
        //{
        //    this.showPage(typeof(PageReportPurchasePowerAgreement), sender);
        //} 
        //private void btnReportPurchaseAgreement_Click(object sender, EventArgs e)
        //{
        //    this.showPage(typeof(PageReportPurchaseAgreement), sender);
        //} 
        private void btnReportTransformerDetail_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportTransformerDetail), sender);
        }

        private void btnSummaryMeterByTransformer_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportSummaryMeterByTransformer), sender);
        }

        private void btnCustomerStatistics_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportCustomerStatistics), sender);
        }

        private void lblGenerator_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageGenerator), sender);
            ((PageGenerator)this.currentPage).lookup();
        }

        private void lblPowerGeneration_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePowerGeneration), sender);
            ((PagePowerGeneration)this.currentPage).BindGenerator();
        }

        private void lblLicensee_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageLicensee), sender);
        }

        //private void lblLicenseeConnection_Click(object sender, EventArgs e)
        //{
        //    this.showPage(typeof(PageLicenseeConnection), sender);
        //}

        private void btnLicenseVillage_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageLicenseVillage), sender);
        }


        private void btnPowerTotal_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePowerUsage), sender);
        }

        private void btnPowerAdjust_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PagePowerAdjust), sender);
        }

        private void btnFuelSource_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageFuelSource), sender);
        }

        private void btnFuelPurchase_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageFuelPurchase), sender);
            ((PageFuelPurchase)this.currentPage).lookup();
        }

        private void btnREPORT_QUARTERLY_Click(object sender, EventArgs e)
        {

            new DialogQuarterReport().ShowDialog();
        }

        private void btnREPORT_OPS_Click(object sender, EventArgs e)
        {
            new EPower.Interface.OPSReport.DialogOPSReport().ShowDialog();
        }

        private void btnREPORT_ANNUAL_Click(object sender, EventArgs e)
        {
            new DialogAnnualReport().Show();
        }

        private void btnREPORT_SUBSIDY_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportSubsidy), sender);
        }

        private void btnREPORT_CUSTOMER_USAGE_STATISTIC_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportCustomerUsageStatistic), sender);
        }
        private void btnEAC_Click(object sender, EventArgs e)
        {
            new DialogEAC().ShowDialog();
        }

        private void btnACCOUNT_CHART_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageAccount), sender);
        }

        private void btnINCOME_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageAccountTransactionIncome), btnINCOME);
        }

        private void btnEXCHANGE_RATE_Click(object sender, EventArgs e)
        {
            new DialogExchangeRate().ShowDialog();
        }

        private void btnREPORT_INCOME_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportTranIncomeDetail), sender);
        }

        private void btnREPORT_EXPENSE_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportTranExpenseDetail), sender);
        }

        private void btnREPORT_PROFIT_LOSS_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportProfitLoss), sender);
        }

        private void btnREPORT_FIX_ASSET_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportFixAsset), sender);
            ((PageReportFixAsset)this.currentPage).lookup();
        }

        private void btnREPORT_FIX_ASSET_DEPRECIATION_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageReportFixAssetDepreciation), sender);
            ((PageReportFixAssetDepreciation)this.currentPage).lookup();
        }

        private void btnFIX_ASSET_TYPE_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageFixAssetCategory), sender);
        }

        private void btnFIX_ASSET_ITEM_Click(object sender, EventArgs e)
        {
            this.showPage(typeof(PageFixAssetItem), sender);
        }
    }
}
