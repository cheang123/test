﻿using EPower.Base.Logic;
using EPower.Base.Properties;
using EPower.Logic;
using SoftTech;
using SoftTech.Component;
using SoftTech.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;

namespace EPower.Interface
{
    public partial class DialogAdjustment : ExDialog
    {
        #region Private Data
        TBL_CUSTOMER _objCustomer = new TBL_CUSTOMER();
        TBL_INVOICE _objInvoice = new TBL_INVOICE();
        TBL_INVOICE_DETAIL objInvoiceDetail = new TBL_INVOICE_DETAIL();

        TBL_USAGE _objUsageNew = new TBL_USAGE();
        TBL_USAGE _objUsageOld = new TBL_USAGE();
        string oldBasePrice = "";
        bool _blnStart = true;
        decimal oldusage = 0;
        #endregion Private Data

        #region Constructor
        public DialogAdjustment(TBL_CUSTOMER objCustomer, TBL_INVOICE objInvoice, int intPriceId)
        {
            InitializeComponent();
            // bind lookup value
            UIHelper.SetDataSourceToComboBox(this.cboCurrency, Lookup.GetCurrencies());

            // Utility 
            lblSUBSIDY_TARIFF.Visible =
            label2.Visible = lblStar_.Visible =
            lblCURRENCY.Visible =
            txtBASED_PRICE.Visible =
            cboCurrency.Visible = DataHelper.ParseToBoolean(Method.Utilities[Utility.ENABLE_ADJUST_BASED_PRICE_SUBSIDY]);

            this._objCustomer = objCustomer;
            objInvoice._CopyTo(this._objInvoice);
            oldBasePrice = Method.FormatPrice(objInvoice.BASED_PRICE, this._objInvoice.CURRENCY_ID);

            var ItemIds = new List<int>() { -1, 1 };
            this.objInvoiceDetail = DBDataContext.Db.TBL_INVOICE_DETAILs.Where(x => x.INVOICE_ID == objInvoice.INVOICE_ID && ItemIds.Contains(x.INVOICE_ITEM_ID)).OrderByDescending(x => x.INVOICE_DETAIL_ID).FirstOrDefault();
            this.loadUsage();
            txtTotalAmountNew.Text = txtTotalAmount.Text;
        }

        #endregion Constructor

        #region Method

        private void loadUsage()
        {
            try
            {
                var objView = from u in DBDataContext.Db.TBL_USAGEs
                              join m in DBDataContext.Db.TBL_METERs on u.METER_ID equals m.METER_ID
                              join iu in DBDataContext.Db.TBL_INVOICE_USAGEs on u.METER_ID equals iu.METER_ID
                              join c in DBDataContext.Db.TBL_CUSTOMERs on iu.CUSTOMER_ID equals c.CUSTOMER_ID
                              orderby u.USAGE_MONTH descending
                              where iu.INVOICE_ID == _objInvoice.INVOICE_ID
                                    && u.USAGE_MONTH.Date == _objInvoice.INVOICE_MONTH.Date
                                    && (u.CUSTOMER_ID == c.CUSTOMER_ID || u.CUSTOMER_ID == c.USAGE_CUSTOMER_ID)
                              select new { u.USAGE_ID, m.METER_CODE };

                //set meter code to combo box
                UIHelper.SetDataSourceToComboBox(cboMeterCode, objView);
                if (cboMeterCode.Items.Count > 0)
                {
                    cboMeterCode.SelectedIndex = 0;
                }

                // total amount before adjustment.
                txtTotalAmount.Text = UIHelper.FormatCurrency(_objInvoice.TOTAL_AMOUNT, _objInvoice.CURRENCY_ID);
                // keep it blank for confirm user to change amount
                txtTotalAmountNew.Text = "";
                this.cboCurrency.SelectedValue = _objInvoice.CURRENCY_ID;
            }
            catch (Exception exp)
            {
                MsgBox.ShowError(exp);
            }
        }

        private void read()
        {
            this.dtpStartUsage.Value = _objInvoice.START_DATE;
            this.dtpEndUsage.Value = _objInvoice.END_DATE;
            this.txtStartUsage.Text = _objUsageNew.START_USAGE.ToString(UIHelper._DefaultUsageFormat);
            this.txtEndUsage.Text = _objUsageNew.END_USAGE.ToString(UIHelper._DefaultUsageFormat);
            this.txtMultiplier.Text = _objUsageNew.MULTIPLIER.ToString("0");
            this.txtTotalUsage.Text = Method.GetTotalUsage(_objUsageNew).ToString("0");
            this.txtPrice.Text = Method.FormatPrice(objInvoiceDetail.PRICE, this._objInvoice.CURRENCY_ID);
            this.txtBASED_PRICE.Text = Method.FormatPrice(_objInvoice.BASED_PRICE, this._objInvoice.CURRENCY_ID);
            oldusage = DataHelper.ParseToDecimal(txtTotalUsage.Text);
        }

        private void write()
        {
            _objUsageNew.START_USAGE = DataHelper.ParseToDecimal(this.txtStartUsage.Text);
            _objUsageNew.END_USAGE = DataHelper.ParseToDecimal(this.txtEndUsage.Text);
            _objUsageNew.MULTIPLIER = DataHelper.ParseToInt(this.txtMultiplier.Text);
            _objUsageNew.IS_METER_RENEW_CYCLE = this.chkIS_NEW_CYCLE.Checked;
            _objInvoice.BASED_PRICE = DataHelper.ParseToDecimal(this.txtBASED_PRICE.Text);
            _objInvoice.CURRENCY_ID = (int)this.cboCurrency.SelectedValue;
            //Log new amount if user change debt 
            var culculatedAmount = DataHelper.ParseToDecimal(this.txtTotalAmountNew.Tag?.ToString() ?? "0");
            var newAmount = DataHelper.ParseToDecimal(this.txtTotalAmountNew.Text);
            var calulatedAmountAfterTrim = DataHelper.ParseToDecimal(culculatedAmount.ToString("#,##0.##"));

            if (calulatedAmountAfterTrim != newAmount)
            {
                txtTotalAmountNew.Tag = newAmount;
            }
        }

        private bool invalid()
        {
            txtEndUsage.ClearValidation();
            if (!DataHelper.IsNumber(txtEndUsage.Text))
            {
                txtEndUsage.SetValidation(string.Format(Resources.REQUIRED, lblEND_USAGE.Text));
                return true;
            }
            if (DataHelper.ParseToDecimal(this.txtTotalUsage.Text) < 0)
            {
                txtEndUsage.SetValidation(string.Format(Resources.REQUIRED, lblEND_USAGE.Text));
                return true;
            }
            if (DataHelper.ParseToDecimal(this.txtPrice.Text) < 0)
            {
                txtPrice.SetValidation(string.Format(Resources.REQUIRED_LOOKUP, lblPRICE.Text));
                return true;
            }
            if (DataHelper.ParseToDecimal(txtBASED_PRICE.Text) <= 0)
            {
                txtBASED_PRICE.SetValidation(string.Format(Resources.REQUIRED_LOOKUP, lblSUBSIDY_TARIFF.Text));
                return true;
            }
            return false;
        }

        private void SaveAdjustment()
        {
            int oldCurrencyId = _objInvoice.CURRENCY_ID, newCurrencyId = (int)cboCurrency.SelectedValue;
            this.write();

            decimal adjustAmount = DataHelper.ParseToDecimal(this.txtTotalAmountNew.Text) - DataHelper.ParseToDecimal(this.txtTotalAmount.Text);
            decimal adjustUsage = Method.GetTotalUsage(_objUsageNew) - Method.GetTotalUsage(_objUsageOld);

            // if user not change any thig do nothing

            if (txtStartUsage.Enabled = txtEndUsage.Enabled = txtMultiplier.Enabled = chkIS_NEW_CYCLE.Enabled = txtPrice.Enabled = txtTotalAmountNew.Enabled == true)
            {
                if (adjustAmount == 0 && adjustUsage == 0)
                {
                    return;
                }
                try
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        // UPDATE USAGE
                        TBL_USAGE obj = DBDataContext.Db.TBL_USAGEs.FirstOrDefault(x => x.USAGE_ID == _objUsageNew.USAGE_ID);
                        _objUsageNew._CopyTo(obj);
                        DBDataContext.Db.SubmitChanges();

                        if (oldCurrencyId != newCurrencyId)
                        {
                            // void invoice
                            BillLogic logic = new BillLogic();
                            long oldInvId = _objInvoice.INVOICE_ID;
                            _objInvoice.INVOICE_NO = Method.GetNextSequence(Sequence.Invoice, true);
                            _objInvoice.START_USAGE = _objUsageNew.START_USAGE;
                            _objInvoice.END_USAGE = _objUsageNew.END_USAGE;
                            _objInvoice.TOTAL_USAGE = Method.GetTotalUsage(_objUsageNew);
                            _objInvoice.TOTAL_AMOUNT = DataHelper.ParseToDecimal(this.txtTotalAmountNew.Tag?.ToString() ?? "0");
                            _objInvoice.SETTLE_AMOUNT = UIHelper.Round(DataHelper.ParseToDecimal(this.txtTotalAmountNew.Tag?.ToString() ?? "0"), newCurrencyId);
                            _objInvoice.CURRENCY_ID = newCurrencyId;
                            _objInvoice.BASED_PRICE = DataHelper.ParseToDecimal(txtBASED_PRICE.Text);
                            logic.reNewInvoice(_objInvoice, DataHelper.ParseToDecimal(txtPrice.Text), oldInvId);
                            logic.VoidInvoice(oldInvId);
                        }
                        else
                        {
                            // ADJUST INVOICE
                            Method.AdjustInvoiceAmount(adjustAmount, adjustUsage, _objUsageNew.METER_ID, _objInvoice, txtDescription.Text, oldCurrencyId, newCurrencyId, oldusage);
                            objInvoiceDetail = DBDataContext.Db.TBL_INVOICE_DETAILs.Where(x => x.INVOICE_ID == _objInvoice.INVOICE_ID).OrderByDescending(x => x.INVOICE_DETAIL_ID).FirstOrDefault();
                            objInvoiceDetail.PRICE = DataHelper.ParseToDecimal(txtPrice.Text);
                            DBDataContext.Db.SubmitChanges();
                        }
                        tran.Complete();
                    }
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception e)
                {
                    MsgBox.ShowWarning(e.Message, Resources.WARNING);
                }
            }
            else
            {
                try
                {
                    using (TransactionScope tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.MaxValue))
                    {
                        DateTime now = DBDataContext.Db.GetSystemDate();
                        string str = newCurrencyId != oldCurrencyId ? "​​ (ប្តូររូបិយប័ណ្ណពី " + Lookup.GetCurrencyName(oldCurrencyId) + " ទៅ " + Lookup.GetCurrencyName(newCurrencyId) + ")" : "";
                        if (oldCurrencyId != newCurrencyId)
                        {
                            // void invoice
                            long oldInvId = _objInvoice.INVOICE_ID;
                            BillLogic logic = new BillLogic();
                            _objInvoice.INVOICE_NO = Method.GetNextSequence(Sequence.Invoice, true);
                            _objInvoice.START_USAGE = _objUsageNew.START_USAGE;
                            _objInvoice.END_USAGE = _objUsageNew.END_USAGE;
                            _objInvoice.TOTAL_USAGE = Method.GetTotalUsage(_objUsageNew);
                            _objInvoice.TOTAL_AMOUNT = DataHelper.ParseToDecimal(this.txtTotalAmountNew.Tag?.ToString());
                            _objInvoice.SETTLE_AMOUNT = UIHelper.Round(DataHelper.ParseToDecimal(this.txtTotalAmountNew.Tag?.ToString()), newCurrencyId);
                            _objInvoice.CURRENCY_ID = newCurrencyId;
                            _objInvoice.BASED_PRICE = DataHelper.ParseToDecimal(txtBASED_PRICE.Text);
                            logic.reNewInvoice(_objInvoice, DataHelper.ParseToDecimal(txtPrice.Text), oldInvId);
                            logic.VoidInvoice(oldInvId);
                        }
                        else
                        {
                            //adjustment only base price on invoice
                            TBL_INVOICE objNewInvoice = new TBL_INVOICE();
                            objNewInvoice = DBDataContext.Db.TBL_INVOICEs.Where(x => x.INVOICE_ID == _objInvoice.INVOICE_ID).OrderByDescending(x => x.INVOICE_ID).FirstOrDefault();
                            objNewInvoice.BASED_PRICE = DataHelper.ParseToDecimal(txtBASED_PRICE.Text);
                            objNewInvoice.CURRENCY_ID = newCurrencyId;
                            TBL_INVOICE_DETAIL invDetail = DBDataContext.Db.TBL_INVOICE_DETAILs.FirstOrDefault(x => x.INVOICE_ID == objNewInvoice.INVOICE_ID);
                            string old = oldBasePrice;
                            string newBasePrice = Method.FormatPrice(DataHelper.ParseToDecimal(txtBASED_PRICE.Text), this._objInvoice.CURRENCY_ID);

                            //insert into TBL_INVOICE_ADJUSTMENT
                            TBL_INVOICE_ADJUSTMENT objInvoiceAdjust = new TBL_INVOICE_ADJUSTMENT()
                            {
                                INVOICE_ID = _objInvoice.INVOICE_ID,
                                CREATE_BY = SoftTech.Security.Logic.Login.CurrentLogin.LOGIN_NAME,
                                CREATE_ON = now,
                                USER_CASH_DRAWER_ID = Login.CurrentCashDrawer.USER_CASH_DRAWER_ID,
                                ADJUST_REASON = txtDescription.Text + " " + string.Format(Resources.BASE_PRICE_CHANGED, oldBasePrice, newBasePrice) + " " + str,
                                BEFORE_ADJUST_AMOUNT = _objInvoice.TOTAL_AMOUNT,
                                ADJUST_AMOUNT = 0,
                                METER_ID = _objUsageNew.METER_ID,
                                BEFORE_ADJUST_USAGE = oldusage,
                                ADJUST_USAGE = 0,
                                TAX_ADJUST_AMOUNT = 0,
                                EXCHANGE_RATE = invDetail.EXCHANGE_RATE,
                                EXCHANGE_RATE_DATE = invDetail.EXCHANGE_RATE_DATE,
                                INVOICE_ITEM_ID = invDetail.INVOICE_ITEM_ID
                            };
                            DBDataContext.Db.TBL_INVOICE_ADJUSTMENTs.InsertOnSubmit(objInvoiceAdjust);
                            DBDataContext.Db.SubmitChanges();
                        }
                        tran.Complete();
                    }
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception e)
                {
                    MsgBox.ShowWarning(e.Message, Resources.WARNING);
                }
            }
        }

        private void calculateTotalUsage()
        {
            decimal totalUsag = Method.GetTotalUsage(DataHelper.ParseToDecimal(this.txtStartUsage.Text),
                                                           DataHelper.ParseToDecimal(this.txtEndUsage.Text),
                                                           DataHelper.ParseToDecimal(this.txtMultiplier.Text),
                                                           this.chkIS_NEW_CYCLE.Checked);

            txtTotalUsage.Text = totalUsag.ToString(UIHelper._DefaultUsageFormat);
        }

        private void calculateTotalAmount()
        {
            var totalAmountnew = (DataHelper.ParseToDecimal(txtTotalUsage.Text) * DataHelper.ParseToDecimal(txtPrice.Text));
            txtTotalAmountNew.Text = totalAmountnew.ToString("#,##0.##");
            //txtTotalAmountNew.Tag = totalAmountnew;
        }

        private void getPrice()
        {
            if (_objCustomer.CUSTOMER_CONNECTION_TYPE_ID != (int)ConnectionType.Normal || _objInvoice.CURRENCY_ID != (int)Currency.KHR)
            {
                return;
            }
            int totalUsage = DataHelper.ParseToInt(txtTotalUsage.Text);

            // Auto change price for REF 2016
            if ((_objInvoice.INVOICE_MONTH >= new DateTime(2016, 3, 1) && _objInvoice.INVOICE_MONTH <= new DateTime(2017, 2, 1))
                && _objInvoice.BASED_PRICE == 800)
            {
                if (totalUsage >= 0 && totalUsage <= 10)
                {
                    txtPrice.Text = "480";
                }
                else if (totalUsage >= 11)
                {
                    txtPrice.Text = "800";
                }
            }
            // Auto change price for REF 2017
            else if (_objInvoice.INVOICE_MONTH >= new DateTime(2017, 3, 1) && _objInvoice.INVOICE_MONTH <= new DateTime(2018, 2, 1)
                && _objInvoice.BASED_PRICE == 790)
            {
                if (totalUsage >= 0 && totalUsage <= 10)
                {
                    txtPrice.Text = "480";
                }
                else if (totalUsage >= 11 && totalUsage <= 50)
                {
                    txtPrice.Text = "610";
                }
                else if (totalUsage >= 51)
                {
                    txtPrice.Text = "790";
                }
            }
            // Auto change price for REF 2018
            else if (_objInvoice.INVOICE_MONTH >= new DateTime(2018, 3, 1) && _objInvoice.INVOICE_MONTH <= new DateTime(2018, 12, 1)
               && _objInvoice.BASED_PRICE == 770)
            {
                if (totalUsage >= 0 && totalUsage <= 10)
                {
                    txtPrice.Text = "480";
                }
                else if (totalUsage >= 11 && totalUsage <= 50)
                {
                    txtPrice.Text = "610";
                }
                else if (totalUsage >= 51)
                {
                    txtPrice.Text = "770";
                }
            }
            // Auto change price for REF 2019
            else if (_objInvoice.INVOICE_MONTH.Year == 2019 || _objInvoice.INVOICE_MONTH.Year == 2020 && _objInvoice.INVOICE_MONTH.Month == 1)
            {
                if (totalUsage >= 0 && totalUsage <= 10)
                {
                    txtPrice.Text = "380";
                }
                else if (totalUsage >= 11 && totalUsage <= 50)
                {
                    txtPrice.Text = "480";
                }
                else if (totalUsage >= 51 && totalUsage <= 200)
                {
                    txtPrice.Text = "610";
                }
                else if (totalUsage >= 201)
                {
                    txtPrice.Text = "740";
                }
            }
            // Auto change price for REF 2020
            else if (_objInvoice.INVOICE_MONTH.Year == 2020 && _objInvoice.INVOICE_MONTH.Month > 1)
            {
                if (totalUsage >= 0 && totalUsage <= 10)
                {
                    txtPrice.Text = "380";
                }
                else if (totalUsage >= 11 && totalUsage <= 50)
                {
                    txtPrice.Text = "480";
                }
                else if (totalUsage >= 51 && totalUsage <= 200)
                {
                    txtPrice.Text = "610";
                }
                else if (totalUsage >= 201)
                {
                    txtPrice.Text = "730";
                }
            }
            // Auto change price for REF 2021
            else if (_objInvoice.INVOICE_MONTH.Year >= 2021 || _objInvoice.INVOICE_MONTH.Month > 1)
            {
                if (totalUsage >= 0 && totalUsage <= 10)
                {
                    txtPrice.Text = "380";
                }
                else if (totalUsage >= 11 && totalUsage <= 50)
                {
                    txtPrice.Text = "480";
                }
                else if (totalUsage >= 51 && totalUsage <= 200)
                {
                    txtPrice.Text = "610";
                }
                else if (totalUsage >= 201)
                {
                    txtPrice.Text = "730";
                }
            }
        }
        #endregion

        #region Events
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtStartUsage.Enabled = txtEndUsage.Enabled = txtMultiplier.Enabled = chkIS_NEW_CYCLE.Enabled = txtPrice.Enabled = txtTotalAmountNew.Enabled == true)
            {
                if (invalid())
                {
                    return;
                }

                if (!DataHelper.IsNumber(txtTotalAmountNew.Text))
                {
                    txtTotalAmountNew.SetValidation(string.Format(Resources.REQUIRED, lblSETTLE_AMOUNT.Text));
                    return;
                }

                txtTotalAmountNew.ClearValidation();
                if (DataHelper.ParseToDecimal(txtTotalAmount.Text) == DataHelper.ParseToDecimal(txtTotalAmountNew.Text))
                {
                    txtTotalAmountNew.SetValidation(string.Format(Resources.MSG_TOTAL_AMOUNT_NEW));
                    return;
                }
            }
            else if (DataHelper.ParseToDecimal(txtBASED_PRICE.Text) == _objInvoice.BASED_PRICE && (int)cboCurrency.SelectedValue == _objInvoice.CURRENCY_ID)
            {
                txtBASED_PRICE.SetValidation(Resources.MSG_PLEASE_CHANGE_BASEPRICE);
                return;
            }

            txtDescription.ClearValidation();
            if (txtDescription.Text.Trim() == "")
            {
                txtDescription.SetValidation(string.Format(Resources.REQUIRED, lblNOTE.Text));
                return;
            }
            Runner.Run(SaveAdjustment);
        }

        private void txtNumber(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtNewUsage_TextChanged(object sender, EventArgs e)
        {
            calculateTotalUsage();
            getPrice();
            calculateTotalAmount();

        }

        private void cboMeterCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            // set current usage to the new selected one
            TBL_USAGE objUsage = DBDataContext.Db.TBL_USAGEs.FirstOrDefault(x => x.USAGE_ID == (int)cboMeterCode.SelectedValue);
            objUsage._CopyTo(_objUsageNew);
            objUsage._CopyTo(_objUsageOld);
            read();
        }

        private void EnglishKey(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtDescription_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.Khmer;
        }

        private void chkMETER_IS_RENEW_CYCLE_CheckedChanged(object sender, EventArgs e)
        {
            calculateTotalUsage();
        }

        private void txtOldUsage_TextChanged(object sender, EventArgs e)
        {
            calculateTotalUsage();
            getPrice();
            calculateTotalAmount();
        }

        private void txtTotalAmountNew_KeyPress(object sender, KeyPressEventArgs e)
        {
            UIHelper.InputDecimalOnly(sender, e);
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            calculateTotalAmount();
        }

        private void btnCHANGE_LOG_Click(object sender, EventArgs e)
        {
            new DialogAdjustmentHistory(_objInvoice).ShowDialog();
        }

        private void content_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right && Control.ModifierKeys == Keys.Control)
            //{
            //    if (lblSUBSIDY_TARIFF.Visible)
            //    {
            //        lblSUBSIDY_TARIFF.Visible = label2.Visible = lblStar_.Visible = lblCURRENCY.Visible = txtBASED_PRICE.Visible = cboCurrency.Visible= false;
            //    }
            //    else if(!lblSUBSIDY_TARIFF.Visible)
            //    {
            //        lblSUBSIDY_TARIFF.Visible = label2.Visible = lblStar_.Visible = lblCURRENCY.Visible = txtBASED_PRICE.Visible = cboCurrency.Visible= true;
            //    }

            //}

        }

        private void txtBASED_PRICE_Enter(object sender, EventArgs e)
        {
            InputLanguage.CurrentInputLanguage = UIHelper.English;
        }

        private void txtTotalAmountNew_TextChanged(object sender, EventArgs e)
        {
            txtTotalAmountNew.ClearValidation();
        }
        #endregion
    }
}
